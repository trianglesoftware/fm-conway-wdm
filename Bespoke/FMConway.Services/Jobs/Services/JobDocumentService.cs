﻿using FMConway.Services.Jobs.Models;
using FMConway.Services.Files.Interface;
using FMConway.Services.Files.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.Jobs.Services
{
    public class JobDocumentService : Service, FMConway.Services.Jobs.Interfaces.IJobDocumentService
    {
        UserContext _userContext;
        IFileService _fileService;

        public JobDocumentService(UserContext userContext, IFileService fileService)
        {
            _userContext = userContext;
            _fileService = fileService;
        }

        public List<DocumentModel> GetActiveDocumentsForJob(Guid jobID)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            List<DocumentModel> documents = Context.JobDrawings.Where(jd => jd.JobFK == jobID && jd.IsActive).Select(DocumentModel.EntityToModel).ToList();

            return documents;
        }

        public DocumentModel GetDocument(Guid id)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.JobDrawings.Where(jd => jd.JobDrawingPK.Equals(id)).Select(DocumentModel.EntityToModel).SingleOrDefault();

            return document;
        }

        public void Create(Guid jobID, string drawingTitle, string drawingRevision, string fileName, string contentType, byte[] documentData)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var file = new FileModel()
            {
                FileDownloadName = fileName,
                ContentType = contentType,
                Data = documentData            
            };

            FileModel newFile = _fileService.Add(file);

            Drawing drawing = new Drawing();
            drawing.DrawingPK = Guid.NewGuid();
            drawing.DrawingTitle = drawingTitle;
            drawing.Revision = drawingRevision;
            drawing.FileFK = newFile.ID;
            drawing.IsActive = true;
            drawing.CreatedByFK = _userContext.UserDetails.ID;
            drawing.CreatedDate = DateTime.Now;
            drawing.UpdatedByFK = _userContext.UserDetails.ID;
            drawing.UpdatedDate = DateTime.Now;

            Context.Drawings.Add(drawing);

            JobDrawing jobDrawing = new JobDrawing();
            jobDrawing.JobDrawingPK = Guid.NewGuid();
            jobDrawing.JobFK = jobID;
            jobDrawing.DrawingFK = drawing.DrawingPK;
            jobDrawing.IsActive = true;
            jobDrawing.CreatedByFK = _userContext.UserDetails.ID;
            jobDrawing.CreatedDate = DateTime.Now;
            jobDrawing.UpdatedByFK = _userContext.UserDetails.ID;
            jobDrawing.UpdatedDate = DateTime.Now;

            Context.JobDrawings.Add(jobDrawing);

            SaveChanges();
        }

        //public void Create(Guid jobId, FileModel fileModel, Guid securityLevelId, string documentNumber)
        //{
        //    DocumentModel document = new DocumentModel()
        //    {
        //        DocumentID = Guid.NewGuid(),
        //        JobID = jobId,
        //        FileID = fileModel.ID,
        //        DocumentNumber = documentNumber,
        //        SecurityLevelID = securityLevelId,
        //        IsActive = true,
        //        CreatedByID = _userContext.UserDetails.ID,
        //        CreatedDate = DateTime.Now,
        //        UpdatedByID = _userContext.UserDetails.ID,
        //        UpdatedDate = DateTime.Now
        //    };

        //    _documents.Create(DocumentModel.ModelToEntity(document));
        //}

        public void Update(DocumentModel document)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            JobDrawing current = Context.JobDrawings.Where(x => x.JobDrawingPK.Equals(document.DocumentID)).SingleOrDefault();

            current.IsActive = document.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public void Inactivate(Guid id)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.JobDrawings.Where(jd => jd.JobDrawingPK.Equals(id)).Select(DocumentModel.EntityToModel).SingleOrDefault();
            if (document != null)
            {
                document.IsActive = false;
                Update(document);
            }
        }
    }
}
