﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.Jobs.Services
{
    public class JobStatusService : Service, FMConway.Services.Jobs.Interfaces.IJobStatusService
    {
        UserContext _userContext;

        public JobStatusService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<JobStatusModel> GetJobStatus()
        {
            return Context.Status.Where(x => x.IsJobStatus).OrderByDescending(x => x.Status1).Select(JobStatusModel.EntityToModel).ToList();
        }
    }
}
