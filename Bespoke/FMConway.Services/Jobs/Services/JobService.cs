﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Jobs.Extensions;
using FMConway.Services.Jobs.Interfaces;
using FMConway.Services.JobTypes.Interfaces;
using FMConway.Services.Areas.Interfaces;
using FMConway.Services.MethodStatements.Interfaces;
using FMConway.Services.ReasonCodes.Models;
using FMConway.Services.ToolboxTalks.Interfaces;
using FMConway.Services.AreaCertificates.Interfaces;
using FMConway.Services.ErrorLogging.Interfaces;

namespace FMConway.Services.Jobs.Services
{
    public class JobService : Service, FMConway.Services.Jobs.Interfaces.IJobService
    {
        UserContext _userContext;
        IErrorLoggingService _errorLoggingService;

        public IJobStatusService Status { get; set; }
        public IJobTypeService JobTypes { get; set; }
        public IAreaService JobAreas { get; set; }
        public IMethodStatementService MethodStatements { get; set; }
        public IToolboxTalkService ToolboxTalks { get; set; }
        public IAreaCertificateService AreaCertificates { get; set; }
        public IJobDocumentService Documents { get; set; }

        public JobService(UserContext userContext, IErrorLoggingService errorLoggingService)
        {
            _userContext = userContext;
            _errorLoggingService = errorLoggingService;
        }

        public Guid Create(JobModel model, Dictionary<Guid, string> jobTypes, Dictionary<Guid, string> areas, Dictionary<Guid, string> methodStatements, Dictionary<Guid, string> toolboxTalks, Dictionary<Guid, string> certificateTypes)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.JobID = Guid.NewGuid();

            model.JobTitle = model.JobTitle == null ? "" : model.JobTitle;
            model.CustomerProjectCode = model.CustomerProjectCode;
            model.Description = model.Description == null ? "" : model.Description;
            model.AddressLine1 = model.AddressLine1 == null ? "" : model.AddressLine1;
            model.AddressLine2 = model.AddressLine2 == null ? "" : model.AddressLine2;
            model.AddressLine3 = model.AddressLine3 == null ? "" : model.AddressLine3;
            model.City = model.City == null ? "" : model.City;
            model.County = model.County == null ? "" : model.County;
            model.Postcode = model.Postcode == null ? "" : model.Postcode;
            model.Comments = model.Comments == null ? "" : model.Comments;
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Jobs.Add(JobModel.ModelToEntity(model));

            if (jobTypes != null && jobTypes.Count > 0)
            {
                foreach (var jobType in jobTypes)
                {
                    JobJobType newJobType = new JobJobType();
                    newJobType.JobJobTypePK = Guid.NewGuid();
                    newJobType.JobFK = model.JobID;
                    newJobType.JobTypeFK = jobType.Key;
                    newJobType.IsActive = true;
                    newJobType.CreatedByFK = _userContext.UserDetails.ID;
                    newJobType.CreatedDate = DateTime.Now;
                    newJobType.UpdatedByFK = _userContext.UserDetails.ID;
                    newJobType.UpdatedDate = DateTime.Now;

                    Context.JobJobTypes.Add(newJobType);
                }
            }

            if (areas != null && areas.Count > 0)
            {
                foreach (var area in areas)
                {
                    JobArea newJobArea = new JobArea();
                    newJobArea.JobAreaPK = Guid.NewGuid();
                    newJobArea.JobFK = model.JobID;
                    newJobArea.AreaFK = area.Key;
                    newJobArea.IsActive = true;
                    newJobArea.CreatedByFK = _userContext.UserDetails.ID;
                    newJobArea.CreatedDate = DateTime.Now;
                    newJobArea.UpdatedByFK = _userContext.UserDetails.ID;
                    newJobArea.UpdatedDate = DateTime.Now;

                    Context.JobAreas.Add(newJobArea);
                }
            }

            if (methodStatements != null && methodStatements.Count > 0)
            {
                foreach (var methodStatement in methodStatements)
                {
                    JobMethodStatement newJobMethod = new JobMethodStatement();
                    newJobMethod.JobMethodStatementPK = Guid.NewGuid();
                    newJobMethod.JobFK = model.JobID;
                    newJobMethod.MethodStatementFK = methodStatement.Key;
                    newJobMethod.IsActive = true;
                    newJobMethod.CreatedByFK = _userContext.UserDetails.ID;
                    newJobMethod.CreatedDate = DateTime.Now;
                    newJobMethod.UpdatedByFK = _userContext.UserDetails.ID;
                    newJobMethod.UpdatedDate = DateTime.Now;

                    Context.JobMethodStatements.Add(newJobMethod);
                }
            }

            if (toolboxTalks != null && toolboxTalks.Count > 0)
            {
                foreach (var toolboxTalk in toolboxTalks)
                {
                    JobToolboxTalk newToolboxTalk = new JobToolboxTalk();
                    newToolboxTalk.JobToolboxTalkPK = Guid.NewGuid();
                    newToolboxTalk.JobFK = model.JobID;
                    newToolboxTalk.ToolboxTalkFK = toolboxTalk.Key;
                    newToolboxTalk.IsActive = true;
                    newToolboxTalk.CreatedByFK = _userContext.UserDetails.ID;
                    newToolboxTalk.CreatedDate = DateTime.Now;
                    newToolboxTalk.UpdatedByFK = _userContext.UserDetails.ID;
                    newToolboxTalk.UpdatedDate = DateTime.Now;

                    Context.JobToolboxTalks.Add(newToolboxTalk);
                }
            }

            if (certificateTypes != null && certificateTypes.Count > 0)
            {
                foreach (var certificateType in certificateTypes)
                {
                    JobCertificate jobCertificate = new JobCertificate();
                    jobCertificate.JobCertificatePK = Guid.NewGuid();
                    jobCertificate.JobFK = model.JobID;
                    jobCertificate.CertificateTypeFK = certificateType.Key;
                    jobCertificate.IsActive = true;
                    jobCertificate.CreatedByFK = _userContext.UserDetails.ID;
                    jobCertificate.CreatedDate = DateTime.Now;
                    jobCertificate.UpdatedByFK = _userContext.UserDetails.ID;
                    jobCertificate.UpdatedDate = DateTime.Now;

                    Context.JobCertificates.Add(jobCertificate);
                }
            }

            SaveChanges();

            return model.JobID;
        }

        public Guid Update(JobModel model,
            Dictionary<Guid, string> jobTypes, Dictionary<Guid, string> existingJobTypes,
            Dictionary<Guid, string> jobAreas, Dictionary<Guid, string> existingJobAreas,
            Dictionary<Guid, string> methodStatements, Dictionary<Guid, string> existingMethodStatements,
            Dictionary<Guid, string> toolboxTalks, Dictionary<Guid, string> existingToolboxTalks,
            Dictionary<Guid, string> certificateTypes, Dictionary<Guid, string> existingCertificateTypes)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Job current = Context.Jobs.Where(x => x.JobPK.Equals(model.JobID)).SingleOrDefault();

            current.DepotFK = model.DepotID;
            current.JobTitle = model.JobTitle == null ? "" : model.JobTitle;
            current.CustomerProjectCode = model.CustomerProjectCode;
            current.StartDate = model.StartDate;
            current.EndDate = model.EndDate;
            current.Description = model.Description == null ? "" : model.Description;
            current.AddressLine1 = model.AddressLine1 == null ? "" : model.AddressLine1;
            current.AddressLine2 = model.AddressLine2 == null ? "" : model.AddressLine2;
            current.AddressLine3 = model.AddressLine3 == null ? "" : model.AddressLine3;
            current.City = model.City == null ? "" : model.City;
            current.County = model.County == null ? "" : model.County;
            current.Postcode = model.Postcode == null ? "" : model.Postcode;
            current.Comments = model.Comments == null ? "" : model.Comments;
            current.Rural = model.Rural;
            current.IsSuccessful = model.IsSuccessful;
            current.ReasonCodeFK = model.ReasonCodeID;
            current.ReasonNotes = model.ReasonNotes;
            current.IsClientSignatureRequired = model.IsClientSignatureRequired;
            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            // job types
            jobTypes = jobTypes ?? new Dictionary<Guid, string>();
            if (!jobTypes.SequenceEqual(existingJobTypes))
            {
                var jobJobTypes = Context.JobJobTypes.Where(x => x.JobFK == model.JobID);
                foreach (var jobType in jobJobTypes)
                {
                    Context.JobJobTypes.Remove(jobType);
                }

                foreach (var jobType in jobTypes)
                {
                    JobJobType newJobType = new JobJobType();
                    newJobType.JobJobTypePK = Guid.NewGuid();
                    newJobType.JobFK = model.JobID;
                    newJobType.JobTypeFK = jobType.Key;
                    newJobType.IsActive = true;
                    newJobType.CreatedByFK = _userContext.UserDetails.ID;
                    newJobType.CreatedDate = DateTime.Now;
                    newJobType.UpdatedByFK = _userContext.UserDetails.ID;
                    newJobType.UpdatedDate = DateTime.Now;

                    Context.JobJobTypes.Add(newJobType);
                }
            }

            // job areas
            jobAreas = jobAreas ?? new Dictionary<Guid, string>();
            if (!jobAreas.SequenceEqual(existingJobAreas))
            {
                var currentJobAreas = Context.JobAreas.Where(x => x.JobFK == model.JobID);
                foreach (var area in currentJobAreas)
                {
                    Context.JobAreas.Remove(area);
                }

                foreach (var area in jobAreas)
                {
                    JobArea newJobArea = new JobArea();
                    newJobArea.JobAreaPK = Guid.NewGuid();
                    newJobArea.JobFK = model.JobID;
                    newJobArea.AreaFK = area.Key;
                    newJobArea.IsActive = true;
                    newJobArea.CreatedByFK = _userContext.UserDetails.ID;
                    newJobArea.CreatedDate = DateTime.Now;
                    newJobArea.UpdatedByFK = _userContext.UserDetails.ID;
                    newJobArea.UpdatedDate = DateTime.Now;

                    Context.JobAreas.Add(newJobArea);
                }
            }

            // method statements
            methodStatements = methodStatements ?? new Dictionary<Guid, string>();
            if (!methodStatements.SequenceEqual(existingMethodStatements))
            {
                var currentMethodStatements = Context.JobMethodStatements.Where(x => x.JobFK == model.JobID);
                foreach (var method in currentMethodStatements)
                {
                    Context.JobMethodStatements.Remove(method);
                }

                foreach (var method in methodStatements)
                {
                    JobMethodStatement newJobMethod = new JobMethodStatement();
                    newJobMethod.JobMethodStatementPK = Guid.NewGuid();
                    newJobMethod.JobFK = model.JobID;
                    newJobMethod.MethodStatementFK = method.Key;
                    newJobMethod.IsActive = true;
                    newJobMethod.CreatedByFK = _userContext.UserDetails.ID;
                    newJobMethod.CreatedDate = DateTime.Now;
                    newJobMethod.UpdatedByFK = _userContext.UserDetails.ID;
                    newJobMethod.UpdatedDate = DateTime.Now;

                    Context.JobMethodStatements.Add(newJobMethod);
                }
            }

            // toolbox talks
            toolboxTalks = toolboxTalks ?? new Dictionary<Guid, string>();
            if (!toolboxTalks.SequenceEqual(existingToolboxTalks))
            {
                var jobToolbox = Context.JobToolboxTalks.Where(x => x.JobFK == model.JobID);
                foreach (var toolbox in jobToolbox)
                {
                    Context.JobToolboxTalks.Remove(toolbox);
                }

                foreach (var toolbox in toolboxTalks)
                {
                    var newToolbox = new JobToolboxTalk();
                    newToolbox.JobToolboxTalkPK = Guid.NewGuid();
                    newToolbox.JobFK = model.JobID;
                    newToolbox.ToolboxTalkFK = toolbox.Key;
                    newToolbox.IsActive = true;
                    newToolbox.CreatedByFK = _userContext.UserDetails.ID;
                    newToolbox.CreatedDate = DateTime.Now;
                    newToolbox.UpdatedByFK = _userContext.UserDetails.ID;
                    newToolbox.UpdatedDate = DateTime.Now;

                    Context.JobToolboxTalks.Add(newToolbox);
                }
            }

            // certificate types
            certificateTypes = certificateTypes ?? new Dictionary<Guid, string>();
            if (!certificateTypes.SequenceEqual(existingCertificateTypes))
            {
                var jobCertificates = Context.JobCertificates.Where(x => x.JobFK == model.JobID);
                foreach (var jobCertificate in jobCertificates)
                {
                    Context.JobCertificates.Remove(jobCertificate);
                }

                foreach (var certificateType in certificateTypes)
                {
                    var jobCertificate = new JobCertificate();
                    jobCertificate.JobCertificatePK = Guid.NewGuid();
                    jobCertificate.JobFK = model.JobID;
                    jobCertificate.CertificateTypeFK = certificateType.Key;
                    jobCertificate.IsActive = true;
                    jobCertificate.CreatedByFK = _userContext.UserDetails.ID;
                    jobCertificate.CreatedDate = DateTime.Now;
                    jobCertificate.UpdatedByFK = _userContext.UserDetails.ID;
                    jobCertificate.UpdatedDate = DateTime.Now;

                    Context.JobCertificates.Add(jobCertificate);
                }
            }

            Context.SaveChanges();

            return model.JobID;
        }

        public PagedResults<JobModel> GetActiveJobsForSchemePaged(int rowCount, int page, string query, Guid schemeID)
        {
            var baseQuery = Context.Jobs.Where(x => x.SchemeFK == schemeID).ActiveJobs().OrderByDescending(o => o.JobNumber).JobOverviewQuery(query).Select(JobModel.EntityToModel);

            return PagedResults<JobModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<JobModel> GetLiveHireJobsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Jobs.ActiveJobs().OrderByDescending(o => o.StartDate).JobOverviewQuery(query).Select(JobModel.EntityToModel);

            return PagedResults<JobModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<JobModel> GetCompleteJobsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Jobs.ActiveJobs().OrderByDescending(o => o.StartDate).JobOverviewQuery(query).Select(JobModel.EntityToModel);

            return PagedResults<JobModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<JobModel> GetInactiveJobsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Jobs.InactiveJobs().OrderByDescending(o => o.StartDate).JobOverviewQuery(query).Select(JobModel.EntityToModel);

            return PagedResults<JobModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public JobModel Single(Guid id)
        {
            return Context.Jobs.Where(x => x.JobPK.Equals(id)).Select(JobModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<DrawingLookup> GetPagedActiveDrawingsForJobLookup(int rowCount, int page, string query, Guid jobID)
        {
            var baseQuery = Context.JobDrawings.Where(x => x.JobFK == jobID && x.Drawing.IsActive && x.IsActive && x.Drawing.DrawingTitle.Contains(query)).OrderBy(x => x.Drawing.DrawingTitle).Select(x => new DrawingLookup() { Key = x.Drawing.DrawingTitle, Value = x.Drawing.DrawingPK, Revision = x.Drawing.Revision });

            return PagedResults<DrawingLookup>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<JobLookup> GetPagedActiveJobsLookup(int rowCount, int page, string query, Guid? customerID = null)
        {
            var baseQuery = Context.Jobs.ActiveJobs()
                .Where(a => customerID == null || a.Contract.CustomerFK == customerID)
                .OrderBy(o => o.JobNumber)
                .JobOverviewQuery(query)
                .Select(JobLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<JobLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public bool AddDrawingromJob(Guid workInstructionID, List<Guid> drawingIDs)
        {
            foreach (var id in drawingIDs)
            {
                try
                {
                    var drawing = Context.Drawings.Where(d => d.DrawingPK == id).Single();

                    var wiDrawing = new WorkInstructionDrawing()
                    {
                        WorkInstructionDrawingPK = Guid.NewGuid(),
                        WorkInstructionFK = workInstructionID,
                        DrawingFK = id,
                        IsActive = true,
                        CreatedByFK = _userContext.UserDetails.ID,
                        CreatedDate = DateTime.Now,
                        UpdatedByFK = _userContext.UserDetails.ID,
                        UpdatedDate = DateTime.Now
                    };
                    Context.WorkInstructionDrawings.Add(wiDrawing);

                    var jobPackID = Context.WorkInstructions.Where(w => w.WorkInstructionPK == workInstructionID).Select(s => s.JobPackFK).SingleOrDefault();

                    Briefing briefing = new Briefing();
                    briefing.BriefingPK = Guid.NewGuid();
                    briefing.BriefingTypeFK = 5; // Document
                    briefing.JobPackFK = jobPackID;
                    briefing.Name = drawing.DrawingTitle;
                    briefing.Details = "";
                    briefing.FileFK = drawing.FileFK;
                    briefing.IsActive = true;
                    briefing.CreatedByID = _userContext.UserDetails.ID;
                    briefing.CreatedDate = DateTime.Now;
                    briefing.UpdatedByID = _userContext.UserDetails.ID;
                    briefing.UpdatedDate = DateTime.Now;

                    Context.Briefings.Add(briefing);
                    Context.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return true;
        }

        public JobDrawingModel GetJobDrawing (Guid jobDrawingID)
        {
            var res = new JobDrawingModel();

            var doc = Context.Drawings.Where(x => x.DrawingPK == jobDrawingID).Select(JobDrawingModel.EntityToModel).SingleOrDefault();

            if (doc != null)
                res = doc;

            return res;
        }

        public List<JobSpeedModel> GetJobSpeeds()
        {
            return Context.JobSpeeds.Where(a => a.IsActive).OrderBy(a => a.JobSpeedPK).Select(JobSpeedModel.EntityToModel).ToList();
        }

        public decimal GetActiveJobPriceLinesTotalRate(Guid jobID)
        {
            return Context.JobPriceLines.Where(a => a.JobFK == jobID && a.IsActive).DefaultIfEmpty().Sum(a => (a.Quantity ?? 0) * (a.Rate ?? 0));
        }

        public PagedResults<JobPriceLineModel> GetPagedActiveJobPriceLines(int rowCount, int page, string query, Guid jobID)
        {
            int intQuery = 0;
            var isIntQuery = int.TryParse(query, out intQuery);

            var baseQuery = Context.JobPriceLines
                .Where(a => a.IsActive && a.JobFK == jobID)
                .Where(a =>
                    (isIntQuery ? a.WorkInstructionPriceLine.WorkInstruction.WorkInstructionNumber == intQuery : false) ||
                    a.PriceLine.Code.Contains(query) ||
                    a.PriceLine.Description.Contains(query)
                    )
                .OrderBy(a => a.WorkInstructionPriceLine.WorkInstruction.WorkInstructionNumber).ThenBy(a => a.PriceLine.Code)
                .Select(JobPriceLineModel.EntityToModel);

            return PagedResults<JobPriceLineModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public Guid CreateJobPriceLine(JobPriceLineModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.JobPriceLineID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.JobPriceLines.Add(JobPriceLineModel.ModelToEntity(model));

            SaveChanges();

            return model.JobPriceLineID;
        }

        public Guid UpdateJobPriceLine(JobPriceLineModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var current = Context.JobPriceLines.Where(a => a.JobPriceLinePK == model.JobPriceLineID).Single();
            current.Quantity = model.Quantity;
            current.Rate = model.Rate;
            current.Note = model.Note;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            Context.SaveChanges();

            return model.JobID;
        }

        public JobPriceLineModel SingleJobPriceLine(Guid id)
        {
            return Context.JobPriceLines.Where(a => a.JobPriceLinePK == id).Select(JobPriceLineModel.EntityToModel).SingleOrDefault();
        }

        public Guid GetJobStatusID(string name)
        {
            return Context.Status.Where(a => a.IsJobStatus && a.IsActive && a.Status1 == name).Select(a => a.StatusPK).Single();
        }

        public PagedResults<ReasonCodeModel> GetPagedActiveReasonCodesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.ReasonCodes
                .Where(a => a.Code.Contains(query))
                .Select(ReasonCodeModel.EntityToModel);

            return PagedResults<ReasonCodeModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public bool AddPriceLines(Guid jobID, Guid[] priceLineIDs)
        {
            foreach (var id in priceLineIDs ?? new Guid[1])
            {
                var priceLine = Context.PriceLines.Where(x => x.PriceLinePK == id).SingleOrDefault();

                if (priceLine != null)
                {
                    using (var tran = Context.Database.BeginTransaction())
                    {
                        try
                        {
                            JobPriceLine line = new JobPriceLine()
                            {
                                JobPriceLinePK = Guid.NewGuid(),
                                PriceLineFK = id,
                                JobFK = jobID,
                                Quantity = 1,
                                Rate = priceLine.DefaultRate,
                                IsActive = true,
                                CreatedByFK = _userContext.UserDetails.ID,
                                CreatedDate = DateTime.Now,
                                UpdatedByFK = _userContext.UserDetails.ID,
                                UpdatedDate = DateTime.Now
                            };
                            Context.JobPriceLines.Add(line);
                            Context.SaveChanges();
                            tran.Commit();
                        }
                        catch (Exception e)
                        {
                            tran.Rollback();
                            _errorLoggingService.CreateErrorLog("CreateWIPriceLine", e, jobID.ToString());
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

    }
}
