﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Extensions
{
    public static class JobExtensions
    {
        public static IQueryable<Job> ActiveJobs(this IQueryable<Job> model)
        {
            return model.Where(a => a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Job> InactiveJobs(this IQueryable<Job> model)
        {
            return model.Where(a => !a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Job> JobOverviewQuery(this IQueryable<Job> model, string query)
        {
            if (!string.IsNullOrWhiteSpace(query))
            {
                var intQuery = 0;
                var isIntQuery = int.TryParse(query, out intQuery);

                DateTime dateQuery;
                var isDateQuery = DateTime.TryParse(query, out dateQuery);

                model = model.Where(a =>
                    (isIntQuery ? a.JobNumber == intQuery : false) ||
                    a.Contract.ContractNumber.Contains(query) ||
                    a.Contract.Customer.CustomerName.Contains(query) ||
                    a.Contract.Employee.EmployeeName.Contains(query) ||
                    a.Depot.DepotName.Contains(query) ||
                    (a.SchemeFK.HasValue ? a.Scheme.SchemeTitle.Contains(query) : false) ||
                    a.JobTitle.Contains(query) ||
                    (isDateQuery ? a.StartDate == dateQuery : false) ||
                    (isDateQuery ? a.EndDate == dateQuery : false) ||
                    a.Description.Contains(query) ||
                    a.Contract.Customer.PhoneNumber.Contains(query) ||
                    a.AddressLine1.Contains(query) ||
                    a.AddressLine2.Contains(query) ||
                    a.AddressLine3.Contains(query) ||
                    a.City.Contains(query) ||
                    a.County.Contains(query) ||
                    a.Postcode.Contains(query)
                    );
            }

            return model;
        }
    }
}
