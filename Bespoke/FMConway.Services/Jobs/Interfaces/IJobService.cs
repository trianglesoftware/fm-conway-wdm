﻿using FMConway.Services.AreaCertificates.Interfaces;
using FMConway.Services.Areas.Interfaces;
using FMConway.Services.Collections.Paging;
using FMConway.Services.ReasonCodes.Models;
using FMConway.Services.Jobs.Models;
using FMConway.Services.JobTypes.Interfaces;
using FMConway.Services.MethodStatements.Interfaces;
using FMConway.Services.ToolboxTalks.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Interfaces
{
    public interface IJobService : IService
    {
        IJobStatusService Status { get; set; }
        IJobTypeService JobTypes { get; set; }
        IAreaService JobAreas { get; set; }
        IMethodStatementService MethodStatements { get; set; }
        IToolboxTalkService ToolboxTalks { get; set; }
        IAreaCertificateService AreaCertificates { get; set; }
        IJobDocumentService Documents { get; set; }

        Guid Create(JobModel model, Dictionary<Guid, string> jobTypes, Dictionary<Guid, string> areas, Dictionary<Guid, string> methodStatements, Dictionary<Guid, string> toolboxTalks, Dictionary<Guid, string> certificateTypes);
        Guid Update(JobModel model, Dictionary<Guid, string> jobTypes, Dictionary<Guid, string> existingJobTypes, Dictionary<Guid, string> jobAreas, Dictionary<Guid, string> existingJobAreas, Dictionary<Guid, string> methodStatements, Dictionary<Guid, string> existingMethodStatements, Dictionary<Guid, string> toolboxTalks, Dictionary<Guid, string> existingToolboxTalks, Dictionary<Guid, string> certificateTypes, Dictionary<Guid, string> existingCertificateTypes);
        PagedResults<JobModel> GetActiveJobsForSchemePaged(int rowCount, int page, string query, Guid schemeID);
        PagedResults<JobModel> GetLiveHireJobsPaged(int rowCount, int page, string query);
        PagedResults<JobModel> GetCompleteJobsPaged(int rowCount, int page, string query);
        PagedResults<JobModel> GetInactiveJobsPaged(int rowCount, int page, string query);
        JobModel Single(Guid id);

        PagedResults<DrawingLookup> GetPagedActiveDrawingsForJobLookup(int rowCount, int page, string query, Guid jobID);
        PagedResults<JobLookup> GetPagedActiveJobsLookup(int rowCount, int page, string query, Guid? customerID = null);
        bool AddDrawingromJob(Guid workInstructionID, List<Guid> drawingIDs);

        JobDrawingModel GetJobDrawing(Guid jobDrawingID);
        List<JobSpeedModel> GetJobSpeeds();

        PagedResults<JobPriceLineModel> GetPagedActiveJobPriceLines(int rowCount, int page, string v, Guid jobID);
        JobPriceLineModel SingleJobPriceLine(Guid id);
        Guid CreateJobPriceLine(JobPriceLineModel jobPriceLine);
        Guid UpdateJobPriceLine(JobPriceLineModel jobPriceLine);
        decimal GetActiveJobPriceLinesTotalRate(Guid jobID);

        Guid GetJobStatusID(string name);

        bool AddPriceLines(Guid jobID, Guid[] priceLineIDs);
    }
}
