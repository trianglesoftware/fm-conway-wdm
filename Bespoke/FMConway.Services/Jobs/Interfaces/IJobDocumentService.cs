﻿using FMConway.Services.Jobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Interfaces
{
    public interface IJobDocumentService : IService
    {
        void Create(Guid jobId, string drawingTitle, string drawingRevision, string fileName, string contentType, byte[] documentData);

        void Update(DocumentModel document);
        void Inactivate(Guid id);
        DocumentModel GetDocument(Guid id);
        List<DocumentModel> GetActiveDocumentsForJob(Guid jobID);
    }
}
