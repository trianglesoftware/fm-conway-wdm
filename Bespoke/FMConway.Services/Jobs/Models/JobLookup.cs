﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class JobLookup : LookupBase<Guid>
    {
        public int Number { get; set; }

        public static Expression<Func<Job, JobLookup>> EntityToModel = entity => new JobLookup()
        {
            Key = entity.JobTitle,
            Value = entity.JobPK,
            Number = entity.JobNumber
        };
    }
}
