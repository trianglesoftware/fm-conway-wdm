﻿using FMConway.Services.JobTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class JobJobTypeModel
    {
        public Guid JobJobTypeID { get; set; }
        public Guid JobID { get; set; }
        public Guid JobTypeID { get; set; }
        public JobTypeModel JobType { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<JobJobType, JobJobTypeModel>> EntityToModel = entity => new JobJobTypeModel
        {
            JobJobTypeID = entity.JobJobTypePK,
            JobID = entity.JobFK,
            JobTypeID = entity.JobTypeFK,
            JobType = new JobTypeModel()
            {
                Name = entity.JobType.DisplayName
            },

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<JobJobTypeModel, JobJobType> ModelToEntity = model => new JobJobType()
        {
            JobJobTypePK = model.JobJobTypeID,
            JobFK = model.JobID,
            JobTypeFK = model.JobTypeID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
