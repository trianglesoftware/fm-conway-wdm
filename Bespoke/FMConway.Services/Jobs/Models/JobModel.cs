﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class JobModel
    {
        public Guid JobID { get; set; }
        public int JobNumber { get; set; }
        public string ContractNumber { get; set; }
        public string JobTitle { get; set; }
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }
        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }

        public string Comments { get; set; }
        public Guid? SchemeID { get; set; }
        public SchemeModel Scheme { get; set; }
        
        public Guid ContractID { get; set; }
        [Required(ErrorMessage = "Contract / Scheme is required")]
        public string ContractTitle { get; set; }

        public Guid DepotID { get; set; }
        [Required(ErrorMessage = "Depot is required")]
        public string DepotLookup { get; set; }

        [DisplayName("Client Signature Required")]
        public bool IsClientSignatureRequired { get; set; }
        public bool? IsSuccessful { get; set; }
        public Guid? ReasonCodeID { get; set; }
        public string ReasonCodeLookup { get; set; }
        public string ReasonNotes { get; set; }
        public bool Rural { get; set; }

        [Display(Name = "Untick if Cancelled / Aborted")]
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string Customer { get; set; }
        public string CustomerPhone { get; set; }
        public string ContractsManager { get; set; }

        public string CustomerProjectCode { get; set; }

        public static Expression<Func<Job, JobModel>> EntityToModel = entity => new JobModel
        {
            JobID = entity.JobPK,
            JobNumber = entity.JobNumber,
            ContractNumber = entity.Contract.ContractNumber,
            JobTitle = entity.JobTitle,
            StartDate = entity.StartDate,
            EndDate = entity.EndDate,
            Description = entity.Description,

            AddressLine1 = entity.AddressLine1,
            AddressLine2 = entity.AddressLine2,
            AddressLine3 = entity.AddressLine3,
            City = entity.City,
            County = entity.County,
            Postcode = entity.Postcode,

            Comments = entity.Comments,
            SchemeID = entity.SchemeFK,
            Scheme = new SchemeModel(){
                SchemeTitle = entity.Scheme.SchemeTitle
            },
            ContractID = entity.ContractFK,
            ContractTitle = entity.Contract.ContractTitle,
            DepotID = entity.DepotFK,
            DepotLookup = entity.Depot.DepotName,
            IsClientSignatureRequired = entity.IsClientSignatureRequired,
            IsSuccessful = entity.IsSuccessful,
            ReasonCodeID = entity.ReasonCodeFK,
            ReasonCodeLookup = entity.ReasonCode.Code,
            ReasonNotes = entity.ReasonNotes,
            Rural = entity.Rural,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate,

            Customer = entity.Contract.Customer.CustomerName,
            CustomerPhone = entity.Contract.Customer.PhoneNumber,
            ContractsManager = entity.Contract.Employee.EmployeeName,
            CustomerProjectCode = entity.CustomerProjectCode,
        };

        public static Func<JobModel, Job> ModelToEntity = model => new Job
        {
            JobPK = model.JobID,
            JobNumber = model.JobNumber,
            JobTitle = model.JobTitle,
            StartDate = model.StartDate,
            EndDate = model.EndDate,
            Description = model.Description,
            CustomerProjectCode = model.CustomerProjectCode,

            AddressLine1 = model.AddressLine1,
            AddressLine2 = model.AddressLine2,
            AddressLine3 = model.AddressLine3,
            City = model.City,
            County = model.County,
            Postcode = model.Postcode,

            Comments = model.Comments,
            SchemeFK = model.SchemeID,
            ContractFK = model.ContractID,
            DepotFK = model.DepotID,
            IsClientSignatureRequired = model.IsClientSignatureRequired,
            IsSuccessful = model.IsSuccessful,
            ReasonCodeFK = model.ReasonCodeID,
            ReasonNotes = model.ReasonNotes,
            Rural = model.Rural,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
