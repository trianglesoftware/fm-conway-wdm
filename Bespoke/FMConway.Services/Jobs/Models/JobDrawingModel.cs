﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class JobDrawingModel
    {
        public Guid DrawingID { get; set; }
        public string Title { get; set; }

        public static Expression<Func<Drawing, JobDrawingModel>> EntityToModel = entity => new JobDrawingModel
        {
            DrawingID = entity.DrawingPK,
            Title = entity.DrawingTitle,
        };
    }
}
