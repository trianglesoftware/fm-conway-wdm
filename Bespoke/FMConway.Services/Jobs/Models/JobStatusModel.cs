﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Models
{
    public class JobStatusModel
    {
        public Guid StatusID { get; set; }
        public string Status { get; set; }
        public bool IsJobStatus { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Status, JobStatusModel>> EntityToModel = entity => new JobStatusModel()
        {
            StatusID = entity.StatusPK,
            Status = entity.Status1,
            IsJobStatus = entity.IsJobStatus,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<JobStatusModel, Status> ModelToEntity = model => new Status()
        {
            StatusPK = model.StatusID,
            Status1 = model.Status,
            IsJobStatus = model.IsJobStatus,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
