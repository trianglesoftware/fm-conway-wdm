﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class JobStatuses
    {
        public static readonly string LiveHire = "Live Hire";
        public static readonly string Complete = "Complete";
    }
}
