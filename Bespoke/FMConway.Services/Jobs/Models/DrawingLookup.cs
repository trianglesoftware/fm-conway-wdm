﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class DrawingLookup : LookupBase<Guid>
    {
        public string Revision { get; set; }

        public static Expression<Func<Drawing, DrawingLookup>> EntityToModel = entity => new DrawingLookup()
        {
            Key = entity.DrawingTitle,
            Value = entity.DrawingPK,
            Revision = entity.Revision
        };
    }
}
