﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class JobSpeedModel
    {
        public static readonly string _12AB = "12AB Works";
        public static readonly string _12C = "12C Works";
        public static readonly string _12D = "12D Works";

        public Guid JobSpeedPK { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<JobSpeed, JobSpeedModel>> EntityToModel = entity => new JobSpeedModel
        {
            JobSpeedPK = entity.JobSpeedPK,
            Name = entity.Name,
            IsDefault = entity.IsDefault,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };
    }
}
