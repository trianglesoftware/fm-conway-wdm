﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class DocumentModel
    {
        public Guid DocumentID { get; set; }
        public Guid JobID { get; set; }
        public string DocumentTitle { get; set; }
        public string Revision { get; set; }
        public Guid FileID { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<JobDrawing, DocumentModel>> EntityToModel = entity => new DocumentModel()
        {
            DocumentID = entity.JobDrawingPK,
            DocumentTitle = entity.Drawing.DrawingTitle,
            Revision = entity.Drawing.Revision,
            JobID = entity.JobFK,
            FileID = entity.Drawing.FileFK,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<DocumentModel, JobDrawing> ModelToEntity = model => new JobDrawing()
        {
            JobDrawingPK = model.DocumentID,
            JobFK = model.JobID,
            DrawingFK = model.FileID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
