﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Jobs.Models
{
    public class JobPriceLineModel
    {
        public Guid JobPriceLineID { get; set; }
        public Guid JobID { get; set; }
        [Required(ErrorMessage = "Price Line is required")]
        public Guid? PriceLineFK { get; set; }
        public Guid? WorkInstructionPriceLineFK { get; set; }

        public int? WorksInstructionNumber { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Total { get; set; }
        public string Note { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<JobPriceLine, JobPriceLineModel>> EntityToModel = entity => new JobPriceLineModel
        {
            JobPriceLineID = entity.JobPriceLinePK,
            JobID = entity.JobFK,
            PriceLineFK = entity.PriceLineFK,
            WorkInstructionPriceLineFK = entity.WorkInstructionPriceLineFK,

            WorksInstructionNumber = entity.WorkInstructionPriceLine.WorkInstruction.WorkInstructionNumber,
            Code = entity.PriceLine.Code,
            Description = entity.PriceLine.Description,
            Quantity = entity.Quantity,
            Rate = entity.Rate,
            Total = (entity.Quantity ?? 0) * (entity.Rate ?? 0),
            Note = entity.Note,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<JobPriceLineModel, JobPriceLine> ModelToEntity = model => new JobPriceLine
        {
            JobPriceLinePK = model.JobPriceLineID,
            JobFK = model.JobID,
            PriceLineFK = (Guid)model.PriceLineFK,
            WorkInstructionPriceLineFK = model.WorkInstructionPriceLineFK,

            Quantity = model.Quantity,
            Rate = model.Rate,
            Note = model.Note,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
