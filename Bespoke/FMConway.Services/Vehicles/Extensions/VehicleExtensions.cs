﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Vehicles.Extensions
{
    public static class VehicleExtensions
    {
        public static IQueryable<Vehicle> ActiveVehicles(this IQueryable<Vehicle> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<Vehicle> InactiveVehicles(this IQueryable<Vehicle> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<Vehicle> VehicleOverviewQuery(this IQueryable<Vehicle> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Make.Contains(query) ||
                                          bq.Model.Contains(query) ||
                                          bq.Registration.Contains(query));
            }

            return model;
        }
    }
}
