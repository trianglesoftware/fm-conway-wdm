﻿using FMConway.Services.Depots.Models;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.Vehicles.Models
{
    public class VehicleModel
    {
        public Guid VehicleID { get; set; }
        [Required]
        public string Make { get; set; }
        [Required]
        public string Model { get; set; }
        [Required]
        public string Registration { get; set; }
        [Required(ErrorMessage = "The Vehicle Type is required")]
        public Guid VehicleTypeID { get; set; }
        public VehicleTypeModel VehicleType { get; set; }
        [Required(ErrorMessage = "The Depot is required")]
        public Guid DepotID { get; set; }
        public DepotModel Depot { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public List<DateTime> Absences { get; set; }

        public static Expression<Func<Vehicle, VehicleModel>> EntityToModel = entity => new VehicleModel
        {
            VehicleID = entity.VehiclePK,
            Make = entity.Make,
            Model = entity.Model,
            Registration = entity.Registration,
            VehicleTypeID = entity.VehicleTypeFK,
            VehicleType = new VehicleTypeModel()
            {
                Name = entity.VehicleType.Name
            },
            DepotID = entity.DepotFK,
            Depot = new DepotModel()
            {
                DepotName = entity.Depot.DepotName
            },

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<VehicleModel, Vehicle> ModelToEntity = model => new Vehicle
        {
            VehiclePK = model.VehicleID,
            Make = model.Make,
            Model = model.Model,
            Registration = model.Registration,
            VehicleTypeFK = model.VehicleTypeID,
            DepotFK = model.DepotID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
