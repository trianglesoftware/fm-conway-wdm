﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Vehicles.Models
{
    public class VehicleEmployeeModel
    {
        public Guid? EmployeeID { get; set; }
        public string Employee { get; set; }

        public static Expression<Func<Vehicle, VehicleEmployeeModel>> EntityToModel = a => new VehicleEmployeeModel
        {
            EmployeeID = a.Employees.Select(b => (Guid?)b.EmployeePK).FirstOrDefault(),
            Employee = a.Employees.Select(b => b.EmployeeName).FirstOrDefault(),
        };
    }
}
