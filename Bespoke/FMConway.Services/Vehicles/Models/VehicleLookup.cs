﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Vehicles.Models
{
    public class VehicleLookup : LookupBase<Guid>
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public string VehicleType { get; set; }

        public static Expression<Func<Vehicle, VehicleLookup>> EntityToModel = entity => new VehicleLookup()
        {
            Key = entity.Registration,
            Value = entity.VehiclePK,
            Make = entity.Make,
            Model = entity.Model,
            VehicleType = entity.VehicleType.Name
        };
    }
}
