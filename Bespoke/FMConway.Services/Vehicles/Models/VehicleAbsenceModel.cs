﻿using FMConway.Services.Depots.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Vehicles.Models
{
    public class VehicleAbsenceModel
    {
        public Guid VehicleAbsenceID { get; set; }
        public Guid VehicleID { get; set; }
        [Display(Name = "Registration")]
        public string Vehicle { get; set; }
        [Required(ErrorMessage = "Absence Reason is required")]
        [Display(Name = "Absence Reason")]
        public Guid? VehicleAbsenceReasonID { get; set; }
        public string VehicleAbsenceReason { get; set; }
        [Required(ErrorMessage = "From Date is required")]
        [Display(Name = "From Date")]
        public DateTime? FromDate { get; set; }
        [Required(ErrorMessage = "To Date is required")]
        [Display(Name = "To Date")]
        public DateTime? ToDate { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<VehicleAbsence, VehicleAbsenceModel>> EntityToModel = a => new VehicleAbsenceModel
        {
            VehicleAbsenceID = a.VehicleAbsencePK,
            VehicleID = a.VehicleFK,
            Vehicle = a.Vehicle.Registration,
            VehicleAbsenceReasonID = a.VehicleAbsenceReasonFK,
            VehicleAbsenceReason = a.VehicleAbsenceReason.Code,
            FromDate = a.FromDate,
            ToDate = a.ToDate,
            IsActive = a.IsActive,
            CreatedByID = a.CreatedByFK,
            CreatedDate = a.CreatedDate,
            UpdatedByID = a.UpdatedByFK,
            UpdatedDate = a.UpdatedDate,
        };

        public static Func<VehicleAbsenceModel, VehicleAbsence> ModelToEntity = a => new VehicleAbsence
        {
            VehicleAbsencePK = a.VehicleAbsenceID,
            VehicleFK = a.VehicleID,
            VehicleAbsenceReasonFK = (Guid)a.VehicleAbsenceReasonID,
            FromDate = (DateTime)a.FromDate,
            ToDate = (DateTime)a.ToDate,
            IsActive = a.IsActive,
            CreatedByFK = a.CreatedByID,
            CreatedDate = a.CreatedDate,
            UpdatedByFK = a.UpdatedByID,
            UpdatedDate = a.UpdatedDate,
        };
    }
}
