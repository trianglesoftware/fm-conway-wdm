﻿using FMConway.Services.Schedules.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Vehicles.Models
{
    public class ScheduleVehicleModel
    {
        public Guid VehicleID { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Registration { get; set; }

        public int HighestCount { get; set; }

        public List<ScheduleOverviewModel> Schedules { get; set; }
    }
}
