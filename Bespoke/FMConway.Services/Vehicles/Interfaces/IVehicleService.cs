﻿using FMConway.Services.Vehicles.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Vehicles.Interfaces
{
    public interface IVehicleService : IService
    {
        Guid Create(VehicleModel model);
        void Update(VehicleModel model);
        VehicleModel Single(Guid ID);

        List<VehicleModel> GetAllActiveVehicles(DateTime weekStart);
        List<VehicleModel> GetAllActiveVehiclesForDay(DateTime day);
        PagedResults<VehicleModel> GetActiveVehiclesPaged(int rowCount, int page, string query);

        List<VehicleModel> GetAllInactiveVehicles();
        PagedResults<VehicleModel> GetInactiveVehiclesPaged(int rowCount, int page, string query);
        PagedResults<VehicleLookup> GetPagedActiveVehiclesLookup(int rowCount, int page, string query);

        void CreateVehicleAbsence(VehicleAbsenceModel model);
        void UpdateVehicleAbsence(VehicleAbsenceModel model);
        VehicleAbsenceModel GetVehicleAbsence(Guid vehicleAbsenceID);
        PagedResults<VehicleAbsenceModel> GetActiveVehicleAbsencesPaged(int rowCount, int page, string query, Guid vehicleID);
        string GetVehicleRegistration(Guid vehicleID);

        VehicleEmployeeModel GetVehiclesEmployee(Guid employeeID);
    }
}
