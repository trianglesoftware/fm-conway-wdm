﻿using FMConway.Services.Vehicles.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Vehicles.Extensions;
using FMConway.Services.Schedules.Models;

namespace FMConway.Services.Vehicles.Services
{
    public class VehicleService : Service, FMConway.Services.Vehicles.Interfaces.IVehicleService
    {
        UserContext _userContext;

        public VehicleService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(VehicleModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.VehicleID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Vehicles.Add(VehicleModel.ModelToEntity(model));

            SaveChanges();

            return model.VehicleID;
        }

        public void Update(VehicleModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Vehicle current = Context.Vehicles.Where(x => x.VehiclePK.Equals(model.VehicleID)).Single();

            current.Make = model.Make == null ? "" : model.Make;
            current.Model = model.Model == null ? "" : model.Model;
            current.Registration = model.Registration == null ? "" : model.Registration;
            current.VehicleTypeFK = model.VehicleTypeID;
            current.DepotFK = model.DepotID;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public VehicleModel Single(Guid ID)
        {
            return Context.Vehicles.Where(a => a.VehiclePK.Equals(ID)).Select(VehicleModel.EntityToModel).SingleOrDefault();
        }

        public List<VehicleModel> GetAllActiveVehicles(DateTime day)
        {
            var vehicles = Context.Vehicles.ActiveVehicles().Select(VehicleModel.EntityToModel).OrderBy(v => v.Make).ThenBy(v => v.Model).ToList();
            day = day.Date;
            var nextDay = day.AddDays(1);
    
            foreach (var vehicle in vehicles)
            {
                vehicle.Absences = Context.sp_VehicleAbsences(vehicle.VehicleID).Where(a => a >= day && a < nextDay).Select(a => (DateTime)a).ToList();
            }

            return vehicles;
        }

        public List<VehicleModel> GetAllActiveVehiclesForDay(DateTime day)
        {
   
            var vehicles = Context.Vehicles.ActiveVehicles().Select(VehicleModel.EntityToModel).OrderBy(v => v.Make).ThenBy(v => v.Model).ToList();

            foreach (var vehicle in vehicles)
            {
                vehicle.Absences = Context.sp_VehicleAbsences(vehicle.VehicleID).Where(a => a == day).Select(a => (DateTime)a).ToList();
            }

            return vehicles;
        }

        public PagedResults<VehicleModel> GetActiveVehiclesPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Vehicles.ActiveVehicles().OrderBy(o => o.Make).VehicleOverviewQuery(query).Select(VehicleModel.EntityToModel);

            return PagedResults<VehicleModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<VehicleModel> GetAllInactiveVehicles()
        {
            return Context.Vehicles.InactiveVehicles().Select(VehicleModel.EntityToModel).ToList();
        }

        public PagedResults<VehicleModel> GetInactiveVehiclesPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Vehicles.InactiveVehicles().OrderBy(o => o.Make).VehicleOverviewQuery(query).Select(VehicleModel.EntityToModel);

            return PagedResults<VehicleModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<VehicleLookup> GetPagedActiveVehiclesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Vehicles.ActiveVehicles().OrderBy(o => o.Make).VehicleOverviewQuery(query).Select(VehicleLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<VehicleLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(page * rowCount).Take(rowCount).ToList()
            };
        }

        public string GetVehicleRegistration(Guid vehicleID)
        {
            return Context.Vehicles.Where(a => a.VehiclePK == vehicleID).Select(a => a.Registration).Single();
        }

        #region Vehicle Absence

        public void CreateVehicleAbsence(VehicleAbsenceModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.VehicleAbsenceID = Guid.NewGuid();

            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.VehicleAbsences.Add(VehicleAbsenceModel.ModelToEntity(model));

            SaveChanges();
        }

        public void UpdateVehicleAbsence(VehicleAbsenceModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var vehicleAbsence = Context.VehicleAbsences.Where(a => a.VehicleAbsencePK == model.VehicleAbsenceID).Single();

            vehicleAbsence.VehicleFK = model.VehicleID;
            vehicleAbsence.VehicleAbsenceReasonFK = (Guid)model.VehicleAbsenceReasonID;
            vehicleAbsence.FromDate = (DateTime)model.FromDate;
            vehicleAbsence.ToDate = (DateTime)model.ToDate;
            vehicleAbsence.IsActive = model.IsActive;
            vehicleAbsence.UpdatedByFK = _userContext.UserDetails.ID;
            vehicleAbsence.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public VehicleAbsenceModel GetVehicleAbsence(Guid vehicleAbsenceID)
        {
            return Context.VehicleAbsences.Where(a => a.VehicleAbsencePK == vehicleAbsenceID).Select(VehicleAbsenceModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<VehicleAbsenceModel> GetActiveVehicleAbsencesPaged(int rowCount, int page, string query, Guid vehicleID)
        {
            DateTime dateQuery;
            var isDateQuery = DateTime.TryParse(query, out dateQuery);

            var baseQuery = Context.VehicleAbsences.Where(a =>
                a.IsActive && (
                a.VehicleAbsenceReason.Code.Contains(query) ||
                (isDateQuery ? dateQuery >= a.FromDate && dateQuery <= a.ToDate : false)
                )).OrderByDescending(a => a.FromDate).Select(VehicleAbsenceModel.EntityToModel);

            return PagedResults<VehicleAbsenceModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        #endregion

        public VehicleEmployeeModel GetVehiclesEmployee(Guid vehicleID)
        {
            return Context.Vehicles.Where(a => a.VehiclePK == vehicleID).Select(VehicleEmployeeModel.EntityToModel).Single();
        }
    }
}
