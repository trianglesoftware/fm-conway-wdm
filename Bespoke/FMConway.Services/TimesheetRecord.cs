//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FMConway.Services
{
    using System;
    using System.Collections.Generic;
    
    public partial class TimesheetRecord
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TimesheetRecord()
        {
            this.TimesheetRecordWorkInstructions = new HashSet<TimesheetRecordWorkInstruction>();
        }
    
        public System.Guid TimesheetRecordPK { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public int WorkedHours { get; set; }
        public int WorkedMins { get; set; }
        public int BreakHours { get; set; }
        public int BreakMins { get; set; }
        public int TravelHours { get; set; }
        public int TravelMins { get; set; }
        public bool IPVAllowance { get; set; }
        public bool PaidBreakAllowance { get; set; }
        public bool OnCallAllowance { get; set; }
        public System.Guid TimesheetFK { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.Guid CreatedByFK { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public System.Guid UpdatedByFK { get; set; }
    
        public virtual Timesheet Timesheet { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimesheetRecordWorkInstruction> TimesheetRecordWorkInstructions { get; set; }
    }
}
