﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Helpers
{
    public class GpsExtensions
    {
        public static bool IsLocationValid(decimal? longitude, decimal? latitude)
        {
            if (longitude == null || latitude == null)
            {
                return false;
            }

            if (longitude < -180 || longitude > 180 || latitude < -90 || latitude > 90)
            {
                return false;
            }

            if (latitude == 0 && longitude == 0)
            {
                return false;
            }

            return true;
        }
    }
}
