﻿using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Reports.Models;
using FMConway.Services.WorkInstructions.Models;

namespace FMConway.Services.Reports.Interfaces
{
    public interface IReportService : IService
    {
        PagedResults<JobPriceLineReportModel> GetPagedActiveJobPriceLines(int rowCount, int page, string v);
        PagedResults<WorkInstructionPriceLineReportModel> GetPagedActiveWorkInstructionPriceLines(int rowCount, int page, string v);
        PagedResults<EmployeeAbsenceReportModel> GetPagedActiveEmployeeAbsences(int rowCount, int page, string v);
        PagedResults<VehicleAbsenceReportModel> GetPagedActiveVehicleAbsences(int rowCount, int page, string v);
        PagedResults<QualificationExpiryReportModel> GetPagedActiveQualificationExpiry(int rowCount, int page, string v);

        [Obsolete]
        byte[] GenerateWorkInstructionReport(Guid workInstructionID, string imagesPath, out string filename);
        byte[] GenerateJobSheetReport(Guid workInstructionID, out string filename);

        PagedResults<DailyDiaryReportModel> GetPagedActiveDailyDiary(int rowCount, int page, string v, DateTime? startDate);
        PagedResults<TimesheetInformationModel> GetTimesheetInformationPaged(int rowCount, int page, string query);
        PagedResults<TimesheetInformationDetailModel> GetTimesheetInformationDetailPaged(int rowCount, int page, string query, Guid employeeID);
        PagedResults<WeeklyTimesheetReportModel> GetPagedActiveWeeklyTimesheet(int rowCount, int page, string v, DateTime startDate, Guid employeeID);
        PagedResults<PlannedWorksReportModel> GetPagedActivePlannedWorks(int rowCount, int page, string v);
        PagedResults<OnHireInstalledReportModel> GetPagedActiveOnHireInstalled(int rowCount, int page, string v);
        PagedResults<TrackerReportModel> GetPagedActiveTracker(int rowCount, int page, string v, Guid? contractID, DateTime? date);
        PagedResults<PerformanceReportModel> GetPagedActivePerformance(int rowCount, int page, string v);
        PagedResults<ActiveTasksModel> GetPagedActiveTasks(int rowCount, int page, string v);
        PagedResults<CommercialModel> GetPagedActiveCommercial(int rowCount, int page, string v);
        PagedResults<OnHireReportModel>GetPagedActiveOnHire(int rowCount, int page, string v);
        List<WorkInstructionModel> GetActiveTasks();
        bool SetTasksToInvoiced(List<Guid> ids, DateTime invoiceDate);

        int GetWorkInstructionNumber(Guid workInstructionID);
    }
}
