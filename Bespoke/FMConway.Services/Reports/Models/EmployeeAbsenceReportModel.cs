﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class EmployeeAbsenceReportModel
    {
        public Guid EmployeeAbsenceID { get; set; }
        public Guid EmployeeID { get; set; }

        public string Name { get; set; }
        public string Reason { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public static Expression<Func<EmployeeAbsence, EmployeeAbsenceReportModel>> EntityToModel = a => new EmployeeAbsenceReportModel
        {
            EmployeeAbsenceID = a.EmployeeAbsencePK,
            EmployeeID = a.EmployeeFK,

            Name = a.Employee.EmployeeName,
            Reason = a.EmployeeAbsenceReason.Code,
            From = a.FromDate,
            To = a.ToDate
        };
    }
}
