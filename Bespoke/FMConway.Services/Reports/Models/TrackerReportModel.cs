﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class TrackerReportModel
    {
        public Guid WorkInstructionID { get; set; }

        public string ContractNumber { get; set; }
        public int JobNumber { get; set; }
        public int TaskNumber { get; set; }
        public string Activities { get; set; }
        public string Address { get; set; }
        public string Depot { get; set; }
        public string Status { get; set; }
        public DateTime WorkInstructionCreatedDate { get; set; }
        public string IssuedBy { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? TimeArrived { get; set; }
        public bool? IsSuccessful { get; set; }
        public string AbortReason { get; set; }
        public string TaskDetails { get; set; }

        public static Expression<Func<WorkInstruction, TrackerReportModel>> EntityToModel = a => new TrackerReportModel
        {
            WorkInstructionID = a.WorkInstructionPK,

            ContractNumber = a.Job.Contract.ContractNumber,
            JobNumber = a.Job.JobNumber,
            TaskNumber = a.WorkInstructionNumber,
            Activities = a.TrafficManagementActivities,
            Address = StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK),
            Depot = a.Depot.DepotName,
            Status = a.Status.Status1,
            WorkInstructionCreatedDate = a.CreatedDate,
            IssuedBy = a.Employee.EmployeeName,
            StartDate = a.WorkInstructionStartDate,
            TimeArrived = a.Tasks.Where(b => b.Name == "Arrive on Site").Select(b => b.EndTime).FirstOrDefault(),
            IsSuccessful = a.IsSuccessful,
            AbortReason = a.ReasonNotes,
            TaskDetails = a.Comments
        };
    }
}
