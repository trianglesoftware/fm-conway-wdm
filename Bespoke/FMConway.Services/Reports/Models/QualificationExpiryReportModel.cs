﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class QualificationExpiryReportModel
    {
        public Guid EmployeeCertificatePK { get; set; }
        public Guid EmployeeID { get; set; }

        public string Employee { get; set; }
        public string CertificateName { get; set; }
        public string CertificateType { get; set; }
        public string Area { get; set; }
        public string Description { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string DocumentUploaded { get; set; }

        public static Expression<Func<EmployeeCertificate, QualificationExpiryReportModel>> EntityToModel = a => new QualificationExpiryReportModel
        {
            EmployeeCertificatePK = a.EmployeeCertificatePK,
            EmployeeID = a.EmployeeFK,

            Employee = a.Employee.EmployeeName,
            CertificateName = a.Name,
            CertificateType = a.CertificateType.Name,
            Area = a.Area.Area1,
            Description = a.Description,
            ExpiryDate = a.ExpiryDate,
            DocumentUploaded = a.CertificateDocuments.Any(b => b.IsActive && b.File != null) ? "Yes" : "",
        };
    }
}
