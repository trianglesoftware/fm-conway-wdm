﻿using FMConway.Services.WorkInstructions.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class OnHireReportModel
    {
        public Guid WorkInstructionID { get; set; }
        public DateTime Day { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string ShiftType { get; set; }
        public string Times { get; set; }
        public string EquipOnHire { get; set; }
        public int QuantOnHire { get; set; }
        public string OnHire { get; set; }

        public int? CollectionTask { get; set; }
        public string CollectionStatus { get; set; }
        public DateTime? CollectionDate { get; set; }
        public string TMProjectCode { get; set; }
        public string Customer { get; set; }
        public string Authoriser { get; set; }
        public string CustProjectCode { get; set; }
        public string Chargeable { get; set; }
        public string Address { get; set; }
        public string Borough { get; set; }
        public string TMResource { get; set; }
        public decimal Quantity { get; set; }
        public string WorksDescription { get; set; }
        public string SiteContact { get; set; }
        public string SORCode { get; set; }
        public string SORDescription { get; set; }
        public string JobNotes { get; set; }
        public string PO { get; set; }
        public TaskEquipment TaskEquipment { get; set; }
        public ICollection<TaskEquipmentAsset> TaskEquipmentAssets { get; set; }
        public string Names { get; set; }
        public string Vehicles { get; set; }
        public string AssetNumber { get; set; }
        public string AssetDescription { get; set; }
        public ICollection<WorkInstructionPriceLine> PriceLines { get; set; }
        public Shift Shift { get; set; }


        public static Expression<Func<TaskEquipmentAsset, OnHireReportModel>> ETM = e => new OnHireReportModel
        {
            WorkInstructionID = e.TaskEquipment.Task.WorksInstructionFK,
            Day = e.TaskEquipment.Task.WorkInstruction.WorkInstructionStartDate,
            Date = e.TaskEquipment.Task.WorkInstruction.WorkInstructionStartDate,
            StartDate = e.TaskEquipment.Task.WorkInstruction.WorkInstructionStartDate,
            FinishDate = e.TaskEquipment.Task.WorkInstruction.WorkInstructionFinishDate,

            QuantOnHire = e.TaskEquipment.Quantity,
            //OnHire = e.TaskEquipment.Task.WorkInstruction.InstallWIs.Any(b => b.IsActive && b.StatusFK == WorkInstructionStatusesEnum.Complete) ? "N" : "Y",

            //CollectionTask = e.TaskEquipment.Task.WorkInstruction.InstallWIs.Where(b => b.IsActive).Select(b => (int?)b.WorkInstructionNumber).FirstOrDefault(),
            //CollectionStatus = e.TaskEquipment.Task.WorkInstruction.InstallWIs.Where(b => b.IsActive).Select(b => b.Status.Status1).FirstOrDefault(),
            //CollectionDate = e.TaskEquipment.Task.WorkInstruction.InstallWIs.Where(b => b.IsActive).Select(b => (DateTime?)b.WorkInstructionStartDate).FirstOrDefault(),

            TMProjectCode = e.TaskEquipment.Task.WorkInstruction.Job.Contract.ContractNumber,
            Customer = e.TaskEquipment.Task.WorkInstruction.Job.Contract.ContractTitle,
            Authoriser = e.TaskEquipment.Task.WorkInstruction.Employee.EmployeeName,
            CustProjectCode = e.TaskEquipment.Task.WorkInstruction.Job.CustomerProjectCode,
            Chargeable = e.TaskEquipment.Task.WorkInstruction.IsChargeable ? "Y" : "N",
            Address = e.TaskEquipment.Task.WorkInstruction.AddressLine1 == "" ? "" : e.TaskEquipment.Task.WorkInstruction.AddressLine1 + ", " + e.TaskEquipment.Task.WorkInstruction.Postcode,
            Borough = e.TaskEquipment.Task.WorkInstruction.AddressLine3,

            PriceLines = e.TaskEquipment.Task.WorkInstruction.WorkInstructionPriceLines,
            Quantity = e.TaskEquipment.Task.WorkInstruction.WorkInstructionPriceLines.Count(),
            WorksDescription = e.TaskEquipment.Task.WorkInstruction.JobType.DisplayName,
            SiteContact = e.TaskEquipment.Task.WorkInstruction.Employee.EmployeeName,

            
            AssetNumber = e.AssetNumber,
            AssetDescription = e.TaskEquipment.Equipment.EquipmentName,

            JobNotes = e.TaskEquipment.Task.WorkInstruction.Job.Comments,
            TaskEquipmentAssets = e.TaskEquipment.TaskEquipmentAssets,
            TaskEquipment = e.TaskEquipment,
            Shift = e.TaskEquipment.Task.WorkInstruction.JobPack.Shift,

        };

    }
}
