﻿using FMConway.Services.Employees.Enum;
using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
	public class PerformanceReportModel
	{
		public Guid WorkInstructionID { get; set; }

		public int Task { get; set; }
		public int Job { get; set; }
		public string Customer { get; set; }
		public string ContractManager { get; set; }
		public string ContractNumber { get; set; }
		public string JobTitle { get; set; }
		public DateTime StartDate { get; set; }
		public string JobType { get; set; }
		public string Activity { get; set; }
		public string Foreman { get; set; }
		public string SchemeTitle { get; set; }
		public bool? IsSuccessful { get; set; }
		public string IsSuccessfulStr { get; set; }
		public string ReasonCode { get; set; }
		public string ReasonNotes { get; set; }

		public static Expression<Func<WorkInstruction, PerformanceReportModel>> EntityToModel = a => new PerformanceReportModel
		{
			WorkInstructionID = a.WorkInstructionPK,

			Task = a.WorkInstructionNumber,
			Job = a.Job.JobNumber,
			Customer = a.Job.Contract.Customer.CustomerName,
			ContractManager = a.Job.Contract.Employee.EmployeeName,
			ContractNumber = a.Job.Contract.ContractNumber,
			JobTitle = a.Job.JobTitle,
			StartDate = a.WorkInstructionStartDate,
			JobType = a.JobType.DisplayName,
			Activity = a.TrafficManagementActivities,
			Foreman = a.Schedules.SelectMany(b => b.Shift.ShiftStaffs).Where(b => b.Employee.EmployeeType.EmployeeType1 == EmployeeTypesEnum.CrewForeman).Select(b => b.Employee.EmployeeName).FirstOrDefault(),
			SchemeTitle = a.Job.SchemeFK.HasValue ? a.Job.Scheme.SchemeTitle : a.Job.Contract.ContractTitle,
			IsSuccessful = a.IsSuccessful,
			ReasonCode = a.ReasonCode.Code,
			ReasonNotes = a.ReasonNotes

		};

		public static Expression<Func<vv_PerformanceReport, PerformanceReportModel>> ViewToModel = v => new PerformanceReportModel
		{
			WorkInstructionID = v.WorkInstructionPK,
			Task = v.WorkInstructionNumber,
			Job = v.JobNumber,
			Customer = v.CustomerName,
			ContractManager = v.EmployeeName,
			ContractNumber = v.ContractNumber,
			JobTitle = v.JobTitle,
			StartDate = v.WorkInstructionStartDate,
			JobType = v.JobType,
			Activity = v.TrafficManagementActivities,
			Foreman = v.Foreman,
			SchemeTitle = v.SchemeTitle,
			IsSuccessfulStr = v.IsSuccessfulStr,
			IsSuccessful = v.IsSuccessful,
			ReasonCode = v.Code,
			ReasonNotes = v.ReasonNotes
		};

	}
}
