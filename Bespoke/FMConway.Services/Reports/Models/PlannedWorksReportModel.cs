﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class PlannedWorksReportModel
    {
        public Guid WorkInstructionID { get; set; }

        public int Task { get; set; }
        public int Job { get; set; }
        public string Customer { get; set; }
        public string ContractManager { get; set; }
        public string ContractNumber { get; set; }
        public string JobTitle { get; set; }
        public DateTime StartDate { get; set; }
        public string SRW { get; set; }
        public string IssuedBy { get; set; }
        public bool Scheduled { get; set; }
        public string Status { get; set; }
        public bool? Successful { get; set; }
        public string JobType { get; set; }
        public string Address { get; set; }

        public static Expression<Func<WorkInstruction, PlannedWorksReportModel>> EntityToModel = a => new PlannedWorksReportModel
        {
            WorkInstructionID = a.WorkInstructionPK,

            Task = a.WorkInstructionNumber,
            Job = a.Job.JobNumber,
            Customer = a.Job.Contract.Customer.CustomerName,
            ContractManager = a.Job.Contract.Employee.EmployeeName,
            ContractNumber = a.Job.Contract.ContractNumber,
            JobTitle = a.Job.JobTitle,
            StartDate = a.WorkInstructionStartDate,
            SRW = a.SRWNumber,
            IssuedBy = a.Employee.EmployeeName,
            Scheduled = a.IsScheduled,
            Status = a.Status.Status1,
            Successful = a.IsSuccessful,
            JobType = a.JobType.DisplayName,
            Address = StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK)
        };
    }
}
