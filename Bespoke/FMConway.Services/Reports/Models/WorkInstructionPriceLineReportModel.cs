﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class WorkInstructionPriceLineReportModel
    {
        public Guid WorkInstructionID { get; set; }
        public Guid? WorkInstructionPriceLineID { get; set; }

        public int JobNumber { get; set; }
        public string JobTitle { get; set; }
        public string HasPriceLines { get; set; }
        public int? WorkInstructionNumber { get; set; }
        public string Customer { get; set; }
        public string ContractNumber { get; set; }
        public string CustomerCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal? Rate { get; set; }
        public string Note { get; set; }
    }
}
