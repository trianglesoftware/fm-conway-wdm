﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class WeeklyTimesheetReportModel
    {
        public Guid WorkInstructionID { get; set; }

        public string Name { get; set; }
        public string JobTitle { get; set; }
        public string ClockNo { get; set; }
        public string Mon { get; set; }
        public string Tues { get; set; }
        public string Wed { get; set; }
        public string Thur { get; set; }
        public string Fri { get; set; }
        public string Sat { get; set; }
        public string Sun { get; set; }
        public string SleepOverHours { get; set; }
        public string Appears { get; set; }
        public string Hours { get; set; }
        public string TravelTime { get; set; }
        public string StandBy { get; set; }
        public string Bonus { get; set; }
        public string HolidayPayRequired { get; set; }
        public string NoOfNightsSubsistence { get; set; }
    }
}
