﻿using System;
using FMConway.Services.Tasks.Models;
using System.Linq.Expressions;
using System.ComponentModel;

namespace FMConway.Services.Reports.Models
{
    public class ActiveTasksModel
    {
        public Guid WorkInstructionID { get; set; }
        public int WorkInstructionNumber { get; set; }
        public int JobNumber { get; set; }
        public string CustomerName { get; set; }
        public string ContractManager { get; set; }
        public string ContractNumber { get; set; }
        public string JobTitle { get; set; }
        public DateTime StartDate { get; set; }
        public string IssuedBy { get; set; }
        public string Scheduled { get; set; }
        public Depot Depot { get; set; }
        public JobType TaskType { get; set; }
        public bool IsActive { get; set; }
        public Status Status { get; set; }
        public DateTime? InvoiceDate { get; set; }


        public static Expression<Func<WorkInstruction, ActiveTasksModel>> EntityToModel = entity => new ActiveTasksModel
        {
            WorkInstructionID = entity.WorkInstructionPK,
            StartDate = entity.WorkInstructionStartDate,
            Depot = entity.Depot,
            TaskType = entity.JobType,
            WorkInstructionNumber = entity.WorkInstructionNumber,
            JobNumber = entity.Job.JobNumber,
            CustomerName = entity.Job.Contract.Customer.CustomerName,
            ContractManager = entity.Job.Contract.Employee.EmployeeName,
            ContractNumber = entity.Job.Contract.ContractNumber,
            JobTitle = entity.Job.JobTitle,
            IssuedBy = entity.Employee.EmployeeName,
            Scheduled = entity.IsScheduled ? "Yes" : "No",
            IsActive = entity.IsActive,
            Status = entity.Status,
            InvoiceDate = entity.InvoiceDate
        };
    }
}