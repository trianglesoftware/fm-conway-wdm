﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class OnHireInstalledReportModel
    {
        public Guid WorkInstructionID { get; set; }

        public int Task { get; set; }
        public int Job { get; set; }
        public string JobTitle { get; set; }
        public DateTime InstallDate { get; set; }
        public string IssuedBy { get; set; }
        public string Equipment { get; set; }
        public string Type { get; set; }
        public string AssetNumbers { get; set; }
        public decimal Quantity { get; set; }
        public int? CollectionTask { get; set; }
        public string CollectionStatus { get; set; }
        public DateTime? CollectionDate { get; set; }
        public string Address { get; set; }
    }
}
