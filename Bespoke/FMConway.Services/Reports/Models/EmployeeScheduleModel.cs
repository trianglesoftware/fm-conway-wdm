﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class TimesheetInformationModel
    {
        public Guid EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeType { get; set; }
        public string EmployeePhoneNumber { get; set; }
        public int TasksToday { get; set; }
        public int TasksTomorrow { get; set; }
        public int TasksThisWeek { get; set; }
        public int TasksNextWeek { get; set; }
    }
}
