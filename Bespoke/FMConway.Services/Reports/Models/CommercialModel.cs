﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class CommercialModel
    {
        public Guid WorkInstructionID { get; set; }
        public string Depot { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public DateTime? FirstCone { get; set; }
        public DateTime? LastCone { get; set; }
        public string ShiftType { get; set; }
        public string Chargeable { get; set; }
        public string TMProjectCode { get; set; }
        public string Customer { get; set; }
        public string Authoriser { get; set; }
        public string CustProjectCode { get; set; }
        public string Address { get; set; }
        public ICollection<WorkInstructionPriceLine> PriceLines { get; set; }
        public string TMResource { get; set; }
        public decimal Quantity { get; set; }
        public decimal Total { get; set; }
        public decimal Rate { get; set; }
        public string TMActivities { get; set; }
        public string TaskDetails { get; set; }
        public Shift Shift { get; set; }
        public string VehicleReg { get; set; }
        public ICollection<LoadingSheet> LoadingSheets { get; set; }
        public string QuantityShift { get; set; }
        public decimal Cost { get; set; }
        public string Concatenate { get; set; }

        public Job Job { get; set; }

        public static Expression<Func<WorkInstruction, CommercialModel>> EntityToModel = entity => new CommercialModel
        {
            WorkInstructionID = entity.WorkInstructionPK,
            Depot = entity.Depot.DepotName,
            Date = entity.WorkInstructionStartDate,
            StartDate = entity.WorkInstructionStartDate,
            FinishDate = entity.WorkInstructionFinishDate,
            FirstCone = entity.FirstCone,
            LastCone = entity.LastCone,
            Chargeable = entity.IsChargeable ? "Y" : "N",
            TMProjectCode = entity.Job.Contract.ContractNumber,
            Customer = entity.Job.Contract.ContractTitle,
            Authoriser = entity.Employee.EmployeeName,
            CustProjectCode = entity.Job.CustomerProjectCode,
            Address = entity.AddressLine1 + (entity.Postcode != "" ?  ", " + entity.Postcode : ""),
            PriceLines = entity.WorkInstructionPriceLines,
            TMActivities = entity.TrafficManagementActivities,
            TaskDetails = entity.Comments,
            Shift = entity.JobPack.Shift,
            LoadingSheets = entity.LoadingSheets,
            Job = entity.Job
        };
        public static Expression<Func<WorkInstructionPriceLine, CommercialModel>> etm = entity => new CommercialModel
        {
            WorkInstructionID = entity.WorkInstruction.WorkInstructionPK,
            Depot = entity.WorkInstruction.Depot.DepotName,
            Date = entity.WorkInstruction.WorkInstructionStartDate,
            StartDate = entity.WorkInstruction.WorkInstructionStartDate,
            FinishDate = entity.WorkInstruction.WorkInstructionFinishDate,
            FirstCone = entity.WorkInstruction.FirstCone,
            LastCone = entity.WorkInstruction.LastCone,
            Chargeable = entity.WorkInstruction.IsChargeable ? "Y" : "N",
            TMProjectCode = entity.WorkInstruction.Job.Contract.ContractNumber,
            Customer = entity.WorkInstruction.Job.Contract.ContractTitle,
            Authoriser = entity.WorkInstruction.Employee.EmployeeName,
            CustProjectCode = entity.WorkInstruction.Job.CustomerProjectCode,
            Address = entity.WorkInstruction.AddressLine1 + (entity.WorkInstruction.Postcode != "" ? ", " + entity.WorkInstruction.Postcode : ""),
            TMResource = entity.PriceLine.Description,
            Quantity = entity.Quantity ?? 0,
            Rate = entity.Rate ?? 0,
            Total = (entity.Quantity ?? 0) * (entity.Rate ?? 0),
            PriceLines = entity.WorkInstruction.WorkInstructionPriceLines,
            TMActivities = entity.WorkInstruction.TrafficManagementActivities,
            TaskDetails = entity.WorkInstruction.Comments,
            Shift = entity.WorkInstruction.JobPack.Shift,
            LoadingSheets = entity.WorkInstruction.LoadingSheets,
            Job = entity.WorkInstruction.Job
        };
    }
}
