﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class JobSheetReportModel
    {
        public JobSheetReportModel()
        {
            Engineers = new List<JobSheetReportEngineer>();
            Vehicles = new List<JobSheetReportVehicle>();
            CleansingLogs = new List<JobSheetReportCleansingLog>();
            UnloadingProcesses = new List<JobSheetReportUnloadingProcess>();
            Checklists = new List<JobSheetReportChecklist>();
            Images = new List<JobSheetReportImage>();
        }

        public Guid WorkInstructionID { get; set; }
        public int WorkInstructionNumber { get; set; }
        public string CustomerName { get; set; }

        public string WorkOrderNo { get; set; }
        public string ClientRefNumber { get; set; }
        public string VisitDuration { get; set; }
        public string ActivityType { get; set; }
        public string WorkStatus { get; set; }
        public DateTime? DateUploaded { get; set; }

        public string WorkLocation { get; set; }
        public string WorkUndertaken { get; set; }
        public decimal? LocationLongitude { get; set; }
        public decimal? LocationLatitude { get; set; }

        public DateTime? LeaveDepot { get; set; }
        public DateTime? ArrivalAtSite { get; set; }
        public DateTime? LeaveSite { get; set; }
        public DateTime? ReturnToDepot { get; set; }
        public DateTime GeneratedDate { get; set; }

        public string FooterText { get; set; }

        public List<JobSheetReportEngineer> Engineers { get; set; }
        public List<JobSheetReportVehicle> Vehicles { get; set; }

        public List<JobSheetReportCleansingLog> CleansingLogs { get; set; }
        public List<JobSheetReportUnloadingProcess> UnloadingProcesses { get; set; }
        public List<JobSheetReportChecklist> Checklists { get; set; }
        public List<JobSheetReportActivityRiskAssessments> ActivityRiskAssessments { get; set; }
        public List<JobSheetReportImage> Images { get; set; }

        public static Expression<Func<WorkInstruction, JobSheetReportModel>> EntityToModel = a => new JobSheetReportModel
        {
            WorkInstructionID = a.WorkInstructionPK,
            WorkInstructionNumber = a.WorkInstructionNumber,
            CustomerName = a.Job.Contract.Customer.CustomerName,

            WorkOrderNo = null,
            ClientRefNumber = a.ClientReferenceNumber,
            VisitDuration = null,
            ActivityType = a.JobType.DisplayName,
            WorkStatus = a.Status.Status1,
            DateUploaded = a.JobPack.Shift.ShiftProgressInfoes.Where(b => b.ShiftProgress.Progress == "Completed").Select(b => (DateTime?)b.Shift.UpdatedDate).FirstOrDefault(),

            WorkLocation = StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK),
            WorkUndertaken = a.Tasks.Where(b => b.Name == "Loading Process").Select(b => b.Notes).FirstOrDefault(),
            LocationLongitude = a.Tasks.Where(b => b.Name == "Arrive on Site").Select(b => b.Longitude).FirstOrDefault(),
            LocationLatitude = a.Tasks.Where(b => b.Name == "Arrive on Site").Select(b => b.Latitude).FirstOrDefault(),

            LeaveDepot = a.Tasks.Where(b => b.Name == "Start Travel").Select(b => b.EndTime).FirstOrDefault(),
            ArrivalAtSite = a.Tasks.Where(b => b.Name == "Arrive on Site").Select(b => b.EndTime).FirstOrDefault(),
            LeaveSite = a.Tasks.Where(b => b.Name == "Leave Site").Select(b => b.EndTime).FirstOrDefault(),
            ReturnToDepot = a.Tasks.Where(b => b.Name == "End Travel").Select(b => b.EndTime).FirstOrDefault(),
            GeneratedDate = DateTime.Now,
        };
    }

    public class JobSheetReportEngineer
    {
        public Guid ShiftStaffID { get; set; }
        public Guid EmployeeID { get; set; }
        public string EmployeeName { get; set; }

        public static Expression<Func<ShiftStaff, JobSheetReportEngineer>> EntityToModel = a => new JobSheetReportEngineer
        {
            ShiftStaffID = a.ShiftStaffPK,
            EmployeeID = a.EmployeeFK,
            EmployeeName = a.Employee.EmployeeName
        };
    }

    public class JobSheetReportVehicle
    {
        public Guid ShiftVehicleID { get; set; }
        public Guid VehicleID { get; set; }
        public string Registration { get; set; }
        public Guid? EmployeeID { get; set; }

        public static Expression<Func<ShiftVehicle, JobSheetReportVehicle>> EntityToModel = a => new JobSheetReportVehicle
        {
            ShiftVehicleID = a.ShiftVehiclePK,
            VehicleID = a.VehicleFK,
            EmployeeID = a.EmployeeFK,
            Registration = a.Vehicle.Registration
        };
    }

    public class JobSheetReportCleansingLog
    {
        public Guid CleansingLogID { get; set; }
        public string Road { get; set; }
        public string Section { get; set; }
        public string LaneLookup { get; set; }
        public string DirectionLookup { get; set; }
        public string StartMarker { get; set; }
        public string EndMarker { get; set; }
        public decimal? GulliesCleaned { get; set; }
        public decimal? GulliesMissed { get; set; }
        public decimal? CatchpitsCleaned { get; set; }
        public decimal? CatchpitsMissed { get; set; }
        public decimal? Channels { get; set; }
        public decimal? SlotDrains { get; set; }
        public decimal? Defects { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedDate { get; set; }

        public static Expression<Func<CleansingLog, JobSheetReportCleansingLog>> EntityToModel = a => new JobSheetReportCleansingLog
        {
            CleansingLogID = a.CleansingLogPK,
            Road = a.Road,
            Section = a.Section,
            LaneLookup = a.Lane.Name,
            DirectionLookup = a.Direction.Name,
            StartMarker = a.StartMarker,
            EndMarker = a.EndMarker,
            GulliesCleaned = a.GulliesCleaned,
            GulliesMissed = a.GulliesMissed,
            CatchpitsCleaned = a.CatchpitsCleaned,
            CatchpitsMissed = a.CatchpitsMissed,
            Channels = a.Channels,
            SlotDrains = a.SlotDrains,
            Defects = a.Defects,
            Comments = a.Comments,
            CreatedDate = a.CreatedDate,
        };
    }

    public class JobSheetReportUnloadingProcess
    {
        public Guid UnloadingProcessPK { get; set; }
        public DateTime? DepartToDisposalSiteDate { get; set; }
        public DateTime? ArriveAtDisposalSiteDate { get; set; }
        public Guid? DisposalSiteFK { get; set; }
        public string DisposalSiteLookup { get; set; }
        public string WasteTicketNumber { get; set; }
        public decimal? VolumeOfWasteDisposed { get; set; }
        public string Comments { get; set; }
        public DateTime? LeaveDisposalSiteDate { get; set; }

        public static Expression<Func<UnloadingProcess, JobSheetReportUnloadingProcess>> EntityToModel = a => new JobSheetReportUnloadingProcess
        {
            UnloadingProcessPK = a.UnloadingProcessPK,
            DepartToDisposalSiteDate = a.DepartToDisposalSiteDate,
            ArriveAtDisposalSiteDate = a.ArriveAtDisposalSiteDate,
            DisposalSiteFK = a.DisposalSiteFK,
            DisposalSiteLookup = a.DisposalSite.Name,
            WasteTicketNumber = a.WasteTicketNumber,
            VolumeOfWasteDisposed = a.VolumeOfWasteDisposed,
            Comments = a.Comments,
            LeaveDisposalSiteDate = a.LeaveDisposalSiteDate,
        };
    }

    public class JobSheetReportChecklist
    {
        public JobSheetReportChecklist()
        {
            Answers = new List<JobSheetReportChecklistAnswer>();
        }

        public DateTime? StartTime { get; set; }
        public string AssessmentType { get; set; }
        public string Engineer { get; set; }
        public List<JobSheetReportChecklistAnswer> Answers { get; set; }

        public static Expression<Func<TaskChecklist, JobSheetReportChecklist>> EntityToModel = a => new JobSheetReportChecklist
        {
            StartTime = a.Task.EndTime,
            AssessmentType = a.Task.Name,
            Engineer = StoreFunctions.UserEmployeeName(a.CreatedByID),
            Answers = a.TaskChecklistAnswers.Where(b => b.IsActive).OrderBy(b => b.ChecklistQuestion.QuestionOrder).Select(b => new JobSheetReportChecklistAnswer
            {
                Question = b.ChecklistQuestion.ChecklistQuestion1,
                Answer = b.Answer,
                Reason = b.Reason,
                FileIDs = b.TaskChecklistAnswerPhotos.Where(c => c.IsActive).Select(c => c.FileFK).ToList()
            }).ToList()
        };
    }

    public class JobSheetReportChecklistAnswer
    {
        public JobSheetReportChecklistAnswer()
        {
            FileIDs = new List<Guid>();
            ImageNumbers = new List<int>();
        }

        public string Question { get; set; }
        public int Answer { get; set; }
        public string Reason { get; set; }
        public List<Guid> FileIDs { get; set; }
        public List<int> ImageNumbers { get; set; }
    }

    public class JobSheetReportImage
    {
        public Guid FileID { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
        public DateTime? Captured { get; set; }
        public DateTime? Uploaded { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public string Comments { get; set; }
        public string TaskType { get; set; }
        public string RoadName { get; set; }

        public static Expression<Func<TaskChecklistAnswerPhoto, JobSheetReportImage>> TaskChecklistAnswerPhotoToModel = a => new JobSheetReportImage
        {
            FileID = a.FileFK,
            Name = a.File.FileDownloadName,
            Data = a.File.Data,
            Captured = a.File.CreatedDate,
            Uploaded = a.File.UpdatedDate,
            Longitude = a.File.Longitude,
            Latitude = a.File.Latitude,
            Comments = a.File.Comments,
            TaskType = a.TaskChecklistAnswer.TaskChecklist.Task.Name,
            RoadName = "",
        };

        public static Expression<Func<TaskPhoto, JobSheetReportImage>> TaskPhotoToModel = a => new JobSheetReportImage
        {
            FileID = a.FileFK,
            Name = a.File.FileDownloadName,
            Data = a.File.Data,
            Captured = a.File.CreatedDate,
            Uploaded = a.File.UpdatedDate,
            Longitude = a.File.Longitude,
            Latitude = a.File.Latitude,
            Comments = a.File.Comments,
            TaskType = a.Task.Name,
            RoadName = "",
        };

        public static Expression<Func<CleansingLogPhoto, JobSheetReportImage>> CleansingLogPhotoToModel = a => new JobSheetReportImage
        {
            FileID = a.FileFK,
            Name = a.File.FileDownloadName,
            Data = a.File.Data,
            Captured = a.File.CreatedDate,
            Uploaded = a.File.UpdatedDate,
            Longitude = a.File.Longitude,
            Latitude = a.File.Latitude,
            Comments = a.File.Comments,
            TaskType = a.CleansingLog.Task.Name,
            RoadName = a.CleansingLog.Road,
        };

        public static Expression<Func<UnloadingProcessPhoto, JobSheetReportImage>> UnloadingProcessPhotoToModel = a => new JobSheetReportImage
        {
            FileID = a.FileFK,
            Name = a.File.FileDownloadName,
            Data = a.File.Data,
            Captured = a.File.CreatedDate,
            Uploaded = a.File.UpdatedDate,
            Longitude = a.File.Longitude,
            Latitude = a.File.Latitude,
            Comments = a.File.Comments,
            TaskType = a.UnloadingProcess.Task.Name,
            RoadName = "",
        };
    }

    public class JobSheetReportActivityRiskAssessments
    {
        public Guid ActivityRiskAssessmentID { get; set; }
        public string RoadName { get; set; }
        public string RoadSpeed { get; set; }
        public string TMRequirement { get; set; }
        public bool Schools { get; set; }
        public bool PedestrianCrossings { get; set; }
        public bool TreeCanopies { get; set; }
        public bool WaterCourses { get; set; }
        public bool OverheadCables { get; set; }
        public bool AdverseWeather { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedDate { get; set; }

        public static Expression<Func<ActivityRiskAssessment, JobSheetReportActivityRiskAssessments>> EntityToModel = a => new JobSheetReportActivityRiskAssessments
        {
            ActivityRiskAssessmentID = a.ActivityRiskAssessmentPK,
            RoadName = a.RoadName,
            RoadSpeed = a.RoadSpeed.Speed,
            TMRequirement = a.TMRequirement.Name,
            Schools = a.Schools,
            PedestrianCrossings = a.PedestrianCrossings,
            TreeCanopies = a.TreeCanopies,
            WaterCourses = a.WaterCourses,
            OverheadCables = a.OverheadCables,
            AdverseWeather = a.AdverseWeather,
            Notes = a.Notes,
            CreatedDate = a.CreatedDate,
        };
    }
}
