﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class VehicleAbsenceReportModel
    {
        public Guid VehicleAbsenceID { get; set; }
        public Guid VehicleID { get; set; }

        public string Make { get; set; }
        public string Model { get; set; }
        public string Registration { get; set; }
        public string Reason { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public static Expression<Func<VehicleAbsence, VehicleAbsenceReportModel>> EntityToModel = a => new VehicleAbsenceReportModel
        {
            VehicleAbsenceID = a.VehicleAbsencePK,
            VehicleID = a.VehicleFK,

            Make = a.Vehicle.Make,
            Model = a.Vehicle.Model,
            Registration = a.Vehicle.Registration,
            Reason = a.VehicleAbsenceReason.Code,
            From = a.FromDate,
            To = a.ToDate
        };
    }
}
