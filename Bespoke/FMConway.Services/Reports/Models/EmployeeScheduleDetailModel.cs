﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Reports.Models
{
    public class TimesheetInformationDetailModel
    {
        public DateTime Day { get; set; }
        public TimesheetInformationBreakdownModel Schedule { get; set; }
    }

    public class TimesheetInformationBreakdownModel
    {
        public Guid WorkInstructionID { get; set; }

        public DateTime ScheduledDate { get; set; }
        public int Task { get; set; }
        public string Customer { get; set; }
        public string ContractNumber { get; set; }
        public string ContractManager { get; set; }
        public string JobTitle { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public string JobType { get; set; }
        public string Speed { get; set; }
        public DateTime? OnSite { get; set; }
        public DateTime? OffSite { get; set; }
        public int TravelTime { get; set; }
        public string Address { get; set; }
    }
}
