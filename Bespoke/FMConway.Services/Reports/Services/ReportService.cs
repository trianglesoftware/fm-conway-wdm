﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Employees.Extensions;
using FMConway.Services.ErrorLogging.Services;
using FMConway.Services.Extensions;
using FMConway.Services.Pdf.Extensions;
using FMConway.Services.Reports.Helpers;
using FMConway.Services.Reports.Models;
using FMConway.Services.Tasks.Models;
using FMConway.Services.WorkInstructions.Models;
using FMConway.Services.WorkInstructionDetails.Models;
using FMConway.Services.WorkInstructions.Enums;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Triangle.Membership.Users;
using System.Web.Hosting;
using Humanizer;
using System.Web.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.Text;
using Humanizer.Localisation;
using FMConway.Services.Customers.Extensions;
using System.ServiceModel.Description;

namespace FMConway.Services.Reports.Services
{
    public class ReportService : Service, FMConway.Services.Reports.Interfaces.IReportService
    {
        UserContext _userContext;
        ErrorLoggingService _errorService;
        private int _pageWidth = 530;

        public ReportService(UserContext userContext, ErrorLoggingService errorService)
        {
            _userContext = userContext;
            _errorService = errorService;
        }

        #region Price Lines
        public PagedResults<JobPriceLineReportModel> GetPagedActiveJobPriceLines(int rowCount, int page, string query)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            bool boolQuery = string.Compare(query, "Yes", true) == 0;
            bool isBoolQuery = new List<string> { "Yes", "No" }.Contains(query, StringComparer.OrdinalIgnoreCase);
            decimal decimalQuery;
            bool isDecimalQuery = decimal.TryParse(query, out decimalQuery);

            // short cricut where clause if isBoolQuery to stop mainly the notes from interfering with results

            IQueryable<JobPriceLineReportModel> baseQuery =
                from j in Context.Jobs
                from jpl in Context.JobPriceLines.Where(a => j.JobPK == a.JobFK).DefaultIfEmpty()
                where
                    (query == null || query == "" ? true : false) ||
                    (isBoolQuery ? (jpl != null) == boolQuery : (
                        (isIntQuery ? j.JobNumber == intQuery : false) ||
                        (isIntQuery ? jpl.WorkInstructionPriceLine.WorkInstruction.WorkInstructionNumber == intQuery : false) ||
                        jpl.PriceLine.Code.Contains(query) ||
                        jpl.PriceLine.Description.Contains(query) ||
                        (isDecimalQuery ? jpl.Rate == decimalQuery : false) ||
                        jpl.Note.Contains(query)
                    ))
                orderby j.JobNumber, jpl.WorkInstructionPriceLine.WorkInstruction.WorkInstructionNumber, jpl.PriceLine.Code
                select new JobPriceLineReportModel
                {
                    JobID = j.JobPK,
                    JobPriceLineID = jpl.JobPriceLinePK,

                    JobNumber = j.JobNumber,
                    JobTitle = j.JobTitle,
                    HasPriceLines = jpl != null ? "Yes" : "No",
                    WorkInstructionNumber = jpl.WorkInstructionPriceLine.WorkInstruction.WorkInstructionNumber,
                    Customer = j.Contract.Customer.CustomerName,
                    ContractNumber = j.Contract.ContractNumber,
                    CustomerCode = j.Contract.Customer.CustomerCode,
                    CustomerProjectCode = j.CustomerProjectCode,
                    Code = jpl.PriceLine.Code,
                    Description = jpl.PriceLine.Description,
                    Rate = jpl.Rate,
                    Note = jpl.Note
                };

            return PagedResults<JobPriceLineReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionPriceLineReportModel> GetPagedActiveWorkInstructionPriceLines(int rowCount, int page, string query)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            bool boolQuery = string.Compare(query, "Yes", true) == 0;
            bool isBoolQuery = new List<string> { "Yes", "No" }.Contains(query, StringComparer.OrdinalIgnoreCase);
            decimal decimalQuery;
            bool isDecimalQuery = decimal.TryParse(query, out decimalQuery);

            // short cricut where clause if isBoolQuery to stop mainly the notes from interfering with results

            IQueryable<WorkInstructionPriceLineReportModel> baseQuery =
                from wi in Context.WorkInstructions
                from wipl in Context.WorkInstructionPriceLines.Where(a => wi.WorkInstructionPK == a.WorkInstructionFK).DefaultIfEmpty()
                where wi.JobFK != null
                where
                    (query == null || query == "" ? true : false) ||
                    (isBoolQuery ? (wipl != null) == boolQuery : (
                        (isIntQuery ? wi.Job.JobNumber == intQuery : false) ||
                        (isIntQuery ? wipl.WorkInstruction.WorkInstructionNumber == intQuery : false) ||
                        wipl.PriceLine.Code.Contains(query) ||
                        wipl.PriceLine.Description.Contains(query) ||
                        (isDecimalQuery ? wipl.Rate == decimalQuery : false) ||
                        wipl.Note.Contains(query)
                    ))
                orderby wi.Job.JobNumber, wi.WorkInstructionNumber, wipl.PriceLine.Code
                select new WorkInstructionPriceLineReportModel
                {
                    WorkInstructionID = wi.WorkInstructionPK,
                    WorkInstructionPriceLineID = wipl.WorkInstructionPriceLinePK,

                    JobNumber = wi.Job.JobNumber,
                    JobTitle = wi.Job.JobTitle,
                    HasPriceLines = wipl != null ? "Yes" : "No",
                    WorkInstructionNumber = wi.WorkInstructionNumber,
                    Customer = wi.Job.Contract.Customer.CustomerName,
                    ContractNumber = wi.Job.Contract.ContractNumber,
                    CustomerCode = wi.Job.Contract.Customer.CustomerCode,
                    Code = wipl.PriceLine.Code,
                    Description = wipl.PriceLine.Description,
                    Rate = wipl.Rate,
                    Note = wipl.Note
                };

            return PagedResults<WorkInstructionPriceLineReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region Employee Absences
        public PagedResults<EmployeeAbsenceReportModel> GetPagedActiveEmployeeAbsences(int rowCount, int page, string query)
        {
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);

            IQueryable<EmployeeAbsenceReportModel> baseQuery = Context.EmployeeAbsences
                .Where(a => a.IsActive)
                .Where(a =>
                    a.Employee.EmployeeName.Contains(query) ||
                    a.EmployeeAbsenceReason.Code.Contains(query) ||
                    (isDateQuery ? a.FromDate == dateQuery : false) ||
                    (isDateQuery ? a.ToDate == dateQuery : false)
                    )
                .OrderByDescending(a => a.FromDate).ThenBy(a => a.ToDate)
                .Select(EmployeeAbsenceReportModel.EntityToModel);

            return PagedResults<EmployeeAbsenceReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region Vehicle Absences
        public PagedResults<VehicleAbsenceReportModel> GetPagedActiveVehicleAbsences(int rowCount, int page, string query)
        {
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);

            IQueryable<VehicleAbsenceReportModel> baseQuery = Context.VehicleAbsences
                .Where(a => a.IsActive)
                .Where(a =>
                    a.Vehicle.Make.Contains(query) ||
                    a.Vehicle.Model.Contains(query) ||
                    a.Vehicle.Registration.Contains(query) ||
                    a.VehicleAbsenceReason.Code.Contains(query) ||
                    (isDateQuery ? a.FromDate == dateQuery : false) ||
                    (isDateQuery ? a.ToDate == dateQuery : false)
                    )
                .OrderByDescending(a => a.FromDate).ThenBy(a => a.ToDate)
                .Select(VehicleAbsenceReportModel.EntityToModel);

            return PagedResults<VehicleAbsenceReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region Qualification Expiry
        public PagedResults<QualificationExpiryReportModel> GetPagedActiveQualificationExpiry(int rowCount, int page, string query)
        {
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);

            IQueryable<QualificationExpiryReportModel> baseQuery = Context.EmployeeCertificates
                .Where(a => a.IsActive)
                .Where(a =>
                    a.Employee.EmployeeName.Contains(query) ||
                    a.Name.Contains(query) ||
                    a.CertificateType.Name.Contains(query) ||
                    a.Area.Area1.Contains(query) ||
                    a.Description.Contains(query) ||
                    (isDateQuery ? a.ExpiryDate == dateQuery : false) ||
                    (query == "Yes" && a.CertificateDocuments.Any(b => b.IsActive && b.File != null))
                    )
                .OrderByDescending(a => a.ExpiryDate).ThenBy(a => a.Employee.EmployeeName)
                .Select(QualificationExpiryReportModel.EntityToModel);

            return PagedResults<QualificationExpiryReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region Work Instruction Report

        [Obsolete]
        public byte[] GenerateWorkInstructionReport(Guid workInstructionID, string imagesPath, out string filename)
        {
            try
            {
                WorkInstruction wi = Context.WorkInstructions.Where(w => w.WorkInstructionPK == workInstructionID).SingleOrDefault();

                string title = wi.Job.SchemeFK.HasValue ? wi.Job.Scheme.SchemeTitle : wi.Job.Contract.ContractTitle;
                filename = "TaskReport_" + title + "_" + wi.WorkInstructionNumber.ToString() + ".pdf";

                Document doc = new Document();

                doc.Info.Title = "Task " + wi.WorkInstructionNumber.ToString();

                Section section = doc.AddSection();

                TextFrame thead = section.Headers.Primary.AddTextFrame();
                thead.MarginBottom = 500;
                Paragraph pHead = thead.AddParagraph();

                float tableWidth = 530;
                Table headerTable = thead.AddTable();
                Border tableBorder = new Border { Style = BorderStyle.Single, Color = Color.FromCmyk(0, 0, 0, 50) };
                headerTable.Borders = new Borders { Top = tableBorder, Bottom = tableBorder.Clone(), Left = tableBorder.Clone(), Right = tableBorder.Clone() };

                Column logoColumn = headerTable.AddColumn();
                logoColumn.Width = tableWidth / 5;
                Column middelColumn = headerTable.AddColumn();
                middelColumn.Width = tableWidth / 1.685;
                Column detailColumn = headerTable.AddColumn();
                detailColumn.Width = tableWidth / 5;

                Row headerRow = headerTable.AddRow();
                Row row2 = headerTable.AddRow();
                Row row3 = headerTable.AddRow();

                headerRow.HeadingFormat = true;
                headerRow.Format.Alignment = ParagraphAlignment.Center;

                string headerImagePath = Path.Combine(imagesPath, "fmconway_logo.png");

                Image logo = headerRow.Cells[0].AddImage(headerImagePath);
                headerRow.Cells[0].MergeDown = 2;
                logo.Width = 100;

                Paragraph tmPara = new Paragraph();
                tmPara.Format.Font.Size = 12;
                tmPara.AddFormattedText("Traffic Management Record", TextFormat.Bold);
                headerRow.Cells[1].Add(tmPara);
                Paragraph addressPara = new Paragraph();
                addressPara.Format.Font.Size = 8;
                addressPara.AddFormattedText(wi.AddressLine1 + " " + wi.AddressLine2 + " " + wi.AddressLine3 + " " + wi.City + " " + wi.Postcode);
                headerRow.Cells[1].Add(addressPara);
                headerRow.Cells[1].MergeDown = 2;
                headerRow.Cells[1].VerticalAlignment = VerticalAlignment.Center;

                Paragraph pages = new Paragraph();
                pages.Format.Font.Size = 8;
                pages.AddFormattedText("Page ");
                pages.AddPageField();
                pages.AddFormattedText(" of ");
                pages.AddNumPagesField();
                row3.Format.Alignment = ParagraphAlignment.Center;
                row3.VerticalAlignment = VerticalAlignment.Center;
                row3.Cells[2].Add(pages);

                section.PageSetup.TopMargin = MigraDoc.DocumentObjectModel.Unit.FromCentimeter(3.5);
                section.PageSetup.LeftMargin = MigraDoc.DocumentObjectModel.Unit.FromCentimeter(1);
                section.PageSetup.HeaderDistance = 20;

                //Paragraph p = section.Footers.Primary.AddParagraph("Task " + wi.WorkInstructionNumber.ToString() + " " + DateTime.Now.ToString("dd MMM yyyy"));
                //p.Format.Font.Size = 5;

                //string headerImagePath = Path.Combine(imagesPath, "fmconway_logo.png");
                //Image i = section.AddImage(headerImagePath);
                //i.Width = 100;
                //i.Left = ShapePosition.Center;

                WorkInstructionInfo(section, wi);

                //HorizontalLine(section);
                section.AddLineSpacing(20);

                MaintenanceChecks(section, workInstructionID);
                VehicleChecklists(section, wi, true);
                VehicleChecklists(section, wi, false);

                Tasks(section, wi);
                TaskDetails(section, wi);

                #region RenderDoc
                MigraDoc.Rendering.PdfDocumentRenderer docRenderer = new PdfDocumentRenderer(true);
                docRenderer.Document = doc;
                docRenderer.RenderDocument();

                MemoryStream ms = new MemoryStream();
                docRenderer.PdfDocument.Save(ms, true);

                #endregion

                return ms.ToArray();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateReport", e, workInstructionID.ToString());
                filename = "";
                return new byte[0];
            }
        }

        public void WorkInstructionInfo(Section section, WorkInstruction wi)
        {
            var weatherCondition = "";
            var weatherTask = wi.Tasks.Where(t => t.TaskTypeFK == 11).Single();

            if (weatherTask != null)
            {
                weatherCondition = weatherTask.WeatherCondition.Name;
            }

            TextFrame pageTitle = section.AddTextFrame();
            pageTitle.Height = 35;
            pageTitle.MarginTop = 10;
            pageTitle.MarginBottom = 20;
            pageTitle.Width = 530;

            //Paragraph p = pageTitle.AddParagraph();
            //p.Format.Font.Bold = true;
            //p.Format.Font.Size = 12;
            //p.Format.SpaceAfter = 4;
            //p.AddText("Customer: " + wi.Job.Contract.Customer.CustomerName);

            //p = pageTitle.AddParagraph();
            //p.Format.Font.Bold = true;
            //p.Format.Font.Size = 12;
            //p.Format.SpaceAfter = 1;
            //p.AddText("Job Title: " + wi.Job.JobTitle);

            int columnWidth = _pageWidth / 4;// 3;

            Table table = section.AddTable();
            table.Format.Font.Size = 7;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.TopPadding = 5;
            table.BottomPadding = 5;

            table.AddColumn(columnWidth);
            table.AddColumn(columnWidth);
            table.AddColumn(columnWidth);
            table.AddColumn(columnWidth);

            Border tableBorder = new Border { Style = BorderStyle.Single, Color = Color.FromCmyk(0, 0, 0, 50) };
            table.Borders = new Borders { Top = tableBorder, Bottom = tableBorder.Clone(), Left = tableBorder.Clone(), Right = tableBorder.Clone() };

            Row row = table.AddRow();
            row.Cells[0].AddParagraph().AddFormattedText("Customer:", TextFormat.Bold);
            row.Cells[0].Shading.Color = Colors.LightGray;
            row.Cells[1].AddParagraph().AddFormattedText(wi.Job.Contract.Customer.CustomerName ?? "");
            row.Cells[2].AddParagraph().AddFormattedText("Date of Closure:", TextFormat.Bold);
            row.Cells[2].Shading.Color = Colors.LightGray;
            row.Cells[3].AddParagraph().AddFormattedText(wi.Job.EndDate.Value.ToShortDateString());
            row = table.AddRow();
            row.Cells[0].AddParagraph().AddFormattedText("FMC Project Code:", TextFormat.Bold);
            row.Cells[0].Shading.Color = Colors.LightGray;
            row.Cells[1].AddParagraph().AddFormattedText(wi.Job.Contract.ContractNumber);
            row.Cells[2].AddParagraph().AddFormattedText("Customer Project Code", TextFormat.Bold);
            row.Cells[2].Shading.Color = Colors.LightGray;
            row.Cells[3].AddParagraph().AddFormattedText(wi.Job.CustomerProjectCode ?? "");
            row = table.AddRow();
            row.Cells[0].AddParagraph().AddFormattedText("Permit / NOMS Number:", TextFormat.Bold);
            row.Cells[0].Shading.Color = Colors.LightGray;
            row.Cells[1].AddParagraph().AddFormattedText(wi.SRWNumber);
            row.Cells[2].AddParagraph().AddFormattedText("Weather:", TextFormat.Bold);
            row.Cells[2].Shading.Color = Colors.LightGray;
            row.Cells[3].AddParagraph().AddFormattedText(weatherCondition);
            //row = table.AddRow();
            //row.Cells[0].AddParagraph().AddFormattedText("Motorway/Road Number:", TextFormat.Bold);
            //row.Cells[0].Shading.Color = Colors.LightGray;
            //row.Cells[1].AddParagraph().AddFormattedText();
            //row.Cells[2].AddParagraph().AddFormattedText("Direction/Carriageway:", TextFormat.Bold);
            //row.Cells[2].Shading.Color = Colors.LightGray;
            //row.Cells[3].AddParagraph().AddFormattedText();
            row = table.AddRow();
            row.Cells[0].AddParagraph().AddFormattedText("TM Layout:", TextFormat.Bold);
            row.Cells[0].Shading.Color = Colors.LightGray;
            row.Cells[1].AddParagraph().AddFormattedText(wi.TrafficManagementActivities ?? "");
            row.Cells[1].MergeRight = 2;
            //row.Cells[2].AddParagraph().AddFormattedText("Lane/s Closed:", TextFormat.Bold);
            //row.Cells[2].Shading.Color = Colors.LightGray;
            //row.Cells[3].AddParagraph().AddFormattedText();
            row = table.AddRow();
            row.Cells[0].AddParagraph().AddFormattedText("Description of Works:", TextFormat.Bold);
            row.Cells[0].Shading.Color = Colors.LightGray;
            row.Cells[1].AddParagraph().AddFormattedText(wi.Job.Description);
            row.Cells[1].MergeRight = 2;


            //table.AddColumn(columnWidth);
            //table.AddColumn(columnWidth);
            //table.AddColumn(columnWidth);

            //Row r1 = table.AddRow();

            //r1.Cells[0].AddParagraph().AddFormattedText("Task Number: ", TextFormat.Bold).AddText(wi.WorkInstructionNumber.ToString());
            //r1.Cells[1].AddParagraph().AddFormattedText("Location: ", TextFormat.Bold).AddText((wi.Job.AddressLine1 ?? "") + " " + (wi.Job.Postcode ?? ""));
            //r1.Cells[2].AddParagraph().AddFormattedText("Job Type: ", TextFormat.Bold).AddText(wi.JobType.JobType1);
        }

        public void TaskEquipment(Section section, Guid taskID)
        {
            List<EquipmentModel> equipment = Context.TaskEquipments.Where(t => t.TaskFK.Equals(taskID) && t.IsActive).Select(EquipmentModel.ETM).ToList();
            Table table = section.AddTable();

            table.Format.Font.Size = 7;
            table.Rows.Height = 10;
            table.Rows.HeightRule = RowHeightRule.Exactly;

            table.AddColumn(250);
            table.AddColumn(125);
            table.AddColumn(125);

            Row head = table.AddRow();
            head.Height = 15;
            head.Format.Font.Bold = true;

            head.Cells[0].AddParagraph("Equipment Name");
            head.Cells[1].AddParagraph("Quantity");
            head.Cells[2].AddParagraph("Asset");

            foreach (EquipmentModel e in equipment)
            {
                Row r = table.AddRow();
                r.Height = 15;

                r.Cells[0].AddParagraph(e.Equipment);
                r.Cells[1].AddParagraph(e.QuantityInstalled.ToString());
                r.Cells[2].AddParagraph(e.VmsOrAsset ?? "");
            }
        }

        public void TaskTitle(Section section, string taskName)
        {
            section.AddTextFrame().Height = 10;

            Paragraph pa = section.AddParagraph();
            pa.Format.Font.Bold = true;
            pa.Format.Font.Size = 10;
            pa.AddText(taskName + " Task");
        }

        public void HorizontalLine(Section section, double specifiedHeight)
        {
            double height = specifiedHeight;
            Color hrFillColor = Color.FromCmyk(0, 0, 0, 50);
            Color hrBorderColor = Color.FromCmyk(0, 0, 0, 50);

            Paragraph p = new Paragraph();
            Border newBorder = new Border { Style = BorderStyle.Single, Color = hrBorderColor };

            p.Format = new ParagraphFormat
            {
                Font = new Font("Courier New", new Unit(height)),
                Shading = new Shading { Visible = true, Color = hrFillColor },
                Borders = new Borders
                {
                    Bottom = newBorder,
                    Left = newBorder.Clone(),
                    Right = newBorder.Clone(),
                    Top = newBorder.Clone()
                }
            };

            section.Add(p);
        }

        public void HorizontalLine(Section section)
        {
            HorizontalLine(section, (1.0));
        }

        public void MaintenanceChecks(Section section, Guid workInstructionID)
        {
            var checks = Context.MaintenanceChecks.Where(x => x.WorksInstructionFK == workInstructionID && x.IsActive).ToList();

            if (checks.Count() > 0)
            {
                Paragraph par = section.AddParagraph("Maintenance Checks");
                par.Format.SpaceBefore = 10;
                par.Format.Font.Size = 12;
                par.Format.Font.Bold = true;

                section.AddLineSpacing(10);

                Table table = section.AddTable();
                table.Rows.VerticalAlignment = VerticalAlignment.Center;
                Border tableBorder = new Border { Style = BorderStyle.Single, Color = Color.FromCmyk(0, 0, 0, 50) };
                table.Borders = new Borders { Top = tableBorder, Bottom = tableBorder.Clone(), Left = tableBorder.Clone(), Right = tableBorder.Clone() };
                table.Format.Font.Size = 7;
                table.Rows.Height = 10;
                table.Rows.HeightRule = RowHeightRule.Exactly;

                table.AddColumn(50);
                table.AddColumn(240);
                table.AddColumn(50);
                table.AddColumn(140);
                table.AddColumn(50);

                Row row = table.AddRow();
                row.Height = 15;
                row.Format.Font.Bold = true;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Shading.Color = Colors.LightGray;
                row.Cells[0].AddParagraph("Question No.");
                row.Cells[1].AddParagraph("Question");
                row.Cells[2].AddParagraph("Answer");
                row.Cells[3].AddParagraph("Details");
                row.Cells[4].AddParagraph("Photo");

                foreach (var check in checks.OrderBy(o => o.CreatedDate))
                {
                    row = table.AddRow();
                    row.Format.Font.Bold = true;
                    row.Height = 15;

                    row.Cells[0].AddParagraph("Completed On: " + check.Time.ToString());
                    row.Cells[0].MergeRight = 4;

                    foreach (var answer in check.MaintenanceCheckAnswers.OrderBy(o => o.ChecklistQuestion.QuestionOrder))
                    {
                        row = table.AddRow();
                        row.Height = 15;

                        row.Cells[0].AddParagraph(answer.ChecklistQuestion.QuestionOrder.ToString());
                        row.Cells[1].AddParagraph(answer.ChecklistQuestion.ChecklistQuestion1 ?? "");
                        row.Cells[2].AddParagraph(answer.Answer == false ? "No" : answer.Answer == true ? "Yes" : "N/A");
                        row.Cells[4].AddParagraph(answer.MaintenanceCheckDetails.SelectMany(s => s.MaintenanceCheckDetailPhotos).Any() ? "Yes" : "No");

                        if (answer.MaintenanceCheckDetails.Any())
                        {
                            String detailText = "";

                            foreach (var detail in answer.MaintenanceCheckDetails)
                            {
                                detailText += " " + detail.Description ?? "";
                            }
                            row.Cells[3].AddParagraph(detailText);
                        }
                    }

                }
                #region MaintenancePhotos

                //           if (checks.SelectMany(s => s.MaintenanceCheckAnswers).SelectMany(s => s.MaintenanceCheckDetails).Select(s => s.MaintenanceCheckDetailPhotos).Any())
                //           {

                //par = section.AddParagraph("Maintenance Check Photos");
                //par.Format.SpaceBefore = 10;
                //par.Format.Font.Size = 12;
                //par.Format.Font.Bold = true;

                //section.AddLineSpacing(10);

                //table = section.AddTable();
                //table.Rows.VerticalAlignment = VerticalAlignment.Center;
                //tableBorder = new Border { Style = BorderStyle.Single, Color = Color.FromCmyk(0, 0, 0, 50) };
                //table.Borders = new Borders { Top = tableBorder, Bottom = tableBorder.Clone(), Left = tableBorder.Clone(), Right = tableBorder.Clone() };
                //table.Format.Font.Size = 7;
                //table.Rows.Height = 10;
                //table.Rows.HeightRule = RowHeightRule.Exactly;

                //table.AddColumn(50);
                //table.AddColumn(480);


                //row = table.AddRow();
                //row.Height = 15;
                //row.Format.Font.Bold = true;
                //row.VerticalAlignment = VerticalAlignment.Center;
                //row.Shading.Color = Colors.LightGray;
                //row.Cells[0].AddParagraph("Question No.");
                //row.Cells[1].AddParagraph("Photo");

                //foreach (var detail in checks.SelectMany(s => s.MaintenanceCheckAnswers).OrderBy(o => o.ChecklistQuestion.QuestionOrder).SelectMany(s => s.MaintenanceCheckDetails))
                //               {
                //                   if (detail.MaintenanceCheckDetailPhotos.Any())
                //                   {
                //		var photos = detail.MaintenanceCheckDetailPhotos;
                //		int imageIdx = 0;
                //		Row i = null;

                //		foreach (MaintenanceCheckDetailPhoto imageRow in photos)
                //		{

                //			if (imageIdx % 2 == 0)
                //			{
                //				i = table.AddRow();
                //				i.Height = _pageWidth / 2 * .9;
                //				i.Format.Alignment = ParagraphAlignment.Left;
                //			}

                //			byte[] data = imageRow.File.Data;

                //			System.Drawing.Image bm = System.Drawing.Bitmap.FromStream(new System.IO.MemoryStream(data));
                //			MemoryStream mem = new System.IO.MemoryStream();

                //			System.Drawing.Bitmap ri = bm.ResizeImage((int)(_pageWidth / 2 * 1.2), _pageWidth / 2);
                //			ri.Save(mem, ImageFormat.Png);

                //			string imageString = "base64:" + Convert.ToBase64String(mem.ToArray());

                //			i.Cells[0].AddParagraph(detail.MaintenanceCheckAnswer.ChecklistQuestion.QuestionOrder.ToString());
                //			i.Cells[1].AddParagraph(imageRow.File.FileDownloadName + " ");
                //			i.Cells[1].AddParagraph();
                //			i.Cells[1].AddImage(imageString);
                //			imageIdx++;

                //		}
                //	}
                //               }
                //         }

                #endregion
            }
            else
            {
                return;
            }
        }

        public void VehicleChecklists(Section section, WorkInstruction wi, bool startOfShift)
        {
            VehicleChecklist vehicleChecklist = Context.WorkInstructions
                .Where(a => a.WorkInstructionPK == wi.WorkInstructionPK)
                .SelectMany(a => a.JobPack.Shift.VehicleChecklists)
                .Where(a => a.StartOfShift == startOfShift && a.IsActive)
                .FirstOrDefault();

            if (vehicleChecklist == null)
            {
                return;
            }

            Paragraph par = section.AddParagraph("Vehicle Checklist");
            par.Format.SpaceBefore = 10;
            par.Format.Font.Size = 12;
            par.Format.Font.Bold = true;

            //HorizontalLine(section);

            section.AddLineSpacing(10);

            Table table = section.AddTable();
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            Border tableBorder = new Border { Style = BorderStyle.Single, Color = Color.FromCmyk(0, 0, 0, 50) };
            table.Borders = new Borders { Top = tableBorder, Bottom = tableBorder.Clone(), Left = tableBorder.Clone(), Right = tableBorder.Clone() };
            table.Format.Font.Size = 7;
            table.Rows.Height = 10;
            table.Rows.HeightRule = RowHeightRule.Exactly;

            table.AddColumn(50);
            table.AddColumn(240);
            table.AddColumn(50);
            table.AddColumn(140);
            table.AddColumn(50);

            Row row = table.AddRow();
            row.Height = 15;
            row.Format.Font.Bold = true;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Shading.Color = Colors.LightGray;
            row.Cells[0].AddParagraph("Number");
            row.Cells[1].AddParagraph("Question");
            row.Cells[2].AddParagraph("Answer");
            row.Cells[3].AddParagraph("Reason");
            row.Cells[4].AddParagraph("Photo");

            foreach (VehicleChecklistAnswer answer in vehicleChecklist.VehicleChecklistAnswers.OrderBy(a => a.ChecklistQuestion.QuestionOrder))
            {
                row = table.AddRow();
                row.Height = 15;

                row.Cells[0].AddParagraph(answer.ChecklistQuestion.QuestionOrder.ToString());
                row.Cells[1].AddParagraph(answer.ChecklistQuestion.ChecklistQuestion1 ?? "");
                row.Cells[2].AddParagraph(answer.Answer == 0 ? "No" : answer.Answer == 1 ? "Yes" : "N/A");

                VehicleChecklistDefect defect = answer.VehicleChecklistDefects.FirstOrDefault();
                row.Cells[3].AddParagraph(defect?.Description ?? "");
                row.Cells[4].AddParagraph((defect != null && defect.VehicleChecklistDefectPhotos.Any()) ? "Yes" : "");
            }

            section.AddLineSpacing(5);

            table = section.AddTable();
            table.AddColumn(175);
            table.AddColumn(175);
            table.AddColumn(175);

            int i = -1;

            List<VehicleChecklistDefectPhoto> photos = vehicleChecklist.VehicleChecklistAnswers
                .SelectMany(a => a.VehicleChecklistDefects.Where(b => b.IsActive))
                .SelectMany(a => a.VehicleChecklistDefectPhotos.Where(b => b.IsActive))
                .ToList();

            foreach (VehicleChecklistDefectPhoto photo in photos)
            {
                i++;

                if (i == 0 || i == 3)
                {
                    i = 0;
                    row = table.AddRow();
                    row.Format.SpaceBefore = 3;
                    row.Format.SpaceAfter = 3;
                    row.Format.Font.Size = 8;
                }

                par = row.Cells[i].AddParagraph("Question Number: " + photo.VehicleChecklistDefect.VehicleChecklistAnswer.ChecklistQuestion.QuestionOrder);

                System.Drawing.Bitmap bmp = photo.File.Data.ToBitmap();
                bmp = bmp.CropWhitespace();
                bmp = bmp.Rescale(200, 200);

                string imageString = "base64:" + Convert.ToBase64String(bmp.ToByteArray());
                row.Cells[i].AddImage(imageString);
            }
        }

        public void Tasks(Section section, WorkInstruction wi)
        {
            section.AddTextFrame().Height = 10;

            Paragraph p = section.AddParagraph();
            p.Format.Font.Bold = true;
            p.Format.Font.Size = 12;
            p.AddText("Tasks");

            //HorizontalLine(section);

            section.AddLineSpacing(10);

            Table table = section.AddTable();

            Border tableBorder = new Border { Style = BorderStyle.Single, Color = Color.FromCmyk(0, 0, 0, 50) };
            table.Borders = new Borders { Top = tableBorder, Bottom = tableBorder.Clone(), Left = tableBorder.Clone(), Right = tableBorder.Clone() };
            table.Format.Font.Size = 7;
            table.Rows.Height = 10;
            table.Rows.HeightRule = RowHeightRule.Exactly;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            table.AddColumn(50);
            table.AddColumn(100);
            table.AddColumn(100);
            table.AddColumn(100);
            table.AddColumn(90);
            table.AddColumn(90);

            Row head = table.AddRow();
            head.Height = 15;
            head.Format.Font.Bold = true;
            head.Shading.Color = Colors.LightGray;

            head.Cells[0].AddParagraph("Task Order");
            head.Cells[1].AddParagraph("Description");
            head.Cells[2].AddParagraph("Start Time");
            head.Cells[3].AddParagraph("End Time");
            head.Cells[4].AddParagraph("Latitude");
            head.Cells[5].AddParagraph("Longitude");

            List<TaskModel> tasks = Context.Tasks.Where(a => a.WorksInstructionFK == wi.WorkInstructionPK).OrderBy(t => t.TaskOrder).Select(TaskModel.EntityToModel).ToList();

            foreach (TaskModel task in tasks)
            {
                Row r = table.AddRow();
                r.Height = 15;

                r.Cells[0].AddParagraph(task.TaskOrder.ToString());
                r.Cells[1].AddParagraph(task.Name);
                r.Cells[2].AddParagraph(task.StartTime.HasValue ? task.StartTime.Value.ToString() : "N/A");
                r.Cells[3].AddParagraph(task.EndTime.HasValue ? task.EndTime.Value.ToString() : "N/A");
                r.Cells[4].AddParagraph(task.Latitude.ToString());
                r.Cells[5].AddParagraph(task.Longitude.ToString());
            }

        }

        public void TaskDetails(Section section, WorkInstruction wi)
        {
            section.AddTextFrame().Height = 10;

            Paragraph p = section.AddParagraph();
            p.Format.Font.Bold = true;
            p.Format.Font.Size = 12;
            p.AddText("Task Details");

            //	HorizontalLine(section);

            List<TaskModel> tasks = Context.Tasks.Where(a => a.WorksInstructionFK == wi.WorkInstructionPK).OrderBy(t => t.TaskOrder).Select(TaskModel.EntityToModel).ToList();

            foreach (TaskModel task in tasks)
            {
                switch (task.TaskType)
                {
                    case "Checklist":
                        TaskTitle(section, task.Name);
                        section.AddTextFrame().Height = 10;
                        TaskChecklist(section, task.TaskID);
                        HorizontalLine(section);
                        break;
                    case "Signature":
                    case "Handover":
                        TaskTitle(section, task.Name);
                        section.AddTextFrame().Height = 10;
                        TaskChecklist(section, task.TaskID);
                        HorizontalLine(section, 0.5);
                        TaskSignatures(section, task.TaskID);
                        HorizontalLine(section, 0.5);
                        HorizontalLine(section);
                        break;
                    case "Equipment Install":
                        TaskTitle(section, task.Name);
                        section.AddTextFrame().Height = 10;
                        TaskEquipment(section, task.TaskID);
                        HorizontalLine(section);
                        break;
                    case "Equipment Collection":
                        TaskTitle(section, task.Name);
                        section.AddTextFrame().Height = 10;
                        TaskEquipment(section, task.TaskID);
                        HorizontalLine(section);
                        break;
                    case "Traffic Count":
                        section.AddTextFrame().Height = 10;
                        Paragraph pa = section.AddParagraph();
                        pa.Format.Font.Bold = true;
                        pa.Format.Font.Size = 10;
                        pa.AddText(task.Name + " Task");
                        pa = section.AddParagraph();
                        if (task.Notes != null)
                        {
                            var notesDetail = task.Notes.Split('.');
                            foreach (var note in notesDetail)
                            {
                                Paragraph notes = section.AddParagraph();
                                notes.Format.Font.Size = 8;
                                notes.AddText(note.Trim());
                            }
                        }
                        section.AddTextFrame().Height = 10;
                        HorizontalLine(section);
                        break;
                    case "Weather":
                        section.AddTextFrame().Height = 10;
                        pa = section.AddParagraph();
                        pa.Format.Font.Bold = true;
                        pa.Format.Font.Size = 10;
                        pa.AddText(task.Name + " Task - " + task.Weather);
                        section.AddTextFrame().Height = 10;
                        HorizontalLine(section);
                        break;
                    default:
                        if (task.PhotosRequired)
                        {
                            TaskTitle(section, task.Name);
                        }
                        break;
                }
                section.AddTextFrame().Height = 10;
                TaskPhotos(section, task.TaskID);
            }
        }


        public void TaskSignatures(Section section, Guid taskID)
        {
            List<TaskSignatureModel> sigs = Context.TaskSignatures.Where(t => t.TaskFK.Equals(taskID)).OrderBy(t => t.CreatedDate).Select(TaskSignatureModel.ETMFile).ToList();

            if (sigs.Count == 0)
                return;

            Table imageTable = section.AddTable();


            imageTable.AddColumn(_pageWidth / 2);
            imageTable.AddColumn(_pageWidth / 2);

            Row title = imageTable.AddRow();
            title.Height = 30;
            title.Format.Font.Size = 10;

            title.Cells[0].MergeRight = 1;
            title.Cells[0].AddTextTitle("Task Signatures");
            title.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            int imageIdx = 0;
            Row i = null;

            foreach (TaskSignatureModel imageRow in sigs)
            {
                string photoTitle = "";

                if (imageRow.IsClient)
                {
                    photoTitle = "Client: " + imageRow.EmployeeName;
                }
                else
                {
                    photoTitle = "Employee: " + imageRow.EmployeeName;
                }

                if (imageIdx % 2 == 0)
                {
                    i = imageTable.AddRow();
                    i.Height = 70;
                    i.Format.Alignment = ParagraphAlignment.Center;
                }

                if (imageRow.Data != default(byte[]))
                {
                    System.Drawing.Bitmap bmp = imageRow.Data.ToBitmap();
                    bmp = bmp.CropWhitespace();
                    bmp = bmp.Rescale(200, 40);
                    bmp = bmp.Padding(10);

                    string imageString = "base64:" + Convert.ToBase64String(bmp.ToByteArray());

                    Cell cell = i.Cells[imageIdx % 2];

                    cell.AddParagraph(photoTitle);

                    Paragraph par = cell.AddParagraph();
                    par.AddImage(imageString);
                }


                imageIdx++;
            }

            if (imageIdx > 0)
            {
                title.KeepWith = 1;
            }
        }

        public bool IsTaskRiskAssessmentChecklist(Guid taskID)
        {
            return Context.Tasks.Any(a => a.TaskPK == taskID && (a.Checklist.IsHighSpeedChecklist || a.Checklist.IsLowSpeedChecklist));
        }

        public void TaskChecklist(Section section, Guid taskID)
        {
            bool isTaskRiskAssessmentChecklist = IsTaskRiskAssessmentChecklist(taskID);

            Func<int?, string> standardAnswer = a =>
            {
                return a == null ? "" :
                    a == 0 ? "No" :
                    a == 1 ? "Yes" : "";
            };

            Func<int?, string> riskAssessmentAnswer = a =>
            {
                return a == null ? "" :
                    a == 0 ? "High" :
                    a == 1 ? "Med" :
                    a == 2 ? "Low" : "";
            };

            List<ChecklistAnswerModel> answers = Context.vv_TaskChecklists.Where(mc => mc.TaskPK.Equals(taskID))
                .OrderBy(mc => mc.TaskChecklistPK)
                .ThenBy(mc => mc.QuestionOrder)
                .Select(ChecklistAnswerModel.TaskETM)
                .ToList();

            if (!answers.Any())
            {
                return;
            }

            Table table = section.AddTable();

            table.Format.Font.Size = 7;
            table.Rows.Height = 10;
            table.Rows.HeightRule = RowHeightRule.Exactly;

            table.AddColumn(300);
            table.AddColumn(50);
            table.AddColumn(100);

            Row head = table.AddRow();
            head.Height = 15;
            head.Format.Font.Bold = true;

            head.Cells[0].AddParagraph("Question");
            head.Cells[1].AddParagraph("Answer");
            head.Cells[2].AddParagraph("Time");

            foreach (ChecklistAnswerModel a in answers)
            {
                Row r = table.AddRow();
                r.Height = 15;

                string answer = "N/A";
                if (isTaskRiskAssessmentChecklist)
                {
                    answer = riskAssessmentAnswer(a.ChecklistAnswer);
                }
                else
                {
                    answer = standardAnswer(a.ChecklistAnswer);
                }

                r.Cells[0].AddParagraph(a.ChecklistQuestion);
                r.Cells[1].AddParagraph(answer);
                r.Cells[2].AddParagraph(a.ChecklistTime.ToString("dd/MM/yyyy HH:mm:ss"));
            }
        }

        public void TaskPhotos(Section section, Guid taskID)
        {
            List<TaskPhotoModel> photos = Context.TaskPhotos.Where(t => t.TaskFK.Equals(taskID)).Select(TaskPhotoModel.ETMFile).ToList();

            if (photos.Count == 0)
                return;

            Table imageTable = section.AddTable();


            imageTable.AddColumn(_pageWidth / 2);
            imageTable.AddColumn(_pageWidth / 2);

            Row title = imageTable.AddRow();
            title.Height = 30;
            title.Format.Font.Size = 10;

            title.Cells[0].MergeRight = 1;
            title.Cells[0].AddTextTitle("Task Photos");
            title.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            int imageIdx = 0;
            Row i = null;

            foreach (TaskPhotoModel imageRow in photos)
            {

                if (imageIdx % 2 == 0)
                {
                    i = imageTable.AddRow();
                    i.Height = _pageWidth / 2 * .8;
                    i.Format.Alignment = ParagraphAlignment.Center;
                }

                byte[] data = imageRow.Data;

                System.Drawing.Image bm = System.Drawing.Bitmap.FromStream(new System.IO.MemoryStream(data));
                MemoryStream mem = new System.IO.MemoryStream();

                System.Drawing.Bitmap ri = bm.ResizeImage((int)(_pageWidth / 2 * 1.2), _pageWidth / 2);
                ri.Save(mem, ImageFormat.Png);

                string imageString = "base64:" + Convert.ToBase64String(mem.ToArray());

                i.Cells[imageIdx % 2].AddParagraph(imageRow.Name).AddImage(imageString);
                imageIdx++;

            }

            if (imageIdx > 0)
                title.KeepWith = 1;

            HorizontalLine(section);
        }

        #endregion

        #region JobSheetReport

        public byte[] GenerateJobSheetReport(Guid workInstructionID, out string filename)
        {
            var model = Context.WorkInstructions.Where(a => a.WorkInstructionPK == workInstructionID).Select(JobSheetReportModel.EntityToModel).Single();

            model.WorkOrderNo = $"{CustomerExtensions.Abbreviate(model.CustomerName)}-{model.WorkInstructionNumber.ToString("D5")}";

            if (model.ArrivalAtSite != null && model.LeaveSite != null)
            {
                model.VisitDuration = ((DateTime)model.LeaveSite - (DateTime)model.ArrivalAtSite).Humanize(2, minUnit: TimeUnit.Minute);
            }

            model.Engineers = Context.ShiftStaffs.Where(a => a.IsActive && a.Shift.Schedules.Any(b => b.WorkInstructionFK == workInstructionID)).Select(JobSheetReportEngineer.EntityToModel).ToList();
            model.Vehicles = Context.ShiftVehicles.Where(a => a.IsActive && a.Shift.Schedules.Any(b => b.WorkInstructionFK == workInstructionID)).Select(JobSheetReportVehicle.EntityToModel).ToList();

            model.CleansingLogs = Context.CleansingLogs.Where(a => a.WorkInstructionFK == workInstructionID && a.IsActive).OrderBy(a => a.CreatedDate).Select(JobSheetReportCleansingLog.EntityToModel).ToList();
            model.UnloadingProcesses = Context.UnloadingProcesses.Where(a => a.WorkInstructionFK == workInstructionID && a.IsActive).OrderBy(a => a.CreatedDate).Select(JobSheetReportUnloadingProcess.EntityToModel).ToList();
            model.Checklists = Context.TaskChecklists.Where(a => a.Task.WorksInstructionFK == model.WorkInstructionID && a.IsActive).OrderBy(a => a.Task.TaskOrder).Select(JobSheetReportChecklist.EntityToModel).ToList();
            model.ActivityRiskAssessments = Context.ActivityRiskAssessments.Where(a => a.Task.WorksInstructionFK == model.WorkInstructionID && a.IsActive).OrderBy(a => a.Task.TaskOrder).Select(JobSheetReportActivityRiskAssessments.EntityToModel).ToList();

            model.Images.AddRange(Context.TaskChecklistAnswerPhotos.Where(a => a.TaskChecklistAnswer.TaskChecklist.Task.WorksInstructionFK == model.WorkInstructionID && a.File != null && a.IsActive).Select(JobSheetReportImage.TaskChecklistAnswerPhotoToModel).ToList());
            model.Images.AddRange(Context.TaskPhotos.Where(a => a.Task.WorksInstructionFK == model.WorkInstructionID && a.File != null && a.IsActive).Select(JobSheetReportImage.TaskPhotoToModel).ToList());
            model.Images.AddRange(Context.CleansingLogPhotos.Where(a => a.CleansingLog.Task.WorksInstructionFK == model.WorkInstructionID && a.File != null && a.IsActive).Select(JobSheetReportImage.CleansingLogPhotoToModel).ToList());
            model.Images.AddRange(Context.UnloadingProcessPhotos.Where(a => a.UnloadingProcess.Task.WorksInstructionFK == model.WorkInstructionID && a.File != null && a.IsActive).Select(JobSheetReportImage.UnloadingProcessPhotoToModel).ToList());
            model.Images = model.Images.OrderBy(a => a.Captured).ToList();

            //model.FooterText = "Example Footer Text";

            filename = $"Job Sheet {model.WorkOrderNo} ({DateTime.Now.ToString("dd-MM-yyyy")}).pdf";

            // create pdf
            var document = new Document();
            document.DefaultStyles();

            var section = document.DefaultSection();
            section.PageSetup.HeaderDistance = 24;
            section.PageSetup.BottomMargin = 24;

            var sectionWidth = section.Width();

            // header
            section.PageSetup.DifferentFirstPageHeaderFooter = true;
            section.PageSetup.TopMargin = 124;

            SetJobSheetHeader(section.Headers.FirstPage, model, true);
            SetJobSheetHeader(section.Headers.Primary, model, false);
            //SetJobSheetFooter(section.Footers.FirstPage, model);
            //SetJobSheetFooter(section.Footers.Primary, model);

            // visit
            var table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.Format.LeftIndent = 5;
            table.Format.SpaceBefore = 6;
            table.Format.SpaceAfter = 6;
            table.Format.Font.Size = 14;
            table.Format.Font.Color = Colors.White;
            table.Shading.Color = Color.Parse("#257f44");

            table.AddColumn(sectionWidth);

            var row = table.AddRow();
            var par = row.Cells[0].AddParagraph("This Visit");

            section.AddLineSpacing(10);

            // client ref
            table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.Format.SpaceBefore = 4;
            table.Format.SpaceAfter = 4;
            table.Shading.Color = Color.Parse("#f2f2f2");

            table.AddColumn(sectionWidth * 0.20);
            table.AddColumn(sectionWidth * 0.80);

            row = table.AddRow();
            par = row.Cells[0].AddParagraph("Client Ref Number:");
            par.Format.LeftIndent = 5;
            par.Format.Font.Bold = true;

            par = row.Cells[1].AddParagraph(model.ClientRefNumber ?? "");

            section.AddLineSpacing(8);

            // engineer container table
            table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.LeftPadding = 0;
            table.RightPadding = 0;

            table.AddColumn((sectionWidth * 0.50) + 4);
            table.AddColumn((sectionWidth * 0.50) - 4);

            var containerRow = table.AddRow();

            // left table
            var leftTable = new Table();
            leftTable.Format.SpaceBefore = 4;
            leftTable.Format.SpaceAfter = 4;
            leftTable.Shading.Color = Color.Parse("#f2f2f2");

            leftTable.AddColumn(sectionWidth * 0.20);
            leftTable.AddColumn((sectionWidth * 0.30) - 8);

            for (var i = 0; i < model.Engineers.Count; i++)
            {
                var engineer = model.Engineers[i];
                var isDriver = model.Vehicles.Any(a => a.EmployeeID == engineer.EmployeeID);

                row = leftTable.AddRow();

                if (i == 0)
                {
                    par = row.Cells[0].AddParagraph("Engineer(s):");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;
                }

                par = row.Cells[1].AddParagraph(engineer.EmployeeName + (isDriver ? " (driver)" : "") ?? "");
            }

            for (var i = 0; i < model.Vehicles.Count; i++)
            {
                row = leftTable.AddRow();

                if (i == 0)
                {
                    par = row.Cells[0].AddParagraph("Vehicle(s):");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;
                }

                par = row.Cells[1].AddParagraph(model.Vehicles[i].Registration ?? "");
            }

            if (!string.IsNullOrWhiteSpace(model.VisitDuration))
            {
                row = leftTable.AddRow();
                par = row.Cells[0].AddParagraph("Visit Duration:");
                par.Format.LeftIndent = 5;
                par.Format.Font.Bold = true;
                par = row.Cells[1].AddParagraph(model.VisitDuration ?? "");
            }

            containerRow.Cells[0].Elements.Add(leftTable);

            // right table
            var rightTable = new Table();
            rightTable.Format.SpaceBefore = 4;
            rightTable.Format.SpaceAfter = 4;
            rightTable.Shading.Color = Color.Parse("#f2f2f2");

            rightTable.AddColumn(sectionWidth * 0.20);
            rightTable.AddColumn((sectionWidth * 0.30) - 4);

            row = rightTable.AddRow();
            par = row.Cells[0].AddParagraph("Activity Type:");
            par.Format.LeftIndent = 5;
            par.Format.Font.Bold = true;
            par = row.Cells[1].AddParagraph(model.ActivityType);

            row = rightTable.AddRow();
            par = row.Cells[0].AddParagraph("Work Status:");
            par.Format.LeftIndent = 5;
            par.Format.Font.Bold = true;
            par = row.Cells[1].AddParagraph(model.WorkStatus);

            row = rightTable.AddRow();
            par = row.Cells[0].AddParagraph("Date Uploaded:");
            par.Format.LeftIndent = 5;
            par.Format.Font.Bold = true;
            par = row.Cells[1].AddParagraph(model.DateUploaded != null ? model.DateUploaded.Value.ToOrdinalWords() + " @ " + model.DateUploaded.Value.ToString("HH:mm") : "");

            containerRow.Cells[1].Elements.Add(rightTable);

            section.AddLineSpacing(8);

            // work location
            table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.Format.SpaceBefore = 4;
            table.Format.SpaceAfter = 4;
            table.Shading.Color = Color.Parse("#f2f2f2");

            table.AddColumn(sectionWidth * 0.20);
            table.AddColumn(sectionWidth * 0.80);

            row = table.AddRow();
            par = row.Cells[0].AddParagraph("Work Location:");
            par.Format.LeftIndent = 5;
            par.Format.Font.Bold = true;

            par = row.Cells[1].AddParagraph(model.WorkLocation ?? "");

            section.AddLineSpacing(8);

            // work undertaken
            if (!string.IsNullOrWhiteSpace(model.WorkUndertaken))
            {
                table = section.AddTable();
                table.Rows.LeftIndent = 0;
                table.Format.SpaceBefore = 4;
                table.Format.SpaceAfter = 4;
                table.Shading.Color = Color.Parse("#f2f2f2");

                table.AddColumn(sectionWidth * 0.20);
                table.AddColumn(sectionWidth * 0.80);

                row = table.AddRow();
                par = row.Cells[0].AddParagraph("Work Undertaken:");
                par.Format.LeftIndent = 5;
                par.Format.Font.Bold = true;

                par = row.Cells[1].AddParagraph(model.WorkUndertaken ?? "");

                section.AddLineSpacing(8);
            }

            // travel summary
            section.AddLineSpacing(2);

            par = section.AddParagraph("Travel Summary");
            par.Format.Font.Size = 11;
            par.Format.Font.Bold = true;
            par.Format.SpaceAfter = 6;
            par.Format.LeftIndent = 8;

            table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.Format.SpaceBefore = 4;
            table.Format.SpaceAfter = 4;
            table.Shading.Color = Color.Parse("#f2f2f2");

            table.AddColumn(sectionWidth * 0.20);
            table.AddColumn(sectionWidth * 0.80);

            row = table.AddRow();
            par = row.Cells[0].AddParagraph("Leave Depot:");
            par.Format.LeftIndent = 5;
            par.Format.Font.Bold = true;

            par = row.Cells[1].AddParagraph(model.LeaveDepot?.ToString("dd/MM/yyyy HH:mm") ?? "");

            row = table.AddRow();
            par = row.Cells[0].AddParagraph("Arrival at Site:");
            par.Format.LeftIndent = 5;
            par.Format.Font.Bold = true;

            par = row.Cells[1].AddParagraph(model.ArrivalAtSite?.ToString("dd/MM/yyyy HH:mm") ?? "");

            if (!model.UnloadingProcesses.Any())
            {
                row = table.AddRow();
                par = row.Cells[0].AddParagraph("Leave Site:");
                par.Format.LeftIndent = 5;
                par.Format.Font.Bold = true;

                par = row.Cells[1].AddParagraph(model.LeaveSite?.ToString("dd/MM/yyyy HH:mm") ?? "");
            }

            row = table.AddRow();
            par = row.Cells[0].AddParagraph("Return to Depot:");
            par.Format.LeftIndent = 5;
            par.Format.Font.Bold = true;

            par = row.Cells[1].AddParagraph(model.ReturnToDepot?.ToString("dd/MM/yyyy HH:mm") ?? "");

            section.AddLineSpacing(8);

            // unloading process
            if (model.UnloadingProcesses.Any())
            {
                for (var i = 0; i < model.UnloadingProcesses.Count; i++)
                {
                    var unloadingProcess = model.UnloadingProcesses[i];

                    // container table
                    table = section.AddTable();
                    table.Rows.LeftIndent = 0;
                    table.LeftPadding = 0;
                    table.RightPadding = 0;
                    table.Shading.Color = Color.Parse("#f2f2f2");

                    table.AddColumn((sectionWidth * 0.50) + 4);
                    table.AddColumn((sectionWidth * 0.50) - 4);

                    containerRow = table.AddRow();

                    // left table
                    leftTable = new Table();
                    leftTable.Format.SpaceBefore = 4;
                    leftTable.Format.SpaceAfter = 4;

                    leftTable.AddColumn(sectionWidth * 0.26);
                    leftTable.AddColumn((sectionWidth * 0.24) - 8);

                    row = leftTable.AddRow();
                    par = row.Cells[0].AddParagraph("Departed for Disposal Site:");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;
                    par = row.Cells[1].AddParagraph(unloadingProcess.DepartToDisposalSiteDate?.ToString("dd/MM/yyyy HH:mm") ?? "");

                    row = leftTable.AddRow();
                    par = row.Cells[0].AddParagraph("Arrived at Disposal Site:");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;
                    par = row.Cells[1].AddParagraph(unloadingProcess.ArriveAtDisposalSiteDate?.ToString("dd/MM/yyyy HH:mm") ?? "");

                    row = leftTable.AddRow();
                    par = row.Cells[0].AddParagraph("Completed Disposal:");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;
                    par = row.Cells[1].AddParagraph(unloadingProcess.LeaveDisposalSiteDate?.ToString("dd/MM/yyyy HH:mm") ?? "");

                    containerRow.Cells[0].Elements.Add(leftTable);

                    // right table
                    rightTable = new Table();
                    rightTable.Format.SpaceBefore = 4;
                    rightTable.Format.SpaceAfter = 4;

                    rightTable.AddColumn(sectionWidth * 0.24);
                    rightTable.AddColumn((sectionWidth * 0.26) - 4);

                    row = rightTable.AddRow();
                    par = row.Cells[0].AddParagraph("Disposal Site:");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;
                    par = row.Cells[1].AddParagraph(unloadingProcess.DisposalSiteLookup ?? "");

                    row = rightTable.AddRow();
                    par = row.Cells[0].AddParagraph("Waste Ticket Number:");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;
                    par = row.Cells[1].AddParagraph(unloadingProcess.WasteTicketNumber ?? "");

                    row = rightTable.AddRow();
                    par = row.Cells[0].AddParagraph("Volume Waste Disposed:");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;
                    par = row.Cells[1].AddParagraph(unloadingProcess.VolumeOfWasteDisposed?.ToString("0.#") ?? "");

                    containerRow.Cells[1].Elements.Add(rightTable);

                    if (i < model.UnloadingProcesses.Count - 1)
                    {
                        section.AddLineSpacing(8);
                    }
                }

                section.AddLineSpacing(10);
            }

            section.AddPageBreak();

            // location
            string base64 = null;
            Image image = null;

            var isLocationValid = GpsExtensions.IsLocationValid(model.LocationLongitude, model.LocationLatitude);

            table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.Format.LeftIndent = 5;
            table.Format.SpaceBefore = 6;
            table.Format.SpaceAfter = 6;
            table.Format.Font.Size = 14;
            table.Format.Font.Color = Colors.White;
            table.Shading.Color = Color.Parse("#257f44");

            table.AddColumn(sectionWidth * 0.50);
            table.AddColumn(sectionWidth * 0.50);

            row = table.AddRow();

            par = row.Cells[0].AddParagraph("Location");

            par = row.Cells[1].AddParagraph($"Work Start GPS: {(isLocationValid ? model.LocationLatitude + ", " + model.LocationLongitude : "N/A")}");
            par.Format.SpaceBefore = 8;
            par.Format.RightIndent = 5;
            par.Format.Font.Size = 10;
            par.Format.Font.Color = Color.Parse("#e3e3e3");
            par.Format.Alignment = ParagraphAlignment.Right;

            section.AddLineSpacing(10);

            // map
            if (isLocationValid)
            {
                var bytes = GetJobSheetGoogleMapImage((decimal)model.LocationLongitude, (decimal)model.LocationLatitude);
                if (bytes != null)
                {
                    base64 = "base64:" + Convert.ToBase64String(bytes);
                    image = section.AddImage(base64);
                    image.Width = sectionWidth;

                    section.AddLineSpacing(10);
                }
            }

            // work activity details
            // cleansing log is the only thing left in this section now, if no cleansing log skip section
            if (model.CleansingLogs.Any())
            {
                table = section.AddTable();
                table.Rows.LeftIndent = 0;
                table.Format.LeftIndent = 5;
                table.Format.SpaceBefore = 6;
                table.Format.SpaceAfter = 6;
                table.Format.Font.Size = 14;
                table.Format.Font.Color = Colors.White;
                table.Shading.Color = Color.Parse("#257f44");

                table.AddColumn(sectionWidth);

                row = table.AddRow();
                par = row.Cells[0].AddParagraph("Work Activity Details");

                section.AddLineSpacing(10);
            }

            // cleansing log
            if (model.CleansingLogs.Any() & !model.ActivityRiskAssessments.Any())
            {
                par = section.AddParagraph("Cleansing Log");
                par.Format.Font.Size = 11;
                par.Format.Font.Bold = true;
                par.Format.SpaceAfter = 6;
                par.Format.LeftIndent = 8;

                for (var i = 0; i < model.CleansingLogs.Count; i++)
                {
                    var cleansingLog = model.CleansingLogs[i];

                    // road table
                    table = section.AddTable();
                    table.Rows.LeftIndent = 0;
                    table.Format.LeftIndent = 5;
                    table.Format.SpaceBefore = 4;
                    table.Format.SpaceAfter = 4;
                    table.Shading.Color = Color.Parse("#f2f2f2");
                    table.Borders.Right.Width = 0.5;
                    table.Borders.Right.Color = Colors.White;

                    table.AddColumn(sectionWidth * 0.22);
                    table.AddColumn(sectionWidth * 0.20);
                    table.AddColumn(sectionWidth * 0.15);
                    table.AddColumn(sectionWidth * 0.11);
                    table.AddColumn(sectionWidth * 0.16);
                    table.AddColumn(sectionWidth * 0.16);

                    row = table.AddRow();
                    row.Format.Font.Color = Colors.White;
                    row.Shading.Color = Color.Parse("#808080");
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                    par = row.Cells[0].AddParagraph("Road");
                    par = row.Cells[1].AddParagraph("Section");
                    par = row.Cells[2].AddParagraph("Lane");
                    par = row.Cells[3].AddParagraph("Direction");
                    par = row.Cells[4].AddParagraph("Start Marker");
                    par = row.Cells[5].AddParagraph("End Marker");

                    row = table.AddRow();
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                    par = row.Cells[0].AddParagraph(cleansingLog.Road ?? "");
                    par = row.Cells[1].AddParagraph(cleansingLog.Section ?? "");
                    par = row.Cells[2].AddParagraph(cleansingLog.LaneLookup ?? "");
                    par = row.Cells[3].AddParagraph(cleansingLog.DirectionLookup ?? "");
                    par = row.Cells[4].AddParagraph(cleansingLog.StartMarker ?? "");
                    par = row.Cells[5].AddParagraph(cleansingLog.EndMarker ?? "");

                    section.AddLineSpacing(6);

                    // gullies table
                    table = section.AddTable();
                    table.Rows.LeftIndent = 0;
                    table.Format.LeftIndent = 5;
                    table.Format.SpaceBefore = 4;
                    table.Format.SpaceAfter = 4;
                    table.Shading.Color = Color.Parse("#f2f2f2");
                    table.Borders.Right.Width = 0.5;
                    table.Borders.Right.Color = Colors.White;

                    table.AddColumn(sectionWidth * 0.17);
                    table.AddColumn(sectionWidth * 0.16);
                    table.AddColumn(sectionWidth * 0.18);
                    table.AddColumn(sectionWidth * 0.17);
                    table.AddColumn(sectionWidth * 0.16);
                    table.AddColumn(sectionWidth * 0.16);

                    row = table.AddRow();
                    row.Format.Font.Color = Colors.White;
                    row.Shading.Color = Color.Parse("#808080");
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                    par = row.Cells[0].AddParagraph("Gullies Cleaned");
                    par = row.Cells[1].AddParagraph("Gullies Missed");
                    par = row.Cells[2].AddParagraph("Catchpits Cleaned");
                    par = row.Cells[3].AddParagraph("Catchpits Missed");
                    par = row.Cells[4].AddParagraph("Channels (m)");
                    par = row.Cells[5].AddParagraph("Slot Drains (m)");

                    row = table.AddRow();
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                    par = row.Cells[0].AddParagraph(cleansingLog.GulliesCleaned?.ToString("0.#") ?? "");
                    par = row.Cells[1].AddParagraph(cleansingLog.GulliesMissed?.ToString("0.#") ?? "");
                    par = row.Cells[2].AddParagraph(cleansingLog.CatchpitsCleaned?.ToString("0.#") ?? "");
                    par = row.Cells[3].AddParagraph(cleansingLog.CatchpitsMissed?.ToString("0.#") ?? "");
                    par = row.Cells[4].AddParagraph(cleansingLog.Channels?.ToString("0.#") ?? "");
                    par = row.Cells[5].AddParagraph(cleansingLog.SlotDrains?.ToString("0.#") ?? "");

                    section.AddLineSpacing(6);

                    // comments
                    table = section.AddTable();
                    table.Rows.LeftIndent = 0;
                    table.Format.SpaceBefore = 4;
                    table.Format.SpaceAfter = 4;
                    table.Shading.Color = Color.Parse("#f2f2f2");

                    table.AddColumn(sectionWidth * 0.20);
                    table.AddColumn(sectionWidth * 0.80);

                    row = table.AddRow();
                    par = row.Cells[0].AddParagraph("Comments:");
                    par.Format.LeftIndent = 5;
                    par.Format.Font.Bold = true;

                    par = row.Cells[1].AddParagraph(cleansingLog.Comments ?? "");

                    if (i < model.CleansingLogs.Count - 1)
                    {
                        par = section.AddParagraph();
                        par.Format.LineSpacingRule = LineSpacingRule.Exactly;
                        par.Format.LineSpacing = 0.5;
                        par.Format.SpaceBefore = 8;
                        par.Format.SpaceAfter = 8;
                        par.Format.LeftIndent = 30;
                        par.Format.RightIndent = 30;
                        par.Format.Shading.Color = Color.Parse("#257f44");
                    }
                }

                section.AddLineSpacing(8);
            }
            else if (model.CleansingLogs.Any() & model.ActivityRiskAssessments.Any())
            {
                par = section.AddParagraph("Cleansing Log");
                par.Format.Font.Size = 11;
                par.Format.Font.Bold = true;
                par.Format.SpaceAfter = 6;
                par.Format.LeftIndent = 8;

                // road table
                table = section.AddTable();
                table.Format.Font.Size = 8;
                table.Rows.LeftIndent = 0;
                table.Format.SpaceBefore = 4;
                table.Format.SpaceAfter = 4;
                table.Shading.Color = Color.Parse("#f2f2f2");
                table.Borders.Right.Width = 0.5;
                table.Borders.Right.Color = Colors.White;

                table.AddColumn(sectionWidth * 0.3);
                table.AddColumn(sectionWidth * 0.07);
                table.AddColumn(sectionWidth * 0.07);
                table.AddColumn(sectionWidth * 0.08);
                table.AddColumn(sectionWidth * 0.07);
                table.AddColumn(sectionWidth * 0.07);
                table.AddColumn(sectionWidth * 0.34);

                row = table.AddRow();
                row.Format.Font.Color = Colors.White;
                row.Shading.Color = Color.Parse("#808080");
                row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                row.Cells[0].AddParagraph("Road");
                row.Cells[1].AddParagraph("Gullies");
                row.Cells[2].AddParagraph("Pits");
                row.Cells[3].AddParagraph("Channels");
                row.Cells[4].AddParagraph("Defects");
                row.Cells[5].AddParagraph("Missed");
                row.Cells[6].AddParagraph("Comments");

                for (var i = 0; i < model.CleansingLogs.Count; i++)
                {
                    var cleansingLog = model.CleansingLogs[i];

                    row = table.AddRow();
                    if (i % 2 == 0)
                        row.Shading.Color = Color.Parse("#dedede");
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                    row.Cells[1].Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[2].Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[3].Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[4].Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[5].Format.Alignment = ParagraphAlignment.Right;

                    row.Cells[0].AddParagraph(cleansingLog.Road ?? "");
                    row.Cells[1].AddParagraph(cleansingLog.GulliesCleaned?.ToString("0.#") ?? "");
                    row.Cells[2].AddParagraph(cleansingLog.CatchpitsCleaned?.ToString("0.#") ?? "");
                    row.Cells[3].AddParagraph(cleansingLog.Channels?.ToString("0.#") ?? "");
                    row.Cells[4].AddParagraph(cleansingLog.Defects?.ToString("0.#") ?? "");
                    if (cleansingLog.GulliesMissed.HasValue || cleansingLog.CatchpitsMissed.HasValue)
                        row.Cells[5].AddParagraph(((cleansingLog.GulliesMissed ?? 0) + (cleansingLog.CatchpitsMissed ?? 0)).ToString("0.#") ?? "");
                    row.Cells[6].AddParagraph(cleansingLog.Comments ?? "");

                    section.AddLineSpacing(6);
                }

                section.AddLineSpacing(8);
            }

            section.AddPageBreak();

            //Activity Roads
            if (model.ActivityRiskAssessments.Any())
            {
                par = section.AddParagraph("Activity Risk Assessments");
                par.Format.Font.Size = 11;
                par.Format.Font.Bold = true;
                par.Format.SpaceAfter = 6;
                par.Format.LeftIndent = 8;

                for (var i = 0; i < model.ActivityRiskAssessments.Count; i++)
                {
                    var activityRiskAssessment = model.ActivityRiskAssessments[i];

                    table = section.AddTable();
                    table.Rows.LeftIndent = 0;
                    table.Format.LeftIndent = 5;
                    table.Format.SpaceBefore = 4;
                    table.Format.SpaceAfter = 4;
                    table.Shading.Color = Color.Parse("#f2f2f2");
                    table.Borders.Right.Width = 0.5;
                    table.Borders.Right.Color = Colors.White;

                    table.AddColumn(sectionWidth * 0.08);
                    table.AddColumn(sectionWidth * 0.256);
                    table.AddColumn(sectionWidth * 0.126);
                    table.AddColumn(sectionWidth * 0.04);
                    table.AddColumn(sectionWidth * 0.17);
                    table.AddColumn(sectionWidth * 0.328);

                    //row = table.AddRow();
                    //row.Format.Font.Color = Colors.White;
                    //row.Shading.Color = Color.Parse("#808080");
                    //row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;
                    //row.Cells[0].AddParagraph().AddFormattedText("Road  ", TextFormat.Bold).AddFormattedText(activityRiskAssessment.RoadName ?? "", TextFormat.NotBold);
                    //row.Cells[1].AddParagraph().AddFormattedText("Road  ", TextFormat.Bold).AddFormattedText(activityRiskAssessment.RoadName ?? "", TextFormat.NotBold);
                    ////row.Cells[0].MergeRight = 1;
                    //row.Cells[2].AddParagraph().AddFormattedText("Road Speed  ", TextFormat.Bold).AddFormattedText(activityRiskAssessment.RoadSpeed ?? "", TextFormat.NotBold);
                    //row.Cells[3].AddParagraph().AddFormattedText("TM Requirements  ", TextFormat.Bold).AddFormattedText(activityRiskAssessment.TMRequirement ?? "", TextFormat.NotBold);
                    //row.Cells[3].MergeRight = 2;

                    row = table.AddRow();
                    row.Format.Font.Color = Colors.White;
                    row.Shading.Color = Color.Parse("#808080");
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;
                    row.Cells[0].AddParagraph().AddFormattedText("Road  ", TextFormat.Bold);
                    row.Cells[0].Borders.Right.Width = 0;
                    row.Cells[1].AddParagraph(activityRiskAssessment.RoadName ?? "");
                    row.Cells[1].Format.LeftIndent = 0;
                    row.Cells[2].AddParagraph().AddFormattedText("Road Speed  ", TextFormat.Bold);
                    row.Cells[2].Borders.Right.Width = 0;
                    row.Cells[3].AddParagraph(activityRiskAssessment.RoadSpeed ?? "");
                    row.Cells[3].Format.LeftIndent = 0;
                    row.Cells[4].AddParagraph().AddFormattedText("TM Requirements  ", TextFormat.Bold);
                    row.Cells[4].Borders.Right.Width = 0;
                    row.Cells[5].AddParagraph(activityRiskAssessment.TMRequirement ?? "");
                    row.Cells[5].Format.LeftIndent = 0;

                    table = section.AddTable();
                    table.Rows.LeftIndent = 0;
                    table.Format.LeftIndent = 5;
                    table.Format.SpaceBefore = 4;
                    table.Format.SpaceAfter = 4;
                    table.Shading.Color = Color.Parse("#f2f2f2");
                    table.Borders.Right.Width = 0.5;
                    table.Borders.Right.Color = Colors.White;

                    table.AddColumn(sectionWidth * 0.17);
                    table.AddColumn(sectionWidth * 0.166);
                    table.AddColumn(sectionWidth * 0.166);
                    table.AddColumn(sectionWidth * 0.166);
                    table.AddColumn(sectionWidth * 0.166);
                    table.AddColumn(sectionWidth * 0.166);

                    row = table.AddRow();
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    //row.VerticalAlignment = VerticalAlignment.Bottom;
                    row.Format.LeftIndent = 0;
                    row.Cells[0].AddParagraph().AddFormattedText("Schools", TextFormat.Bold);
                    row.Cells[1].AddParagraph().AddFormattedText("Tree Canopies", TextFormat.Bold);
                    row.Cells[2].AddParagraph().AddFormattedText("Pedestrian Crossings", TextFormat.Bold);
                    row.Cells[3].AddParagraph().AddFormattedText("Water Courses", TextFormat.Bold);
                    row.Cells[4].AddParagraph().AddFormattedText("Overhead Cables", TextFormat.Bold);
                    row.Cells[5].AddParagraph().AddFormattedText("Adverse Weather", TextFormat.Bold);

                    row = table.AddRow();
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;
                    row.Borders.Top.Width = 0.5;
                    row.Borders.Top.Color = Colors.White;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.Format.Font.Name = "Wingdings";
                    row.Cells[0].AddParagraph(activityRiskAssessment.Schools ? "\u00fc" : "");
                    row.Cells[1].AddParagraph(activityRiskAssessment.TreeCanopies ? "\u00fc" : "");
                    row.Cells[2].AddParagraph(activityRiskAssessment.PedestrianCrossings ? "\u00fc" : "");
                    row.Cells[3].AddParagraph(activityRiskAssessment.WaterCourses ? "\u00fc" : "");
                    row.Cells[4].AddParagraph(activityRiskAssessment.OverheadCables ? "\u00fc" : "");
                    row.Cells[5].AddParagraph(activityRiskAssessment.AdverseWeather ? "\u00fc" : "");

                    row = table.AddRow();
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;
                    row.Borders.Top.Width = 0.5;
                    row.Borders.Top.Color = Colors.White;
                    row.Format.SpaceAfter = 0;
                    row.Cells[0].AddParagraph().AddFormattedText("Notes ", TextFormat.Bold);
                    row.Cells[0].MergeRight = 5;

                    row = table.AddRow();
                    row.Format.SpaceBefore = 0;
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;
                    row.Cells[0].AddParagraph(activityRiskAssessment.Notes ?? "");
                    row.Cells[0].MergeRight = 5;

                    section.AddLineSpacing(6);
                }

                section.AddLineSpacing(8);
            }

            section.AddPageBreak();

            // images
            table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.Format.LeftIndent = 5;
            table.Format.SpaceBefore = 6;
            table.Format.SpaceAfter = 6;
            table.Format.Font.Size = 14;
            table.Format.Font.Color = Colors.White;
            table.Shading.Color = Color.Parse("#257f44");

            table.AddColumn(sectionWidth);

            row = table.AddRow();
            par = row.Cells[0].AddParagraph($"Images ({model.Images.Count})");

            section.AddLineSpacing(8);

            table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.Format.Font.Size = 7;
            table.Format.LineSpacing = 10;
            table.Format.LineSpacingRule = LineSpacingRule.Exactly;

            table.AddColumn(sectionWidth * 0.235);
            table.AddColumn(sectionWidth * 0.02);
            table.AddColumn(sectionWidth * 0.235);
            table.AddColumn(sectionWidth * 0.02);
            table.AddColumn(sectionWidth * 0.235);
            table.AddColumn(sectionWidth * 0.02);
            table.AddColumn(sectionWidth * 0.235);

            for (var i = 0; i < model.Images.Count; i++)
            {
                var cellIndex = (i % 4) * 2;

                var imageModel = model.Images[i];
                base64 = "base64:" + Convert.ToBase64String(imageModel.Data);

                if (cellIndex == 0)
                {
                    if (i > 0)
                    {
                        row = table.AddRow();
                        row.HeightRule = RowHeightRule.Exactly;
                        row.Height = 8;
                        row.KeepWith = 1;
                    }

                    row = table.AddRow();
                }

                var cell = row.Cells[cellIndex];

                cell.Borders.Width = 0.5;
                cell.Borders.Color = Color.Parse("#a2a2a2");
                cell.Shading.Color = Color.Parse("#f2f2f2");

                par = cell.AddParagraph(imageModel.TaskType ?? "");
                par.Format.Font.Bold = true;
                par.Format.SpaceBefore = 1;

                image = cell.AddImage(base64);
                image.Width = (sectionWidth * 0.235) - 7;
                image.WrapFormat.DistanceTop = 3;
                image.WrapFormat.DistanceBottom = 2;

                par = cell.AddParagraph("Image No: ");
                par.Format.Font.Bold = true;
                par.AddFormattedText((i + 1).ToString(), TextFormat.NotBold);
                par.Format.AddTabStop(68);
                par.AddTab();

                foreach (var answer in model.Checklists.SelectMany(a => a.Answers))
                {
                    if (answer.FileIDs.Contains(imageModel.FileID))
                    {
                        answer.ImageNumbers.Add(i + 1);
                    }
                }

                if (GpsExtensions.IsLocationValid(imageModel.Longitude, imageModel.Latitude))
                {
                    par.AddFormattedText($"{imageModel.Latitude.Value.TruncateDigits(3).ToString("0.000")},{imageModel.Longitude.Value.TruncateDigits(3).ToString("0.000")}", TextFormat.NotBold);
                }

                if (!string.IsNullOrWhiteSpace(imageModel.RoadName))
                {
                    par = cell.AddParagraph("Road: ");
                    par.Format.Font.Bold = true;
                    par.AddFormattedText(imageModel.RoadName, TextFormat.NotBold);
                }

                par = cell.AddParagraph("Captured: ");
                par.Format.Font.Bold = true;
                par.AddFormattedText(imageModel.Captured?.ToString("dd/MM/yyyy HH:mm") ?? "", TextFormat.NotBold);

                par = cell.AddParagraph("Uploaded: ");
                par.Format.Font.Bold = true;
                par.AddFormattedText(imageModel.Uploaded?.ToString("dd/MM/yyyy HH:mm") ?? "", TextFormat.NotBold);

                if (!string.IsNullOrWhiteSpace(imageModel.Comments))
                {
                    par = cell.AddParagraph("Comments: ");
                    par.Format.Font.Bold = true;
                    par.AddFormattedText(imageModel.Comments ?? "", TextFormat.NotBold);
                }

                par.Format.SpaceAfter = 5;
            }

            section.AddPageBreak();

            // checklists
            table = section.AddTable();
            table.Rows.LeftIndent = 0;
            table.Format.LeftIndent = 5;
            table.Format.SpaceBefore = 6;
            table.Format.SpaceAfter = 6;
            table.Format.Font.Size = 14;
            table.Format.Font.Color = Colors.White;
            table.Shading.Color = Color.Parse("#257f44");

            table.AddColumn(sectionWidth);

            row = table.AddRow();
            par = row.Cells[0].AddParagraph($"Checklists ({model.Checklists.Count})");

            section.AddLineSpacing(8);

            for (var i = 0; i < model.Checklists.Count; i++)
            {
                var checklist = model.Checklists[i];

                if (i > 0)
                {
                    section.AddPageBreak();
                }

                // assessment type
                table = section.AddTable();
                table.Rows.LeftIndent = 0;
                table.Format.LeftIndent = 5;
                table.Format.SpaceBefore = 4;
                table.Format.SpaceAfter = 4;
                table.Shading.Color = Color.Parse("#f2f2f2");
                table.Borders.Right.Width = 0.5;
                table.Borders.Right.Color = Colors.White;

                table.AddColumn(sectionWidth * 0.30);
                table.AddColumn(sectionWidth * 0.30);
                table.AddColumn(sectionWidth * 0.40);

                row = table.AddRow();
                row.Format.Font.Color = Colors.White;
                row.Shading.Color = Color.Parse("#808080");
                row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                par = row.Cells[0].AddParagraph("Assessment Type");
                par = row.Cells[1].AddParagraph("Start Time");
                par = row.Cells[2].AddParagraph("Engineer");

                row = table.AddRow();
                row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                par = row.Cells[0].AddParagraph(checklist.AssessmentType ?? "");
                par = row.Cells[1].AddParagraph(checklist.StartTime != null ? checklist.StartTime.Value.ToOrdinalWords() + " @ " + checklist.StartTime.Value.ToString("HH:mm") : "");
                par = row.Cells[2].AddParagraph(checklist.Engineer ?? "");

                section.AddLineSpacing(8);

                // answers
                table = section.AddTable();
                table.Rows.LeftIndent = 0;
                table.Format.LeftIndent = 5;
                table.Format.SpaceBefore = 4;
                table.Format.SpaceAfter = 4;
                table.Shading.Color = Color.Parse("#f2f2f2");
                table.Borders.Right.Width = 0.5;
                table.Borders.Right.Color = Colors.White;

                table.AddColumn(sectionWidth * 0.38);
                table.AddColumn(sectionWidth * 0.12);
                table.AddColumn(sectionWidth * 0.38);
                table.AddColumn(sectionWidth * 0.12);

                row = table.AddRow();
                row.Format.Font.Color = Colors.White;
                row.Shading.Color = Color.Parse("#808080");
                row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                par = row.Cells[0].AddParagraph("Questions");
                par = row.Cells[1].AddParagraph("Answers");
                par = row.Cells[2].AddParagraph("Reason");
                par = row.Cells[3].AddParagraph("Image #");

                foreach (var answer in checklist.Answers)
                {
                    row = table.AddRow();
                    row.Cells[table.Columns.Count - 1].Borders.Right.Width = 0;

                    par = row.Cells[0].AddParagraph(answer.Question ?? "");
                    par = row.Cells[1].AddParagraph(answer.Answer == 0 ? "No" : answer.Answer == 1 ? "Yes" : "N/A");
                    par = row.Cells[2].AddParagraph(answer.Reason ?? "");
                    par = row.Cells[3].AddParagraph(string.Join(", ", answer.ImageNumbers.OrderBy(a => a)));
                }
            }

            // render
            return document.ToByteArray();
        }

        private void SetJobSheetHeader(HeaderFooter header, JobSheetReportModel model, bool includeLogo)
        {
            var sectionWidth = header.Section.Width();

            Table table = null;
            Row row = null;

            // generated date / page number
            var par = header.AddParagraph();
            par.Format.Font.Size = 9;
            par.Format.Font.Color = Color.Parse("#d5d5d5");
            par.Format.SpaceAfter = 16;
            par.Format.RightIndent = -15;
            par.Format.AddTabStop(sectionWidth - 48);
            par.AddText($"Report Generated: {model.GeneratedDate.ToOrdinalWords()} @ {model.GeneratedDate.ToString("HH:mm")}");
            par.AddTab();
            par.AddText("Page ");
            par.AddPageField();
            par.AddText(" of ");
            par.AddNumPagesField();

            // logos
            if (includeLogo)
            {
                table = header.AddTable();
                table.Rows.LeftIndent = 0;

                table.AddColumn(sectionWidth);

                var conwayLogoPath = HostingEnvironment.MapPath(@"~\Content\images\pdf-conway.png");

                row = table.AddRow();
                var image = row.Cells[0].AddImage(conwayLogoPath);
                image.WrapFormat.DistanceTop = 5;
                image.Height = 60;

                par = header.AddParagraph();
                par.Format.LineSpacingRule = LineSpacingRule.Exactly;
                par.Format.LineSpacing = Unit.FromMillimeter(0.0);
                par.Format.SpaceAfter = 17;
            }

            // work order
            table = header.AddTable();
            table.Rows.LeftIndent = 0;

            table.AddColumn(sectionWidth * 0.50);
            table.AddColumn(sectionWidth * 0.50);

            row = table.AddRow();
            row.Format.SpaceBefore = 22;
            row.Format.SpaceAfter = 22;
            row.Format.Font.Size = 18;
            row.Format.Font.Color = Colors.White;
            row.Shading.Color = Color.Parse("#257f44");

            par = row.Cells[0].AddParagraph("Work Order No:");
            par.Format.LeftIndent = 5;

            par = row.Cells[1].AddParagraph(model.WorkOrderNo ?? "");
            par.Format.Alignment = ParagraphAlignment.Right;
            par.Format.RightIndent = 5;

            if (includeLogo)
            {
                // the header primary height is set to page 2 height, for first page (logo) add additional spacing to main section
                header.Document.LastSection.AddLineSpacing(82);
            }
        }

        private void SetJobSheetFooter(HeaderFooter footer, JobSheetReportModel model)
        {
            var sectionWidth = footer.Section.Width();
            var techInfinityLogoPath = HostingEnvironment.MapPath(@"~\Content\images\pdf-techfinity.png");

            var table = footer.AddTable();
            table.Rows.LeftIndent = 0;

            table.AddColumn(sectionWidth * 0.13);
            table.AddColumn(sectionWidth * 0.87);

            var row = table.AddRow();

            var image = row.Cells[0].AddImage(techInfinityLogoPath);
            image.Height = 36;

            var par = row.Cells[1].AddParagraph(model.FooterText ?? "");
            par.Format.Font.Size = 7;
            par.Format.Font.Color = Color.Parse("#787878");
        }

        private byte[] GetJobSheetGoogleMapImage(decimal longitude, decimal latitude)
        {
            var googleMapsApiKey = WebConfigurationManager.AppSettings["google-maps-api-key"];
            byte[] bytes = null;

            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;

            try
            {
                using (var webClient = new WebClient())
                {
                    var url = $@"https://maps.googleapis.com/maps/api/staticmap?
                        center={latitude},{longitude}
                        &zoom=15
                        &size=600x300
                        &maptype=roadmap
                        &markers=color:red%7C{latitude},{longitude}
                        &key={googleMapsApiKey}";

                    bytes = webClient.DownloadData(url);
                }
            }
            catch (Exception ex)
            {
                _errorService.CreateErrorLog(nameof(GetJobSheetGoogleMapImage), ex);
            }

            return bytes;
        }

        #endregion

        #region Daily Diary
        public PagedResults<DailyDiaryReportModel> GetPagedActiveDailyDiary(int rowCount, int page, string query, DateTime? startDate)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);
            bool boolQuery = string.Compare(query, "Yes", true) == 0;
            bool isBoolQuery = new List<string> { "Yes", "No" }.Contains(query, StringComparer.OrdinalIgnoreCase);

            IQueryable<DailyDiaryReportModel> baseQuery = Context.WorkInstructions
                .Where(a => a.IsActive)
                .Where(a => a.JobFK != null)
                .Where(a => startDate == null || DbFunctions.TruncateTime(a.WorkInstructionStartDate) == startDate)
                .Where(a =>
                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                    (isIntQuery ? a.Job.JobNumber == intQuery : false) ||
                    a.Job.Contract.Customer.CustomerName.Contains(query) ||
                    a.Job.Contract.Employee.EmployeeName.Contains(query) ||
                    a.Job.Contract.ContractNumber.Contains(query) ||
                    a.Job.JobTitle.Contains(query) ||
                    (isDateQuery ? a.WorkInstructionStartDate == dateQuery : false) ||
                    a.SRWNumber.Contains(query) ||
                    a.Employee.EmployeeName.Contains(query) ||
                    (isBoolQuery ? a.IsScheduled == boolQuery : false) ||
                    a.Status.Status1.Contains(query) ||
                    (isBoolQuery ? a.IsSuccessful == boolQuery : false) ||
                    a.JobType.DisplayName.Contains(query) ||
                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK).Contains(query)
                    )
                .OrderBy(a => a.WorkInstructionStartDate)
                .Select(DailyDiaryReportModel.EntityToModel);

            return PagedResults<DailyDiaryReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region Timesheet Information
        public PagedResults<TimesheetInformationModel> GetTimesheetInformationPaged(int rowCount, int page, string query)
        {
            DateTime today = DateTime.Today;
            DateTime tomorrow = DateTime.Today.AddDays(1);
            DateTime startOfWeek = DateTime.Today.StartOfWeek(DayOfWeek.Monday);
            DateTime endOfWeek = startOfWeek.AddDays(6);
            DateTime startOfNextWeek = startOfWeek.AddDays(7);
            DateTime endOfNextWeek = startOfWeek.AddDays(13);

            IOrderedQueryable<TimesheetInformationModel> baseQuery = Context.Employees
                .ActiveEmployees()
                .Where(a =>
                    a.EmployeeName.Contains(query) ||
                    a.EmployeeType.EmployeeType1.Contains(query) ||
                    a.PhoneNumber.Contains(query)
                )
                .Select(a => new TimesheetInformationModel
                {
                    EmployeeID = a.EmployeePK,
                    EmployeeName = a.EmployeeName,
                    EmployeeType = a.EmployeeType.EmployeeType1,
                    EmployeePhoneNumber = a.PhoneNumber,
                    TasksToday = a.ScheduleEmployees.Where(b => b.IsActive && DbFunctions.TruncateTime(b.Schedule.StartDate) == today).Count(),
                    TasksTomorrow = a.ScheduleEmployees.Where(b => b.IsActive && DbFunctions.TruncateTime(b.Schedule.StartDate) == tomorrow).Count(),
                    TasksThisWeek = a.ScheduleEmployees.Where(b => b.IsActive && DbFunctions.TruncateTime(b.Schedule.StartDate) >= startOfWeek && DbFunctions.TruncateTime(b.Schedule.StartDate) <= endOfWeek).Count(),
                    TasksNextWeek = a.ScheduleEmployees.Where(b => b.IsActive && DbFunctions.TruncateTime(b.Schedule.StartDate) >= startOfNextWeek && DbFunctions.TruncateTime(b.Schedule.StartDate) <= endOfNextWeek).Count(),
                })
                .OrderBy(o => o.EmployeeName);

            return PagedResults<TimesheetInformationModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<TimesheetInformationDetailModel> GetTimesheetInformationDetailPaged(int rowCount, int page, string query, Guid employeeID)
        {
            DateTime dateQuery;
            int integerQuery = 0;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);
            bool isIntegerQuery = int.TryParse(query, out integerQuery);

            DateTime startOfWeek = DateTime.Today.StartOfWeek(DayOfWeek.Monday);
            DateTime endOfNextWeek = startOfWeek.AddDays(13);

            // get schedules
            List<TimesheetInformationBreakdownModel> schedules = Context.ScheduleEmployees
                .Where(a =>
                    a.IsActive &&
                    a.EmployeeFK == employeeID &&
                    DbFunctions.TruncateTime(a.Schedule.StartDate) >= startOfWeek &&
                    DbFunctions.TruncateTime(a.Schedule.StartDate) <= endOfNextWeek
                )
                .Select(a => new TimesheetInformationBreakdownModel
                {
                    WorkInstructionID = a.Schedule.WorkInstruction.WorkInstructionPK,

                    ScheduledDate = (DateTime)DbFunctions.TruncateTime(a.Schedule.StartDate),
                    Task = a.Schedule.WorkInstruction.WorkInstructionNumber,
                    Customer = a.Schedule.WorkInstruction.Job.Contract.Customer.CustomerName,
                    ContractNumber = a.Schedule.WorkInstruction.Job.Contract.ContractNumber,
                    ContractManager = a.Schedule.WorkInstruction.Job.Contract.Employee.EmployeeName,
                    JobTitle = a.Schedule.WorkInstruction.Job.JobTitle,
                    Description = a.Schedule.WorkInstruction.Job.Description,
                    StartDate = a.Schedule.StartDate,
                    JobType = a.Schedule.WorkInstruction.JobType.DisplayName,
                    OnSite = a.Schedule.WorkInstruction.Tasks.Where(b => b.Name == "Arrive on Site").Select(b => b.EndTime).FirstOrDefault(),
                    OffSite = a.Schedule.WorkInstruction.Tasks.Where(b => b.Name == "Leave Site").Select(b => b.EndTime).FirstOrDefault(),
                    TravelTime = a.Schedule.Shift.TimesheetRecordWorkInstructions.Select(b => b.TimesheetRecord).Select(b => (b.TravelHours * 60) + b.TravelMins).FirstOrDefault(),
                    Address = StoreFunctions.WorkInstructionAddressSpaceDelim(a.Schedule.WorkInstructionFK)
                })
                .ToList();

            // get fortnight in dates
            List<DateTime> fortnight = Enumerable.Range(0, 14).Select(a => startOfWeek.AddDays(a)).ToList();

            // combine lists
            IEnumerable<TimesheetInformationDetailModel> results = from day in fortnight
                                                                   join s in schedules on day equals s.ScheduledDate into sub
                                                                   from a in sub.DefaultIfEmpty()
                                                                   select new TimesheetInformationDetailModel
                                                                   {
                                                                       Day = day,
                                                                       Schedule = a
                                                                   };

            // filter
            results = results.Where(a =>
                a.Day.DayOfWeek.ToString().Contains(query, StringComparison.OrdinalIgnoreCase) ||
                (isDateQuery ? a.Day == dateQuery : false) ||
                (isIntegerQuery && a.Schedule != null ? a.Schedule.Task == integerQuery : false) ||
                (a.Schedule != null ? a.Schedule.Customer.Contains(query, StringComparison.OrdinalIgnoreCase) : false) ||
                (a.Schedule != null ? a.Schedule.ContractManager.Contains(query, StringComparison.OrdinalIgnoreCase) : false) ||
                (a.Schedule != null ? a.Schedule.JobTitle.Contains(query, StringComparison.OrdinalIgnoreCase) : false) ||
                (isDateQuery && a.Schedule != null ? a.Schedule.StartDate == dateQuery : false) ||
                (a.Schedule != null ? a.Schedule.JobType.Contains(query, StringComparison.OrdinalIgnoreCase) : false) ||
                (a.Schedule != null ? a.Schedule.Speed.Contains(query, StringComparison.OrdinalIgnoreCase) : false)
            );

            return PagedResults<TimesheetInformationDetailModel>.GetPagedResults(results, rowCount, page);
        }
        #endregion

        #region Weekly Timesheet
        public PagedResults<WeeklyTimesheetReportModel> GetPagedActiveWeeklyTimesheet(int rowCount, int page, string query, DateTime startDate, Guid employeeID)
        {
            DateTime endDate = startDate.AddDays(6);

            List<ShiftStaff> shiftStaffs = Context.ShiftStaffs.Where(a =>
                a.IsActive &&
                a.EmployeeFK == employeeID &&
                DbFunctions.TruncateTime(a.Shift.ShiftDate) >= startDate &&
                DbFunctions.TruncateTime(a.Shift.ShiftDate) <= endDate
                )
                .OrderBy(a => a.Shift.ShiftDate)
                .ToList();

            List<WeeklyTimesheetReportModel> results = new List<WeeklyTimesheetReportModel>();
            foreach (ShiftStaff shiftStaff in shiftStaffs)
            {
                WorkInstruction workInstruction = shiftStaff.Shift.JobPacks.SelectMany(a => a.WorkInstructions).Single();

                WeeklyTimesheetReportModel model = new WeeklyTimesheetReportModel();
                model.Name = workInstruction.Job.Contract.Customer.CustomerName;
                model.JobTitle = workInstruction.Job.JobTitle;
                model.ClockNo = "";

                int? minutesWorked = shiftStaff.Shift.TimesheetRecordWorkInstructions.Select(a => (a.TimesheetRecord.WorkedHours * 60) + (int?)a.TimesheetRecord.WorkedMins).FirstOrDefault();
                string timeWorked = minutesWorked != null ? TimeSpan.FromMinutes((int)minutesWorked).ToString(@"hh\:mm") : "";

                switch (workInstruction.WorkInstructionStartDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        model.Mon = timeWorked;
                        break;
                    case DayOfWeek.Tuesday:
                        model.Tues = timeWorked;
                        break;
                    case DayOfWeek.Wednesday:
                        model.Wed = timeWorked;
                        break;
                    case DayOfWeek.Thursday:
                        model.Thur = timeWorked;
                        break;
                    case DayOfWeek.Friday:
                        model.Fri = timeWorked;
                        break;
                    case DayOfWeek.Saturday:
                        model.Sat = timeWorked;
                        break;
                    case DayOfWeek.Sunday:
                        model.Sun = timeWorked;
                        break;
                }

                model.SleepOverHours = "";
                model.Appears = "";
                model.Hours = "";

                int? minutesTravelled = shiftStaff.Shift.TimesheetRecordWorkInstructions.Select(a => (a.TimesheetRecord.TravelHours * 60) + (int?)a.TimesheetRecord.TravelMins).FirstOrDefault();
                model.TravelTime = minutesWorked != null ? TimeSpan.FromMinutes((int)minutesTravelled).ToString(@"hh\:mm") : "";

                model.StandBy = "";
                model.Bonus = "";
                model.HolidayPayRequired = "";
                model.NoOfNightsSubsistence = "";

                results.Add(model);
            }

            return PagedResults<WeeklyTimesheetReportModel>.GetPagedResults(results, rowCount, page);
        }
        #endregion

        #region Planned Works
        public PagedResults<PlannedWorksReportModel> GetPagedActivePlannedWorks(int rowCount, int page, string query)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);
            bool boolQuery = string.Compare(query, "Yes", true) == 0;
            bool isBoolQuery = new List<string> { "Yes", "No" }.Contains(query, StringComparer.OrdinalIgnoreCase);

            IQueryable<PlannedWorksReportModel> baseQuery = Context.WorkInstructions
                .Where(a => a.IsActive)
                .Where(a => a.WorkInstructionStartDate >= DateTime.Today)
                .Where(a =>
                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                    (isIntQuery ? a.Job.JobNumber == intQuery : false) ||
                    a.Job.Contract.Customer.CustomerName.Contains(query) ||
                    a.Job.Contract.Employee.EmployeeName.Contains(query) ||
                    a.Job.Contract.ContractNumber.Contains(query) ||
                    a.Job.JobTitle.Contains(query) ||
                    (isDateQuery ? a.WorkInstructionStartDate == dateQuery : false) ||
                    a.SRWNumber.Contains(query) ||
                    a.Employee.EmployeeName.Contains(query) ||
                    (isBoolQuery ? a.IsScheduled == boolQuery : false) ||
                    a.Status.Status1.Contains(query) ||
                    (isBoolQuery ? a.IsSuccessful == boolQuery : false) ||
                    a.JobType.DisplayName.Contains(query) ||
                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK).Contains(query)
                    )
                .OrderBy(a => a.WorkInstructionStartDate)
                .Select(PlannedWorksReportModel.EntityToModel);

            return PagedResults<PlannedWorksReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region On Hire / Installed
        public PagedResults<OnHireInstalledReportModel> GetPagedActiveOnHireInstalled(int rowCount, int page, string query)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);

            IQueryable<OnHireInstalledReportModel> baseQuery = Context.vv_OnHireInstalledReport
                .Where(a =>
                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                    (isIntQuery ? a.JobNumber == intQuery : false) ||
                    a.JobTitle.Contains(query) ||
                    (isDateQuery ? a.StartDate == dateQuery : false) ||
                    a.EmployeeName.Contains(query) ||
                    a.EquipmentName.Contains(query) ||
                    a.Name.Contains(query) ||
                    a.AssetNumbers.Contains(query) ||
                    (isIntQuery ? a.Quantity == intQuery : false) ||
                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionID.Value).Contains(query)
                    )
                .OrderBy(a => a.StartDate)
                .ThenBy(a => a.WorkInstructionID)
                .ThenBy(a => a.AssetNumbers)
                .Select(a => new OnHireInstalledReportModel
                {
                    WorkInstructionID = a.WorkInstructionID.Value,
                    Task = a.WorkInstructionNumber.Value,
                    Job = a.JobNumber.Value,
                    JobTitle = a.JobTitle,
                    InstallDate = a.StartDate.Value,
                    IssuedBy = a.EmployeeName,
                    Equipment = a.EquipmentName,
                    Type = a.Name,
                    AssetNumbers = a.AssetNumbers,
                    Quantity = a.Quantity.Value,
                    CollectionTask = a.InstallID,
                    CollectionStatus = a.Status,
                    CollectionDate = a.CollectionDate,
                    Address = StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionID.Value)
                })
                .Union(Context.TaskEquipments
                .Where(a =>
                    ((isIntQuery ? a.Task.WorkInstruction.WorkInstructionNumber == intQuery : false) ||
                    (isIntQuery ? a.Task.WorkInstruction.Job.JobNumber == intQuery : false) ||
                    a.Task.WorkInstruction.Job.JobTitle.Contains(query) ||
                    (isDateQuery ? a.Task.WorkInstruction.WorkInstructionStartDate == dateQuery : false) ||
                    a.Task.WorkInstruction.Employee.EmployeeName.Contains(query) ||
                    a.Equipment.EquipmentName.Contains(query) ||
                    a.Equipment.EquipmentType.Name.Contains(query) ||
                    (isIntQuery ? a.Quantity == intQuery : false) ||
                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.Task.WorksInstructionFK).Contains(query)) &&
                    a.Task.WorkInstruction.JobType.JobType1 == "Install" &&
                    a.Equipment.EquipmentType.EquipmentTypeVmsOrAssetFK == null &&
                    a.Task.WorkInstruction.IsActive &&
                    a.Task.WorkInstruction.StatusFK != WorkInstructionStatusesEnum.Cancelled
                )
                .OrderBy(a => a.Task.WorkInstruction.WorkInstructionStartDate)
                .ThenBy(a => a.Task.WorkInstruction.WorkInstructionPK)
                .Select(a => new OnHireInstalledReportModel
                {
                    WorkInstructionID = a.Task.WorkInstruction.WorkInstructionPK,
                    Task = a.Task.WorkInstruction.WorkInstructionNumber,
                    Job = a.Task.WorkInstruction.Job.JobNumber,
                    JobTitle = a.Task.WorkInstruction.Job.JobTitle,
                    InstallDate = a.Task.WorkInstruction.WorkInstructionStartDate,
                    IssuedBy = a.Task.WorkInstruction.Employee.EmployeeName,
                    Equipment = a.Equipment.EquipmentName,
                    Type = a.Equipment.EquipmentType.Name,
                    AssetNumbers = "",
                    Quantity = a.Quantity,
                    CollectionTask = a.Task.WorkInstruction.InstallWIs.Select(s => s.WorkInstructionNumber).FirstOrDefault() == 0 ? (int?)null : a.Task.WorkInstruction.InstallWIs.Select(s => s.WorkInstructionNumber).FirstOrDefault(),
                    CollectionStatus = a.Task.WorkInstruction.InstallWIs.Select(s => s.Status.Status1).FirstOrDefault(),
                    CollectionDate = a.Task.WorkInstruction.InstallWIs.Select(s => s.WorkInstructionStartDate).FirstOrDefault() == default(DateTime) ? (DateTime?)null : a.Task.WorkInstruction.InstallWIs.Select(s => s.WorkInstructionStartDate).FirstOrDefault(),
                    Address = StoreFunctions.WorkInstructionAddressSpaceDelim(a.Task.WorksInstructionFK)
                })).OrderBy(o => o.Task).ThenBy(j => j.Job);

            return PagedResults<OnHireInstalledReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region Tracker
        public PagedResults<TrackerReportModel> GetPagedActiveTracker(int rowCount, int page, string query, Guid? contractID, DateTime? date)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);
            bool boolQuery = string.Compare(query, "Yes", true) == 0;
            bool isBoolQuery = new List<string> { "Yes", "No" }.Contains(query, StringComparer.OrdinalIgnoreCase);

            IQueryable<TrackerReportModel> baseQuery = Context.WorkInstructions
                .Where(a => a.IsActive)
                .Where(a => a.JobFK != null)
                .Where(a => contractID == null || a.Job.ContractFK == contractID)
                .Where(a => date == null || DbFunctions.TruncateTime(a.WorkInstructionStartDate) == date)
                .Where(a =>
                    (query == null || query == "") ||
                    a.Job.Contract.ContractNumber.Contains(query) ||
                    (isIntQuery ? a.Job.JobNumber == intQuery : false) ||
                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                    a.TrafficManagementActivities.Contains(query) ||
                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK).Contains(query) ||
                    a.Depot.DepotName.Contains(query) ||
                    a.Status.Status1.Contains(query) ||
                    (isDateQuery ? DbFunctions.TruncateTime(a.CreatedDate) == dateQuery : false) ||
                    a.Employee.EmployeeName.Contains(query) ||
                    (isDateQuery ? DbFunctions.TruncateTime(a.WorkInstructionStartDate) == dateQuery : false) ||
                    (isBoolQuery ? a.IsSuccessful == boolQuery : false) ||
                    a.ReasonNotes.Contains(query) ||
                    a.Comments.Contains(query)
                    )
                .OrderBy(a => a.WorkInstructionStartDate)
                .Select(TrackerReportModel.EntityToModel);

            return PagedResults<TrackerReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region Performance
        public PagedResults<PerformanceReportModel> GetPagedActivePerformance(int rowCount, int page, string query)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);
            bool boolQuery = string.Compare(query, "Yes", true) == 0;
            bool isBoolQuery = new List<string> { "Yes", "No" }.Contains(query, StringComparer.OrdinalIgnoreCase);

            // short cricut where clause if isBoolQuery to stop mainly the notes from interfering with results

            //Rebuilt to use view instead of this query ^

            IQueryable<PerformanceReportModel> baseQuery = Context.vv_PerformanceReport
                .Where(a => a.IsActive)
                .Where(a =>
                    (query == null || query == "") ||
                    (isBoolQuery ? a.IsSuccessful == boolQuery : (
                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                    (isIntQuery ? a.JobNumber == intQuery : false) ||
                    a.CustomerName.Contains(query) ||
                    a.EmployeeName.Contains(query) ||
                    a.ContractNumber.Contains(query) ||
                    a.JobTitle.Contains(query) ||
                    (isDateQuery ? DbFunctions.TruncateTime(a.WorkInstructionStartDate) == dateQuery : false) ||
                    a.JobType.Contains(query) ||
                    a.TrafficManagementActivities.Contains(query) ||
                    a.Foreman.Contains(query) ||
                    a.SchemeTitle.Contains(query) ||
                    a.Code.Contains(query) ||
                    a.ReasonNotes.Contains(query)
                    )))
                    .OrderBy(o => o.WorkInstructionStartDate)
                .Select(PerformanceReportModel.ViewToModel);

            return PagedResults<PerformanceReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

        #region Tasks

        public PagedResults<ActiveTasksModel> GetPagedActiveTasks(int rowCount, int page, string query)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);
            bool boolQuery = string.Compare(query, "Yes", true) == 0;
            bool isBoolQuery = new List<string> { "Yes", "No" }.Contains(query, StringComparer.OrdinalIgnoreCase);

            IQueryable<ActiveTasksModel> baseQuery = Context.WorkInstructions
                .Where(a => a.IsActive)
                .Where(a =>
                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                    (isIntQuery ? a.Job.JobNumber == intQuery : false) ||
                    a.Job.Contract.Customer.CustomerName.Contains(query) ||
                    a.Job.Contract.Employee.EmployeeName.Contains(query) ||
                    a.Job.Contract.ContractNumber.Contains(query) ||
                    a.Job.JobTitle.Contains(query) ||
                    (isDateQuery ? a.WorkInstructionStartDate == dateQuery : false) ||
                    a.SRWNumber.Contains(query) ||
                    a.Employee.EmployeeName.Contains(query) ||
                    (isBoolQuery ? a.IsScheduled == boolQuery : false) ||
                    a.Status.Status1.Contains(query) ||
                    (isBoolQuery ? a.IsSuccessful == boolQuery : false) ||
                    a.JobType.JobType1.Contains(query) ||
                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK).Contains(query)
                    )
                .OrderBy(a => a.WorkInstructionStartDate)
                .Select(ActiveTasksModel.EntityToModel);

            return PagedResults<ActiveTasksModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<WorkInstructionModel> GetActiveTasks()
        {
            return Context.WorkInstructions.Where(x => x.IsActive && x.JobFK != null && (x.StatusFK == WorkInstructionStatusesEnum.Complete || x.StatusFK == WorkInstructionStatusesEnum.Invoiced))
                                           .Select(WorkInstructionModel.EntityToModel).ToList();
        }

        public bool SetTasksToInvoiced(List<Guid> selectedTaskIDs, DateTime invoiceDate)
        {
            var selectedTasks = Context.WorkInstructions.Where(x => selectedTaskIDs.Contains(x.WorkInstructionPK)).ToList();

            if (selectedTasks != null)
            {
                using (var transaction = Context.Database.BeginTransaction())
                {
                    var id = default(Guid);
                    try
                    {
                        foreach (var task in selectedTasks)
                        {
                            id = task.WorkInstructionPK;
                            task.InvoiceDate = invoiceDate;
                            task.StatusFK = WorkInstructionStatusesEnum.Invoiced;

                            Context.SaveChanges();
                        }
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        //transaction.Rollback();
                        _errorService.CreateErrorLog("SetTaskInvoiceDate", e, id.ToString());
                        return false;
                    }
                    return true;
                }
            }
            return true;
        }
        #endregion

        #region Commercial
        public PagedResults<CommercialModel> GetPagedActiveCommercial(int rowCount, int page, string query)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);
            bool boolQuery = string.Compare(query, "Yes", true) == 0;
            bool isBoolQuery = new List<string> { "Yes", "No" }.Contains(query, StringComparer.OrdinalIgnoreCase);

            IQueryable<CommercialModel> test = Context.WorkInstructionPriceLines
                                .Where(a => a.IsActive && a.WorkInstruction.IsActive)
                                .Where(a =>
                                    (isIntQuery ? a.WorkInstruction.WorkInstructionNumber == intQuery : false) ||
                                    (isIntQuery ? a.WorkInstruction.Job.JobNumber == intQuery : false) ||
                                    a.WorkInstruction.Job.Contract.Customer.CustomerName.Contains(query) ||
                                    a.WorkInstruction.Job.Contract.Employee.EmployeeName.Contains(query) ||
                                    a.WorkInstruction.Job.Contract.ContractNumber.Contains(query) ||
                                    a.WorkInstruction.Job.JobTitle.Contains(query) ||
                                    (isDateQuery ? a.WorkInstruction.WorkInstructionStartDate == dateQuery : false) ||
                                    a.WorkInstruction.SRWNumber.Contains(query) ||
                                    a.WorkInstruction.Employee.EmployeeName.Contains(query) ||
                                    (isBoolQuery ? a.WorkInstruction.IsScheduled == boolQuery : false) ||
                                    a.WorkInstruction.Status.Status1.Contains(query) ||
                                    (isBoolQuery ? a.WorkInstruction.IsSuccessful == boolQuery : false) ||
                                    a.WorkInstruction.JobType.JobType1.Contains(query) ||
                                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionFK).Contains(query)
                                )
                                .OrderBy(a => a.WorkInstruction.WorkInstructionStartDate)
                                .ThenBy(a => a.WorkInstruction.WorkInstructionNumber)
                                .Select(CommercialModel.etm);

            IQueryable<CommercialModel> baseQuery = Context.WorkInstructions
                                .Where(a => a.IsActive)
                                .Where(a =>
                                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                                    (isIntQuery ? a.Job.JobNumber == intQuery : false) ||
                                    a.Job.Contract.Customer.CustomerName.Contains(query) ||
                                    a.Job.Contract.Employee.EmployeeName.Contains(query) ||
                                    a.Job.Contract.ContractNumber.Contains(query) ||
                                    a.Job.JobTitle.Contains(query) ||
                                    (isDateQuery ? a.WorkInstructionStartDate == dateQuery : false) ||
                                    a.SRWNumber.Contains(query) ||
                                    a.Employee.EmployeeName.Contains(query) ||
                                    (isBoolQuery ? a.IsScheduled == boolQuery : false) ||
                                    a.Status.Status1.Contains(query) ||
                                    (isBoolQuery ? a.IsSuccessful == boolQuery : false) ||
                                    a.JobType.JobType1.Contains(query) ||
                                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK).Contains(query)
                                    )
                                .OrderBy(a => a.WorkInstructionStartDate)
                                .Select(CommercialModel.EntityToModel);

            return PagedResults<CommercialModel>.GetPagedResults(test, rowCount, page);
        }

        #endregion

        #region On Hire
        public PagedResults<OnHireReportModel> GetPagedActiveOnHire(int rowCount, int page, string query)
        {
            int intQuery;
            bool isIntQuery = int.TryParse(query, out intQuery);
            DateTime dateQuery;
            bool isDateQuery = DateTime.TryParse(query, out dateQuery);

            IQueryable<OnHireReportModel> baseQuery = Context.TaskEquipmentAssets
                .Where(a =>
                    a.TaskEquipment.Task.WorkInstruction.JobType.JobType1 == JobTypesEnum.Install // &&
                                                                                                  //!a.TaskEquipment.Task.WorkInstruction.InstallWIs.Any(b => b.IsActive && b.StatusFK == WorkInstructionStatusesEnum.Complete)
                    )
                .Where(a =>
                    (isIntQuery ? a.TaskEquipment.Task.WorkInstruction.WorkInstructionNumber == intQuery : false) ||
                    (isIntQuery ? a.TaskEquipment.Task.WorkInstruction.Job.JobNumber == intQuery : false) ||
                    a.TaskEquipment.Task.WorkInstruction.Job.JobTitle.Contains(query) ||
                    (isDateQuery ? a.TaskEquipment.Task.WorkInstruction.WorkInstructionStartDate == dateQuery : false) ||
                    a.TaskEquipment.Task.WorkInstruction.Employee.EmployeeName.Contains(query) ||
                    a.TaskEquipment.Equipment.EquipmentName.Contains(query) ||
                    a.TaskEquipment.Equipment.EquipmentType.Name.Contains(query) ||
                    a.AssetNumber.Contains(query) ||
                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.TaskEquipment.Task.WorksInstructionFK).Contains(query)
                    )
                .OrderBy(a => a.TaskEquipment.Task.WorkInstruction.WorkInstructionStartDate)
                .ThenBy(a => a.TaskEquipment.Task.WorksInstructionFK)
                .ThenBy(a => a.ItemNumber).Select(OnHireReportModel.ETM);

            return PagedResults<OnHireReportModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        #endregion

        public int GetWorkInstructionNumber(Guid workInstructionID)
        {
            return Context.WorkInstructions.Where(a => a.WorkInstructionPK == workInstructionID).Select(a => a.WorkInstructionNumber).Single();
        }
    }
}
