﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using System.Security.Authentication;
using FMConway.Services.ErrorLogging.Models;

namespace FMConway.Services.ErrorLogging.Services
{
    public class ErrorLoggingService : Service, FMConway.Services.ErrorLogging.Interfaces.IErrorLoggingService
    {
        UserContext _userContext;

        public ErrorLoggingService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public void CreateErrorLog(string description, Exception exception, string id = null)
        {
            try
            {
                string errorMessage = _userContext.Name + " \n\n" + exception.ToString();

                if (!string.IsNullOrEmpty(id))
                {
                    errorMessage += " \n\n " + id;
                }

                ErrorLog error = new ErrorLog();

                error.LogPK = Guid.NewGuid();
                error.Location = "Desktop";
                error.Description = description;
                error.Exception = errorMessage;
                error.CreatedDate = DateTime.Now;

                Context.ErrorLogs.Add(error);

                SaveChanges();
            }
            catch (Exception e)            
            {
                ErrorLog error = new ErrorLog();

                error.LogPK = Guid.NewGuid();
                error.Location = "Desktop";
                error.Description = "CreateError";
                error.Exception = e.ToString() ;
                error.CreatedDate = DateTime.Now;

                Context.ErrorLogs.Add(error);

                SaveChanges();
            }
        }
    }
}
