﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ErrorLogging.Models
{
    public class ErrorModel
    {
        public Guid LogPK { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string Exception { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
