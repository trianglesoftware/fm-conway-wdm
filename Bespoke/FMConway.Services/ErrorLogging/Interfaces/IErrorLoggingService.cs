﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ErrorLogging.Interfaces
{
    public interface IErrorLoggingService : IService
    {
        void CreateErrorLog(string description, Exception exception, string id);
    }
}
