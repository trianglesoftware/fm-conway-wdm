//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FMConway.Services
{
    using System;
    using System.Collections.Generic;
    
    public partial class Observation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Observation()
        {
            this.ObservationPhotos = new HashSet<ObservationPhoto>();
        }
    
        public System.Guid ObservationPK { get; set; }
        public Nullable<System.Guid> ShiftFK { get; set; }
        public string PeopleInvolved { get; set; }
        public string Location { get; set; }
        public string ObservationDescription { get; set; }
        public string CauseOfProblem { get; set; }
        public System.DateTime ObservationDate { get; set; }
        public bool IsActive { get; set; }
        public System.Guid CreatedByID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.Guid UpdatedByID { get; set; }
        public System.DateTime UpdatedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ObservationPhoto> ObservationPhotos { get; set; }
        public virtual Shift Shift { get; set; }
    }
}
