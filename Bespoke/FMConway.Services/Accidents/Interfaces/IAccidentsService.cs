﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Accidents.Models;

namespace FMConway.Services.Accidents.Interfaces
{
    public interface IAccidentsService : IService
    {
        PagedResults<AccidentModel> GetAccidentsPaged(int rowCount, int page, string query, Guid shiftID);

        PagedResults<AccidentModel> GetAccidentsForJobPaged(int rowCount, int page, string query, Guid jobID);

        PagedResults<AccidentPhotoModel> GetAccidentPhotosPaged(int rowCount, int page, string query, Guid accidentID);

        PagedResults<AccidentModel> GetAccidentsWithShifts(int rowCount, int page, string query);

        PagedResults<AccidentModel> GetAccidentsWithoutShifts(int rowCount, int page, string query);

        AccidentModel SingleAccident(Guid accidentID);

    }
}
