﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Accidents.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using System.Security.Authentication;
using FMConway.Services.Accidents.Interfaces;
using FMConway.Services.Accidents.Extensions;

namespace FMConway.Services.Accidents.Services
{
    public class AccidentsService : Service, FMConway.Services.Accidents.Interfaces.IAccidentsService
    {
        UserContext _userContext;

        public AccidentsService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public PagedResults<AccidentModel> GetAccidentsPaged(int rowCount, int page, string query, Guid shiftID)
        {
            var baseQuery = Context.Accidents.Where(a => a.ShiftFK.Value.Equals(shiftID) && a.IsActive).OrderBy(t => t.AccidentDate).AccidentOverviewQuery(query).Select(AccidentModel.EntityToModel);

            return PagedResults<AccidentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<AccidentModel> GetAccidentsForJobPaged(int rowCount, int page, string query, Guid jobID)
        {
            var baseQuery = Context.Accidents.Where(a => a.Shift.JobPacks.Any(jp => jp.WorkInstructions.Any(wi => wi.JobFK == jobID))).OrderBy(a => a.AccidentDate).AccidentOverviewQuery(query).Select(AccidentModel.EntityToModel);

            return PagedResults<AccidentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<AccidentModel> GetAccidentsWithShifts(int rowCount, int page, string query)
        {
            var baseQuery = Context.Accidents.Where(a => a.ShiftFK.HasValue).OrderByDescending(a => a.AccidentDate).AccidentOverviewQuery(query).Select(AccidentModel.EntityToModel);

            return PagedResults<AccidentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<AccidentModel> GetAccidentsWithoutShifts(int rowCount, int page, string query)
        {
            var baseQuery = Context.Accidents.Where(a => !a.ShiftFK.HasValue).OrderByDescending(a => a.AccidentDate).AccidentOverviewQuery(query).Select(AccidentModel.EntityToModel);

            return PagedResults<AccidentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<AccidentPhotoModel> GetAccidentPhotosPaged(int rowCount, int page, string query, Guid accidentID)
        {
            var baseQuery = Context.AccidentPhotos.Where(a => a.AccidentFK.Equals(accidentID) && a.IsActive).OrderBy(a => a.CreatedDate).AccidentPhotoOverviewQuery(query).Select(AccidentPhotoModel.ETM);

            return PagedResults<AccidentPhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public AccidentModel SingleAccident(Guid accidentID)
        {
            return Context.Accidents.Where(a => a.AccidentPK.Equals(accidentID) && a.IsActive).Select(AccidentModel.EntityToModel).SingleOrDefault();
        }
    }
}
