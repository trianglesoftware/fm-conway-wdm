﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Accidents.Extensions
{
    public static class AccidentsExtensions
    {
        public static IQueryable<Accident> AccidentOverviewQuery(this IQueryable<Accident> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.AccidentDescription.Contains(query) || bq.Location.Contains(query));
            }

            return model;
        }

        public static IQueryable<AccidentPhoto> AccidentPhotoOverviewQuery(this IQueryable<AccidentPhoto> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.File.FileDownloadName.Contains(query));
            }

            return model;
        }
    }
}
