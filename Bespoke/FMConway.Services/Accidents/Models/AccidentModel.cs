﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Accidents.Models
{
    public class AccidentModel
    {
        public Guid AccidentID { get; set; }
        public string AccidentDescription { get; set; }
        public DateTime AccidentDate { get; set; }
        public string Registrations { get; set; }
        public string Location { get; set; }
        public string PersonsReporting { get; set; }
        public string PeopleInvolved { get; set; }
        public string Witnesses { get; set; }
        public string ActionTaken { get; set; }
        public Guid? ShiftID { get; set; }
        public bool IsActive { get; set; }

        public static Expression<Func<Accident, AccidentModel>> EntityToModel = e => new AccidentModel() 
        {
            AccidentID = e.AccidentPK,
            AccidentDescription = e.AccidentDescription,
            AccidentDate = e.AccidentDate,
            Registrations = e.Registrations,
            PersonsReporting = e.PersonReporting,
            PeopleInvolved = e.PeopleInvolved,
            Witnesses = e.Witnesses,
            ShiftID = e.ShiftFK,
            Location = e.Location,
            ActionTaken = e.ActionTaken,
            IsActive = e.IsActive
        };
    }

    public class AccidentPhotoModel
    {
        public Guid AccidentID { get; set; }
        public Guid AccidentPhotoID { get; set; }
        public Guid FileID { get; set; }
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }

        public static Expression<Func<AccidentPhoto, AccidentPhotoModel>> ETM = e => new AccidentPhotoModel() 
        {
            AccidentID = e.AccidentFK,
            AccidentPhotoID = e.AccidentPhotoPK,
            FileID = e.FileFK,
            FileName = e.File.FileDownloadName,
            Date = e.CreatedDate,
            Longitude = e.File.Longitude ?? 0.00m,
            Latitude = e.File.Latitude ?? 0.00m
        }; 
    }
}
