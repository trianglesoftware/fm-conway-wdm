﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.JobPacks.Models
{
    public class JobPackModel
    {
        public Guid JobPackID { get; set; }
        public Guid? ShiftID { get; set; }
        public string JobPackName { get; set; }

        public static Expression<Func<JobPack, JobPackModel>> EntityToModel = e => new JobPackModel 
        {
            JobPackID = e.JobPackPK,
            ShiftID = e.ShiftFK.Value,
            JobPackName = e.JobPackName
        };
    }
}
