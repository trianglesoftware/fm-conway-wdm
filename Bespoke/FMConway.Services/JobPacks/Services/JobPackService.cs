﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.JobPacks.Models;
using FMConway.Services.JobPacks.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using System.Security.Authentication;

namespace FMConway.Services.JobPacks.Services
{
    public class JobPackService : Service, FMConway.Services.JobPacks.Interfaces.IJobPackService
    {
        UserContext _userContext;

        public JobPackService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid CreateJobPack(string jobPackName)
        {
            JobPack jp = new JobPack();
            jp.JobPackPK = Guid.NewGuid();
            jp.JobPackName = jobPackName;
            jp.IsActive = true;
            jp.CreatedByID = _userContext.UserDetails.ID;
            jp.CreatedDate = DateTime.Now;
            jp.UpdatedByID = _userContext.UserDetails.ID;
            jp.UpdatedDate = DateTime.Now;

            Context.JobPacks.Add(jp);

            return jp.JobPackPK;
        }
    }
}
