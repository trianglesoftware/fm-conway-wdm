﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Collections.Paging;
using FMConway.Services.JobPacks.Models;

namespace FMConway.Services.JobPacks.Interfaces
{
    public interface IJobPackService : IService
    {
        Guid CreateJobPack(string jobPackName);
       

    }
}
