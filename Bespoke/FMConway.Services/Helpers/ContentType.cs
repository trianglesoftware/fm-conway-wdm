﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Helpers
{
    public class ContentType
    {
        public static string Pdf = "application/pdf";
        public static string Csv = "text/csv";
        public static string Xls = "application/vnd.ms-excel";
        public static string Xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }
}
