﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Helpers
{
    public class StopwatchHelper
    {
        public static long TimeThis(Action code)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            code();
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }
    }
}
