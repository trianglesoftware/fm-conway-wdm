﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Helpers
{
    public class ServiceResults<T>
    {
        public T Result { get; set; }
        public List<ServiceError> Errors { get; set; }

        public ServiceResults()
        {
            Errors = new List<ServiceError>();
        }
    }

    public class ServiceResults : ServiceResults<bool>
    {

    }

    public class ServiceError
    {
        public string Key { get; set; }
        public string Error { get; set; }
    }

    public static class ServiceResultExtensions
    {
        public static void Add(this List<ServiceError> serviceErrors, string key, string error)
        {
            serviceErrors.Add(new ServiceError
            {
                Key = key,
                Error = error
            });
        }

        public static void Add(this List<ServiceError> serviceErrors, string error)
        {
            serviceErrors.Add("", error);
        }
    }
}
