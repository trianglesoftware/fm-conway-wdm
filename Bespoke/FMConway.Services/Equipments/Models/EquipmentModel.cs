﻿using FMConway.Services.EquipmentTypes.Models;
using FMConway.Services.Files.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.Equipments.Models
{
    public class EquipmentModel
    {
        public Guid EquipmentID { get; set; }
        [Required(ErrorMessage = "Equipment Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Equipment Type is required")]
        public Guid EquipmentTypeID { get; set; }
        public EquipmentTypeModel EquipmentType { get; set; }
        public Guid? FileID { get; set; }
        public FileModel File { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Equipment, EquipmentModel>> EntityToModel = entity => new EquipmentModel
        {
            EquipmentID = entity.EquipmentPK,
            Name = entity.EquipmentName,
            EquipmentTypeID = entity.EquipmentTypeFK,
            EquipmentType = new EquipmentTypeModel()
            {
                Name = entity.EquipmentType.Name
            },
            FileID = entity.FileFK,
            File = new FileModel()
            {
                Data = entity.File.Data
            },

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<EquipmentModel, Equipment> ModelToEntity = model => new Equipment
        {
            EquipmentPK = model.EquipmentID,
            EquipmentName = model.Name,
            EquipmentTypeFK = model.EquipmentTypeID,
            FileFK = model.FileID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
