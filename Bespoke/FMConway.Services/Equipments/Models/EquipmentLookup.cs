﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Equipments.Models
{
    public class EquipmentLookup : LookupBase<Guid>
    {
        public string EquipmentType { get; set; }

        public static Expression<Func<Equipment, EquipmentLookup>> EntityToModel = entity => new EquipmentLookup()
        {
            Key = entity.EquipmentName,
            Value = entity.EquipmentPK,
            EquipmentType = entity.EquipmentType.Name
        };
    }
}
