﻿using FMConway.Services.Equipments.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Equipments.Extensions;
using System.Web;
using FMConway.Services.LoadingSheets.Models;

namespace FMConway.Services.Equipments.Services
{
    public class EquipmentService : Service, FMConway.Services.Equipments.Interfaces.IEquipmentService
    {
        UserContext _userContext;

        public EquipmentService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(EquipmentModel model, HttpPostedFileBase image)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            if (image != null)
            {
                File newImage = new File();
                newImage.FilePK = Guid.NewGuid();
                newImage.FileDownloadName = image.FileName;

                byte[] imageData = new byte[image.ContentLength];
                image.InputStream.Read(imageData, 0, imageData.Length);

                newImage.Data = imageData;
                newImage.ContentType = image.ContentType;
                newImage.IsActive = true;
                newImage.CreatedByFK = _userContext.UserDetails.ID;
                newImage.CreatedDate = DateTime.Now;
                newImage.UpdatedByFK = _userContext.UserDetails.ID;
                newImage.UpdatedDate = DateTime.Now;

                Context.Files.Add(newImage);

                model.FileID = newImage.FilePK;
            }

            model.EquipmentID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Equipments.Add(EquipmentModel.ModelToEntity(model));

            SaveChanges();

            return model.EquipmentID;
        }

        public void Update(EquipmentModel model, HttpPostedFileBase image)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Equipment current = Context.Equipments.Where(x => x.EquipmentPK.Equals(model.EquipmentID)).Single();

            if (image != null)
            {

                if (current.FileFK.HasValue)
                {
                    Context.Files.Remove(current.File);
                }

                File newImage = new File();
                newImage.FilePK = Guid.NewGuid();
                newImage.FileDownloadName = image.FileName;

                byte[] imageData = new byte[image.ContentLength];
                image.InputStream.Read(imageData, 0, imageData.Length);

                newImage.Data = imageData;
                newImage.ContentType = image.ContentType;
                newImage.IsActive = true;
                newImage.CreatedByFK = _userContext.UserDetails.ID;
                newImage.CreatedDate = DateTime.Now;
                newImage.UpdatedByFK = _userContext.UserDetails.ID;
                newImage.UpdatedDate = DateTime.Now;

                Context.Files.Add(newImage);

                current.FileFK = newImage.FilePK;
            }

            current.EquipmentName = model.Name == null ? "" : model.Name;
            current.EquipmentTypeFK = model.EquipmentTypeID;
            
            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public void UpdateQuantity(EquipmentModel model, Guid loadingSheetID, int sheetOrder, decimal quantity)
        {
            var equipment = Context.LoadingSheetEquipments.Where(x => x.EquipmentFK == model.EquipmentID && x.LoadingSheetFK == loadingSheetID && x.SheetOrder == sheetOrder).SingleOrDefault();

            if (equipment != null) 
            {
                using (var tran = Context.Database.BeginTransaction())
                {
                    try
                    {
                        equipment.Quantity = quantity;
                        
                        SaveChanges();
                        tran.Commit();
                    }
                    catch (Exception e)
                    {

                    }
                }
                
            }
            SaveChanges();
        }

        public EquipmentModel Single(Guid ID)
        {
            return Context.Equipments.Where(a => a.EquipmentPK.Equals(ID)).Select(EquipmentModel.EntityToModel).SingleOrDefault();
        }

        public byte[] GetImageData(Guid ID)
        {
            var equipment = Context.Equipments.Where(a => a.EquipmentPK.Equals(ID)).SingleOrDefault();
            if (equipment.File != null)
                return equipment.File.Data;
            else
                return new byte[0];
        }

        public List<EquipmentModel> GetAllActiveEquipment()
        {
            return Context.Equipments.ActiveEquipment().Select(EquipmentModel.EntityToModel).ToList();
        }

        public PagedResults<EquipmentModel> GetActiveEquipmentPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Equipments.ActiveEquipment().OrderBy(o => o.EquipmentName).EquipmentOverviewQuery(query).Select(EquipmentModel.EntityToModel);

            return PagedResults<EquipmentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<LoadingSheetEquipmentModel> GetActiveLoadingSheetEquipmentPaged(int rowCount, int page, string query, Guid loadingSheetID)
        {
            var baseQuery = Context.LoadingSheetEquipments.Where(x => x.IsActive && (x.Equipment.EquipmentName.Contains(query) || x.Equipment.EquipmentType.Name.Contains(query) || x.NonStandardEquipment.Contains(query) || (x.IsNonStandard ? "Non Standard".Contains(query) : false)) && x.LoadingSheetFK.Equals(loadingSheetID)).OrderBy(o => o.SheetOrder).Select(x => new LoadingSheetEquipmentModel()
            {
                Name = x.IsNonStandard ? x.NonStandardEquipment : x.Equipment.EquipmentName,
                EquipmentType = new EquipmentTypes.Models.EquipmentTypeModel()
                {
                    Name = x.IsNonStandard ? "Non Standard" : x.Equipment.EquipmentType.Name
                },
                Quantity = x.Quantity,
                LoadingSheetEquipmentID = x.LoadingSheetEquipmentPK,
                EquipmentID = x.EquipmentFK,
                SheetOrder = x.SheetOrder
            });

            return PagedResults<LoadingSheetEquipmentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<LoadingSheetEquipmentModel> GetActiveLoadingSheetTemplateEquipmentPaged(int rowCount, int page, string query, Guid loadingSheetTemplateID)
        {
            var baseQuery = Context.LoadingSheetTemplateEquipments
                .Where(x => x.IsActive && (x.Equipment.EquipmentName.Contains(query) || x.Equipment.EquipmentType.Name.Contains(query) || x.NonStandardEquipment.Contains(query) || 
                    (x.IsNonStandard ? "Non Standard".Contains(query) : false)) && x.LoadingSheetTemplateFK.Equals(loadingSheetTemplateID)).OrderBy(o => o.SheetOrder)
                .Select(x => new LoadingSheetEquipmentModel()
                {
                    Name = x.IsNonStandard ? x.NonStandardEquipment : x.Equipment.EquipmentName,
                    EquipmentType = new EquipmentTypes.Models.EquipmentTypeModel()
                    {
                        Name = x.IsNonStandard ? "Non Standard" : x.Equipment.EquipmentType.Name
                    },
                    Quantity = x.Quantity,
                    LoadingSheetEquipmentID = x.LoadingSheetTemplateEquipmentPK,
                    EquipmentID = x.EquipmentFK
                });

            return PagedResults<LoadingSheetEquipmentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<EquipmentModel> GetActiveEquipmentNotInLSPaged(int rowCount, int page, string query, Guid loadingSheetID)
        {
            var loadingSheetEquipment = Context.LoadingSheetEquipments.Where(x => x.IsActive && x.LoadingSheetFK.Equals(loadingSheetID)).Select(x => x.EquipmentFK).ToList();

            var baseQuery = Context.Equipments.Where(x => !loadingSheetEquipment.Any(y => y == x.EquipmentPK)).ActiveEquipment().OrderBy(o => o.EquipmentName).EquipmentOverviewQuery(query).Select(EquipmentModel.EntityToModel);

            return PagedResults<EquipmentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<EquipmentModel> GetActiveEquipmentNotInLSTemplatePaged(int rowCount, int page, string query, Guid loadingSheetTemplateID)
        {
            var loadingSheetEquipment = Context.LoadingSheetTemplateEquipments.Where(x => x.IsActive && x.LoadingSheetTemplateFK.Equals(loadingSheetTemplateID)).Select(x => x.EquipmentFK).ToList();

            var baseQuery = Context.Equipments.Where(x => !loadingSheetEquipment.Any(y => y == x.EquipmentPK)).ActiveEquipment().OrderBy(o => o.EquipmentName).EquipmentOverviewQuery(query).Select(EquipmentModel.EntityToModel);

            return PagedResults<EquipmentModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        
        public List<EquipmentModel> GetAllInactiveEquipment()
        {
            return Context.Equipments.InactiveEquipment().Select(EquipmentModel.EntityToModel).ToList();
        }

        public decimal GetLoadingSheetQuantity(Guid equipmentID, Guid loadingSheetID, int sheetOrder)
        {
            return Context.LoadingSheetEquipments.Where(e => e.LoadingSheetFK == loadingSheetID && e.EquipmentFK == equipmentID && e.SheetOrder == sheetOrder).Select(s => s.Quantity).SingleOrDefault();
        }

        public PagedResults<EquipmentModel> GetInactiveEquipmentPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Equipments.InactiveEquipment().OrderBy(o => o.EquipmentName).EquipmentOverviewQuery(query).Select(EquipmentModel.EntityToModel);

            return PagedResults<EquipmentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<EquipmentLookup> GetPagedActiveEquipmentLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Equipments.ActiveEquipment().OrderBy(o => o.EquipmentName).EquipmentOverviewQuery(query).Select(EquipmentLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<EquipmentLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public int? GetLoadingSheetEquipmentLastOrderNumber(Guid loadingSheetID)
        {
            return Context.LoadingSheetEquipments.Where(a => a.LoadingSheetFK == loadingSheetID && a.IsActive).Select(a => (int?)a.SheetOrder).OrderByDescending(a => a).Take(1).FirstOrDefault();
        }

    }
}
