﻿using FMConway.Services.Equipments.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using FMConway.Services.LoadingSheets.Models;

namespace FMConway.Services.Equipments.Interfaces
{
    public interface IEquipmentService : IService
    {
        Guid Create(EquipmentModel model, HttpPostedFileBase image);
        void Update(EquipmentModel model, HttpPostedFileBase image);
        EquipmentModel Single(Guid ID);
        byte[] GetImageData(Guid ID);

        List<EquipmentModel> GetAllActiveEquipment();
        PagedResults<EquipmentModel> GetActiveEquipmentPaged(int rowCount, int page, string query);
        PagedResults<LoadingSheetEquipmentModel> GetActiveLoadingSheetEquipmentPaged(int rowCount, int page, string query, Guid loadingSheetID);
        PagedResults<LoadingSheetEquipmentModel> GetActiveLoadingSheetTemplateEquipmentPaged(int rowCount, int page, string query, Guid loadingSheetTemplateID);
        PagedResults<EquipmentModel> GetActiveEquipmentNotInLSPaged(int rowCount, int page, string query, Guid loadingSheetID);
        PagedResults<EquipmentModel> GetActiveEquipmentNotInLSTemplatePaged(int rowCount, int page, string query, Guid loadingSheetTemplateID);

        List<EquipmentModel> GetAllInactiveEquipment();
        PagedResults<EquipmentModel> GetInactiveEquipmentPaged(int rowCount, int page, string query);

        PagedResults<EquipmentLookup> GetPagedActiveEquipmentLookup(int rowCount, int page, string query);
        int? GetLoadingSheetEquipmentLastOrderNumber(Guid id);
        decimal GetLoadingSheetQuantity(Guid id, Guid loadingSheetID, int sheetOrder);
        void UpdateQuantity(EquipmentModel equipment, Guid loadingSHeetID, int sheetOrder, decimal quantity);
    }
}
