﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Equipments.Extensions
{
    public static class EquipmentExtensions
    {
        public static IQueryable<Equipment> ActiveEquipment(this IQueryable<Equipment> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<Equipment> InactiveEquipment(this IQueryable<Equipment> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<Equipment> EquipmentOverviewQuery(this IQueryable<Equipment> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.EquipmentName.Contains(query) ||
                                          bq.EquipmentType.Name.Contains(query));
            }

            return model;
        }
    }
}
