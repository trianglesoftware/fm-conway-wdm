﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Collections.Validation
{
    public class ModelValidation
    {
        public Dictionary<string, string> Errors { get; set; }

        public bool IsValid { get { return !Errors.Any(); } }

        public ModelValidation()
        {
            Errors = new Dictionary<string, string>();
        }

    }
}
