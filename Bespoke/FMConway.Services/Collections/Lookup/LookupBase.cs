﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Collections.Lookup
{
    public class LookupBase<TValue>
    {
        public string Key { get; set; }
        public TValue Value { get; set; }
    }
}
