﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Collections.Paging
{
    public class PagedResults<T>
    {

        public int Page { get; set; }
        public int PageCount { get; set; }
        public List<T> Data { get; set; }

        public static PagedResults<T> GetPagedResults(IQueryable<T> baseQuery, int rowCount, int page)
        {
            int recordCount = baseQuery.Count();

            var queryResults = baseQuery.Skip(rowCount * page).Take(rowCount);

            List<T> results = queryResults.ToList();

            return new PagedResults<T>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = results
            };
        }

        public static PagedResults<T> GetPagedResults(IEnumerable<T> baseQuery, int rowCount, int page)
        {
            int recordCount = baseQuery.Count();

            var queryResults = baseQuery.Skip(rowCount * page).Take(rowCount);

            List<T> results = queryResults.ToList();

            return new PagedResults<T>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = results
            };
        }

    }
}
