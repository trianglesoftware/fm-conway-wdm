﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Employees.Extensions
{
    public static class EmployeeExtensions
    {
        public static IQueryable<Employee> ActiveEmployees(this IQueryable<Employee> model)
        {
            return model.Where(a => a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Employee> InactiveEmployees(this IQueryable<Employee> model)
        {
            return model.Where(a => !a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Employee> EmployeeOverviewQuery(this IQueryable<Employee> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.EmployeeName.Contains(query) ||
                                          bq.PhoneNumber.Contains(query) ||
                                          bq.EmailAddress.Contains(query) ||
                                          bq.EmployeeType.EmployeeType1.Contains(query));
            }

            return model;
        }
    }
}
