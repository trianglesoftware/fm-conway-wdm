﻿using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.Employees.Services
{
    public class EmployeeTypeService : Service, FMConway.Services.Employees.Interfaces.IEmployeeTypeService
    {
        UserContext _userContext;

        public EmployeeTypeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<EmployeeTypeModel> GetEmployeeTypes()
        {
            return Context.EmployeeTypes.OrderBy(x => x.EmployeeType1).Select(EmployeeTypeModel.EntityToModel).ToList();
        }
    }
}
