﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Employees.Extensions;
using FMConway.Services.Employees.Interfaces;
using System.Data.Entity;
using FMConway.Services.Extensions;

namespace FMConway.Services.Employees.Services
{
    public class EmployeeService : Service, FMConway.Services.Employees.Interfaces.IEmployeeService
    {
        UserContext _userContext;

        public IEmployeeTypeService Types { get; set; }

        public EmployeeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(EmployeeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            if (model.VehicleID != null)
            {
                var otherEmployeesWithVehicle = Context.Employees.Where(a => a.VehicleFK == model.VehicleID).ToList();
                foreach (var employee in otherEmployeesWithVehicle)
                {
                    employee.VehicleFK = null;
                }
            }

            model.EmployeeID = Guid.NewGuid();
            model.AgencyName = model.AgencyName == null ? "" : model.AgencyName;
            model.PhoneNumber = model.PhoneNumber == null ? "" : model.PhoneNumber;
            model.EmailAddress = model.EmailAddress == null ? "" : model.EmailAddress;
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Employees.Add(EmployeeModel.ModelToEntity(model));

            SaveChanges();

            return model.EmployeeID;
        }

        public void Update(EmployeeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            if (model.VehicleID != null)
            {
                var otherEmployeesWithVehicle = Context.Employees.Where(a => a.VehicleFK == model.VehicleID && a.EmployeePK != model.EmployeeID).ToList();
                foreach (var employee in otherEmployeesWithVehicle)
                {
                    employee.VehicleFK = null;
                }
            }

            var current = Context.Employees.Where(x => x.EmployeePK.Equals(model.EmployeeID)).Single();

            current.EmployeeTypeFK = model.EmployeeTypeID;
            current.EmployeeName = model.EmployeeName == null ? "" : model.EmployeeName;
            current.PhoneNumber = model.PhoneNumber == null ? "" : model.PhoneNumber;
            current.EmailAddress = model.EmailAddress == null ? "" : model.EmailAddress;
            current.AgencyStaff = model.AgencyStaff;
            current.AgencyName = model.AgencyName == null ? "" : model.AgencyName;
            current.IsCurrentlyEmployeed = model.IsCurrentlyEmployeed;
            current.DepotFK = model.DepotID;
            current.VehicleFK = model.VehicleID;
            current.StartDate = model.StartDate;
            current.EndDate = model.EndDate;
            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public EmployeeModel Single(Guid ID)
        {
            return Context.Employees.Where(a => a.EmployeePK.Equals(ID)).Select(EmployeeModel.EntityToModel).SingleOrDefault();
        }

        public List<EmployeeModel> GetAllActiveEmployees()
        {
            return Context.Employees.ActiveEmployees().Select(EmployeeModel.EntityToModel).ToList();
        }

        public PagedResults<EmployeeModel> GetActiveEmployeesPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Employees.ActiveEmployees().OrderBy(o => o.EmployeeName).EmployeeOverviewQuery(query).Select(EmployeeModel.EntityToModel);

            return PagedResults<EmployeeModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<EmployeeModel> GetAllInactiveEmployees()
        {
            return Context.Employees.InactiveEmployees().Select(EmployeeModel.EntityToModel).ToList();
        }

        public List<EmployeeModel> GetAllActiveScheduleEmployees(DateTime day)
        {
            var employeeTypeIDs = new List<string>
            {
                "Crew Foreman",
                "Crew Member",
                "Supervisor",
                "Qualified Operative"
            };

            day = day.Date;
            var nextDay = day.AddDays(1);

            var employees = Context.Employees
                .ActiveEmployees()
                .Where(a => employeeTypeIDs.Contains(a.EmployeeType.EmployeeType1))
                .Select(EmployeeModel.EntityToModel)
                .OrderBy(e => e.EmployeeName)
                .ToList();

            foreach (var employee in employees)
            {
                employee.Absences = Context.sp_EmployeeAbsences(employee.EmployeeID).Where(a => a >= day && a < nextDay).Select(a => (DateTime)a).ToList();
            }

            return employees;
        }
        public List<EmployeeModel> GetAllActiveScheduleEmployeesForDay(DateTime day)
        {
            var employeeTypeIDs = new List<string>
            {
                "Crew Foreman",
                "Crew Member",
                "Supervisor",
                "Qualified Operative"
            };

  

            var employees = Context.Employees
                .ActiveEmployees()
                .Where(a => employeeTypeIDs.Contains(a.EmployeeType.EmployeeType1))
                .Select(EmployeeModel.EntityToModel)
                .OrderBy(e => e.EmployeeName)
                .ToList();

            foreach (var employee in employees)
            {
                employee.Absences = Context.sp_EmployeeAbsences(employee.EmployeeID).Where(a => a == day).Select(a => (DateTime)a).ToList();
            }

            return employees;
        }
        public PagedResults<EmployeeModel> GetInactiveEmployeesPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Employees.InactiveEmployees().OrderBy(o => o.EmployeeName).EmployeeOverviewQuery(query).Select(EmployeeModel.EntityToModel);

            return PagedResults<EmployeeModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<EmployeeLookup> GetPagedActiveEmployeesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Employees.ActiveEmployees().OrderBy(o => o.EmployeeName).EmployeeOverviewQuery(query).Select(EmployeeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<EmployeeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public PagedResults<EmployeeLookup> GetPagedActiveEmployeeUsersLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Employees.ActiveEmployees().Where(e => e.UserProfiles.Count.Equals(0)).OrderBy(o => o.EmployeeName).EmployeeOverviewQuery(query).Select(EmployeeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<EmployeeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public PagedResults<EmployeeLookup> GetPagedActiveFilteredEmployeesLookup(int rowCount, int page, string query, Guid? areaID, Guid? jobID, Guid? certificateTypeID, bool isAgencyStaff)
        {
            var employeeCertificates = Context.EmployeeCertificates.Where(x => x.CertificateTypeFK.Equals(certificateTypeID.Value)).Select(x => x.EmployeeFK);
            

            var baseQuery = Context.Employees.Where(x => x.AgencyStaff.Equals(isAgencyStaff)).ActiveEmployees().OrderBy(o => o.EmployeeName).EmployeeOverviewQuery(query).Select(EmployeeLookup.EntityToModel);
                        
            int recordCount = baseQuery.Count();

            return new PagedResults<EmployeeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(page * rowCount).Take(rowCount).ToList()
            };
        }

        public PagedResults<EmployeeLookup> GetPagedActiveContractManagersLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Employees.ActiveEmployees().Where(x => x.EmployeeType.IsContractManager).OrderBy(o => o.EmployeeName).EmployeeOverviewQuery(query).Select(EmployeeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<EmployeeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public PagedResults<EmployeeLookup> GetPagedActiveEmployeesLookupByType(int rowCount, int page, string query, Guid type)
        {
            var baseQuery = Context.Employees.ActiveEmployees().Where(x => x.EmployeeType.EmployeeTypePK.Equals(type)).OrderBy(o => o.EmployeeName).EmployeeOverviewQuery(query).Select(EmployeeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<EmployeeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public string GetEmployeeName(Guid employeeID)
        {
            return Context.Employees.Where(a => a.EmployeePK == employeeID).Select(a => a.EmployeeName).Single();
        }

        #region Employee Absence
        public void CreateEmployeeAbsence(EmployeeAbsenceModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.EmployeeAbsenceID = Guid.NewGuid();

            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.EmployeeAbsences.Add(EmployeeAbsenceModel.ModelToEntity(model));

            SaveChanges();
        }

        public void UpdateEmployeeAbsence(EmployeeAbsenceModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var employeeAbsence = Context.EmployeeAbsences.Where(a => a.EmployeeAbsencePK == model.EmployeeAbsenceID).Single();

            employeeAbsence.EmployeeFK = model.EmployeeID;
            employeeAbsence.EmployeeAbsenceReasonFK = (Guid)model.EmployeeAbsenceReasonID;
            employeeAbsence.FromDate = (DateTime)model.FromDate;
            employeeAbsence.ToDate = (DateTime)model.ToDate;
            employeeAbsence.IsActive = model.IsActive;
            employeeAbsence.UpdatedByFK = _userContext.UserDetails.ID;
            employeeAbsence.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public EmployeeAbsenceModel GetEmployeeAbsence(Guid employeeAbsenceID)
        {
            return Context.EmployeeAbsences.Where(a => a.EmployeeAbsencePK == employeeAbsenceID).Select(EmployeeAbsenceModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<EmployeeAbsenceModel> GetActiveEmployeeAbsencesPaged(int rowCount, int page, string query, Guid employeeID)
        {
            DateTime dateQuery;
            var isDateQuery = DateTime.TryParse(query, out dateQuery);

            var baseQuery = Context.EmployeeAbsences.Where(a => 
                a.IsActive && (
                a.EmployeeAbsenceReason.Code.Contains(query) ||
                (isDateQuery ? dateQuery >= a.FromDate && dateQuery <= a.ToDate : false)
                )).OrderByDescending(a => a.FromDate).Select(EmployeeAbsenceModel.EntityToModel);

            return PagedResults<EmployeeAbsenceModel>.GetPagedResults(baseQuery, rowCount, page);
        }
        #endregion

    }
}
