﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Employees.Models
{
    public class EmployeeTypeModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<EmployeeType, EmployeeTypeModel>> EntityToModel = entity => new EmployeeTypeModel()
        {
            ID = entity.EmployeeTypePK,
            Name = entity.EmployeeType1,
            
            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<EmployeeTypeModel, EmployeeType> ModelToEntity = model => new EmployeeType()
        {
            EmployeeTypePK = model.ID,
            EmployeeType1 = model.Name,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
