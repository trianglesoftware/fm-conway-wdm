﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Employees.Models
{
    public class EmployeeLookup : LookupBase<Guid>
    {
        public static Expression<Func<Employee, EmployeeLookup>> EntityToModel = entity => new EmployeeLookup()
        {
            Key = entity.EmployeeName,
            Value = entity.EmployeePK
        };
    }
}
