﻿using FMConway.Services.Depots.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Employees.Models
{
    public class EmployeeModel
    {
        public Guid EmployeeID { get; set; }
        [DisplayName("Employee Type")]
        [Required(ErrorMessage = "The Employee Type is required")]
        public Guid EmployeeTypeID { get; set; }
        public EmployeeTypeModel EmployeeType { get; set; }
        [DisplayName("Employee Name"), Required]
        public string EmployeeName { get; set; }
        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool AgencyStaff { get; set; }
        public string AgencyName { get; set; }
        public bool IsCurrentlyEmployeed { get; set; }
        [Required(ErrorMessage = "Depot is required")]
        public Guid DepotID { get; set; }
        public DepotModel Depot { get; set; }
        public Guid? VehicleID { get; set; }
        public string Vehicle { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public List<Guid> CertificateTypeID { get; set; }
        public List<DateTime> Absences { get; set; }

        public static Expression<Func<Employee, EmployeeModel>> EntityToModel = entity => new EmployeeModel
        {
            EmployeeID = entity.EmployeePK,
            EmployeeTypeID = entity.EmployeeTypeFK,
            EmployeeType = new EmployeeTypeModel()
            {
                Name = entity.EmployeeType.EmployeeType1
            },
            EmployeeName = entity.EmployeeName,
            PhoneNumber = entity.PhoneNumber,
            EmailAddress = entity.EmailAddress,
            AgencyStaff = entity.AgencyStaff,
            AgencyName = entity.AgencyName,
            IsCurrentlyEmployeed = entity.IsCurrentlyEmployeed,
            DepotID = entity.DepotFK,
            Depot = new DepotModel()
            {
                DepotName = entity.Depot.DepotName
            },
            VehicleID = entity.VehicleFK,
            Vehicle = entity.Vehicle.Registration,
            Username = entity.UserProfiles.Take(1).Select(u => u.User.UserName).FirstOrDefault() ?? "",
            StartDate = entity.StartDate,
            EndDate = entity.EndDate,
            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate,
            CertificateTypeID = entity.EmployeeCertificates.Where(a => a.IsActive).Select(a => a.CertificateTypeFK).ToList(),
        };

        public static Func<EmployeeModel, Employee> ModelToEntity = model => new Employee
        {
            EmployeePK = model.EmployeeID,
            EmployeeTypeFK = model.EmployeeTypeID,
            EmployeeName = model.EmployeeName,
            PhoneNumber = model.PhoneNumber,
            EmailAddress = model.EmailAddress,
            AgencyStaff = model.AgencyStaff,
            AgencyName = model.AgencyName,
            IsCurrentlyEmployeed = model.IsCurrentlyEmployeed,
            DepotFK = model.DepotID,
            VehicleFK = model.VehicleID,
            StartDate = model.StartDate,
            EndDate = model.EndDate,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
