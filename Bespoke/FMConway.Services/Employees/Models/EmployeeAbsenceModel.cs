﻿using FMConway.Services.Depots.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Employees.Models
{
    public class EmployeeAbsenceModel
    {
        public Guid EmployeeAbsenceID { get; set; }
        public Guid EmployeeID { get; set; }
        [Display(Name = "Employee Name")]
        public string Employee { get; set; }
        [Required]
        [Display(Name = "Absence Reason")]
        public Guid? EmployeeAbsenceReasonID { get; set; }
        public string EmployeeAbsenceReason { get; set; }
        [Required]
        [Display(Name = "From Date")]
        public DateTime? FromDate { get; set; }
        [Required]
        [Display(Name = "To Date")]
        public DateTime? ToDate { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<EmployeeAbsence, EmployeeAbsenceModel>> EntityToModel = a => new EmployeeAbsenceModel
        {
            EmployeeAbsenceID = a.EmployeeAbsencePK,
            EmployeeID = a.EmployeeFK,
            Employee = a.Employee.EmployeeName,
            EmployeeAbsenceReasonID = a.EmployeeAbsenceReasonFK,
            EmployeeAbsenceReason = a.EmployeeAbsenceReason.Code,
            FromDate = a.FromDate,
            ToDate = a.ToDate,
            IsActive = a.IsActive,
            CreatedByID = a.CreatedByFK,
            CreatedDate = a.CreatedDate,
            UpdatedByID = a.UpdatedByFK,
            UpdatedDate = a.UpdatedDate,
        };

        public static Func<EmployeeAbsenceModel, EmployeeAbsence> ModelToEntity = a => new EmployeeAbsence
        {
            EmployeeAbsencePK = a.EmployeeAbsenceID,
            EmployeeFK = a.EmployeeID,
            EmployeeAbsenceReasonFK = (Guid)a.EmployeeAbsenceReasonID,
            FromDate = (DateTime)a.FromDate,
            ToDate = (DateTime)a.ToDate,
            IsActive = a.IsActive,
            CreatedByFK = a.CreatedByID,
            CreatedDate = a.CreatedDate,
            UpdatedByFK = a.UpdatedByID,
            UpdatedDate = a.UpdatedDate,
        };
    }
}
