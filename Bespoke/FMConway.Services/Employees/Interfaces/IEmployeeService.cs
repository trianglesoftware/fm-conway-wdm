﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Employees.Interfaces
{
    public interface IEmployeeService : IService
    {
        IEmployeeTypeService Types { get; set; }

        Guid Create(EmployeeModel model);
        void Update(EmployeeModel model);
        EmployeeModel Single(Guid ID);

        List<EmployeeModel> GetAllActiveEmployees();
        PagedResults<EmployeeModel> GetActiveEmployeesPaged(int rowCount, int page, string query);

        List<EmployeeModel> GetAllActiveScheduleEmployees(DateTime weekStart);
        List<EmployeeModel> GetAllActiveScheduleEmployeesForDay(DateTime day);
        List<EmployeeModel> GetAllInactiveEmployees();
        PagedResults<EmployeeModel> GetInactiveEmployeesPaged(int rowCount, int page, string query);
        
        PagedResults<EmployeeLookup> GetPagedActiveEmployeesLookup(int rowCount, int page, string query);
        PagedResults<EmployeeLookup> GetPagedActiveEmployeeUsersLookup(int rowCount, int page, string query);
        PagedResults<EmployeeLookup> GetPagedActiveFilteredEmployeesLookup(int rowCount, int page, string query, Guid? areaID, Guid? jobID, Guid? certificateTypeID, bool isAgencyStaff);

        PagedResults<EmployeeLookup> GetPagedActiveContractManagersLookup(int rowCount, int page, string query);
        PagedResults<EmployeeLookup> GetPagedActiveEmployeesLookupByType(int rowCount, int page, string query, Guid type);

        void CreateEmployeeAbsence(EmployeeAbsenceModel model);
        void UpdateEmployeeAbsence(EmployeeAbsenceModel model);
        EmployeeAbsenceModel GetEmployeeAbsence(Guid employeeAbsenceID);
        PagedResults<EmployeeAbsenceModel> GetActiveEmployeeAbsencesPaged(int rowCount, int page, string query, Guid employeeID);
        string GetEmployeeName(Guid employeeID);
    }
}
