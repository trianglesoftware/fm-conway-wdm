﻿using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Employees.Interfaces
{
    public interface IEmployeeTypeService : IService
    {
        List<EmployeeTypeModel> GetEmployeeTypes();
    }
}
