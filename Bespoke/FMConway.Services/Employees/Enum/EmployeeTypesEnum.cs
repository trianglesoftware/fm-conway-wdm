﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Employees.Enum
{
    public class EmployeeTypesEnum
    {
        public static string OperationManager = "Operation Manager";
        public static string Administrator = "Administrator";
        public static string ContractManager = "Contract Manager";
        public static string CrewForeman = "Crew Foreman";
        public static string CrewMember = "Crew Member";
    }
}
