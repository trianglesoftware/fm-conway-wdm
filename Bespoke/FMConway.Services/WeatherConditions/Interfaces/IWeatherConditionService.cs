﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.WeatherConditions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WeatherConditions.Interfaces
{
    public interface IWeatherConditionService : IService
    {
        void Create(WeatherConditionModel model);
        List<WeatherConditionModel> GetActiveWeatherConditions();
        List<WeatherConditionModel> GetInactiveWeatherConditions();
        WeatherConditionModel Single(Guid id);
        void Update(WeatherConditionModel model);

        PagedResults<WeatherConditionLookup> GetPagedActiveWeatherConditionsLookup(int rowCount, int page, string query);
    }
}
