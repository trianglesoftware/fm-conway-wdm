﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WeatherConditions.Extensions
{
    public static class WeatherConditionExtensions
    {
        public static IQueryable<WeatherCondition> ActiveWeatherConditions(this IQueryable<WeatherCondition> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<WeatherCondition> InactiveWeatherConditions(this IQueryable<WeatherCondition> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<WeatherCondition> WeatherConditionOverviewQuery(this IQueryable<WeatherCondition> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
