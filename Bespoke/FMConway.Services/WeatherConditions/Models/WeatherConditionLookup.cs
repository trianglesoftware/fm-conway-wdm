﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WeatherConditions.Models
{
    public class WeatherConditionLookup : LookupBase<Guid>
    {
        public string Description { get; set; }

        public static Expression<Func<WeatherCondition, WeatherConditionLookup>> EntityToModel = entity => new WeatherConditionLookup
        {
            Value = entity.WeatherConditionPK,
            Key = entity.Name,
        };
    }
}
