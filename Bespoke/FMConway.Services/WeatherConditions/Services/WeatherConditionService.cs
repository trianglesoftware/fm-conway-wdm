﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.WeatherConditions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.WeatherConditions.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.WeatherConditions.Services
{
    public class WeatherConditionService : Service, FMConway.Services.WeatherConditions.Interfaces.IWeatherConditionService
    {
        UserContext _userContext;

        public WeatherConditionService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<WeatherConditionModel> GetActiveWeatherConditions()
        {
            return Context.WeatherConditions.Where(a => a.IsActive).Select(WeatherConditionModel.EntityToModel).ToList();
        }

        public List<WeatherConditionModel> GetInactiveWeatherConditions()
        {
            return Context.WeatherConditions.Where(a => !a.IsActive).Select(WeatherConditionModel.EntityToModel).ToList();
        }

        public void Create(WeatherConditionModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.WeatherConditionID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.WeatherConditions.Add(WeatherConditionModel.ModelToEntity(model));

            SaveChanges();
        }

        public void Update(WeatherConditionModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var current = Context.WeatherConditions.Where(x => x.WeatherConditionPK.Equals(model.WeatherConditionID)).SingleOrDefault();
            current.Name = model.Name;
            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;

            SaveChanges();
        }

        public WeatherConditionModel Single(Guid id)
        {
            return Context.WeatherConditions.Where(a => a.WeatherConditionPK.Equals(id)).Select(WeatherConditionModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<WeatherConditionLookup> GetPagedActiveWeatherConditionsLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.WeatherConditions.ActiveWeatherConditions().OrderBy(o => o.Name).WeatherConditionOverviewQuery(query).Select(WeatherConditionLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<WeatherConditionLookup>
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
