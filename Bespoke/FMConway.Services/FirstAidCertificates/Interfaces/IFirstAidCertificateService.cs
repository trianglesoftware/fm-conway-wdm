﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.FirstAidCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.FirstAidCertificates.Interfaces
{
    public interface IFirstAidCertificateService : IService
    {
        IFirstAidCertificateDocumentService Documents { get; set; }

        Guid Create(FirstAidCertificateModel model);
        Guid Update(FirstAidCertificateModel model);
        PagedResults<FirstAidCertificateModel> GetActiveFirstAidCertificatesPaged(int rowCount, int page, string query, Guid contractID);
        FirstAidCertificateModel Single(Guid id);
    }
}
