﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.FirstAidCertificates.Extensions
{
    public static class FirstAidCertificateExtensions
    {
        public static IQueryable<EmployeeCertificate> ActiveFirstAidCertificates(this IQueryable<EmployeeCertificate> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<EmployeeCertificate> FirstAidCertificateOverviewQuery(this IQueryable<EmployeeCertificate> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query) ||
                                          bq.Description.Contains(query) ||
                                          bq.CertificateType.Name.Contains(query));
            }

            return model;
        }
    }
}
