﻿using FMConway.Services.CertificateTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.FirstAidCertificates.Models
{
    public class FirstAidCertificateModel
    {
        public Guid FirstAidCertificateID { get; set; }
        public string CertificateName { get; set; }
        public string CertificateDescription { get; set; }
        public DateTime ExpiryDate { get; set; }
        public Guid CertificateTypeID { get; set; }
        public CertificateTypeModel CertificateType { get; set; }
        public Guid EmployeeID { get; set; }
        public bool IsFirstAid { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<EmployeeCertificate, FirstAidCertificateModel>> EntityToModel = entity => new FirstAidCertificateModel
        {
            FirstAidCertificateID = entity.EmployeeCertificatePK,
            CertificateName = entity.Name,
            CertificateDescription = entity.Description,
            ExpiryDate = entity.ExpiryDate,
            CertificateTypeID = entity.CertificateTypeFK,
            CertificateType = new CertificateTypeModel()
            {
                Name = entity.CertificateType.Name
            },
            EmployeeID = entity.EmployeeFK,
            IsFirstAid = entity.IsFirstAid,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<FirstAidCertificateModel, EmployeeCertificate> ModelToEntity = model => new EmployeeCertificate
        {
            EmployeeCertificatePK = model.FirstAidCertificateID,
            Name = model.CertificateName,
            Description = model.CertificateDescription,
            ExpiryDate = model.ExpiryDate,
            CertificateTypeFK = model.CertificateTypeID,
            EmployeeFK = model.EmployeeID,
            IsFirstAid = model.IsFirstAid,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
