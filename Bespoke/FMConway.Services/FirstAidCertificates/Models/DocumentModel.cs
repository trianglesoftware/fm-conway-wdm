﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.FirstAidCertificates.Models
{
    public class DocumentModel
    {
        public Guid DocumentID { get; set; }
        public Guid FirstAidCertificateID { get; set; }
        public string Title { get; set; }
        public Guid FileID { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<CertificateDocument, DocumentModel>> EntityToModel = entity => new DocumentModel()
        {
            DocumentID = entity.CertificateDocumentPK,
            Title = entity.Title,
            FirstAidCertificateID = entity.EmployeeCertificateFK,
            FileID = entity.FileFK,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<DocumentModel, CertificateDocument> ModelToEntity = model => new CertificateDocument()
        {
            CertificateDocumentPK = model.DocumentID,
            Title = model.Title,
            EmployeeCertificateFK = model.FirstAidCertificateID,
            FileFK = model.FileID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
