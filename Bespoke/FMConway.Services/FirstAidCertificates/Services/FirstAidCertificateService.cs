﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.FirstAidCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.FirstAidCertificates.Extensions;
using FMConway.Services.FirstAidCertificates.Interfaces;

namespace FMConway.Services.FirstAidCertificates.Services
{
    public class FirstAidCertificateService : Service, FMConway.Services.FirstAidCertificates.Interfaces.IFirstAidCertificateService
    {
        UserContext _userContext;

        public IFirstAidCertificateDocumentService Documents { get; set; }

        public FirstAidCertificateService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(FirstAidCertificateModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.FirstAidCertificateID = Guid.NewGuid();
            model.CertificateName = model.CertificateName == null ? "" : model.CertificateName;
            model.CertificateDescription = model.CertificateDescription == null ? "" : model.CertificateDescription;
            model.IsFirstAid = true;
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.EmployeeCertificates.Add(FirstAidCertificateModel.ModelToEntity(model));

            SaveChanges();

            return model.FirstAidCertificateID;
        }

        public Guid Update(FirstAidCertificateModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            EmployeeCertificate current = Context.EmployeeCertificates.Where(x => x.EmployeeCertificatePK.Equals(model.FirstAidCertificateID)).SingleOrDefault();

            current.Name = model.CertificateName == null ? "" : model.CertificateName;
            current.Description = model.CertificateDescription == null ? "" : model.CertificateDescription;
            current.CertificateTypeFK = model.CertificateTypeID;
            current.ExpiryDate = model.ExpiryDate;
            current.EmployeeFK = model.EmployeeID;
            current.IsFirstAid = true;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            Context.SaveChanges();

            return model.FirstAidCertificateID;
        }

        public PagedResults<FirstAidCertificateModel> GetActiveFirstAidCertificatesPaged(int rowCount, int page, string query, Guid employeeID)
        {
            var baseQuery = Context.EmployeeCertificates.Where(x => x.EmployeeFK.Equals(employeeID) && x.IsFirstAid).ActiveFirstAidCertificates().OrderBy(o => o.Name).FirstAidCertificateOverviewQuery(query).Select(FirstAidCertificateModel.EntityToModel);

            return PagedResults<FirstAidCertificateModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public FirstAidCertificateModel Single(Guid id)
        {
            return Context.EmployeeCertificates.Where(x => x.EmployeeCertificatePK.Equals(id)).Select(FirstAidCertificateModel.EntityToModel).SingleOrDefault();
        }
    }
}
