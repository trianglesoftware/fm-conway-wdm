﻿using FMConway.Services.FirstAidCertificates.Models;
using FMConway.Services.Files.Interface;
using FMConway.Services.Files.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.FirstAidCertificates.Services
{
    public class FirstAidCertificateDocumentService : Service, FMConway.Services.FirstAidCertificates.Interfaces.IFirstAidCertificateDocumentService
    {
        UserContext _userContext;
        IFileService _fileService;

        public FirstAidCertificateDocumentService(UserContext userContext, IFileService fileService)
        {
            _userContext = userContext;
            _fileService = fileService;
        }

        public List<DocumentModel> GetActiveDocumentsForFirstAidCertificate(Guid certificateID)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            List<DocumentModel> documents = Context.CertificateDocuments.Where(jd => jd.EmployeeCertificateFK.Equals(certificateID) && jd.IsActive).Select(DocumentModel.EntityToModel).ToList();

            return documents;
        }

        public DocumentModel GetDocument(Guid id)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.CertificateDocuments.Where(jd => jd.CertificateDocumentPK.Equals(id)).Select(DocumentModel.EntityToModel).SingleOrDefault();

            return document;
        }

        public void Create(Guid certificateID, string title, string fileName, string contentType, byte[] documentData)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var file = new FileModel()
            {
                FileDownloadName = fileName,
                ContentType = contentType,
                Data = documentData
            };

            FileModel newFile = _fileService.Add(file);

            DocumentModel document = new DocumentModel()
            {
                DocumentID = Guid.NewGuid(),
                FirstAidCertificateID = certificateID,
                FileID = newFile.ID,
                Title = title,
                IsActive = true,
                CreatedByID = _userContext.UserDetails.ID,
                CreatedDate = DateTime.Now,
                UpdatedByID = _userContext.UserDetails.ID,
                UpdatedDate = DateTime.Now
            };

            Context.CertificateDocuments.Add(DocumentModel.ModelToEntity(document));

            SaveChanges();
        }

        public void Update(DocumentModel document)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            CertificateDocument current = Context.CertificateDocuments.Where(x => x.CertificateDocumentPK.Equals(document.DocumentID)).SingleOrDefault();

            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            current.IsActive = document.IsActive;

            SaveChanges();
        }

        public void Inactivate(Guid id)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.CertificateDocuments.Where(jd => jd.CertificateDocumentPK.Equals(id)).Select(DocumentModel.EntityToModel).SingleOrDefault();
            if (document != null)
            {
                document.IsActive = false;
                Update(document);
            }
        }
    }
}
