﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.PriceLines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.PriceLines.Interfaces
{
    public interface IPriceLineService : IService
    {
        void Create(PriceLineModel model);
        List<PriceLineModel> GetActivePriceLines();
        List<PriceLineModel> GetInactivePriceLines();
        PriceLineModel Single(Guid id);
        void Update(PriceLineModel model);

        PagedResults<PriceLineLookup> GetPagedActivePriceLinesLookup(int rowCount, int page, string query);
    }
}
