﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.PriceLines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.PriceLines.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.PriceLines.Services
{
    public class PriceLineService : Service, FMConway.Services.PriceLines.Interfaces.IPriceLineService
    {
        UserContext _userContext;

        public PriceLineService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<PriceLineModel> GetActivePriceLines()
        {
            return Context.PriceLines.Where(a => a.IsActive).Select(PriceLineModel.EntityToModel).ToList();
        }

        public List<PriceLineModel> GetInactivePriceLines()
        {
            return Context.PriceLines.Where(a => !a.IsActive).Select(PriceLineModel.EntityToModel).ToList();
        }

        public void Create(PriceLineModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.PriceLineID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.PriceLines.Add(PriceLineModel.ModelToEntity(model));

            SaveChanges();
        }

        public void Update(PriceLineModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var current = Context.PriceLines.Where(x => x.PriceLinePK.Equals(model.PriceLineID)).SingleOrDefault();
            current.Code = model.Code;
            current.Description = model.Description;
            current.DefaultRate = model.DefaultRate;
            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;

            SaveChanges();
        }

        public PriceLineModel Single(Guid id)
        {
            return Context.PriceLines.Where(a => a.PriceLinePK.Equals(id)).Select(PriceLineModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<PriceLineLookup> GetPagedActivePriceLinesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.PriceLines.ActivePriceLines().OrderBy(o => o.Code).PriceLineOverviewQuery(query).Select(PriceLineLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<PriceLineLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
