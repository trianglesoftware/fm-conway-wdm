﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.PriceLines.Models
{
    public class PriceLineModel
    {
        public Guid PriceLineID { get; set; }
        [Required(ErrorMessage = "Code is required")]
        public string Code { get; set; }
        public string Description { get; set; }
        [DisplayFormat(DataFormatString = "0.00##")]
        public decimal? DefaultRate { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<PriceLine, PriceLineModel>> EntityToModel = entity => new PriceLineModel
        {
            PriceLineID = entity.PriceLinePK,
            Code = entity.Code,
            Description = entity.Description,
            DefaultRate = entity.DefaultRate,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<PriceLineModel, PriceLine> ModelToEntity = model => new PriceLine
        {
            PriceLinePK = model.PriceLineID,
            Code = model.Code,
            Description = model.Description,
            DefaultRate = model.DefaultRate,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
