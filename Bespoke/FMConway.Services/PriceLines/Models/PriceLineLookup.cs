﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.PriceLines.Models
{
    public class PriceLineLookup : LookupBase<Guid>
    {
        public string Description { get; set; }

        public static Expression<Func<PriceLine, PriceLineLookup>> EntityToModel = entity => new PriceLineLookup
        {
            Key = entity.Code,
            Value = entity.PriceLinePK,
            Description = entity.Description
        };
    }
}
