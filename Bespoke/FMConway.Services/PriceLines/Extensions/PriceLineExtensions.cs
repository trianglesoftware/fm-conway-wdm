﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.PriceLines.Extensions
{
    public static class PriceLineExtensions
    {
        public static IQueryable<PriceLine> ActivePriceLines(this IQueryable<PriceLine> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<PriceLine> InactivePriceLines(this IQueryable<PriceLine> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<PriceLine> PriceLineOverviewQuery(this IQueryable<PriceLine> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Code.Contains(query));
            }

            return model;
        }
    }
}
