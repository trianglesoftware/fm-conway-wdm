﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Extensions
{
    public static class NameValueCollectionExtensions
    {
        public static T Get<T>(this NameValueCollection appSettings, string column)
        {
            var value = ConfigurationManager.AppSettings[column];
            var type = typeof(T);

            if (value == null)
            {
                return default(T);
            }
            else if (type == typeof(string))
            {
                return (T)(object)value;
            }

            var output = Activator.CreateInstance<T>();
            var converter = TypeDescriptor.GetConverter(type);

            if (converter != null)
            {
                try
                {
                    var converted = converter.ConvertFromString(value);
                    if (converted != null)
                    {
                        output = (T)converted;
                    }
                }
                catch (Exception)
                {
                    // ignore
                }
            }

            return output;
        }
    }
}
