﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ToolboxTalks.Models
{
    public class ToolboxTalkLookup : LookupBase<Guid>
    {
        public string Revision { get; set; }

        public static Expression<Func<ToolboxTalk, ToolboxTalkLookup>> EntityToModel = entity => new ToolboxTalkLookup()
        {
            Key = entity.ToolboxTalk1,
            Value = entity.ToolboxTalkPK,
            Revision = entity.Revision
        };
    }
}
