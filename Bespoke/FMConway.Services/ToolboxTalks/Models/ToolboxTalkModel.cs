﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ToolboxTalks.Models
{
    public class ToolboxTalkModel
    {
        public Guid ToolboxTalkID { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Revision is required")]
        public string Revision { get; set; }
        [Required(ErrorMessage = "Body is required")]
        public string Body { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<ToolboxTalk, ToolboxTalkModel>> EntityToModel = entity => new ToolboxTalkModel
        {
            ToolboxTalkID = entity.ToolboxTalkPK,
            Title = entity.ToolboxTalk1,
            Revision = entity.Revision,
            Body = entity.Body,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<ToolboxTalkModel, ToolboxTalk> ModelToEntity = model => new ToolboxTalk
        {
            ToolboxTalkPK = model.ToolboxTalkID,
            ToolboxTalk1 = model.Title,
            Revision = model.Revision,
            Body = model.Body,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
