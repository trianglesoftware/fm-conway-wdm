﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ToolboxTalks.Extensions
{
    public static class ToolboxTalkExtensions
    {
        public static IQueryable<ToolboxTalk> ActiveToolboxTalks(this IQueryable<ToolboxTalk> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<ToolboxTalk> InactiveToolboxTalks(this IQueryable<ToolboxTalk> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<ToolboxTalk> ToolboxTalkOverviewQuery(this IQueryable<ToolboxTalk> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ToolboxTalk1.Contains(query) ||
                                          bq.Revision.Contains(query));
            }

            return model;
        }
    }
}
