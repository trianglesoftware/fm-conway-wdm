﻿using FMConway.Services.ToolboxTalks.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.ToolboxTalks.Extensions;

namespace FMConway.Services.ToolboxTalks.Services
{
    public class ToolboxTalkService : Service, FMConway.Services.ToolboxTalks.Interfaces.IToolboxTalkService
    {
        UserContext _userContext;

        public ToolboxTalkService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(ToolboxTalkModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.ToolboxTalkID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.ToolboxTalks.Add(ToolboxTalkModel.ModelToEntity(model));

            SaveChanges();

            return model.ToolboxTalkID;
        }

        public void Update(ToolboxTalkModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            ToolboxTalk current = Context.ToolboxTalks.Where(x => x.ToolboxTalkPK.Equals(model.ToolboxTalkID)).Single();

            current.ToolboxTalk1 = model.Title == null ? "" : model.Title;
            current.Revision = model.Revision == null ? "" : model.Revision;
            current.Body = model.Body == null ? "" : model.Body;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public ToolboxTalkModel Single(Guid ID)
        {
            return Context.ToolboxTalks.Where(a => a.ToolboxTalkPK.Equals(ID)).Select(ToolboxTalkModel.EntityToModel).SingleOrDefault();
        }

        public List<ToolboxTalkModel> GetAllActiveToolboxTalks()
        {
            return Context.ToolboxTalks.ActiveToolboxTalks().Select(ToolboxTalkModel.EntityToModel).ToList();
        }

        public PagedResults<ToolboxTalkModel> GetActiveToolboxTalksPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.ToolboxTalks.ActiveToolboxTalks().OrderBy(o => o.ToolboxTalk1).ToolboxTalkOverviewQuery(query).Select(ToolboxTalkModel.EntityToModel);

            return PagedResults<ToolboxTalkModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<ToolboxTalkModel> GetAllInactiveToolboxTalks()
        {
            return Context.ToolboxTalks.InactiveToolboxTalks().Select(ToolboxTalkModel.EntityToModel).ToList();
        }

        public PagedResults<ToolboxTalkModel> GetInactiveToolboxTalksPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.ToolboxTalks.InactiveToolboxTalks().OrderBy(o => o.ToolboxTalk1).ToolboxTalkOverviewQuery(query).Select(ToolboxTalkModel.EntityToModel);

            return PagedResults<ToolboxTalkModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ToolboxTalkLookup> GetPagedActiveToolboxTalksLookup(int rowCount, int page, string query)
        {
            var filteredRows = new List<ToolboxTalkLookup>();

            var baseQuery = Context.ToolboxTalks.ActiveToolboxTalks().OrderBy(o => o.ToolboxTalk1).ToolboxTalkOverviewQuery(query);

            int recordCount = baseQuery.Count();

            filteredRows = baseQuery.OrderBy(o => o.ToolboxTalk1).Select(ToolboxTalkLookup.EntityToModel).Skip(rowCount * page).Take(rowCount).ToList();

            return new PagedResults<ToolboxTalkLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = filteredRows
            };
        }

        public List<WorkInstructionToolboxTalk> GetForWI(Guid workInstructionID)
        {
            return Context.WorkInstructionToolboxTalks.Where(a => a.WorkInstructionFK.Equals(workInstructionID)).ToList();
        }

        public List<JobToolboxTalk> GetForJob(Guid jobID)
        {
            return Context.JobToolboxTalks.Where(a => a.JobFK == jobID).ToList();
        }
    }
}
