﻿using FMConway.Services.MethodStatements.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.ToolboxTalks.Models;

namespace FMConway.Services.ToolboxTalks.Interfaces
{
    public interface IToolboxTalkService : IService
    {
        Guid Create(ToolboxTalkModel model);
        void Update(ToolboxTalkModel model);
        ToolboxTalkModel Single(Guid ID);

        List<WorkInstructionToolboxTalk> GetForWI(Guid workInstructionID);
        List<JobToolboxTalk> GetForJob(Guid jobID);

        List<ToolboxTalkModel> GetAllActiveToolboxTalks();
        PagedResults<ToolboxTalkModel> GetActiveToolboxTalksPaged(int rowCount, int page, string query);

        List<ToolboxTalkModel> GetAllInactiveToolboxTalks();
        PagedResults<ToolboxTalkModel> GetInactiveToolboxTalksPaged(int rowCount, int page, string query);

        PagedResults<ToolboxTalkLookup> GetPagedActiveToolboxTalksLookup(int rowCount, int page, string query);
    }
}
