﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Customers.Models
{
    public class CustomerUsersLookup : LookupBase<Guid>
    {
        public static Expression<Func<Customer, CustomerUsersLookup>> EntityToModel = entity => new CustomerUsersLookup
        {
            Key = entity.CustomerName,
            Value = entity.CustomerPK
        };
    }
}
