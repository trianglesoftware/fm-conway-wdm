﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace FMConway.Services.Customers.Models
{
    public class CustomerModel
    {
        public Guid CustomerID { get; set; }
        [Required(ErrorMessage = "Customer Name is required")]
        public string CustomerName { get; set; }
        public string AccountNumber { get; set; }
        public string CustomerCode { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Comments { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string UserName { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Customer, CustomerModel>> EntityToModel = entity => new CustomerModel
        {
            CustomerID = entity.CustomerPK,
            CustomerName = entity.CustomerName,
            AccountNumber = entity.AccountNumber,
            CustomerCode = entity.CustomerCode,
            AddressLine1 = entity.AddressLine1,
            AddressLine2 = entity.AddressLine2,
            AddressLine3 = entity.AddressLine3,
            City = entity.City,
            County = entity.County,
            Postcode = entity.Postcode,
            Comments = entity.Comments,
            PhoneNumber = entity.PhoneNumber,
            FaxNumber = entity.FaxNumber,
            EmailAddress = entity.EmailAddress,
            UserName = entity.UserProfiles.Select(b => b.User.UserName).FirstOrDefault(),

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<CustomerModel, Customer> ModelToEntity = model => new Customer
        {
            CustomerPK = model.CustomerID,
            CustomerName = model.CustomerName == null ? "" : model.CustomerName,
            AccountNumber = model.AccountNumber == null ? "" : model.AccountNumber,
            CustomerCode = model.CustomerCode == null ? "" : model.CustomerCode,
            AddressLine1 = model.AddressLine1 == null ? "" : model.AddressLine1,
            AddressLine2 = model.AddressLine2 == null ? "" : model.AddressLine2,
            AddressLine3 = model.AddressLine3 == null ? "" : model.AddressLine3,
            City = model.City == null ? "" : model.City,
            County = model.County == null ? "" : model.County,
            Postcode = model.Postcode == null ? "" : model.Postcode,
            Comments = model.Comments == null ? "" : model.Comments,
            PhoneNumber = model.PhoneNumber == null ? "" : model.PhoneNumber,
            FaxNumber = model.FaxNumber == null ? "" : model.FaxNumber,
            EmailAddress = model.EmailAddress == null ? "" : model.EmailAddress,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
