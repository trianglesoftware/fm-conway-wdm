﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Customers.Models
{
    public class CustomerLookup : LookupBase<Guid>
    {
        public static Expression<Func<Customer, CustomerLookup>> EntityToModel = entity => new CustomerLookup()
        {
            Key = entity.CustomerName,
            Value = entity.CustomerPK
        };
    }
}
