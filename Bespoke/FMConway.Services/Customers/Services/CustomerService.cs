﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Customers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Customers.Extensions;
using System.Security.Authentication;

namespace FMConway.Services.Customers.Services
{
    public class CustomerService : Service, FMConway.Services.Customers.Interfaces.ICustomerService
    {
        UserContext _userContext;
        
        public CustomerService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(CustomerModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.CustomerID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Customers.Add(CustomerModel.ModelToEntity(model));

            SaveChanges();

            return model.CustomerID;
        }

        public void Update(CustomerModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Customer current = Context.Customers.Where(x => x.CustomerPK.Equals(model.CustomerID)).Single();

            current.CustomerName = model.CustomerName == null ? "" : model.CustomerName;
            current.AccountNumber = model.AccountNumber == null ? "" : model.AccountNumber;
            current.CustomerCode = model.CustomerCode == null ? "" : model.CustomerCode;
            current.AddressLine1 = model.AddressLine1 == null ? "" : model.AddressLine1;
            current.AddressLine2 = model.AddressLine2 == null ? "" : model.AddressLine2;
            current.AddressLine3 = model.AddressLine3 == null ? "" : model.AddressLine3;
            current.City = model.City == null ? "" : model.City;
            current.County = model.County == null ? "" : model.County;
            current.Postcode = model.Postcode == null ? "" : model.Postcode;
            current.Comments = model.Comments == null ? "" : model.Comments;
            current.PhoneNumber = model.PhoneNumber == null ? "" : model.PhoneNumber;
            current.FaxNumber = model.FaxNumber == null ? "" : model.FaxNumber;
            current.EmailAddress = model.EmailAddress == null ? "" : model.EmailAddress;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public CustomerModel Single(Guid ID)
        {
            return Context.Customers.Where(a => a.CustomerPK.Equals(ID)).Select(CustomerModel.EntityToModel).SingleOrDefault();
        }

        public List<CustomerModel> GetAllActiveCustomers()
        {
            return Context.Customers.ActiveCustomers().Select(CustomerModel.EntityToModel).OrderBy(o => o.CustomerName).ToList();
        }

        public PagedResults<CustomerModel> GetActiveCustomersPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Customers.ActiveCustomers().OrderBy(o => o.CustomerName).CustomerOverviewQuery(query).Select(CustomerModel.EntityToModel);

            return PagedResults<CustomerModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<CustomerModel> GetAllInactiveCustomers()
        {
            return Context.Customers.InactiveCustomers().Select(CustomerModel.EntityToModel).ToList();
        }

        public PagedResults<CustomerModel> GetInactiveCustomersPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Customers.InactiveCustomers().OrderBy(o => o.CustomerName).CustomerOverviewQuery(query).Select(CustomerModel.EntityToModel);

            return PagedResults<CustomerModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<CustomerLookup> GetPagedActiveCustomersLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Customers.ActiveCustomers().OrderBy(o => o.CustomerName).CustomerOverviewQuery(query).Select(CustomerLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<CustomerLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public PagedResults<CustomerUsersLookup> GetPagedActiveCustomerUsersLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Customers.ActiveCustomers().Where(e => !e.UserProfiles.Any()).OrderBy(o => o.CustomerName).CustomerOverviewQuery(query).Select(CustomerUsersLookup.EntityToModel);

            return PagedResults<CustomerUsersLookup>.GetPagedResults(baseQuery, rowCount, page);
        }
    }
}
