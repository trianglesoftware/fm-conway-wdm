﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Customers.Extensions
{
    public static class CustomerExtensions
    {
        public static IQueryable<Customer> ActiveCustomers(this IQueryable<Customer> model)
        {
            return model.Where(a => a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Customer> InactiveCustomers(this IQueryable<Customer> model)
        {
            return model.Where(a => !a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Customer> CustomerOverviewQuery(this IQueryable<Customer> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.CustomerName.Contains(query) ||
                                          bq.AccountNumber.Contains(query) ||
                                          bq.AddressLine1.Contains(query) ||
                                          bq.AddressLine2.Contains(query) ||
                                          bq.AddressLine3.Contains(query) ||
                                          bq.City.Contains(query) ||
                                          bq.County.Contains(query) ||
                                          bq.Postcode.Contains(query) ||
                                          bq.PhoneNumber.Contains(query) ||
                                          bq.EmailAddress.Contains(query));
            }

            return model;
        }

        // abbreviate agency name if longer then required length
        public static string Abbreviate(string name, int length = 3)
        {
            if (name == null)
            {
                return null;
            }

            if (name.Length <= length)
            {
                return name.ToUpperInvariant();
            }

            var spaces = name.Where(a => a == ' ').Count();
            if (spaces == 0)
            {
                // if no spaces, take first x characters
                return name.Substring(0, length).ToUpperInvariant();
            }
            else
            {
                // if has spaces, take first character of each word in upper case
                return new string(name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s[0]).ToArray()).ToUpperInvariant();
            }
        }

    }
}
