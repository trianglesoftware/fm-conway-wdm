﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Customers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Customers.Interfaces
{
    public interface ICustomerService : IService
    {
        Guid Create(CustomerModel model);
        void Update(CustomerModel model);
        CustomerModel Single(Guid ID);

        List<CustomerModel> GetAllActiveCustomers();
        PagedResults<CustomerModel> GetActiveCustomersPaged(int rowCount, int page, string query);

        List<CustomerModel> GetAllInactiveCustomers();
        PagedResults<CustomerModel> GetInactiveCustomersPaged(int rowCount, int page, string query);

        PagedResults<CustomerLookup> GetPagedActiveCustomersLookup(int rowCount, int page, string query);
        PagedResults<CustomerUsersLookup> GetPagedActiveCustomerUsersLookup(int rowCount, int page, string query);
    }
}
