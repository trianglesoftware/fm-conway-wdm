﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Tasks.Enums;
using FMConway.Services.Tasks.Models;
using FMConway.Services.WorkInstructionDetails.Models;

namespace FMConway.Services.Tasks.Interfaces
{
    public interface ITaskService : IService
    {
        PagedResults<TaskModel> GetActiveTasksPaged(int rowCount, int page, string query, Guid workInstructionID);
        PagedResults<ChecklistAnswerModel> GetTaskChecklistAnswersPaged(int rowCount, int page, string query, Guid taskID);
        PagedResults<TaskSignatureModel> GetTaskSignaturesPaged(int rowCount, int page, string query, Guid taskID);
        PagedResults<TaskPhotoModel> GetTaskPhotosPaged(int rowCount, int page, string query, Guid taskID);
        PagedResults<TaskPhotoModel> GetTaskChecklistAnswerPhotosPaged(int rowCount, int page, string query, Guid taskChecklistAnswerID);
        PagedResults<EquipmentModel> GetTaskEquipmentPaged(int rowCount, int page, string query, Guid workInstructionID);
        PagedResults<AssetModel> GetTaskEquipmentAssetsPaged(int rowCount, int page, string query, Guid workInstructionID, Guid equipmentID, string taskType);
        TaskModel Single(Guid taskID);
        TaskModel Update(TaskModel model);     
        TaskPhotoModel UploadTaskPhoto(TaskPhotoModel model);
        void UploadUnloadingProcessPhoto(UnloadingProcessPhotoModel model);
        void UpdateUnloadingProcess(UnloadingProcessModel model);
        void UploadCleansingLogPhoto(CleansingLogPhotoModel model);
        TaskChecklistAnswerPhotoModel UploadTaskChecklistPhoto(TaskChecklistAnswerPhotoModel model);
        void UpdateTaskActivity(TaskActivityDetailsModel model);
        void CreateFreeTextTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null);
        void CreateFreeTextWithDetailsTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, bool photosRequired);
        void CreatePhotoSignatureTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, bool photosRequired, bool withDetails = false, bool showClientSignature = false, bool clientSignatureRequired = false, string tag = null);
        void CreateTrafficCountTask(int taskOrder, Guid worksInstructionID, bool isMandatory);
        void CreateAreaCallTask(int taskOrder, Guid worksInstructionID, Guid areaFK);
        void CreateChecklistTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, Guid worksInstructionChecklistID, string tag = null);
        void CreateSignaturesTask(int taskOrder, Guid worksInstructionID, bool isMandatory, bool showAmendments = true, bool showChecklist = true, bool showClientSignature = true, bool clientSignatureRequired = true, bool showStaffSignature = true, bool staffSignatureRequired = true, string tag = null);
        Guid CreateEquipmentTask(int taskOrder, Guid worksInstructionID, EquipmentTaskType equipmentTaskType, string tag = null);
        void CreateTaskDescription(int taskOrder, Guid worksInstructionID, string name);
        void CreateWeatherTask(int taskOrder, Guid workInstructionID, bool isMandatory);
        void CreateBatteryTask(int taskOrder, Guid worksInstructionID, Guid checklistID, string tag = null);
        void CreateActivityDetailsTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null);
        void CreateActivityRiskAssessmentTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null);
        void CreateCleansingLogTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null);
        void CreateUnloadingProcessTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null);
        void UpdateCleansingLog(CleansingLogModel model);
        AreaCallModel GetAreaCallDetails(Guid areaID);
        TaskActivityDetailsModel GetActivityDetails(Guid workInstructionID);
        PagedResults<TaskActivityRiskAssessmentModel> GetActivityRiskAssessmentsPaged(int rowCount, int page, string query, Guid taskID);
        bool HasActivityRiskAssessments(Guid workInstructionID);
        PagedResults<TaskCleansingLogsModel> GetCleansingLogsPaged(int rowCount, int page, string query, Guid taskID);
        CleansingLogModel GetCleansingLog(Guid cleansingLogID);
        TaskActivityRiskAssessmentModel GetActivityRiskAssessment(Guid activityRiskAssessmentID);
        PagedResults<CleansingLogPhotoModel> GetCleansingLogPhotosPaged(int rowCount, int page, string query, Guid cleansingLogID);
        PagedResults<TaskUnloadingProcessesModel> GetUnloadingProcessesPaged(int rowCount, int page, string query, Guid taskID);
        UnloadingProcessModel GetUnloadingProcess(Guid unloadingProcessID);
        PagedResults<UnloadingProcessPhotoModel> GetUnloadingProcessPhotosPaged(int rowCount, int page, string query, Guid unloadingProcessID);

        bool IsTaskRiskAssessmentChecklist(Guid taskID);
        bool IsTaskBatteryChecklist(Guid taskID);
        bool IsYesNoNaChecklist(Guid taskID);
        bool UpdateAssets(Guid taskID, Guid assetID, Guid equipmentID, string itemNumber, string assetNumber);
        bool RemoveAssets(Guid equipmentID, List<Guid> assetID);
        bool UpdateEquipmentQuantity(Guid equipmentID, int quantity);
    }
}
