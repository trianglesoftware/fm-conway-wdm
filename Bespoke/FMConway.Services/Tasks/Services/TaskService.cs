﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Tasks.Models;
using FMConway.Services.Tasks.Interfaces;
using FMConway.Services.Tasks.Extensions;
using FMConway.Services.ErrorLogging.Services;
using FMConway.Services.WorkInstructionDetails.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using System.Security.Authentication;
using FMConway.Services.Tasks.Enums;
using FMConway.Services.WorkInstructions.Models;

namespace FMConway.Services.Tasks.Services
{
    public class TaskService : Service, FMConway.Services.Tasks.Interfaces.ITaskService
    {
        UserContext _userContext;
        ErrorLoggingService _errorService;


        public TaskService(UserContext userContext, ErrorLoggingService errorService)
        {
            _userContext = userContext;
            _errorService = errorService;
        }

        public PagedResults<TaskModel> GetActiveTasksPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            var baseQuery = Context.Tasks.Where(t => t.WorksInstructionFK.Equals(workInstructionID) && !t.TaskType.TaskType1.Equals("Install")).OrderBy(t => t.TaskOrder).TaskOverviewQuery(query).Select(TaskModel.EntityToModel);

            return PagedResults<TaskModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ChecklistAnswerModel> GetTaskChecklistAnswersPaged(int rowCount, int page, string query, Guid taskID)
        {
            var baseQuery = 
                Context.vv_TaskChecklists.Where(mc => mc.TaskPK.Equals(taskID))
                .OrderBy(mc => mc.TaskChecklistPK).ThenBy(mc => mc.QuestionOrder)
                .TaskChecklistAnswersOverviewQuery(query)
                .Select(ChecklistAnswerModel.TaskETM);

            return PagedResults<ChecklistAnswerModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<TaskSignatureModel> GetTaskSignaturesPaged(int rowCount, int page, string query, Guid taskID)
        {
            var baseQuery = Context.TaskSignatures.Where(s => s.TaskFK.Equals(taskID)).OrderBy(s => s.Employee.EmployeeName).TaskSignatureOverviewQuery(query).Select(TaskSignatureModel.ETM);

            return PagedResults<TaskSignatureModel>.GetPagedResults(baseQuery, rowCount, page); 
        }

        public PagedResults<TaskPhotoModel> GetTaskPhotosPaged(int rowCount, int page, string query, Guid taskID)
        {
            var baseQuery = Context.TaskPhotos.Where(s => s.TaskFK.Equals(taskID)).OrderBy(s => s.CreatedDate).TaskPhotoQuery(query).Select(TaskPhotoModel.ETM);

            return PagedResults<TaskPhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<TaskPhotoModel> GetTaskChecklistAnswerPhotosPaged(int rowCount, int page, string query, Guid taskChecklistAnswerID)
        {
            var baseQuery = Context.TaskChecklistAnswerPhotos.Where(a => a.TaskChecklistAnswerFK == taskChecklistAnswerID).OrderBy(s => s.CreatedDate).TaskChecklistPhotoQuery(query).Select(TaskPhotoModel.TaskChecklistAnswerPhotoToModel);

            return PagedResults<TaskPhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<EquipmentModel> GetTaskEquipmentPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            var workInstructionIDs = GetAssociatedWorkInstructionIDs(workInstructionID);
            var workInstructions = Context.WorkInstructions.Where(a => workInstructionIDs.Contains(a.WorkInstructionPK));
            var hasEquipmentInstallTask = workInstructions.SelectMany(a => a.Tasks).Any(a => a.TaskType.TaskType1 == TaskTypesEnum.EquipmentInstall);
            var hasEquipmentCollectionTask = workInstructions.SelectMany(a => a.Tasks).Any(a => a.TaskType.TaskType1 == TaskTypesEnum.EquipmentCollection);

            var baseQuery = Context.TaskEquipments
                .Where(a => a.IsActive && workInstructionIDs.Contains(a.Task.WorksInstructionFK))
                .Where(a => 
                    a.Equipment.EquipmentName.Contains(query) ||
                    a.Equipment.EquipmentType.Name.Contains(query) ||
                    (a.Equipment.EquipmentType.EquipmentTypeVmsOrAsset != null && a.Equipment.EquipmentType.EquipmentTypeVmsOrAsset.Name.Contains(query))
                )
                .GroupBy(a => a.Equipment)
                .Select(a => new EquipmentModel
                {
                    EquipmentID = a.Key.EquipmentPK,
                    Equipment = a.Key.EquipmentName,
                    TaskEquipmentID = a.FirstOrDefault().TaskEquipmentPK,
                    Type = a.Key.EquipmentType.Name,
                    QuantityInstalled = a.Where(b => b.Task.TaskType.TaskType1 == TaskTypesEnum.EquipmentInstall).Sum(b => (int?)b.Quantity),
                    QuantityCollected = a.Where(b => b.Task.TaskType.TaskType1 == TaskTypesEnum.EquipmentCollection).Sum(b => (int?)b.Quantity),
                    VmsOrAsset = a.Key.EquipmentType.EquipmentTypeVmsOrAsset.Name,
                    hasEquipmentInstallTask = hasEquipmentInstallTask,
                    hasEquipmentCollectionTask = hasEquipmentCollectionTask,
                })
                .OrderBy(a => a.Equipment);

            return PagedResults<EquipmentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<AssetModel> GetTaskEquipmentAssetsPaged(int rowCount, int page, string query, Guid workInstructionID, Guid equipmentID, string taskType)
        {
            var workInstructionIDs = GetAssociatedWorkInstructionIDs(workInstructionID);

            var baseQuery = Context.TaskEquipmentAssets
                .Where(a => 
                    workInstructionIDs.Contains(a.TaskEquipment.Task.WorksInstructionFK) &&
                    a.TaskEquipment.EquipmentFK == equipmentID &&
                    a.TaskEquipment.Task.TaskType.TaskType1 == taskType
                )
                .Where(a => a.AssetNumber.Contains(query))
                .OrderBy(s => s.ItemNumber)
                .Select(AssetModel.ETM);

            return PagedResults<AssetModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        private List<Guid> GetAssociatedWorkInstructionIDs(Guid workInstructionID)
        {
            var installWorkInstructionID = Context.WorkInstructions.Where(a => a.WorkInstructionPK == workInstructionID).Select(a => a.InstallWIPK).SingleOrDefault();
            var collectionWorkInstructionID = Context.WorkInstructions.Where(a => a.InstallWIPK == workInstructionID).Select(a => (Guid?)a.WorkInstructionPK).FirstOrDefault();

            var workInstructionIDs = new List<Guid>();
            workInstructionIDs.Add(workInstructionID);

            if (installWorkInstructionID != null)
            {
                workInstructionIDs.Add((Guid)installWorkInstructionID);
            }
            if (collectionWorkInstructionID != null)
            {
                workInstructionIDs.Add((Guid)collectionWorkInstructionID);
            }

            return workInstructionIDs;
        }

        public TaskModel Single(Guid taskID)
        {
            return Context.Tasks.Where(t => t.TaskPK.Equals(taskID)).Select(TaskModel.EntityToModel).SingleOrDefault();
        }

        public AreaCallModel GetAreaCallDetails(Guid areaID)
        {
            AreaCallModel model = new AreaCallModel();

            var ac = Context.AreaCallAreas.Where(a => a.AreaCallAreaPK.Equals(areaID)).SingleOrDefault();

            if (ac != null)
            {
                model.PersonsName = ac.PersonsName;
                model.Reference = ac.Reference;
                model.ExtraDetails = ac.ExtraDetails;
                model.NCCNumber = ac.NCCNumber;
                model.RCCNumber = ac.RCCNumber;
                model.CallProtocol = ac.CallProtocol;                 
            }

            return model;
        }

        public TaskActivityDetailsModel GetActivityDetails(Guid workInstructionID)
        {
            return Context.WorkInstructions.Where(a => a.WorkInstructionPK == workInstructionID).Select(TaskActivityDetailsModel.EntityToModel).Single();
        }

        public PagedResults<TaskActivityRiskAssessmentModel> GetActivityRiskAssessmentsPaged(int rowCount, int page, string query, Guid taskID)
        {
            var baseQuery = Context.ActivityRiskAssessments
                .Where(w => w.TaskFK == taskID)
                .Where(a =>
                    a.RoadName.Contains(query)
                )
                .OrderBy(s => s.CreatedDate)
                .Select(TaskActivityRiskAssessmentModel.ETM);

            return PagedResults<TaskActivityRiskAssessmentModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public bool HasActivityRiskAssessments(Guid taskID)
        {
            return Context.ActivityRiskAssessments.Where(w => w.TaskFK == taskID).Any();
        }

        public TaskActivityRiskAssessmentModel GetActivityRiskAssessment(Guid activityRiskAssessmentID)
        {
            return Context.ActivityRiskAssessments.Where(a => a.ActivityRiskAssessmentPK == activityRiskAssessmentID).Select(TaskActivityRiskAssessmentModel.ETM).Single();
        }

        public void UpdateTaskActivity(TaskActivityDetailsModel model)
        {
            var activity = Context.WorkInstructions.Where(a => a.WorkInstructionPK == model.WorkInstructionID).Single();
            activity.Operator = model.Operator;
            activity.ClientReferenceNumber = model.ClientReferenceNumber;
            activity.SiteName = model.SiteName;
            activity.ActivityPriorityFK = model.ActivityPriorityID;
            activity.WorkInstructionPK = model.WorkInstructionID;
            Context.SaveChanges();
        }
        public PagedResults<TaskCleansingLogsModel> GetCleansingLogsPaged(int rowCount, int page, string query, Guid taskID)
        {
            var baseQuery = Context.CleansingLogs
                .Where(a => a.TaskFK == taskID && a.IsActive)
                .Where(a =>
                    query == null || query.Trim().Length == 0 ||
                    a.Road.Contains(query) ||
                    a.Section.Contains(query)
                )
                .OrderBy(a => a.CreatedDate)
                .Select(TaskCleansingLogsModel.EntityToModel);

            return PagedResults<TaskCleansingLogsModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public void UpdateCleansingLog(CleansingLogModel model)
        {
            var log = Context.CleansingLogs.Where(w => w.CleansingLogPK.Equals(model.CleansingLogID)).Single();

            log.LaneFK = model.LaneID;
            log.DirectionFK = model.DirectionID;
            log.Section = model.Section;
            log.Road = model.Road;
            log.StartMarker = model.StartMarker;
            log.EndMarker = model.EndMarker;
            log.CatchpitsCleaned = model.CatchpitsCleaned;
            log.Channels = model.Channels;
            log.CatchpitsMissed = model.CatchpitsMissed;
            log.EndMarker = model.EndMarker;
            log.GulliesCleaned = model.GulliesCleaned;
            log.GulliesMissed = model.GulliesMissed;
            log.SlotDrains = model.SlotDrains;
            log.Defects = model.Defects;
            log.UpdatedByFK = _userContext.UserDetails.ID;
            log.UpdatedDate = DateTime.Now;
            Context.SaveChanges();
        }

        public CleansingLogModel GetCleansingLog(Guid cleansingLogID)
        {
            return Context.CleansingLogs.Where(a => a.CleansingLogPK == cleansingLogID).Select(CleansingLogModel.EntityToModel).Single();
        }

        public PagedResults<CleansingLogPhotoModel> GetCleansingLogPhotosPaged(int rowCount, int page, string query, Guid cleansingLogID)
        {
            var baseQuery = Context.CleansingLogPhotos
                .Where(s => s.CleansingLogFK == cleansingLogID)
                .Where(a =>
                    query == null || query.Trim().Length == 0 ||
                    a.File.FileDownloadName.Contains(query)
                )
                .OrderBy(s => s.CreatedDate)
                .Select(CleansingLogPhotoModel.EntityToModel);

            return PagedResults<CleansingLogPhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<TaskUnloadingProcessesModel> GetUnloadingProcessesPaged(int rowCount, int page, string query, Guid taskID)
        {
            var isDecimal = decimal.TryParse(query, out decimal decimalQuery);

            var baseQuery = Context.UnloadingProcesses
                .Where(a => a.TaskFK == taskID && a.IsActive)
                .Where(a =>
                    query == null || query.Trim().Length == 0 ||
                    a.WasteTicketNumber.Contains(query) ||
                    (isDecimal ? a.VolumeOfWasteDisposed == decimalQuery : false)
                )
                .OrderBy(a => a.CreatedDate)
                .Select(TaskUnloadingProcessesModel.EntityToModel);

            return PagedResults<TaskUnloadingProcessesModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public UnloadingProcessModel GetUnloadingProcess(Guid unloadingProcessID)
        {
            return Context.UnloadingProcesses.Where(a => a.UnloadingProcessPK == unloadingProcessID).Select(UnloadingProcessModel.EntityToModel).Single();
        }

        public PagedResults<UnloadingProcessPhotoModel> GetUnloadingProcessPhotosPaged(int rowCount, int page, string query, Guid unloadingProcessID)
        {
            var baseQuery = Context.UnloadingProcessPhotos
                .Where(s => s.UnloadingProcessFK == unloadingProcessID)
                .Where(a =>
                    query == null || query.Trim().Length == 0 ||
                    a.File.FileDownloadName.Contains(query)
                )
                .OrderBy(s => s.CreatedDate)
                .Select(UnloadingProcessPhotoModel.EntityToModel);

            return PagedResults<UnloadingProcessPhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        #region Task Creation

        public void CreateFreeTextTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null)
        {
            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name;
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 5;
                task.TaskOrder = taskOrder;
                task.PhotosRequired = false;
                task.IsMandatory = isMandatory;
                task.IsCancelled = false;
                task.Tag = tag;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateFreeTextTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateFreeTextWithDetailsTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, bool photosRequired)
        {
            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name;
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 6;
                task.TaskOrder = taskOrder;
                task.PhotosRequired = photosRequired;
                task.IsMandatory = isMandatory;
                task.IsCancelled = false;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateFreeTextTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        // photosRequired = is at least one photo manditory
        // withDetails = display work instruction comments
        // showClientSignature = show client signature button
        // clientSignatureRequired = is client signature button required
        // tag = free text field to pass custom args
        public void CreatePhotoSignatureTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, bool photosRequired, bool withDetails = false, bool showClientSignature = false, bool clientSignatureRequired = false, string tag = null)
        {
            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name;
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 10;
                task.TaskOrder = taskOrder;
                task.PhotosRequired = photosRequired;
                task.ShowDetails = withDetails;
                task.ShowClientSiganture = showClientSignature;
                task.ClientSignatureRequired = clientSignatureRequired;
                task.Tag = tag;
                task.IsMandatory = isMandatory;
                task.IsCancelled = false;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreatePhotosTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateTrafficCountTask(int taskOrder, Guid worksInstructionID, bool isMandatory)
        {
            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = "Traffic Count";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 1;
                task.TaskOrder = taskOrder;
                task.IsMandatory = isMandatory;
                task.IsCancelled = false;
                task.PhotosRequired = false;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateTrafficCountTask", e, worksInstructionID.ToString());
                throw e;
            }

        }

        public void CreateAreaCallTask(int taskOrder, Guid worksInstructionID, Guid areaID)
        {
            try
            {
                var area = Context.Areas.Where(a => a.AreaPK.Equals(areaID)).SingleOrDefault();

                if (area != null)
                {
                    AreaCallArea newArea = new AreaCallArea() 
                    {
                        AreaCallAreaPK = Guid.NewGuid(),
                        Area = area.Area1,
                        CallProtocol = area.CallProtocol,
                        NCCNumber = area.NCCNumber,
                        RCCNumber = area.RCCNumber,
                        PersonsName = "",
                        Reference = "",
                        ExtraDetails = "",
                        IsActive = true,
                        CreatedByFK = _userContext.UserDetails.ID,
                        CreatedDate = DateTime.Now,
                        UpdatedByFK = _userContext.UserDetails.ID,
                        UpdatedDate = DateTime.Now,
                    };

                    Context.AreaCallAreas.Add(newArea);

                    SaveChanges();

                    Task task = new Task();

                    task.TaskPK = Guid.NewGuid();
                    task.Name = "Area Call";
                    task.WorksInstructionFK = worksInstructionID;
                    task.TaskTypeFK = 2;
                    task.TaskOrder = taskOrder;
                    task.AreaCallAreaFK = newArea.AreaCallAreaPK;
                    task.IsCancelled = false;
                    task.PhotosRequired = false;
                    task.IsActive = true;
                    task.CreatedByID = _userContext.UserDetails.ID;
                    task.CreatedDate = DateTime.Now;
                    task.UpdatedByID = _userContext.UserDetails.ID;
                    task.UpdatedDate = DateTime.Now;

                    Context.Tasks.Add(task);

                    SaveChanges();
                }
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateAreaCallTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateChecklistTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, Guid checklistID, string tag = null)
        {
            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name ?? "Risk Assessment";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 3;
                task.TaskOrder = taskOrder;
                task.ChecklistFK = checklistID;
                task.IsMandatory = isMandatory;
                task.IsCancelled = false;
                task.PhotosRequired = false;
                task.Tag = tag;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateChecklistTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateSignaturesTask(int taskOrder, Guid worksInstructionID, bool isMandatory, bool showAmendments = true, bool showChecklist = true, bool showClientSignature = true, bool clientSignatureRequired = true, bool showStaffSignature = true, bool staffSignatureRequired = true, string tag = null)
        {
            tag = tag ?? "";
            var args = tag.Split('|').ToList();

            if (showAmendments)
            {
                args.Add("ShowAmendments");
            }

            if (showChecklist)
            {
                args.Add("ShowChecklist");
            }

            tag = string.Join("|", args);

            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = "Signatures Required";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 7;
                task.ChecklistFK = null;
                task.TaskOrder = taskOrder;
                task.IsMandatory = isMandatory;
                task.IsCancelled = false;
                task.PhotosRequired = false;
                task.ShowClientSiganture = showClientSignature;
                task.ClientSignatureRequired = clientSignatureRequired;
                task.ShowStaffSignature = showStaffSignature;
                task.StaffSignatureRequired = staffSignatureRequired;
                task.Tag = tag;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateSignaturesTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public Guid CreateEquipmentTask(int taskOrder, Guid worksInstructionID, EquipmentTaskType equipmentTaskType, string tag = null)
        {
            try
            {
                var taskTypeFK = equipmentTaskType == EquipmentTaskType.Install ? 8 : 9;
                var name = equipmentTaskType == EquipmentTaskType.Install ? "Equipment Installation" : "Equipment Collection";

                var task = new Task();
                task.TaskPK = Guid.NewGuid();
                task.Name = name;
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = taskTypeFK;
                task.TaskOrder = taskOrder;
                task.IsCancelled = false;
                task.PhotosRequired = false;
                task.Tag = tag;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;
                Context.Tasks.Add(task);

                if (equipmentTaskType == EquipmentTaskType.Collection)
                {
                    var installWI = Context.WorkInstructions.Where(wi => wi.WorkInstructionPK == worksInstructionID).Select(wi => wi.InstallWIPK).SingleOrDefault();
                    if (installWI != null)
                    {
                        var taskEquipments = Context.Tasks.Where(a =>
                            a.WorkInstruction.IsActive &&
                            a.IsActive &&
                            a.WorksInstructionFK == installWI &&
                            a.TaskType.TaskType1 == TaskTypesEnum.EquipmentInstall)
                            .SelectMany(a => a.TaskEquipments.Where(b => b.IsActive)).ToList();

                        foreach (var taskEquipment in taskEquipments) // Copy the equipment over from the original installation if exists.
                        {
                            var te = new TaskEquipment();
                            te.TaskEquipmentPK = Guid.NewGuid();
                            te.Task = task;
                            te.EquipmentFK = taskEquipment.EquipmentFK;
                            te.Quantity = taskEquipment.Quantity;
                            te.IsActive = true;
                            te.CreatedDate = DateTime.Now;
                            te.CreatedByID = _userContext.UserDetails.ID;
                            te.UpdatedDate = DateTime.Now;
                            te.UpdatedByID = _userContext.UserDetails.ID;
                            Context.TaskEquipments.Add(te);

                            foreach (var taskEquipmentAsset in taskEquipment.TaskEquipmentAssets)
                            {
                                var tea = new TaskEquipmentAsset();
                                tea.TaskEquipmentAssetPK = Guid.NewGuid();
                                tea.TaskEquipment = te;
                                tea.ItemNumber = taskEquipmentAsset.ItemNumber;
                                tea.AssetNumber = taskEquipmentAsset.AssetNumber;
                                Context.TaskEquipmentAssets.Add(tea);
                            }
                        }
                    }
                }

                SaveChanges();

                return task.TaskPK;
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateInstallEquipmentTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateTaskDescription(int taskOrder, Guid worksInstructionID, string name)
        {
            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name;
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 4;
                task.TaskOrder = taskOrder;
                task.IsCancelled = false;
                task.PhotosRequired = false;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateSignaturesTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateWeatherTask(int taskOrder, Guid worksInstructionID, bool isMandatory)
        {
            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = "Weather";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 11;
                task.TaskOrder = taskOrder;
                task.IsMandatory = isMandatory;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateWeatherTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateBatteryTask(int taskOrder, Guid worksInstructionID, Guid checklistID, string tag = null)
        {
            try
            {
                Task task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = "Battery";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 3;
                task.TaskOrder = taskOrder;
                task.ChecklistFK = checklistID;
                task.IsCancelled = false;
                task.PhotosRequired = false;
                task.Tag = tag;
                task.IsActive = true;
                task.CreatedByID = _userContext.UserDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = _userContext.UserDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("CreateWeatherTask", e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateActivityDetailsTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null)
        {
            try
            {
                var userDetails = _userContext.UserDetails;
                var task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name ?? "Activity Details";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 13;
                task.TaskOrder = taskOrder;
                task.Tag = tag;
                task.IsMandatory = isMandatory;
                task.IsActive = true;
                task.CreatedByID = userDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = userDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog(nameof(CreateActivityDetailsTask), e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateActivityRiskAssessmentTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null)
        {
            try
            {
                var userDetails = _userContext.UserDetails;
                var task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name ?? "Activity Details";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 16;
                task.TaskOrder = taskOrder;
                task.Tag = tag;
                task.IsMandatory = isMandatory;
                task.IsActive = true;
                task.CreatedByID = userDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = userDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog(nameof(CreateActivityDetailsTask), e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateCleansingLogTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null)
        {
            try
            {
                var userDetails = _userContext.UserDetails;
                var task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name ?? "Cleansing Log";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 14;
                task.TaskOrder = taskOrder;
                task.Tag = tag;
                task.IsMandatory = isMandatory;
                task.IsActive = true;
                task.CreatedByID = userDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = userDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog(nameof(CreateCleansingLogTask), e, worksInstructionID.ToString());
                throw e;
            }
        }

        public void CreateUnloadingProcessTask(int taskOrder, Guid worksInstructionID, string name, bool isMandatory, string tag = null)
        {
            try
            {
                var userDetails = _userContext.UserDetails;
                var task = new Task();

                task.TaskPK = Guid.NewGuid();
                task.Name = name ?? "Unloading Process";
                task.WorksInstructionFK = worksInstructionID;
                task.TaskTypeFK = 15;
                task.TaskOrder = taskOrder;
                task.Tag = tag;
                task.IsMandatory = isMandatory;
                task.IsActive = true;
                task.CreatedByID = userDetails.ID;
                task.CreatedDate = DateTime.Now;
                task.UpdatedByID = userDetails.ID;
                task.UpdatedDate = DateTime.Now;

                Context.Tasks.Add(task);

                SaveChanges();
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog(nameof(CreateUnloadingProcessTask), e, worksInstructionID.ToString());
                throw e;
            }
        }

        #endregion

        public bool IsTaskRiskAssessmentChecklist(Guid taskID)
        {
            return Context.Tasks.Any(a => a.TaskPK == taskID && (a.Checklist.IsHighSpeedChecklist || a.Checklist.IsLowSpeedChecklist || a.Checklist.IsBatteryChecklist || a.Checklist.IsPostHireBatteryChecklist));
        }

        public bool IsTaskBatteryChecklist(Guid taskID)
        {
            return Context.Tasks.Any(a => a.TaskPK == taskID && (a.Checklist.IsPostHireBatteryChecklist && !a.Checklist.IsHighSpeedChecklist && !a.Checklist.IsLowSpeedChecklist && !a.Checklist.IsBatteryChecklist));
        }

        public bool IsYesNoNaChecklist(Guid taskID)
        {
            return Context.Tasks.Any(a => a.TaskPK == taskID && (
                a.Checklist.IsJettingPermitToWorkChecklist || a.Checklist.IsJettingRiskAssessmentChecklist ||
                a.Checklist.IsTankeringPermitToWorkChecklist || a.Checklist.IsTankeringRiskAssessmentChecklist ||
                a.Checklist.IsCCTVRiskAssessmentChecklist));
        }

        public TaskModel Update(TaskModel model)
        {
            var task = Context.Tasks.Where(a => a.TaskPK == model.TaskID).Single();
            task.StartTime = model.StartTime;
            task.EndTime = model.EndTime;
            task.Longitude = model.Longitude;   
            task.Latitude = model.Latitude;
            task.Notes = model.Notes;
            Context.SaveChanges();
            return model;
        }

        
        public TaskChecklistAnswerPhotoModel UploadTaskChecklistPhoto(TaskChecklistAnswerPhotoModel model)
        {
            var TaskChecklistAnswerPhoto = new TaskChecklistAnswerPhoto();
            TaskChecklistAnswerPhoto.TaskChecklistAnswerFK = model.TaskChecklistAnswerID;
            TaskChecklistAnswerPhoto.FileFK = model.FileID;
            TaskChecklistAnswerPhoto.CreatedByID = _userContext.UserDetails.ID;
            TaskChecklistAnswerPhoto.CreatedDate = DateTime.Now;
            TaskChecklistAnswerPhoto.UpdatedByID = _userContext.UserDetails.ID;
            TaskChecklistAnswerPhoto.UpdatedDate = DateTime.Now;
            TaskChecklistAnswerPhoto.TaskChecklistAnswerPhotoPK = Guid.NewGuid();
            TaskChecklistAnswerPhoto.IsActive = true;
            Context.TaskChecklistAnswerPhotos.Add(TaskChecklistAnswerPhoto);
            var file = Context.Files.Where(f => f.FilePK == model.FileID).Single();
            file.Longitude = model.Longitude;
            file.Latitude = model.Latitude;
            Context.SaveChanges();
            return model;
        }
        public TaskPhotoModel UploadTaskPhoto(TaskPhotoModel model)
        {
            var taskPhoto = new TaskPhoto();
            taskPhoto.TaskFK = model.TaskID;
            taskPhoto.FileFK = model.FileID;
            taskPhoto.CreatedByID = _userContext.UserDetails.ID;
            taskPhoto.CreatedDate = DateTime.Now;
            taskPhoto.UpdatedByID = _userContext.UserDetails.ID;
            taskPhoto.UpdatedDate = DateTime.Now;
            taskPhoto.TaskPhotoPK = Guid.NewGuid();
            taskPhoto.IsActive = true;
            Context.TaskPhotos.Add(taskPhoto);
            var file = Context.Files.Where(f => f.FilePK == model.FileID).Single();
            file.Longitude = model.Longitude;
            file.Latitude  = model.Latitude;
            Context.SaveChanges();
            return model;
        }

        public void UploadCleansingLogPhoto(CleansingLogPhotoModel model)
        {    
            var file = Context.Files.Where(f => f.FilePK == model.FileID).Single();
            file.Longitude = model.Longitude;
            file.Latitude = model.Latitude;
            file.Comments  = model.Comments;

            var CleansingLogPhoto = new CleansingLogPhoto();
            CleansingLogPhoto.CleansingLogPhotoPK = Guid.NewGuid(); 
            CleansingLogPhoto.CleansingLogFK = model.CleansingLogID;
            CleansingLogPhoto.FileFK = model.FileID;
            CleansingLogPhoto.CreatedByFK = _userContext.UserDetails.ID;
            CleansingLogPhoto.CreatedDate = DateTime.Now;
            CleansingLogPhoto.UpdatedByFK = _userContext.UserDetails.ID;
            CleansingLogPhoto.UpdatedDate = DateTime.Now;
            CleansingLogPhoto.IsActive = true;
            Context.CleansingLogPhotos.Add(CleansingLogPhoto);

            Context.SaveChanges();
        }

        public void UploadUnloadingProcessPhoto(UnloadingProcessPhotoModel model)
        {
            var file = Context.Files.Where(f => f.FilePK == model.FileID).Single();
            file.Longitude = model.Longitude;
            file.Latitude = model.Latitude;
            file.Comments = model.Comments;

            var UnloadingProcessPhoto = new UnloadingProcessPhoto();
            UnloadingProcessPhoto.UnloadingProcessPhotoPK = Guid.NewGuid();
            UnloadingProcessPhoto.UnloadingProcessFK = model.UnloadingProcessID;
            UnloadingProcessPhoto.FileFK = model.FileID;
            UnloadingProcessPhoto.CreatedByFK = _userContext.UserDetails.ID;
            UnloadingProcessPhoto.CreatedDate = DateTime.Now;
            UnloadingProcessPhoto.UpdatedByFK = _userContext.UserDetails.ID;
            UnloadingProcessPhoto.UpdatedDate = DateTime.Now;
            UnloadingProcessPhoto.IsActive = true;
            Context.UnloadingProcessPhotos.Add(UnloadingProcessPhoto);

            Context.SaveChanges();
        }

        public void UpdateUnloadingProcess(UnloadingProcessModel model)
        {


            var UnloadingProcess = Context.UnloadingProcesses.Where(p => p.UnloadingProcessPK.Equals(model.UnloadingProcessID)).Single();
            UnloadingProcess.LeaveDisposalSiteDate = model.LeaveDisposalSiteDate;
            UnloadingProcess.ArriveAtDisposalSiteDate = model.ArriveAtDisposalSiteDate;
            UnloadingProcess.DepartToDisposalSiteDate = model.DepartToDisposalSiteDate;
            UnloadingProcess.Comments = model.Comments;
            UnloadingProcess.WasteTicketNumber = model.WasteTicketNumber;
            UnloadingProcess.VolumeOfWasteDisposed = model.VolumeOfWasteDisposed;
            Context.SaveChanges();
        }

        public bool UpdateAssets(Guid taskID, Guid assetID, Guid equipmentID, string itemNumber, string assetNumber)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                try
                {
                    if (assetID == Guid.Empty)
                    {
                        var taskEquipmentID = Context.TaskEquipments.Where(t => t.TaskFK == taskID && t.EquipmentFK == equipmentID).Select(s => s.TaskEquipmentPK).FirstOrDefault();
                        TaskEquipmentAsset ass = new TaskEquipmentAsset {
                            TaskEquipmentAssetPK = Guid.NewGuid(),
                            TaskEquipmentFK = taskEquipmentID,
                            ItemNumber = int.Parse(itemNumber),
                            AssetNumber = assetNumber
                        };

                        Context.TaskEquipmentAssets.Add(ass);
                        SaveChanges();

                        var taskEquipment = Context.TaskEquipments.Where(t => t.TaskEquipmentPK == taskEquipmentID).SingleOrDefault();
                        if (taskEquipment != null)
                        {
                            taskEquipment.Quantity = Context.TaskEquipmentAssets.Where(a => a.TaskEquipmentFK == taskEquipmentID).Count();
                            SaveChanges();
                        }

                        tran.Commit();
                    }
                    else
                    {
                        var asset = Context.TaskEquipmentAssets.Where(x => x.TaskEquipmentAssetPK == assetID).SingleOrDefault();
                        if (asset != null)
                        {
                            asset.AssetNumber = assetNumber;
                            SaveChanges();

                            tran.Commit();
                        }
                        else return false;
                    }

                } catch (Exception e)
                {
                    _errorService.CreateErrorLog("UpdateAssetNumber", e, assetID.ToString());
                    return false;
                }
            }                           
            return true;
        }

        public bool RemoveAssets(Guid equipmentID, List<Guid> assetIDs)
        {
            var existingAssets = Context.TaskEquipmentAssets.Where(e => e.TaskEquipmentFK == equipmentID).Select(s => s.TaskEquipmentAssetPK).ToList();
            foreach (var existingAsset in existingAssets)
            {
                if (!assetIDs.Contains(existingAsset))
                {
                    using (var tran = Context.Database.BeginTransaction())
                    {
                        try
                        {
                            var asset = Context.TaskEquipmentAssets.Where(a => a.TaskEquipmentAssetPK == existingAsset).SingleOrDefault();
                            Context.TaskEquipmentAssets.Remove(asset);
                            SaveChanges();

                            var taskEquipment = Context.TaskEquipments.Where(t => t.TaskEquipmentPK == equipmentID).SingleOrDefault();
                            if (taskEquipment != null)
                            {
                                taskEquipment.Quantity = Context.TaskEquipmentAssets.Where(a => a.TaskEquipmentFK == equipmentID).Count();
                                SaveChanges();
                            }

                            tran.Commit();

                        } catch (Exception e)
                        {
                            _errorService.CreateErrorLog("RemoveInstalledAsset", e, existingAsset.ToString());
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public bool UpdateEquipmentQuantity(Guid equipmentID, int quantity)
        {
            var equip = Context.TaskEquipments.Where(e => e.TaskEquipmentPK == equipmentID).SingleOrDefault();

            if (equip != null)
            {
                using (var tran = Context.Database.BeginTransaction())
                {
                    try
                    {
                        equip.Quantity = quantity;

                        SaveChanges();
                        tran.Commit();

                        return true;
                    }
                    catch (Exception e)
                    {
                        _errorService.CreateErrorLog("UpdateTaskEuipmentQuantity", e, equipmentID.ToString());
                        return false;
                    }
                }
                
            }
            return false;
        }
    }
}
