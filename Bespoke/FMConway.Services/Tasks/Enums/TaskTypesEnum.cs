﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Enums
{
    public class TaskTypesEnum
    {
        public static string TrafficCount = "Traffic Count";
        public static string AreaCall = "Area Call";
        public static string Checklist = "Checklist";
        public static string Install = "Install";
        public static string FreeText = "Free Text";
        public static string FreeTextAndDetails = "Free Text and Details";
        public static string Signature = "Signature";
        public static string EquipmentInstall = "Equipment Install";
        public static string EquipmentCollection = "Equipment Collection";
        public static string Photos = "Photos";
        public static string Weather = "Weather";
        public static string ActivityDetails = "Activity Details";
        public static string CleansingLog = "Cleansing Log";
        public static string UnloadingProcess = "Unloading Process";
    }
}
