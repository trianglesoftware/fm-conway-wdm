﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Extensions
{
    public static class TaskExtensions
    {
        public static IQueryable<Task> TaskOverviewQuery(this IQueryable<Task> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }

        public static IQueryable<vv_TaskChecklists> TaskChecklistAnswersOverviewQuery(this IQueryable<vv_TaskChecklists> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ChecklistQuestion.Contains(query));
            }

            return model;
        }

        public static IQueryable<TaskSignature> TaskSignatureOverviewQuery(this IQueryable<TaskSignature> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Employee.EmployeeName.Contains(query));
            }

            return model;
        }

        public static IQueryable<TaskPhoto> TaskPhotoQuery(this IQueryable<TaskPhoto> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.File.FileDownloadName.Contains(query));
            }

            return model;
        }

        public static IQueryable<TaskChecklistAnswerPhoto> TaskChecklistPhotoQuery(this IQueryable<TaskChecklistAnswerPhoto> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.File.FileDownloadName.Contains(query));
            }

            return model;
        }
    }
}
