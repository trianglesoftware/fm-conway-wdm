﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class CleansingLogModel
    {
        public Guid CleansingLogID { get; set; }
        public Guid TaskID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public string Road { get; set; }
        public string Section { get; set; }
        public Guid? LaneID { get; set; }
        public string LaneLookup { get; set; }
        public Guid? DirectionID { get; set; }
        public string DirectionLookup { get; set; }
        public string StartMarker { get; set; }
        public string EndMarker { get; set; }
        public decimal? GulliesCleaned { get; set; }
        public decimal? GulliesMissed { get; set; }
        public decimal? CatchpitsCleaned { get; set; }
        public decimal? CatchpitsMissed { get; set; }
        public decimal? Channels { get; set; }
        public decimal? SlotDrains { get; set; }
        public decimal? Defects { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedDate { get; set; }

        public static Expression<Func<CleansingLog, CleansingLogModel>> EntityToModel = a => new CleansingLogModel
        {
            CleansingLogID = a.CleansingLogPK,
            TaskID = a.TaskFK,
            WorkInstructionID = a.WorkInstructionFK,
            Road = a.Road,
            Section = a.Section,
            LaneID = a.LaneFK,
            LaneLookup = a.Lane.Name,
            DirectionLookup = a.Direction.Name,
            StartMarker = a.StartMarker,
            EndMarker = a.EndMarker,
            GulliesCleaned = a.GulliesCleaned,
            GulliesMissed = a.GulliesMissed,
            CatchpitsCleaned = a.CatchpitsCleaned,
            CatchpitsMissed = a.CatchpitsMissed,
            Channels = a.Channels,
            SlotDrains = a.SlotDrains,
            Defects = a.Defects,
            Comments = a.Comments,
            CreatedDate = a.CreatedDate,
        };
    }
}
