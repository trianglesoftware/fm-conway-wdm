﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class TaskActivityDetailsModel
    {
        public Guid ActivityPriorityID { get; set; }    
        public Guid WorkInstructionID { get; set; }
        public string SiteName { get; set; }
        public string ClientReferenceNumber { get; set; }
        public string ActivityPriorityLookup { get; set; }
        public string Operator { get; set; }

        public static Expression<Func<WorkInstruction, TaskActivityDetailsModel>> EntityToModel = a => new TaskActivityDetailsModel
        {
            WorkInstructionID = a.WorkInstructionPK,
            SiteName = a.SiteName,
            ClientReferenceNumber = a.ClientReferenceNumber,
            ActivityPriorityLookup = a.ActivityPriority.Name,
            ActivityPriorityID = a.ActivityPriorityFK.Value,
            Operator = a.Operator,
        };
    }
}
