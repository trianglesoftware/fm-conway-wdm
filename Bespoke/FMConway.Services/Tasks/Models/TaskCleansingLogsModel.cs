﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class TaskCleansingLogsModel
    {
        public Guid CleansingLogID { get; set; }
        public string Road { get; set; }
        public string Section { get; set; }
        public DateTime CreatedDate { get; set; }

        public static Expression<Func<CleansingLog, TaskCleansingLogsModel>> EntityToModel = a => new TaskCleansingLogsModel
        {
            CleansingLogID = a.CleansingLogPK,
            Road = a.Road,
            Section = a.Section,
            CreatedDate = a.CreatedDate,
        };
    }
}
