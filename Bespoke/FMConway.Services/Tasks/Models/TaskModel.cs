﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class TaskModel
    {
        public Guid TaskID { get; set; }
        public string Name { get; set; }
        public int WINumber { get; set; }
        public int TaskOrder { get; set; }
        public string TaskType { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Notes { get; set; }
        public Guid WorkInstructionID { get; set; }
        public Guid? ChecklistID { get; set; }
        public Guid? AreaCallAreaID { get; set; }
        public bool PhotosRequired { get; set; }
        public string Tag { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string Weather { get; set; }

        public static Expression<Func<Task, TaskModel>> EntityToModel = entity => new TaskModel 
        {
            TaskID = entity.TaskPK,
            Name = entity.Name,
            WINumber = entity.WorkInstruction.WorkInstructionNumber,
            TaskOrder = entity.TaskOrder,
            TaskType = entity.TaskType.TaskType1,
            StartTime = entity.StartTime,
            EndTime = entity.EndTime,
            Notes = entity.Notes,
            WorkInstructionID = entity.WorksInstructionFK,
            ChecklistID = entity.ChecklistFK,
            AreaCallAreaID = entity.AreaCallAreaFK,
            PhotosRequired = entity.PhotosRequired,
            Tag = entity.Tag,
            Longitude = entity.Longitude ?? 0.00m,
            Latitude = entity.Latitude ?? 0.00m,
            Weather = entity.WeatherCondition.Name
        };
    }

    public class AreaCallModel
    {
        public string PersonsName { get; set; }
        public string Reference { get; set; }
        public string ExtraDetails { get; set; }

        public string CallProtocol { get; set; }
        public string RCCNumber { get; set; }
        public string NCCNumber { get; set; }
    }

    public class TaskSignatureModel
    {
        public Guid TaskID { get; set; }
        public Guid TaskSignatureID { get; set; }
        public Guid? EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public Guid FileID { get; set; }
        public bool IsClient { get; set; }
        public byte[] Data { get; set; }

        public static Expression<Func<TaskSignature, TaskSignatureModel>> ETM = e => new TaskSignatureModel
        {
            TaskID = e.TaskFK,
            TaskSignatureID = e.TaskSignaturePK,
            EmployeeID = e.EmployeeFK,
            EmployeeName = (e.PrintedName != null && e.PrintedName.Trim() != "") ? e.PrintedName : (e.EmployeeFK.HasValue ? e.Employee.EmployeeName : ""),
            FileID = e.FileFK,
            IsClient = e.IsClient
        };

        public static Expression<Func<TaskSignature, TaskSignatureModel>> ETMFile = e => new TaskSignatureModel
        {
            TaskID = e.TaskFK,
            TaskSignatureID = e.TaskSignaturePK,
            EmployeeID = e.EmployeeFK,
            EmployeeName = (e.PrintedName != null && e.PrintedName.Trim() != "") ? e.PrintedName : (e.EmployeeFK.HasValue ? e.Employee.EmployeeName : ""),
            FileID = e.FileFK,
            IsClient = e.IsClient,
            Data = e.File.Data
        };
    }

    public class TaskPhotoModel
    {
        public Guid TaskID { get; set; }
        public Guid TaskPhotoID { get; set; }
        public Guid FileID { get; set; }
        public string Name { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public byte[] Data { get; set; }
        public string Comments { get; set; }

        public static Expression<Func<TaskPhoto, TaskPhotoModel>> ETM = e => new TaskPhotoModel() 
        {
            TaskID = e.TaskFK,
            TaskPhotoID = e.TaskPhotoPK,
            FileID = e.FileFK,
            Name = e.File.FileDownloadName,
            Longitude = e.File.Longitude ?? 0.00m,
            Latitude = e.File.Latitude ?? 0.00m,
            Comments = e.File.Comments,
        };

        public static Expression<Func<TaskPhoto, TaskPhotoModel>> ETMFile = e => new TaskPhotoModel()
        {
            TaskID = e.TaskFK,
            TaskPhotoID = e.TaskPhotoPK,
            FileID = e.FileFK,
            Name = e.File.FileDownloadName,
            Longitude = e.File.Longitude ?? 0.00m,
            Latitude = e.File.Latitude ?? 0.00m,
            Data = e.File.Data,
            Comments = e.File.Comments,
        };

        public static Expression<Func<TaskChecklistAnswerPhoto, TaskPhotoModel>> TaskChecklistAnswerPhotoToModel = e => new TaskPhotoModel
        {
            TaskID = e.TaskChecklistAnswer.TaskChecklist.TaskFK,
            TaskPhotoID = e.TaskChecklistAnswerPhotoPK,
            FileID = e.FileFK,
            Name = e.File.FileDownloadName,
            Longitude = e.File.Longitude ?? 0.00m,
            Latitude = e.File.Latitude ?? 0.00m,
            Comments = e.File.Comments,
        };
    }
}
