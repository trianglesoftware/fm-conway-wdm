﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class CleansingLogPhotoModel
    {
        public Guid CleansingLogPhotoID { get; set; }
        public Guid CleansingLogID { get; set; }
        public Guid FileID { get; set; }
        public string Name { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public byte[] Data { get; set; }
        public string Comments { get; set; }

        public static Expression<Func<CleansingLogPhoto, CleansingLogPhotoModel>> EntityToModel = a => new CleansingLogPhotoModel
        {
            CleansingLogPhotoID = a.CleansingLogPhotoPK,
            CleansingLogID = a.CleansingLogFK,
            FileID = a.FileFK,
            Name = a.File.FileDownloadName,
            Longitude = a.File.Longitude ?? 0.00m,
            Latitude = a.File.Latitude ?? 0.00m,
            Comments = a.File.Comments,
        };
    }
}
