﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class TaskActivityRiskAssessmentModel
    {
        public Guid ActivityRiskAssessmentID { get; set; }
        public Guid TaskID { get; set; }
        public Guid CleansingLogID { get; set; }
        public string RoadName { get; set; }
        public Guid RoadSpeedID { get; set; }
        public string RoadSpeed { get; set; }
        public Guid TMRequirementID { get; set; }
        public string TMRequirement { get; set; }
        public bool Schools { get; set; }
        public bool PedestrianCrossings { get; set; }
        public bool TreeCanopies { get; set; }
        public bool WaterCourses { get; set; }
        public bool OverheadCables { get; set; }
        public bool AdverseWeather { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedDate { get; set; }

        public static Expression<Func<ActivityRiskAssessment, TaskActivityRiskAssessmentModel>> ETM = e => new TaskActivityRiskAssessmentModel
        {
            ActivityRiskAssessmentID = e.ActivityRiskAssessmentPK, 
            TaskID = e.TaskFK,
            CleansingLogID = e.CleansingLogFK,
            RoadName = e.RoadName,
            RoadSpeed = e.RoadSpeed.Speed,
            TMRequirement = e.TMRequirement.Name,
            Schools = e.Schools, 
            PedestrianCrossings = e.PedestrianCrossings, 
            TreeCanopies = e.TreeCanopies,
            WaterCourses = e.WaterCourses,
            OverheadCables = e.OverheadCables,
            AdverseWeather = e.AdverseWeather,
            Notes = e.Notes,
            CreatedDate = e.CreatedDate,
        };
    }
}
