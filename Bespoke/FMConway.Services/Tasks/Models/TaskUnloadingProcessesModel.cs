﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class TaskUnloadingProcessesModel
    {
        public Guid UnloadingProcessID { get; set; }
        public string WasteTicketNumber { get; set; }
        public Decimal? VolumeOfWasteDisposed { get; set; }
        public DateTime? Completed { get; set; }

        public static Expression<Func<UnloadingProcess, TaskUnloadingProcessesModel>> EntityToModel = a => new TaskUnloadingProcessesModel
        {
            UnloadingProcessID = a.UnloadingProcessPK,
            WasteTicketNumber = a.WasteTicketNumber,
            VolumeOfWasteDisposed = a.VolumeOfWasteDisposed,
            Completed = a.LeaveDisposalSiteDate,
        };
    }
}
