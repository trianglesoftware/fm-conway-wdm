﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class UnloadingProcessModel
    {
        public Guid UnloadingProcessID { get; set; }
        public Guid TaskID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public DateTime? DepartToDisposalSiteDate { get; set; }
        public DateTime? ArriveAtDisposalSiteDate { get; set; }
        public Guid? DisposalSiteFK { get; set; }
        public string DisposalSiteLookup { get; set; }
        public string WasteTicketNumber { get; set; }
        public decimal? VolumeOfWasteDisposed { get; set; }
        public string Comments { get; set; }
        public DateTime? LeaveDisposalSiteDate { get; set; }

        public static Expression<Func<UnloadingProcess, UnloadingProcessModel>> EntityToModel = a => new UnloadingProcessModel
        {
            UnloadingProcessID = a.UnloadingProcessPK,
            TaskID = a.TaskFK,
            WorkInstructionID = a.WorkInstructionFK,
            DepartToDisposalSiteDate = a.DepartToDisposalSiteDate,
            ArriveAtDisposalSiteDate = a.ArriveAtDisposalSiteDate,
            DisposalSiteFK = a.DisposalSiteFK,
            DisposalSiteLookup = a.DisposalSite.Name,
            WasteTicketNumber = a.WasteTicketNumber,
            VolumeOfWasteDisposed = a.VolumeOfWasteDisposed,
            Comments = a.Comments,
            LeaveDisposalSiteDate = a.LeaveDisposalSiteDate,
        };
    }
}
