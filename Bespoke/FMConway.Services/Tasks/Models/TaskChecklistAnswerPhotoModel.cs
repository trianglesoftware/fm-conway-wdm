﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Tasks.Models
{
    public class TaskChecklistAnswerPhotoModel
    {
        public Guid TaskChecklistAnswerPhotoID { get; set; }
        public Guid TaskChecklistAnswerID { get; set; }
        public Guid FileID { get; set; }
        public string Name { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public byte[] Data { get; set; }
        public string Comments { get; set; }

        public static Expression<Func<TaskChecklistAnswerPhoto, TaskChecklistAnswerPhotoModel>> EntityToModel = a => new TaskChecklistAnswerPhotoModel
        {
            TaskChecklistAnswerPhotoID = a.TaskChecklistAnswerPhotoPK,
            TaskChecklistAnswerID = a.TaskChecklistAnswerFK,
            FileID = a.FileFK,
            Name = a.File.FileDownloadName,
            Longitude = a.File.Longitude ?? 0.00m,
            Latitude = a.File.Latitude ?? 0.00m,
            Comments = a.File.Comments,
        };
    }
}
