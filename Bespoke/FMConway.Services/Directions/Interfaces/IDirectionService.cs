﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Lanes.Models;

namespace FMConway.Services.Directions.Interfaces
{
    public interface IDirectionService : IService
    {
        PagedResults<DirectionLookup> GetPagedActiveDirectionsLookup(int rowCount, int page, string query);
    }
}
