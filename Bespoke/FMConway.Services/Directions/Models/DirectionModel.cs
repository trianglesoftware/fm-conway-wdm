﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.Lanes.Models
{
    public class DirectionModel
    {
        public Guid DirectionID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Direction, DirectionModel>> EntityToModel = entity => new DirectionModel
        {
            DirectionID = entity.DirectionPK,
            Name = entity.Name,
            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };
    }
}
