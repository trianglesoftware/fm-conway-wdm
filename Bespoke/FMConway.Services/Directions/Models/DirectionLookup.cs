﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Lanes.Models
{
    public class DirectionLookup : LookupBase<Guid>
    {
        public static Expression<Func<Direction, DirectionLookup>> EntityToModel = entity => new DirectionLookup()
        {
            Key = entity.Name,
            Value = entity.DirectionPK
        };
    }
}
