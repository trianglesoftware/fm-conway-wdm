﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Depots.Extensions;
using FMConway.Services.Lanes.Models;
using FMConway.Services.Lanes.Extensions;
using FMConway.Services.Directions.Extensions;

namespace FMConway.Services.Directions.Services
{
    public class DirectionService : Service, FMConway.Services.Directions.Interfaces.IDirectionService
    {
        UserContext _userContext;

        public DirectionService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public PagedResults<DirectionLookup> GetPagedActiveDirectionsLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Directions.OrderBy(o => o.Name).DirectionOverviewQuery(query).Select(DirectionLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<DirectionLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
            };
        }
    }
}
