﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Directions.Extensions
{
    public static class DirectionExtensions
    {
        public static IQueryable<Direction> DirectionOverviewQuery(this IQueryable<Direction> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}

