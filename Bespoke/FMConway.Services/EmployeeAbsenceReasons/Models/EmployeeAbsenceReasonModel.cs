﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.EmployeeAbsenceReasons.Models
{
    public class EmployeeAbsenceReasonModel
    {
        public Guid EmployeeAbsenceReasonID { get; set; }
        [Required(ErrorMessage = "Code is required")]
        public string Code { get; set; }
        public string Description { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<EmployeeAbsenceReason, EmployeeAbsenceReasonModel>> EntityToModel = entity => new EmployeeAbsenceReasonModel
        {
            EmployeeAbsenceReasonID = entity.EmployeeAbsenceReasonPK,
            Code = entity.Code,
            Description = entity.Description,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<EmployeeAbsenceReasonModel, EmployeeAbsenceReason> ModelToEntity = model => new EmployeeAbsenceReason
        {
            EmployeeAbsenceReasonPK = model.EmployeeAbsenceReasonID,
            Code = model.Code,
            Description = model.Description,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
