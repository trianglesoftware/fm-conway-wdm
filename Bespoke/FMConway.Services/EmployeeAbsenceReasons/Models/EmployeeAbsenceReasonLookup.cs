﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.EmployeeAbsenceReasons.Models
{
    public class EmployeeAbsenceReasonLookup : LookupBase<Guid>
    {
        public string Description { get; set; }

        public static Expression<Func<EmployeeAbsenceReason, EmployeeAbsenceReasonLookup>> EntityToModel = entity => new EmployeeAbsenceReasonLookup
        {
            Value = entity.EmployeeAbsenceReasonPK,
            Key = entity.Code,

            Description = entity.Description
        };
    }
}
