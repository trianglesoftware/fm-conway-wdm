﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.EmployeeAbsenceReasons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.EmployeeAbsenceReasons.Interfaces
{
    public interface IEmployeeAbsenceReasonService : IService
    {
        void Create(EmployeeAbsenceReasonModel model);
        List<EmployeeAbsenceReasonModel> GetActiveEmployeeAbsenceReasons();
        List<EmployeeAbsenceReasonModel> GetInactiveEmployeeAbsenceReasons();
        EmployeeAbsenceReasonModel Single(Guid id);
        void Update(EmployeeAbsenceReasonModel model);

        PagedResults<EmployeeAbsenceReasonLookup> GetPagedActiveEmployeeAbsenceReasonsLookup(int rowCount, int page, string query);
    }
}
