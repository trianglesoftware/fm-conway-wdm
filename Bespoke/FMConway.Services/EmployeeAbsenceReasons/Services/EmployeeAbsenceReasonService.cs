﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.EmployeeAbsenceReasons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.EmployeeAbsenceReasons.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.EmployeeAbsenceReasons.Services
{
    public class EmployeeAbsenceReasonService : Service, FMConway.Services.EmployeeAbsenceReasons.Interfaces.IEmployeeAbsenceReasonService
    {
        UserContext _userContext;

        public EmployeeAbsenceReasonService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<EmployeeAbsenceReasonModel> GetActiveEmployeeAbsenceReasons()
        {
            return Context.EmployeeAbsenceReasons.Where(a => a.IsActive).Select(EmployeeAbsenceReasonModel.EntityToModel).ToList();
        }

        public List<EmployeeAbsenceReasonModel> GetInactiveEmployeeAbsenceReasons()
        {
            return Context.EmployeeAbsenceReasons.Where(a => !a.IsActive).Select(EmployeeAbsenceReasonModel.EntityToModel).ToList();
        }

        public void Create(EmployeeAbsenceReasonModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.EmployeeAbsenceReasonID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.EmployeeAbsenceReasons.Add(EmployeeAbsenceReasonModel.ModelToEntity(model));

            SaveChanges();
        }

        public void Update(EmployeeAbsenceReasonModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var current = Context.EmployeeAbsenceReasons.Where(x => x.EmployeeAbsenceReasonPK.Equals(model.EmployeeAbsenceReasonID)).SingleOrDefault();
            current.Code = model.Code;
            current.Description = model.Description;
            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;

            SaveChanges();
        }

        public EmployeeAbsenceReasonModel Single(Guid id)
        {
            return Context.EmployeeAbsenceReasons.Where(a => a.EmployeeAbsenceReasonPK.Equals(id)).Select(EmployeeAbsenceReasonModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<EmployeeAbsenceReasonLookup> GetPagedActiveEmployeeAbsenceReasonsLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.EmployeeAbsenceReasons.ActiveEmployeeAbsenceReasons().OrderBy(o => o.Code).EmployeeAbsenceReasonOverviewQuery(query).Select(EmployeeAbsenceReasonLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<EmployeeAbsenceReasonLookup>
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
