﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.EmployeeAbsenceReasons.Extensions
{
    public static class EmployeeAbsenceReasonExtensions
    {
        public static IQueryable<EmployeeAbsenceReason> ActiveEmployeeAbsenceReasons(this IQueryable<EmployeeAbsenceReason> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<EmployeeAbsenceReason> InactiveEmployeeAbsenceReasons(this IQueryable<EmployeeAbsenceReason> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<EmployeeAbsenceReason> EmployeeAbsenceReasonOverviewQuery(this IQueryable<EmployeeAbsenceReason> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Code.Contains(query));
            }

            return model;
        }
    }
}
