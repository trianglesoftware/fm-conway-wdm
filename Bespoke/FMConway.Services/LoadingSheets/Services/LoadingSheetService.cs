﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.LoadingSheets.Models;
using FMConway.Services.LoadingSheets.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.LoadingSheets.Reports;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.WorkInstructions.Enums;

namespace FMConway.Services.LoadingSheets.Services
{
    public class LoadingSheetService : Service, FMConway.Services.LoadingSheets.Interfaces.ILoadingSheetService
    {
        public ILoadingSheetStatusService Status { get; set; }

        UserContext _userContext;

        IErrorLoggingService _errorLoggingService;

        public LoadingSheetService(UserContext userContext, IErrorLoggingService errorLoggingService)
        {
            _userContext = userContext;
            _errorLoggingService = errorLoggingService;
        }

        public Guid Create(LoadingSheetModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.LoadingSheetID = Guid.NewGuid();

            var topRecord = Context.LoadingSheets.OrderByDescending(x => x.LoadingSheetNumber).FirstOrDefault();
            if (topRecord != null)
            {
                model.LoadingSheetNumber = topRecord.LoadingSheetNumber + 1;
            }
            else
            {
                model.LoadingSheetNumber = 1;
            }

            //model.Site = model.Site == null ? "" : model.Site;
            model.Name = model.Name == null ? "" : model.Name;
            model.Comments = model.Comments == null ? "" : model.Comments;

            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.LoadingSheets.Add(LoadingSheetModel.ModelToEntity(model));

            SaveChanges();

            return model.LoadingSheetID;
        }

        public Guid CreateFromTemplate(Guid templateID, Guid workInstructionID)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            LoadingSheetTemplate template = Context.LoadingSheetTemplates.Where(x => x.LoadingSheetTemplatePK.Equals(templateID)).SingleOrDefault();

            LoadingSheet loadingSheet = new LoadingSheet();
            loadingSheet.LoadingSheetPK = Guid.NewGuid();

            var topRecord = Context.LoadingSheets.OrderByDescending(x => x.LoadingSheetNumber).FirstOrDefault();
            if (topRecord != null)
            {
                loadingSheet.LoadingSheetNumber = topRecord.LoadingSheetNumber + 1;
            }
            else
            {
                loadingSheet.LoadingSheetNumber = 1;
            }

            loadingSheet.Name = template.Name;
            loadingSheet.WorkInstructionFK = workInstructionID;
            loadingSheet.LoadingSheetDate = DateTime.Today;
            //loadingSheet.VehicleTypeFK = template.VehicleTypeFK;
            loadingSheet.Comments = "";
            loadingSheet.EmployeeFK = Context.Employees.Where(x => x.UserProfiles.Any(u => u.UserFK.Equals(_userContext.UserDetails.ID))).Select(x => x.EmployeePK).SingleOrDefault();
            //loadingSheet.Site = "";
            loadingSheet.StatusFK = Context.Status.Where(x => x.IsLoadingSheetStatus && x.Status1.Equals("Current")).SingleOrDefault().StatusPK;
            loadingSheet.IsActive = true;
            loadingSheet.CreatedByFK = _userContext.UserDetails.ID;
            loadingSheet.CreatedDate = DateTime.Now;
            loadingSheet.UpdatedByFK = _userContext.UserDetails.ID;
            loadingSheet.UpdatedDate = DateTime.Now;

            Context.LoadingSheets.Add(loadingSheet);

            foreach (var templateEquipment in template.LoadingSheetTemplateEquipments.Where(x => x.IsActive))
	        {
		        LoadingSheetEquipment equipment = new LoadingSheetEquipment();
                equipment.LoadingSheetEquipmentPK = Guid.NewGuid();
                equipment.LoadingSheetFK = loadingSheet.LoadingSheetPK;
                equipment.NonStandardEquipment = templateEquipment.NonStandardEquipment;
                equipment.IsNonStandard = templateEquipment.IsNonStandard;
                equipment.EquipmentFK = templateEquipment.EquipmentFK;
                equipment.Quantity = templateEquipment.Quantity;
                equipment.SheetOrder = templateEquipment.SheetOrder;
                equipment.IsActive = true;
                equipment.CreatedByFK = _userContext.UserDetails.ID;
                equipment.CreatedDate = DateTime.Now;
                equipment.UpdatedByFK = _userContext.UserDetails.ID;
                equipment.UpdatedDate = DateTime.Now;

                Context.LoadingSheetEquipments.Add(equipment);

                var tasks = Context.Tasks.Where(t => t.WorksInstructionFK == workInstructionID);
                if (tasks != null)
                {
                    foreach (var task in tasks.Where(x => x.TaskTypeFK == 8 /*Equipment Install*/))
                    {
                        TaskEquipment taskEquip = new TaskEquipment()
                        {
                            TaskEquipmentPK = Guid.NewGuid(),
                            TaskFK = task.TaskPK,
                            EquipmentFK = templateEquipment.EquipmentFK.GetValueOrDefault(),
                            Quantity = Decimal.ToInt32(templateEquipment.Quantity),
                            IsActive = true,
                            CreatedByID = _userContext.UserDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByID = _userContext.UserDetails.ID,
                            UpdatedDate = DateTime.Now
                        };

                        Context.TaskEquipments.Add(taskEquip);

                        }
                    }
                }

            SaveChanges();

            return loadingSheet.LoadingSheetPK;
        }

        public void CreateDuplicateLoadingSheets(Guid parentWorkInstructionID, Guid workInstructionID)
        {
            try
            {
                var loadingSheets = Context.LoadingSheets.Where(l => l.WorkInstructionFK.Equals(parentWorkInstructionID)).ToList();

                foreach (var ls in loadingSheets)
                {
                    LoadingSheet loadingSheet = new LoadingSheet();
                    loadingSheet.LoadingSheetPK = Guid.NewGuid();

                    var topRecord = Context.LoadingSheets.OrderByDescending(x => x.LoadingSheetNumber).FirstOrDefault();
                    if (topRecord != null)
                    {
                        loadingSheet.LoadingSheetNumber = topRecord.LoadingSheetNumber + 1;
                    }
                    else
                    {
                        loadingSheet.LoadingSheetNumber = 1;
                    }

                    loadingSheet.WorkInstructionFK = workInstructionID;
                    loadingSheet.LoadingSheetDate = DateTime.Today;
                    //loadingSheet.VehicleTypeFK = template.VehicleTypeFK;
                    loadingSheet.Comments = "";
                    loadingSheet.EmployeeFK = Context.Employees.Where(x => x.UserProfiles.Any(u => u.UserFK.Equals(_userContext.UserDetails.ID))).Select(x => x.EmployeePK).SingleOrDefault();
                    //loadingSheet.Site = "";
                    loadingSheet.StatusFK = Context.Status.Where(x => x.IsLoadingSheetStatus && x.Status1.Equals("Current")).SingleOrDefault().StatusPK;
                    loadingSheet.IsActive = true;
                    loadingSheet.CreatedByFK = _userContext.UserDetails.ID;
                    loadingSheet.CreatedDate = DateTime.Now;
                    loadingSheet.UpdatedByFK = _userContext.UserDetails.ID;
                    loadingSheet.UpdatedDate = DateTime.Now;

                    Context.LoadingSheets.Add(loadingSheet);

                    SaveChanges();

                    foreach (var item in ls.LoadingSheetEquipments)
                    {
                        LoadingSheetEquipment equipment = new LoadingSheetEquipment();
                        equipment.LoadingSheetEquipmentPK = Guid.NewGuid();
                        equipment.LoadingSheetFK = loadingSheet.LoadingSheetPK;
                        equipment.NonStandardEquipment = item.NonStandardEquipment;
                        equipment.IsNonStandard = item.IsNonStandard;
                        equipment.EquipmentFK = item.EquipmentFK;
                        equipment.Quantity = item.Quantity;
                        equipment.SheetOrder = item.SheetOrder;
                        equipment.IsActive = true;
                        equipment.CreatedByFK = _userContext.UserDetails.ID;
                        equipment.CreatedDate = DateTime.Now;
                        equipment.UpdatedByFK = _userContext.UserDetails.ID;
                        equipment.UpdatedDate = DateTime.Now;

                        Context.LoadingSheetEquipments.Add(equipment);

                        SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("DuplicateLoadingSheets", e, parentWorkInstructionID.ToString());
            }
        }

        public void Update(LoadingSheetModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            LoadingSheet current = Context.LoadingSheets.Where(x => x.LoadingSheetPK.Equals(model.LoadingSheetID)).Single();

            current.Name = model.Name == null ? "" : model.Name;
            current.LoadingSheetDate = model.LoadingSheetDate;
            //current.VehicleFK = model.VehicleID;
            //current.VehicleTypeFK = model.VehicleTypeID;
            current.Comments = model.Comments == null ? "" : model.Comments;
            current.EmployeeFK = model.EmployeeID;
            //current.Site = model.Site == null ? "" : model.Site;
            current.StatusFK = model.StatusID;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public void UpdateOperations(LoadingSheetModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            LoadingSheet current = Context.LoadingSheets.Where(x => x.LoadingSheetPK.Equals(model.LoadingSheetID)).Single();

            //current.VehicleFK = model.VehicleID;
            current.StatusFK = model.StatusID;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public LoadingSheetModel Single(Guid ID)
        {
            var model = Context.LoadingSheets.Where(a => a.LoadingSheetPK == ID).Select(LoadingSheetModel.EntityToModel).Single();

            var hasVmsOrAssetEquipment = Context.LoadingSheets
                .Where(a => a.LoadingSheetPK == ID)
                .SelectMany(a => a.LoadingSheetEquipments.Where(b => b.IsActive))
                .Any(a => a.Equipment.EquipmentType.EquipmentTypeVmsOrAssetFK != null);

            var hasVmsAndAssetChecklists = Context.Checklists.Where(a => (a.IsVmsChecklist || a.IsPreHireChecklist) && a.IsActive).Count() > 0;

            model.HasVmsOrAssetWithoutChecklist = hasVmsOrAssetEquipment && !hasVmsAndAssetChecklists;

            return model;
        }

        public PagedResults<LoadingSheetModel> GetActiveLoadingSheetsPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            var baseQuery = Context.LoadingSheets.Where(x => x.WorkInstructionFK.Equals(workInstructionID)).ActiveLoadingSheets().OrderBy(o => o.LoadingSheetNumber).LoadingSheetOverviewQuery(query).Select(LoadingSheetModel.EntityToModel);

            return PagedResults<LoadingSheetModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public void AddEquipment(List<string> selectedEquipment, decimal quantity, Guid loadingSheetID, int sheetOrder)
        {
            foreach (var e in selectedEquipment)
            {
                var currentEquipment = Context.LoadingSheetEquipments.Where(x => x.LoadingSheetFK.Equals(loadingSheetID) && x.SheetOrder >= sheetOrder).OrderBy(x => x.SheetOrder).ToList();

                bool updateOrder = false;
                foreach (var eq in currentEquipment)
                {
                    if (eq.SheetOrder == sheetOrder)
                        updateOrder = true;

                    if (updateOrder)
                        eq.SheetOrder++;
                }

                LoadingSheetEquipment equipment = new LoadingSheetEquipment();
                equipment.LoadingSheetEquipmentPK = Guid.NewGuid();
                equipment.LoadingSheetFK = loadingSheetID;
                equipment.EquipmentFK = new Guid(e);
                equipment.NonStandardEquipment = "";
                equipment.IsNonStandard = false;
                equipment.Quantity = quantity;
                equipment.SheetOrder = sheetOrder;
                equipment.IsActive = true;
                equipment.CreatedByFK = _userContext.UserDetails.ID;
                equipment.CreatedDate = DateTime.Now;
                equipment.UpdatedByFK = _userContext.UserDetails.ID;
                equipment.UpdatedDate = DateTime.Now;

                Context.LoadingSheetEquipments.Add(equipment);

                var tasks = Context.LoadingSheets.Where(x => x.LoadingSheetPK == loadingSheetID).SelectMany(s => s.WorkInstruction.Tasks).ToList();

                if (tasks != null)
                {
                    foreach (var task in tasks.Where(t => t.TaskTypeFK == 8) /*Equipment Install*/)
                    {
                        TaskEquipment taskEquip = new TaskEquipment()
                        {
                            TaskEquipmentPK = Guid.NewGuid(),
                            TaskFK = task.TaskPK,
                            EquipmentFK = equipment.EquipmentFK.GetValueOrDefault(),
                            Quantity = Decimal.ToInt32(quantity),
                            IsActive = true,
                            CreatedByID = _userContext.UserDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByID = _userContext.UserDetails.ID,
                            UpdatedDate = DateTime.Now
                        };

                        Context.TaskEquipments.Add(taskEquip);
                    }
                }
            }

            SaveChanges();
        }

        public void AddTemplateEquipment(List<string> selectedEquipment, decimal quantity, Guid loadingSheetTemplateID, int sheetOrder)
        {
            foreach (var e in selectedEquipment)
            {
                var currentEquipment = Context.LoadingSheetTemplateEquipments.Where(x => x.LoadingSheetTemplateFK.Equals(loadingSheetTemplateID) && x.SheetOrder >= sheetOrder).OrderBy(x => x.SheetOrder).ToList();

                bool updateOrder = false;
                foreach (var eq in currentEquipment)
                {
                    if (eq.SheetOrder == sheetOrder)
                        updateOrder = true;

                    if (updateOrder)
                        eq.SheetOrder++;
                }

                LoadingSheetTemplateEquipment equipment = new LoadingSheetTemplateEquipment();
                equipment.LoadingSheetTemplateEquipmentPK = Guid.NewGuid();
                equipment.LoadingSheetTemplateFK = loadingSheetTemplateID;
                equipment.EquipmentFK = new Guid(e);
                equipment.NonStandardEquipment = "";
                equipment.IsNonStandard = false;
                equipment.Quantity = quantity;
                equipment.SheetOrder = sheetOrder;
                equipment.IsActive = true;
                equipment.CreatedByFK = _userContext.UserDetails.ID;
                equipment.CreatedDate = DateTime.Now;
                equipment.UpdatedByFK = _userContext.UserDetails.ID;
                equipment.UpdatedDate = DateTime.Now;

                Context.LoadingSheetTemplateEquipments.Add(equipment);
            }

            SaveChanges();
        }

        public void AddNonStandardEquipment(Guid loadingSheetID, string equipmentName, decimal quantity, int sheetOrder)
        {
            var currentEquipment = Context.LoadingSheetEquipments.Where(x => x.LoadingSheetFK.Equals(loadingSheetID) && x.SheetOrder >= sheetOrder).OrderBy(x => x.SheetOrder).ToList();

            bool updateOrder = false;
            foreach (var eq in currentEquipment)
            {
                if (eq.SheetOrder == sheetOrder)
                    updateOrder = true;

                if (updateOrder)
                    eq.SheetOrder++;
            }

            LoadingSheetEquipment equipment = new LoadingSheetEquipment();
            equipment.LoadingSheetEquipmentPK = Guid.NewGuid();
            equipment.LoadingSheetFK = loadingSheetID;
            equipment.EquipmentFK = null;
            equipment.NonStandardEquipment = equipmentName;
            equipment.IsNonStandard = true;
            equipment.Quantity = quantity;
            equipment.SheetOrder = sheetOrder;
            equipment.IsActive = true;
            equipment.CreatedByFK = _userContext.UserDetails.ID;
            equipment.CreatedDate = DateTime.Now;
            equipment.UpdatedByFK = _userContext.UserDetails.ID;
            equipment.UpdatedDate = DateTime.Now;

            Context.LoadingSheetEquipments.Add(equipment);

            SaveChanges();
        }

        public void AddNonStandardTemplateEquipment(Guid loadingSheetTemplateID, string equipmentName, decimal quantity, int sheetOrder)
        {
            var currentEquipment = Context.LoadingSheetTemplateEquipments.Where(x => x.LoadingSheetTemplateFK.Equals(loadingSheetTemplateID) && x.SheetOrder >= sheetOrder).OrderBy(x => x.SheetOrder).ToList();

            bool updateOrder = false;
            foreach (var eq in currentEquipment)
            {
                if (eq.SheetOrder == sheetOrder)
                    updateOrder = true;

                if (updateOrder)
                    eq.SheetOrder++;
            }

            LoadingSheetTemplateEquipment equipment = new LoadingSheetTemplateEquipment();
            equipment.LoadingSheetTemplateEquipmentPK = Guid.NewGuid();
            equipment.LoadingSheetTemplateFK = loadingSheetTemplateID;
            equipment.EquipmentFK = null;
            equipment.NonStandardEquipment = equipmentName;
            equipment.IsNonStandard = true;
            equipment.Quantity = quantity;
            equipment.SheetOrder = sheetOrder;
            equipment.IsActive = true;
            equipment.CreatedByFK = _userContext.UserDetails.ID;
            equipment.CreatedDate = DateTime.Now;
            equipment.UpdatedByFK = _userContext.UserDetails.ID;
            equipment.UpdatedDate = DateTime.Now;

            Context.LoadingSheetTemplateEquipments.Add(equipment);

            SaveChanges();
        }

        public Guid RemoveEquipment(Guid loadingSheetEquipmentID)
        {
            var toRemove = Context.LoadingSheetEquipments.Where(x => x.LoadingSheetEquipmentPK.Equals(loadingSheetEquipmentID)).SingleOrDefault();

            Context.LoadingSheetEquipments.Remove(toRemove);

            SaveChanges();

            return toRemove.LoadingSheetFK;
        }

        public Guid RemoveTemplateEquipment(Guid loadingSheetTemplateEquipmentID)
        {
            var toRemove = Context.LoadingSheetTemplateEquipments.Where(x => x.LoadingSheetTemplateEquipmentPK.Equals(loadingSheetTemplateEquipmentID)).SingleOrDefault();

            Context.LoadingSheetTemplateEquipments.Remove(toRemove);

            SaveChanges();

            return toRemove.LoadingSheetTemplateFK;
        }

        public PagedResults<LoadingSheetModel> GetActiveLoadingSheetsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.LoadingSheets.ActiveLoadingSheets().OrderBy(o => o.LoadingSheetNumber).LoadingSheetOverviewQuery(query).Select(LoadingSheetModel.EntityToModel);

            return PagedResults<LoadingSheetModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<LoadingSheetModel> GetInactiveLoadingSheetsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.LoadingSheets.InactiveLoadingSheets().OrderBy(o => o.LoadingSheetNumber).LoadingSheetOverviewQuery(query).Select(LoadingSheetModel.EntityToModel);

            return PagedResults<LoadingSheetModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public byte[] Print(Guid ID)
        {
            LoadingSheetReportModel reportModel = Context.LoadingSheets.Where(x => x.LoadingSheetPK.Equals(ID)).Select(n => new LoadingSheetReportModel()
            {
                IssuedBy = n.Employee.EmployeeName,
                //Vehicle = n.VehicleFK.HasValue ? n.Vehicle.Make + " - " + n.Vehicle.Model + " - " + n.Vehicle.Registration : "",
                //Site = n.Site,
                ContractRef = n.WorkInstruction.Job.Contract.ContractTitle
            }).SingleOrDefault();

            reportModel.Equipment = Context.LoadingSheetEquipments.Where(x => x.LoadingSheetFK.Equals(ID) && x.IsActive).Select(LoadingSheetEquipmentModel.EntityToModel).ToList();

            LoadingSheetReport report = new LoadingSheetReport();

            return report.CreateReport(reportModel);
        }
    }
}
