﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Employees.Models;
using FMConway.Services.LoadingSheets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.LoadingSheets.Services
{
    public class LoadingSheetStatusService : Service, FMConway.Services.LoadingSheets.Interfaces.ILoadingSheetStatusService
    {
        UserContext _userContext;

        public LoadingSheetStatusService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<LoadingSheetStatusModel> GetLoadingSheetStatus()
        {
            return Context.Status.Where(x => x.IsLoadingSheetStatus).OrderBy(x => x.Status1).Select(LoadingSheetStatusModel.EntityToModel).ToList();
        }
    }
}
