﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheets.Extensions
{
    public static class LoadingSheetExtensions
    {
        public static IQueryable<LoadingSheet> ActiveLoadingSheets(this IQueryable<LoadingSheet> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<LoadingSheet> InactiveLoadingSheets(this IQueryable<LoadingSheet> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<LoadingSheet> LoadingSheetOverviewQuery(this IQueryable<LoadingSheet> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => SqlFunctions.StringConvert((decimal)bq.LoadingSheetNumber).Contains(query) ||
                                          (SqlFunctions.StringConvert((decimal)bq.LoadingSheetDate.Day).Trim() + "/" + SqlFunctions.StringConvert((decimal)bq.LoadingSheetDate.Month).Trim() + "/" + SqlFunctions.StringConvert((decimal)bq.LoadingSheetDate.Year).Trim()).Contains(query));
            }

            return model;
        }
    }
}
