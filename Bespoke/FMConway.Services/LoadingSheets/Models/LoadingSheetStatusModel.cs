﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheets.Models
{
    public class LoadingSheetStatusModel
    {
        public Guid StatusID { get; set; }
        public string Status { get; set; }
        public bool IsLoadingSheetStatus { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Status, LoadingSheetStatusModel>> EntityToModel = entity => new LoadingSheetStatusModel()
        {
            StatusID = entity.StatusPK,
            Status = entity.Status1,
            IsLoadingSheetStatus = entity.IsLoadingSheetStatus,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<LoadingSheetStatusModel, Status> ModelToEntity = model => new Status()
        {
            StatusPK = model.StatusID,
            Status1 = model.Status,
            IsLoadingSheetStatus = model.IsLoadingSheetStatus,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
