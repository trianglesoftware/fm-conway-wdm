﻿using FMConway.Services.Employees.Models;
using FMConway.Services.Vehicles.Models;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheets.Models
{
    public class LoadingSheetModel
    {
        public Guid LoadingSheetID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public int LoadingSheetNumber { get; set; }
        public string Name { get; set; }
        public DateTime LoadingSheetDate { get; set; }
        //public Guid? VehicleID { get; set; }
        //public VehicleModel Vehicle { get; set; }
        //public Guid VehicleTypeID { get; set; }
        //public VehicleTypeModel VehicleType { get; set; }
        public string Comments { get; set; }
        public Guid EmployeeID { get; set; }
        public EmployeeModel Employee { get; set; }
        //public string Site { get; set; }
        [DisplayName("Status")]
        public Guid StatusID { get; set; }
        public string Status { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public bool HasVmsOrAssetWithoutChecklist { get; set; }

        public static Expression<Func<LoadingSheet, LoadingSheetModel>> EntityToModel = entity => new LoadingSheetModel
        {
            LoadingSheetID = entity.LoadingSheetPK,
            WorkInstructionID = entity.WorkInstructionFK,
            LoadingSheetNumber = entity.LoadingSheetNumber,
            Name = entity.Name,
            LoadingSheetDate = entity.LoadingSheetDate,
            //VehicleID = entity.VehicleFK,
            //Vehicle = new VehicleModel()
            //{
            //    Registration = entity.Vehicle != null ? entity.Vehicle.Registration : ""
            //},
            //VehicleTypeID = entity.VehicleTypeFK,
            //VehicleType = new VehicleTypeModel()
            //{
            //    Name = entity.VehicleType.Name
            //},
            Comments = entity.Comments,
            EmployeeID = entity.EmployeeFK,
            Employee = new EmployeeModel()
            {
                EmployeeName = entity.Employee.EmployeeName
            },
            //Site = entity.Site,
            StatusID = entity.StatusFK,
            Status = entity.Status.Status1,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<LoadingSheetModel, LoadingSheet> ModelToEntity = model => new LoadingSheet
        {
            LoadingSheetPK = model.LoadingSheetID,
            WorkInstructionFK = model.WorkInstructionID,
            LoadingSheetNumber = model.LoadingSheetNumber,
            Name = model.Name,
            LoadingSheetDate = model.LoadingSheetDate,
            //VehicleFK = model.VehicleID,
            //VehicleTypeFK = model.VehicleTypeID,
            Comments = model.Comments,
            EmployeeFK = model.EmployeeID,
            //Site = model.Site,
            StatusFK = model.StatusID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
