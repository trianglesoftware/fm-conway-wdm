﻿using FMConway.Services.EquipmentTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheets.Models
{
     public class LoadingSheetEquipmentModel
    {
         public Guid LoadingSheetEquipmentID { get; set; }
         public Guid LoadingSheetID { get; set; }
         public Guid? EquipmentID { get; set; }
         public string Name { get; set; }
         public bool IsNonStandard { get; set; }
         public decimal Quantity { get; set; }
         public int SheetOrder { get; set; }
         public EquipmentTypeModel EquipmentType { get; set; }

         public static Expression<Func<LoadingSheetEquipment, LoadingSheetEquipmentModel>> EntityToModel = entity => new LoadingSheetEquipmentModel()
         {
             LoadingSheetEquipmentID = entity.LoadingSheetEquipmentPK,
             LoadingSheetID = entity.LoadingSheetFK,
             Name = entity.EquipmentFK.HasValue ? entity.Equipment.EquipmentName : entity.NonStandardEquipment,
             IsNonStandard = entity.IsNonStandard,
             Quantity = entity.Quantity,
             SheetOrder = entity.SheetOrder,
             EquipmentID = entity.EquipmentFK,
             EquipmentType = new EquipmentTypeModel()
             {
                 Name = entity.Equipment.EquipmentType.Name
             }
         };
    }
}
