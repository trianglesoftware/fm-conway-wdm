﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheets.Models
{
    public class LoadingSheetReportModel
    {
        public string IssuedBy { get; set; }
        public string Vehicle { get; set; }
        public string Site { get; set; }
        public string ContractRef { get; set; }
        public List<LoadingSheetEquipmentModel> Equipment { get; set; }
    }
}
