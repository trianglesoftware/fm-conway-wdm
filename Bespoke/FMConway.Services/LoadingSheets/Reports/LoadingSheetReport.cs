﻿using FMConway.Services.LoadingSheets.Models;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheets.Reports
{
    public class LoadingSheetReport
    {
        public byte[] CreateReport(LoadingSheetReportModel loadingSheet)
        {
            Document doc = new Document();

            doc.Info.Title = "Loading Sheet : " + loadingSheet.Vehicle;

            Section section = doc.AddSection();

            #region Header

            TextFrame logosFrame;

            logosFrame = section.AddTextFrame();
            logosFrame.Height = "3.0cm";
            logosFrame.Width = "4.0cm";
            logosFrame.Left = ShapePosition.Left;
            logosFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            logosFrame.Top = "1cm";
            logosFrame.MarginLeft = "1cm";
            logosFrame.RelativeVertical = RelativeVertical.Page;

            Paragraph paragraph = logosFrame.AddParagraph();

            var logoPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/images/fmconway_logo.png");
            Image img = paragraph.AddImage(logoPath);

            img.Width = Unit.FromPoint(100);
            img.Height = Unit.FromPoint(70);

            TextFrame addressFrame;

            addressFrame = section.AddTextFrame();
            addressFrame.Height = "3.0cm";
            addressFrame.Width = "6.0cm";
            addressFrame.Left = ShapePosition.Right;
            addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            addressFrame.Top = "1.25cm";
            addressFrame.RelativeVertical = RelativeVertical.Page;

            paragraph = addressFrame.AddParagraph();
            paragraph.AddText("FM Conway");
            paragraph.AddLineBreak();
            paragraph.AddText("Conway House, Vestry Road, Sevenoaks, Kent");
            paragraph.AddLineBreak();
            paragraph.AddText("TN14 5EL");
            paragraph.AddLineBreak();
            paragraph.AddText("TEL: 01732 600 700");
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;
            paragraph.Format.Font.Bold = true;
            paragraph.Format.SpaceAfter = 1;

            TextFrame titleFrame;

            titleFrame = section.AddTextFrame();
            titleFrame.Height = "3.0cm";
            titleFrame.Width = "6.0cm";
            titleFrame.Left = ShapePosition.Right;
            titleFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            titleFrame.Top = "2.5cm";
            titleFrame.MarginRight = "1.1cm";
            titleFrame.RelativeVertical = RelativeVertical.Page;

            paragraph = titleFrame.AddParagraph();
            paragraph.AddText("MATERIAL LOADING SHEET");
            paragraph.AddLineBreak();
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;
            paragraph.Format.Font.Bold = true;
            paragraph.Format.Font.Underline = Underline.Single;
            paragraph.Format.SpaceAfter = 1;

            paragraph = titleFrame.AddParagraph();
            paragraph.AddText("Document Ref 05 (Issue 5)");
            paragraph.AddLineBreak();
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;
            paragraph.Format.SpaceAfter = 1;

            TextFrame serperatorFrame;
            serperatorFrame = section.AddTextFrame();
            serperatorFrame.Height = "1.0cm";
            serperatorFrame.Width = "15.0cm";
            serperatorFrame.Left = ShapePosition.Center;
            serperatorFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            serperatorFrame.Top = "3.5cm";
            serperatorFrame.RelativeVertical = RelativeVertical.Page;

            paragraph = serperatorFrame.AddParagraph();
            paragraph.Format.Borders.Bottom = new Border { Style = BorderStyle.Single, Color = new Color(0,0,0) };

            #endregion

            #region PreTable Info

            TextFrame loadingSheetDetailsFrame1;

            loadingSheetDetailsFrame1 = section.AddTextFrame();
            loadingSheetDetailsFrame1.Height = "3.0cm";
            loadingSheetDetailsFrame1.Width = "6.0cm";
            loadingSheetDetailsFrame1.Left = ShapePosition.Left;
            loadingSheetDetailsFrame1.RelativeHorizontal = RelativeHorizontal.Margin;
            loadingSheetDetailsFrame1.Top = "4.2cm";
            loadingSheetDetailsFrame1.MarginLeft = "1cm";
            loadingSheetDetailsFrame1.RelativeVertical = RelativeVertical.Page;

            paragraph = loadingSheetDetailsFrame1.AddParagraph();
            paragraph.AddText("ISSUED BY: " + loadingSheet.IssuedBy);
            paragraph.AddLineBreak();
            paragraph.AddText("VEHICLE: " + loadingSheet.Vehicle);
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;

            TextFrame loadingSheetDetailsFrame2;

            loadingSheetDetailsFrame2 = section.AddTextFrame();
            loadingSheetDetailsFrame2.Height = "3.0cm";
            loadingSheetDetailsFrame2.Width = "4.0cm";
            loadingSheetDetailsFrame2.Left = ShapePosition.Center;
            loadingSheetDetailsFrame2.RelativeHorizontal = RelativeHorizontal.Margin;
            loadingSheetDetailsFrame2.Top = "4cm";
            loadingSheetDetailsFrame2.RelativeVertical = RelativeVertical.Page;

            paragraph = loadingSheetDetailsFrame2.AddParagraph();
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddText("SITE: " + loadingSheet.Site);
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;

            TextFrame loadingSheetDetailsFrame3;

            loadingSheetDetailsFrame3 = section.AddTextFrame();
            loadingSheetDetailsFrame3.Height = "3.0cm";
            loadingSheetDetailsFrame3.Width = "6.0cm";
            loadingSheetDetailsFrame3.Left = ShapePosition.Right;
            loadingSheetDetailsFrame3.RelativeHorizontal = RelativeHorizontal.Margin;
            loadingSheetDetailsFrame3.Top = "4cm";
            loadingSheetDetailsFrame3.RelativeVertical = RelativeVertical.Page;

            paragraph = loadingSheetDetailsFrame3.AddParagraph();
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddText("CONTRACT REF: " + loadingSheet.ContractRef);
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;

            #endregion

            #region Equipment

            TextFrame equipmentFrame;
            Table equipmentTable;

            equipmentFrame = section.AddTextFrame();
            equipmentFrame.Height = "0.8cm";
            equipmentFrame.Width = "17cm";
            equipmentFrame.Left = ShapePosition.Left;
            equipmentFrame.Top = "2.5cm";
            equipmentFrame.MarginLeft = "2cm";
            equipmentFrame.RelativeHorizontal = RelativeHorizontal.Page;
            equipmentFrame.RelativeVertical = RelativeVertical.Margin;

            equipmentTable = equipmentFrame.AddTable();
            equipmentTable.Style = "Table";
            equipmentTable.Borders.Color = Colors.Black;
            equipmentTable.Borders.Width = 0.25;
            equipmentTable.Borders.Left.Width = 0.5;
            equipmentTable.Borders.Right.Width = 0.5;
            equipmentTable.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = equipmentTable.AddColumn("7.0cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            column.LeftPadding = 0;
            column.RightPadding = 0;

            column = equipmentTable.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column.LeftPadding = 0;
            column.RightPadding = 0;

            column = equipmentTable.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column.LeftPadding = 0;
            column.RightPadding = 0;

            column = equipmentTable.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column.LeftPadding = 0;
            column.RightPadding = 0;

            column = equipmentTable.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column.LeftPadding = 0;
            column.RightPadding = 0;

            column = equipmentTable.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column.LeftPadding = 0;
            column.RightPadding = 0;

            Row row = equipmentTable.AddRow();
            row.HeadingFormat = true;
            row.Height = 15;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            row.Cells[0].AddParagraph("DESCRIPTION");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].Format.Font.Name = "Times New Roman";
            row.Cells[0].Format.Font.Size = 6;

            row.Cells[1].AddParagraph("SIZE");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].Format.Font.Name = "Times New Roman";
            row.Cells[1].Format.Font.Size = 6;

            row.Cells[2].AddParagraph("Q/FIT");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].Format.Font.Name = "Times New Roman";
            row.Cells[2].Format.Font.Size = 6;

            row.Cells[3].AddParagraph("RIG");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].Format.Font.Name = "Times New Roman";
            row.Cells[3].Format.Font.Size = 6;

            row.Cells[4].AddParagraph("NUMBER OF");
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].Format.Font.Name = "Times New Roman";
            row.Cells[4].Format.Font.Size = 6;

            row.Cells[5].AddParagraph("LOCATION");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].Format.Font.Name = "Times New Roman";
            row.Cells[5].Format.Font.Size = 6;

            row = equipmentTable.AddRow();
            row.HeadingFormat = true;
            row.Height = 10;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            row.Cells[0].AddParagraph("WORK FORCE IN ROAD SLOW");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].Format.Font.Name = "Times New Roman";
            row.Cells[0].Format.Font.Size = 6;

            foreach (var item in loadingSheet.Equipment)
            {
                row = equipmentTable.AddRow();
                row.HeadingFormat = true;
                row.Height = 10;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = false;
                row.Cells[0].AddParagraph(item.Name.ToUpper() + " - " + (item.IsNonStandard ? "NON STANDARD" : item.EquipmentType.Name.ToUpper()));
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].Format.Font.Name = "Times New Roman";
                row.Cells[0].Format.Font.Size = 6;

                row.Cells[4].AddParagraph(item.Quantity.ToString());
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].Format.Font.Name = "Times New Roman";
                row.Cells[4].Format.Font.Size = 6;
            }

            row = equipmentTable.AddRow();
            row.HeadingFormat = true;
            row.Height = 10;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = false;
            row.Cells[0].AddParagraph("TOTAL   -   FRAMES:");
            row.Cells[0].AddParagraph("                    SANDBARS:");
            row.Cells[0].AddParagraph("                    SANDBAGS:");
            row.Cells[0].AddParagraph("                    CONES:");
            row.Cells[0].AddParagraph("                    LAMPS:");
            row.Cells[0].AddParagraph("                    MISC:");
            row.Cells[0].Format.LeftIndent = 5;
            row.Cells[0].Format.SpaceBefore = 2;
            row.Cells[0].Format.SpaceAfter = 2;
            row.Cells[0].Borders.Right.Visible = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].Format.Font.Name = "Times New Roman";
            row.Cells[0].Format.Font.Size = 6;

            row.Cells[1].Borders.Left.Visible = false;
            row.Cells[1].Borders.Right.Visible = false;
            row.Cells[2].Borders.Left.Visible = false;
            row.Cells[2].Borders.Right.Visible = false;
            row.Cells[3].Borders.Left.Visible = false;
            row.Cells[3].Borders.Right.Visible = false;
            row.Cells[4].Borders.Left.Visible = false;
            row.Cells[4].Borders.Right.Visible = false;
            row.Cells[5].Borders.Left.Visible = false;

            #endregion

            #region Post Table Info

            loadingSheetDetailsFrame1 = section.Footers.Primary.AddTextFrame();
            loadingSheetDetailsFrame1.Height = "3.0cm";
            loadingSheetDetailsFrame1.Width = "15.0cm";
            loadingSheetDetailsFrame1.Left = ShapePosition.Left;
            loadingSheetDetailsFrame1.RelativeHorizontal = RelativeHorizontal.Margin;
            //loadingSheetDetailsFrame1.Top = "4.2cm";
            loadingSheetDetailsFrame1.MarginLeft = "1cm";
            loadingSheetDetailsFrame1.RelativeVertical = RelativeVertical.Page;

            paragraph = loadingSheetDetailsFrame1.AddParagraph();
            paragraph.AddText("LOADED BY:");
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddLineBreak();
            paragraph.AddText("SIGNED:");
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;

            paragraph = loadingSheetDetailsFrame1.AddParagraph();
            paragraph.Format.Borders.Bottom = new Border { Style = BorderStyle.Single, Color = new Color(0, 0, 0) };

            paragraph = loadingSheetDetailsFrame1.AddParagraph();
            paragraph.AddText("Regional Offices:    Borough Green (Sevenoaks)    -    Winchester");
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 8;
            paragraph.Format.Font.Bold = true;

            loadingSheetDetailsFrame2 = section.Footers.Primary.AddTextFrame();
            loadingSheetDetailsFrame2.Height = "3.0cm";
            loadingSheetDetailsFrame2.Width = "4.0cm";
            loadingSheetDetailsFrame2.Left = "2.5cm";
            loadingSheetDetailsFrame2.RelativeHorizontal = RelativeHorizontal.Margin;
            //loadingSheetDetailsFrame2.Top = "4cm";
            loadingSheetDetailsFrame2.RelativeVertical = RelativeVertical.Page;

            paragraph = loadingSheetDetailsFrame2.AddParagraph();
            paragraph.AddText("PRINT:");
            paragraph.AddLineBreak();
            paragraph.AddText("DATE:");
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;

            loadingSheetDetailsFrame3 = section.Footers.Primary.AddTextFrame();
            loadingSheetDetailsFrame3.Height = "3.0cm";
            loadingSheetDetailsFrame3.Width = "4.0cm";
            loadingSheetDetailsFrame3.Left = "8.0cm";
            loadingSheetDetailsFrame3.RelativeHorizontal = RelativeHorizontal.Margin;
            //loadingSheetDetailsFrame3.Top = "4.2cm";
            loadingSheetDetailsFrame3.MarginLeft = "1cm";
            loadingSheetDetailsFrame3.RelativeVertical = RelativeVertical.Page;

            paragraph = loadingSheetDetailsFrame3.AddParagraph();
            paragraph.AddText("CHECKED BY:");
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;

            loadingSheetDetailsFrame2 = section.Footers.Primary.AddTextFrame();
            loadingSheetDetailsFrame2.Height = "3.0cm";
            loadingSheetDetailsFrame2.Width = "4.0cm";
            loadingSheetDetailsFrame2.Left = "10.7cm";
            loadingSheetDetailsFrame2.RelativeHorizontal = RelativeHorizontal.Margin;
            //loadingSheetDetailsFrame2.Top = "4cm";
            loadingSheetDetailsFrame2.RelativeVertical = RelativeVertical.Page;

            paragraph = loadingSheetDetailsFrame2.AddParagraph();
            paragraph.AddText("PRINT:");
            paragraph.AddLineBreak();
            paragraph.AddText("DATE:");
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 6;

            //loadingSheetDetailsFrame2 = section.Footers.Primary.AddTextFrame();
            //loadingSheetDetailsFrame2.Height = "2.0cm";
            //loadingSheetDetailsFrame2.Width = "10.0cm";
            //loadingSheetDetailsFrame2.Left = ShapePosition.Center;
            //loadingSheetDetailsFrame2.RelativeHorizontal = RelativeHorizontal.Margin;
            ////loadingSheetDetailsFrame2.Top = "6cm";
            //loadingSheetDetailsFrame2.RelativeVertical = RelativeVertical.Page;

            //paragraph = loadingSheetDetailsFrame2.AddParagraph();
            //paragraph.AddText("Regional Office:    Borough Green (Sevenoaks)    -    Winchester");
            //paragraph.Format.Font.Name = "Times New Roman";
            //paragraph.Format.Font.Size = 6;

            #endregion

            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false, PdfFontEmbedding.Always);
            pdfRenderer.Document = doc;

            pdfRenderer.RenderDocument();

            string filename = "C:\\temp\\" + loadingSheet.Vehicle + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + ".pdf";
            pdfRenderer.PdfDocument.Save(filename);
            //// ...and start a viewer.
            //Process.Start(filename);

            FileStream fstream = null;
            fstream = System.IO.File.OpenRead(filename);
            byte[] bytes = new byte[fstream.Length];
            fstream.Read(bytes, 0, Convert.ToInt32(fstream.Length));
            return bytes;
        }
    }
}
