﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Employees.Models;
using FMConway.Services.LoadingSheets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheets.Interfaces
{
    public interface ILoadingSheetStatusService : IService
    {
        List<LoadingSheetStatusModel> GetLoadingSheetStatus();
    }
}
