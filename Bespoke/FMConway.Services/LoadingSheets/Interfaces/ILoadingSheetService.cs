﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.LoadingSheets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheets.Interfaces
{
    public interface ILoadingSheetService : IService
    {
        ILoadingSheetStatusService Status { get; set; }

        Guid Create(LoadingSheetModel model);
        Guid CreateFromTemplate(Guid templateID, Guid workInstructionID);
        void Update(LoadingSheetModel model);
        void UpdateOperations(LoadingSheetModel model);
        byte[] Print(Guid ID);
        LoadingSheetModel Single(Guid ID);

        void AddEquipment(List<string> selectedEquipment, decimal quantity, Guid loadingSheetID, int sheetOrder);
        void AddTemplateEquipment(List<string> selectedEquipment, decimal quantity, Guid loadingSheetTemplateID, int sheetOrder);
        void AddNonStandardEquipment(Guid loadingSheetID, string equipmentName, decimal quantity, int sheetOrder);
        void AddNonStandardTemplateEquipment(Guid loadingSheetTemplateID, string equipmentName, decimal quantity, int sheetOrder);
        Guid RemoveEquipment(Guid loadingSheetEquipmentID);
        Guid RemoveTemplateEquipment(Guid loadingSheetTemplateEquipmentID);
        void CreateDuplicateLoadingSheets(Guid parentWorkInstructionID, Guid workInstructionID);

        PagedResults<LoadingSheetModel> GetActiveLoadingSheetsPaged(int rowCount, int page, string query, Guid workInstructionID);

        //Operation
        PagedResults<LoadingSheetModel> GetActiveLoadingSheetsPaged(int rowCount, int page, string query);
        PagedResults<LoadingSheetModel> GetInactiveLoadingSheetsPaged(int rowCount, int page, string query);
    }
}
