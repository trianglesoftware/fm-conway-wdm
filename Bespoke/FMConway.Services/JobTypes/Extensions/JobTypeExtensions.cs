﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.JobTypes.Extensions
{
    public static class JobTypeExtensions
    {
        public static IQueryable<JobType> ActiveJobTypes(this IQueryable<JobType> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<JobType> InactiveJobTypes(this IQueryable<JobType> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<JobType> JobTypeOverviewQuery(this IQueryable<JobType> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.DisplayName.Contains(query));
            }

            return model;
        }
    }
}
