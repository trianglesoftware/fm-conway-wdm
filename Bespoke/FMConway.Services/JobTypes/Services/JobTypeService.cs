﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.JobTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.JobTypes.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.JobTypes.Services
{
    public class JobTypeService : Service, FMConway.Services.JobTypes.Interfaces.IJobTypeService
    {
        UserContext _userContext;

        public JobTypeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<JobTypeModel> GetActiveJobTypes()
        {
            return Context.JobTypes.Where(a => a.IsActive).Select(JobTypeModel.EntityToModel).ToList();
        }

        public List<JobTypeModel> GetInactiveJobTypes()
        {
            return Context.JobTypes.Where(a => !a.IsActive).Select(JobTypeModel.EntityToModel).ToList();
        }

        public void Create(JobTypeModel model, Dictionary<Guid, string> checklists)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.JobTypeID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.JobTypes.Add(JobTypeModel.ModelToEntity(model));

            if (checklists != null && checklists.Count > 0)
            {
                foreach (var checklist in checklists)
                {
                    JobTypeChecklist newJobTypeChecklist = new JobTypeChecklist();
                    newJobTypeChecklist.JobTypeChecklistPK = Guid.NewGuid();
                    newJobTypeChecklist.JobTypeFK = model.JobTypeID;
                    newJobTypeChecklist.ChecklistFK = checklist.Key;
                    newJobTypeChecklist.IsActive = true;
                    newJobTypeChecklist.CreatedByFK = _userContext.UserDetails.ID;
                    newJobTypeChecklist.CreatedDate = DateTime.Now;
                    newJobTypeChecklist.UpdatedByFK = _userContext.UserDetails.ID;
                    newJobTypeChecklist.UpdatedDate = DateTime.Now;

                    Context.JobTypeChecklists.Add(newJobTypeChecklist);
                }
            }

            SaveChanges();
        }

        public void Update(JobTypeModel model, Dictionary<Guid, string> checklists, Dictionary<Guid, string> checklistModel)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            JobType current = Context.JobTypes.Where(x => x.JobTypePK.Equals(model.JobTypeID)).SingleOrDefault();

            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.JobType1 = model.Name == null ? "" : model.Name;

            if (checklistModel != null)
            {
                if (checklists != null)
                {
                    if (!checklists.SequenceEqual(checklistModel))
                    {
                        var jobTypeChecklists = Context.JobTypeChecklists.Where(x => x.JobTypeFK.Equals(model.JobTypeID));
                        foreach (var checklist in jobTypeChecklists)
                        {
                            Context.JobTypeChecklists.Remove(checklist);
                        }

                        foreach (var checklist in checklists)
                        {
                            JobTypeChecklist newJobTypeChecklist = new JobTypeChecklist();
                            newJobTypeChecklist.JobTypeChecklistPK = Guid.NewGuid();
                            newJobTypeChecklist.JobTypeFK = model.JobTypeID;
                            newJobTypeChecklist.ChecklistFK = checklist.Key;
                            newJobTypeChecklist.IsActive = true;
                            newJobTypeChecklist.CreatedByFK = _userContext.UserDetails.ID;
                            newJobTypeChecklist.CreatedDate = DateTime.Now;
                            newJobTypeChecklist.UpdatedByFK = _userContext.UserDetails.ID;
                            newJobTypeChecklist.UpdatedDate = DateTime.Now;

                            Context.JobTypeChecklists.Add(newJobTypeChecklist);
                        }
                    }
                }
                else
                {
                    var jobTypeChecklists = Context.JobTypeChecklists.Where(x => x.JobTypeFK.Equals(model.JobTypeID));
                    foreach (var checklist in jobTypeChecklists)
                    {
                        Context.JobTypeChecklists.Remove(checklist);
                    }
                }
            }

            SaveChanges();
        }

        public JobTypeModel Single(Guid id)
        {
            return Context.JobTypes.Where(a => a.JobTypePK.Equals(id)).Select(JobTypeModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<JobTypeLookup> GetPagedActiveJobTypesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.JobTypes.ActiveJobTypes().OrderBy(o => o.JobType1).JobTypeOverviewQuery(query).Select(JobTypeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<JobTypeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public List<JobJobTypeModel> Get(Guid jobID)
        {
            return Context.JobJobTypes.Where(a => a.JobFK == jobID).Select(JobJobTypeModel.EntityToModel).ToList();
        }

        //public List<WorkInstructionJobType> GetForWI(Guid workInstructionID)
        //{
        //    return Context.WorkInstructionJobTypes.Where(x => x.WorkInstructionFK.Equals(workInstructionID)).ToList();
        //}
    }
}
