﻿using FMConway.Services.Checklists.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.JobTypes.Models
{
    public class JobTypeChecklistModel
    {
        public Guid JobTypeChecklistID { get; set; }
        public Guid JobTypeID { get; set; }
        public JobTypeModel JobType { get; set; }
        public Guid ChecklistID { get; set; }
        public ChecklistModel Checklist { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<JobTypeChecklist, JobTypeChecklistModel>> EntityToModel = entity => new JobTypeChecklistModel
        {
            JobTypeChecklistID = entity.JobTypeChecklistPK,
            JobTypeID = entity.JobTypeFK,
            JobType = new JobTypeModel()
            {
                Name = entity.JobType.DisplayName
            },
            ChecklistID = entity.ChecklistFK,
            Checklist = new ChecklistModel()
            {
                ChecklistName = entity.Checklist.ChecklistName
            },

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<JobTypeChecklistModel, JobTypeChecklist> ModelToEntity = model => new JobTypeChecklist()
        {
            JobTypeChecklistPK = model.JobTypeChecklistID,
            JobTypeFK = model.JobTypeID,
            ChecklistFK = model.ChecklistID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
