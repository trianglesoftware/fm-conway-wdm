﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.JobTypes.Models
{
    public class JobTypeLookup : LookupBase<Guid>
    {
        public static Expression<Func<JobType, JobTypeLookup>> EntityToModel = entity => new JobTypeLookup()
        {
            //Key = entity.JobType1,
            Key = entity.DisplayName,
            Value = entity.JobTypePK,
        };
    }
}
