﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.JobTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.JobTypes.Interfaces
{
    public interface IJobTypeService : IService
    {
        void Create(JobTypeModel model, Dictionary<Guid,string> checklists);
        List<JobTypeModel> GetActiveJobTypes();
        List<JobTypeModel> GetInactiveJobTypes();
        JobTypeModel Single(Guid id);
        void Update(JobTypeModel model, Dictionary<Guid, string> checklists, Dictionary<Guid, string> checklistModel);
        List<JobJobTypeModel> Get(Guid jobID);
        //List<WorkInstructionJobType> GetForWI(Guid workInstructionID);

        PagedResults<JobTypeLookup> GetPagedActiveJobTypesLookup(int rowCount, int page, string query);
    }
}
