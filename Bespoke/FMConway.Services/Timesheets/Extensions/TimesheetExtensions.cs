﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Timesheets.Extensions
{
    public static class TimesheetExtensions
    {
        public static IQueryable<TimesheetRecord> TimesheetOverviewQuery(this IQueryable<TimesheetRecord> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq =>
                    SqlFunctions.StringConvert((decimal)bq.WorkedHours).Contains(query) ||
                    SqlFunctions.StringConvert((decimal)bq.BreakHours).Contains(query)  ||
                    SqlFunctions.StringConvert((decimal)bq.TravelHours).Contains(query) ||
                    (SqlFunctions.StringConvert((decimal)bq.StartDate.Day).Trim() + "/" + SqlFunctions.StringConvert((decimal)bq.StartDate.Month).Trim() + "/" + SqlFunctions.StringConvert((decimal)bq.StartDate.Year).Trim() + " " + SqlFunctions.StringConvert((decimal)bq.StartDate.Hour).Trim() + ":" + SqlFunctions.StringConvert((decimal)bq.StartDate.Minute).Trim() + ":" + SqlFunctions.StringConvert((decimal)bq.StartDate.Second).Trim()).Contains(query) ||
                    (SqlFunctions.StringConvert((decimal)bq.EndDate.Day).Trim() + "/" + SqlFunctions.StringConvert((decimal)bq.EndDate.Month).Trim() + "/" + SqlFunctions.StringConvert((decimal)bq.EndDate.Year).Trim() + " " + SqlFunctions.StringConvert((decimal)bq.EndDate.Hour).Trim() + ":" + SqlFunctions.StringConvert((decimal)bq.EndDate.Minute).Trim() + ":" + SqlFunctions.StringConvert((decimal)bq.EndDate.Second).Trim()).Contains(query));
            }

            return model;
        }
    }
}
