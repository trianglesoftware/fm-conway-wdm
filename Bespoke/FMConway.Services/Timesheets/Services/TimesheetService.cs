﻿using FMConway.Services.Timesheets.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Timesheets.Extensions;
using FMConway.Services.ErrorLogging.Interfaces;
using Novacode;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using FMConway.Services.WorkInstructions.Extensions;
using System.Data.Entity;

namespace FMConway.Services.Timesheets.Services
{
    public class TimesheetService : Service, FMConway.Services.Timesheets.Interfaces.ITimesheetService
    {
        UserContext _userContext;
        IErrorLoggingService _errorLoggingService;

        public TimesheetService(UserContext userContext, IErrorLoggingService errorLoggingService)
        {
            _userContext = userContext;
            _errorLoggingService = errorLoggingService;
        }

        public PagedResults<TimesheetModel> GetActiveTimesheetsPaged(int rowCount, int page, string query, Guid employeeID)
        {
            DateTime dateQuery;
            var isDateQuery = DateTime.TryParse(query, out dateQuery);
            var emptyQuery = String.IsNullOrWhiteSpace(query);

            var baseQuery = Context.Timesheets.Where(x => x.EmployeeFK.Equals(employeeID) && x.IsActive)
                                              .Where(a => emptyQuery || (isDateQuery && DbFunctions.TruncateTime(a.WeekStart) == dateQuery))                                
                .OrderBy(o => o.WeekStart).Select(TimesheetModel.EntityToModel);

            return PagedResults<TimesheetModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<TimesheetModel> GetAllActiveTimesheetsPaged(int rowCount, int page, string query)
        {
            DateTime dateQuery;
            var isDateQuery = DateTime.TryParse(query, out dateQuery);

            var baseQuery = Context.Timesheets
                .Where(a => a.IsActive)
                .Where(a =>
                    a.Employee.EmployeeName.Contains(query) ||
                    (isDateQuery ? DbFunctions.TruncateTime(a.WeekStart) == dateQuery : false)
                    )
                .OrderByDescending(a => a.WeekStart)
                .ThenBy(a => a.Employee.EmployeeName)
                .Select(TimesheetModel.EntityToModel);

            return PagedResults<TimesheetModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<TimesheetModel> GetActiveDailyTimesheetsPaged(int rowCount, int page, string query)
        {
            DateTime dateQuery;
            var isDateQuery = DateTime.TryParse(query, out dateQuery);

            var baseQuery = Context.TimesheetRecords
                .Where(a => a.IsActive)
                .Where(a => 
                    a.Timesheet.Employee.EmployeeName.Contains(query) ||
                    (isDateQuery ? DbFunctions.TruncateTime(a.StartDate) == dateQuery : false)
                    )
                .GroupBy(a => new { StartDate = (DateTime)DbFunctions.TruncateTime(a.StartDate), a.Timesheet.EmployeeFK })
                .Select(a => new TimesheetModel
                {
                    EmployeeID = a.Key.EmployeeFK,
                    EmployeeName = a.Select(b => b.Timesheet.Employee.EmployeeName).FirstOrDefault(),
                    WeekStart = a.Key.StartDate,
                    WorkedHours = a.Sum(b => b.WorkedHours),
                    WorkedMins = a.Sum(b => b.WorkedMins),
                    BreakHours = a.Sum(b => b.BreakHours),
                    BreakMins = a.Sum(b => b.BreakMins),
                    TravelHours = a.Sum(b => b.TravelHours),
                    TravelMins = a.Sum(b => b.TravelMins),
                    NumberOfShifts = a.Count(),
                })
                .OrderByDescending(a => a.WeekStart)
                .ThenBy(a => a.EmployeeName);

            return PagedResults<TimesheetModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<TimesheetRecordModel> GetActiveTimesheetRecordsPaged(int rowCount, int page, string query, Guid timesheetID)
        {
            var baseQuery = Context.TimesheetRecords.Where(x => x.TimesheetFK.Equals(timesheetID) && x.IsActive).OrderBy(o => o.StartDate).TimesheetOverviewQuery(query).Select(TimesheetRecordModel.EntityToModel);

            var results = baseQuery.ToList();

            foreach (var record in results)
            {
                record.WorkInstruction = GetWorkInstructionsForRecord(record.TimesheetRecordID);
            }

            return PagedResults<TimesheetRecordModel>.GetPagedResults(results, rowCount, page);
        }

        public TimesheetModel Single(Guid id)
        {
            return Context.Timesheets.Where(x => x.TimesheetPK.Equals(id)).Select(TimesheetModel.EntityToModel).SingleOrDefault();
        }

        public TimesheetRecordModel SingleRecord(Guid id)
        {
            var record = Context.TimesheetRecords.Where(r => r.TimesheetRecordPK.Equals(id)).Select(TimesheetRecordModel.EntityToModel).SingleOrDefault();

            record.WorkInstruction = GetWorkInstructionsForRecord(id);

            return record;
        }

        public byte[] GetTimesheetSignature(Guid timesheetID)
        {
            var file = Context.TimesheetSignatures.Where(x => x.TimesheetFK.Equals(timesheetID)).SingleOrDefault();

            if(file == null)
            {
                return new byte[0];
            }
            else
            {
                return file.File.Data;
            }

        }

        public string GetWorkInstructionsForRecord(Guid recordID)
        {
            var recordWorkInstructions = Context.TimesheetRecordWorkInstructions.Where(x => x.TimesheetRecordFK.Equals(recordID)).ToList();

            string workInstructionText = "";

            foreach (var rwi in recordWorkInstructions)
            {
                foreach (var wi in rwi.Shift.JobPacks.SelectMany(jp => jp.WorkInstructions))
                {
                    workInstructionText += wi.WorkInstructionNumber + ", ";

                }

                if (workInstructionText.Length > 0)
                {
                    workInstructionText = workInstructionText.Substring(0, workInstructionText.Length - 2);
                }
            }

            return workInstructionText;
        }

        public void PrintTimesheet(Guid timesheetID, out string name, out byte[] timesheetReport)
        {   
            var timesheet = Context.Timesheets.Where(x => x.TimesheetPK.Equals(timesheetID)).SingleOrDefault();
            
            MemoryStream ms = new MemoryStream();

            var timesheetPath = System.Web.HttpContext.Current.Server.MapPath("~/Documents/FMCONWAY_TIMESHEET.docx");

            #warning Change doc location
            using (DocX document = DocX.Load(timesheetPath))
            {
                #region Content
                document.ReplaceText("{EMPNAME}", timesheet.Employee.EmployeeName);
                document.ReplaceText("{WEEKSTART}", timesheet.WeekStart.ToShortDateString());

                List<int> daysWorked = new List<int>();

                foreach (var record in timesheet.TimesheetRecords)
                {
                    if (record.StartDate.Date == timesheet.WeekStart.AddDays(1).Date)
                    {
                        daysWorked.Add(1);
                        string jobs = "";
                        string mondayVehicles = "";

                        foreach (var wi in record.TimesheetRecordWorkInstructions)
	                    {
		                    var shift = Context.Shifts.Where(x => x.ShiftPK.Equals(wi.ShiftFK)).SingleOrDefault();

                            foreach (var item in shift.JobPacks)
                            {
                                foreach (var w in item.WorkInstructions)
                                {
                                    var addressInLine = w.AddressInLine();

                                    if (jobs == "")
                                        jobs += w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                    else
                                        jobs += "; " + System.Environment.NewLine + w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                }
                            }

                            foreach(var item in shift.ShiftVehicles)
                            {
                            if (mondayVehicles == "")
                                        mondayVehicles += item.Vehicle.Registration;
                                    else
                                mondayVehicles += ", " + item.Vehicle.Registration; 
                            }
                                                        
	                    }

                        var totalMinutes = (record.WorkedHours * 60) + record.WorkedMins;

                        string allowancesText = "";

                        if (record.IPVAllowance)
                        {
                            allowancesText += "IPV ";
                        }

                        if (record.OnCallAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "On Call ";
                        }

                        if (record.PaidBreakAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "Paid Break";
                            totalMinutes += (record.BreakHours * 60) + record.WorkedMins;
                        }

                        document.ReplaceText("{MONDAYJOB}", jobs);
                        document.ReplaceText("{MONDAYVEHICLE}", mondayVehicles);
                        document.ReplaceText("{MONSTA}", record.StartDate.ToShortTimeString());
                        document.ReplaceText("{MONFIN}", record.EndDate.ToShortTimeString());                        
                        document.ReplaceText("{MON1}", MinutesToTimestamp((record.BreakHours * 60) + record.BreakMins));
                        document.ReplaceText("{MON2}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));
                        document.ReplaceText("{MON3}", allowancesText);
                        document.ReplaceText("{MON4}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));

                    }
                    else if (record.StartDate.Date == timesheet.WeekStart.AddDays(2).Date)
                    {
                        daysWorked.Add(2);
                        string jobs = "";
                        string tuesdayVehicles = "";

                        foreach (var wi in record.TimesheetRecordWorkInstructions)
                        {
                            var shift = Context.Shifts.Where(x => x.ShiftPK.Equals(wi.ShiftFK)).SingleOrDefault();

                            foreach (var item in shift.JobPacks)
                            {
                                foreach (var w in item.WorkInstructions)
                                {
                                    var addressInLine = w.AddressInLine();

                                    if (jobs == "")
                                        jobs += w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                    else
                                        jobs += "; " + System.Environment.NewLine + w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                }
                            }

                            foreach (var item in shift.ShiftVehicles)
                            {
                                if (tuesdayVehicles == "")
                                    tuesdayVehicles += item.Vehicle.Registration;
                                else
                                    tuesdayVehicles += ", " + item.Vehicle.Registration;
                            }
                        }

                        var totalMinutes = (record.WorkedHours * 60) + record.WorkedMins;

                        string allowancesText = "";

                        if (record.IPVAllowance)
                        {
                            allowancesText += "IPV";
                        }

                        if (record.OnCallAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "On Call ";
                        }

                        if (record.PaidBreakAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "Paid Break";
                            totalMinutes += (record.BreakHours * 60) + record.WorkedMins;
                        }

                        document.ReplaceText("{TUESDAYJOB}", jobs);
                        document.ReplaceText("{TUESDAYVEHICLE}", tuesdayVehicles);
                        document.ReplaceText("{TUESTA}", record.StartDate.ToShortTimeString());
                        document.ReplaceText("{TUEFIN}", record.EndDate.ToShortTimeString());                        
                        document.ReplaceText("{TUE1}", MinutesToTimestamp((record.BreakHours * 60) + record.BreakMins));
                        document.ReplaceText("{TUE2}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));
                        document.ReplaceText("{TUE3}", allowancesText);
                        document.ReplaceText("{TUE4}", MinutesToTimestamp(totalMinutes));


                    }
                    else if (record.StartDate.Date == timesheet.WeekStart.AddDays(3).Date)
                    {
                        daysWorked.Add(3);
                        string jobs = "";
                        string wednesdayVehicles = "";

                        foreach (var wi in record.TimesheetRecordWorkInstructions)
                        {
                            var shift = Context.Shifts.Where(x => x.ShiftPK.Equals(wi.ShiftFK)).SingleOrDefault();

                            foreach (var item in shift.JobPacks)
                            {
                                foreach (var w in item.WorkInstructions)
                                {
                                    var addressInLine = w.AddressInLine();

                                    if (jobs == "")
                                        jobs += w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                    else
                                        jobs += "; " + System.Environment.NewLine + w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                }
                            }

                            foreach (var item in shift.ShiftVehicles)
                            {
                                if (wednesdayVehicles == "")
                                    wednesdayVehicles += item.Vehicle.Registration;
                                else
                                    wednesdayVehicles += ", " + item.Vehicle.Registration;
                            }
                        }

                        var totalMinutes = (record.WorkedHours * 60) + record.WorkedMins;

                        string allowancesText = "";

                        if (record.IPVAllowance)
                        {
                            allowancesText += "IPV";
                        }

                        if (record.OnCallAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "On Call ";
                        }

                        if (record.PaidBreakAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "Paid Break";
                            totalMinutes += (record.BreakHours * 60) + record.WorkedMins;
                        }

                        document.ReplaceText("{WEDNESDAYJOB}", jobs);
                        document.ReplaceText("{WEDNESDAYVEHICLE}", wednesdayVehicles);
                        document.ReplaceText("{WEDSTA}", record.StartDate.ToShortTimeString());
                        document.ReplaceText("{WEDFIN}", record.EndDate.ToShortTimeString());                        
                        document.ReplaceText("{WED1}", MinutesToTimestamp((record.BreakHours * 60) + record.BreakMins));
                        document.ReplaceText("{WED2}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));
                        document.ReplaceText("{WED3}", allowancesText);
                        document.ReplaceText("{WED4}", MinutesToTimestamp(totalMinutes));

                    }
                    else if (record.StartDate.Date == timesheet.WeekStart.AddDays(4).Date)
                    {
                        daysWorked.Add(4);
                        string jobs = "";
                        string thursdayVehicles = "";

                        foreach (var wi in record.TimesheetRecordWorkInstructions)
                        {
                            var shift = Context.Shifts.Where(x => x.ShiftPK.Equals(wi.ShiftFK)).SingleOrDefault();

                            foreach (var item in shift.JobPacks)
                            {
                                foreach (var w in item.WorkInstructions)
                                {
                                    var addressInLine = w.AddressInLine();

                                    if (jobs == "")
                                        jobs += w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                    else
                                        jobs += "; " + System.Environment.NewLine + w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                }
                            }

                            foreach (var item in shift.ShiftVehicles)
                            {
                                if (thursdayVehicles == "")
                                    thursdayVehicles += item.Vehicle.Registration;
                                else
                                    thursdayVehicles += ", " + item.Vehicle.Registration;
                            }
                        }

                        var totalMinutes = (record.WorkedHours * 60) + record.WorkedMins;

                        string allowancesText = "";

                        if (record.IPVAllowance)
                        {
                            allowancesText += "IPV";
                        }

                        if (record.OnCallAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "On Call ";
                        }

                        if (record.PaidBreakAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "Paid Break";
                            totalMinutes += (record.BreakHours * 60) + record.WorkedMins;
                        }

                        document.ReplaceText("{THURSDAYJOB}", jobs);
                        document.ReplaceText("{THURSDAYVEHICLE}", thursdayVehicles);
                        document.ReplaceText("{THUSTA}", record.StartDate.ToShortTimeString());
                        document.ReplaceText("{THUFIN}", record.EndDate.ToShortTimeString());                        
                        document.ReplaceText("{THU1}", MinutesToTimestamp((record.BreakHours * 60) + record.BreakMins));
                        document.ReplaceText("{THU2}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));
                        document.ReplaceText("{THU3}", allowancesText);
                        document.ReplaceText("{THU4}", MinutesToTimestamp(totalMinutes));

                    }
                    else if (record.StartDate.Date == timesheet.WeekStart.AddDays(5).Date)
                    {
                        daysWorked.Add(5);
                        string jobs = "";
                        string fridayVehicles = "";

                        foreach (var wi in record.TimesheetRecordWorkInstructions)
                        {
                            var shift = Context.Shifts.Where(x => x.ShiftPK.Equals(wi.ShiftFK)).SingleOrDefault();

                            foreach (var item in shift.JobPacks)
                            {
                                foreach (var w in item.WorkInstructions)
                                {
                                    var addressInLine = w.AddressInLine();

                                    if (jobs == "")
                                        jobs += w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                    else
                                        jobs += "; " + System.Environment.NewLine + w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                }
                            }

                            foreach (var item in shift.ShiftVehicles)
                            {
                                if (fridayVehicles == "")
                                    fridayVehicles += item.Vehicle.Registration;
                                else
                                    fridayVehicles += ", " + item.Vehicle.Registration;
                            }
                        }

                        var totalMinutes = (record.WorkedHours * 60) + record.WorkedMins;

                        string allowancesText = "";

                        if (record.IPVAllowance)
                        {
                            allowancesText += "IPV";
                        }

                        if (record.OnCallAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "On Call ";
                        }

                        if (record.PaidBreakAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "Paid Break";
                            totalMinutes += (record.BreakHours * 60) + record.WorkedMins;
                        }

                        document.ReplaceText("{FRIDAYJOB}", jobs);
                        document.ReplaceText("{FRIDAYVEHICLE}", fridayVehicles);
                        document.ReplaceText("{FRISTA}", record.StartDate.ToShortTimeString());
                        document.ReplaceText("{FRIFIN}", record.EndDate.ToShortTimeString());                        
                        document.ReplaceText("{FRI1}", MinutesToTimestamp((record.BreakHours * 60) + record.BreakMins));
                        document.ReplaceText("{FRI2}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));
                        document.ReplaceText("{FRI3}", allowancesText);
                        document.ReplaceText("{FRI4}", MinutesToTimestamp(totalMinutes));

                    }
                    else if (record.StartDate.Date == timesheet.WeekStart.AddDays(6).Date)
                    {
                        daysWorked.Add(6);
                        string jobs = "";
                        string saturdayVehicles = "";

                        foreach (var wi in record.TimesheetRecordWorkInstructions)
                        {
                            var shift = Context.Shifts.Where(x => x.ShiftPK.Equals(wi.ShiftFK)).SingleOrDefault();

                            foreach (var item in shift.JobPacks)
                            {
                                foreach (var w in item.WorkInstructions)
                                {
                                    var addressInLine = w.AddressInLine();

                                    if (jobs == "")
                                        jobs += w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                    else
                                        jobs += "; " + System.Environment.NewLine + w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                }
                            }

                            foreach (var item in shift.ShiftVehicles)
                            {
                                if (saturdayVehicles == "")
                                    saturdayVehicles += item.Vehicle.Registration;
                                else
                                    saturdayVehicles += ", " + item.Vehicle.Registration;
                            }
                        }

                        var totalMinutes = (record.WorkedHours * 60) + record.WorkedMins;

                        string allowancesText = "";

                        if (record.IPVAllowance)
                        {
                            allowancesText += "IPV";
                        }

                        if (record.OnCallAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "On Call ";
                        }

                        if (record.PaidBreakAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "Paid Break";
                            totalMinutes += (record.BreakHours * 60) + record.WorkedMins;
                        }

                        document.ReplaceText("{SATURDAYJOB}", jobs);
                        document.ReplaceText("{SATURDAYVEHICLE}", saturdayVehicles);
                        document.ReplaceText("{SATSTA}", record.StartDate.ToShortTimeString());
                        document.ReplaceText("{SATFIN}", record.EndDate.ToShortTimeString());
                        document.ReplaceText("{SAT1}", MinutesToTimestamp((record.BreakHours * 60) + record.BreakMins));
                        document.ReplaceText("{SAT2}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));
                        document.ReplaceText("{SAT3}", allowancesText);
                        document.ReplaceText("{SAT4}", MinutesToTimestamp(totalMinutes));

                    }
                    else if (record.StartDate.Date == timesheet.WeekStart.Date)
                    {
                        daysWorked.Add(0);
                        string jobs = "";
                        string sundayVehicles = "";

                        foreach (var wi in record.TimesheetRecordWorkInstructions)
                        {
                            var shift = Context.Shifts.Where(x => x.ShiftPK.Equals(wi.ShiftFK)).SingleOrDefault();

                            foreach (var item in shift.JobPacks)
                            {
                                foreach (var w in item.WorkInstructions)
                                {
                                    var addressInLine = w.AddressInLine();

                                    if (jobs == "")
                                        jobs += w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                    else
                                        jobs += "; " + System.Environment.NewLine + w.WorkInstructionNumber + ", " + addressInLine + ", " + w.Job.Description;
                                }
                            }

                            foreach (var item in shift.ShiftVehicles)
                            {
                                if (sundayVehicles == "")
                                    sundayVehicles += item.Vehicle.Registration;
                                else
                                    sundayVehicles += ", " + item.Vehicle.Registration;
                            }
                        }

                        var totalMinutes = (record.WorkedHours * 60) + record.WorkedMins;

                        string allowancesText = "";

                        if (record.IPVAllowance)
                        {
                            allowancesText += "IPV";
                        }

                        if (record.OnCallAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "On Call ";
                        }

                        if (record.PaidBreakAllowance)
                        {
                            if (allowancesText != "")
                            {
                                allowancesText += System.Environment.NewLine;
                            }

                            allowancesText += "Paid Break";
                            totalMinutes += (record.BreakHours * 60) + record.WorkedMins;
                        }

                        document.ReplaceText("{SUNDAYJOB}", jobs);
                        document.ReplaceText("{SUNDAYVEHICLE}", sundayVehicles);
                        document.ReplaceText("{SUNSTA}", record.StartDate.ToShortTimeString());
                        document.ReplaceText("{SUNFIN}", record.EndDate.ToShortTimeString());
                        document.ReplaceText("{SUN1}", MinutesToTimestamp((record.BreakHours * 60) + record.BreakMins));
                        document.ReplaceText("{SUN2}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));
                        document.ReplaceText("{SUN3}", allowancesText);
                        document.ReplaceText("{SUN4}", MinutesToTimestamp((record.WorkedHours * 60) + record.WorkedMins));

                    }
                }

                #region BlankEmptySpaces

                document.ReplaceText("{MONDAYJOB}", "");
                document.ReplaceText("{MONDAYVEHICLE}", "");
                document.ReplaceText("{MONSTA}", "");
                document.ReplaceText("{MONFIN}", "");
                document.ReplaceText("{MON1}", "");
                document.ReplaceText("{MON2}", "");
                document.ReplaceText("{MON3}", "");
                document.ReplaceText("{MON4}", "");
                

                document.ReplaceText("{TUESDAYJOB}", "");
                document.ReplaceText("{TUESDAYVEHICLE}", "");
                document.ReplaceText("{TUESTA}", "");
                document.ReplaceText("{TUEFIN}", "");
                document.ReplaceText("{TUE1}", "");
                document.ReplaceText("{TUE2}", "");
                document.ReplaceText("{TUE3}", "");
                document.ReplaceText("{TUE4}", "");
                

                document.ReplaceText("{WEDNESDAYJOB}", "");
                document.ReplaceText("{WEDNESDAYVEHICLE}", "");
                document.ReplaceText("{WEDSTA}", "");
                document.ReplaceText("{WEDFIN}", "");
                document.ReplaceText("{WED1}", "");
                document.ReplaceText("{WED2}", "");
                document.ReplaceText("{WED3}", "");
                document.ReplaceText("{WED4}", "");
               

                document.ReplaceText("{THURSDAYJOB}", "");
                document.ReplaceText("{THURSDAYVEHICLE}", "");
                document.ReplaceText("{THUSTA}", "");
                document.ReplaceText("{THUFIN}", "");
                document.ReplaceText("{THU1}", "");
                document.ReplaceText("{THU2}", "");
                document.ReplaceText("{THU3}", "");
                document.ReplaceText("{THU4}", "");

                document.ReplaceText("{FRIDAYJOB}", "");
                document.ReplaceText("{FRIDAYVEHICLE}", "");
                document.ReplaceText("{FRISTA}", "");
                document.ReplaceText("{FRIFIN}", "");
                document.ReplaceText("{FRI1}", "");
                document.ReplaceText("{FRI2}", "");
                document.ReplaceText("{FRI3}", "");
                document.ReplaceText("{FRI4}", "");

                document.ReplaceText("{SATURDAYJOB}", "");
                document.ReplaceText("{SATURDAYVEHICLE}", "");
                document.ReplaceText("{SATSTA}", "");
                document.ReplaceText("{SATFIN}", "");
                document.ReplaceText("{SAT1}", "");
                document.ReplaceText("{SAT2}", "");
                document.ReplaceText("{SAT3}", "");
                document.ReplaceText("{SAT4}", "");

                document.ReplaceText("{SUNDAYJOB}", "");
                document.ReplaceText("{SUNDAYVEHICLE}", "");
                document.ReplaceText("{SUNSTA}", "");
                document.ReplaceText("{SUNFIN}", "");
                document.ReplaceText("{SUN1}", "");
                document.ReplaceText("{SUN2}", "");
                document.ReplaceText("{SUN3}", "");
                document.ReplaceText("{SUN4}", "");

                #endregion
                #endregion

                document.SaveAs(ms);                
            }

            timesheetReport = ms.ToArray();

            name = "Timesheet_" + timesheet.Employee.EmployeeName.Replace(" ", "") + "_" + timesheet.WeekStart.Year.ToString() + "_" + timesheet.WeekStart.Month.ToString() + "_" + timesheet.WeekStart.Day.ToString() + ".docx";
        }

        public Guid CreateTimesheet(Guid employeeID, DateTime startTime)
        {
            try
            {
                DateTime weekStart = GetWeekStart(startTime);

                Timesheet timesheet = new Timesheet()
                {
                    TimesheetPK = Guid.NewGuid(),
                    EmployeeFK = employeeID,
                    WeekStart = weekStart,
                    CreatedByFK = _userContext.UserDetails.ID,
                    CreatedDate = DateTime.Now,
                    UpdatedByFK = _userContext.UserDetails.ID,
                    UpdatedDate = DateTime.Now,
                    IsActive = true
                };

                Context.Timesheets.Add(timesheet);

                SaveChanges();

                return timesheet.TimesheetPK;
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("CreateTimesheet", e, employeeID.ToString());
                return Guid.Empty;
            }
        }

        public Guid CreateTimesheetRecord(TimesheetRecordModel model)
        {
            try
            {
                TimesheetRecord record = new TimesheetRecord() 
                {
                    TimesheetRecordPK = Guid.NewGuid(),
                    StartDate = model.StartTime,
                    EndDate = model.EndTime,
                    WorkedHours = model.WorkedHours,
                    WorkedMins = model.WorkedMins,
                    BreakHours = model.BreakHours,
                    BreakMins = model.BreakMins,
                    TravelHours = model.TravelHours,
                    TravelMins = model.TravelMins,
                    IPVAllowance = model.IPVAllowance,
                    OnCallAllowance = model.OnCallAllowance,
                    PaidBreakAllowance = model.PaidBreakAllowance,
                    TimesheetFK = model.TimesheetID,
                    IsActive = true,
                    CreatedByFK = _userContext.UserDetails.ID,
                    CreatedDate = DateTime.Now,
                    UpdatedByFK = _userContext.UserDetails.ID,
                    UpdatedDate = DateTime.Now
                };

                Context.TimesheetRecords.Add(record);

                SaveChanges();

                TimesheetRecordWorkInstruction trwi = new TimesheetRecordWorkInstruction() 
                {
                    TimesheetRecordWorkInstructionPK = Guid.NewGuid(),
                    TimesheetRecordFK = record.TimesheetRecordPK,
                    ShiftFK = model.ShiftID,
                    IsActive = true,
                    CreatedByFK = _userContext.UserDetails.ID,
                    CreatedDate = DateTime.Now,
                    UpdatedByFK = _userContext.UserDetails.ID,
                    UpdatedDate = DateTime.Now
                };

                Context.TimesheetRecordWorkInstructions.Add(trwi);

                SaveChanges();

                return record.TimesheetRecordPK;
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("CreateTimesheetRecord", e, model.TimesheetID.ToString());
                return Guid.Empty;
            }
        }

        public bool UpdateTimesheetRecord(TimesheetRecordModel record)
        {
            try
            {
                var timesheet = Context.TimesheetRecords.Where(t => t.TimesheetRecordPK.Equals(record.TimesheetRecordID)).SingleOrDefault();

                if (timesheet != null)
                {
                    timesheet.WorkedHours = record.WorkedHours;
                    timesheet.WorkedMins = record.WorkedMins;
                    timesheet.BreakHours = record.BreakHours;
                    timesheet.BreakMins = record.BreakMins;
                    timesheet.TravelHours = record.TravelHours;
                    timesheet.TravelMins = record.TravelMins;
                    timesheet.IPVAllowance = record.IPVAllowance;
                    timesheet.OnCallAllowance = record.OnCallAllowance;
                    timesheet.PaidBreakAllowance = record.PaidBreakAllowance;
                    timesheet.UpdatedByFK = _userContext.UserDetails.ID;
                    timesheet.UpdatedDate = DateTime.Now;

                    SaveChanges();
                }
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("UpdateTimesheetRecord", e, record.TimesheetRecordID.ToString());
                return false;
            }

            return true;
        }
        
        public DateTime GetWeekStart(DateTime date)
        {
            DateTime weekStart = date.Date;

            switch ((int)weekStart.DayOfWeek)
            {
                case 0:
                    break;
                case 1:
                    weekStart = weekStart.AddDays(-1);
                    break;
                case 2:
                    weekStart = weekStart.AddDays(-2);
                    break;
                case 3:
                    weekStart = weekStart.AddDays(-3);
                    break;
                case 4:
                    weekStart = weekStart.AddDays(-4);
                    break;
                case 5:
                    weekStart = weekStart.AddDays(-5);
                    break;
                case 6:
                    weekStart = weekStart.AddDays(-6);
                    break;
            }

            return weekStart;
        }

        public PagedResults<ShiftLookup> GetPagedActiveShiftsLookup(int rowCount, int page, string query, Guid employeeID)
        {
            var baseQuery = Context.Shifts.Where(s => s.ShiftStaffs.Any(ss => ss.EmployeeFK.Equals(employeeID) && ss.IsActive) && s.IsActive && 
                    !s.TimesheetRecordWorkInstructions.Any(t => t.TimesheetRecord.Timesheet.EmployeeFK.Equals(employeeID)))
                .OrderBy(s => s.ShiftDate)
                .Select(a => new
                {
                    a.ShiftPK,
                    a.ShiftDate
                })
                .ToList()
                .Select(a => new ShiftLookup
                {
                    Value = a.ShiftPK,
                    Key = a.ShiftDate.Day + "/" + a.ShiftDate.Month + "/" + a.ShiftDate.Year
                });

            PagedResults<ShiftLookup> results = new PagedResults<ShiftLookup>();

            try 
            {
                int recordCount = baseQuery.Count();
                
                results.Page = page;
                results.PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount);
                results.Data = baseQuery.ToList();
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetPagedActiveShiftsLookup", e, employeeID.ToString());
            }

            return results;
        }

        public PagedResults<ShiftLookup> GetPagedActiveShiftsForTimesheetLookup(int rowCount, int page, string query, Guid timesheetID)
        {
            var results = new PagedResults<ShiftLookup>();

            var timesheet = Context.Timesheets.Where(t => t.TimesheetPK == timesheetID).SingleOrDefault();
            if (timesheet != null)
            {
                var plusWeek = timesheet.WeekStart.AddDays(7);

                var baseQuery = Context.Shifts.Where(s =>
                    s.ShiftStaffs.Any(ss => ss.EmployeeFK == timesheet.EmployeeFK && ss.IsActive) &&
                    s.IsActive &&
                    !s.TimesheetRecordWorkInstructions.Any(t => t.TimesheetRecord.Timesheet.EmployeeFK == timesheet.EmployeeFK) &&
                    s.ShiftDate >= timesheet.WeekStart &&
                    s.ShiftDate <= plusWeek)
                    .OrderBy(s => s.ShiftDate)
                    .Select(a => new
                    {
                        a.ShiftPK,
                        a.ShiftDate
                    })
                    .ToList()
                    .Select(a => new ShiftLookup
                    {
                        Value = a.ShiftPK,
                        // had to do this client side as linq to entities doesnt appear to like int + string
                        Key = a.ShiftDate.Day + "/" + a.ShiftDate.Month + "/" + a.ShiftDate.Year
                    })
                    .ToList();
                
                int recordCount = baseQuery.Count();

                results.Page = page;
                results.PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount);
                results.Data = baseQuery;
            }
            else
            {
                results.Data = new List<ShiftLookup>();
            }

            return results;
        }

        private string MinutesToTimestamp(int minutes)
        {
            var timespan = TimeSpan.FromMinutes(minutes);
            return timespan.ToString(@"hh\:mm");
        }
    }
}
