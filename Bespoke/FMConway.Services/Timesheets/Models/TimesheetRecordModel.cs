﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Timesheets.Models
{
    public class TimesheetRecordModel
    {
        public Guid TimesheetRecordID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string WorkInstruction { get; set; }
        public Guid TimesheetID { get; set; }
        public Guid ShiftID { get; set; }

        public int WorkedHours { get; set; }
        public int WorkedMins { get; set; }
        public int BreakHours { get; set; }
        public int BreakMins { get; set; }
        public int TravelHours { get; set; }
        public int TravelMins { get; set; }

        public bool IPVAllowance { get; set; }
        public bool OnCallAllowance { get; set; }
        public bool PaidBreakAllowance { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<TimesheetRecord, TimesheetRecordModel>> EntityToModel = entity => new TimesheetRecordModel
        {
            TimesheetRecordID = entity.TimesheetRecordPK,
            TimesheetID = entity.TimesheetFK,
            StartTime = entity.StartDate,
            EndTime = entity.EndDate,

            WorkedHours = entity.WorkedHours,
            WorkedMins = entity.WorkedMins,
            BreakHours = entity.BreakHours,
            BreakMins = entity.BreakMins,
            TravelHours = entity.TravelHours,
            TravelMins = entity.TravelMins,

            IPVAllowance = entity.IPVAllowance,
            OnCallAllowance = entity.OnCallAllowance,
            PaidBreakAllowance = entity.PaidBreakAllowance,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };
    }
}
