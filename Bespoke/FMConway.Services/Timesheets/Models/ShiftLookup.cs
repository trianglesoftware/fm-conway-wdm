﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Timesheets.Models
{
    public class ShiftLookup : LookupBase<Guid>
    {
        public static Expression<Func<Shift, ShiftLookup>> ETM = e => new ShiftLookup()
        {
            Value = e.ShiftPK,
            Key = e.ShiftDate.Day + "/" + e.ShiftDate.Month + "/" + e.ShiftDate.Year
        };
    }
}
