﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Timesheets.Models
{
    public class TimesheetReportModel
    {
        public string EmployeeName { get; set; }
        public DateTime WeekEnding { get; set; }
        public List<TimesheetRecordReportModel> TimesheetDays { get; set; }

        public Guid SignatureID { get; set; }
    }
}
