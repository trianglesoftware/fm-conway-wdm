﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Timesheets.Models
{
    public class TimesheetRecordReportModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public decimal DrivingTime { get; set; }
        public decimal PassengerTime { get; set; }
        public decimal BreakTime { get; set; }
        public decimal WorkingTime { get; set; }
        public decimal TotalTime { get; set; }
        public string WorkInstruction { get; set; }
        public string VehicleReg { get; set; }
    }
}
