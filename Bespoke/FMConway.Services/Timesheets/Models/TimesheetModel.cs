﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Timesheets.Models
{
    public class TimesheetModel
    {
        public Guid TimesheetID { get; set; }
        public Guid EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public DateTime WeekStart { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public int WorkedHours { get; set; }
        public int WorkedMins { get; set; }
        public int BreakHours { get; set; }
        public int BreakMins { get; set; }
        public int TravelHours { get; set; }
        public int TravelMins { get; set; }

        public int NumberOfShifts { get; set; }

        public static Expression<Func<Timesheet, TimesheetModel>> EntityToModel = entity => new TimesheetModel
        {
            TimesheetID = entity.TimesheetPK,
            EmployeeID = entity.EmployeeFK,
            EmployeeName = entity.Employee.EmployeeName,
            WeekStart = entity.WeekStart,
            WorkedHours = entity.TimesheetRecords.Sum(s => s.WorkedHours),
            WorkedMins = entity.TimesheetRecords.Sum(s => s.WorkedMins),
            BreakHours = entity.TimesheetRecords.Sum(s => s.BreakHours),
            BreakMins = entity.TimesheetRecords.Sum(s => s.BreakMins),
            TravelHours = entity.TimesheetRecords.Sum(s => s.TravelHours),
            TravelMins = entity.TimesheetRecords.Sum(s => s.TravelMins),
            NumberOfShifts = entity.TimesheetRecords.Count,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

    }
}
