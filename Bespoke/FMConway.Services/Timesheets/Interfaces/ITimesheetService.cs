﻿using FMConway.Services.Timesheets.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Timesheets.Interfaces
{
    public interface ITimesheetService : IService
    {
        PagedResults<TimesheetModel> GetActiveTimesheetsPaged(int rowCount, int page, string query, Guid employeeID);
        PagedResults<TimesheetModel> GetAllActiveTimesheetsPaged(int rowCount, int page, string query);
        PagedResults<TimesheetRecordModel> GetActiveTimesheetRecordsPaged(int rowCount, int page, string query, Guid timesheetID);
        TimesheetModel Single(Guid id);
        TimesheetRecordModel SingleRecord(Guid id);
        byte[] GetTimesheetSignature(Guid timesheetID);
        void PrintTimesheet(Guid timesheetID, out string name, out byte[] timesheetReport);
        //Guid GetWorkInstructionID(Guid activityID);

        Guid CreateTimesheet(Guid employeeID, DateTime startTime);
        Guid CreateTimesheetRecord(TimesheetRecordModel record);

        bool UpdateTimesheetRecord(TimesheetRecordModel record);

        DateTime GetWeekStart(DateTime date);

        PagedResults<ShiftLookup> GetPagedActiveShiftsLookup(int rowCount, int page, string query, Guid employeeID);
        PagedResults<ShiftLookup> GetPagedActiveShiftsForTimesheetLookup(int rowCount, int page, string query, Guid timesheetID);
        PagedResults<TimesheetModel> GetActiveDailyTimesheetsPaged(int rowCount, int page, string query);
    }
}
