﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MethodStatements.Extensions
{
    public static class MethodStatementExtensions
    {
        public static IQueryable<MethodStatement> ActiveMethodStatements(this IQueryable<MethodStatement> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<MethodStatement> InactiveMethodStatements(this IQueryable<MethodStatement> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<MethodStatement> MethodStatementOverviewQuery(this IQueryable<MethodStatement> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.MethodStatementTitle.Contains(query) ||
                                          bq.Revision.Contains(query));
            }

            return model;
        }
    }
}
