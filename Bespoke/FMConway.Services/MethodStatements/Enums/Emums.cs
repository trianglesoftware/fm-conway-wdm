﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MethodStatements.Enums
{
    public static class Enums
    {
        public static Guid DefaultMethodStatement = new Guid("272c132c-b0b3-4c73-8912-e2c1bf5f4879");

        public static List<Guid> DefaultJobTypes()
        {
            var list = new List<Guid>();
            list.Add(new Guid("C6A76397-0E0E-477D-902B-77E2BB08BEE8"));
            list.Add(new Guid("DE143847-A68C-4E7B-904F-D9C0A628F0D9"));
            return list;
        }
    }
}
