﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.MethodStatements.Models
{
    public class MethodStatementModel
    {
        public Guid MethodStatementID { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
        //[Required(ErrorMessage = "Revision is required")]
        public string Revision { get; set; }
        public Guid? FileID { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<MethodStatement, MethodStatementModel>> EntityToModel = entity => new MethodStatementModel
        {
            MethodStatementID = entity.MethodStatementPK,
            Title = entity.MethodStatementTitle,
            Revision = entity.Revision,
            FileID = entity.FileFK,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<MethodStatementModel, MethodStatement> ModelToEntity = model => new MethodStatement
        {
            MethodStatementPK = model.MethodStatementID,
            MethodStatementTitle = model.Title,
            Revision = model.Revision,
            FileFK = model.FileID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
