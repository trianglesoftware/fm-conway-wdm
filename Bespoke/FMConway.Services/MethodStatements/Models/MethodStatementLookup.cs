﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MethodStatements.Models
{
    public class MethodStatementLookup : LookupBase<Guid>
    {
       // public string Revision { get; set; }

        public static Expression<Func<MethodStatement, MethodStatementLookup>> EntityToModel = entity => new MethodStatementLookup()
        {
            Key = entity.MethodStatementTitle,
            Value = entity.MethodStatementPK
            //Revision = entity.Revision
        };
    }
}
