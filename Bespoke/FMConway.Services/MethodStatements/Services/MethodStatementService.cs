﻿using FMConway.Services.MethodStatements.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.MethodStatements.Extensions;

namespace FMConway.Services.MethodStatements.Services
{
    public class MethodStatementService : Service, FMConway.Services.MethodStatements.Interfaces.IMethodStatementService
    {
        UserContext _userContext;

        public MethodStatementService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(MethodStatementModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.MethodStatementID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;
            model.Revision = "1";

            Context.MethodStatements.Add(MethodStatementModel.ModelToEntity(model));

            SaveChanges();

            return model.MethodStatementID;
        }

        public void Update(MethodStatementModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            MethodStatement current = Context.MethodStatements.Where(x => x.MethodStatementPK.Equals(model.MethodStatementID)).Single();

            current.MethodStatementTitle = model.Title == null ? "" : model.Title;
            current.Revision = model.Revision == null ? "" : model.Revision;
            current.FileFK = model.FileID;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public MethodStatementModel Single(Guid ID)
        {
            return Context.MethodStatements.Where(a => a.MethodStatementPK.Equals(ID)).Select(MethodStatementModel.EntityToModel).SingleOrDefault();
        }

        public List<MethodStatementModel> GetAllActiveMethodStatements()
        {
            return Context.MethodStatements.ActiveMethodStatements().Select(MethodStatementModel.EntityToModel).ToList();
        }

        public PagedResults<MethodStatementModel> GetActiveMethodStatementsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.MethodStatements.ActiveMethodStatements().OrderBy(o => o.MethodStatementTitle).MethodStatementOverviewQuery(query).Select(MethodStatementModel.EntityToModel);

            return PagedResults<MethodStatementModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<MethodStatementModel> GetAllInactiveMethodStatements()
        {
            return Context.MethodStatements.InactiveMethodStatements().Select(MethodStatementModel.EntityToModel).ToList();
        }

        public PagedResults<MethodStatementModel> GetInactiveMethodStatementsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.MethodStatements.InactiveMethodStatements().OrderBy(o => o.MethodStatementTitle).MethodStatementOverviewQuery(query).Select(MethodStatementModel.EntityToModel);

            return PagedResults<MethodStatementModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<MethodStatementLookup> GetPagedActiveMethodStatementsLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.MethodStatements.ActiveMethodStatements().OrderBy(o => o.MethodStatementTitle).MethodStatementOverviewQuery(query).Select(MethodStatementLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<MethodStatementLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public PagedResults<MethodStatementLookup> GetPagedActiveMethodStatementsForJobLookup(int rowCount, int page, string query, Guid jobID)
        {
            var baseQuery = Context.JobMethodStatements.Where(x => 
                x.JobFK == jobID && 
                x.MethodStatement.IsActive && 
                x.IsActive && 
                x.MethodStatement.MethodStatementTitle.Contains(query)
            )
            .OrderBy(x => x.MethodStatement.MethodStatementTitle)
            .Select(x => new MethodStatementLookup
            {
                Key = x.MethodStatement.MethodStatementTitle,
                Value = x.MethodStatement.MethodStatementPK
                /*Revision = x.MethodStatement.Revision*/
            });
            
            int recordCount = baseQuery.Count();

            return new PagedResults<MethodStatementLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public List<JobMethodStatement> GetForJob(Guid jobID)
        {
            return Context.JobMethodStatements.Where(a => a.JobFK == jobID).ToList();
        }

        public List<WorkInstructionMethodStatement> GetForWI(Guid workInstructionID)
        {
            return Context.WorkInstructionMethodStatements.Where(a => a.WorkInstructionFK.Equals(workInstructionID)).ToList();
        }
    }
}
