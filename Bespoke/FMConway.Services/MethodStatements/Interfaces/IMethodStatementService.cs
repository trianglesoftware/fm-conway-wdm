﻿using FMConway.Services.MethodStatements.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MethodStatements.Interfaces
{
    public interface IMethodStatementService : IService
    {
        Guid Create(MethodStatementModel model);
        void Update(MethodStatementModel model);
        MethodStatementModel Single(Guid ID);
        List<JobMethodStatement> GetForJob(Guid jobID);
        List<WorkInstructionMethodStatement> GetForWI(Guid workInstructionID);

        List<MethodStatementModel> GetAllActiveMethodStatements();
        PagedResults<MethodStatementModel> GetActiveMethodStatementsPaged(int rowCount, int page, string query);

        List<MethodStatementModel> GetAllInactiveMethodStatements();
        PagedResults<MethodStatementModel> GetInactiveMethodStatementsPaged(int rowCount, int page, string query);

        PagedResults<MethodStatementLookup> GetPagedActiveMethodStatementsLookup(int rowCount, int page, string query);
        PagedResults<MethodStatementLookup> GetPagedActiveMethodStatementsForJobLookup(int rowCount, int page, string query, Guid jobID);
    }
}
