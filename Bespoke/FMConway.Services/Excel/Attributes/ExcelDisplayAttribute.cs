﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Excel.Attributes
{
    public sealed class ExcelDisplayAttribute : Attribute
    {
        public string Name { get; set; }
        public bool Hidden { get; set; }
        public string Format { get; set; }

        public ExcelDisplayAttribute(string name = null)
        {
            Name = name;
        }
    }
}
