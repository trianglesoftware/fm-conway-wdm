﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Excel.Models
{
    public class ExcelHeader
    {
        public PropertyInfo PropertyInfo { get; set; }
        public int Column { get; set; }
    }
}
