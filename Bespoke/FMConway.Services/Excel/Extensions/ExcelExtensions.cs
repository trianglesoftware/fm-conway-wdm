﻿using FMConway.Services.Excel.Attributes;
using FMConway.Services.Excel.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Excel.Extensions
{
    public static class ExcelExtensions
    {
        private static void ReflectColumnType(DataTable dataTable, ColumnType columnType)
        {
            var maxRows = 100;
            columnType.IsDateTime = true;
            columnType.IsDate = true;
            columnType.IsNumber = true;
            columnType.HasValue = false;

            var i = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                var value = row[columnType.Ordinal]?.ToString();
                if (!String.IsNullOrWhiteSpace(value))
                {
                    columnType.HasValue = true;

                    i++;
                    if (i >= maxRows)
                    {
                        break;
                    }

                    if (columnType.IsDateTime)
                    {
                        DateTime dateTime;
                        if (!DateTime.TryParseExact(value, "dd/MM/yyyy hh:mm:ss", null, System.Globalization.DateTimeStyles.None, out dateTime))
                        {
                            columnType.IsDateTime = false;
                        }
                    }

                    if (columnType.IsDate)
                    {
                        DateTime dateTime;
                        if (!DateTime.TryParseExact(value, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dateTime))
                        {
                            columnType.IsDate = false;
                        }
                    }

                    if (columnType.IsNumber)
                    {
                        decimal number;
                        if (!decimal.TryParse(value, out number))
                        {
                            columnType.IsNumber = false;
                        }
                        else
                        {
                            // excel will trim leading 0's, to preserve them the value should be stored as text
                            if (number > 0 && value[0] == '0')
                            {
                                columnType.IsNumber = false;
                            }
                        }
                    }

                    // if all possible types have been exhausted but has value, return and use text format
                    if (!columnType.IsDateTime && !columnType.IsDate && !columnType.IsNumber)
                    {
                        break;
                    }
                }
            }

            columnType.Style = 
                !columnType.HasValue ? "General" :
                columnType.IsDateTime ? "dd/mm/yyyy hh:mm:ss" :
                columnType.IsDate ? "dd/mm/yyyy" :
                columnType.IsNumber ? "General" :
                "@";
        }

        public class ColumnType
        {
            public int Ordinal { get; set; }
            public bool HasValue { get; set; }
            public bool IsDateTime { get; set; }
            public bool IsDate { get; set; }
            public bool IsNumber { get; set; }
            public string Style { get; set; }

            public ColumnType(int ordinal)
            {
                Ordinal = ordinal;
                Style = "General";
            }
        }

        public static byte[] ToXlsx(this DataTable dataTable, bool reflectColumnTypes = false)
        {
            using (var excel = new ExcelPackage())
            {
                var worksheet = excel.Workbook.Worksheets.Add("Sheet1");

                var i = 0;
                var columnTypes = new List<ColumnType>();
                foreach (DataColumn column in dataTable.Columns)
                {
                    i++;
                    worksheet.Cells[1, i].Value = column.ColumnName;

                    var columnType = new ColumnType(i - 1);
                    columnTypes.Add(columnType);

                    if (reflectColumnTypes)
                    {
                        ReflectColumnType(dataTable, columnType);
                        worksheet.Column(i).Style.Numberformat.Format = columnType.Style;
                    }
                }

                i = 1;
                foreach (DataRow row in dataTable.Rows)
                {
                    i++;
                    var j = 0;
                    foreach (object cell in row.ItemArray)
                    {
                        j++;

                        // unfortunately the column format alone is not enough to dictate the format of the cell.
                        // the value set must be converted into it's appropriate type

                        var columnType = columnTypes[j - 1];

                        var value = cell?.ToString();
                        if (value == null)
                        {
                            worksheet.Cells[i, j].Value = value;
                        }
                        else if (columnType.IsDateTime)
                        {
                            DateTime dateTime;
                            if (DateTime.TryParseExact(value, "dd/MM/yyyy hh:mm:ss", null, System.Globalization.DateTimeStyles.None, out dateTime))
                            {
                                worksheet.Cells[i, j].Value = dateTime;
                            }
                            else
                            {
                                worksheet.Cells[i, j].Value = value;
                            }
                        }
                        else if (columnType.IsDate)
                        {
                            DateTime dateTime;
                            if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dateTime))
                            {
                                worksheet.Cells[i, j].Value = dateTime;
                            }
                            else
                            {
                                worksheet.Cells[i, j].Value = value;
                            }
                        }
                        else if (columnType.IsNumber)
                        {
                            Decimal number;
                            if (Decimal.TryParse(value, out number))
                            {
                                worksheet.Cells[i, j].Value = number;
                            }
                            else
                            {
                                worksheet.Cells[i, j].Value = value;
                            }
                        }
                        else if (columnType.HasValue)
                        {
                            worksheet.Cells[i, j].Value = value;
                        }
                        else
                        {
                            worksheet.Cells[i, j].Value = value;
                        }
                    }
                }

                i = 0;
                foreach (DataColumn column in dataTable.Columns)
                {
                    i++;
                    worksheet.Column(i).AutoFit();
                }

                return excel.GetAsByteArray();

            }
        }

        private static byte[] ToXlsx<T>(this IEnumerable<T> enumerable)
        {
            using (var excel = new ExcelPackage())
            {
                var worksheet = excel.Workbook.Worksheets.Add("Sheet1");

                var properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                var headers = new List<ExcelHeader>();

                var i = 0;
                foreach (var property in properties)
                {
                    i++;
                    headers.Add(new ExcelHeader
                    {
                        PropertyInfo = property,
                        Column = i
                    });

                    var excelAttribute = property.GetCustomAttribute(typeof(ExcelDisplayAttribute), true) as ExcelDisplayAttribute;
                    if (excelAttribute != null && excelAttribute.Name != null)
                    {
                        worksheet.Cells[1, i].Value = excelAttribute.Name;
                    }
                    else
                    {
                        worksheet.Cells[1, i].Value = property.Name;
                    }

                    if (excelAttribute != null && excelAttribute.Hidden)
                    {
                        worksheet.Column(i).Hidden = true;
                    }
                }

                i = 1;
                foreach (var row in enumerable)
                {
                    i++;
                    var j = 0;
                    foreach (var header in headers)
                    {
                        j++;
                        var value = header.PropertyInfo.GetValue(row, null);
                        worksheet.Cells[i, j].Value = value;
                    }
                }

                return excel.GetAsByteArray();
            }
        }
    }
}
