﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.ActivityPriorities.Models;
using FMConway.Services.ActivityPriorities.Extensions;

namespace FMConway.Services.ActivityPriorities.Services
{
    public class ActivityPriorityService : Service, FMConway.Services.ActivityPriorities.Interfaces.IActivityPriorityService
    {
        UserContext _userContext;

        public ActivityPriorityService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public PagedResults<ActivityPriorityLookup> GetPagedActiveActivityPriorityLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.ActivityPriorities.OrderBy(o => o.Name).ActivityPriorityOverviewQuery(query).Select(ActivityPriorityLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<ActivityPriorityLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
            };
        }
    }
}
