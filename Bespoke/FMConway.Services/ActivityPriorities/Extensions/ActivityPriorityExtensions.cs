﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ActivityPriorities.Extensions
{
    public static class ActivityPriorityExtensions
    {
        public static IQueryable<ActivityPriority> ActivityPriorityOverviewQuery(this IQueryable<ActivityPriority> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
