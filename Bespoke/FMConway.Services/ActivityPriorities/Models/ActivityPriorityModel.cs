﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.ActivityPriorities.Models
{
    public class ActivityPriorityModel
    {
        public Guid? ActivityPriorityID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<ActivityPriority, ActivityPriorityModel>> EntityToModel = entity => new ActivityPriorityModel
        {
            ActivityPriorityID = entity.ActivityPriorityPK,
            Name = entity.Name,
            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };
    }
}
