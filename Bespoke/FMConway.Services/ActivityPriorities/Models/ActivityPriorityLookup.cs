﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services;
namespace FMConway.Services.ActivityPriorities.Models
{
    public class ActivityPriorityLookup : LookupBase<Guid>
    {
        public static Expression<Func<ActivityPriority, ActivityPriorityLookup>> EntityToModel = entity => new ActivityPriorityLookup()
        {
            Key = entity.Name,
            Value = entity.ActivityPriorityPK
        };
    }
}
