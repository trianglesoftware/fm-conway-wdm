﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.ActivityPriorities.Models;

namespace FMConway.Services.ActivityPriorities.Interfaces
{
    public interface IActivityPriorityService : IService
    {
        PagedResults<ActivityPriorityLookup> GetPagedActiveActivityPriorityLookup(int rowCount, int page, string query);
    }
}
