﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheetTemplates.Extensions
{
    public static class LoadingSheetTemplateExtensions
    {
        public static IQueryable<LoadingSheetTemplate> ActiveLoadingSheetTemplates(this IQueryable<LoadingSheetTemplate> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<LoadingSheetTemplate> InactiveLoadingSheetTemplates(this IQueryable<LoadingSheetTemplate> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<LoadingSheetTemplate> LoadingSheetTemplateOverviewQuery(this IQueryable<LoadingSheetTemplate> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
