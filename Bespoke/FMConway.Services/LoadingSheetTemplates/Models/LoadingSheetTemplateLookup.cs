﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheetTemplates.Models
{
    public class LoadingSheetTemplateLookup : LookupBase<Guid>
    {
        public static Expression<Func<LoadingSheetTemplate, LoadingSheetTemplateLookup>> EntityToModel = entity => new LoadingSheetTemplateLookup()
        {
            Key = entity.Name,
            Value = entity.LoadingSheetTemplatePK,
        };
    }
}
