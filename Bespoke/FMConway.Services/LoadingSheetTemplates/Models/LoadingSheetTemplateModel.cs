﻿using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.LoadingSheetTemplates.Models
{
    public class LoadingSheetTemplateModel
    {
        public Guid LoadingSheetTemplateID { get; set; }
        [Required]
        public string Name { get; set; }
        //public Guid VehicleTypeID { get; set; }
        //public VehicleTypeModel VehicleType { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<LoadingSheetTemplate, LoadingSheetTemplateModel>> EntityToModel = entity => new LoadingSheetTemplateModel
        {
            LoadingSheetTemplateID = entity.LoadingSheetTemplatePK,
            Name = entity.Name,
            //VehicleTypeID = entity.VehicleTypeFK,
            //VehicleType = new VehicleTypeModel()
            //{
            //    Name = entity.VehicleType.Name
            //},

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<LoadingSheetTemplateModel, LoadingSheetTemplate> ModelToEntity = model => new LoadingSheetTemplate
        {
            LoadingSheetTemplatePK = model.LoadingSheetTemplateID,
            Name = model.Name,
            //VehicleTypeFK = model.VehicleTypeID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
