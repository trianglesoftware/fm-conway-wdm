﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.LoadingSheetTemplates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.LoadingSheetTemplates.Interfaces
{
    public interface ILoadingSheetTemplateService : IService
    {
        Guid Create(LoadingSheetTemplateModel model);
        List<LoadingSheetTemplateModel> GetActiveLoadingSheetTemplates();
        List<LoadingSheetTemplateModel> GetInactiveLoadingSheetTemplates();
        LoadingSheetTemplateModel Single(Guid id);
        void Update(LoadingSheetTemplateModel model);

        PagedResults<LoadingSheetTemplateLookup> GetPagedActiveLoadingSheetTemplatesLookup(int rowCount, int page, string query);
    }
}
