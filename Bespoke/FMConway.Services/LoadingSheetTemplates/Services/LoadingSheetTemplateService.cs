﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.LoadingSheetTemplates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.LoadingSheetTemplates.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.LoadingSheetTemplates.Services
{
    public class LoadingSheetTemplateService : Service, FMConway.Services.LoadingSheetTemplates.Interfaces.ILoadingSheetTemplateService
    {
        UserContext _userContext;

        public LoadingSheetTemplateService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<LoadingSheetTemplateModel> GetActiveLoadingSheetTemplates()
        {
            return Context.LoadingSheetTemplates.Where(a => a.IsActive).Select(LoadingSheetTemplateModel.EntityToModel).ToList();
        }

        public List<LoadingSheetTemplateModel> GetInactiveLoadingSheetTemplates()
        {
            return Context.LoadingSheetTemplates.Where(a => !a.IsActive).Select(LoadingSheetTemplateModel.EntityToModel).ToList();
        }

        public Guid Create(LoadingSheetTemplateModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.LoadingSheetTemplateID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.LoadingSheetTemplates.Add(LoadingSheetTemplateModel.ModelToEntity(model));

            SaveChanges();

            return model.LoadingSheetTemplateID;
        }

        public void Update(LoadingSheetTemplateModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            LoadingSheetTemplate current = Context.LoadingSheetTemplates.Where(x => x.LoadingSheetTemplatePK.Equals(model.LoadingSheetTemplateID)).SingleOrDefault();

            current.Name = model.Name == null ? "" : model.Name;
            //current.VehicleTypeFK = model.VehicleTypeID;

            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;

            SaveChanges();
        }

        public LoadingSheetTemplateModel Single(Guid id)
        {
            return Context.LoadingSheetTemplates.Where(a => a.LoadingSheetTemplatePK.Equals(id)).Select(LoadingSheetTemplateModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<LoadingSheetTemplateLookup> GetPagedActiveLoadingSheetTemplatesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.LoadingSheetTemplates.ActiveLoadingSheetTemplates().OrderBy(o => o.Name).LoadingSheetTemplateOverviewQuery(query).Select(LoadingSheetTemplateLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<LoadingSheetTemplateLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
