﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.CertificateTypes.Models
{
    public class CertificateTypeLookup : LookupBase<Guid>
    {
        public static Expression<Func<CertificateType, CertificateTypeLookup>> EntityToModel = entity => new CertificateTypeLookup()
        {
            Key = entity.Name,
            Value = entity.CertificateTypePK,
        };
    }
}
