﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.CertificateTypes.Models
{
    public class CertificateTypeModel
    {
        public Guid CertificateTypeID { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<CertificateType, CertificateTypeModel>> EntityToModel = entity => new CertificateTypeModel
        {
            CertificateTypeID = entity.CertificateTypePK,
            Name = entity.Name,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<CertificateTypeModel, CertificateType> ModelToEntity = model => new CertificateType
        {
            CertificateTypePK = model.CertificateTypeID,
            Name = model.Name,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
