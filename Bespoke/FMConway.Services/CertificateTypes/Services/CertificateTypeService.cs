﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.CertificateTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.CertificateTypes.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.CertificateTypes.Services
{
    public class CertificateTypeService : Service, FMConway.Services.CertificateTypes.Interfaces.ICertificateTypeService
    {
        UserContext _userContext;

        public CertificateTypeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<CertificateTypeModel> GetActiveCertificateTypes()
        {
            return Context.CertificateTypes.Where(a => a.IsActive).Select(CertificateTypeModel.EntityToModel).ToList();
        }

        public List<CertificateTypeModel> GetInactiveCertificateTypes()
        {
            return Context.CertificateTypes.Where(a => !a.IsActive).Select(CertificateTypeModel.EntityToModel).ToList();
        }

        public void Create(CertificateTypeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.CertificateTypeID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.CertificateTypes.Add(CertificateTypeModel.ModelToEntity(model));

            SaveChanges();
        }

        public void Update(CertificateTypeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            CertificateType current = Context.CertificateTypes.Where(x => x.CertificateTypePK.Equals(model.CertificateTypeID)).SingleOrDefault();

            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.Name = model.Name == null ? "" : model.Name;

            SaveChanges();
        }

        public CertificateTypeModel Single(Guid id)
        {
            return Context.CertificateTypes.Where(a => a.CertificateTypePK.Equals(id)).Select(CertificateTypeModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<CertificateTypeLookup> GetPagedActiveCertificateTypesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.CertificateTypes.ActiveCertificateTypes().OrderBy(o => o.Name).CertificateTypeOverviewQuery(query).Select(CertificateTypeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<CertificateTypeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
