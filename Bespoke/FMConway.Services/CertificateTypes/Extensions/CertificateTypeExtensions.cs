﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.CertificateTypes.Extensions
{
    public static class CertificateTypeExtensions
    {
        public static IQueryable<CertificateType> ActiveCertificateTypes(this IQueryable<CertificateType> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<CertificateType> InactiveCertificateTypes(this IQueryable<CertificateType> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<CertificateType> CertificateTypeOverviewQuery(this IQueryable<CertificateType> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
