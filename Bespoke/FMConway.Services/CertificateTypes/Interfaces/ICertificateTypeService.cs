﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.CertificateTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.CertificateTypes.Interfaces
{
    public interface ICertificateTypeService : IService
    {
        void Create(CertificateTypeModel model);
        List<CertificateTypeModel> GetActiveCertificateTypes();
        List<CertificateTypeModel> GetInactiveCertificateTypes();
        CertificateTypeModel Single(Guid id);
        void Update(CertificateTypeModel model);

        PagedResults<CertificateTypeLookup> GetPagedActiveCertificateTypesLookup(int rowCount, int page, string query);
    }
}
