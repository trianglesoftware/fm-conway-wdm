﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services
{
    public class Service : FMConway.Services.IService
    {
        public ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public string GeneralErrorMessage { get { return $"{DateTime.Now.ToString()} - An error has occurred. Please contact triangle for support"; } }

        private FMConwayEntities _context;
        private bool _managedExternally = false;

        public FMConwayEntities Context
        {
            get { return _context; }
        }

        public void SetContext(FMConwayEntities context, bool managedExternally = true)
        {
            _managedExternally = managedExternally;
            _context = context;
        }

        public Service()
        {
            _context = new FMConwayEntities();
            _context.Database.CommandTimeout = 180;
        }

        public virtual void SaveChanges()
        {
            if (!_managedExternally)
                _context.SaveChanges();
        }

    }
}
