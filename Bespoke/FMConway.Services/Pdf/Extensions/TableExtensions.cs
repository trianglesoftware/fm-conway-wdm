﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Extensions
{
    public static class TableExtensions
    {
        public static Table BorderOutline(this Table table, Unit width)
        {
            var rows = table.Rows.Count;
            var columns = table.Columns.Count;

            for (var i = 0; i < rows; i++)
            {
                var row = table.Rows[i];

                if (i == 0)
                {
                    row.Borders.Top.Width = width;
                }

                row.Cells[0].Borders.Left.Width = width;
                row.Cells[columns - 1].Borders.Right.Width = width;

                if (i == rows - 1)
                {
                    row.Borders.Bottom.Width = width;
                }
            }

            return table;
        }

        public static Table HeaderBold(this Table table)
        {
            var rows = table.Rows.Count;
            var columns = table.Columns.Count;

            if (rows == 0 || columns == 0)
            {
                return table;
            }

            var header = table.Rows[0];

            for (var i = 0; i < columns; i++)
            {
                header.Cells[i].Format.Font.Bold = true;
            }

            return table;
        }
    }
}
