﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Extensions
{
    public static class UnitExtensions
    {
        public static double ConvertPointsToPixels(double points)
        {
            return points * 1.29;
        }
    }
}
