﻿using FMConway.Services.Pdf.Attributes;
using FMConway.Services.Pdf.Enums;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Extensions
{
    public static class SectionExtensions
    {
        public static double Width(this Section section)
        {
            return Math.Round(section.PageSetup.PageWidth - section.PageSetup.LeftMargin - section.PageSetup.RightMargin, 0);
        }

        public static Table AddTable<T>(this Section section, T element)
        {
            return section.AddTable(new List<T>
            {
                element
            });
        }

        public static Table AddTable<T>(this Section section, List<T> items)
        {
            var properties = typeof(T)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(a => a.GetCustomAttributes<PdfTableColumnAttribute>().FirstOrDefault() != null)
                .ToList();

            if (!properties.Any())
            {
                return null;
            }

            var table = section.AddTable();

            // columns
            foreach (var property in properties)
            {
                var columnAttribute = property.GetCustomAttributes<PdfTableColumnAttribute>().Single();

                double width = 0;
                if (columnAttribute.WidthType == WidthType.Percent)
                {
                    width = (section.Width() / 100) * columnAttribute.Width;
                }
                else
                {
                    width = columnAttribute.Width;
                }

                var column = table.AddColumn(width);
            }

            // header
            var row = table.AddRow();
            row.Shading.Color = ColourEx.LightGray;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;

            var i = -1;
            foreach (var property in properties)
            {
                i++;
                var columnAttribute = property.GetCustomAttributes<PdfTableColumnAttribute>().Single();
                var par = row.Cells[i].AddParagraph(columnAttribute.Name);
                //par.Format.Font.Bold = true;
                par.Format.Alignment = ParagraphAlignment.Center;
            }

            // body
            foreach (var item in items)
            {
                row = table.AddRow();
                row.Borders.Width = 0.5;
                row.Format.SpaceBefore = 1.5;
                row.Format.SpaceAfter = 1.5;

                var j = -1;
                foreach (var property in properties)
                {
                    j++;

                    var objectValue = property.GetValue(item);
                    if (property.PropertyType == typeof(byte[]))
                    {
                        var byteArray = (byte[])objectValue;
                        if (byteArray == null)
                        {
                            continue;
                        }

                        var bmp = ImageExtensions.ToBitmap(byteArray);

                        var cropWhitespaceAttribute = property.GetCustomAttributes<ImageCropWhitespaceAttribute>().SingleOrDefault();
                        if (cropWhitespaceAttribute != null)
                        {
                            bmp = ImageExtensions.CropWhitespace(bmp);
                        }

                        var rescaleAttribute = property.GetCustomAttributes<ImageRescaleAttribute>().SingleOrDefault();
                        if (rescaleAttribute != null)
                        {
                            bmp = ImageExtensions.Rescale(bmp, rescaleAttribute.MaxWidth, rescaleAttribute.MaxHeight);
                        }

                        var paddingAttribute = property.GetCustomAttributes<ImagePaddingAttribute>().SingleOrDefault();
                        if (paddingAttribute != null)
                        {
                            bmp = ImageExtensions.Padding(bmp, paddingAttribute.Padding);
                        }
                        
                        byteArray = ImageExtensions.ToByteArray(bmp);

                        var base64 = "base64:" + Convert.ToBase64String(byteArray);

                        var par = row.Cells[j].AddImage(base64);
                    }
                    else
                    {
                        string value = "";

                        if (objectValue != null)
                        {
                            var columnAttribute = property.GetCustomAttributes<PdfTableColumnAttribute>().Single();
                            if (columnAttribute.Format != null)
                            {
                                value = string.Format("{0:" + columnAttribute.Format + "}", objectValue);
                            }
                            else
                            {
                                value = objectValue.ToString();
                            }

                            var alignment = columnAttribute.Alignment == Alignment.Left ? ParagraphAlignment.Left :
                                columnAttribute.Alignment == Alignment.Center ? ParagraphAlignment.Center :
                                ParagraphAlignment.Right;

                            row.Cells[j].Format.Alignment = alignment;
                        }

                        var par = row.Cells[j].AddParagraph(value);
                        par.Format.Font.Size = 8;
                    }
                }
            }

            return table;
        }

        public static Paragraph AddLineSpacing(this Section section, Unit space)
        {
            var par = section.AddParagraph();
            par.Format.LineSpacingRule = LineSpacingRule.Exactly;
            par.Format.LineSpacing = Unit.FromMillimeter(0.0);
            par.Format.SpaceAfter = space;
            return par;
        }
    }
}
