﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Extensions
{
    public static class DocumentExtensions
    {
        public static byte[] ToByteArray(this Document document)
        {
            var pdfRenderer = new PdfDocumentRenderer(false);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();

            using (var memoryStream = new MemoryStream())
            {
                pdfRenderer.Save(memoryStream, false);
                return memoryStream.ToArray();
            }
        }

        public static void DefaultStyles(this Document document)
        {
            var style = document.Styles["Normal"];
            style.Font.Size = 9;

            style = document.Styles.AddStyle("Title", "Normal");
            style.Font.Size = 10;
            style.Font.Bold = true;

            style = document.Styles.AddStyle("Header", "Normal");
            style.Font.Size = 9;
            style.Font.Bold = true;

            style = document.Styles.AddStyle("Table-Header", "Normal");
            style.Font.Size = 9;
            style.Font.Bold = true;
        }

        public static Section DefaultSection(this Document document)
        {
            var section = document.AddSection();
            section.PageSetup = document.DefaultPageSetup.Clone();
            section.PageSetup.PageFormat = PageFormat.A4;
            section.PageSetup.LeftMargin = 40;
            section.PageSetup.RightMargin = 40;
            section.PageSetup.TopMargin = 40;

            return section;
        }

        public static Row DefaultStyle(this Row row)
        {
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;

            return row;
        }

        public static List<Paragraph> AddParagraphs(this Cells cells, params string[] text)
        {
            var paragraphs = new List<Paragraph>();

            for (var i = 0; i < text.Length; i++)
            {
                var paragraph = cells[i].AddParagraph(text[i]);
                paragraphs.Add(paragraph);
            }

            var emptyCells = cells.Table.Columns.Count - text.Length;
            if (emptyCells > 0)
            {
                cells[text.Length - 1].MergeRight = emptyCells;
            }

            return paragraphs;
        }
    }
}
