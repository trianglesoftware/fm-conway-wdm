﻿using FMConway.Services.Pdf.Extensions;
using FMConway.Services.Pdf.Models;
using FMConway.Services.Tasks.Enums;
using FMConway.Services.WorkInstructions.Extensions;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.Pdf.Services
{
    public class PdfService : Service
    {
        UserContext _userContext;

        public PdfService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public byte[] GetBriefingSheet(Guid workInstructionID)
        {
            var workInstruction = Context.WorkInstructions.Where(a => a.WorkInstructionPK == workInstructionID).Single();

            var model = new BriefingSheetModel();
            model.SRW = workInstruction.SRWNumber ?? "";
            model.Date = workInstruction.WorkInstructionStartDate.ToShortDateString();
            model.Weather = workInstruction.Tasks.Where(b => b.TaskType.TaskType1 == "Weather").Select(b => b.WeatherCondition?.Name).SingleOrDefault() ?? "";
            model.Postcodes = workInstruction.Postcode ?? "";
            model.Location = workInstruction.AddressInLine() ?? "";
            model.Task = workInstruction.WorkInstructionNumber.ToString();
            model.RAMS = workInstruction.WorkInstructionMethodStatements.Select(a => a.MethodStatement.MethodStatementTitle).FirstOrDefault() ?? "";
            model.CustomerNumber = workInstruction.Job.Contract.Customer.PhoneNumber ?? "";
            model.Details = workInstruction.Comments;

            var shiftVehicles = workInstruction.Schedules.SelectMany(a => a.Shift.ShiftVehicles.Where(b => b.IsActive)).ToList();
            for (var i = 0; i < shiftVehicles.Count; i++)
            {
                if (i >= 2)
                {
                    break;
                }

                if (i == 0)
                {
                    model.VRN1 = shiftVehicles[i]?.Vehicle?.Registration ?? "";
                    model.Driver1 = shiftVehicles[i]?.Employee?.EmployeeName ?? "";
                }
                else if (i == 1)
                {
                    model.VRN2 = shiftVehicles[i]?.Vehicle?.Registration ?? "";
                    model.Driver2 = shiftVehicles[i]?.Employee?.EmployeeName ?? "";
                }
            }

            foreach (var scheduleEmployee in workInstruction.Schedules.SelectMany(b => b.ScheduleEmployees))
            {
                var workInstructionSignature = workInstruction.WorkInstructionSignatures.Where(a => a.EmployeeFK == scheduleEmployee.EmployeeFK).FirstOrDefault();

                var workforceModel = new WorkforceModel();
                workforceModel.Name = scheduleEmployee.Employee.EmployeeName;
                workforceModel.Qualification = string.Join(", ", scheduleEmployee.Employee.EmployeeCertificates.Select(c => c.CertificateType.Name).ToList()) ?? "";
                workforceModel.Company = scheduleEmployee.Employee.Depot.DepotName;
                workforceModel.Signature = workInstructionSignature?.File?.Data;
                workforceModel.Date = workInstruction.WorkInstructionStartDate.ToShortDateString();
                model.Workforces.Add(workforceModel);
            }

            var trafficModel = new TrafficModel();
            trafficModel.Location = workInstruction.AddressLine1 ?? "";
            trafficModel.Time = workInstruction.Tasks.Where(a => a.TaskType.TaskType1 == "Traffic Count").Select(a => a.EndTime).SingleOrDefault()?.ToShortTimeString() ?? "";
            trafficModel.Total = workInstruction.Tasks.Where(a => a.TaskType.TaskType1 == "Traffic Count").Select(a => a.Notes).FirstOrDefault() ?? "";
            model.Traffic.Add(trafficModel);

            var jobTimes = new JobTimeModel();
            jobTimes.StartShift = workInstruction.Tasks.Where(a => a.Name == "Start Journey").Select(a => a.EndTime).SingleOrDefault()?.ToShortTimeString() ?? "";
            jobTimes.OnSite = workInstruction.Tasks.Where(a => a.Name == "Arrive on Site").Select(a => a.EndTime).SingleOrDefault()?.ToShortTimeString() ?? "";
            jobTimes.FirstCone = workInstruction.Tasks.Where(a => a.Name == "First Cone").Select(a => a.EndTime).SingleOrDefault()?.ToShortTimeString() ?? "";
            jobTimes.Installed = workInstruction.Tasks.Where(a => a.Name == "Job Installed").Select(a => a.EndTime).SingleOrDefault()?.ToShortTimeString() ?? "";
            jobTimes.EndShift = workInstruction.Tasks.Where(a => a.Name == "Leave Site").Select(a => a.EndTime).SingleOrDefault()?.ToShortTimeString() ?? "";
            model.JobTimes = jobTimes;

            var count = 0;
            foreach (var maintenanceCheck in workInstruction.MaintenanceChecks.OrderBy(a => a.Time))
            {
                count = count + 1;
                var maintenanceLog = new MaintenanceLogModel();
                maintenanceLog.Question = $"Maintenance Log: {count}";
                maintenanceLog.Details = maintenanceCheck.Description ?? "";
                maintenanceLog.Time = maintenanceCheck.Time.ToShortTimeString();
                model.MaintenanceLogs.Add(maintenanceLog);

                foreach (var answer in maintenanceCheck.MaintenanceCheckAnswers.OrderBy(a => a.ChecklistQuestion.QuestionOrder))
                {
                    maintenanceLog = new MaintenanceLogModel();
                    maintenanceLog.Question = answer.ChecklistQuestion.ChecklistQuestion1 ?? "";
                    maintenanceLog.Answer = answer.Answer ? "Yes" : "No";
                    maintenanceLog.Details = answer.MaintenanceCheckDetails.Select(a => a.Description).FirstOrDefault() ?? "";
                    maintenanceLog.Time = maintenanceCheck.Time.ToShortTimeString();
                    model.MaintenanceLogs.Add(maintenanceLog);
                }
            }

            // generate document
            var document = new Document();
            document.DefaultStyles();

            var section = document.DefaultSection();

            var par = section.AddParagraph("TM SITE / JOB BRIEFING SHEET", "Title");
            par.Format.SpaceAfter = 12;

            var table = section.AddTable();
            table.Format.SpaceAfter = 7;
            table.AddColumn(400);
            table.AddColumn(200);
            var row = table.AddRow();
            row.Cells[0].AddParagraph($"SRW / Ref No: {model.SRW}");
            row.Cells[1].AddParagraph($"Date: {model.Date}");

            table = section.AddTable();
            table.Format.SpaceAfter = 7;
            table.AddColumn(200);
            table.AddColumn(200);
            table.AddColumn(200);
            row = table.AddRow();
            row.Cells[0].AddParagraph($"Weather / Visibility: {model.Weather}");
            row.Cells[1].AddParagraph($"Road Speed: {model.RoadSpeed}");
            row.Cells[2].AddParagraph($"Postcodes: {model.Postcodes}");

            par = section.AddParagraph($"Location / Road / Route: {model.Location}");
            par.Format.SpaceAfter = 7;

            par = section.AddParagraph($"Task: {model.Task}");
            par.Format.SpaceAfter = 7;

            table = section.AddTable();
            table.Format.SpaceAfter = 7;
            table.AddColumn(200);
            table.AddColumn(400);
            row = table.AddRow();
            row.Cells[0].AddParagraph($"RAMS: {model.RAMS}");
            row.Cells[1].AddParagraph($"Customer & Contact No: {model.CustomerNumber}");

            table = section.AddTable();
            table.Format.SpaceAfter = 7;
            table.AddColumn(200);
            table.AddColumn(400);
            row = table.AddRow();
            row.Cells[0].AddParagraph($"VRN: {model.VRN1}");
            row.Cells[1].AddParagraph($"Driver: {model.Driver1}");

            table = section.AddTable();
            table.Format.SpaceAfter = 7;
            table.AddColumn(200);
            table.AddColumn(400);
            row = table.AddRow();
            row.Cells[0].AddParagraph($"VRN: {model.VRN2}");
            row.Cells[1].AddParagraph($"Driver: {model.Driver2}");

            // workforce
            table = section.AddTable();
            table.Format.SpaceAfter = 7;
            table.AddColumn(55);
            table.AddColumn(440);
            row = table.AddRow();
            par = row.Cells[0].AddParagraph("Workforce:");
            par.Style = "Header";
            par = row.Cells[1].AddParagraph("(Ensure all personnel have signed for confirm understanding of Company Risk Assessment Method Statements)");
            par.Format.Font.Size = "8";
            par.Format.SpaceBefore = 1;

            table = section.AddTable(model.Workforces);

            // traffic count
            par = section.AddParagraph("Traffic Count:");
            par.Format.SpaceBefore = 7;
            par.Format.SpaceAfter = 7;
            par.Format.Font.Bold = true;

            table = section.AddTable(model.Traffic);

            // job times
            par = section.AddParagraph("Job Times - PLEASE ENSURE THESE ARE COMPLETED");
            par.Format.SpaceBefore = 7;
            par.Format.SpaceAfter = 7;
            par.Format.Font.Bold = true;

            table = section.AddTable(model.JobTimes);

            // maintenance log
            par = section.AddParagraph();
            par.Format.LineSpacingRule = LineSpacingRule.Exactly;
            par.Format.LineSpacing = Unit.FromMillimeter(0.0);
            par.Format.SpaceAfter = 10;

            table = section.AddTable(model.MaintenanceLogs);

            // details
            par = section.AddParagraph("Task Details:");
            par.Format.SpaceBefore = 7;
            par.Format.SpaceAfter = 7;
            par.Format.Font.Bold = true;

            par = section.AddParagraph(model.Details);
            par.Format.SpaceBefore = 7;
            par.Format.SpaceAfter = 7;

            // footer
            table = section.Footers.Primary.AddTable();
            table.AddColumn(139);
            table.AddColumn(250);
            table.AddColumn(136);

            row = table.AddRow();
            par = row[1].AddParagraph("DUTY SUPERVISOR - 07739 189598");
            par.Format.SpaceAfter = 5;
            par.Format.Alignment = ParagraphAlignment.Center;
            par.Format.Font.Size = 11;
            par.Format.Font.Bold = true;

            row = table.AddRow();
            par = row[0].AddParagraph("Version 2");
            par = row[1].AddParagraph("CG3-CP-TM-121");
            par.Format.Alignment = ParagraphAlignment.Center;
            par = row[2].AddParagraph("Aug 2017");
            par.Format.Alignment = ParagraphAlignment.Right;

            return document.ToByteArray();
        }

        public byte[] GetOnOffHireNote(Guid workInstructionID, string imagesPath, bool onHire)
        {
            var workInstruction = Context.WorkInstructions.Where(a => a.WorkInstructionPK == workInstructionID).Single();

            var model = new OnHireNoteModel();
            model.JobNumber = workInstruction.Job.JobNumber;
            model.JobTitle = workInstruction.Job.JobTitle ?? "";
            model.Customer = workInstruction.Job.Contract.Customer.CustomerName ?? "";
            model.ContractNumber = workInstruction.Job.Contract.ContractNumber ?? "";
            model.OnHireDate = workInstruction.WorkInstructionStartDate;
            model.DeliveryAddress = workInstruction.AddressNewLine() ?? "";
            model.DeliveryDate = workInstruction.WorkInstructionStartDate;

            var shiftVehicles = workInstruction.Schedules.SelectMany(a => a.Shift.ShiftVehicles.Where(b => b.IsActive)).ToList();
            for (var i = 0; i < shiftVehicles.Count; i++)
            {
                if (i >= 1)
                {
                    break;
                }

                if (i == 0)
                {
                    model.CphVehicleReg = shiftVehicles[i]?.Vehicle?.Registration ?? "";
                    model.CphDriver = shiftVehicles[i]?.Employee?.EmployeeName ?? "";
                }
            }

            var installOrCollectionTask = workInstruction.Tasks.Where(a => 
                (onHire && a.TaskType.TaskType1 == TaskTypesEnum.EquipmentInstall) ||
                (!onHire && a.TaskType.TaskType1 == TaskTypesEnum.EquipmentCollection)
                ).Single();

            var taskEquipments = installOrCollectionTask.TaskEquipments.ToList();

            foreach (var taskEquipment in taskEquipments)
            {
                var isAsset = taskEquipment.TaskEquipmentAssets.Any();
                if (isAsset)
                {
                    foreach (var asset in taskEquipment.TaskEquipmentAssets)
                    {
                        var equipmentModel = new EquipmentModel();
                        equipmentModel.Quantity = 1;
                        equipmentModel.AssetNumber = asset.AssetNumber ?? "";
                        equipmentModel.ItemDescription = taskEquipment.Equipment.EquipmentName + " " + taskEquipment.Equipment.EquipmentType.Name;
                        model.Equipments.Add(equipmentModel);
                    }
                }
                else
                {
                    var equipmentModel = new EquipmentModel();
                    equipmentModel.Quantity = taskEquipment.Quantity;
                    equipmentModel.AssetNumber = "";
                    equipmentModel.ItemDescription = taskEquipment.Equipment.EquipmentName + " " + taskEquipment.Equipment.EquipmentType.Name;
                    model.Equipments.Add(equipmentModel);
                }
            }

            model.Comments.Comment = installOrCollectionTask.Notes ?? "";
            model.IsOutOfHours = workInstruction.IsOutOfHours;

            model.IsEmergencyCancellation = workInstruction?.ReasonCode?.IsEmergencyCancellation ?? false;
            model.ReasonCode = workInstruction?.ReasonCode?.Code ?? "";
            model.ReasonNotes = workInstruction.ReasonNotes ?? "";

            // client sig
            var signatureTask = workInstruction.Tasks.Where(a => a.TaskType.TaskType1 == TaskTypesEnum.Signature).LastOrDefault();

            var clientTaskSignature = signatureTask?.TaskSignatures.Where(a => a.IsActive && a.IsClient).FirstOrDefault();
            model.EquipmentReceived_PrintName = clientTaskSignature?.PrintedName ?? "NOS";
            model.EquipmentReceived_ClientVehicleReg = clientTaskSignature?.ClientVehicleReg ?? "";

            if (clientTaskSignature != null)
            {
                var byteArray = clientTaskSignature.File.Data;
                var bmp = byteArray.ToBitmap();
                bmp = bmp.CropWhitespace();
                bmp = bmp.Rescale(165, 30);
                bmp = bmp.Padding(2);
                byteArray = bmp.ToByteArray();

                model.EquipmentReceived_Signature = byteArray;
            }

            // staff sig
            var staffTaskSignature = signatureTask?.TaskSignatures.Where(a => a.IsActive && a.EmployeeFK != null).FirstOrDefault();
            model.Representative_PrintName = staffTaskSignature?.PrintedName ?? "";

            if (staffTaskSignature != null)
            {
                var byteArray = staffTaskSignature.File.Data;
                var bmp = byteArray.ToBitmap();
                bmp = bmp.CropWhitespace();
                bmp = bmp.Rescale(165, 30);
                bmp = bmp.Padding(2);
                byteArray = bmp.ToByteArray();

                model.Representative_Signature = byteArray;
            }

            // generate document
            var document = new Document();
            document.DefaultStyles();

            var section = document.DefaultSection();

            // header
            var image = section.AddImage(Path.Combine(imagesPath, "fmconway_logo.png"));
            image.Width = 100;
            image.Left = ShapePosition.Left;
            image.WrapFormat.DistanceBottom = 15;
            image.WrapFormat.Style = WrapStyle.Through;

            var par = section.AddParagraph("CONWAY HOUSE - VESTRY ROAD - SEVENOAKS - KENT - TN14 5EL");
            par.Format.SpaceBefore = 13;
            par.Format.LeftIndent = 80;
            par.Format.Alignment = ParagraphAlignment.Center;
            par.Format.Font.Size = 10;
            par = section.AddParagraph("www.fmconway.co.uk | enquiries@fmconway.co.uk");
            par.Format.LeftIndent = 80;
            par.Format.Alignment = ParagraphAlignment.Center;
            par = section.AddParagraph("T: 01895 823711");
            par.Format.LeftIndent = 80;
            par.Format.Alignment = ParagraphAlignment.Center;

            section.AddLineSpacing(30);

            // top table
            var table = section.AddTable();
            table.AddColumn(section.Width());

            var row = table.AddRow();
            row.Shading.Color = Colors.LightGray;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;

            par = row.Cells[0].AddParagraph($"{(onHire ? "ON" : "OFF")} HIRE NOTE");
            par.Format.Font.Size = 17;
            par.Format.Alignment = ParagraphAlignment.Center;
            par.Format.Font.Bold = true;

            section.AddLineSpacing(7);

            par = section.AddParagraph($"Job Title: {model.JobTitle}");
            par.Format.Font.Size = 10;
            par.Format.Alignment = ParagraphAlignment.Left;
            par.Format.SpaceAfter = 7;
            par.Format.AddTabStop(507, TabAlignment.Right);
            par.AddTab();
            par.AddText($"Job Number: {model.JobNumber}");

            table = section.AddTable();
            table.Format.SpaceAfter = 7;
            table.AddColumn(section.Width() * 0.56);
            table.AddColumn(section.Width() * 0.22);
            table.AddColumn(section.Width() * 0.22);

            row = table.AddRow();
            row.Shading.Color = Colors.LightGray;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph($"CUSTOMER / REQUESTED BY");
            par = row.Cells[1].AddParagraph($"CONTRACT NUMBER");
            par.Format.Alignment = ParagraphAlignment.Center;
            par = row.Cells[2].AddParagraph($"{(onHire ? "ON" : "OFF")} HIRE DATE");
            par.Format.Alignment = ParagraphAlignment.Center;

            row = table.AddRow();
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 5;
            row.Format.SpaceAfter = 5;
            row.Format.Font.Size = 9;
            row.Cells[0].AddParagraph(model.Customer);
            par = row.Cells[1].AddParagraph(model.ContractNumber);
            par.Format.Alignment = ParagraphAlignment.Center;
            par = row.Cells[2].AddParagraph(model.OnHireDate.ToShortDateString());
            par.Format.Alignment = ParagraphAlignment.Center;

            row = table.AddRow();
            row.Shading.Color = Colors.LightGray;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph($"DELIVERY ADDRESS");
            row.Cells[0].MergeRight = 1;
            par = row.Cells[2].AddParagraph($"DELIVERY DATE");
            par.Format.Alignment = ParagraphAlignment.Center;

            row = table.AddRow();
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 3;
            row.Format.SpaceAfter = 3;
            row.Format.Font.Size = 9;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].MergeDown = 4;
            row.Cells[0].AddParagraph(model.DeliveryAddress);
            par = row.Cells[2].AddParagraph(model.DeliveryDate.ToShortDateString());
            par.Format.Alignment = ParagraphAlignment.Center;

            row = table.AddRow();
            row.Shading.Color = Colors.LightGray;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Format.Font.Bold = true;
            par = row.Cells[2].AddParagraph($"DRIVER");
            par.Format.Alignment = ParagraphAlignment.Center;

            row = table.AddRow();
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 3;
            row.Format.SpaceAfter = 3;
            row.Format.Font.Size = 9;
            par = row.Cells[2].AddParagraph(model.CphDriver);
            par.Format.Alignment = ParagraphAlignment.Center;

            row = table.AddRow();
            row.Shading.Color = Colors.LightGray;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Format.Font.Bold = true;
            par = row.Cells[2].AddParagraph($"Vehicle");
            par.Format.Alignment = ParagraphAlignment.Center;

            row = table.AddRow();
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 3;
            row.Format.SpaceAfter = 3;
            row.Format.Font.Size = 9;
            par = row.Cells[2].AddParagraph(model.CphVehicleReg);
            par.Format.Alignment = ParagraphAlignment.Center;

            section.AddLineSpacing(10);

            // middle table
            table = section.AddTable(model.Equipments);

            section.AddLineSpacing(10);

            // middle second table
            table = section.AddTable(model.Comments);

            section.AddLineSpacing(10);

            // bottom tables container
            table = section.AddTable();
            table.Rows.LeftIndent = -4;
            table.LeftPadding = 0;
            table.RightPadding = 0;

            table.AddColumn((section.Width() / 2) + 5);
            table.AddColumn((section.Width() / 2) - 5);

            var containerRow = table.AddRow();

            // bottom left table
            var leftTable = new MigraDoc.DocumentObjectModel.Tables.Table();
            var leftTableWidth = (section.Width() / 2) - 10;
            leftTable.AddColumn(63);
            var col = leftTable.AddColumn(leftTableWidth - 63);
            col.LeftPadding = 0;

            row = leftTable.AddRow();
            row.Shading.Color = Colors.LightGray;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Format.Font.Bold = true;
            row.Cells[0].MergeRight = 1;
            par = row.Cells[0].AddParagraph($"EQUIPMENT RECEIVED");
            par.Format.Alignment = ParagraphAlignment.Center;

            row = leftTable.AddRow();
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Cells[0].Borders.Right.Clear();
            row.Cells[0].Borders.Bottom.Clear();
            row.Cells[0].AddParagraph($"PRINT NAME:");
            row.Cells[1].Borders.Bottom.Clear();
            row.Cells[1].AddParagraph(model.EquipmentReceived_PrintName);

            row = leftTable.AddRow();
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Cells[0].Borders.Right.Clear();
            row.Cells[0].Borders.Bottom.Clear();
            row.Cells[0].AddParagraph($"VEHICLE REG:");
            row.Cells[1].Borders.Bottom.Clear();
            row.Cells[1].AddParagraph(model.EquipmentReceived_ClientVehicleReg);

            row = leftTable.AddRow();
            row.TopPadding = 2;
            row.BottomPadding = 2;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Cells[0].Borders.Right.Clear();
            par = row.Cells[0].AddParagraph("SIGNATURE:");

            if (model.EquipmentReceived_Signature != default(byte[]))
            {
                var base64 = "base64:" + Convert.ToBase64String(model.EquipmentReceived_Signature);
                row.Cells[1].AddImage(base64);
            }

            containerRow.Cells[0].Elements.Add(leftTable);

            // bottom right table
            var rightTable = new MigraDoc.DocumentObjectModel.Tables.Table();
            var rightTableWidth = (section.Width() / 2) - 5;
            rightTable.AddColumn(60);
            col = rightTable.AddColumn(rightTableWidth - 60);
            col.LeftPadding = 0;

            row = rightTable.AddRow();
            row.Shading.Color = Colors.LightGray;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Format.Font.Bold = true;
            row.Cells[0].MergeRight = 1;
            par = row.Cells[0].AddParagraph($"FM CONWAY HIRE REPRESENTATIVE");
            par.Format.Alignment = ParagraphAlignment.Center;

            row = rightTable.AddRow();
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Cells[0].Borders.Right.Clear();
            row.Cells[0].Borders.Bottom.Clear();
            par = row.Cells[0].AddParagraph($"PRINT NAME:");
            row.Cells[1].Borders.Bottom.Clear();
            row.Cells[1].AddParagraph(model.Representative_PrintName);

            row = rightTable.AddRow();
            row.TopPadding = 2;
            row.BottomPadding = 2;
            row.Borders.Width = 0.5;
            row.Format.SpaceBefore = 2;
            row.Format.SpaceAfter = 2;
            row.Format.Font.Size = 8;
            row.Cells[0].Borders.Right.Clear();
            par = row.Cells[0].AddParagraph("SIGNATURE:");

            if (model.Representative_Signature != default(byte[]))
            {
                var base64 = "base64:" + Convert.ToBase64String(model.Representative_Signature);
                row.Cells[1].AddImage(base64);
            }

            containerRow.Cells[1].Elements.Add(rightTable);

            // out of hours
            par = section.AddParagraph($"Out of Hours: " + (model.IsOutOfHours ? "Yes" : "No"));
            par.Format.Font.Size = 10;
            par.Format.SpaceBefore = 8;
            par.Format.SpaceAfter = 12;

            // emergency cancellation (cancelled on site)
            if (!string.IsNullOrWhiteSpace(model.ReasonCode))
            {
                par = section.AddParagraph(model.ReasonCode);
                par.Format.Font.Size = 10;
                par.Format.Font.Bold = true;
                par.Format.Font.Underline = Underline.Single;
                par.Format.Font.Color = Colors.Red;
                par.Format.SpaceAfter = 4;

                par = section.AddParagraph(model.ReasonNotes);
                par.Format.Font.Size = 10;
            }

            return document.ToByteArray();
        }

    }
}
