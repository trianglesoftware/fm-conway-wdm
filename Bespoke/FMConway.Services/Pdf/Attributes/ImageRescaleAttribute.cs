﻿using FMConway.Services.Pdf.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Attributes
{
    public class ImageRescaleAttribute : Attribute
    {
        public int MaxWidth { get; set; }
        public int MaxHeight { get; set; }

        public ImageRescaleAttribute(int maxWidth, int maxHeight)
        {
            MaxWidth = maxWidth;
            MaxHeight = maxHeight;
        }
    }
}
