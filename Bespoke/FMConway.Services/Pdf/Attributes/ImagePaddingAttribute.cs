﻿using FMConway.Services.Pdf.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Attributes
{
    public class ImagePaddingAttribute : Attribute
    {
        public int Padding { get; set; }

        public ImagePaddingAttribute(int padding)
        {
            Padding = padding;
        }
    }
}
