﻿using FMConway.Services.Pdf.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Attributes
{
    public class PdfTableColumnAttribute : Attribute
    {
        public string Name { get; set; }
        public double Width { get; set; }
        public WidthType WidthType { get; set; }
        public string Format { get; set; }
        public Alignment Alignment { get; set; }

        public PdfTableColumnAttribute(string name, double width, WidthType widthType, string format = null, Alignment alignment = Alignment.Left)
        {
            Name = name;
            Width = width;
            WidthType = widthType;
            Format = format;
            Alignment = alignment;
        }
    }
}
