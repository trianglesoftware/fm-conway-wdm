﻿using FMConway.Services.Pdf.Attributes;
using FMConway.Services.Pdf.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Models
{
    public class BriefingSheetModel
    {
        public BriefingSheetModel()
        {
            Workforces = new List<WorkforceModel>();
            Traffic = new List<TrafficModel>();
            JobTimes = new JobTimeModel();
            MaintenanceLogs = new List<MaintenanceLogModel>();
        }

        public string SRW { get; set; }
        public string Date { get; set; }
        public string Weather { get; set; }
        public string RoadSpeed { get; set; }
        public string Postcodes { get; set; }
        public string Location { get; set; }
        public string Task { get; set; }
        public string RAMS { get; set; }
        public string CustomerNumber { get; set; }
        public string VRN1 { get; set; }
        public string Driver1 { get; set; }
        public string VRN2 { get; set; }
        public string Driver2 { get; set; }
        public string Details { get; set; }

        public List<WorkforceModel> Workforces { get; set; }
        public List<TrafficModel> Traffic { get; set; }
        public JobTimeModel JobTimes { get; set; }
        public List<MaintenanceLogModel> MaintenanceLogs { get; set; }
    }

    public class WorkforceModel
    {
        [PdfTableColumn("Name", 25, WidthType.Percent)]
        public string Name { get; set; }
        [PdfTableColumn("Qualification", 17, WidthType.Percent)]
        public string Qualification { get; set; }
        [PdfTableColumn("Company", 17, WidthType.Percent)]
        public string Company { get; set; }
        [ImageCropWhitespace]
        [ImageRescale(165, 30)]
        [ImagePadding(2)]
        [PdfTableColumn("Signature", 26, WidthType.Percent)]
        public byte[] Signature { get; set; }
        [PdfTableColumn("Date", 15, WidthType.Percent)]
        public string Date { get; set; }
    }

    public class TrafficModel
    {
        [PdfTableColumn("Location", 40, WidthType.Percent)]
        public string Location { get; set; }
        [PdfTableColumn("Time", 30, WidthType.Percent)]
        public string Time { get; set; }
        [PdfTableColumn("Total", 30, WidthType.Percent)]
        public string Total { get; set; }
    }

    public class JobTimeModel
    {
        [PdfTableColumn("Start Shift", 19, WidthType.Percent)]
        public string StartShift { get; set; }
        [PdfTableColumn("On Site", 19, WidthType.Percent)]
        public string OnSite { get; set; }
        [PdfTableColumn("First Cone", 19, WidthType.Percent)]
        public string FirstCone { get; set; }
        [PdfTableColumn("Installed", 19, WidthType.Percent)]
        public string Installed { get; set; }
        [PdfTableColumn("End Shift / Leave Yard", 24, WidthType.Percent)]
        public string EndShift { get; set; }
    }

    public class MaintenanceLogModel
    {
        [PdfTableColumn("Maintenance Log / Action Taken", 40, WidthType.Percent)]
        public string Question { get; set; }
        [PdfTableColumn("Answer", 15, WidthType.Percent)]
        public string Answer { get; set; }
        [PdfTableColumn("Details", 30, WidthType.Percent)]
        public string Details { get; set; }
        [PdfTableColumn("Time", 15, WidthType.Percent)]
        public string Time { get; set; }
    }
}
