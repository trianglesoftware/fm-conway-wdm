﻿using FMConway.Services.Pdf.Attributes;
using FMConway.Services.Pdf.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Models
{
    public class OnHireNoteModel
    {
        public OnHireNoteModel()
        {
            Equipments = new List<EquipmentModel>();
            Comments = new CommentsModel();
        }

        public int JobNumber { get; set; }
        public string JobTitle { get; set; }
        public string Customer { get; set; }
        public string ContractNumber { get; set; }
        public DateTime OnHireDate { get; set; }
        public string DeliveryAddress { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string CphDriver { get; set; }
        public string CphVehicleReg { get; set; }
        public bool IsOutOfHours { get; set; }

        public bool IsEmergencyCancellation { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonNotes { get; set; }


        // customer
        public string EquipmentReceived_PrintName { get; set; }
        public byte[] EquipmentReceived_Signature { get; set; }
        public string EquipmentReceived_ClientVehicleReg { get; set; }

        // staff
        public string Representative_PrintName { get; set; }
        public byte[] Representative_Signature { get; set; }

        public List<EquipmentModel> Equipments { get; set; }
        public CommentsModel Comments { get; set; }
    }

    public class EquipmentModel
    {
        [PdfTableColumn("QUANTITY", 12, WidthType.Percent)]
        public int Quantity { get; set; }
        [PdfTableColumn("ASSET NUMBER", 23, WidthType.Percent)]
        public string AssetNumber { get; set; }
        [PdfTableColumn("ITEM DESCRIPTION", 65, WidthType.Percent)]
        public string ItemDescription { get; set; }
    }

    public class CommentsModel
    {
        [PdfTableColumn("DAMAGES / MISSING ITEMS / COMMENTS", 100, WidthType.Percent)]
        public string Comment { get; set; }
    }
}
