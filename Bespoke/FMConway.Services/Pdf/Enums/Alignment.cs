﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Pdf.Enums
{
    public enum Alignment
    {
        Left,
        Center,
        Right
    }
}
