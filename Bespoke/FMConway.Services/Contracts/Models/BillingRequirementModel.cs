﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Models
{
    public class BillingRequirementModel
    {
        public Guid ID { get; set; }
        public string BillingRequirement { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<BillingRequirement, BillingRequirementModel>> EntityToModel = entity => new BillingRequirementModel()
        {
            ID = entity.BillingRequirementPK,
            BillingRequirement = entity.BillingRequirement1,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<BillingRequirementModel, BillingRequirement> ModelToEntity = model => new BillingRequirement()
        {
            BillingRequirementPK = model.ID,
            BillingRequirement1 = model.BillingRequirement,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
