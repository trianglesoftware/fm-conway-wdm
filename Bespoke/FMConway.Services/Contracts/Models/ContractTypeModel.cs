﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Models
{
    public class ContractTypeModel
    {
        public Guid ContractTypeID { get; set; }
        public string ContractTypeDescription { get; set; }

        public static Expression<Func<ContractType, ContractTypeModel>> EntityToModel = entity => new ContractTypeModel()
        {
            ContractTypeID = entity.ContractTypePK,
            ContractTypeDescription = entity.ContractTypeDescription,
        };

        public static Func<ContractTypeModel, ContractType> ModelToEntity = model => new ContractType()
        {
            ContractTypePK = model.ContractTypeID,
            ContractTypeDescription = model.ContractTypeDescription,
        };

    }
}
