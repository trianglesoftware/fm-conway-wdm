﻿using FMConway.Services.Checklists.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Models
{
    public class ContractChecklistModel
    {
        public Guid ContractChecklistID { get; set; }
        public Guid ContractID { get; set; }
        public Guid ChecklistID { get; set; }
        public ChecklistModel Checklist { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<ContractTenderChecklist, ContractChecklistModel>> EntityToModel = entity => new ContractChecklistModel
        {
            ContractChecklistID = entity.ContractTenderChecklistPK,
            ContractID = entity.ContractFK,
            ChecklistID = entity.ChecklistFK,
            Checklist = new ChecklistModel()
            {
                ChecklistName = entity.Checklist.ChecklistName
            },

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<ContractChecklistModel, ContractTenderChecklist> ModelToEntity = model => new ContractTenderChecklist()
        {
            ContractTenderChecklistPK = model.ContractChecklistID,
            ContractFK = model.ContractID,
            ChecklistFK = model.ChecklistID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
