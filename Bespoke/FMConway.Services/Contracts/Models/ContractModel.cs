﻿using FMConway.Services.Customers.Models;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.Contracts.Models
{
    public class ContractModel
    {
        public Guid ID { get; set; }
        [Required(ErrorMessage = "Sub Contract Code is required")]
        public string ContractNumber { get; set; }
        [Required(ErrorMessage = "Contract Title is required")]
        public string ContractTitle { get; set; }
        [DisplayName("Customer")]
        [Required(ErrorMessage = "The Customer is required")]
        public Guid CustomerID { get; set; }
        [DisplayName("Contract Type")]
        [Required(ErrorMessage = "The Contract Type is required")]
        public Guid ContractTypeID { get; set; }
        public ContractTypeModel Type { get; set; }
        public CustomerModel Customer { get; set; }
        [Required(ErrorMessage = "The Depot is required")]
        public Guid DepotID { get; set; }
        public string DepotLookup { get; set; }
        [Required(ErrorMessage = "Date Awarded is required")]
        public DateTime? DateAwarded { get; set; }
        [Required(ErrorMessage = "Date Expires is required")]
        public DateTime? DateExpires { get; set; }
        [DisplayName("Status")]
        [Required(ErrorMessage = "Status is required")]
        public Guid StatusID { get; set; }
        public ContractStatusModel Status { get; set; }
        public string TenderNumber { get; set; }
        [DisplayName("Contract Manager")]
        [Required(ErrorMessage = "The Contract Manager is required")]
        public Guid EmployeeID { get; set; }
        public EmployeeModel Employee { get; set; }
        [DisplayName("Billing Requirement")]
        [Required(ErrorMessage = "Billing Requirement is required")]
        public Guid BillingRequirementID { get; set; }
        public BillingRequirementModel BillingRequirement { get; set; }
        public string SubmitDate { get; set; }
        public string QuantitySurveyor { get; set; }
        public bool CreateScheme { get; set; }
        public bool IsCleansingLogMandatory { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Contract, ContractModel>> EntityToModel = entity => new ContractModel
        {
            ID = entity.ContractPK,
            ContractNumber = entity.ContractNumber,
            ContractTitle = entity.ContractTitle,
            ContractTypeID = entity.ContractTypeFK,
            Type = new ContractTypeModel(){
                ContractTypeDescription = entity.ContractType.ContractTypeDescription     
            },
            CustomerID = entity.CustomerFK,
            Customer = new CustomerModel(){
                CustomerName = entity.Customer.CustomerName
            },
            DepotID = entity.DepotFK,
            DepotLookup = entity.Depot.DepotName,
            DateAwarded = entity.DateAwarded,
            DateExpires = entity.DateExpires,
            StatusID = entity.StatusFK,
            Status = new ContractStatusModel(){
                Status = entity.Status.Status1
            },
            TenderNumber = entity.TenderNumber,
            EmployeeID = entity.EmployeeFK,
            Employee = new EmployeeModel()
            {
                EmployeeName = entity.Employee.EmployeeName
            },
            BillingRequirementID = entity.BillingRequirementFK,
            BillingRequirement = new BillingRequirementModel()
            {
                BillingRequirement = entity.BillingRequirement.BillingRequirement1
            },
            SubmitDate = entity.DateToBeSubmitted,
            QuantitySurveyor = entity.QuantitySurveyor,
            IsCleansingLogMandatory = entity.IsCleansingLogMandatory,
            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<ContractModel, Contract> ModelToEntity = model => new Contract
        {
            ContractPK = model.ID,
            ContractNumber = model.ContractNumber == null ? "" : model.ContractNumber,
            ContractTypeFK = model.ContractTypeID,
            ContractTitle = model.ContractTitle == null ? "" : model.ContractTitle,
            CustomerFK = model.CustomerID,
            DepotFK = model.DepotID,
            DateAwarded = (DateTime)model.DateAwarded,
            DateExpires = (DateTime)model.DateExpires,
            StatusFK = model.StatusID,
            TenderNumber = model.TenderNumber == null ? "" : model.TenderNumber,
            EmployeeFK = model.EmployeeID,
            BillingRequirementFK = model.BillingRequirementID,
            DateToBeSubmitted = model.SubmitDate == null ? "" : model.SubmitDate,
            QuantitySurveyor = model.QuantitySurveyor == null ? "" : model.QuantitySurveyor,
            IsCleansingLogMandatory = model.IsCleansingLogMandatory,
            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
