﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Models
{
    public class DocumentModel
    {
        public Guid DocumentID { get; set; }
        public Guid ContractID { get; set; }
        public string DocumentNumber { get; set; }
        public Guid FileID { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<ContractCostBuildUp, DocumentModel>> EntityToModel = entity => new DocumentModel()
        {
            DocumentID = entity.ContractCostBuildUpPK,
            DocumentNumber = entity.DocumentNumber,
            ContractID = entity.ContractFK,
            FileID = entity.FileFK,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<DocumentModel, ContractCostBuildUp> ModelToEntity = model => new ContractCostBuildUp()
        {
            ContractCostBuildUpPK = model.DocumentID,
            DocumentNumber = model.DocumentNumber,
            ContractFK = model.ContractID,
            FileFK = model.FileID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
