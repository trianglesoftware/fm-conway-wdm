﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Models
{
    public class ContractStatusModel
    {
        public Guid StatusID { get; set; }
        public string Status { get; set; }
        public bool IsContractStatus { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Status, ContractStatusModel>> EntityToModel = entity => new ContractStatusModel()
        {
            StatusID = entity.StatusPK,
            Status = entity.Status1,
            IsContractStatus = entity.IsContractStatus,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<ContractStatusModel, Status> ModelToEntity = model => new Status()
        {
            StatusPK = model.StatusID,
            Status1 = model.Status,
            IsContractStatus = model.IsContractStatus,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
