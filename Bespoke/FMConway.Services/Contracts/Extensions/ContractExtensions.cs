﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Extensions
{
    public static class ContractExtensions
    {
        public static IQueryable<Contract> ActiveContracts(this IQueryable<Contract> model)
        {
            return model.Where(a => a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Contract> InactiveContracts(this IQueryable<Contract> model)
        {
            return model.Where(a => !a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Contract> ActiveContractsWithNoSchemes(this IQueryable<Contract> model)
        {
            return model.ActiveContracts().Where(w => !w.Schemes.Any());
        }

        public static IQueryable<Contract> ContractOverviewQuery(this IQueryable<Contract> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ContractNumber.Contains(query) ||
                                          bq.ContractTitle.Contains(query) ||
                                          bq.Customer.CustomerName.Contains(query));
            }

            return model;
        }

        public static IQueryable<Contract> ContractQuery(this IQueryable<Contract> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ContractNumber.Contains(query) ||
                                          bq.ContractTitle.Contains(query));
            }

            return model;
        }
    }
}
