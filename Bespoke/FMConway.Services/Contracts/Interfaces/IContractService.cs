﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Interfaces
{
    public interface IContractService : IService
    {
        IContractStatusService Status { get; set; }
        IContractTypeService ContractType { get; set; }
        IBillingRequirementService BillingRequirements { get; set; }
        IContractDocumentService Documents { get; set; }

        Guid Create(ContractModel model/*, Dictionary<Guid,string> tenderChecklists*/);
        void Update(ContractModel model/*, Dictionary<Guid, string> tenderChecklists, Dictionary<Guid, string> tenderChecklistModel*/);
        ContractModel Single(Guid ID);

        List<ContractModel> GetAllActiveContracts();
        PagedResults<ContractModel> GetActiveContractsPaged(int rowCount, int page, string query);

        List<ContractModel> GetAllInactiveContracts();
        PagedResults<ContractModel> GetInactiveContractsPaged(int rowCount, int page, string query);
        PagedResults<ContractLookup> GetPagedActiveContractsLookup(int rowCount, int page, string query);
        PagedResults<ContractSchemeLookup> GetPagedActiveContractSchemesLookup(int rowCount, int page, string query, Guid? customerID);
    }
}
