﻿using FMConway.Services.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Interfaces
{
    public interface IContractDocumentService
    {
        void Create(Guid contractId, string documentNumber, string fileName, string contentType, byte[] documentData);
        //void Create(Guid jobID, FileModel fileModel, Guid securityLevelId, string documentNumber);

        void Update(DocumentModel document);
        void Inactivate(Guid id);
        DocumentModel GetDocument(Guid id);
        List<DocumentModel> GetActiveDocumentsForContract(Guid jobID);
    }
}
