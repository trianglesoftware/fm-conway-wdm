﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Interfaces
{
    public interface IBillingRequirementService : IService
    {
        List<BillingRequirementModel> GetBillingRequirements();
    }
}
