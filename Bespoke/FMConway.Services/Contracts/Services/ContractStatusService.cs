﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.Contracts.Services
{
    public class ContractStatusService : Service, FMConway.Services.Contracts.Interfaces.IContractStatusService
    {
        UserContext _userContext;

        public ContractStatusService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<ContractStatusModel> GetContractStatus()
        {
            return Context.Status.Where(x => x.IsContractStatus).OrderBy(x => x.Status1).Select(ContractStatusModel.EntityToModel).ToList();
        }
    }
}
