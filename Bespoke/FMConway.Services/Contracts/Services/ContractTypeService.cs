﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.Contracts.Services
{
    public class ContractTypeService : Service, FMConway.Services.Contracts.Interfaces.IContractTypeService
    {
        UserContext _userContext;

        public ContractTypeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<ContractTypeModel> GetContractType()
        {
            return Context.ContractTypes.OrderBy(x => x.ContractTypeDescription).Select(ContractTypeModel.EntityToModel).ToList();
        }
    }
}
