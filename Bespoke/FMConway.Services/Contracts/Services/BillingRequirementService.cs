﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.Contracts.Services
{
    public class BillingRequirementService : Service, FMConway.Services.Contracts.Interfaces.IBillingRequirementService
    {
        UserContext _userContext;

        public BillingRequirementService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<BillingRequirementModel> GetBillingRequirements()
        {
            return Context.BillingRequirements.OrderBy(x => x.BillingRequirement1).Select(BillingRequirementModel.EntityToModel).ToList();
        }
    }
}
