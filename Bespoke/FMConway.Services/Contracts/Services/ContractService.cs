﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Contracts.Extensions;
using FMConway.Services.Contracts.Interfaces;
using FMConway.Services.Schemes.Interfaces;
using FMConway.Services.Schemes.Extensions;

namespace FMConway.Services.Contracts.Services
{
    public class ContractService : Service, FMConway.Services.Contracts.Interfaces.IContractService
    {
        UserContext _userContext;

        public IContractStatusService Status { get; set; }
        public IContractTypeService ContractType { get; set; }
        public IBillingRequirementService BillingRequirements { get; set; }
        public IContractDocumentService Documents { get; set; }
        public ISchemeService Schemes { get; set; }

        public ContractService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(ContractModel model/*, Dictionary<Guid, string> tenderChecklists*/)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.ID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Contracts.Add(ContractModel.ModelToEntity(model));

            //foreach (var checklist in tenderChecklists)
            //{
            //    ContractChecklistModel tenderChecklist = new ContractChecklistModel();
            //    tenderChecklist.ContractChecklistID = Guid.NewGuid();
            //    tenderChecklist.ContractID = model.ID;
            //    tenderChecklist.ChecklistID = checklist.Key;
            //    tenderChecklist.IsActive = true;
            //    tenderChecklist.CreatedByID = _userContext.UserDetails.ID;
            //    tenderChecklist.CreatedDate = DateTime.Now;
            //    tenderChecklist.UpdatedByID = _userContext.UserDetails.ID;
            //    tenderChecklist.UpdatedDate = DateTime.Now;

            //    Context.ContractTenderChecklists.Add(ContractChecklistModel.ModelToEntity(tenderChecklist));
            //}

            SaveChanges();

            if (model.CreateScheme)
            {
                Schemes.Create(model);
            }

            return model.ID;
        }

        public void Update(ContractModel model/*, Dictionary<Guid, string> tenderChecklistModel, Dictionary<Guid, string> tenderChecklists*/)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Contract current = Context.Contracts.Where(x => x.ContractPK.Equals(model.ID)).Single();

            current.ContractNumber = model.ContractNumber == null ? "" : model.ContractNumber;
            current.ContractTitle = model.ContractTitle == null ? "" : model.ContractTitle;
            current.ContractTypeFK = model.ContractTypeID;
            current.CustomerFK = model.CustomerID;
            current.DepotFK = model.DepotID;
            current.DateAwarded = (DateTime)model.DateAwarded;
            current.DateExpires = (DateTime)model.DateExpires;
            current.StatusFK = model.StatusID;
            current.TenderNumber = model.TenderNumber == null ? "" : model.TenderNumber;
            current.EmployeeFK = model.EmployeeID;
            current.QuantitySurveyor = model.QuantitySurveyor == null ? "" : model.QuantitySurveyor;
            current.DateToBeSubmitted = model.SubmitDate == null ? "" : model.SubmitDate;
            current.BillingRequirementFK = model.BillingRequirementID;
            current.IsCleansingLogMandatory = model.IsCleansingLogMandatory;
            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            /*if (tenderChecklists != null)
            {
                if (tenderChecklistModel != null)
                {
                    if (!tenderChecklists.SequenceEqual(tenderChecklistModel))
                    {
                        var checklists = Context.ContractTenderChecklists.Where(x => x.ContractFK.Equals(model.ID));
                        foreach (var checklist in checklists)
                        {
                            Context.ContractTenderChecklists.Remove(checklist);
                        }

                        foreach (var checklist in tenderChecklists)
                        {
                            ContractChecklistModel tenderChecklist = new ContractChecklistModel();
                            tenderChecklist.ContractChecklistID = Guid.NewGuid();
                            tenderChecklist.ContractID = model.ID;
                            tenderChecklist.ChecklistID = checklist.Key;
                            tenderChecklist.IsActive = true;
                            tenderChecklist.CreatedByID = _userContext.UserDetails.ID;
                            tenderChecklist.CreatedDate = DateTime.Now;
                            tenderChecklist.UpdatedByID = _userContext.UserDetails.ID;
                            tenderChecklist.UpdatedDate = DateTime.Now;

                            Context.ContractTenderChecklists.Add(ContractChecklistModel.ModelToEntity(tenderChecklist));
                        }
                    }
                }
                else 
                {
                    var checklists = Context.ContractTenderChecklists.Where(x => x.ContractFK.Equals(model.ID));
                    foreach (var checklist in checklists)
	                {
                        Context.ContractTenderChecklists.Remove(checklist);
	                }
                }
            }*/

            SaveChanges();
        }

        public ContractModel Single(Guid ID)
        {
            return Context.Contracts.Where(a => a.ContractPK.Equals(ID)).Select(ContractModel.EntityToModel).SingleOrDefault();
        }

        public List<ContractModel> GetAllActiveContracts()
        {
            return Context.Contracts.ActiveContracts().Select(ContractModel.EntityToModel).OrderBy(o => o.ContractTitle).ToList();
        }

        public PagedResults<ContractModel> GetActiveContractsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Contracts.ActiveContracts().OrderBy(o => o.ContractNumber).ContractOverviewQuery(query).Select(ContractModel.EntityToModel);

            return PagedResults<ContractModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<ContractModel> GetAllInactiveContracts()
        {
            return Context.Contracts.InactiveContracts().Select(ContractModel.EntityToModel).ToList();
        }

        public PagedResults<ContractModel> GetInactiveContractsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Contracts.InactiveContracts().OrderBy(o => o.ContractNumber).ContractOverviewQuery(query).Select(ContractModel.EntityToModel);

            return PagedResults<ContractModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ContractLookup> GetPagedActiveContractsLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Contracts.ActiveContracts().OrderBy(o => o.ContractNumber).ContractQuery(query).Select(ContractLookup.EntityToModel);

            return PagedResults<ContractLookup>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ContractSchemeLookup> GetPagedActiveContractSchemesLookup(int rowCount, int page, string query, Guid? customerID)
        {
            var withSchemes = Context.Schemes
                .Where(a => customerID == null || a.Contract.CustomerFK == customerID)
                .Where(w => w.IsActive && w.Contract.IsActive)
                .SchemeOverviewQuery(query)
                .Select(ContractSchemeLookup.EntityToModel);

            var withoutSchemes = Context.Contracts
                .Where(a => customerID == null || a.CustomerFK == customerID)
                .ActiveContractsWithNoSchemes()
                .ContractQuery(query)
                .Select(ContractSchemeLookup.NoSchemeETM);

            var baseQuery = withSchemes.Union(withoutSchemes).OrderBy(o => o.Contract);

            return PagedResults<ContractSchemeLookup>.GetPagedResults(baseQuery, rowCount, page);
        }
    }
}
