﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Files.Interface;
using FMConway.Services.Files.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.Contracts.Services
{
    public class ContractDocumentService : Service, FMConway.Services.Contracts.Interfaces.IContractDocumentService
    {
        UserContext _userContext;
        IFileService _fileService;

        public ContractDocumentService(UserContext userContext, IFileService fileService)
        {
            _userContext = userContext;
            _fileService = fileService;
        }

        public List<DocumentModel> GetActiveDocumentsForContract(Guid contractID)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            List<DocumentModel> documents = Context.ContractCostBuildUps.Where(jd => jd.ContractFK.Equals(contractID) && jd.IsActive).Select(DocumentModel.EntityToModel).ToList();

            return documents;
        }

        public DocumentModel GetDocument(Guid id)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.ContractCostBuildUps.Where(jd => jd.ContractCostBuildUpPK.Equals(id)).Select(DocumentModel.EntityToModel).SingleOrDefault();

            return document;
        }

        public void Create(Guid contractID, string documentNumber, string fileName, string contentType, byte[] documentData)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var file = new FileModel()
            {
                FileDownloadName = fileName,
                ContentType = contentType,
                Data = documentData
            };

            FileModel newFile = _fileService.Add(file);

            DocumentModel document = new DocumentModel()
            {
                DocumentID = Guid.NewGuid(),
                ContractID = contractID,
                FileID = newFile.ID,
                DocumentNumber = documentNumber,
                IsActive = true,
                CreatedByID = _userContext.UserDetails.ID,
                CreatedDate = DateTime.Now,
                UpdatedByID = _userContext.UserDetails.ID,
                UpdatedDate = DateTime.Now
            };

            Context.ContractCostBuildUps.Add(DocumentModel.ModelToEntity(document));

            SaveChanges();
        }

        //public void Create(Guid jobId, FileModel fileModel, Guid securityLevelId, string documentNumber)
        //{
        //    DocumentModel document = new DocumentModel()
        //    {
        //        DocumentID = Guid.NewGuid(),
        //        JobID = jobId,
        //        FileID = fileModel.ID,
        //        DocumentNumber = documentNumber,
        //        SecurityLevelID = securityLevelId,
        //        IsActive = true,
        //        CreatedByID = _userContext.UserDetails.ID,
        //        CreatedDate = DateTime.Now,
        //        UpdatedByID = _userContext.UserDetails.ID,
        //        UpdatedDate = DateTime.Now
        //    };

        //    _documents.Create(DocumentModel.ModelToEntity(document));
        //}

        public void Update(DocumentModel document)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            ContractCostBuildUp current = Context.ContractCostBuildUps.Where(x => x.ContractCostBuildUpPK.Equals(document.DocumentID)).SingleOrDefault();

            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public void Inactivate(Guid id)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.ContractCostBuildUps.Where(jd => jd.ContractCostBuildUpPK.Equals(id)).Select(DocumentModel.EntityToModel).SingleOrDefault();
            if (document != null)
            {
                document.IsActive = false;
                Update(document);
            }
        }
    }
}
