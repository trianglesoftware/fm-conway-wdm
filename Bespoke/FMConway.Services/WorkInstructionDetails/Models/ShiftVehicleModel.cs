﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class ShiftVehicleModel
    {
        public Guid ShiftVehicleID { get; set; }
        public Guid ShiftID { get; set; }
        public string VehicleMake { get; set; }
        public string Registration { get; set; }
        public string Driver { get; set; }
        public int StartMileage { get; set; }
        public int EndMileage { get; set; }
        public bool NotOnShift { get; set; }
        public string ReasonNotOnShift { get; set; }

        public static Expression<Func<ShiftVehicle, ShiftVehicleModel>> ETM = e => new ShiftVehicleModel() 
        {
            ShiftVehicleID = e.ShiftVehiclePK,
            ShiftID = e.ShiftFK,
            VehicleMake = e.Vehicle.Make + " " + e.Vehicle.Model,
            Registration = e.Vehicle.Registration,
            Driver = e.Employee.EmployeeName,
            StartMileage = e.StartMileage,
            EndMileage = e.EndMileage,
            NotOnShift = e.NotOnShift,
            ReasonNotOnShift = e.ReasonNotOnShift ?? ""
        };
    }
}
