﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class MaintenanceDetailModel
    {
        public Guid MaintenanceCheckDetailID { get; set; }
        public Guid MaintenanceCheckAnswerID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public string Description { get; set; }
        public string MaintenanceQuestion { get; set; }

        public static Expression<Func<MaintenanceCheckDetail, MaintenanceDetailModel>> ETM = e => new MaintenanceDetailModel() 
        {
            MaintenanceCheckDetailID = e.MaintenanceCheckDetailPK,
            MaintenanceCheckAnswerID = e.MaintenanceCheckAnswerFK,
            WorkInstructionID = e.MaintenanceCheckAnswer.MaintenanceCheck.WorksInstructionFK,
            Description = e.Description,
            MaintenanceQuestion = e.MaintenanceCheckAnswer.ChecklistQuestion.ChecklistQuestion1
        };
    }

    public class MaintenanceDetailPhotoModel
    {
        public Guid MaintenanceCheckDetailID { get; set; }
        public Guid MaintenanceCheckDetailPhotoID { get; set; }
        public Guid FileID { get; set; }
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }

        public static Expression<Func<MaintenanceCheckDetailPhoto, MaintenanceDetailPhotoModel>> ETM = e => new MaintenanceDetailPhotoModel()
        {
            MaintenanceCheckDetailID = e.MaintenanceCheckDetailFK,
            MaintenanceCheckDetailPhotoID = e.MaintenanceCheckDetailPhotoPK,
            FileID = e.FileFK,
            Date = e.CreatedDate,
            FileName = e.File.FileDownloadName,
            Longitude = e.File.Longitude ?? 0.00m,
            Latitude =  e.File.Latitude ?? 0.00m
        };
    }
}
