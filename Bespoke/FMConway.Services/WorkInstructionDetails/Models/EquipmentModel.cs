﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class EquipmentModel
    {
        public Guid EquipmentID { get; set; }
        public string Equipment { get; set; }
        public string Type { get; set; }
        public int? QuantityInstalled { get; set; }
        public int? QuantityCollected { get; set; }
        public string VmsOrAsset { get; set; }
        public Guid? TaskEquipmentID { get; set; }
        public bool hasEquipmentInstallTask { get; set; }
        public bool hasEquipmentCollectionTask { get; set; }


        public static Expression<Func<TaskEquipment, EquipmentModel>> ETM = e => new EquipmentModel
        {
            EquipmentID = e.TaskEquipmentPK,
            Equipment = e.Equipment.EquipmentName,
            QuantityInstalled = e.Quantity,
            VmsOrAsset = e.Equipment.EquipmentType.EquipmentTypeVmsOrAsset.Name
        };
    }
}
