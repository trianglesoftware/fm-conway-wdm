﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class ChecklistAnswerModel
    {
        public Guid? WorkInstructionID { get; set; }
        public Guid? TaskID { get; set; }
        public Guid? ChecklistID { get; set; }
        public Guid? ChecklistAnswerID { get; set; }
        public Guid? ChecklistDetailID { get; set; }
        public string ChecklistQuestion { get; set; }
        public DateTime ChecklistTime { get; set; }
        public int? ChecklistAnswer { get; set; }
        public int QuestionOrder { get; set; }
        public string Reason { get; set; }

        public static Expression<Func<vv_MaintenanceChecks, ChecklistAnswerModel>> ETM = e => new ChecklistAnswerModel() 
        {
            WorkInstructionID = e.WorkInstructionPK,
            ChecklistID = e.MaintenanceCheckPK,
            ChecklistAnswerID = e.MaintenanceCheckAnswerPK,
            ChecklistDetailID = e.MaintenanceCheckDetailPK,
            ChecklistQuestion = e.ChecklistQuestion,
            ChecklistTime = e.Time,
            QuestionOrder = e.QuestionOrder,
            ChecklistAnswer = e.Answer ? 1 : 0,
            Reason = null
        };

        public static Expression<Func<vv_TaskChecklists, ChecklistAnswerModel>> TaskETM = e => new ChecklistAnswerModel()
        {
            TaskID = e.TaskPK,
            ChecklistID = e.TaskChecklistPK,
            ChecklistAnswerID = e.TaskChecklistAnswerPK,
            ChecklistQuestion = e.ChecklistQuestion,
            QuestionOrder = e.QuestionOrder,
            ChecklistAnswer = e.Answer,
            Reason = e.Reason,
            ChecklistTime = e.UpdatedDate ?? (DateTime)SqlDateTime.MinValue
        };
    }
}
