﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class AssetModel
    {
        public Guid TaskEquipmentAssetID { get; set; }
        public Guid EquipmentID { get; set; }
        public int ItemNumber { get; set; }
        public string AssetNumber { get; set; }

        public static Expression<Func<TaskEquipmentAsset, AssetModel>> ETM = a => new AssetModel
        {
            TaskEquipmentAssetID = a.TaskEquipmentAssetPK,
            EquipmentID = a.TaskEquipmentFK,
            ItemNumber = a.ItemNumber,
            AssetNumber = a.AssetNumber
        };
    }
}
