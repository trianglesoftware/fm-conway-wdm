﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class LoadingSheetSignatureModel
    {
        public Guid LoadingSheetSignatureID { get; set; }
        public Guid LoadingSheetID { get; set; }
        public int Number { get; set; }
        public Guid FileID { get; set; }

        public static Expression<Func<LoadingSheetSignature, LoadingSheetSignatureModel>> ETM = e => new LoadingSheetSignatureModel()
        {
            LoadingSheetSignatureID = e.LoadingSheetSignaturePK,
            LoadingSheetID = e.LoadingSheetFK,
            Number = e.LoadingSheet.LoadingSheetNumber,
            FileID = e.FileFK
        };
    }
}
