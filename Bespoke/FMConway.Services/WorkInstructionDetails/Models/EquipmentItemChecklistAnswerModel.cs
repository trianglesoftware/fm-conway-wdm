﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class EquipmentItemChecklistAnswerModel
    {
        public Guid EquipmentItemChecklistAnswerID { get; set; }
        public Guid ChecklistQuestionID { get; set; }
        public string Question { get; set; }
        public bool Answer { get; set; }
        public string Reason { get; set; }

        public static Expression<Func<EquipmentItemChecklistAnswer, EquipmentItemChecklistAnswerModel>> EntityToModel = a => new EquipmentItemChecklistAnswerModel
        {
            EquipmentItemChecklistAnswerID = a.EquipmentItemChecklistAnswerPK,
            ChecklistQuestionID = a.ChecklistQuestionFK,
            Question = a.ChecklistQuestion.ChecklistQuestion1,
            Answer = a.Answer,
            Reason = a.Reason
        };
    }
}
