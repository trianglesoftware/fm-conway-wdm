﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class LoadingSheetEquipmentItemModel
    {
        public Guid LoadingSheetEquipmentItemID { get; set; }
        public Guid EquipmentitemChecklistPK { get; set; }
        public int LoadingSheet { get; set; }
        public string Equipment { get; set; }
        public string VmsOrAsset { get; set; }
        public int ItemNumber { get; set; }

        public static Expression<Func<LoadingSheetEquipmentItem, LoadingSheetEquipmentItemModel>> EntityToModel = a => new LoadingSheetEquipmentItemModel
        {
            LoadingSheetEquipmentItemID = a.LoadingSheetEquipmentItemPK,
            EquipmentitemChecklistPK = a.EquipmentItemChecklists.Select(b => b.EquipmentItemChecklistPK).FirstOrDefault(),
            LoadingSheet = a.LoadingSheetEquipment.LoadingSheet.LoadingSheetNumber,
            Equipment = a.LoadingSheetEquipment.Equipment.EquipmentName,
            VmsOrAsset = a.LoadingSheetEquipment.Equipment.EquipmentType.EquipmentTypeVmsOrAsset.Name,
            ItemNumber = a.ItemNumber,
        };
    }
}
