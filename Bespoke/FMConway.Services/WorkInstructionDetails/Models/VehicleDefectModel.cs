﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class VehicleDefectModel
    {
        public Guid VehicleChecklistDefectID { get; set; }
        public Guid VehicleChecklistAnswerID { get; set; }
        public string VehicleChecklistQuestion { get; set; }
        public string Description { get; set; }

        public static Expression<Func<VehicleChecklistDefect, VehicleDefectModel>> ETM = e => new VehicleDefectModel() 
        {
            VehicleChecklistDefectID = e.VehicleChecklistDefectPK,
            VehicleChecklistAnswerID = e.VehicleChecklistAnswerFK,
            VehicleChecklistQuestion = e.VehicleChecklistAnswer.ChecklistQuestion.ChecklistQuestion1,
            Description = e.Description
        };        
    }

    public class VehicleDefectPhotoModel
    {
        public Guid VehicleChecklistDefectID { get; set; }
        public Guid VehicleChecklistDefectPhotoID { get; set; }
        public Guid FileID { get; set; }
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }

        public static Expression<Func<VehicleChecklistDefectPhoto, VehicleDefectPhotoModel>> ETM = e => new VehicleDefectPhotoModel()
        {
            VehicleChecklistDefectID = e.VehicleChecklistDefectFK,
            VehicleChecklistDefectPhotoID = e.VehicleChecklistDefectPhotoPK,
            FileID = e.FileFK,
            Date = e.CreatedDate,
            FileName = e.File.FileDownloadName,
            Latitude = e.File.Latitude ?? 0.00m,
            Longitude = e.File.Longitude ?? 0.00m
        };
    }
}
