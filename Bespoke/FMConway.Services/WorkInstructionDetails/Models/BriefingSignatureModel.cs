﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class BriefingSignatureModel
    {
        public Guid BriefingSignatureID { get; set; }
        public Guid BriefingID { get; set; }
        public string BriefingName { get; set; }
        public string EmployeeName { get; set; }
        public DateTime BriefingStart { get; set; }
        public DateTime BriefingEnd { get; set; }
        public Guid FileID { get; set; }

        public static Expression<Func<BriefingSignature, BriefingSignatureModel>> ETM = e => new BriefingSignatureModel() 
        {
            BriefingSignatureID = e.BriefingSignaturePK,
            BriefingID = e.BriefingFK,
            BriefingName = e.Briefing.Name,
            EmployeeName = e.Employee.EmployeeName,
            BriefingStart = e.BriefingStart,
            BriefingEnd = e.BriefingEnd,
            FileID = e.FileFK
        };

    }
}
