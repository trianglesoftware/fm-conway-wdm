﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class ShiftStaffModel
    {
        public Guid ShiftStaffID { get; set; }
        public Guid ShiftID { get; set; }
        public Guid EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string AgencyEmployeeName { get; set; }
        public bool IsAgency { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool NotOnShift { get; set; }
        public string ReasonNotOnShift { get; set; }

        public static Expression<Func<ShiftStaff, ShiftStaffModel>> ETM = e => new ShiftStaffModel() 
        {
            ShiftStaffID = e.ShiftStaffPK,
            ShiftID = e.ShiftFK,
            EmployeeID = e.EmployeeFK,
            AgencyEmployeeName = e.AgencyEmployeeName,
            IsAgency = e.Employee.AgencyStaff,
            StartTime = e.StartTime,
            EndTime = e.EndTime,
            EmployeeName = e.Employee.EmployeeName,
            NotOnShift = e.NotOnShift,
            ReasonNotOnShift = e.ReasonNotOn ?? "",
        };
    }
}
