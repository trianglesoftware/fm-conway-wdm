﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class EquipmentItemChecklistModel
    {
        public Guid EquipmentItemChecklistID { get; set; }
        public Guid WorkInstructionID { get; set; }

        public int LoadingSheet { get; set; }
        public string Equipment { get; set; }
        public string VmsOrAsset { get; set; }
        public int ItemNumber { get; set; }

        public static Expression<Func<EquipmentItemChecklist, EquipmentItemChecklistModel>> EntityToModel = a => new EquipmentItemChecklistModel
        {
            EquipmentItemChecklistID = a.EquipmentItemChecklistPK,
            WorkInstructionID = a.LoadingSheetEquipmentItem.LoadingSheetEquipment.LoadingSheet.WorkInstructionFK,

            LoadingSheet = a.LoadingSheetEquipmentItem.LoadingSheetEquipment.LoadingSheet.LoadingSheetNumber,
            Equipment = a.LoadingSheetEquipmentItem.LoadingSheetEquipment.Equipment.EquipmentName,
            VmsOrAsset = a.LoadingSheetEquipmentItem.LoadingSheetEquipment.Equipment.EquipmentType.EquipmentTypeVmsOrAsset.Name,
            ItemNumber = a.LoadingSheetEquipmentItem.ItemNumber,
        };
    }
}
