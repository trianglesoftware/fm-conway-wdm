﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class VehicleChecklistAnswersModel
    {
        public Guid? VehicleChecklistAnswerID { get; set; }
        public Guid? VehicleChecklistID { get; set; }
        public Guid ShiftID { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleRegistration { get; set; }
        public bool? StartOfShift { get; set; }
        public string Question { get; set; }
        public int? Answer { get; set; }
        public int QuestionOrder { get; set; }
        public Guid? VehicleChecklistDefectID { get; set; }

        public static Expression<Func<vv_VehicleChecklistAnswers, VehicleChecklistAnswersModel>> ETM = e => new VehicleChecklistAnswersModel()
        {
            VehicleChecklistAnswerID = e.VehicleChecklistAnswerPK,
            VehicleChecklistID = e.VehicleChecklistPK,
            ShiftID = e.ShiftPK,
            VehicleMake = e.Make,
            VehicleRegistration = e.Registration,
            StartOfShift = e.StartOfShift,
            Question = e.ChecklistQuestion,
            Answer = e.Answer,
            QuestionOrder = e.QuestionOrder,
            VehicleChecklistDefectID = e.VehicleChecklistDefectPK
        };
    }
}
