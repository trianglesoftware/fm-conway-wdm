﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class ShiftVehicleDetailModel
    {
        public Guid ShiftVehicleID { get; set; }
        public Guid ShiftID { get; set; }
        public Guid VehicleID { get; set; }
        public Guid WorkInstructionID { get; set; }

        public string Vehicle { get; set; }
        public string Registration { get; set; }
        public string Driver { get; set; }
        public int StartMileage { get; set; }
        public int EndMileage { get; set; }
        public string ReasonNotOn { get; set; }

        public static Expression<Func<ShiftVehicle, ShiftVehicleDetailModel>> EntityToModel = a => new ShiftVehicleDetailModel
        {
            ShiftVehicleID = a.ShiftVehiclePK,
            ShiftID = a.ShiftFK,
            VehicleID = a.VehicleFK,
            WorkInstructionID = a.Shift.JobPacks.SelectMany(b => b.WorkInstructions).Select(b => b.WorkInstructionPK).FirstOrDefault(),

            Vehicle = a.Vehicle.Model,
            Registration = a.Vehicle.Registration,
            Driver = a.Employee.EmployeeName,
            StartMileage = a.StartMileage,
            EndMileage = a.EndMileage,
            ReasonNotOn = a.ReasonNotOnShift
        };        
    }

    public class ShiftVehiclePhotoModel
    {
        public Guid VehicleChecklistPK { get; set; }
        public Guid FileID { get; set; }
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public bool StartOfShift { get; set; }

        public static Expression<Func<VehicleChecklist, ShiftVehiclePhotoModel>> EntityToModel = e => new ShiftVehiclePhotoModel
        {
            VehicleChecklistPK = e.VehicleChecklistPK,
            FileID = (Guid)e.PhotoFileFK,
            Date = e.CreatedDate,
            FileName = e.File.FileDownloadName,
            Latitude = e.File.Latitude ?? 0.00m,
            Longitude = e.File.Longitude ?? 0.00m,
            StartOfShift = e.StartOfShift
        };
    }
}
