﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Models
{
    public class NoteModel
    {
        public Guid NoteID { get; set; }
        public string NoteDetail { get; set; }
        public DateTime NoteDate { get; set; }

        public static Expression<Func<Note, NoteModel>> EntityToModel = entity => new NoteModel() 
        {
            NoteID = entity.NotePK,
            NoteDetail = entity.Detail,
            NoteDate = entity.NoteDate
        };
    }
}
