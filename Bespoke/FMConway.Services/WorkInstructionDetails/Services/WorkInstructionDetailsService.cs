﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.WorkInstructionDetails.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using System.Security.Authentication;
using FMConway.Services.WorkInstructionDetails.Interfaces;
using FMConway.Services.WorkInstructions.Interfaces;
using FMConway.Services.WorkInstructionDetails.Extensions;

namespace FMConway.Services.WorkInstructionDetails.Services
{
    public class WorkInstructionDetailsService : Service, FMConway.Services.WorkInstructionDetails.Interfaces.IWorkInstructionDetailsService
    {
        UserContext _userContext;
        IWorkInstructionService _workInstructionService;

        public WorkInstructionDetailsService(UserContext userContext, IWorkInstructionService workInstructionService)
        {
            _userContext = userContext;
            _workInstructionService = workInstructionService;
        }

        #region Tables

        public PagedResults<VehicleDefectPhotoModel> GetVehicleDefectPhotosPaged(int rowCount, int page, string query, Guid vehicleChecklistDefectID)
        {
            var baseQuery = Context.VehicleChecklistDefectPhotos.Where(d => d.VehicleChecklistDefectFK.Equals(vehicleChecklistDefectID) && d.IsActive).OrderBy(d => d.CreatedDate).VehicleChecklistDefectPhotoOverviewQuery(query).Select(VehicleDefectPhotoModel.ETM);

            return PagedResults<VehicleDefectPhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ShiftVehiclePhotoModel> GetShiftVehiclePhotosPaged(int rowCount, int page, string query, Guid shiftVehicleID)
        {
            var sv = Context.ShiftVehicles.Where(a => a.ShiftVehiclePK == shiftVehicleID).Select(a => new
            {
                a.ShiftFK,
                a.VehicleFK
            }).Single();

            var baseQuery = Context.VehicleChecklists
                .Where(a => a.ShiftFK == sv.ShiftFK && a.VehicleFK == sv.VehicleFK && a.PhotoFileFK != null)
                .OrderBy(a => !a.StartOfShift)
                .ShiftVehiclePhotoQuery(query)
                .Select(ShiftVehiclePhotoModel.EntityToModel);

            return PagedResults<ShiftVehiclePhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<MaintenanceDetailPhotoModel> GetMaintenanceCheckPhotosPaged(int rowCount, int page, string query, Guid maintenanceCheckDefectID)
        {
            var baseQuery = Context.MaintenanceCheckDetailPhotos.Where(m => m.MaintenanceCheckDetailFK.Equals(maintenanceCheckDefectID) && m.IsActive).OrderBy(m => m.CreatedDate).MaintenanceCheckDetailPhotoOverviewQuery(query).Select(MaintenanceDetailPhotoModel.ETM);

            return PagedResults<MaintenanceDetailPhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<NoteModel> GetNotesPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            var baseQuery = Context.Notes.Where(n => n.Shift.JobPacks.Any(jp => jp.WorkInstructions.Any(wi => wi.WorkInstructionPK.Equals(workInstructionID))) && n.IsActive).OrderBy(n => n.NoteDate).NoteOverviewQuery(query).Select(NoteModel.EntityToModel);

            return PagedResults<NoteModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<VehicleChecklistAnswersModel> GetVehicleChecklistAnswersPaged(int rowCount, int page, string query, Guid shiftID)
        {
            var baseQuery = Context.vv_VehicleChecklistAnswers
                .Where(v => v.ShiftPK.Equals(shiftID))
                .OrderByDescending(v => v.Make).ThenByDescending(a => a.StartOfShift).ThenBy(a => a.QuestionOrder)
                .VehicleChecklistAnswersOverviewQuery(query).Select(VehicleChecklistAnswersModel.ETM);

            return PagedResults<VehicleChecklistAnswersModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ChecklistAnswerModel> GetMaintenanceCheckAnswersPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            var baseQuery = Context.vv_MaintenanceChecks.Where(mc => mc.WorkInstructionPK.Equals(workInstructionID)).OrderBy(mc => mc.MaintenanceCheckPK).ThenBy(mc => mc.QuestionOrder).MaintenanceCheckAnswersOverviewQuery(query).Select(ChecklistAnswerModel.ETM);

            return PagedResults<ChecklistAnswerModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<BriefingSignatureModel> GetBriefingSignaturesPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            var baseQuery = Context.BriefingSignatures.Where(bs => bs.Briefing.JobPack.WorkInstructions.Any(wi => wi.WorkInstructionPK.Equals(workInstructionID)) && bs.IsActive).OrderBy(bs => bs.BriefingStart).ThenBy(bs => bs.EmployeeFK).BriefingSignatureOverviewQuery(query).Select(BriefingSignatureModel.ETM);

            return PagedResults<BriefingSignatureModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<LoadingSheetSignatureModel> GetLoadingSheetSignaturesPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            var baseQuery = Context.LoadingSheetSignatures.Where(bs => bs.LoadingSheet.WorkInstructionFK.Equals(workInstructionID) && bs.IsActive).OrderBy(bs => bs.CreatedDate).LoadingSheetSignatureOverviewQuery(query).Select(LoadingSheetSignatureModel.ETM);

            return PagedResults<LoadingSheetSignatureModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ShiftStaffModel> GetShiftStaffPaged(int rowCount, int page, string query, Guid shiftID)
        {
            var baseQuery = Context.ShiftStaffs.Where(s => s.ShiftFK.Equals(shiftID) && s.IsActive).OrderBy(s => s.Employee.EmployeeName).ShiftStaffOverviewQuery(query).Select(ShiftStaffModel.ETM);

            return PagedResults<ShiftStaffModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ShiftVehicleModel> GetShiftVehiclesPaged(int rowCount, int page, string query, Guid shiftID)
        {
            var baseQuery = Context.ShiftVehicles.Where(s => s.ShiftFK.Equals(shiftID) && s.IsActive).OrderBy(s => s.Vehicle.Make).ShiftVehicleOverviewQuery(query).Select(ShiftVehicleModel.ETM);

            return PagedResults<ShiftVehicleModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<LoadingSheetEquipmentItemModel> GetLoadingSheetEquipmentItemsPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            int intQuery;
            var isintQuery = int.TryParse(query, out intQuery);

            var baseQuery = Context.LoadingSheetEquipmentItems
                .Where(a => a.LoadingSheetEquipment.LoadingSheet.WorkInstructionFK == workInstructionID)
                .Where(a =>
                    (isintQuery ? intQuery == a.LoadingSheetEquipment.LoadingSheet.LoadingSheetNumber : false) ||
                    a.LoadingSheetEquipment.Equipment.EquipmentName.Contains(query) ||
                    a.LoadingSheetEquipment.Equipment.EquipmentType.EquipmentTypeVmsOrAsset.Name.Contains(query) ||
                    (isintQuery ? intQuery == a.ItemNumber : false)
                )
                .OrderBy(s => s.LoadingSheetEquipment.Equipment.EquipmentName).ThenBy(a => a.ItemNumber)
                .Select(LoadingSheetEquipmentItemModel.EntityToModel)
                .ToList();

            return PagedResults<LoadingSheetEquipmentItemModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<EquipmentItemChecklistAnswerModel> GetEquipmentItemChecklistAnswersPaged(int rowCount, int page, string query, Guid equipmentItemChecklistID)
        {
            var baseQuery = Context.EquipmentItemChecklistAnswers
                .Where(a => a.EquipmentItemChecklistFK == equipmentItemChecklistID)
                .Where(a =>
                    a.ChecklistQuestion.ChecklistQuestion1.Contains(query) ||
                    (query == "yes" ? a.Answer : false) ||
                    (query == "no" ? !a.Answer : false) ||
                    a.Reason.Contains(query)
                )
                .OrderBy(s => s.ChecklistQuestion.QuestionOrder)
                .Select(EquipmentItemChecklistAnswerModel.EntityToModel)
                .ToList();

            return PagedResults<EquipmentItemChecklistAnswerModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        #endregion

        #region Single





        public VehicleDefectModel SingleVehicleDefect(Guid vehicleAnswerID)
        {
            return Context.VehicleChecklistDefects.Where(vd => vd.VehicleChecklistAnswerFK.Equals(vehicleAnswerID) && vd.IsActive).Select(VehicleDefectModel.ETM).SingleOrDefault();
        }

        public MaintenanceDetailModel SingleMaintenanceDetail(Guid maintenanceAnswerID)
        {
            return Context.MaintenanceCheckDetails.Where(md => md.MaintenanceCheckAnswerFK.Equals(maintenanceAnswerID) && md.IsActive).Select(MaintenanceDetailModel.ETM).SingleOrDefault();
        }

        public ShiftVehicleDetailModel SingleShiftVehicle(Guid shiftVehicleID)
        {
            return Context.ShiftVehicles.Where(a => a.ShiftVehiclePK == shiftVehicleID && a.IsActive).Select(ShiftVehicleDetailModel.EntityToModel).SingleOrDefault();
        }

        #endregion

        #region TestData

        public void CreateVehicleChecklist()
        {
            try
            {
                //Create Vehicle checklists
                
                Guid vehicleID = new Guid("7E8519F0-B5C6-4608-8E31-825931D7F48D");
                Guid checklistID = new Guid("3BC297C0-FFD4-4D10-B471-61E593F40523");
                Guid shiftID = new Guid("4D830360-2A05-4AE8-A65B-379FFF9EB8CD");

                List<ChecklistQuestion> questions = Context.ChecklistQuestions.Where(q => q.ChecklistFK.Equals(checklistID)).OrderBy(q => q.QuestionOrder).ToList();

                VehicleChecklist vc = new VehicleChecklist()
                {
                    VehicleChecklistPK = Guid.NewGuid(),
                    VehicleFK = vehicleID,
                    ShiftFK = shiftID,
                    ChecklistFK = checklistID,
                    StartOfShift = false,
                    IsActive = true,
                    CreatedByID = _userContext.UserDetails.ID,
                    CreatedDate = DateTime.Now,
                    UpdatedByID = _userContext.UserDetails.ID,
                    UpdatedDate = DateTime.Now
                };

                int i = 0;
                foreach (var q in questions)
                {
                    VehicleChecklistAnswer vca = new VehicleChecklistAnswer()
                    {
                        VehicleChecklistAnswerPK = Guid.NewGuid(),
                        VehicleChecklistFK = vc.VehicleChecklistPK,
                        ChecklistQuestionFK = q.ChecklistQuestionPK,
                        Answer = 1,
                        CreatedByID = _userContext.UserDetails.ID,
                        CreatedDate = DateTime.Now,
                        UpdatedByID = _userContext.UserDetails.ID,
                        UpdatedDate = DateTime.Now
                    };

                    if (i == 0)
                    {
                        vca.Answer = 0;

                        VehicleChecklistDefect vcad = new VehicleChecklistDefect()
                        {
                            VehicleChecklistDefectPK = Guid.NewGuid(),
                            Description = "Broken axle",
                            VehicleChecklistAnswerFK = vca.VehicleChecklistAnswerPK,
                            IsActive = true,
                            CreatedByID = _userContext.UserDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByID = _userContext.UserDetails.ID,
                            UpdatedDate = DateTime.Now
                        };

                        Guid fileID = new Guid("5059FCBC-D1C3-453D-AD32-27C6AB271F18");

                        VehicleChecklistDefectPhoto vcadp = new VehicleChecklistDefectPhoto()
                        {
                            VehicleChecklistDefectPhotoPK = Guid.NewGuid(),
                            VehicleChecklistDefectFK = vcad.VehicleChecklistDefectPK,
                            FileFK = fileID,
                            IsActive = true,
                            CreatedByID = _userContext.UserDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByID = _userContext.UserDetails.ID,
                            UpdatedDate = DateTime.Now
                        };

                        vcad.VehicleChecklistDefectPhotos.Add(vcadp);

                        vca.VehicleChecklistDefects.Add(vcad);

                        i++;

                    }

                    vc.VehicleChecklistAnswers.Add(vca);
                }

                Context.VehicleChecklists.Add(vc);
                SaveChanges();
            }
            catch (Exception e)
            {
            }
        }

        public void CreateChecklistChecklist()
        {
            try
            {
                Checklist checklist = new Checklist() 
                {
                    ChecklistPK = Guid.NewGuid(),
                    ChecklistName = "Maintenance Checklist",
                    IsContractChecklist = false,
                    IsJobTypeChecklist = false,
                    IsVehicleTypeChecklist = false,
                    IsMaintenanceChecklist = true,
                    CompleteAtDepot = false,
                    StartOfWork = true,
                    IsActive = true,
                    CreatedByFK = _userContext.UserDetails.ID,
                    CreatedDate = DateTime.Now,
                    UpdatedByFK = _userContext.UserDetails.ID,
                    UpdatedDate = DateTime.Now
                };

                for (int i = 1; i < 11; i++)
                {
                    ChecklistQuestion cq = new ChecklistQuestion() 
                    {
                        ChecklistQuestionPK = Guid.NewGuid(),
                        ChecklistQuestion1 = "Question",
                        QuestionOrder = i,
                        ChecklistFK = checklist.ChecklistPK,
                        IsActive = true,
                        CreatedByFK = _userContext.UserDetails.ID,
                        CreatedDate = DateTime.Now,
                        UpdatedByFK = _userContext.UserDetails.ID,
                        UpdatedDate = DateTime.Now
                    };

                    checklist.ChecklistQuestions.Add(cq);
                }

                Context.Checklists.Add(checklist);

                SaveChanges();

            }
            catch (Exception e)
            { }
        }

        public void CreateMaintenaceChecklist()
        {
            try
            {
                //Create Maintenance checklist

                Guid checklistID = new Guid("F4159969-0995-496A-8653-80FD38562BF5");
                Guid workInstructionID = new Guid("F7A8ED80-BD46-487A-8668-613947FE615B");

                List<ChecklistQuestion> questions = Context.ChecklistQuestions.Where(q => q.ChecklistFK.Equals(checklistID)).OrderBy(q => q.QuestionOrder).ToList();

                MaintenanceCheck mc = new MaintenanceCheck()
                {
                    MaintenanceCheckPK = Guid.NewGuid(),
                    WorksInstructionFK = workInstructionID,
                    Time = DateTime.Now,
                    Description = "Maintenance Checklist",
                    IsActive = true,
                    CreatedByID = _userContext.UserDetails.ID,
                    CreatedDate = DateTime.Now,
                    UpdatedByID = _userContext.UserDetails.ID,
                    UpdatedDate = DateTime.Now
                };

                int i = 0;
                foreach (var q in questions)
                {
                    MaintenanceCheckAnswer mca = new MaintenanceCheckAnswer()
                    {
                        MaintenanceCheckAnswerPK = Guid.NewGuid(),
                        MaintenanceCheckFK = mc.MaintenanceCheckPK,
                        ChecklistQuestionFK = q.ChecklistQuestionPK,
                        Answer = true,
                        CreatedByID = _userContext.UserDetails.ID,
                        CreatedDate = DateTime.Now,
                        UpdatedByID = _userContext.UserDetails.ID,
                        UpdatedDate = DateTime.Now
                    };

                    if (i == 0)
                    {
                        mca.Answer = false;

                        MaintenanceCheckDetail mcad = new MaintenanceCheckDetail()
                        {
                            MaintenanceCheckDetailPK = Guid.NewGuid(),
                            Description = "Broken signs",
                            MaintenanceCheckAnswerFK = mca.MaintenanceCheckAnswerPK,
                            IsActive = true,
                            CreatedByID = _userContext.UserDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByID = _userContext.UserDetails.ID,
                            UpdatedDate = DateTime.Now
                        };

                        Guid fileID = new Guid("5059FCBC-D1C3-453D-AD32-27C6AB271F18");

                        MaintenanceCheckDetailPhoto mcadp = new MaintenanceCheckDetailPhoto()
                        {
                            MaintenanceCheckDetailPhotoPK = Guid.NewGuid(),
                            MaintenanceCheckDetailFK = mcad.MaintenanceCheckDetailPK,
                            FileFK = fileID,
                            IsActive = true,
                            CreatedByID = _userContext.UserDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByID = _userContext.UserDetails.ID,
                            UpdatedDate = DateTime.Now
                        };

                        mcad.MaintenanceCheckDetailPhotos.Add(mcadp);

                        mca.MaintenanceCheckDetails.Add(mcad);

                        i++;

                    }

                    mc.MaintenanceCheckAnswers.Add(mca);
                }

                Context.MaintenanceChecks.Add(mc);
                SaveChanges();
            }
            catch (Exception e)
            {
            }
        }

        #endregion
    }
}
