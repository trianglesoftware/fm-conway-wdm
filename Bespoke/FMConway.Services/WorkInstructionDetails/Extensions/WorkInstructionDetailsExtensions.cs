﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructionDetails.Extensions
{
    public static class WorkInstructionDetailsExtensions
    {    
        public static IQueryable<Note> NoteOverviewQuery(this IQueryable<Note> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Detail.Contains(query));
            }

            return model;
        }

        public static IQueryable<vv_VehicleChecklistAnswers> VehicleChecklistAnswersOverviewQuery(this IQueryable<vv_VehicleChecklistAnswers> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ChecklistQuestion.Contains(query) || bq.Registration.Contains(query) || bq.Make.Contains(query));
            }

            return model; 
        }

        public static IQueryable<vv_MaintenanceChecks> MaintenanceCheckAnswersOverviewQuery(this IQueryable<vv_MaintenanceChecks> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ChecklistQuestion.Contains(query));
            }

            return model;
        }


        public static IQueryable<BriefingSignature> BriefingSignatureOverviewQuery(this IQueryable<BriefingSignature> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bs => bs.Briefing.Name.Contains(query));
            }

            return model;
        }

        public static IQueryable<VehicleChecklistDefectPhoto> VehicleChecklistDefectPhotoOverviewQuery(this IQueryable<VehicleChecklistDefectPhoto> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.File.FileDownloadName.Contains(query));
            }

            return model;
        }

        public static IQueryable<VehicleChecklist> ShiftVehiclePhotoQuery(this IQueryable<VehicleChecklist> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.File.FileDownloadName.Contains(query));
            }

            return model;
        }

        public static IQueryable<MaintenanceCheckDetailPhoto> MaintenanceCheckDetailPhotoOverviewQuery(this IQueryable<MaintenanceCheckDetailPhoto> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.File.FileDownloadName.Contains(query));
            }

            return model;
        }

        public static IQueryable<LoadingSheetSignature> LoadingSheetSignatureOverviewQuery(this IQueryable<LoadingSheetSignature> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.LoadingSheet.Employee.EmployeeName.Contains(query) || bq.File.FileDownloadName.Contains(query));
            }

            return model;
        }

        public static IQueryable<ShiftStaff> ShiftStaffOverviewQuery(this IQueryable<ShiftStaff> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(ss => ss.Employee.EmployeeName.Contains(query));
            }

            return model;
        }

        public static IQueryable<ShiftVehicle> ShiftVehicleOverviewQuery(this IQueryable<ShiftVehicle> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(ss => ss.Vehicle.Make.Contains(query) || ss.Vehicle.Model.Contains(query));
            }

            return model;
        }
    }
}
