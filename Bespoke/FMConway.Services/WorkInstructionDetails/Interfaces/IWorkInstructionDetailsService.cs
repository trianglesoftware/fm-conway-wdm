﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Collections.Paging;
using FMConway.Services.WorkInstructions.Models;
using FMConway.Services.WorkInstructions.Interfaces;
using FMConway.Services.WorkInstructionDetails.Models;

namespace FMConway.Services.WorkInstructionDetails.Interfaces
{
    public interface IWorkInstructionDetailsService : IService
    { 
        PagedResults<NoteModel> GetNotesPaged(int rowCount, int page, string query, Guid workInstructionID);

        PagedResults<VehicleChecklistAnswersModel> GetVehicleChecklistAnswersPaged(int rowCount, int page, string query, Guid shiftID);

        PagedResults<VehicleDefectPhotoModel> GetVehicleDefectPhotosPaged(int rowCount, int page, string query, Guid vehicleChecklistDefectID);

        PagedResults<ShiftVehiclePhotoModel> GetShiftVehiclePhotosPaged(int rowCount, int page, string query, Guid shiftVehicleID);

        PagedResults<ChecklistAnswerModel> GetMaintenanceCheckAnswersPaged(int rowCount, int page, string query, Guid workInstructionID);

        PagedResults<MaintenanceDetailPhotoModel> GetMaintenanceCheckPhotosPaged(int rowCount, int page, string query, Guid maintenanceCheckDefectID);

        PagedResults<BriefingSignatureModel> GetBriefingSignaturesPaged(int rowCount, int page, string query, Guid workInstructionID);

        PagedResults<LoadingSheetSignatureModel> GetLoadingSheetSignaturesPaged(int rowCount, int page, string query, Guid workInstructionID);

        PagedResults<ShiftStaffModel> GetShiftStaffPaged(int rowCount, int page, string query, Guid shiftID);

        PagedResults<ShiftVehicleModel> GetShiftVehiclesPaged(int rowCount, int page, string query, Guid shiftID);
        
        PagedResults<LoadingSheetEquipmentItemModel> GetLoadingSheetEquipmentItemsPaged(int rowCount, int page, string query, Guid workInstructionID);

        PagedResults<EquipmentItemChecklistAnswerModel> GetEquipmentItemChecklistAnswersPaged(int rowCount, int page, string query, Guid equipmentItemChecklistID);

        VehicleDefectModel SingleVehicleDefect(Guid vehicleAnswerID);

        MaintenanceDetailModel SingleMaintenanceDetail(Guid maintenanceAnswerID);

        ShiftVehicleDetailModel SingleShiftVehicle(Guid shiftVehicleID);

    }
}
