﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleTypes.Interfaces
{
    public interface IVehicleTypeService : IService
    {
        void Create(VehicleTypeModel model, Dictionary<Guid, string> checklists);
        List<VehicleTypeModel> GetActiveVehicleTypes();
        List<VehicleTypeModel> GetInactiveVehicleTypes();
        VehicleTypeModel Single(Guid id);
        void Update(VehicleTypeModel model, Dictionary<Guid, string> checklists, Dictionary<Guid, string> checklistModel);
        //List<JobJobTypeModel> Get(Guid jobID);
        //List<WorkInstructionJobType> GetForWI(Guid workInstructionID);

        PagedResults<VehicleTypeLookup> GetPagedActiveVehicleTypesLookup(int rowCount, int page, string query);
    }
}
