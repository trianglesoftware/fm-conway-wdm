﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.VehicleTypes.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.VehicleTypes.Services
{
    public class VehicleTypeService : Service, FMConway.Services.VehicleTypes.Interfaces.IVehicleTypeService
    {
        UserContext _userContext;

        public VehicleTypeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<VehicleTypeModel> GetActiveVehicleTypes()
        {
            return Context.VehicleTypes.Where(a => a.IsActive).Select(VehicleTypeModel.EntityToModel).ToList();
        }

        public List<VehicleTypeModel> GetInactiveVehicleTypes()
        {
            return Context.VehicleTypes.Where(a => !a.IsActive).Select(VehicleTypeModel.EntityToModel).ToList();
        }

        public void Create(VehicleTypeModel model, Dictionary<Guid, string> checklists)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.VehicleTypeID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.VehicleTypes.Add(VehicleTypeModel.ModelToEntity(model));

            if (checklists != null && checklists.Count > 0)
            {
                foreach (var checklist in checklists)
                {
                    VehicleTypeChecklist newVehicleTypeChecklist = new VehicleTypeChecklist();
                    newVehicleTypeChecklist.VehicleTypeChecklistPK = Guid.NewGuid();
                    newVehicleTypeChecklist.VehicleTypeFK = model.VehicleTypeID;
                    newVehicleTypeChecklist.ChecklistFK = checklist.Key;
                    newVehicleTypeChecklist.IsActive = true;
                    newVehicleTypeChecklist.CreatedByFK = _userContext.UserDetails.ID;
                    newVehicleTypeChecklist.CreatedDate = DateTime.Now;
                    newVehicleTypeChecklist.UpdatedByFK = _userContext.UserDetails.ID;
                    newVehicleTypeChecklist.UpdatedDate = DateTime.Now;

                    Context.VehicleTypeChecklists.Add(newVehicleTypeChecklist);
                }
            }

            SaveChanges();
        }

        public void Update(VehicleTypeModel model, Dictionary<Guid, string> checklists, Dictionary<Guid, string> checklistModel)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            VehicleType current = Context.VehicleTypes.Where(x => x.VehicleTypePK.Equals(model.VehicleTypeID)).SingleOrDefault();

            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.Name = model.Name == null ? "" : model.Name;

            if (checklistModel != null)
            {
                if (checklists != null)
                {
                    if (!checklists.SequenceEqual(checklistModel))
                    {
                        var vehicleTypeChecklists = Context.VehicleTypeChecklists.Where(x => x.VehicleTypeFK.Equals(model.VehicleTypeID));
                        foreach (var checklist in vehicleTypeChecklists)
                        {
                            Context.VehicleTypeChecklists.Remove(checklist);
                        }

                        foreach (var checklist in checklists)
                        {
                            VehicleTypeChecklist newVehicleTypeChecklist = new VehicleTypeChecklist();
                            newVehicleTypeChecklist.VehicleTypeChecklistPK = Guid.NewGuid();
                            newVehicleTypeChecklist.VehicleTypeFK = model.VehicleTypeID;
                            newVehicleTypeChecklist.ChecklistFK = checklist.Key;
                            newVehicleTypeChecklist.IsActive = true;
                            newVehicleTypeChecklist.CreatedByFK = _userContext.UserDetails.ID;
                            newVehicleTypeChecklist.CreatedDate = DateTime.Now;
                            newVehicleTypeChecklist.UpdatedByFK = _userContext.UserDetails.ID;
                            newVehicleTypeChecklist.UpdatedDate = DateTime.Now;

                            Context.VehicleTypeChecklists.Add(newVehicleTypeChecklist);
                        }
                    }
                }
                else
                {
                    var vehicleTypeChecklists = Context.VehicleTypeChecklists.Where(x => x.VehicleTypeFK.Equals(model.VehicleTypeID));
                    foreach (var checklist in vehicleTypeChecklists)
                    {
                        Context.VehicleTypeChecklists.Remove(checklist);
                    }
                }
            }

            SaveChanges();
        }

        public VehicleTypeModel Single(Guid id)
        {
            return Context.VehicleTypes.Where(a => a.VehicleTypePK.Equals(id)).Select(VehicleTypeModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<VehicleTypeLookup> GetPagedActiveVehicleTypesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.VehicleTypes.ActiveVehicleTypes().OrderBy(o => o.Name).VehicleTypeOverviewQuery(query).Select(VehicleTypeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<VehicleTypeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        //public List<VehicleTypeModel> Get(Guid jobID)
        //{
        //    return Context.JobVehicleTypes.Where(a => a.JobFK == jobID).Select(JobVehicleTypeModel.EntityToModel).ToList();
        //}

        //public List<WorkInstructionVehicleType> GetForWI(Guid workInstructionID)
        //{
        //    return Context.WorkInstructionVehicleTypes.Where(x => x.WorkInstructionFK.Equals(workInstructionID)).ToList();
        //}
    }
}
