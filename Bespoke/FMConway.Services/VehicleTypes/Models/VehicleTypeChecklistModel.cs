﻿using FMConway.Services.Checklists.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleTypes.Models
{
    public class VehicleTypeChecklistModel
    {
        public Guid VehicleTypeChecklistID { get; set; }
        public Guid VehicleTypeID { get; set; }
        public VehicleTypeModel VehicleType { get; set; }
        public Guid ChecklistID { get; set; }
        public ChecklistModel Checklist { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<VehicleTypeChecklist, VehicleTypeChecklistModel>> EntityToModel = entity => new VehicleTypeChecklistModel
        {
            VehicleTypeChecklistID = entity.VehicleTypeChecklistPK,
            VehicleTypeID = entity.VehicleTypeFK,
            VehicleType = new VehicleTypeModel()
            {
                Name = entity.VehicleType.Name
            },
            ChecklistID = entity.ChecklistFK,
            Checklist = new ChecklistModel()
            {
                ChecklistName = entity.Checklist.ChecklistName
            },

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<VehicleTypeChecklistModel, VehicleTypeChecklist> ModelToEntity = model => new VehicleTypeChecklist()
        {
            VehicleTypeChecklistPK = model.VehicleTypeChecklistID,
            VehicleTypeFK = model.VehicleTypeID,
            ChecklistFK = model.ChecklistID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
