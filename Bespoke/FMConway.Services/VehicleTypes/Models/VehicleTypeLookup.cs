﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleTypes.Models
{
    public class VehicleTypeLookup : LookupBase<Guid>
    {
        public static Expression<Func<VehicleType, VehicleTypeLookup>> EntityToModel = entity => new VehicleTypeLookup()
        {
            Key = entity.Name,
            Value = entity.VehicleTypePK,
        };
    }
}
