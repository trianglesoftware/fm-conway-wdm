﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleTypes.Models
{
    public class VehicleTypeModel
    {
        public Guid VehicleTypeID { get; set; }
        //[Required]
        public string Name { get; set; }
        
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<VehicleType, VehicleTypeModel>> EntityToModel = entity => new VehicleTypeModel
        {
            VehicleTypeID = entity.VehicleTypePK,
            Name = entity.Name,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<VehicleTypeModel, VehicleType> ModelToEntity = model => new VehicleType
        {
            VehicleTypePK = model.VehicleTypeID,
            Name = model.Name,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
