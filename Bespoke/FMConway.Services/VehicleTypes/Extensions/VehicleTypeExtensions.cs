﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleTypes.Extensions
{
    public static class VehicleTypeExtensions
    {
        public static IQueryable<VehicleType> ActiveVehicleTypes(this IQueryable<VehicleType> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<VehicleType> InactiveVehicleTypes(this IQueryable<VehicleType> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<VehicleType> VehicleTypeOverviewQuery(this IQueryable<VehicleType> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
