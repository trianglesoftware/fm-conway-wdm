﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ReasonCodes.Models
{
    public class ReasonCodeLookup : LookupBase<Guid>
    {
        public string Description { get; set; }

        public static Expression<Func<ReasonCode, ReasonCodeLookup>> EntityToModel = entity => new ReasonCodeLookup
        {
            Value = entity.ReasonCodePK,
            Key = entity.Code,

            Description = entity.Description
        };
    }
}
