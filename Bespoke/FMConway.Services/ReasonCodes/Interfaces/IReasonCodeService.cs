﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.ReasonCodes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ReasonCodes.Interfaces
{
    public interface IReasonCodeService : IService
    {
        void Create(ReasonCodeModel model);
        List<ReasonCodeModel> GetActiveReasonCodes();
        List<ReasonCodeModel> GetInactiveReasonCodes();
        ReasonCodeModel Single(Guid id);
        void Update(ReasonCodeModel model);

        PagedResults<ReasonCodeLookup> GetPagedActiveReasonCodesLookup(int rowCount, int page, string query);
    }
}
