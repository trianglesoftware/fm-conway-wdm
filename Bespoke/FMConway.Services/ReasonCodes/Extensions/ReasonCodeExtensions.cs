﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.ReasonCodes.Extensions
{
    public static class ReasonCodeExtensions
    {
        public static IQueryable<ReasonCode> ActiveReasonCodes(this IQueryable<ReasonCode> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<ReasonCode> InactiveReasonCodes(this IQueryable<ReasonCode> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<ReasonCode> ReasonCodeOverviewQuery(this IQueryable<ReasonCode> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Code.Contains(query));
            }

            return model;
        }
    }
}
