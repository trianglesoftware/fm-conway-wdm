﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.ReasonCodes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.ReasonCodes.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.ReasonCodes.Services
{
    public class ReasonCodeService : Service, FMConway.Services.ReasonCodes.Interfaces.IReasonCodeService
    {
        UserContext _userContext;

        public ReasonCodeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<ReasonCodeModel> GetActiveReasonCodes()
        {
            return Context.ReasonCodes.Where(a => a.IsActive).Select(ReasonCodeModel.EntityToModel).ToList();
        }

        public List<ReasonCodeModel> GetInactiveReasonCodes()
        {
            return Context.ReasonCodes.Where(a => !a.IsActive).Select(ReasonCodeModel.EntityToModel).ToList();
        }

        public void Create(ReasonCodeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.ReasonCodeID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.ReasonCodes.Add(ReasonCodeModel.ModelToEntity(model));

            SaveChanges();
        }

        public void Update(ReasonCodeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var current = Context.ReasonCodes.Where(x => x.ReasonCodePK.Equals(model.ReasonCodeID)).SingleOrDefault();
            current.Code = model.Code;
            current.Description = model.Description;
            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;

            SaveChanges();
        }

        public ReasonCodeModel Single(Guid id)
        {
            return Context.ReasonCodes.Where(a => a.ReasonCodePK.Equals(id)).Select(ReasonCodeModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<ReasonCodeLookup> GetPagedActiveReasonCodesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.ReasonCodes.ActiveReasonCodes().OrderBy(o => o.Code).ReasonCodeOverviewQuery(query).Select(ReasonCodeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<ReasonCodeLookup>
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
