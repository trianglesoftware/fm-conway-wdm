﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.EquipmentTypes.Models
{
    public class EquipmentTypeModel
    {
        public Guid EquipmentTypeID { get; set; }
        public Guid? EquipmentTypeVmsOrAssetID { get; set; }
        public string Name { get; set; }
        public string Asset { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<EquipmentType, EquipmentTypeModel>> EntityToModel = entity => new EquipmentTypeModel
        {
            EquipmentTypeID = entity.EquipmentTypePK,
            EquipmentTypeVmsOrAssetID = entity.EquipmentTypeVmsOrAssetFK,

            Name = entity.Name,
            Asset = entity.EquipmentTypeVmsOrAsset.Name,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<EquipmentTypeModel, EquipmentType> ModelToEntity = model => new EquipmentType
        {
            EquipmentTypePK = model.EquipmentTypeID,
            EquipmentTypeVmsOrAssetFK = model.EquipmentTypeVmsOrAssetID,

            Name = model.Name,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
