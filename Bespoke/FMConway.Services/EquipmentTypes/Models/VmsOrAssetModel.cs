﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.EquipmentTypes.Models
{
    public class VmsOrAssetModel
    {
        public Guid VmsOrAssetID { get; set; }
        public string Name { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<EquipmentTypeVmsOrAsset, VmsOrAssetModel>> EntityToModel = entity => new VmsOrAssetModel
        {
            VmsOrAssetID = entity.EquipmentTypeVmsOrAssetPK,
            Name = entity.Name,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<VmsOrAssetModel, EquipmentTypeVmsOrAsset> ModelToEntity = model => new EquipmentTypeVmsOrAsset
        {
            EquipmentTypeVmsOrAssetPK = model.VmsOrAssetID,
            Name = model.Name,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
