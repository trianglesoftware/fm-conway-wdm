﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.EquipmentTypes.Models
{
    public class EquipmentTypeLookup : LookupBase<Guid>
    {
        public static Expression<Func<EquipmentType, EquipmentTypeLookup>> EntityToModel = entity => new EquipmentTypeLookup()
        {
            Key = entity.Name,
            Value = entity.EquipmentTypePK,
        };
    }
}
