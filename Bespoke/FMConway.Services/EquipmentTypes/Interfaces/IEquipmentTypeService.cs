﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.EquipmentTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.EquipmentTypes.Interfaces
{
    public interface IEquipmentTypeService : IService
    {
        void Create(EquipmentTypeModel model);
        List<EquipmentTypeModel> GetActiveEquipmentTypes();
        List<EquipmentTypeModel> GetInactiveEquipmentTypes();
        EquipmentTypeModel Single(Guid id);
        void Update(EquipmentTypeModel model);

        PagedResults<EquipmentTypeLookup> GetPagedActiveEquipmentTypesLookup(int rowCount, int page, string query);
        List<VmsOrAssetModel> GetVmsOrAssets();
    }
}
