﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.EquipmentTypes.Extensions
{
    public static class EquipmentTypeExtensions
    {
        public static IQueryable<EquipmentType> ActiveEquipmentTypes(this IQueryable<EquipmentType> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<EquipmentType> InactiveEquipmentTypes(this IQueryable<EquipmentType> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<EquipmentType> EquipmentTypeOverviewQuery(this IQueryable<EquipmentType> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
