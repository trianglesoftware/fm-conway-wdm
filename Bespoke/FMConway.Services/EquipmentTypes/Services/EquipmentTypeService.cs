﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.EquipmentTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.EquipmentTypes.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.EquipmentTypes.Services
{
    public class EquipmentTypeService : Service, FMConway.Services.EquipmentTypes.Interfaces.IEquipmentTypeService
    {
        UserContext _userContext;

        public EquipmentTypeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<EquipmentTypeModel> GetActiveEquipmentTypes()
        {
            return Context.EquipmentTypes.Where(a => a.IsActive).Select(EquipmentTypeModel.EntityToModel).ToList();
        }

        public List<EquipmentTypeModel> GetInactiveEquipmentTypes()
        {
            return Context.EquipmentTypes.Where(a => !a.IsActive).Select(EquipmentTypeModel.EntityToModel).ToList();
        }

        public void Create(EquipmentTypeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.EquipmentTypeID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.EquipmentTypes.Add(EquipmentTypeModel.ModelToEntity(model));

            SaveChanges();
        }

        public void Update(EquipmentTypeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            EquipmentType current = Context.EquipmentTypes.Where(x => x.EquipmentTypePK.Equals(model.EquipmentTypeID)).SingleOrDefault();

            current.EquipmentTypeVmsOrAssetFK = model.EquipmentTypeVmsOrAssetID;
            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.Name = model.Name == null ? "" : model.Name;

            SaveChanges();
        }

        public EquipmentTypeModel Single(Guid id)
        {
            return Context.EquipmentTypes.Where(a => a.EquipmentTypePK.Equals(id)).Select(EquipmentTypeModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<EquipmentTypeLookup> GetPagedActiveEquipmentTypesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.EquipmentTypes.ActiveEquipmentTypes().OrderBy(o => o.Name).EquipmentTypeOverviewQuery(query).Select(EquipmentTypeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<EquipmentTypeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public List<VmsOrAssetModel> GetVmsOrAssets()
        {
            return Context.EquipmentTypeVmsOrAssets.Where(a => a.IsActive).Select(VmsOrAssetModel.EntityToModel).ToList();
        }
    }
}
