﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Schemes.Extensions
{
    public static class SchemeExtensions
    {
        public static IQueryable<Scheme> ActiveSchemes(this IQueryable<Scheme> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<Scheme> ActiveSchemesWithActiveContract(this IQueryable<Scheme> model)
        {
            return model.Where(a => a.IsActive && a.Contract.IsActive);
        }

        public static IQueryable<Scheme> SchemeOverviewQuery(this IQueryable<Scheme> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.SchemeTitle.Contains(query) ||
                                          bq.Description.Contains(query));
            }

            return model;
        }
    }
}
