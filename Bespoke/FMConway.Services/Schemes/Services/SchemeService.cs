﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Schemes.Extensions;
using FMConway.Services.Contracts.Models;

namespace FMConway.Services.Schemes.Services
{
    public class SchemeService : Service, FMConway.Services.Schemes.Interfaces.ISchemeService
    {
        UserContext _userContext;

        public SchemeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(ContractModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var s = new SchemeModel
            {
                SchemeTitle = model.ContractTitle ?? "",
                ContractID = model.ID,
            };


            return Create(s);
        }

        public Guid Create(SchemeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.SchemeID = Guid.NewGuid();
            model.SchemeTitle = model.SchemeTitle == null ? "" : model.SchemeTitle;
            model.SchemeDescription = model.SchemeDescription == null ? "" : model.SchemeDescription;

            model.AddressLine1 = model.AddressLine1 == null ? "" : model.AddressLine1;
            model.AddressLine2 = model.AddressLine2 == null ? "" : model.AddressLine2;
            model.AddressLine3 = model.AddressLine3 == null ? "" : model.AddressLine3;
            model.City = model.City == null ? "" : model.City;
            model.County = model.County == null ? "" : model.County;
            model.Postcode = model.Postcode == null ? "" : model.Postcode;

            model.Comments = model.Comments == null ? "" : model.Comments;
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Schemes.Add(SchemeModel.ModelToEntity(model));

            SaveChanges();

            return model.SchemeID;
        }

        public Guid Update(SchemeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Scheme current = Context.Schemes.Where(x => x.SchemePK.Equals(model.SchemeID)).SingleOrDefault();

            current.SchemeTitle = model.SchemeTitle == null ? "" : model.SchemeTitle;
            current.Description = model.SchemeDescription == null ? "" : model.SchemeDescription;

            current.AddressLine1 = model.AddressLine1 == null ? "" : model.AddressLine1;
            current.AddressLine2 = model.AddressLine2 == null ? "" : model.AddressLine2;
            current.AddressLine3 = model.AddressLine3 == null ? "" : model.AddressLine3;
            current.City = model.City == null ? "" : model.City;
            current.County = model.County == null ? "" : model.County;
            current.Postcode = model.Postcode == null ? "" : model.Postcode;

            current.Comments = model.Comments == null ? "" : model.Comments;
            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            Context.SaveChanges();

            return model.SchemeID;
        }

        public PagedResults<SchemeModel> GetActiveSchemesPaged(int rowCount, int page, string query, Guid contractID)
        {
            var baseQuery = Context.Schemes.Where(x => x.ContractFK.Equals(contractID)).ActiveSchemes().OrderBy(o => o.SchemeTitle).SchemeOverviewQuery(query).Select(SchemeModel.EntityToModel);

            return PagedResults<SchemeModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public SchemeModel Single(Guid id)
        {
            return Context.Schemes.Where(x => x.SchemePK.Equals(id)).Select(SchemeModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<SchemeLookup> GetPagedActiveSchemesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Schemes.ActiveSchemesWithActiveContract().OrderBy(o => o.SchemeTitle).SchemeOverviewQuery(query).Select(SchemeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<SchemeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
            };
        }


    }
}
