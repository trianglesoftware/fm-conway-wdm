﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.Schemes.Models
{
    public class SchemeModel
    {
        public Guid SchemeID { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string SchemeTitle { get; set; }
        public string SchemeDescription { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Comments { get; set; }
        public Guid ContractID { get; set; }
        public Guid DepotID { get; set; }
        public string DepotLookup { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Scheme, SchemeModel>> EntityToModel = entity => new SchemeModel
        {
            SchemeID = entity.SchemePK,
            SchemeTitle = entity.SchemeTitle,
            SchemeDescription = entity.Description,
            AddressLine1 = entity.AddressLine1,
            AddressLine2 = entity.AddressLine2,
            AddressLine3 = entity.AddressLine3,
            City = entity.City,
            County = entity.County,
            Postcode = entity.Postcode,
            Comments = entity.Comments,
            ContractID = entity.ContractFK,
            DepotID = entity.Contract.DepotFK,
            DepotLookup = entity.Contract.Depot.DepotName,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<SchemeModel, Scheme> ModelToEntity = model => new Scheme
        {
            SchemePK = model.SchemeID,
            SchemeTitle = model.SchemeTitle,
            Description = model.SchemeDescription,
            AddressLine1 = model.AddressLine1,
            AddressLine2 = model.AddressLine2,
            AddressLine3 = model.AddressLine3,
            City = model.City,
            County = model.County,
            Postcode = model.Postcode,
            Comments = model.Comments,
            ContractFK = model.ContractID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
