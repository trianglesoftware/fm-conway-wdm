﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Schemes.Models
{
    public class SchemeLookup : LookupBase<Guid>
    {
        public string Contract { get; set; }

        public static Expression<Func<Scheme, SchemeLookup>> EntityToModel = entity => new SchemeLookup()
        {
            Key = entity.SchemeTitle,
            Value = entity.SchemePK,
            Contract = entity.Contract.ContractTitle
        };
    }
}
