﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Contracts.Models;
using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Schemes.Interfaces
{
    public interface ISchemeService : IService
    {
        Guid Create(ContractModel model);
        Guid Create(SchemeModel model);
        Guid Update(SchemeModel model);
        PagedResults<SchemeModel> GetActiveSchemesPaged(int rowCount, int page, string query, Guid contractID);
        SchemeModel Single(Guid id);

        PagedResults<SchemeLookup> GetPagedActiveSchemesLookup(int rowCount, int page, string query);
    }
}
