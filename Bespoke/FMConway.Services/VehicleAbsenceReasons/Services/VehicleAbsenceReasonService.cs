﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.VehicleAbsenceReasons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.VehicleAbsenceReasons.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.VehicleAbsenceReasons.Services
{
    public class VehicleAbsenceReasonService : Service, FMConway.Services.VehicleAbsenceReasons.Interfaces.IVehicleAbsenceReasonService
    {
        UserContext _userContext;

        public VehicleAbsenceReasonService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<VehicleAbsenceReasonModel> GetActiveVehicleAbsenceReasons()
        {
            return Context.VehicleAbsenceReasons.Where(a => a.IsActive).Select(VehicleAbsenceReasonModel.EntityToModel).ToList();
        }

        public List<VehicleAbsenceReasonModel> GetInactiveVehicleAbsenceReasons()
        {
            return Context.VehicleAbsenceReasons.Where(a => !a.IsActive).Select(VehicleAbsenceReasonModel.EntityToModel).ToList();
        }

        public void Create(VehicleAbsenceReasonModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.VehicleAbsenceReasonID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.VehicleAbsenceReasons.Add(VehicleAbsenceReasonModel.ModelToEntity(model));

            SaveChanges();
        }

        public void Update(VehicleAbsenceReasonModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var current = Context.VehicleAbsenceReasons.Where(x => x.VehicleAbsenceReasonPK.Equals(model.VehicleAbsenceReasonID)).SingleOrDefault();
            current.Code = model.Code;
            current.Description = model.Description;
            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;

            SaveChanges();
        }

        public VehicleAbsenceReasonModel Single(Guid id)
        {
            return Context.VehicleAbsenceReasons.Where(a => a.VehicleAbsenceReasonPK.Equals(id)).Select(VehicleAbsenceReasonModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<VehicleAbsenceReasonLookup> GetPagedActiveVehicleAbsenceReasonsLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.VehicleAbsenceReasons.ActiveVehicleAbsenceReasons().OrderBy(o => o.Code).VehicleAbsenceReasonOverviewQuery(query).Select(VehicleAbsenceReasonLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<VehicleAbsenceReasonLookup>
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
