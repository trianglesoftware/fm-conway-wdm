﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.VehicleAbsenceReasons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleAbsenceReasons.Interfaces
{
    public interface IVehicleAbsenceReasonService : IService
    {
        void Create(VehicleAbsenceReasonModel model);
        List<VehicleAbsenceReasonModel> GetActiveVehicleAbsenceReasons();
        List<VehicleAbsenceReasonModel> GetInactiveVehicleAbsenceReasons();
        VehicleAbsenceReasonModel Single(Guid id);
        void Update(VehicleAbsenceReasonModel model);

        PagedResults<VehicleAbsenceReasonLookup> GetPagedActiveVehicleAbsenceReasonsLookup(int rowCount, int page, string query);
    }
}
