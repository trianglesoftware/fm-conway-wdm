﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleAbsenceReasons.Models
{
    public class VehicleAbsenceReasonModel
    {
        public Guid VehicleAbsenceReasonID { get; set; }
        [Required(ErrorMessage = "A Vehicle Absence Reason Code is required")]
        public string Code { get; set; }
        public string Description { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<VehicleAbsenceReason, VehicleAbsenceReasonModel>> EntityToModel = entity => new VehicleAbsenceReasonModel
        {
            VehicleAbsenceReasonID = entity.VehicleAbsenceReasonPK,
            Code = entity.Code,
            Description = entity.Description,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<VehicleAbsenceReasonModel, VehicleAbsenceReason> ModelToEntity = model => new VehicleAbsenceReason
        {
            VehicleAbsenceReasonPK = model.VehicleAbsenceReasonID,
            Code = model.Code,
            Description = model.Description,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
