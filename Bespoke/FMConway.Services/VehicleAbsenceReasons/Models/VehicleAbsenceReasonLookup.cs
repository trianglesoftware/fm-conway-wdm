﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleAbsenceReasons.Models
{
    public class VehicleAbsenceReasonLookup : LookupBase<Guid>
    {
        public string Description { get; set; }

        public static Expression<Func<VehicleAbsenceReason, VehicleAbsenceReasonLookup>> EntityToModel = entity => new VehicleAbsenceReasonLookup
        {
            Value = entity.VehicleAbsenceReasonPK,
            Key = entity.Code,

            Description = entity.Description
        };
    }
}
