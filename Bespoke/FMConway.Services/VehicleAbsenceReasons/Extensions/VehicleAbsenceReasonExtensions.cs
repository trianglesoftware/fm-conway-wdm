﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.VehicleAbsenceReasons.Extensions
{
    public static class VehicleAbsenceReasonExtensions
    {
        public static IQueryable<VehicleAbsenceReason> ActiveVehicleAbsenceReasons(this IQueryable<VehicleAbsenceReason> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<VehicleAbsenceReason> InactiveVehicleAbsenceReasons(this IQueryable<VehicleAbsenceReason> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<VehicleAbsenceReason> VehicleAbsenceReasonOverviewQuery(this IQueryable<VehicleAbsenceReason> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Code.Contains(query));
            }

            return model;
        }
    }
}
