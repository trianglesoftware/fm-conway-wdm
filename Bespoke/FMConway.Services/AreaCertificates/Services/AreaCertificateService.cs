﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.AreaCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.AreaCertificates.Extensions;
using FMConway.Services.AreaCertificates.Interfaces;

namespace FMConway.Services.AreaCertificates.Services
{
    public class AreaCertificateService : Service, FMConway.Services.AreaCertificates.Interfaces.IAreaCertificateService
    {
        UserContext _userContext;

        public IAreaCertificateDocumentService Documents { get; set; }

        public AreaCertificateService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(AreaCertificateModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.AreaCertificateID = Guid.NewGuid();
            model.CertificateName = model.CertificateName == null ? "" : model.CertificateName;
            model.CertificateDescription = model.CertificateDescription == null ? "" : model.CertificateDescription;
            model.IsFirstAid = false;
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.EmployeeCertificates.Add(AreaCertificateModel.ModelToEntity(model));

            SaveChanges();

            return model.AreaCertificateID;
        }

        public Guid Update(AreaCertificateModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            EmployeeCertificate current = Context.EmployeeCertificates.Where(x => x.EmployeeCertificatePK.Equals(model.AreaCertificateID)).SingleOrDefault();

            current.Name = model.CertificateName == null ? "" : model.CertificateName;
            current.Description = model.CertificateDescription == null ? "" : model.CertificateDescription;
            current.CertificateTypeFK = model.CertificateTypeID;
            current.AreaFK = model.AreaID;
            current.ExpiryDate = model.ExpiryDate;
            current.EmployeeFK = model.EmployeeID;
            current.IsFirstAid = false;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            Context.SaveChanges();

            return model.AreaCertificateID;
        }

        public PagedResults<AreaCertificateModel> GetActiveAreaCertificatesPaged(int rowCount, int page, string query, Guid employeeID)
        {
            var baseQuery = Context.EmployeeCertificates.Where(x => x.EmployeeFK.Equals(employeeID) && !x.IsFirstAid).ActiveAreaCertificates().OrderBy(o => o.Name).AreaCertificateOverviewQuery(query).Select(AreaCertificateModel.EntityToModel);

            return PagedResults<AreaCertificateModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public AreaCertificateModel Single(Guid id)
        {
            return Context.EmployeeCertificates.Where(x => x.EmployeeCertificatePK.Equals(id)).Select(AreaCertificateModel.EntityToModel).SingleOrDefault();
        }

        public List<JobCertificateModel> GetForJob(Guid id)
        {
            return Context.JobCertificates.Where(a => a.JobFK == id && a.IsActive).Select(JobCertificateModel.EntityToModel).ToList();
        }
    }
}
