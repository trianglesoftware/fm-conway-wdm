﻿using FMConway.Services.Areas.Models;
using FMConway.Services.CertificateTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.AreaCertificates.Models
{
    public class AreaCertificateModel
    {
        public Guid AreaCertificateID { get; set; }
        public string CertificateName { get; set; }
        public string CertificateDescription { get; set; }
        public DateTime ExpiryDate { get; set; }
        public Guid CertificateTypeID { get; set; }
        public CertificateTypeModel CertificateType { get; set; }
        public Guid EmployeeID { get; set; }
        public bool IsFirstAid { get; set; }
        public Guid? AreaID { get; set; }
        public AreaModel Area { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<EmployeeCertificate, AreaCertificateModel>> EntityToModel = entity => new AreaCertificateModel
        {
            AreaCertificateID = entity.EmployeeCertificatePK,
            CertificateName = entity.Name,
            CertificateDescription = entity.Description,
            ExpiryDate = entity.ExpiryDate,
            CertificateTypeID = entity.CertificateTypeFK,
            CertificateType = new CertificateTypeModel()
            {
                Name = entity.CertificateType.Name
            },
            EmployeeID = entity.EmployeeFK,
            IsFirstAid = entity.IsFirstAid,
            AreaID = entity.AreaFK,
            Area = new AreaModel()
            {
                Name = entity.Area.Area1
            },

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<AreaCertificateModel, EmployeeCertificate> ModelToEntity = model => new EmployeeCertificate
        {
            EmployeeCertificatePK = model.AreaCertificateID,
            Name = model.CertificateName,
            Description = model.CertificateDescription,
            ExpiryDate = model.ExpiryDate,
            CertificateTypeFK = model.CertificateTypeID,
            EmployeeFK = model.EmployeeID,
            IsFirstAid = model.IsFirstAid,
            AreaFK = model.AreaID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
