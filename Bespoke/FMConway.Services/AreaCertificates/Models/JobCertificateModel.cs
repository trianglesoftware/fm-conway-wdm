﻿using FMConway.Services.Areas.Models;
using FMConway.Services.CertificateTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.AreaCertificates.Models
{
    public class JobCertificateModel
    {
        public Guid JobCertificateID { get; set; }
        public Guid JobID { get; set; }
        public Guid CertificateTypeID { get; set; }

        public string CertificateType { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<JobCertificate, JobCertificateModel>> EntityToModel = a => new JobCertificateModel
        {
            JobCertificateID = a.JobCertificatePK,
            JobID = a.JobFK,
            CertificateTypeID = a.CertificateTypeFK,

            CertificateType = a.CertificateType.Name,

            IsActive = a.IsActive,
            CreatedByID = a.CreatedByFK,
            CreatedDate = a.CreatedDate,
            UpdatedByID = a.UpdatedByFK,
            UpdatedDate = a.UpdatedDate
        };

        public static Func<JobCertificateModel, JobCertificate> ModelToEntity = a => new JobCertificate
        {
            JobCertificatePK = a.JobCertificateID,
            JobFK = a.JobID,
            CertificateTypeFK = a.CertificateTypeID,

            IsActive = a.IsActive,
            CreatedByFK = a.CreatedByID,
            CreatedDate = a.CreatedDate,
            UpdatedByFK = a.UpdatedByID,
            UpdatedDate = a.UpdatedDate
        };
    }
}
