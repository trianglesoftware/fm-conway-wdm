﻿using FMConway.Services.AreaCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.AreaCertificates.Interfaces
{
    public interface IAreaCertificateDocumentService
    {
        void Create(Guid contractId, string documentNumber, string fileName, string contentType, byte[] documentData);

        void Update(DocumentModel document);
        void Inactivate(Guid id);
        DocumentModel GetDocument(Guid id);
        List<DocumentModel> GetActiveDocumentsForAreaCertificate(Guid jobID);
    }
}
