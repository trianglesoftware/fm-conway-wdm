﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.AreaCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.AreaCertificates.Interfaces
{
    public interface IAreaCertificateService : IService
    {
        IAreaCertificateDocumentService Documents { get; set; }

        Guid Create(AreaCertificateModel model);
        Guid Update(AreaCertificateModel model);
        PagedResults<AreaCertificateModel> GetActiveAreaCertificatesPaged(int rowCount, int page, string query, Guid contractID);
        AreaCertificateModel Single(Guid id);

        List<JobCertificateModel> GetForJob(Guid id);
    }
}
