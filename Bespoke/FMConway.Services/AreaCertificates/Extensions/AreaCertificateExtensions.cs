﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.AreaCertificates.Extensions
{
    public static class AreaCertificateExtensions
    {
        public static IQueryable<EmployeeCertificate> ActiveAreaCertificates(this IQueryable<EmployeeCertificate> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<EmployeeCertificate> AreaCertificateOverviewQuery(this IQueryable<EmployeeCertificate> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query) ||
                                          bq.Description.Contains(query) ||
                                          bq.CertificateType.Name.Contains(query));
            }

            return model;
        }
    }
}
