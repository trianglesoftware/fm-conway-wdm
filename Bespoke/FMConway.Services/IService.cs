﻿using System;

namespace FMConway.Services
{
    public interface IService
    {
        FMConwayEntities Context { get; }
        void SetContext(FMConwayEntities context, bool managedExternally = true);
        void SaveChanges();
    }
}
