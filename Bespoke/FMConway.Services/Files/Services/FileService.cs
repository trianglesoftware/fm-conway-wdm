﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Files.Model;
using FMConway.Services.Files.Interface;
using Triangle.Membership.Users;
using System.IO;
using System.Security.Authentication;

namespace FMConway.Services.Files.Services
{
    public class FileService : Service, FMConway.Services.Files.Interface.IFileService
    {
        UserContext _userContext;

        public FileService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public FileModel Add(FileModel file)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            //GetFileType(file);
            file.ID = Guid.NewGuid();
            file.CreatedByID = _userContext.UserDetails.ID;
            file.CreatedDate = DateTime.Now;
            file.UpdatedByID = file.CreatedByID;
            file.UpdatedDate = DateTime.Now;
            file.FileDownloadName = file.FileDownloadName.Replace(" ", "_");
            file.FileDownloadName = file.FileDownloadName.Replace("&", "");

            file.IsActive = true;

            Context.Files.Add(FileModel.ModelToEntity(file));

            SaveChanges();

            return file;
        }

        public void Update(FileModel file)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            File current = Context.Files.Where(x => x.FilePK.Equals(file.ID)).SingleOrDefault();

            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public void Delete(Guid id)
        {
            var file = Context.Files.Where(f => f.FilePK.Equals(id)).Single();
            Context.Files.Remove(file);
        }

        public FileModel Single(Guid id)
        {
            var file = Context.Files.Where(f => f.FilePK.Equals(id)).Select(FileModel.EntityToModel).Single();
            return file;
        }
    }
}
