﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Files.Model
{
    public class FileModel
    {
        public Guid ID { get; set; }
        public string FileDownloadName { get; set; }
        public byte[] Data { get; set; }
        public string ContentType { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public bool IsActive { get; set; }

        public static Expression<Func<File, FileModel>> EntityToModel = entity => new FileModel()
        {
            ID = entity.FilePK,
            FileDownloadName = entity.FileDownloadName,
            Data = entity.Data,
            ContentType = entity.ContentType,
            CreatedDate = entity.CreatedDate,
            CreatedByID = entity.CreatedByFK,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate,

            IsActive = entity.IsActive,
        };

        public static Func<FileModel, File> ModelToEntity = model => new File()
        {
            FilePK = model.ID,
            FileDownloadName = model.FileDownloadName,
            Data = model.Data,
            ContentType = model.ContentType,
            CreatedDate = model.CreatedDate,
            CreatedByFK = model.CreatedByID,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate,

            IsActive = model.IsActive,
        };
    }
}
