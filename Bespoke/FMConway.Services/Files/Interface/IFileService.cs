﻿using System;
using FMConway.Services.Files.Model;

namespace FMConway.Services.Files.Interface
{
    public interface IFileService : IService
    {
        FileModel Add(FileModel file);
        void Delete(Guid id);
        void Update(FileModel file);
        FileModel Single(Guid id);
    }
}
