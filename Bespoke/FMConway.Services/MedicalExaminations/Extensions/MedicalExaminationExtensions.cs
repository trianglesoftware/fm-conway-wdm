﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MedicalExaminations.Extensions
{
    public static class MedicalExaminationExtensions
    {
        public static IQueryable<MedicalExamination> ActiveMedicalExaminations(this IQueryable<MedicalExamination> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<MedicalExamination> MedicalExaminationOverviewQuery(this IQueryable<MedicalExamination> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query) ||
                                          bq.Description.Contains(query) ||
                                          bq.MedicalExaminationType.Name.Contains(query));
            }

            return model;
        }
    }
}
