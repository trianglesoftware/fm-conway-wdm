﻿using FMConway.Services.MedicalExaminationTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MedicalExaminations.Models
{
    public class MedicalExaminationModel
    {
        public Guid MedicalExaminationID { get; set; }
        public string MedicalExaminationName { get; set; }
        public string MedicalExaminationDescription { get; set; }
        public DateTime ExaminationDate { get; set; }
        public Guid MedicalExaminationTypeID { get; set; }
        public MedicalExaminationTypeModel MedicalExaminationType { get; set; }
        public Guid EmployeeID { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<MedicalExamination, MedicalExaminationModel>> EntityToModel = entity => new MedicalExaminationModel
        {
            MedicalExaminationID = entity.MedicalExaminationPK,
            MedicalExaminationName = entity.Name,
            MedicalExaminationDescription = entity.Description,
            ExaminationDate = entity.ExaminationDate,
            MedicalExaminationTypeID = entity.MedicalExaminationTypeFK,
            MedicalExaminationType = new MedicalExaminationTypeModel()
            {
                Name = entity.MedicalExaminationType.Name
            },
            EmployeeID = entity.EmployeeFK,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<MedicalExaminationModel, MedicalExamination> ModelToEntity = model => new MedicalExamination
        {
            MedicalExaminationPK = model.MedicalExaminationID,
            Name = model.MedicalExaminationName,
            Description = model.MedicalExaminationDescription,
            ExaminationDate = model.ExaminationDate,
            MedicalExaminationTypeFK = model.MedicalExaminationTypeID,
            EmployeeFK = model.EmployeeID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
