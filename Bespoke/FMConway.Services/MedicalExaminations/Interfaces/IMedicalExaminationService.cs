﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.MedicalExaminations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MedicalExaminations.Interfaces
{
    public interface IMedicalExaminationService : IService
    {
        Guid Create(MedicalExaminationModel model);
        Guid Update(MedicalExaminationModel model);
        PagedResults<MedicalExaminationModel> GetActiveMedicalExaminationsPaged(int rowCount, int page, string query, Guid contractID);
        MedicalExaminationModel Single(Guid id);
    }
}
