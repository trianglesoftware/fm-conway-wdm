﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.MedicalExaminations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.MedicalExaminations.Extensions;

namespace FMConway.Services.MedicalExaminations.Services
{
    public class MedicalExaminationService : Service, FMConway.Services.MedicalExaminations.Interfaces.IMedicalExaminationService
    {
        UserContext _userContext;

        public MedicalExaminationService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(MedicalExaminationModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.MedicalExaminationID = Guid.NewGuid();
            model.MedicalExaminationName = model.MedicalExaminationName == null ? "" : model.MedicalExaminationName;
            model.MedicalExaminationDescription = model.MedicalExaminationDescription == null ? "" : model.MedicalExaminationDescription;
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.MedicalExaminations.Add(MedicalExaminationModel.ModelToEntity(model));

            SaveChanges();

            return model.MedicalExaminationID;
        }

        public Guid Update(MedicalExaminationModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            MedicalExamination current = Context.MedicalExaminations.Where(x => x.MedicalExaminationPK.Equals(model.MedicalExaminationID)).SingleOrDefault();

            current.Name = model.MedicalExaminationName == null ? "" : model.MedicalExaminationName;
            current.Description = model.MedicalExaminationDescription == null ? "" : model.MedicalExaminationDescription;
            current.MedicalExaminationTypeFK = model.MedicalExaminationTypeID;
            current.ExaminationDate = model.ExaminationDate;
            current.EmployeeFK = model.EmployeeID;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            Context.SaveChanges();

            return model.MedicalExaminationID;
        }

        public PagedResults<MedicalExaminationModel> GetActiveMedicalExaminationsPaged(int rowCount, int page, string query, Guid employeeID)
        {
            var baseQuery = Context.MedicalExaminations.Where(x => x.EmployeeFK.Equals(employeeID)).ActiveMedicalExaminations().OrderBy(o => o.Name).MedicalExaminationOverviewQuery(query).Select(MedicalExaminationModel.EntityToModel);

            return PagedResults<MedicalExaminationModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public MedicalExaminationModel Single(Guid id)
        {
            return Context.MedicalExaminations.Where(x => x.MedicalExaminationPK.Equals(id)).Select(MedicalExaminationModel.EntityToModel).SingleOrDefault();
        }
    }
}
