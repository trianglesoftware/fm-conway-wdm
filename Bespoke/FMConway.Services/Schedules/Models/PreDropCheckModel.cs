﻿using FMConway.Services.Vehicles.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Schedules.Models
{
    public class PreDropCheckModel
    {
        public bool IsEmployeeAlreadyScheduled { get; set; }
        public bool IsVehicleAlreadyScheduled { get; set; }

        public static Expression<Func<Schedule, PreDropCheckModel>> EntityToModel = entity => new PreDropCheckModel
        {

        };
    }
}
