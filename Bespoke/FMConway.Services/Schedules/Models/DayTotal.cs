﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.Services.Schedules.Models
{
    public class DayTotal
    {
        public DateTime Day { get; set; }
        public int ScheduleCount { get; set; }
    }
}