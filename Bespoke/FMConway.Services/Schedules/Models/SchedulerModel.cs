﻿using FMConway.Services.Vehicles.Models;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.Schedules.Models
{
    public class SchedulerModel
    {
        public SchedulerModel()
        {
            Vehicles = new List<VehicleModel>();
            Operatives = new List<EmployeeModel>();
            RequiredCertificateTypeIDs = new List<Guid>();
        }

        public Guid ScheduleID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public Guid DepoID { get; set; }
        
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string Contract { get; set; }
        public string ContractNumber { get; set; }
        public string JobType { get; set; }
        public string JobCustomerProjectCode { get; set; }
        public string TaskDetails { get; set; }
        public string TrafficManagementActivities { get; set; }
        public string Address { get; set; }
        public int WorkInstructionNumber { get; set; }

        public bool HasShift { get; set; }
        public string ShiftProgress { get; set; }
        public DateTime TasksStartedTime { get; set; }
        public DateTime CheckCompletedTime { get; set; }
        public DateTime CompletedTime { get; set; }
        public DateTime CancelledTime { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool Confirmed { get; set; }
        public bool Cancelled { get; set; }
        public bool IsNightShift { get; set; }
        public int OrderNumber { get; set; }

        public List<VehicleModel> Vehicles { get; set; }
        public List<EmployeeModel> Operatives { get; set; }
        public List<Guid> RequiredCertificateTypeIDs { get; set; }
        public string TopVehicleRegistration { get; set; }

        public static Expression<Func<Schedule, SchedulerModel>> EntityToModel = a => new SchedulerModel
        {
            ScheduleID = a.SchedulePK,
            WorkInstructionID = a.WorkInstructionFK,
            DepoID = a.WorkInstruction.DepotFK,
            Contract = a.WorkInstruction.Job.Contract.ContractTitle,
            ContractNumber = a.WorkInstruction.Job.Contract.ContractNumber,
            JobType = a.WorkInstruction.JobType.DisplayName,
            JobCustomerProjectCode = a.WorkInstruction.Job.CustomerProjectCode,
            TaskDetails = a.WorkInstruction.Comments,
            TrafficManagementActivities = a.WorkInstruction.TrafficManagementActivities,
            WorkInstructionNumber = a.WorkInstruction.WorkInstructionNumber,
            Address = StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionFK),
            StartDate = a.StartDate,
            EndDate = a.EndDate,
            HasShift = a.ShiftFK != null,
            ShiftProgress = a.Shift.ShiftProgressInfoes.Count > 0 ?
                a.Shift.ShiftProgressInfoes.OrderByDescending(p => p.Time).Select(p => p.ShiftProgress.Progress).FirstOrDefault() :
                "NoShift",
            TasksStartedTime = a.Shift.ShiftProgressInfoes.Where(p => p.ShiftProgress.Progress.Equals("TasksStarted")).Select(p => p.Time).FirstOrDefault(),
            CheckCompletedTime = a.Shift.ShiftProgressInfoes.Where(p => p.ShiftProgress.Progress.Equals("ChecksCompleted")).Select(p => p.Time).FirstOrDefault(),
            CompletedTime = a.Shift.ShiftProgressInfoes.Where(p => p.ShiftProgress.Progress.Equals("Completed")).Select(p => p.Time).FirstOrDefault(),
            CancelledTime = a.Shift.ShiftProgressInfoes.Where(p => p.ShiftProgress.Progress.Equals("Cancelled")).Select(p => p.Time).FirstOrDefault(),
            UpdatedDate = a.UpdatedDate,
            Vehicles = a.Shift.ShiftVehicles.Where(w => w.IsActive).Select(s => new VehicleModel
            {
                VehicleID = s.VehicleFK,
                Make = s.Vehicle.Make,
                Model = s.Vehicle.Model,
                Registration = s.Vehicle.Registration
            }).ToList(),
            Operatives = a.Shift.ShiftStaffs.Where(w => w.IsActive).Select(ss => new EmployeeModel
            {
                EmployeeID = ss.EmployeeFK,
                EmployeeName = ss.Employee.EmployeeName
            }).ToList(), // try with stored procedure
            RequiredCertificateTypeIDs = a.WorkInstruction.Job.JobCertificates.Where(b => b.IsActive).Select(b => b.CertificateTypeFK).ToList(),
            Confirmed = a.Shift == null ? false : a.Shift.IsConfirmed,
            Cancelled = a.Shift == null ? false : a.Shift.IsCancelled,
            IsNightShift = a.WorkInstruction.IsNightShift,
            OrderNumber = a.WorkInstruction.OrderNumber,
        };
    }

    public class ProgressModel
    {
        public Guid ScheduleID { get; set; }
        public string ShiftProgress { get; set; }
        public DateTime CompletedDate { get; set; }
        public string CompletedTime { get; set; }
    }
}
