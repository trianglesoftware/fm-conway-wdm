﻿using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Schedules.Models
{
    public class ScheduleEmployeeModel
    {
        public Guid ScheduleEmployeeID { get; set; }
        public Guid ScheduleID { get; set; }
        public Guid EmployeeID { get; set; }
        public EmployeeModel Employee { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<ScheduleEmployee, ScheduleEmployeeModel>> EntityToModel = entity => new ScheduleEmployeeModel
        {
            ScheduleEmployeeID = entity.ScheduleEmployeePK,
            ScheduleID = entity.ScheduleFK,
            EmployeeID = entity.EmployeeFK,
            Employee = new EmployeeModel(){
                EmployeeName = entity.Employee.EmployeeName
            },
            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };
    }
}
