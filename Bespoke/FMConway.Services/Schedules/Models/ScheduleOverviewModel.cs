﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Schedules.Models
{
    public class ScheduleOverviewModel
    {
        public Guid ScheduleID { get; set; }
        public int WorkInstructionNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Job { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ScheduleRow { get; set; }

        public static Expression<Func<Schedule, ScheduleOverviewModel>> EntityToModel = entity => new ScheduleOverviewModel
        {
            ScheduleID = entity.SchedulePK,
            WorkInstructionNumber = entity.WorkInstruction.WorkInstructionNumber,
            AddressLine1 = entity.WorkInstruction.AddressLine1,
            AddressLine2 = entity.WorkInstruction.AddressLine2,
            AddressLine3 = entity.WorkInstruction.AddressLine3,
            City = entity.WorkInstruction.City,
            County = entity.WorkInstruction.County,
            Postcode = entity.WorkInstruction.Postcode,
            Job = entity.WorkInstruction.Job.JobTitle,
            StartDate = entity.StartDate,
            EndDate = entity.EndDate,
            ScheduleRow = 0
        };
    }
}
