﻿using FMConway.Services.Vehicles.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Schedules.Models
{
    public class ScheduleModel
    {
        public Guid ScheduleID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public WorkInstruction WorkInstruction { get; set; }  
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int ScheduleRow { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Schedule, ScheduleModel>> EntityToModel = entity => new ScheduleModel
        {
            ScheduleID = entity.SchedulePK,
            WorkInstructionID = entity.WorkInstructionFK,
            WorkInstruction = entity.WorkInstruction,
            StartDate = entity.StartDate,
            EndDate = entity.EndDate,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<ScheduleModel, Schedule> ModelToEntity = model => new Schedule
        {
            SchedulePK = model.ScheduleID,
            WorkInstructionFK = model.WorkInstructionID,
            StartDate = model.StartDate,
            EndDate = model.EndDate,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
