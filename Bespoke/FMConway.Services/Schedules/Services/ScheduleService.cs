﻿using FMConway.Services.Schedules.Models;
using FMConway.Services.ErrorLogging.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using System.Data.Entity;
using FMConway.Services.WorkInstructions.Extensions;
using FMConway.Services.WorkInstructions.Enums;
using FMConway.Services.Helpers;
using System.Transactions;
using FMConway.Services.Schedules.Interfaces;

namespace FMConway.Services.Schedules.Services
{
    public class ScheduleService : Service, FMConway.Services.Schedules.Interfaces.IScheduleService
    {
        UserContext _userContext;
        ErrorLoggingService _errorService;

        public ScheduleService(UserContext userContext, ErrorLoggingService errorService)
        {
            _userContext = userContext;
            _errorService = errorService;
        }

        public DateTime GetScheduleUpdatedDate(Guid schedulePK)
        {
            return Context.Schedules.Where(a => a.SchedulePK == schedulePK).Select(a => a.UpdatedDate).Single();
        }

        public bool IsWorkInstructionActiveForSchedule(Guid schedulePK)
        {
            return Context.Schedules.Where(a => a.SchedulePK == schedulePK).Select(a => a.WorkInstruction.IsActive).Single();
        }

        public ServiceResults<ScheduleResult> UpdateFromScheduler(Guid schedulePK, List<Guid> vehicles, List<Guid> operatives, bool confirmed)
        {
            var results = new ServiceResults<ScheduleResult>();

            if (!_userContext.IsAuthenticated)
            {
                throw new AuthenticationException();
            }

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var userDetails = _userContext.UserDetails;

                    var schedule = Context.Schedules.Where(a => a.SchedulePK == schedulePK).Single();
                    schedule.UpdatedByFK = userDetails.ID;
                    schedule.UpdatedDate = DateTime.Now;

                    var workInstruction = Context.WorkInstructions.Where(a => a.WorkInstructionPK == schedule.WorkInstructionFK).Single();

                    Context.ScheduleEmployees.RemoveRange(Context.ScheduleEmployees.Where(a => a.ScheduleFK == schedulePK));

                    if (operatives != null)
                    {
                        Context.ScheduleEmployees.AddRange(operatives.Select(o => new ScheduleEmployee
                        {
                            ScheduleEmployeePK = Guid.NewGuid(),
                            ScheduleFK = schedulePK,
                            EmployeeFK = o,
                            IsActive = true,
                            CreatedByFK = userDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByFK = userDetails.ID,
                            UpdatedDate = DateTime.Now
                        }));
                    }

                    Context.ScheduleVehicles.RemoveRange(Context.ScheduleVehicles.Where(x => x.ScheduleFK == schedulePK));

                    if (vehicles != null)
                    {
                        Context.ScheduleVehicles.AddRange(vehicles.Select(v => new ScheduleVehicle
                        {
                            ScheduleVehiclePK = Guid.NewGuid(),
                            ScheduleFK = schedulePK,
                            VehicleFK = v,
                            IsActive = true,
                            CreatedByID = userDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByID = userDetails.ID,
                            UpdatedDate = DateTime.Now
                        }));
                    }

                    workInstruction.IsScheduled = true;

                    SaveChanges();

                    var shift = Context.Schedules.Where(sc => sc.SchedulePK == schedule.SchedulePK).Select(sc => sc.WorkInstruction.JobPack.Shift).SingleOrDefault();
                    bool isNew = false;

                    if (shift == null)
                    {
                        shift = new Shift()
                        {
                            ShiftPK = Guid.NewGuid(),
                            ShiftComplete = false,
                            ShiftCompletionStatus = 0,
                            IsActive = true,
                            CreatedByID = userDetails.ID,
                            CreatedDate = DateTime.Now,
                            IsCancelled = false
                        };

                        isNew = true;
                    }

                    shift.IsConfirmed = confirmed;
                    shift.ShiftDate = schedule.StartDate;
                    shift.UpdatedByID = userDetails.ID;
                    shift.UpdatedDate = DateTime.Now;

                    SaveChanges();

                    if (confirmed)
                    {
                        if (shift.ShiftProgressInfoes.Count.Equals(0))
                        {
                            ShiftProgressInfo info = new ShiftProgressInfo();
                            info.ShiftFK = shift.ShiftPK;
                            info.ShiftProgressID = 1; // Not Started
                            info.Time = DateTime.Now;
                            info.CreatedDate = DateTime.Now;

                            shift.ShiftProgressInfoes.Add(info);

                            SaveChanges();
                        }
                    }

                    // Create ShiftStaff
                    var existingStaffs = Context.ShiftStaffs.Where(ss => ss.ShiftFK == shift.ShiftPK).ToList();

                    foreach (var shiftStaff in existingStaffs.Where(a => a.EmployeeFK != null && !operatives.Any(b => b == a.EmployeeFK)))
                    {
                        shiftStaff.IsActive = false;
                    }

                    foreach (var existingStaff in existingStaffs.Where(a => a.EmployeeFK != null).ToList())
                    {
                        var operativeExists = operatives.Where(a => a == existingStaff.EmployeeFK).Any();
                        if (operativeExists)
                        {
                            existingStaff.IsActive = true;
                        }
                    }

                    foreach (var operativeModel in operatives.Where(o => !existingStaffs.Select(es => es.EmployeeFK).Contains(o)))
                    {
                        var shiftStaff = new ShiftStaff();
                        shiftStaff.ShiftStaffPK = Guid.NewGuid();
                        shiftStaff.Shift = shift;
                        shiftStaff.EmployeeFK = operativeModel;
                        shiftStaff.NotOnShift = false;
                        shiftStaff.ReasonNotOn = "";
                        shiftStaff.IsActive = true;
                        shiftStaff.CreatedByID = userDetails.ID;
                        shiftStaff.CreatedDate = DateTime.Now;
                        shiftStaff.UpdatedByID = userDetails.ID;
                        shiftStaff.UpdatedDate = DateTime.Now;

                        Context.ShiftStaffs.Add(shiftStaff);
                    }

                    SaveChanges();

                    // Create ShiftVehicles
                    var existingVehicles = Context.ShiftVehicles.Where(sv => sv.ShiftFK == shift.ShiftPK).ToList();

                    foreach (var shiftVehicle in existingVehicles.Where(ev => !vehicles.Contains(ev.VehicleFK)))
                    {
                        shiftVehicle.IsActive = false;
                    }

                    foreach (var shiftVehicle in existingVehicles.Where(ev => vehicles.Contains(ev.VehicleFK)))
                    {
                        shiftVehicle.IsActive = true;
                    }

                    foreach (var vehiclePK in vehicles.Where(v => !existingVehicles.Select(ev => ev.VehicleFK).Contains(v)))
                    {
                        var shiftVehicle = new ShiftVehicle();
                        shiftVehicle.ShiftVehiclePK = Guid.NewGuid();
                        shiftVehicle.ShiftFK = shift.ShiftPK;
                        shiftVehicle.VehicleFK = vehiclePK;
                        shiftVehicle.EmployeeFK = Context.Employees.Where(a => operatives.Contains(a.EmployeePK) && a.VehicleFK == vehiclePK && a.IsActive).Select(a => (Guid?)a.EmployeePK).FirstOrDefault();
                        shiftVehicle.StartMileage = 0;
                        shiftVehicle.EndMileage = 0;
                        shiftVehicle.NotOnShift = false;
                        shiftVehicle.ReasonNotOnShift = "";
                        shiftVehicle.LoadingSheetCompleted = false;
                        shiftVehicle.IsActive = true;
                        shiftVehicle.CreatedByID = userDetails.ID;
                        shiftVehicle.CreatedDate = DateTime.Now;
                        shiftVehicle.UpdatedByID = userDetails.ID;
                        shiftVehicle.UpdatedDate = DateTime.Now;

                        Context.ShiftVehicles.Add(shiftVehicle);
                    }

                    SaveChanges();

                    if (isNew)
                    {
                        workInstruction.JobPack.Shift = shift;
                        workInstruction.JobPack.UpdatedByID = userDetails.ID;
                        workInstruction.JobPack.UpdatedDate = DateTime.Now;
                    }

                    SaveChanges();


                    if (workInstruction.StatusFK.Equals(new Guid("9265273E-C0CD-4EB7-933E-46689478C5F0"))) // Ready To Schedule
                    {
                        workInstruction.StatusFK = new Guid("CDD506A6-670B-41AE-AF9B-8D21F0326717"); // Scheduled
                    }

                    SaveChanges();

                    schedule.Shift = shift;

                    SaveChanges();

                    transaction.Commit();

                    results.Result = new ScheduleResult
                    {
                        UpdatedDate = schedule.UpdatedDate,
                    };
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    _errorService.CreateErrorLog("UpdateSchedule", e, schedulePK.ToString());
                    results.Errors.Add(e.Message);
                }

                return results;
            }
        }

        public ServiceResults<DateTime> UnconfirmSchedule(Guid scheduleID)
        {
            var results = new ServiceResults<DateTime>();

            var schedule = Context.Schedules.Where(a => a.SchedulePK == scheduleID).SingleOrDefault();
            if (schedule == null)
            {
                // schedule doesnt exist
                results.Errors.Add("schedule doesnt exist");
                return results;
            }

            var shift = schedule.Shift;
            if (shift == null)
            {
                // shift doesnt exist
                results.Errors.Add("shift doesnt exist");
                return results;
            }

            // check shift progress has "Not Started"
            if (shift.ShiftProgressInfoes.Count != 1 || shift.ShiftProgressInfoes.Any(a => a.ShiftProgressID != 1))
            {
                results.Errors.Add("shift has progressed beyond point of return");
                return results;
            }

            // reverse any operations done by confirming
            shift.IsConfirmed = false;

            Context.ShiftProgressInfoes.RemoveRange(shift.ShiftProgressInfoes.ToList());

            // update schedule
            schedule.UpdatedByFK = _userContext.UserDetails.ID;
            schedule.UpdatedDate = DateTime.Now;

            SaveChanges();

            results.Result = schedule.UpdatedDate;

            return results;
        }

        public List<ScheduleModel> GetActiveSchedules(DateTime weekStart)
        {
            DateTime weekEnd = weekStart.AddDays(7);
            return Context.Schedules.Where(x => (x.StartDate >= weekStart || x.StartDate <= weekEnd || x.EndDate >= weekStart || x.EndDate <= weekEnd) && x.IsActive).Select(ScheduleModel.EntityToModel).ToList();
        }

        public ScheduleModel Single(Guid id)
        {
            return Context.Schedules.Where(x => x.SchedulePK.Equals(id)).Select(ScheduleModel.EntityToModel).SingleOrDefault();
        }

        public List<ScheduleEmployeeModel> GetEmployees(Guid id)
        {
            return Context.ScheduleEmployees.Where(x => x.ScheduleFK.Equals(id)).Select(ScheduleEmployeeModel.EntityToModel).ToList();
        }

        public List<SchedulerModel> GetActiveSchedulesForWeek(DateTime weekStart, Guid? contractID,  Guid? depotID, List<string> selectedContracts,List<string> selectedCustomers)
        {
            var weekEnd = weekStart.AddDays(7);

            var query = Context.Schedules.Where(x =>
                x.IsActive &&
                x.StartDate >= weekStart &&
                x.StartDate <= weekEnd &&
                x.WorkInstruction.JobFK != null &&
                x.WorkInstruction.IsActive &&
                (
                    (x.ShiftFK == null && x.WorkInstruction.StatusFK != WorkInstructionStatusesEnum.Cancelled) ||
                    x.ShiftFK != null
                ));

            if (selectedContracts != null)
            {
                query = query.Where(a => selectedContracts.Contains(a.WorkInstruction.Job.Contract.ContractPK.ToString()));
            }

            if (selectedCustomers != null)
            {
                query = query.Where(a => selectedCustomers.Contains(a.WorkInstruction.Job.Contract.Customer.CustomerPK.ToString()));
            }

            if (depotID != null)
            {
                query = query.Where(a => a.WorkInstruction.DepotFK == depotID);
            }
    
            var schedules = query.Select(SchedulerModel.EntityToModel).ToList();

            return schedules;
        }

        public List<DayTotal> GetActiveSchedulesTotalsForWeek(DateTime weekStart, Guid? contractID, Guid? depotID, List<string> selectedContracts, List<string> selectedCustomers)
        {
            var weekEnd = weekStart.AddDays(7);

            var query = Context.Schedules.Where(x =>
                x.IsActive &&
                x.StartDate >= weekStart &&
                x.StartDate <= weekEnd &&
                x.WorkInstruction.JobFK != null &&
                x.WorkInstruction.IsActive &&
                (
                    (x.ShiftFK == null && x.WorkInstruction.StatusFK != WorkInstructionStatusesEnum.Cancelled) ||
                    x.ShiftFK != null
                ));

            if (selectedContracts != null)
            {
                query = query.Where(a => selectedContracts.Contains(a.WorkInstruction.Job.Contract.ContractPK.ToString()));
            }

            if (selectedCustomers != null)
            {
                query = query.Where(a => selectedCustomers.Contains(a.WorkInstruction.Job.Contract.Customer.CustomerPK.ToString()));
            }

            if (depotID != null)
            {
                query = query.Where(a => a.WorkInstruction.DepotFK == depotID);
            }
            var dayTotals = new List<DayTotal>();
            for (int i = 0;i <7;i++)
            {
                var dayTotal = new DayTotal();
                dayTotal.Day = weekStart.AddDays(i).Date;
                dayTotal.ScheduleCount = query.Where(w => DbFunctions.TruncateTime(w.StartDate) == dayTotal.Day).Count();
                dayTotals.Add(dayTotal);
            }

            return dayTotals;
        }


        public List<SchedulerModel> GetActiveSchedulesForDay(DateTime day, Guid? contractID, Guid? depotID, List<string> selectedContracts, List<string> selectedCustomers)
        {
            day = day.Date;
            var nextDay = day.AddDays(1);
            var query = Context.Schedules.Where(x =>
                x.IsActive &&
                x.StartDate >= day  && x.StartDate < nextDay  &&
                x.WorkInstruction.JobFK != null &&
                x.WorkInstruction.IsActive &&
                (
                    (x.ShiftFK == null && x.WorkInstruction.StatusFK != WorkInstructionStatusesEnum.Cancelled) ||
                    x.ShiftFK != null
                ));

            if (selectedContracts != null)
            {
                query = query.Where(a => selectedContracts.Contains(a.WorkInstruction.Job.Contract.ContractPK.ToString()));
            }

            if (selectedCustomers != null)
            {
                query = query.Where(a => selectedCustomers.Contains(a.WorkInstruction.Job.Contract.Customer.CustomerPK.ToString()));
            }

            if (depotID != null)
            {
                query = query.Where(a => a.WorkInstruction.DepotFK == depotID);
            }

            var schedules = query.Select(SchedulerModel.EntityToModel).ToList();

            return schedules;
        }

        public List<SchedulerModel> GetActiveSchedulesForDay(DateTime day, Guid? contractID)
        {
            if (contractID.HasValue)
            {
                return Context.Schedules.Where(x => 
                    x.IsActive &&
                    x.StartDate.Day.Equals(day.Day) &&
                    x.StartDate.Month.Equals(day.Month) &&
                    x.StartDate.Year.Equals(day.Year) &&
                    x.WorkInstruction.JobFK != null &&
                    x.WorkInstruction.Job.Contract.ContractPK.Equals(contractID.Value) && x.Shift != null)
                    .Select(SchedulerModel.EntityToModel)
                    .ToList();
            }
            else
            {
                return Context.Schedules.Where(x =>
                    x.IsActive &&
                    x.StartDate.Day.Equals(day.Day) &&
                    x.StartDate.Month.Equals(day.Month) &&
                    x.StartDate.Year.Equals(day.Year) &&
                    x.WorkInstruction.JobFK != null &&
                    x.Shift != null)
                    .Select(SchedulerModel.EntityToModel)
                    .ToList();
            }
        }

        public List<ProgressModel> GetShiftProgress(DateTime lastUpdate, Guid? contractID)
        {
            List<ProgressModel> progress = new List<ProgressModel>();

            DateTime day = DateTime.Now.Date;

            #region test code
            //DateTime weekStart = new DateTime(2017, 01, 30);

            //DateTime weekEnd = weekStart.AddDays(7);

            #endregion

            try
            {
                if (contractID.HasValue)
                {
                    progress = Context.Schedules.Where(x => x.IsActive && x.WorkInstruction.Job.Contract.ContractPK.Equals(contractID.Value) &&
                        x.WorkInstruction.JobPack.Shift.ShiftProgressInfoes.Any(p => p.CreatedDate > lastUpdate)).
                        Select(x => new ProgressModel
                        {
                            ScheduleID = x.SchedulePK,
                            ShiftProgress = x.WorkInstruction.JobPack.Shift.ShiftProgressInfoes.OrderByDescending(p => p.Time).Take(1).
                                Select(p => p.ShiftProgress.Progress).FirstOrDefault(),
                            CompletedDate = x.WorkInstruction.JobPack.Shift.ShiftProgressInfoes.OrderByDescending(p => p.Time).Take(1).
                                Select(p => p.Time).FirstOrDefault(),
                            CompletedTime = ""
                        }).ToList();
                }
                else
                {
                    progress = Context.Schedules.Where(x => x.IsActive &&
                        x.WorkInstruction.JobPack.Shift.ShiftProgressInfoes.Any(p => p.CreatedDate > lastUpdate)).
                        Select(x => new ProgressModel
                        {
                            ScheduleID = x.SchedulePK,
                            ShiftProgress = x.WorkInstruction.JobPack.Shift.ShiftProgressInfoes.OrderByDescending(p => p.Time).Take(1).
                                Select(p => p.ShiftProgress.Progress).FirstOrDefault(),
                            CompletedDate = x.WorkInstruction.JobPack.Shift.ShiftProgressInfoes.OrderByDescending(p => p.Time).Take(1).
                                Select(p => p.Time).FirstOrDefault(),
                            CompletedTime = ""
                        }).ToList();
                }
            }
            catch (Exception e)
            {
                _errorService.CreateErrorLog("GetShiftProgress", e, lastUpdate.ToString());
            }

            foreach (ProgressModel s in progress)
            {
                s.ShiftProgress = s.ShiftProgress?.Replace(" ", "");

                s.CompletedTime = (s.CompletedDate.TimeOfDay.Hours < 10 ? "0" + s.CompletedDate.TimeOfDay.Hours.ToString() : s.CompletedDate.TimeOfDay.Hours.ToString())
                    + ":" + (s.CompletedDate.TimeOfDay.Minutes < 10 ? "0" + s.CompletedDate.TimeOfDay.Minutes.ToString() : s.CompletedDate.TimeOfDay.Minutes.ToString());
            }

            return progress;
        }

        public PreDropCheckModel PreDropChecks(Guid id, bool isOperative, DateTime date)
        {
            var model = new PreDropCheckModel();

            if (isOperative)
            {
                model.IsEmployeeAlreadyScheduled = Context.ScheduleEmployees.Where(a => a.IsActive && a.EmployeeFK == id && DbFunctions.TruncateTime(a.Schedule.StartDate) == date).Any();
            }
            else
            {
                model.IsVehicleAlreadyScheduled = Context.ScheduleVehicles.Where(a => a.IsActive && a.VehicleFK == id && DbFunctions.TruncateTime(a.Schedule.StartDate) == date).Any();
            }

            return model;
        }

        public ServiceResults SaveOrderNumber(Guid workInstructionID, int orderNumber)
        {
            var results = new ServiceResults();

            try
            {
                var workInstruction = Context.WorkInstructions.Where(a => a.WorkInstructionPK == workInstructionID).Single();
                workInstruction.OrderNumber = orderNumber;

                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                _errorService.CreateErrorLog(nameof(SaveOrderNumber), ex, workInstructionID.ToString());
                results.Errors.Add(GeneralErrorMessage);
            }

            return results;
        }

    }
}
