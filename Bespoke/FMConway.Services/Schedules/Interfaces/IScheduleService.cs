﻿using FMConway.Services.Helpers;
using FMConway.Services.Schedules.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Schedules.Interfaces
{
    public interface IScheduleService : IService
    {
        DateTime GetScheduleUpdatedDate(Guid schedulePK);
        bool IsWorkInstructionActiveForSchedule(Guid schedulePK);

        ServiceResults<ScheduleResult> UpdateFromScheduler(Guid schedulePK, List<Guid> vehicles, List<Guid> operatives, bool confirmed);
        ServiceResults<DateTime> UnconfirmSchedule(Guid schedulePK);

        List<ScheduleEmployeeModel> GetEmployees(Guid id);
        List<ScheduleModel> GetActiveSchedules(DateTime weekStart);
        ScheduleModel Single(Guid id);
        List<SchedulerModel> GetActiveSchedulesForWeek(DateTime weekStart, Guid? contractID, Guid? depotID, List<string> selectedContracts,List<string> selectedCustomers);
        List<SchedulerModel> GetActiveSchedulesForDay(DateTime day, Guid? contractID, Guid? depotID, List<string> selectedContracts, List<string> selectedCustomers);
        List<DayTotal> GetActiveSchedulesTotalsForWeek(DateTime weekStart, Guid? contractID, Guid? depotID, List<string> selectedContracts, List<string> selectedCustomers);
        List<SchedulerModel> GetActiveSchedulesForDay(DateTime day, Guid? contractID);
        List<ProgressModel> GetShiftProgress(DateTime lastUpdate, Guid? contractID);
        PreDropCheckModel PreDropChecks(Guid id, bool isOperative, DateTime date);
        ServiceResults SaveOrderNumber(Guid workInstructionID, int orderNumber);
    }

    public class ScheduleResult
    {
        public DateTime UpdatedDate { get; set; }
    }
}
