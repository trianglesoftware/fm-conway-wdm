﻿using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.UserProfiles.Models
{
    public class RoleModel
    {
        public Guid RoleID { get; set; }
        public string Name { get; set; }

        public static Expression<Func<Role, RoleModel>> EntityToModel = a => new RoleModel
        {
            RoleID = a.RolePK,
            Name = a.Name,
        };
    }
}
