﻿using FMConway.Services.Customers.Models;
using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.UserProfiles.Models
{
    public class UserProfileModel
    {
        public Guid UserID { get; set; }

        public Guid? EmployeeID { get; set; }
        public EmployeeModel Employee { get; set; }
        public Guid? CustomerID { get; set; }
        public CustomerModel Customer { get; set; }

        public UserTypeModel UserType { get; set; }

        public static Expression<Func<User, UserProfileModel>> EntityToModel = entity => new UserProfileModel()
        {
            UserID = entity.UserPK,
            EmployeeID = entity.UserProfile.EmployeeFK,
            Employee = entity.UserProfile.EmployeeFK != null ? new EmployeeModel
            {
                EmployeeName = entity.UserProfile.Employee.EmployeeName
            } : null,
            CustomerID = entity.UserProfile.CustomerFK,
            Customer = entity.UserProfile.CustomerFK != null ? new CustomerModel
            {
                CustomerName = entity.UserProfile.Customer.CustomerName,
            } : null,
            UserType = entity.UserProfile.EmployeeFK != null ? new UserTypeModel
            {
                ID = entity.UserProfile.Employee.EmployeeTypeFK,
                UserType = entity.UserProfile.Employee.EmployeeType.EmployeeType1,
                IsContractManager = entity.UserProfile.Employee.EmployeeType.IsContractManager
            } : null
        };
    }
}
