﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.UserProfiles.Models
{
    public class UserTypeModel
    {
        public Guid ID { get; set; }
        public string UserType { get; set; }
        public bool IsContractManager { get; set; }
        public bool IsOperationManager { get; set; }

        public static Expression<Func<UserType, UserTypeModel>> EntityToModel = entity => new UserTypeModel()
        {
            ID = entity.UserTypePK,
            UserType = entity.UserType1,
            IsContractManager = entity.IsContractManager,
            IsOperationManager = entity.IsOperationManager,
        };
    }
}
