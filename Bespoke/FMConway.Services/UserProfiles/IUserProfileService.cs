﻿using FMConway.Services.UserProfiles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.UserProfiles
{
    public interface IUserProfileService : IService
    {
        UserProfileModel GetUserProfile(Guid userID);
        void CreateUserProfile(UserProfileModel model);
        void UpdateUserProfile(UserProfileModel model);
        bool IsEmployeeAvailable(Guid employeeID, Guid excludeUserID);
        bool IsCustomerAvailable(Guid customerID, Guid excludeUserID);
        List<RoleModel> GetAllRoles(bool excludeCustomer);
    }
}
