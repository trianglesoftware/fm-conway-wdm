﻿using FMConway.Services.UserProfiles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.UserProfiles
{
    public class UserProfileService : Service, FMConway.Services.UserProfiles.IUserProfileService
    {
        UserContext _userContext;

        public UserProfileService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public UserProfileModel GetUserProfile(Guid userID)
        {
            using (var context = new FMConwayEntities())
            {
                return context.Users.Where(u => u.UserPK == userID).Select(UserProfileModel.EntityToModel).Single();
            }
        }

        public void CreateUserProfile(UserProfileModel model)
        {
            var userProfile = Context.UserProfiles.Where(a => a.UserFK == model.UserID).SingleOrDefault();

            if (userProfile == null)
            {
                userProfile = new UserProfile();
                userProfile.UserFK = model.UserID;
                Context.UserProfiles.Add(userProfile);
            }

            userProfile.EmployeeFK = model.EmployeeID;
            userProfile.CustomerFK = model.CustomerID;

            SaveChanges();
        }

        public void UpdateUserProfile(UserProfileModel model)
        {
            var userProfile = Context.UserProfiles.Where(a => a.UserFK == model.UserID).SingleOrDefault();

            if (userProfile == null)
            {
                userProfile = new UserProfile();
                userProfile.UserFK = model.UserID;
                Context.UserProfiles.Add(userProfile);
            }

            userProfile.EmployeeFK = model.EmployeeID;
            userProfile.CustomerFK = model.CustomerID;

            SaveChanges();
        }

        public bool IsEmployeeAvailable(Guid employeeID, Guid excludeUserID)
        {
            return !Context.Users.Where(a => a.UserProfile.EmployeeFK == employeeID && a.UserPK != excludeUserID).Any();
        }

        public bool IsCustomerAvailable(Guid customerID, Guid excludeUserID)
        {
            return !Context.Users.Where(a => a.UserProfile.CustomerFK == customerID && a.UserPK != excludeUserID).Any();
        }

        public List<RoleModel> GetAllRoles(bool excludeCustomer)
        {
            return Context.Roles
                .Where(a => !excludeCustomer || a.Name != "Customer")
                .Select(RoleModel.EntityToModel).ToList();
        }
    }
}
