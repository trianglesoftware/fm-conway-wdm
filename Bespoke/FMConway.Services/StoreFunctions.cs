﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services
{
    public class StoreFunctions
    {
        [DbFunction("FMConwayModel.Store", "JobAddressSpaceDelim")]
        public static string JobAddressSpaceDelim(Guid jobPK)
        {
            throw new NotSupportedException("Direct calls are not supported");
        }

        [DbFunction("FMConwayModel.Store", "JobAddressCommaDelim")]
        public static string JobAddressCommaDelim(Guid jobPK)
        {
            throw new NotSupportedException("Direct calls are not supported");
        }

        [DbFunction("FMConwayModel.Store", "JobAddressNewLineDelim")]
        public static string JobAddressNewLineDelim(Guid jobPK)
        {
            throw new NotSupportedException("Direct calls are not supported");
        }

        [DbFunction("FMConwayModel.Store", "UserEmployeeName")]
        public static string UserEmployeeName(Guid userPK)
        {
            throw new NotSupportedException("Direct calls are not supported");
        }

        [DbFunction("FMConwayModel.Store", "WorkInstructionAddressSpaceDelim")]
        public static string WorkInstructionAddressSpaceDelim(Guid workInstructionPK)
        {
            throw new NotSupportedException("Direct calls are not supported");
        }

        [DbFunction("FMConwayModel.Store", "WorkInstructionAddressCommaDelim")]
        public static string WorkInstructionAddressCommaDelim(Guid workInstructionPK)
        {
            throw new NotSupportedException("Direct calls are not supported");
        }

        [DbFunction("FMConwayModel.Store", "WorkInstructionAddressNewLineDelim")]
        public static string WorkInstructionAddressNewLineDelim(Guid workInstructionPK)
        {
            throw new NotSupportedException("Direct calls are not supported");
        }
    }
}
