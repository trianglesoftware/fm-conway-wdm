﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.DepotAreas.Models
{
    public class DepotAreaLookup : LookupBase<Guid>
    {
        public static Expression<Func<DepotArea, DepotAreaLookup>> EntityToModel = entity => new DepotAreaLookup()
        {
            Key = entity.Name,
            Value = entity.DepotAreaPK
        };
    }
}
