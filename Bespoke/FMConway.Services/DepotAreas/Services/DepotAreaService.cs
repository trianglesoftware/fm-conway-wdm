﻿using FMConway.Services.DepotAreas.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.DepotAreas.Extensions;

namespace FMConway.Services.DepotAreas.Services
{
    public class DepotAreaService : Service, FMConway.Services.DepotAreas.Interfaces.IDepotAreaService
    {
        UserContext _userContext;

        public DepotAreaService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(DepotAreaModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.DepotAreaID = Guid.NewGuid();

            model.Name = model.Name == null ? "" : model.Name;

            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.DepotAreas.Add(DepotAreaModel.ModelToEntity(model));

            SaveChanges();

            return model.DepotAreaID;
        }

        public void Update(DepotAreaModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DepotArea current = Context.DepotAreas.Where(x => x.DepotAreaPK.Equals(model.DepotAreaID)).Single();

            current.Name = model.Name == null ? "" : model.Name;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public DepotAreaModel Single(Guid ID)
        {
            return Context.DepotAreas.Where(a => a.DepotAreaPK.Equals(ID)).Select(DepotAreaModel.EntityToModel).SingleOrDefault();
        }

        public List<DepotAreaModel> GetAllActiveDepotAreas()
        {
            return Context.DepotAreas.ActiveDepotAreas().Select(DepotAreaModel.EntityToModel).ToList();
        }

        public PagedResults<DepotAreaModel> GetActiveDepotAreasPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.DepotAreas.ActiveDepotAreas().OrderBy(o => o.Name).DepotAreaOverviewQuery(query).Select(DepotAreaModel.EntityToModel);

            return PagedResults<DepotAreaModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<DepotAreaModel> GetAllInactiveDepotAreas()
        {
            return Context.DepotAreas.InactiveDepotAreas().Select(DepotAreaModel.EntityToModel).ToList();
        }

        public PagedResults<DepotAreaModel> GetInactiveDepotAreasPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.DepotAreas.InactiveDepotAreas().OrderBy(o => o.Name).DepotAreaOverviewQuery(query).Select(DepotAreaModel.EntityToModel);

            return PagedResults<DepotAreaModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<DepotAreaLookup> GetPagedActiveDepotAreasLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.DepotAreas.ActiveDepotAreas().OrderBy(o => o.Name).DepotAreaOverviewQuery(query).Select(DepotAreaLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<DepotAreaLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
