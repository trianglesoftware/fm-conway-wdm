﻿using FMConway.Services.DepotAreas.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.DepotAreas.Interfaces
{
    public interface IDepotAreaService : IService
    {
        Guid Create(DepotAreaModel model);
        void Update(DepotAreaModel model);
        DepotAreaModel Single(Guid ID);

        List<DepotAreaModel> GetAllActiveDepotAreas();
        PagedResults<DepotAreaModel> GetActiveDepotAreasPaged(int rowCount, int page, string query);

        List<DepotAreaModel> GetAllInactiveDepotAreas();
        PagedResults<DepotAreaModel> GetInactiveDepotAreasPaged(int rowCount, int page, string query);

        PagedResults<DepotAreaLookup> GetPagedActiveDepotAreasLookup(int rowCount, int page, string query);
    }
}
