﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.DepotAreas.Extensions
{
    public static class DepotAreaExtensions
    {
        public static IQueryable<DepotArea> ActiveDepotAreas(this IQueryable<DepotArea> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<DepotArea> InactiveDepotAreas(this IQueryable<DepotArea> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<DepotArea> DepotAreaOverviewQuery(this IQueryable<DepotArea> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
