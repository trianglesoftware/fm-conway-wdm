﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Depots.Models
{
    public class DepotLookup : LookupBase<Guid>
    {
        public static Expression<Func<Depot, DepotLookup>> EntityToModel = entity => new DepotLookup()
        {
            Key = entity.DepotName,
            Value = entity.DepotPK
        };
    }
}
