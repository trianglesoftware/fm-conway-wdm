﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.Depots.Models
{
    public class DepotModel
    {
        public Guid DepotID { get; set; }
        [Required(ErrorMessage = "Depot Area is required")]
        public Guid DepotAreaID { get; set; }
        public string DepotAreaLookup { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string DepotName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Depot, DepotModel>> EntityToModel = entity => new DepotModel
        {
            DepotID = entity.DepotPK,
            DepotAreaID = entity.DepotAreaFK,
            DepotAreaLookup = entity.DepotArea.Name,
            DepotName = entity.DepotName,
            AddressLine1 = entity.AddressLine1,
            AddressLine2 = entity.AddressLine2,
            AddressLine3 = entity.AddressLine3,
            City = entity.City,
            County = entity.County,
            Postcode = entity.Postcode,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<DepotModel, Depot> ModelToEntity = model => new Depot
        {
            DepotPK = model.DepotID,
            DepotAreaFK = model.DepotAreaID,
            DepotName = model.DepotName,
            AddressLine1 = model.AddressLine1,
            AddressLine2 = model.AddressLine2,
            AddressLine3 = model.AddressLine3,
            City = model.City,
            County = model.County,
            Postcode = model.Postcode,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
