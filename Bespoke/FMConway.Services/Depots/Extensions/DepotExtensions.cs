﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Depots.Extensions
{
    public static class DepotExtensions
    {
        public static IQueryable<Depot> ActiveDepots(this IQueryable<Depot> model)
        {
            return model.Where(a => a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Depot> InactiveDepots(this IQueryable<Depot> model)
        {
            return model.Where(a => !a.IsActive && !a.IsHidden);
        }

        public static IQueryable<Depot> DepotOverviewQuery(this IQueryable<Depot> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.DepotName.Contains(query) ||
                                          bq.AddressLine1.Contains(query) ||
                                          bq.AddressLine2.Contains(query) ||
                                          bq.AddressLine3.Contains(query) ||
                                          bq.City.Contains(query) ||
                                          bq.County.Contains(query) ||
                                          bq.Postcode.Contains(query));
            }

            return model;
        }
    }
}
