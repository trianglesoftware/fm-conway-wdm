﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Depots.Interfaces
{
    public interface IDepotService : IService
    {
        Guid Create(DepotModel model);
        void Update(DepotModel model);
        DepotModel Single(Guid ID);

        List<DepotModel> GetAllActiveDepots();
        PagedResults<DepotModel> GetActiveDepotsPaged(int rowCount, int page, string query);

        List<DepotModel> GetAllInactiveDepots();
        PagedResults<DepotModel> GetInactiveDepotsPaged(int rowCount, int page, string query);

        PagedResults<DepotLookup> GetPagedActiveDepotsLookup(int rowCount, int page, string query);
    }
}
