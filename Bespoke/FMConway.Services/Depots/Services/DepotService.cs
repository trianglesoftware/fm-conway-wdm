﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Depots.Extensions;

namespace FMConway.Services.Depots.Services
{
    public class DepotService : Service, FMConway.Services.Depots.Interfaces.IDepotService
    {
        UserContext _userContext;

        public DepotService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(DepotModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.DepotID = Guid.NewGuid();
            model.DepotAreaID = model.DepotAreaID;

            model.AddressLine1 = model.AddressLine1 == null ? "" : model.AddressLine1;
            model.AddressLine2 = model.AddressLine2 == null ? "" : model.AddressLine2;
            model.AddressLine3 = model.AddressLine3 == null ? "" : model.AddressLine3;
            model.City = model.City == null ? "" : model.City;
            model.County = model.County == null ? "" : model.County;
            model.Postcode = model.Postcode == null ? "" : model.Postcode;

            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Depots.Add(DepotModel.ModelToEntity(model));

            SaveChanges();

            return model.DepotID;
        }

        public void Update(DepotModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Depot current = Context.Depots.Where(x => x.DepotPK.Equals(model.DepotID)).Single();

            current.DepotAreaFK = model.DepotAreaID;
            current.DepotName = model.DepotName == null ? "" : model.DepotName;
            current.AddressLine1 = model.AddressLine1 == null ? "" : model.AddressLine1;
            current.AddressLine2 = model.AddressLine2 == null ? "" : model.AddressLine2;
            current.AddressLine3 = model.AddressLine3 == null ? "" : model.AddressLine3;
            current.City = model.City == null ? "" : model.City;
            current.County = model.County == null ? "" : model.County;
            current.Postcode = model.Postcode == null ? "" : model.Postcode;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public DepotModel Single(Guid ID)
        {
            return Context.Depots.Where(a => a.DepotPK.Equals(ID)).Select(DepotModel.EntityToModel).SingleOrDefault();
        }

        public List<DepotModel> GetAllActiveDepots()
        {
            return Context.Depots.ActiveDepots().Select(DepotModel.EntityToModel).ToList();
        }

        public PagedResults<DepotModel> GetActiveDepotsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Depots.ActiveDepots().OrderBy(o => o.DepotName).DepotOverviewQuery(query).Select(DepotModel.EntityToModel);

            return PagedResults<DepotModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<DepotModel> GetAllInactiveDepots()
        {
            return Context.Depots.InactiveDepots().Select(DepotModel.EntityToModel).ToList();
        }

        public PagedResults<DepotModel> GetInactiveDepotsPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Depots.InactiveDepots().OrderBy(o => o.DepotName).DepotOverviewQuery(query).Select(DepotModel.EntityToModel);

            return PagedResults<DepotModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<DepotLookup> GetPagedActiveDepotsLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Depots.ActiveDepots().OrderBy(o => o.DepotName).DepotOverviewQuery(query).Select(DepotLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<DepotLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
