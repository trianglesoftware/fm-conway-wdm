﻿using FMConway.Services.Checklists.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Checklists.Extensions;
using FMConway.Services.Contracts.Models;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.VehicleTypes.Models;
using FMConway.Services.Helpers;
using FMConway.Services.WorkInstructionDetails.Models;

namespace FMConway.Services.Checklists.Services
{
    public class ChecklistService : Service, FMConway.Services.Checklists.Interfaces.IChecklistService
    {
        UserContext _userContext;

        public ChecklistService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<ChecklistModel> GetActiveChecklists()
        {
            return Context.Checklists.Where(a => a.IsActive).Select(ChecklistModel.EntityToModel).OrderBy(a => a.ChecklistName).ToList();
        }

        public List<ChecklistModel> GetInactiveChecklists()
        {
            return Context.Checklists.Where(a => !a.IsActive).Select(ChecklistModel.EntityToModel).OrderBy(a => a.ChecklistName).ToList();
        }

        public ServiceResults<Guid> Create(ChecklistModel model)
        {
            var serviceResults = new ServiceResults<Guid>();

            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.ChecklistID = Guid.NewGuid();

            switch (model.ChecklistType)
            {
                case "Contract":
                    model.IsContractChecklist = true;
                    break;
                case "JobType":
                    model.IsJobTypeChecklist = true;
                    break;
                case "VehicleType":
                    model.IsVehicleTypeChecklist = true;
                    break;
                case "Maintenance":
                    model.IsMaintenanceChecklist = true;
                    break;
                case "Signature":
                    model.IsSignatureChecklist = true;
                    break;
                case "HighSpeed":
                    model.IsHighSpeedChecklist = true;
                    break;
                case "12C":
                    model.Is12cChecklist = true;
                    break;
                case "LowSpeed":
                    model.IsLowSpeedChecklist = true;
                    break;
                case "PreHire":
                    model.IsPreHireChecklist = true;
                    break;
                case "Vms":
                    model.IsVmsChecklist = true;
                    break;
                case "Battery":
                    model.IsBatteryChecklist = true;
                    break;
                case "PreHireBattery":
                    model.IsPreHireBatteryChecklist = true;
                    break;
                case "PostHireBattery":
                    model.IsPostHireBatteryChecklist = true;
                    break;
                case "JettingPermitToWork":
                    model.IsJettingPermitToWorkChecklist = true;
                    break;
                case "JettingRiskAssessment":
                    model.IsJettingRiskAssessmentChecklist = true;
                    break;
                case "TankeringPermitToWork":
                    model.IsTankeringPermitToWorkChecklist = true;
                    break;
                case "TankeringRiskAssessment":
                    model.IsTankeringRiskAssessmentChecklist = true;
                    break;
                case "CCTVRiskAssessment":
                    model.IsCCTVRiskAssessmentChecklist = true;
                    break;
                case "CyclicalWorksRiskAssessment":
                    model.IsCyclicalWorksRiskAssessmentChecklist = true;
                    break;
                default:
                    break;
            }

            if (model.IsHighSpeedChecklist)
            {
                var highSpeedChecklistExists = Context.Checklists.Where(a => a.IsHighSpeedChecklist && a.IsActive).Any();
                if (highSpeedChecklistExists)
                {
                    serviceResults.Errors.Add("A high speed checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.Is12cChecklist)
            {
                var _12cChecklistExists = Context.Checklists.Where(a => a.Is12cChecklist && a.IsActive).Any();
                if (_12cChecklistExists)
                {
                    serviceResults.Errors.Add("A 12C Works checklist already exists. De-activate it in order to create a new one");
                }
            }

            if (model.IsLowSpeedChecklist)
            {
                var lowSpeedChecklistExists = Context.Checklists.Where(a => a.IsLowSpeedChecklist && a.IsActive).Any();
                if (lowSpeedChecklistExists)
                {
                    serviceResults.Errors.Add("A low speed checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsPreHireChecklist)
            {
                var preHireChecklistExists = Context.Checklists.Where(a => a.IsPreHireChecklist && a.IsActive).Any();
                if (preHireChecklistExists)
                {
                    serviceResults.Errors.Add("A pre hire checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsVmsChecklist)
            {
                var vmsChecklistExists = Context.Checklists.Where(a => a.IsVmsChecklist && a.IsActive).Any();
                if (vmsChecklistExists)
                {
                    serviceResults.Errors.Add("A vms checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsMaintenanceChecklist)
            {
                var batteryChecklistExists = Context.Checklists.Where(a => a.IsBatteryChecklist && a.IsActive).Any();
                if (batteryChecklistExists)
                {
                    serviceResults.Errors.Add("A maintenance checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsBatteryChecklist)
            {
                var batteryChecklistExists = Context.Checklists.Where(a => a.IsBatteryChecklist && a.IsActive).Any();
                if (batteryChecklistExists)
                {
                    serviceResults.Errors.Add("A battery POW checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsPreHireBatteryChecklist)
            {
                var preBatteryChecklistExists = Context.Checklists.Where(a => a.IsPreHireBatteryChecklist && a.IsActive).Any();
                if (preBatteryChecklistExists)
                {
                    serviceResults.Errors.Add("A pre-hire battery checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsPostHireBatteryChecklist)
            {
                var postBatteryChecklistExists = Context.Checklists.Where(a => a.IsPostHireBatteryChecklist && a.IsActive).Any();
                if (postBatteryChecklistExists)
                {
                    serviceResults.Errors.Add("A post-hire battery checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsJettingPermitToWorkChecklist)
            {
                var jettingPermitToWorkChecklistExists = Context.Checklists.Where(a => a.IsJettingPermitToWorkChecklist && a.IsActive).Any();
                if (jettingPermitToWorkChecklistExists)
                {
                    serviceResults.Errors.Add("A jetting permit to work checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsJettingRiskAssessmentChecklist)
            {
                var jettingRiskAssessmentChecklistExists = Context.Checklists.Where(a => a.IsJettingRiskAssessmentChecklist && a.IsActive).Any();
                if (jettingRiskAssessmentChecklistExists)
                {
                    serviceResults.Errors.Add("A jetting risk assessment checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsTankeringPermitToWorkChecklist)
            {
                var tankeringPermitToWorkChecklistExists = Context.Checklists.Where(a => a.IsTankeringPermitToWorkChecklist && a.IsActive).Any();
                if (tankeringPermitToWorkChecklistExists)
                {
                    serviceResults.Errors.Add("A tankering permit to work checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsTankeringRiskAssessmentChecklist)
            {
                var tankeringRiskAssessmentChecklistExists = Context.Checklists.Where(a => a.IsTankeringRiskAssessmentChecklist && a.IsActive).Any();
                if (tankeringRiskAssessmentChecklistExists)
                {
                    serviceResults.Errors.Add("A tankering risk assessment checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsCCTVRiskAssessmentChecklist)
            {
                var cctvRiskAssessmentChecklistExists = Context.Checklists.Where(a => a.IsCCTVRiskAssessmentChecklist && a.IsActive).Any();
                if (cctvRiskAssessmentChecklistExists)
                {
                    serviceResults.Errors.Add("A cctv risk assessment checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (model.IsCyclicalWorksRiskAssessmentChecklist)
            {
                var cyclicalWorksRiskAssessmentChecklistExists = Context.Checklists.Where(a => a.IsJettingNewRiskAssessmentChecklist && a.IsActive).Any();
                if (cyclicalWorksRiskAssessmentChecklistExists)
                {
                    serviceResults.Errors.Add("A Cyclical Works risk assessment checklist already exists. De-activate it in order to create a new one.");
                }
            }

            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.Checklists.Add(ChecklistModel.ModelToEntity(model));

            if (!serviceResults.Errors.Any())
            {
                SaveChanges();
            }

            serviceResults.Result = model.ChecklistID;

            return serviceResults;
        }

        public ServiceResults Update(ChecklistModel model)
        {
            var serviceResults = new ServiceResults();

            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Checklist current = Context.Checklists.Where(x => x.ChecklistPK.Equals(model.ChecklistID)).SingleOrDefault();

            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.ChecklistName = model.ChecklistName == null ? "" : model.ChecklistName;

            current.IsContractChecklist = model.ChecklistType == "Contract";
            current.IsJobTypeChecklist = model.ChecklistType == "JobType";
            current.IsVehicleTypeChecklist = model.ChecklistType == "VehicleType";
            current.IsMaintenanceChecklist = model.ChecklistType == "Maintenance";
            current.IsSignatureChecklist = model.ChecklistType == "Signature";
            current.IsHighSpeedChecklist = model.ChecklistType == "HighSpeed";  // 12 AB
            current.Is12cChecklist = model.ChecklistType == "12C";              // 12 C
            current.IsLowSpeedChecklist = model.ChecklistType == "LowSpeed";    // 12 D
            
            current.IsPreHireChecklist = model.ChecklistType == "PreHire";
            current.IsVmsChecklist = model.ChecklistType == "Vms";
            current.IsBatteryChecklist = model.ChecklistType == "Battery";  //battery POW
            current.IsPreHireBatteryChecklist = model.ChecklistType == "PreHireBattery";
            current.IsPostHireBatteryChecklist = model.ChecklistType == "PostHireBattery";
            current.IsJettingNewRiskAssessmentChecklist = model.ChecklistType == "CyclicalWorksRiskAssessment";

            if (current.IsHighSpeedChecklist)
            {
                var highSpeedChecklistExists = Context.Checklists.Where(a => a.IsHighSpeedChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (highSpeedChecklistExists)
                {
                    serviceResults.Errors.Add("A high speed checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (current.Is12cChecklist)
            {
                var _12cChecklistExists = Context.Checklists.Where(a => a.Is12cChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (_12cChecklistExists)
                {
                    serviceResults.Errors.Add("A 12C Works checklist already exists. De-activate it in order to create a new one");
                }
            }

            if (current.IsLowSpeedChecklist)
            {
                var lowSpeedChecklistExists = Context.Checklists.Where(a => a.IsLowSpeedChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (lowSpeedChecklistExists)
                {
                    serviceResults.Errors.Add("A low speed checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (current.IsPreHireChecklist)
            {
                var preHireChecklistExists = Context.Checklists.Where(a => a.IsPreHireChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (preHireChecklistExists)
                {
                    serviceResults.Errors.Add("A pre hire checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (current.IsVmsChecklist)
            {
                var vmsChecklistExists = Context.Checklists.Where(a => a.IsVmsChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (vmsChecklistExists)
                {
                    serviceResults.Errors.Add("A vms checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (current.IsMaintenanceChecklist)
            {
                var maintenanceChecklistExists = Context.Checklists.Where(a => a.IsMaintenanceChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (maintenanceChecklistExists)
                {
                    serviceResults.Errors.Add("A maintenance checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (current.IsBatteryChecklist)
            {
                var batteryChecklistExists = Context.Checklists.Where(a => a.IsBatteryChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (batteryChecklistExists)
                {
                    serviceResults.Errors.Add("A battery POW checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (current.IsPreHireBatteryChecklist)
            {
                var preBatteryChecklistExists = Context.Checklists.Where(a => a.IsPreHireBatteryChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (preBatteryChecklistExists)
                {
                    serviceResults.Errors.Add("A pre-hire battery checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (current.IsPostHireBatteryChecklist)
            {
                var postBatteryChecklistExists = Context.Checklists.Where(a => a.IsPostHireBatteryChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (postBatteryChecklistExists)
                {
                    serviceResults.Errors.Add("A post-hire battery checklist already exists. De-activate it in order to create a new one.");
                }
            }

            if (current.IsJettingNewRiskAssessmentChecklist)
            {
                var cyclicalWorksRiskAssessmentChecklistExists = Context.Checklists.Where(a => a.IsJettingNewRiskAssessmentChecklist && a.IsActive && a.ChecklistPK != current.ChecklistPK).Any();
                if (cyclicalWorksRiskAssessmentChecklistExists)
                {
                    serviceResults.Errors.Add("A Cyclical Works risk assessment checklist already exists. De-activate it in order to create a new one.");
                }
            }

            current.CompleteAtDepot = model.CompleteAtDepot;
            current.StartOfWork = model.StartOfWork;

            // bypass validation errors if set to inactive
            if (!current.IsActive)
            {
                serviceResults.Errors.Clear();
            }

            if (!serviceResults.Errors.Any())
            {
                SaveChanges();
            }

            return serviceResults;
        }

        public ChecklistModel Single(Guid id)
        {
            return Context.Checklists.Where(a => a.ChecklistPK.Equals(id)).Select(ChecklistModel.EntityToModel).SingleOrDefault();
        }

        public List<ContractChecklistModel> Get(Guid contractID)
        {
            return Context.ContractTenderChecklists.Where(a => a.ContractFK.Equals(contractID)).Select(ContractChecklistModel.EntityToModel).ToList();
        }

        //public List<WorkInstructionChecklist> GetForWI(Guid workInstructionID)
        //{
        //    return Context.WorkInstructionChecklists.Where(x => x.WorkInstructionFK.Equals(workInstructionID)).ToList();
        //}

        public List<JobTypeChecklistModel> GetJobTypeChecklists(Guid jobTypeID)
        {
            return Context.JobTypeChecklists.Where(a => a.JobTypeFK.Equals(jobTypeID)).Select(JobTypeChecklistModel.EntityToModel).ToList();
        }

        public List<VehicleTypeChecklistModel> GetVehicleTypeChecklists(Guid vehicleTypeID)
        {
            return Context.VehicleTypeChecklists.Where(a => a.VehicleTypeFK.Equals(vehicleTypeID)).Select(VehicleTypeChecklistModel.EntityToModel).ToList();
        }

        public PagedResults<ChecklistModel> GetActiveTenderChecklists(int rowCount, int page, string query)
        {
            var baseQuery = Context.Checklists.Where(x => x.IsActive && x.IsContractChecklist).OrderBy(o => o.ChecklistName).ChecklistOverviewQuery(query).Select(ChecklistModel.EntityToModel);

            return PagedResults<ChecklistModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ChecklistModel> GetActiveJobTypeChecklists(int rowCount, int page, string query)
        {
            var baseQuery = Context.Checklists.Where(x => x.IsActive && x.IsJobTypeChecklist).OrderBy(o => o.ChecklistName).ChecklistOverviewQuery(query).Select(ChecklistModel.EntityToModel);

            return PagedResults<ChecklistModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ChecklistModel> GetActiveVehicleTypeChecklists(int rowCount, int page, string query)
        {
            var baseQuery = Context.Checklists.Where(x => x.IsActive && x.IsVehicleTypeChecklist).OrderBy(o => o.ChecklistName).ChecklistOverviewQuery(query).Select(ChecklistModel.EntityToModel);

            return PagedResults<ChecklistModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ChecklistQuestionModel> GetActiveChecklistQuestions(int rowCount, int page, string query, Guid checklistId)
        {
            var baseQuery = Context.ChecklistQuestions.Where(x => x.ChecklistFK.Equals(checklistId)).OrderBy(o => o.QuestionOrder).ChecklistQuestionOverviewQuery(query).Select(ChecklistQuestionModel.EntityToModel);

            return PagedResults<ChecklistQuestionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public Guid CreateChecklistQuestion(ChecklistQuestionModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.ID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.ChecklistQuestions.Add(ChecklistQuestionModel.ModelToEntity(model));

            SaveChanges();

            return model.ID;
        }

        public ChecklistQuestionModel SingleQuestion(Guid id)
        {
            return Context.ChecklistQuestions.Where(a => a.ChecklistQuestionPK.Equals(id)).Select(ChecklistQuestionModel.EntityToModel).SingleOrDefault();
        }

        public void UpdateQuestion(ChecklistQuestionModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            ChecklistQuestion current = Context.ChecklistQuestions.Where(x => x.ChecklistQuestionPK.Equals(model.ID)).SingleOrDefault();

            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.ChecklistQuestion1 = model.ChecklistQuestion == null ? "" : model.ChecklistQuestion;
            current.QuestionOrder = model.QuestionOrder;

            SaveChanges();
        }

        public void UpdateAnswer(ChecklistAnswerModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();
            var id = model.ChecklistAnswerID;
            var current = Context.TaskChecklistAnswers.Where(x => x.TaskChecklistAnswerPK.Equals(id.Value)).SingleOrDefault();

          
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByID = _userContext.UserDetails.ID;
            current.Reason = model.Reason;
            current.Answer = model.ChecklistAnswer.Value;
            SaveChanges();
        }

        public List<ChecklistQuestionModel> GetChecklistQuestions(Guid checklistID)
        {
            return Context.ChecklistQuestions.Where(x => x.ChecklistFK.Equals(checklistID)).Select(ChecklistQuestionModel.EntityToModel).ToList();
        }

        public int GetNextQuestionOrderNumber(Guid checklistID)
        {
            var questionOrder = Context.ChecklistQuestions.Where(a => a.ChecklistFK == checklistID && a.IsActive).OrderByDescending(a => a.QuestionOrder).Select(a => (int?)a.QuestionOrder).FirstOrDefault();
            if (questionOrder == null)
            {
                return 1;
            }
            else
            {
                return (int)questionOrder + 1;
            }
        }
    }
}
