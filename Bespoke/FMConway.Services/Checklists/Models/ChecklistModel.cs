﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Checklists.Models
{
    public class ChecklistModel
    {
        public Guid ChecklistID { get; set; }
        [Required]
        public string ChecklistName { get; set; }
        [DisplayName("Tender Checklist")]
        public bool IsContractChecklist { get; set; }
        [DisplayName("Job Type Checklist")]
        public bool IsJobTypeChecklist { get; set; }
        [DisplayName("Vehicle Type Checklist")]
        public bool IsVehicleTypeChecklist { get; set; }
        [DisplayName("Maintenance Checklist")]
        public bool IsMaintenanceChecklist { get; set; }
        [DisplayName("Signature Checklist")]
        public bool IsSignatureChecklist { get; set; }


        [DisplayName("12AB Works Checklist")]
        public bool IsHighSpeedChecklist { get; set; }
        [DisplayName("12C Works Checklist")]
        public bool Is12cChecklist { get; set; }
        [DisplayName("12D Works Checklist")]
        public bool IsLowSpeedChecklist { get; set; }

        [DisplayName("Pre Hire Checklist")]
        public bool IsPreHireChecklist { get; set; }
        [DisplayName("Vms Checklist")]
        public bool IsVmsChecklist { get; set; }
        public bool IsBatteryChecklist { get; set; }
        [DisplayName("Pre Hire Battery")]
        public bool IsPreHireBatteryChecklist { get; set; }
        [DisplayName("Post Hire Battery")]
        public bool IsPostHireBatteryChecklist { get; set; }

        [DisplayName("Jetting Permit To Work Checklist")]
        public bool IsJettingPermitToWorkChecklist { get; set; }
        [DisplayName("Jetting Risk Assessment Checklist")]
        public bool IsJettingRiskAssessmentChecklist { get; set; }
        [DisplayName("Tankering Permit To Work Checklist")]
        public bool IsTankeringPermitToWorkChecklist { get; set; }
        [DisplayName("Tankering Risk Assessment Checklist")]
        public bool IsTankeringRiskAssessmentChecklist { get; set; }
        [DisplayName("CCTV Risk Assessment Checklist")]
        public bool IsCCTVRiskAssessmentChecklist { get; set; }

        [DisplayName("Cyclical Works Risk Assessment Checklist")]
        public bool IsCyclicalWorksRiskAssessmentChecklist { get; set; }
        public bool CompleteAtDepot { get; set; }
        public bool StartOfWork { get; set; }

        public string ChecklistType { get; set; }

        public List<ChecklistQuestionModel> ChecklistQuestions { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Checklist, ChecklistModel>> EntityToModel = entity => new ChecklistModel
        {
            ChecklistID = entity.ChecklistPK,
            ChecklistName = entity.ChecklistName,
            IsContractChecklist = entity.IsContractChecklist,
            IsJobTypeChecklist = entity.IsJobTypeChecklist,
            IsVehicleTypeChecklist = entity.IsVehicleTypeChecklist,
            IsMaintenanceChecklist = entity.IsMaintenanceChecklist,
            IsSignatureChecklist = entity.IsSignatureChecklist,
            IsHighSpeedChecklist = entity.IsHighSpeedChecklist,
            Is12cChecklist = entity.Is12cChecklist,
            IsLowSpeedChecklist = entity.IsLowSpeedChecklist,
            IsPreHireChecklist = entity.IsPreHireChecklist,
            IsVmsChecklist = entity.IsVmsChecklist,
            IsBatteryChecklist = entity.IsBatteryChecklist,
            IsPreHireBatteryChecklist = entity.IsPreHireBatteryChecklist,
            IsPostHireBatteryChecklist = entity.IsPostHireBatteryChecklist,
            IsJettingPermitToWorkChecklist = entity.IsJettingPermitToWorkChecklist,
            IsJettingRiskAssessmentChecklist = entity.IsJettingRiskAssessmentChecklist,
            IsTankeringPermitToWorkChecklist = entity.IsTankeringPermitToWorkChecklist,
            IsTankeringRiskAssessmentChecklist = entity.IsTankeringRiskAssessmentChecklist,
            IsCCTVRiskAssessmentChecklist = entity.IsCCTVRiskAssessmentChecklist,
            IsCyclicalWorksRiskAssessmentChecklist = entity.IsJettingNewRiskAssessmentChecklist,

            CompleteAtDepot = entity.CompleteAtDepot,
            StartOfWork = entity.StartOfWork,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<ChecklistModel, Checklist> ModelToEntity = model => new Checklist
        {
            ChecklistPK = model.ChecklistID,
            ChecklistName = model.ChecklistName,
            IsContractChecklist = model.IsContractChecklist,
            IsJobTypeChecklist = model.IsJobTypeChecklist,
            IsVehicleTypeChecklist = model.IsVehicleTypeChecklist,
            IsMaintenanceChecklist = model.IsMaintenanceChecklist,
            IsSignatureChecklist = model.IsSignatureChecklist,
            IsHighSpeedChecklist = model.IsHighSpeedChecklist,
            Is12cChecklist = model.Is12cChecklist,
            IsLowSpeedChecklist = model.IsLowSpeedChecklist,
            IsPreHireChecklist = model.IsPreHireChecklist,
            IsVmsChecklist = model.IsVmsChecklist,
            IsBatteryChecklist = model.IsBatteryChecklist,
            IsPreHireBatteryChecklist = model.IsPreHireBatteryChecklist,
            IsPostHireBatteryChecklist = model.IsPostHireBatteryChecklist,
            IsJettingPermitToWorkChecklist = model.IsJettingPermitToWorkChecklist,
            IsJettingRiskAssessmentChecklist = model.IsJettingRiskAssessmentChecklist,
            IsTankeringPermitToWorkChecklist = model.IsTankeringPermitToWorkChecklist,
            IsTankeringRiskAssessmentChecklist = model.IsTankeringRiskAssessmentChecklist,
            IsCCTVRiskAssessmentChecklist = model.IsCCTVRiskAssessmentChecklist,
            IsJettingNewRiskAssessmentChecklist = model.IsCyclicalWorksRiskAssessmentChecklist,

            CompleteAtDepot = model.CompleteAtDepot,
            StartOfWork = model.StartOfWork,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
