﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Checklists.Models
{
    public class ChecklistQuestionModel
    {
        public Guid ID { get; set; }
        [Required]
        public string ChecklistQuestion { get; set; }
        public int QuestionOrder { get; set; }
        public Guid ChecklistID { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<ChecklistQuestion, ChecklistQuestionModel>> EntityToModel = entity => new ChecklistQuestionModel
        {
            ID = entity.ChecklistQuestionPK,
            ChecklistQuestion = entity.ChecklistQuestion1,
            QuestionOrder = entity.QuestionOrder,
            ChecklistID = entity.ChecklistFK,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<ChecklistQuestionModel, ChecklistQuestion> ModelToEntity = model => new ChecklistQuestion
        {
            ChecklistQuestionPK = model.ID,
            ChecklistQuestion1 = model.ChecklistQuestion,
            QuestionOrder = model.QuestionOrder,
            ChecklistFK = model.ChecklistID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
