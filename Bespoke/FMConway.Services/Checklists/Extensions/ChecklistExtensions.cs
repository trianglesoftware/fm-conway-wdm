﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Checklists.Extensions
{
    public static class ChecklistExtensions
    {
        public static IQueryable<ChecklistQuestion> ActiveChecklistQuestions(this IQueryable<ChecklistQuestion> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<ChecklistQuestion> ChecklistQuestionOverviewQuery(this IQueryable<ChecklistQuestion> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ChecklistQuestion1.Contains(query));
            }

            return model;
        }

        public static IQueryable<Checklist> ChecklistOverviewQuery(this IQueryable<Checklist> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ChecklistName.Contains(query));
            }

            return model;
        }
    }
}
