﻿using FMConway.Services.Checklists.Models;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Contracts.Models;
using FMConway.Services.Helpers;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.VehicleTypes.Models;
using FMConway.Services.WorkInstructionDetails.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Checklists.Interfaces
{
    public interface IChecklistService : IService
    {
        ServiceResults<Guid> Create(ChecklistModel model);
        List<ChecklistModel> GetActiveChecklists();
        List<ChecklistModel> GetInactiveChecklists();
        ChecklistModel Single(Guid id);
        ServiceResults Update(ChecklistModel model);
        List<ContractChecklistModel> Get(Guid contractID);
        //List<WorkInstructionChecklist> GetForWI(Guid workInstructionID);
        List<JobTypeChecklistModel> GetJobTypeChecklists(Guid jobTypeID);
        List<VehicleTypeChecklistModel> GetVehicleTypeChecklists(Guid vehicleTypeID);
       
        List<ChecklistQuestionModel> GetChecklistQuestions(Guid checklistID);

        PagedResults<ChecklistModel> GetActiveTenderChecklists(int rowCount, int page, string query);
        PagedResults<ChecklistModel> GetActiveJobTypeChecklists(int rowCount, int page, string query);
        PagedResults<ChecklistModel> GetActiveVehicleTypeChecklists(int rowCount, int page, string query);
        PagedResults<ChecklistQuestionModel> GetActiveChecklistQuestions(int rowCount, int page, string query, Guid checklistId);
        Guid CreateChecklistQuestion(ChecklistQuestionModel model);
        ChecklistQuestionModel SingleQuestion(Guid id);
        void UpdateQuestion(ChecklistQuestionModel model);
        int GetNextQuestionOrderNumber(Guid id);
        void UpdateAnswer(ChecklistAnswerModel model);
    }
}
