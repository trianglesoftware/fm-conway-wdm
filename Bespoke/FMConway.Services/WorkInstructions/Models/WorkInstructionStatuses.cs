﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Models
{
    public class WorkInstructionStatuses
    {
        public static readonly string New = "New";
        public static readonly string ReadyToSchedule = "Ready To Schedule";
        public static readonly string Scheduled = "Scheduled";
        public static readonly string Cancelled = "Cancelled";
        public static readonly string Complete = "Complete";
    }
}
