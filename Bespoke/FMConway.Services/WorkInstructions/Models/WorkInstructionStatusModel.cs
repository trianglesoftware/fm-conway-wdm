﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FMConway.Services.Collections.Lookup;
using FMConway.Services.WorkInstructions.Models;
using System.Data.Entity.SqlServer;

namespace FMConway.Services.WorkInstructions.Models
{
    public class WorkInstructionStatusModel
    {
        public Guid StatusID { get; set; }
        public string Status { get; set; }
        public bool IsWorkInstructionStatus { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Status, WorkInstructionStatusModel>> EntityToModel = entity => new WorkInstructionStatusModel()
        {
            StatusID = entity.StatusPK,
            Status = entity.Status1,
            IsWorkInstructionStatus = entity.IsWorkInstructionStatus,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<WorkInstructionStatusModel, Status> ModelToEntity = model => new Status()
        {
            StatusPK = model.StatusID,
            Status1 = model.Status,
            IsWorkInstructionStatus = model.IsWorkInstructionStatus,
            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }

    public class ShiftProgressInfoModel
    {
        public int ShiftProgressInfoID { get; set; }
        public int ShiftProgressID { get; set; }
        public Guid ShiftID { get; set; }
        public string Progress { get; set; }
        public DateTime Time { get; set; }
        public DateTime CreatedDate { get; set; }

        public static Expression<Func<ShiftProgressInfo, ShiftProgressInfoModel>> EntityToModel = a => new ShiftProgressInfoModel() 
        {
            ShiftProgressInfoID = a.ShiftProgressInfoPK,
            ShiftProgressID = a.ShiftProgressID,
            ShiftID = a.ShiftFK,
            Progress = a.ShiftProgress.Progress,
            Time = a.Time,
            CreatedDate = a.CreatedDate,
        };
    }

    public class ShiftProgressModel
    {
        public int ShiftProgressID { get; set; }
        public string Progress { get; set; }

        public static Expression<Func<ShiftProgress, ShiftProgressModel>> EntityToModel = entity => new ShiftProgressModel() 
        {
            ShiftProgressID = entity.ShiftProgressID,
            Progress = entity.Progress
        };
    }

    public class InstallLookup : LookupBase<Guid>
    {
        public int JobNumber { get; set; }
        public string JonTitle { get; set; }
        public string ClientName { get; set; }
        public string ContractTitle { get; set; }
    }
}
