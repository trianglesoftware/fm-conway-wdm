﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Models
{
    public class DocumentModel
    {
        public Guid DocumentID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public string DocumentTitle { get; set; }
        public string Revision { get; set; }
        public Guid FileID { get; set; }
        public Guid DrawingID { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<WorkInstructionDrawing, DocumentModel>> EntityToModel = entity => new DocumentModel()
        {
            DocumentID = entity.WorkInstructionDrawingPK,
            DocumentTitle = entity.Drawing.DrawingTitle,
            Revision = entity.Drawing.Revision,
            WorkInstructionID = entity.WorkInstructionFK,
            FileID = entity.Drawing.FileFK,
            DrawingID = entity.Drawing.DrawingPK,
            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Expression<Func<JobDrawing, DocumentModel>> JobETM = entity => new DocumentModel()
        {
            DocumentID = entity.JobDrawingPK,
            DocumentTitle = entity.Drawing.DrawingTitle,
            Revision = entity.Drawing.Revision,
            FileID = entity.Drawing.FileFK,
            DrawingID = entity.Drawing.DrawingPK,
            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<DocumentModel, WorkInstructionDrawing> ModelToEntity = model => new WorkInstructionDrawing()
        {
            WorkInstructionDrawingPK = model.DocumentID,
            WorkInstructionFK = model.WorkInstructionID,
            DrawingFK = model.FileID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
