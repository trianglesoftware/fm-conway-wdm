﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Models
{
    public class WorkInstructionJobOverviewModel
    {
        public string Client { get; set; }
        public string ContractNumber { get; set; }
        public string ContractTitle { get; set; }
        public string SchemeTitle { get; set; }
        public string JobTitle { get; set; }
        public string CustomerProjectCode { get; set; }
        public Guid DepotID { get; set; }
        public string DepotLookup { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }

        public static Expression<Func<Job, WorkInstructionJobOverviewModel>> EntityToModel = entity => new WorkInstructionJobOverviewModel
        {
            Client = entity.Contract.Customer.CustomerName,
            ContractNumber = entity.Contract.ContractNumber,
            ContractTitle = entity.Contract.ContractTitle,
            SchemeTitle = entity.SchemeFK.HasValue ? entity.Scheme.SchemeTitle : entity.Contract.ContractTitle,
            JobTitle = entity.JobTitle,
            CustomerProjectCode = entity.CustomerProjectCode,
            DepotID = entity.DepotFK,
            DepotLookup = entity.Depot.DepotName,

            AddressLine1 = entity.AddressLine1,
            AddressLine2 = entity.AddressLine2,
            AddressLine3 = entity.AddressLine3,
            City = entity.City,
            County = entity.County,
            Postcode = entity.Postcode,
        };
    }
}
