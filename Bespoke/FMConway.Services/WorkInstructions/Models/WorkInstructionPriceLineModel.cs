﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Models
{
    public class WorkInstructionPriceLineModel
    {
        public Guid WorkInstructionPriceLineID { get; set; }
        public Guid WorkInstructionID { get; set; }
        [Required(ErrorMessage = "Price Line is required")]
        public Guid? PriceLineFK { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Rate { get; set; }
        public decimal Total { get; set; }
        public string Note { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<WorkInstructionPriceLine, WorkInstructionPriceLineModel>> EntityToModel = entity => new WorkInstructionPriceLineModel
        {
            WorkInstructionPriceLineID = entity.WorkInstructionPriceLinePK,
            WorkInstructionID = entity.WorkInstructionFK,
            PriceLineFK = entity.PriceLineFK,

            Code = entity.PriceLine.Code,
            Description = entity.PriceLine.Description,
            Quantity = entity.Quantity,
            Rate = entity.Rate,
            Total = (entity.Quantity ?? 0) * (entity.Rate ?? 0),
            Note = entity.Note,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<WorkInstructionPriceLineModel, WorkInstructionPriceLine> ModelToEntity = model => new WorkInstructionPriceLine
        {
            WorkInstructionPriceLinePK = model.WorkInstructionPriceLineID,
            WorkInstructionFK = model.WorkInstructionID,
            PriceLineFK = (Guid)model.PriceLineFK,

            Rate = model.Rate,
            Quantity = model.Quantity,
            Note = model.Note,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
