﻿using FMConway.Services.Employees.Models;
using FMConway.Services.Jobs.Models;
using FMConway.Services.Vehicles.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FMConway.Services.WorkInstructions.Models
{
    public class WorkInstructionModel
    {
        public Guid WorkInstructionID { get; set; }
        public int WorkInstructionNumber { get; set; }
        public Guid? JobID { get; set; }
        public Guid? ShiftID { get; set; }
        public int? JobNumber { get; set; }
        public Guid JobPackID { get; set; }
        public Guid DepotID { get; set; }
        public string DepotLookup { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        [Display(Name = "SRW / Permit Number")]
        public string SRWNumber { get; set; }
        [Display(Name = "Task Details")]
        public string Comments { get; set; }
        public Guid EmployeeID { get; set; }
        public EmployeeModel Employee { get; set; }
        public string RCCNumber { get; set; }
        public string NCCNumber { get; set; }
        public string AreaCommunicationProcedure { get; set; }
        public DateTime? FirstCone { get; set; }
        public DateTime? LastCone { get; set; }
        public string TrafficManagementActivities { get; set; }
        [DisplayName("Maintenance Checklist Reminder In Minutes")]
        public int? MaintenanceChecklistReminderInMinutes { get; set; }
        public string ActivityDescription { get; set; }
        public bool SiteInductionRequired { get; set; }
        public string SiteInductionNotes { get; set; }
        public string EquipmentForActivity { get; set; }
        public string SiteSpecificConditions { get; set; }
        public string SharedRoadSpace { get; set; }
        public Guid StatusID { get; set; }
        public Guid OriginalStatusID { get; set; }
        public bool? IsSuccessful { get; set; }
        public Guid? ReasonCodeID { get; set; }
        public string ReasonCodeLookup { get; set; }
        public string ReasonNotes { get; set; }
        public bool IsScheduled { get; set; }
        public Guid? AreaID { get; set; }
        public string AreaName { get; set; }
        [Display(Name = "Checklist")]
        public Guid? ChecklistID { get; set; }
        public string Checklist { get; set; }
        [Required(ErrorMessage = "Job Type is required")]
        [Display(Name = "Job Type")]
        public Guid? JobTypeID { get; set; }
        public string JobType { get; set; }

        //[Required(ErrorMessage = "Install is required")]
        public Guid? InstallWIID { get; set; }
        public string InstallWI { get; set; }
        public Guid? ParentWIFK { get; set; }

        public string Client { get; set; }
        public string ContractNumber { get; set; }
        public string ContractTitle { get; set; }
        public string ContractManager { get; set; }
        public string SchemeTitle { get; set; }
        public string JobTitle { get; set; }
        public string CustomerProjectCode { get; set; }
        public bool IsOutOfHours { get; set; }
        public string Status { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public bool IsChargeable { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string ClientReferenceNumber { get; set; }
        public string EmployeeNames { get; set; }
        public string VehicleRegistrations { get; set; }
        public bool IsNightShift { get; set; }
        public int OrderNumber { get; set; }

        public static Expression<Func<WorkInstruction, WorkInstructionModel>> EntityToModel = entity => new WorkInstructionModel
        {
            WorkInstructionID = entity.WorkInstructionPK,
            WorkInstructionNumber = entity.WorkInstructionNumber,
            JobID = entity.JobFK,
            JobNumber = entity.Job.JobNumber,
            JobPackID = entity.JobPackFK,
            DepotID = entity.DepotFK,
            DepotLookup = entity.Depot.DepotName,
            ShiftID = entity.JobPack.ShiftFK,
            StartDate = entity.WorkInstructionStartDate,
            FinishDate = entity.WorkInstructionFinishDate,
            SRWNumber = entity.SRWNumber,
            Comments = entity.Comments,
            EmployeeID = entity.EmployeeFK,
            Employee = new EmployeeModel()
            {
                EmployeeName = entity.Employee.EmployeeName
            },
            FirstCone = entity.FirstCone,
            LastCone = entity.LastCone,
            TrafficManagementActivities = entity.TrafficManagementActivities,
            MaintenanceChecklistReminderInMinutes = entity.MaintenanceChecklistReminderInMinutes,
            ActivityDescription = entity.ActivityDescription,
            SiteInductionRequired = entity.SiteInductionRequired,
            SiteInductionNotes = entity.SiteInductionNotes,
            SiteSpecificConditions = entity.SiteSpecificConditions,
            SharedRoadSpace = entity.SharedRoadSpace,
            StatusID = entity.StatusFK,
            OriginalStatusID = entity.StatusFK,
            AreaID = entity.AreaFK,
            AreaName = entity.Area.Area1,
            ChecklistID = entity.ChecklistFK,
            Checklist = entity.Checklist.ChecklistName,
            JobTypeID = entity.JobTypeFK,
            JobType = entity.JobType.DisplayName,
            InstallWIID = entity.InstallWIPK,

            IsScheduled = entity.IsScheduled,

            Client = entity.Job.Contract.Customer.CustomerName,
            ContractNumber = entity.Job.Contract.ContractNumber,
            ContractTitle = entity.Job.Contract.ContractTitle,
            ContractManager = entity.Job.Contract.Employee.EmployeeName,
            SchemeTitle = entity.Job.SchemeFK.HasValue ? entity.Job.Scheme.SchemeTitle : entity.Job.Contract.ContractTitle,
            JobTitle = entity.Job.JobTitle,
            CustomerProjectCode = entity.Job.CustomerProjectCode,
            IsOutOfHours = entity.IsOutOfHours,
            Status = entity.Status.Status1,

            IsSuccessful = entity.IsSuccessful,
            ReasonCodeID = entity.ReasonCodeFK,
            ReasonCodeLookup = entity.ReasonCode.Code,
            ReasonNotes = entity.ReasonNotes,

            AddressLine1 = entity.AddressLine1,
            AddressLine2 = entity.AddressLine2,
            AddressLine3 = entity.AddressLine3,
            City = entity.City,
            County = entity.County,
            Postcode = entity.Postcode,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate,
            IsChargeable = entity.IsChargeable,
            InvoiceDate = entity.InvoiceDate,
            ClientReferenceNumber = entity.ClientReferenceNumber,
            EmployeeNames = entity.WorkInstructionsView.EmployeeNames,
            VehicleRegistrations = entity.WorkInstructionsView.VehicleRegistrations,
            IsNightShift = entity.IsNightShift,
            OrderNumber = entity.OrderNumber,
        };               

        public static Func<WorkInstructionModel, WorkInstruction> ModelToEntity = model => new WorkInstruction
        {
            WorkInstructionPK = model.WorkInstructionID,
            WorkInstructionNumber = model.WorkInstructionNumber,
            JobFK = model.JobID,
            DepotFK = model.DepotID,
            WorkInstructionStartDate = model.StartDate,
            WorkInstructionFinishDate = model.FinishDate,
            SRWNumber = model.SRWNumber,
            Comments = model.Comments,
            EmployeeFK = model.EmployeeID,
            FirstCone = model.FirstCone,
            LastCone = model.LastCone,
            TrafficManagementActivities = model.TrafficManagementActivities,
            MaintenanceChecklistReminderInMinutes = model.MaintenanceChecklistReminderInMinutes,
            ActivityDescription = model.ActivityDescription,
            SiteInductionRequired = model.SiteInductionRequired,
            SiteInductionNotes = model.SiteInductionNotes,
            SiteSpecificConditions = model.SiteSpecificConditions,
            SharedRoadSpace = model.SharedRoadSpace,
            StatusFK = model.StatusID,
            IsOutOfHours = model.IsOutOfHours,
            AreaFK = model.AreaID,
            ChecklistFK = model.ChecklistID,
            JobTypeFK = (Guid)model.JobTypeID,

            AddressLine1 = model.AddressLine1,
            AddressLine2 = model.AddressLine2,
            AddressLine3 = model.AddressLine3,
            City = model.City,
            County = model.County,
            Postcode = model.Postcode,

            IsSuccessful = model.IsSuccessful,
            ReasonCodeFK = model.ReasonCodeID,
            ReasonNotes = model.ReasonNotes,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate,

            IsChargeable = model.IsChargeable,
            ClientReferenceNumber = model.ClientReferenceNumber,
            IsNightShift = model.IsNightShift,
            OrderNumber = model.OrderNumber,
        };
    }

    public class WorkInstructionListModel
    {
        public Guid WorkInstructionID { get; set; }
        public int WorkInstructionNumber { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }

        public static Expression<Func<WorkInstruction, WorkInstructionListModel>> EntityToList = entity => new WorkInstructionListModel
        {
            WorkInstructionID = entity.WorkInstructionPK,
            WorkInstructionNumber = entity.WorkInstructionNumber,
            Date = entity.WorkInstructionStartDate.ToString(),
            Status = entity.Status.Status1
        };
    }
    public class WorkInstructionSimpleModel
    {
        public Guid WorkInstructionID { get; set; }
        public Guid JobTypePK { get; set; }
        public string JobTypeName { get; set; }
        public int WorkInstructionNumber { get; set; }
        public DateTime Date { get; set; }
        public string DateString { get; set; }
        public string Status { get; set; }
        public bool IsComplete { get; set; }

        public static Expression<Func<WorkInstruction, WorkInstructionSimpleModel>> EntityToModel = entity => new WorkInstructionSimpleModel
        {
            WorkInstructionID = entity.WorkInstructionPK,
            JobTypePK = entity.JobTypeFK,
            JobTypeName = entity.JobType.DisplayName,
            WorkInstructionNumber = entity.WorkInstructionNumber,
            Date = entity.WorkInstructionStartDate,
            Status = entity.Status.Status1,
            IsComplete = (entity.Status.Status1 == "Complete")
        };


    }
    public class WorkInstructionDocument
    {
        public Guid? FileID { get; set; }
        public Guid? DrawingID { get; set; }
        public Guid? DocumentID { get; set; }
        public HttpPostedFileBase Document { get; set; }
        public string Title { get; set; }
        public string Revision { get; set; }
        public bool Loaded { get; set; }
        public bool ExistingDrawing { get; set; }
        public bool AddedFromJob { get; set; }
        public bool IsActive { get; set; }
    }

    public class ColumnHeaders
    {
        public static string Engineers = "Engineers";
        public static string Vehicles = "Vehicles";
    };

}
