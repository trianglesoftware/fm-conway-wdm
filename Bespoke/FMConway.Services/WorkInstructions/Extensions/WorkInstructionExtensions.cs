﻿using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Extensions
{
    public static class WorkInstructionExtensions
    {
        public static IQueryable<WorkInstruction> ActiveWorkInstructions(this IQueryable<WorkInstruction> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<WorkInstruction> InactiveWorkInstructions(this IQueryable<WorkInstruction> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<WorkInstruction> WorkInstructionOverviewQuery(this IQueryable<WorkInstruction> model, string query)
        {
            var isIntQuery = int.TryParse(query, out int intQuery);
            var isDateQuery = DateTime.TryParse(query, out DateTime dateQuery);

            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(a =>
                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                    (isDateQuery ? DbFunctions.TruncateTime(a.WorkInstructionStartDate) == dateQuery : false) ||
                    a.JobPack.Shift.ShiftStaffs.Any(b => b.Employee.EmployeeName.Contains(query)) ||
                    a.JobPack.Shift.ShiftVehicles.Any(b => b.Vehicle.Registration.Contains(query)) ||
                    StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionPK).Contains(query) ||
                    (isIntQuery ? a.Job.JobNumber == intQuery : false) ||
                    a.Job.Contract.Customer.CustomerName.Contains(query) ||
                    a.Job.Contract.Employee.EmployeeName.Contains(query) ||
                    a.Job.Contract.ContractNumber.Contains(query) ||
                    a.Job.JobTitle.Contains(query) ||
                    a.SRWNumber.Contains(query) ||
                    a.Employee.EmployeeName.Contains(query) ||
                    a.Status.Status1.Contains(query) ||
                    a.JobType.DisplayName.Contains(query)
                    );
            }

            return model;
        }

        public static IQueryable<ShiftProgressInfo> ShiftProgressInfoQuery(this IQueryable<ShiftProgressInfo> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ShiftProgress.Progress.Contains(query));
            }

            return model;
        }

        // this really needs moving into job extensions, todo
        public static string AddressInLine(this WorkInstruction workInstruction)
        {
            var addressLines = GetAddress(workInstruction);
            return string.Join(" ", addressLines);
        }

        public static string AddressInLine(this WorkInstructionModel workInstrctionModel)
        {
            var workInstruction = WorkInstructionModel.ModelToEntity(workInstrctionModel);
            return workInstruction.AddressInLine();
        }

        public static string AddressNewLine(this WorkInstruction workInstruction)
        {
            var addressLines = GetAddress(workInstruction);
            return string.Join(Environment.NewLine, addressLines);
        }

        private static IEnumerable<string> GetAddress(WorkInstruction workInstruction)
        {
            var addressLines = new[]
            {
                workInstruction.AddressLine1,
                workInstruction.AddressLine2,
                workInstruction.AddressLine3,
                workInstruction.City,
                workInstruction.County,
                workInstruction.Postcode
            }.Where(a => !string.IsNullOrWhiteSpace(a)).Select(a => a.Trim());

            return addressLines;
        }
    }
}
