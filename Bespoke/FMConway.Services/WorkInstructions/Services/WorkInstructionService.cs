﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.WorkInstructions.Extensions;
using System.Security.Authentication;
using FMConway.Services.Contracts.Interfaces;
using FMConway.Services.MethodStatements.Interfaces;
using FMConway.Services.Areas.Interfaces;
using FMConway.Services.ToolboxTalks.Interfaces;
using FMConway.Services.JobTypes.Interfaces;
using FMConway.Services.Checklists.Interfaces;
using FMConway.Services.Tasks.Interfaces;
using FMConway.Services.JobPacks.Interfaces;
using FMConway.Services.WorkInstructions.Interfaces;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.Jobs.Models;
using FMConway.Services.WorkInstructionDetails.Models;
using FMConway.Services.WorkInstructions.Enums;
using FMConway.Services.Tasks.Enums;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Runtime.Serialization;

namespace FMConway.Services.WorkInstructions.Services
{
    public class WorkInstructionService : Service, FMConway.Services.WorkInstructions.Interfaces.IWorkInstructionService
    {
        UserContext _userContext;
        ITaskService _taskService;
        IJobPackService _jobPackService;
        IErrorLoggingService _errorLoggingService;
        ILoadingSheetService _loadingSheetService;

        public IMethodStatementService MethodStatements { get; set; }
        public IToolboxTalkService ToolboxTalks { get; set; }
        public IJobTypeService JobTypes { get; set; }
        public IChecklistService Checklists { get; set; }
        public IWorkInstructionDocumentService Documents { get; set; }

        public WorkInstructionService(UserContext userContext, ITaskService taskService, IJobPackService jobPackService, IErrorLoggingService errorLoggingService, ILoadingSheetService loadingSheetService)
        {
            _userContext = userContext;
            _taskService = taskService; 
            _jobPackService = jobPackService;
            _errorLoggingService = errorLoggingService;
            _loadingSheetService = loadingSheetService;
        }

        public Guid Create(WorkInstructionModel model, Dictionary<Guid, string> drawings, Dictionary<Guid, string> methodStatements, Dictionary<Guid, string> toolboxTalks, List<WorkInstructionPriceLineModel> priceLineModel)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            _taskService.SetContext(Context);

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    model.WorkInstructionID = Guid.NewGuid();

                    var topRecord = Context.WorkInstructions.OrderByDescending(x => x.WorkInstructionNumber).FirstOrDefault();
                    if (topRecord != null)
                    {
                        model.WorkInstructionNumber = topRecord.WorkInstructionNumber + 1;
                    }
                    else
                    {
                        model.WorkInstructionNumber = 1;
                    }

                    model.SRWNumber = model.SRWNumber ?? "";
                    model.Comments = model.Comments ?? "";
                    model.TrafficManagementActivities = model.TrafficManagementActivities ?? "";
                    model.ActivityDescription = model.ActivityDescription ?? "";
                    model.SiteInductionNotes = model.SiteInductionNotes ?? "";
                    model.SiteSpecificConditions = model.SiteSpecificConditions ?? "";
                    model.SharedRoadSpace = model.SharedRoadSpace ?? "";

                    model.AddressLine1 = model.AddressLine1 ?? "";
                    model.AddressLine2 = model.AddressLine2 ?? "";
                    model.AddressLine3 = model.AddressLine3 ?? "";
                    model.City = model.City ?? "";
                    model.County = model.County ?? "";
                    model.Postcode = model.Postcode ?? "";

                    model.IsActive = true;
                    model.CreatedByID = _userContext.UserDetails.ID;
                    model.CreatedDate = DateTime.Now;
                    model.UpdatedByID = _userContext.UserDetails.ID;
                    model.UpdatedDate = DateTime.Now;

                    //if (model.JobType == "Installation maintenance removal") {
                    //    model.ChecklistID = new Guid("4425C77E-68B2-4721-9C73-9D878185DF50"); /* Install, maintenance and remove Traffic management checklist*/
                    //}

                    // set status to "ready to schedule"
                    model.StatusID = WorkInstructionStatusesEnum.ReadyToSchedule;
                    model.Status = "Ready To Schedule";

                    WorkInstruction wi = WorkInstructionModel.ModelToEntity(model);

                    // Install Work Instruction
                    if (model.InstallWIID != null && model.InstallWIID != Guid.Empty && model.JobTypeID == new Guid("ba7325ff-b0f9-4926-b363-b0c3b2cafec3"))
                    {
                        wi.InstallWIPK = model.InstallWIID;
                    }

                    wi.ParentWIFK = model.ParentWIFK;

                    // Create Job Pack
                    JobPack jp = new JobPack();
                    jp.JobPackPK = Guid.NewGuid();
                    jp.JobPackName = model.SchemeTitle;
                    jp.IsActive = true;
                    jp.CreatedByID = _userContext.UserDetails.ID;
                    jp.CreatedDate = DateTime.Now;
                    jp.UpdatedByID = _userContext.UserDetails.ID;
                    jp.UpdatedDate = DateTime.Now;

                    Context.JobPacks.Add(jp);

                    wi.JobPackFK = jp.JobPackPK;

                    Context.WorkInstructions.Add(wi);

                    //briefings
                    if (drawings != null && drawings.Count > 0)
                    {
                        foreach (var drawing in drawings)
                        {
                            WorkInstructionDrawing newDrawing = new WorkInstructionDrawing();
                            newDrawing.WorkInstructionDrawingPK = Guid.NewGuid();
                            newDrawing.WorkInstructionFK = model.WorkInstructionID;
                            newDrawing.DrawingFK = drawing.Key;
                            newDrawing.IsActive = true;
                            newDrawing.CreatedByFK = _userContext.UserDetails.ID;
                            newDrawing.CreatedDate = DateTime.Now;
                            newDrawing.UpdatedByFK = _userContext.UserDetails.ID;
                            newDrawing.UpdatedDate = DateTime.Now;

                            Context.WorkInstructionDrawings.Add(newDrawing);

                            Drawing d = Context.Drawings.Where(k => k.DrawingPK.Equals(drawing.Key)).SingleOrDefault();

                            Briefing briefing = new Briefing();
                            briefing.BriefingPK = Guid.NewGuid();
                            briefing.BriefingTypeFK = 5; // Document
                            briefing.JobPackFK = jp.JobPackPK;
                            briefing.Name = d.DrawingTitle;
                            briefing.Details = "";
                            briefing.FileFK = d.FileFK;
                            briefing.IsActive = true;
                            briefing.CreatedByID = _userContext.UserDetails.ID;
                            briefing.CreatedDate = DateTime.Now;
                            briefing.UpdatedByID = _userContext.UserDetails.ID;
                            briefing.UpdatedDate = DateTime.Now;

                            Context.Briefings.Add(briefing);
                        }
                    }

                    //method statements
                    if (methodStatements != null && methodStatements.Count > 0)
                    {
                        foreach (var methodStatement in methodStatements)
                        {
                            WorkInstructionMethodStatement newMethodStatement = new WorkInstructionMethodStatement();
                            newMethodStatement.WorkInstructionMethodStatementPK = Guid.NewGuid();
                            newMethodStatement.WorkInstructionFK = model.WorkInstructionID;
                            newMethodStatement.MethodStatementFK = methodStatement.Key;
                            newMethodStatement.IsActive = true;
                            newMethodStatement.CreatedByFK = _userContext.UserDetails.ID;
                            newMethodStatement.CreatedDate = DateTime.Now;
                            newMethodStatement.UpdatedByFK = _userContext.UserDetails.ID;
                            newMethodStatement.UpdatedDate = DateTime.Now;

                            Context.WorkInstructionMethodStatements.Add(newMethodStatement);

                            MethodStatement method = Context.MethodStatements.Where(m => m.MethodStatementPK.Equals(methodStatement.Key)).SingleOrDefault();

                            Briefing briefing = new Briefing();
                            briefing.BriefingPK = Guid.NewGuid();
                            briefing.BriefingTypeFK = 4; // Method Statement
                            briefing.JobPackFK = jp.JobPackPK;
                            briefing.Name = method.MethodStatementTitle;
                            briefing.Details = "";
                            briefing.FileFK = method.FileFK;
                            briefing.IsActive = true;
                            briefing.CreatedByID = _userContext.UserDetails.ID;
                            briefing.CreatedDate = DateTime.Now;
                            briefing.UpdatedByID = _userContext.UserDetails.ID;
                            briefing.UpdatedDate = DateTime.Now;

                            Context.Briefings.Add(briefing);
                        }
                    }

                    //toolbox talks
                    if (toolboxTalks != null && toolboxTalks.Count > 0)
                    {
                        foreach (var toolboxTalk in toolboxTalks)
                        {
                            WorkInstructionToolboxTalk newToolboxTalk = new WorkInstructionToolboxTalk();
                            newToolboxTalk.WorkInstructionToolboxTalkPK = Guid.NewGuid();
                            newToolboxTalk.WorkInstructionFK = model.WorkInstructionID;
                            newToolboxTalk.ToolboxTalkFK = toolboxTalk.Key;
                            newToolboxTalk.IsActive = true;
                            newToolboxTalk.CreatedByFK = _userContext.UserDetails.ID;
                            newToolboxTalk.CreatedDate = DateTime.Now;
                            newToolboxTalk.UpdatedByFK = _userContext.UserDetails.ID;
                            newToolboxTalk.UpdatedDate = DateTime.Now;

                            Context.WorkInstructionToolboxTalks.Add(newToolboxTalk);

                            ToolboxTalk talk = Context.ToolboxTalks.Where(tt => tt.ToolboxTalkPK.Equals(toolboxTalk.Key)).SingleOrDefault();

                            Briefing briefing = new Briefing();
                            briefing.BriefingPK = Guid.NewGuid();
                            briefing.BriefingTypeFK = 3; // Toolbox Talk
                            briefing.JobPackFK = jp.JobPackPK;
                            briefing.Name = talk.ToolboxTalk1;
                            briefing.Details = talk.Body;
                            briefing.IsActive = true;
                            briefing.CreatedByID = _userContext.UserDetails.ID;
                            briefing.CreatedDate = DateTime.Now;
                            briefing.UpdatedByID = _userContext.UserDetails.ID;
                            briefing.UpdatedDate = DateTime.Now;

                            Context.Briefings.Add(briefing);
                        }
                    }

                    SaveChanges();

                    // Create tasks
                    int taskOrder = 0;

                    var job = Context.Jobs.Where(a => a.JobPK == model.JobID).Single();
                    var checklists = Context.Checklists.Where(a => a.IsActive).ToList();

                    //string jobStartTaskDescription = "Job Start";
                    //Guid? speedChecklistID = null;
                    //Guid? batteryPOWChecklistID = null;
                    //Guid? batteryPostHireChecklistID = null;

                    //if (job.JobSpeed.Name == JobSpeedModel._12AB)
                    //{
                    //    speedChecklistID = Context.Checklists.Where(w => w.IsHighSpeedChecklist && w.IsActive).Select(a => (Guid?)a.ChecklistPK).FirstOrDefault();
                    //    jobStartTaskDescription = "First Cone";
                    //}
                    //else if (job.JobSpeed.Name == JobSpeedModel._12D)
                    //{
                    //    speedChecklistID = Context.Checklists.Where(w => w.IsLowSpeedChecklist && w.IsActive).Select(a => (Guid?)a.ChecklistPK).FirstOrDefault();
                    //}
                    //else if (job.JobSpeed.Name == JobSpeedModel._12C)
                    //{
                    //    speedChecklistID = Context.Checklists.Where(w => w.Is12cChecklist && w.IsActive).Select(a => (Guid?)a.ChecklistPK).FirstOrDefault();
                    //}

                    //batteryPOWChecklistID = Context.Checklists.Where(w => w.IsBatteryChecklist && w.IsActive).Select(a => (Guid?)a.ChecklistPK).FirstOrDefault();
                    //batteryPostHireChecklistID = Context.Checklists.Where(w => w.IsPostHireBatteryChecklist && w.IsActive).Select(a => (Guid?)a.ChecklistPK).FirstOrDefault();

                    // warning, if you rename some task names such as "Arrive on Site", be sure to search entire solution for references to the string
                    // todo: create static strings class for task names

                    switch (model.JobType)
                    {
                        case JobTypesEnum.Test:
                            var testJettingRiskAssessmentChecklistID = checklists.Where(a => a.IsJettingRiskAssessmentChecklist).Select(a => a.ChecklistPK).Single();

                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "FreeTextTask", true);
                            _taskService.CreateFreeTextWithDetailsTask(++taskOrder, model.WorkInstructionID, "FreeTextWithDetailsTask", true, false);
                            _taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "PhotoSignatureTask", true, false, true, true, false, "ShowNotes");
                            _taskService.CreateTrafficCountTask(++taskOrder, model.WorkInstructionID, true);
                            //_taskService.CreatAreaCallTask(++taskOrder, model.WorkInstructionID, areaFK); // not needed for this project
                            _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, "Risk Assessment", true, testJettingRiskAssessmentChecklistID);
                            _taskService.CreateSignaturesTask(++taskOrder, model.WorkInstructionID, true, true, true, true, false, true, false);
                            _taskService.CreateEquipmentTask(++taskOrder, model.WorkInstructionID, EquipmentTaskType.Install, "ShowNotes");
                            //_taskService.CreateTaskDescription(++taskOrder, model.WorkInstructionID, "TaskDescription"); // doesn't show anything
                            _taskService.CreateWeatherTask(++taskOrder, model.WorkInstructionID, true);
                            //_taskService.CreateBatteryTask(++taskOrder, model.WorkInstructionID, new Guid("772A49A9-84F9-413C-A273-06CCABDEF7E3")); appears no different then a checklist
                            break;

                        case JobTypesEnum.Jetting:
                            var jettingRiskAssessmentChecklistID = checklists.Where(a => a.IsJettingRiskAssessmentChecklist).Select(a => a.ChecklistPK).Single();

                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Travel", true);
                            _taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Arrive on Site", true, true, true, false, false, "ShowNotes");
                            _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, "Risk Assessment", true, jettingRiskAssessmentChecklistID);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Work", true);
                            _taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Loading Process", true, true, false, true, job.IsClientSignatureRequired, "ShowNotes|NotesRequired");
                            _taskService.CreateActivityDetailsTask(++taskOrder, model.WorkInstructionID, "Activity Details", true);
                            _taskService.CreateCleansingLogTask(++taskOrder, model.WorkInstructionID, "Cleansing Log", job.Contract.IsCleansingLogMandatory);
                            _taskService.CreateUnloadingProcessTask(++taskOrder, model.WorkInstructionID, "Unloading Process", false);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site", true);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "End Travel", true);
                            break;

                        case JobTypesEnum.Tankering:
                            //var tankeringPermitToWorkChecklistID = checklists.Where(a => a.IsTankeringPermitToWorkChecklist).Select(a => a.ChecklistPK).Single();
                            var tankeringRiskAssessmentChecklistID = checklists.Where(a => a.IsTankeringRiskAssessmentChecklist).Select(a => a.ChecklistPK).Single();

                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Travel", true);
                            _taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Arrive on Site", true, true, true, false, false, "ShowNotes");
                            _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, "Risk Assessment", true, tankeringRiskAssessmentChecklistID);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Work", true);
                            _taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Loading Process", true, true, false, true, job.IsClientSignatureRequired, "ShowNotes|NotesRequired");
                            _taskService.CreateActivityDetailsTask(++taskOrder, model.WorkInstructionID, "Activity Details", true);
                            _taskService.CreateUnloadingProcessTask(++taskOrder, model.WorkInstructionID, "Unloading Process", false);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site", true);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "End Travel", true);
                            break;

                        case JobTypesEnum.CCTV:
                            var cctvRiskAssessmentChecklistID = checklists.Where(a => a.IsCCTVRiskAssessmentChecklist).Select(a => a.ChecklistPK).Single();

                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Vehicle and Equipment Checks (TODO)", true);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Checklist and Photos (TODO)", true);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Travel", true);
                            _taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Arrive on Site", true, true, true, false, false, "ShowNotes");
                            _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, "Risk Assessment", true, cctvRiskAssessmentChecklistID);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Work", true);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "CCTV Process (TODO)", true);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Finish Work", true);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Unloading Process (TODO)", true);
                            break;

                        case JobTypesEnum.Install:
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Journey");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Arrive on Site");
                            ////if (speedChecklistID != null)
                            ////{
                            ////    _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, (Guid)speedChecklistID);
                            ////}
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Job Screen", false, true);
                            //_taskService.CreateWeatherTask(++taskOrder, model.WorkInstructionID);
                            //_taskService.CreateTrafficCountTask(++taskOrder, model.WorkInstructionID);
                            ////_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, jobStartTaskDescription);
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Job Installed", false, false, false, false, "PhotosOnlyManditoryIfNoPhotosTaken|ShowNotes");
                            //_taskService.CreateSignaturesTask(++taskOrder, model.WorkInstructionID, false, false, true, job.IsClientSignatureRequired, true, false, "ShowPrintedName|ShowClientVehicleReg");
                            //_taskService.CreateEquipmentTask(++taskOrder, model.WorkInstructionID, EquipmentTaskType.Install, "ShowNotes");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site");
                            break;

                        case JobTypesEnum.Deinstall:
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Journey");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Arrive on Site");
                            ////if (speedChecklistID != null)
                            ////{
                            ////    _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, (Guid)speedChecklistID);
                            ////}
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Job Screen", false, true);
                            //_taskService.CreateWeatherTask(++taskOrder, model.WorkInstructionID);
                            //_taskService.CreateTrafficCountTask(++taskOrder, model.WorkInstructionID);
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Job De-installed", false, false, false, false, "PhotosOnlyManditoryIfNoPhotosTaken|ShowNotes");
                            //_taskService.CreateSignaturesTask(++taskOrder, model.WorkInstructionID, false, false, true, job.IsClientSignatureRequired, true, false, "ShowPrintedName|ShowClientVehicleReg");
                            //_taskService.CreateEquipmentTask(++taskOrder, model.WorkInstructionID, EquipmentTaskType.Collection, "ShowNotes");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site");
                            break;

                        case JobTypesEnum.Maintenance:
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Journey");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Arrive on Site");
                            ////if (speedChecklistID != null)
                            ////{
                            ////    _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, (Guid)speedChecklistID);
                            ////}
                            //_taskService.CreateWeatherTask(++taskOrder, model.WorkInstructionID);
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site");
                            break;

                        case JobTypesEnum.CompleteInstall:
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Journey");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Arrive on Site");
                            ////if (speedChecklistID != null)
                            ////{
                            ////    _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, (Guid)speedChecklistID);
                            ////}
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Job Screen", false, true);
                            //_taskService.CreateWeatherTask(++taskOrder, model.WorkInstructionID);
                            //_taskService.CreateTrafficCountTask(++taskOrder, model.WorkInstructionID);
                            ////_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, jobStartTaskDescription);
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Job Installed", false, false, false, false, "PhotosOnlyManditoryIfNoPhotosTaken|ShowNotes");
                            //_taskService.CreateSignaturesTask(++taskOrder, model.WorkInstructionID, false, false, true, job.IsClientSignatureRequired, true, false, "ShowPrintedName|ShowClientVehicleReg");
                            //_taskService.CreateEquipmentTask(++taskOrder, model.WorkInstructionID, EquipmentTaskType.Install, "ShowNotes");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Man on Site");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Stop Man on Site");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start De-install");
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Confirm Site Clear", true);
                            //_taskService.CreateEquipmentTask(++taskOrder, model.WorkInstructionID, EquipmentTaskType.Collection, "ShowNotes");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site", "SiteClearedConfirmation");
                            //_taskService.CreateSignaturesTask(++taskOrder, model.WorkInstructionID, false, false, true, job.IsClientSignatureRequired, true, false, "ShowPrintedName|ShowClientVehicleReg");
                            break;

                        case JobTypesEnum.VmsInstall:
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Journey");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Arrive on Site");
                            ////if (speedChecklistID != null)
                            ////{
                            ////    _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, (Guid)speedChecklistID);
                            ////}
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Job Screen", false, true);
                            //_taskService.CreateWeatherTask(++taskOrder, model.WorkInstructionID);
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Job De-installed", false, false, false, false, "PhotosOnlyManditoryIfNoPhotosTaken|ShowNotes");
                            //_taskService.CreateSignaturesTask(++taskOrder, model.WorkInstructionID, false, false, true, job.IsClientSignatureRequired, true, false, "ShowPrintedName|ShowClientVehicleReg");
                            //_taskService.CreateEquipmentTask(++taskOrder, model.WorkInstructionID, EquipmentTaskType.Collection, "ShowNotes");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site");
                            break;

                        case JobTypesEnum.BatteryChange:
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Journey");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Arrive on Site");
                            ////if (batteryPOWChecklistID != null)
                            ////{
                            ////    _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, (Guid)batteryPOWChecklistID);
                            ////}
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Confirm Battery Change", false, true);
                            ////_taskService.CreateBatteryTask(++taskOrder, model.WorkInstructionID, (Guid)batteryPostHireChecklistID);
                            //_taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Voltage Check Post Exchange", false, false, false, false, "PhotosOnlyManditoryIfNoPhotosTaken");
                            //_taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site");
                            break;

                        case JobTypesEnum.CyclicalWorksRiskAssessment:
                            var cyclicalWorksRiskAssessmentChecklistID = checklists.Where(a => a.IsJettingNewRiskAssessmentChecklist).Select(a => a.ChecklistPK).Single();

                            _taskService.CreateChecklistTask(++taskOrder, model.WorkInstructionID, "Risk Assessment", true, cyclicalWorksRiskAssessmentChecklistID);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Travel", true);
                            _taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Arrive on Site", true, true, true, false, false, "ShowNotes");
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Start Work", true);
                            _taskService.CreatePhotoSignatureTask(++taskOrder, model.WorkInstructionID, "Loading Process", true, true, false, true, job.IsClientSignatureRequired, "ShowNotes|NotesRequired");
                            _taskService.CreateActivityRiskAssessmentTask(++taskOrder, model.WorkInstructionID, "Activity Details", true);
                            //_taskService.CreateActivityDetailsTask(++taskOrder, model.WorkInstructionID, "Activity Details", true);
                            _taskService.CreateCleansingLogTask(++taskOrder, model.WorkInstructionID, "Cleansing Log", job.Contract.IsCleansingLogMandatory);
                            _taskService.CreateUnloadingProcessTask(++taskOrder, model.WorkInstructionID, "Unloading Process", false);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "Leave Site", true);
                            _taskService.CreateFreeTextTask(++taskOrder, model.WorkInstructionID, "End Travel", true);
                            break;

                        default:
                            break;
                    }

                    // Ready to schedule
                    if (model.StatusID == WorkInstructionStatusesEnum.ReadyToSchedule)
                    {
                        Schedule schedule = new Schedule
                        {
                            SchedulePK = Guid.NewGuid(),
                            WorkInstructionFK = model.WorkInstructionID,
                            StartDate = model.StartDate,
                            EndDate = model.FinishDate,
                            IsActive = true,
                            IsComplete = false,
                            CreatedByFK = _userContext.UserDetails.ID,
                            CreatedDate = DateTime.Now,
                            UpdatedByFK = _userContext.UserDetails.ID,
                            UpdatedDate = DateTime.Now
                        };

                        Context.Schedules.Add(schedule);
                    }

                    SaveChanges();

                    // Price Line
                    if (priceLineModel != null)
                    {
                        foreach (var line in priceLineModel)
                        {
                            WorkInstructionPriceLine wiPriceLine = new WorkInstructionPriceLine()
                            {
                                WorkInstructionPriceLinePK = Guid.NewGuid(),
                                PriceLineFK = line.PriceLineFK ?? default(Guid),
                                WorkInstructionFK = model.WorkInstructionID,
                                Quantity = line.Quantity,
                                Rate = line.Rate,
                                Note = line.Note,
                                IsActive = true,
                                CreatedByFK = _userContext.UserDetails.ID,
                                CreatedDate = DateTime.Now,
                                UpdatedByFK = _userContext.UserDetails.ID,
                                UpdatedDate = DateTime.Now
                            };
                            Context.WorkInstructionPriceLines.Add(wiPriceLine);

                            JobPriceLine jobPriceLine = new JobPriceLine()
                            {
                                JobPriceLinePK = Guid.NewGuid(),
                                JobFK = (Guid)model.JobID,
                                PriceLineFK = line.PriceLineFK ?? default(Guid),
                                WorkInstructionPriceLineFK = wiPriceLine.WorkInstructionPriceLinePK,
                                Quantity = line.Quantity,
                                Rate = line.Rate,
                                Note = line.Note,
                                IsActive = true,
                                CreatedByFK = _userContext.UserDetails.ID,
                                CreatedDate = DateTime.Now,
                                UpdatedByFK = _userContext.UserDetails.ID,
                                UpdatedDate = DateTime.Now
                            };
                            Context.JobPriceLines.Add(jobPriceLine);
                        }

                        SaveChanges();
                    }

                    transaction.Commit();
                    
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    _errorLoggingService.CreateErrorLog("CreateWorkInstruction", e, model.WorkInstructionID.ToString());
                    return Guid.Empty;
                }
            }

            return model.WorkInstructionID;
        }

        public void Update(WorkInstructionModel model,
                    Dictionary<Guid, string> drawings, Dictionary<Guid, string> existingDrawings,
                    Dictionary<Guid, string> methodStatements, Dictionary<Guid, string> existingMethodStatements,
                    Dictionary<Guid, string> toolboxTalks, Dictionary<Guid, string> existingToolboxTalks)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                try
                {
                    WorkInstruction current = Context.WorkInstructions.Where(x => x.WorkInstructionPK.Equals(model.WorkInstructionID)).Single();
                    var originalStatusFK = current.StatusFK;
                    var originalIsActive = current.IsActive;

                    // status can be updated by scheduler or mobile service, check to see if status has been changed in background before updating.
                    if (current.StatusFK == model.OriginalStatusID)
                    {
                        // if status hasn't changed in background since editor was loaded - allow user to change the status
                        current.StatusFK = model.StatusID;
                    }
                    else
                    {
                        // if status has changed in background since editor was loaded - set model status to match the entity status
                        // it's important to reset the model StatusID to prevent the shift progress from updating
                        model.StatusID = current.StatusFK;
                    }

                    if (model.StatusID == WorkInstructionStatusesEnum.ReadyToSchedule || model.StatusID == WorkInstructionStatusesEnum.Scheduled)
                    {
                        var schedule = current.Schedules.SingleOrDefault();

                        if (schedule == null)
                        {
                            schedule = new Schedule
                            {
                                SchedulePK = Guid.NewGuid(),
                                WorkInstructionFK = model.WorkInstructionID,
                                StartDate = model.StartDate,
                                EndDate = model.FinishDate,
                                IsActive = true,
                                IsComplete = false,
                                CreatedByFK = _userContext.UserDetails.ID,
                                CreatedDate = DateTime.Now,
                                UpdatedByFK = _userContext.UserDetails.ID,
                                UpdatedDate = DateTime.Now
                            };

                            Context.Schedules.Add(schedule);
                        }
                        else
                        {
                            schedule.StartDate = model.StartDate;
                            schedule.EndDate = model.FinishDate;

                            if (current.JobPack != null && current.JobPack.Shift != null)
                            {
                                current.JobPack.Shift.ShiftDate = model.StartDate;
                            }
                        }
                    }

                    SaveChanges();

                    current.IsOutOfHours = model.IsOutOfHours;
                    current.IsChargeable = model.IsChargeable;
                    current.IsNightShift = model.IsNightShift;
                    current.DepotFK = model.DepotID;
                    current.WorkInstructionStartDate = model.StartDate;
                    current.WorkInstructionFinishDate = model.FinishDate;
                    current.EmployeeFK = model.EmployeeID;
                    current.FirstCone = model.FirstCone;
                    current.LastCone = model.LastCone;
                    current.SiteInductionRequired = model.SiteInductionRequired;
                    current.AreaFK = model.AreaID;
                    current.ChecklistFK = model.ChecklistID;
                    current.SRWNumber = model.SRWNumber ?? "";
                    current.Comments = model.Comments ?? "";
                    current.TrafficManagementActivities = model.TrafficManagementActivities ?? "";
                    current.ActivityDescription = model.ActivityDescription ?? "";
                    current.SiteInductionNotes = model.SiteInductionNotes ?? "";
                    current.SiteSpecificConditions = model.SiteSpecificConditions ?? "";
                    current.SharedRoadSpace = model.SharedRoadSpace ?? "";
                    current.MaintenanceChecklistReminderInMinutes = model.MaintenanceChecklistReminderInMinutes;
                    current.InvoiceDate = model.InvoiceDate;
                    current.ClientReferenceNumber = model.ClientReferenceNumber;

                    current.AddressLine1 = model.AddressLine1;
                    current.AddressLine2 = model.AddressLine2;
                    current.AddressLine3 = model.AddressLine3;
                    current.City = model.City;
                    current.County = model.County;
                    current.Postcode = model.Postcode;

                    current.IsSuccessful = model.IsSuccessful;
                    current.ReasonCodeFK = model.ReasonCodeID;
                    current.ReasonNotes = model.ReasonNotes;

                    current.IsActive = model.IsActive;
                    current.UpdatedByFK = _userContext.UserDetails.ID;
                    current.UpdatedDate = DateTime.Now;

                    // drawing
                    drawings = drawings ?? new Dictionary<Guid, string>();
                    if (!drawings.OrderBy(o => o.Key).SequenceEqual(existingDrawings.OrderBy(o => o.Key)))
                    {
                        var workInstructionDrawings = Context.WorkInstructionDrawings.Where(x => x.WorkInstructionFK.Equals(model.WorkInstructionID));
                        foreach (var drawing in workInstructionDrawings)
                        {
                            Context.WorkInstructionDrawings.Remove(drawing);
                        }

                        var briefings = Context.Briefings.Where(b => b.BriefingTypeFK.Equals(5) && b.JobPackFK.Equals(current.JobPackFK));
                        foreach (var briefing in briefings)
                        {
                            Context.Briefings.Remove(briefing);
                        }

                        foreach (var drawing in drawings)
                        {

                            WorkInstructionDrawing newDrawing = new WorkInstructionDrawing();
                            newDrawing.WorkInstructionDrawingPK = Guid.NewGuid();
                            newDrawing.WorkInstructionFK = model.WorkInstructionID;
                            newDrawing.DrawingFK = drawing.Key;
                            newDrawing.IsActive = true;
                            newDrawing.CreatedByFK = _userContext.UserDetails.ID;
                            newDrawing.CreatedDate = DateTime.Now;
                            newDrawing.UpdatedByFK = _userContext.UserDetails.ID;
                            newDrawing.UpdatedDate = DateTime.Now;

                            Context.WorkInstructionDrawings.Add(newDrawing);

                            Drawing d = Context.Drawings.Where(k => k.DrawingPK.Equals(drawing.Key)).SingleOrDefault();

                            Briefing briefing = new Briefing();
                            briefing.BriefingPK = Guid.NewGuid();
                            briefing.BriefingTypeFK = 5; // Document
                            briefing.JobPackFK = current.JobPackFK;
                            briefing.Name = d.DrawingTitle;
                            briefing.Details = "";
                            briefing.FileFK = d.FileFK;
                            briefing.IsActive = true;
                            briefing.CreatedByID = _userContext.UserDetails.ID;
                            briefing.CreatedDate = DateTime.Now;
                            briefing.UpdatedByID = _userContext.UserDetails.ID;
                            briefing.UpdatedDate = DateTime.Now;

                            Context.Briefings.Add(briefing);
                        }
                    }

                    SaveChanges();

                    // method statements
                    methodStatements = methodStatements ?? new Dictionary<Guid, string>();
                    if (!methodStatements.SequenceEqual(existingMethodStatements))
                    {
                        var workInstructionStatements = Context.WorkInstructionMethodStatements.Where(x => x.WorkInstructionFK.Equals(model.WorkInstructionID));
                        foreach (var statement in workInstructionStatements)
                        {
                            Context.WorkInstructionMethodStatements.Remove(statement);
                        }

                        var briefings = Context.Briefings.Where(b => b.BriefingTypeFK.Equals(4) && b.JobPackFK.Equals(current.JobPackFK));
                        foreach (var briefing in briefings)
                        {
                            Context.Briefings.Remove(briefing);
                        }

                        foreach (var statement in methodStatements)
                        {
                            WorkInstructionMethodStatement newStatement = new WorkInstructionMethodStatement();
                            newStatement.WorkInstructionMethodStatementPK = Guid.NewGuid();
                            newStatement.WorkInstructionFK = model.WorkInstructionID;
                            newStatement.MethodStatementFK = statement.Key;
                            newStatement.IsActive = true;
                            newStatement.CreatedByFK = _userContext.UserDetails.ID;
                            newStatement.CreatedDate = DateTime.Now;
                            newStatement.UpdatedByFK = _userContext.UserDetails.ID;
                            newStatement.UpdatedDate = DateTime.Now;

                            Context.WorkInstructionMethodStatements.Add(newStatement);

                            MethodStatement method = Context.MethodStatements.Where(m => m.MethodStatementPK.Equals(statement.Key)).SingleOrDefault();

                            Briefing b = new Briefing();
                            b.BriefingPK = Guid.NewGuid();
                            b.BriefingTypeFK = 4; // Method Statement
                            b.JobPackFK = current.JobPackFK;
                            b.Name = method.MethodStatementTitle;
                            b.Details = "";
                            b.FileFK = method.FileFK;
                            b.IsActive = true;
                            b.CreatedByID = _userContext.UserDetails.ID;
                            b.CreatedDate = DateTime.Now;
                            b.UpdatedByID = _userContext.UserDetails.ID;
                            b.UpdatedDate = DateTime.Now;

                            Context.Briefings.Add(b);
                        }
                    }

                    // toolbox talks
                    toolboxTalks = toolboxTalks ?? new Dictionary<Guid, string>();
                    if (!toolboxTalks.SequenceEqual(existingToolboxTalks))
                    {
                        var workInstructionToolbox = Context.WorkInstructionToolboxTalks.Where(x => x.WorkInstructionFK.Equals(model.WorkInstructionID));
                        foreach (var toolbox in workInstructionToolbox)
                        {
                            Context.WorkInstructionToolboxTalks.Remove(toolbox);
                        }

                        var briefings = Context.Briefings.Where(b => b.BriefingTypeFK.Equals(3) && b.JobPackFK.Equals(current.JobPackFK));
                        foreach (var briefing in briefings)
                        {
                            Context.Briefings.Remove(briefing);
                        }

                        foreach (var toolbox in toolboxTalks)
                        {
                            WorkInstructionToolboxTalk newToolbox = new WorkInstructionToolboxTalk();
                            newToolbox.WorkInstructionToolboxTalkPK = Guid.NewGuid();
                            newToolbox.WorkInstructionFK = model.WorkInstructionID;
                            newToolbox.ToolboxTalkFK = toolbox.Key;
                            newToolbox.IsActive = true;
                            newToolbox.CreatedByFK = _userContext.UserDetails.ID;
                            newToolbox.CreatedDate = DateTime.Now;
                            newToolbox.UpdatedByFK = _userContext.UserDetails.ID;
                            newToolbox.UpdatedDate = DateTime.Now;

                            Context.WorkInstructionToolboxTalks.Add(newToolbox);

                            ToolboxTalk talk = Context.ToolboxTalks.Where(tt => tt.ToolboxTalkPK.Equals(toolbox.Key)).SingleOrDefault();

                            Briefing briefing = new Briefing();
                            briefing.BriefingPK = Guid.NewGuid();
                            briefing.BriefingTypeFK = 3; // Toolbox Talk
                            briefing.JobPackFK = current.JobPackFK;
                            briefing.Name = talk.ToolboxTalk1;
                            briefing.Details = talk.Body;
                            briefing.IsActive = true;
                            briefing.CreatedByID = _userContext.UserDetails.ID;
                            briefing.CreatedDate = DateTime.Now;
                            briefing.UpdatedByID = _userContext.UserDetails.ID;
                            briefing.UpdatedDate = DateTime.Now;

                            Context.Briefings.Add(briefing);
                        }
                    }

                    // update shift & shiftProgressInfo if shift has been assigned
                    if (model.StatusID != originalStatusFK && current.JobPack != null && current.JobPack.Shift != null)
                    {
                        // clear old completed and cancelled shiftProgressInfo
                        if (originalStatusFK == WorkInstructionStatusesEnum.Cancelled && model.StatusID != WorkInstructionStatusesEnum.Cancelled)
                        {
                            var shiftProgressInfos = current.JobPack.Shift.ShiftProgressInfoes.Where(a => a.ShiftProgress.Progress == "Cancelled").ToList();
                            Context.ShiftProgressInfoes.RemoveRange(shiftProgressInfos);
                        }
                        else if (originalStatusFK == WorkInstructionStatusesEnum.Complete && model.StatusID != WorkInstructionStatusesEnum.Complete)
                        {
                            var shiftProgressInfos = current.JobPack.Shift.ShiftProgressInfoes.Where(a => a.ShiftProgress.Progress == "Completed").ToList();
                            Context.ShiftProgressInfoes.RemoveRange(shiftProgressInfos);
                        }

                        // update shift and shiftProgressInfo
                        if (model.StatusID == WorkInstructionStatusesEnum.Cancelled)
                        {
                            current.JobPack.Shift.IsCancelled = true;
                            current.JobPack.Shift.ShiftComplete = false;

                            var now = DateTime.Now;
                            var shiftProgressInfo = new ShiftProgressInfo();
                            shiftProgressInfo.ShiftFK = (Guid)current.JobPack.ShiftFK;
                            shiftProgressInfo.ShiftProgressID = Context.ShiftProgresses.Where(a => a.Progress == "Cancelled").Select(a => a.ShiftProgressID).Single();
                            shiftProgressInfo.Time = now;
                            shiftProgressInfo.CreatedDate = now;
                            Context.ShiftProgressInfoes.Add(shiftProgressInfo);
                        }
                        else if (model.StatusID == WorkInstructionStatusesEnum.Complete)
                        {
                            current.JobPack.Shift.IsCancelled = false;
                            current.JobPack.Shift.ShiftComplete = true;

                            var now = DateTime.Now;
                            var shiftProgressInfo = new ShiftProgressInfo();
                            shiftProgressInfo.ShiftFK = (Guid)current.JobPack.ShiftFK;
                            shiftProgressInfo.ShiftProgressID = Context.ShiftProgresses.Where(a => a.Progress == "Completed").Select(a => a.ShiftProgressID).Single();
                            shiftProgressInfo.Time = now;
                            shiftProgressInfo.CreatedDate = now;
                            Context.ShiftProgressInfoes.Add(shiftProgressInfo);
                        }
                        else
                        {
                            current.JobPack.Shift.IsCancelled = false;
                            current.JobPack.Shift.ShiftComplete = false;
                        }
                    }

                    SaveChanges();

                    if (!current.IsActive)
                    {
                        if (current.JobPack.Shift != null)
                        {
                            current.JobPack.Shift.IsActive = false;
                            foreach (var staff in current.JobPack.Shift.ShiftStaffs)
                            {
                                staff.IsActive = false;
                            }
                            foreach (var vehicle in current.JobPack.Shift.ShiftVehicles)
                            {
                                vehicle.IsActive = false;
                            }
                            foreach (var schedule in current.Schedules)
                            {
                                schedule.IsActive = false;
                            }
                            foreach (var accident in current.JobPack.Shift.Accidents)
                            {
                                accident.IsActive = false;
                            }
                            foreach (var observation in current.JobPack.Shift.Observations)
                            {
                                observation.IsActive = false;
                            }
                        }
                        foreach (var briefing in current.JobPack.Briefings)
                        {
                            briefing.IsActive = false;
                        }

                        SaveChanges();                      
                    }

                    tran.Commit();
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    _errorLoggingService.CreateErrorLog("UpdateWorkInstruction", e, model.WorkInstructionID.ToString());
                }
            }
        }

        public bool DuplicateWorkInstruction(Guid parentWorkInstructionID, DateTime date, Guid jobTypeID, Guid? checklistID, Guid? installWIID, string comments, string tmActivities, RepeatWorkInstructionAddressModel addressModel)
        {
            WorkInstructionModel model = Single(parentWorkInstructionID);
            
            if (model != null)
            {
                Dictionary<Guid, string> drawings = GetDrawings(model.WorkInstructionID).OrderBy(s => s.Drawing.DrawingTitle).ToDictionary(k => k.DrawingFK, v => v.Drawing.DrawingTitle);
                
                Dictionary<Guid, string> methodStatements = MethodStatements.GetForWI(model.WorkInstructionID).OrderBy(s => s.MethodStatement.MethodStatementTitle).ToDictionary(k => k.MethodStatementFK, v => v.MethodStatement.MethodStatementTitle);
                
                Dictionary<Guid, string> toolboxTalks = ToolboxTalks.GetForWI(model.WorkInstructionID).OrderBy(s => s.ToolboxTalk.ToolboxTalk1).ToDictionary(k => k.ToolboxTalkFK, v => v.ToolboxTalk.ToolboxTalk1);

                int startHour = model.StartDate.Hour;
                int startMin = model.StartDate.Minute;

                int finishHour = model.FinishDate.Hour;
                int finishMin = model.FinishDate.Minute;

                model.StartDate = date.Date;
                model.StartDate = model.StartDate.AddHours(startHour);
                model.StartDate = model.StartDate.AddMinutes(startMin);

                model.FinishDate = date.Date;
                model.FinishDate = model.FinishDate.AddHours(finishHour);
                model.FinishDate = model.FinishDate.AddMinutes(finishMin);

                int? firstHour = model.FirstCone?.Hour;
                int? firstMin = model.FirstCone?.Minute;

                int? lastHour = model.LastCone?.Hour;
                int? lastMin = model.LastCone?.Minute;

                if (firstHour != null && firstMin != null)
                {
                    model.FirstCone = date.Date.AddHours((int)firstHour).AddMinutes((int)firstMin);
                }

                if (lastHour != null && lastMin != null)
                {
                    model.LastCone = date.Date.AddHours((int)lastHour).AddMinutes((int)lastMin);
                }

                if (model.StatusID != new Guid("BCF18FD2-B9AF-44B9-84C3-121854C844CB")) // New
                {
                    model.StatusID = new Guid("9265273E-C0CD-4EB7-933E-46689478C5F0"); // Ready to Schedule
                }

                model.AddressLine1 = addressModel.AddressLine1;
                model.AddressLine2 = addressModel.AddressLine2;
                model.AddressLine3 = addressModel.AddressLine3;
                model.City = addressModel.City;
                model.County = addressModel.County;
                model.Postcode = addressModel.Postcode;

                model.IsActive = true;
                model.IsScheduled = false;
                model.ParentWIFK = parentWorkInstructionID;
                model.JobTypeID = jobTypeID;
                model.JobType = Context.JobTypes.Where(a => a.JobTypePK == jobTypeID).Select(a => a.DisplayName).Single();
                model.ChecklistID = checklistID;
                model.InstallWIID = installWIID;
                model.Comments = comments;
                model.TrafficManagementActivities = tmActivities;
                model.IsOutOfHours = false;
                model.OrderNumber = 1;

                model.WorkInstructionID = Create(model, drawings, methodStatements, toolboxTalks, null);
                if (model.WorkInstructionID.Equals(Guid.Empty))
                {
                    return false;
                }

                _loadingSheetService.CreateDuplicateLoadingSheets(parentWorkInstructionID, model.WorkInstructionID);
            }

            return true;
        }

        public bool AddShiftProgress(ShiftProgressInfoModel model)
        {
            try
            {
                ShiftProgressInfo spi = new ShiftProgressInfo();
                spi.ShiftFK = model.ShiftID;
                spi.Time = model.CreatedDate;
                spi.CreatedDate = DateTime.Now;
                spi.ShiftProgressID = model.ShiftProgressID;

                Context.ShiftProgressInfoes.Add(spi);

                Context.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            

            return true;
        }

        public WorkInstructionModel Single(Guid ID)
        {
            return Context.WorkInstructions.Where(a => a.WorkInstructionPK.Equals(ID)).Select(WorkInstructionModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<WorkInstructionModel> GetActiveWorkInstructionsPaged(int rowCount, int page, string query, Guid JobID)
        {
            var baseQuery = Context.WorkInstructions.Where(x => x.JobFK == JobID).ActiveWorkInstructions().OrderByDescending(o => o.WorkInstructionNumber).WorkInstructionOverviewQuery(query).Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<WorkInstructionSimpleModel> GetActiveWorkInstructions(Guid JobID)
        {
            return Context.WorkInstructions.Where(x => x.JobFK == JobID).ActiveWorkInstructions().OrderByDescending(o => o.WorkInstructionNumber).Select(WorkInstructionSimpleModel.EntityToModel).ToList();
        }

        public bool UpdateWorkInstructionsToInvoiced(string[] tasks)
        {

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    Status invoicedStatus = Context.Status.Where(w => w.Status1 == "Invoiced").SingleOrDefault();

                    foreach (var taskID in tasks)
                    {
                        Guid taskGuid = new Guid(taskID);
                        WorkInstruction wi = Context.WorkInstructions.Where(w => w.WorkInstructionPK.Equals(taskGuid)).SingleOrDefault();
                        if (wi != null)
                        {
                            wi.Status = invoicedStatus;
                            wi.UpdatedByFK = _userContext.UserDetails.ID;
                            wi.UpdatedDate = DateTime.Now;
                        }
                    }
                    
                    SaveChanges();

                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    _errorLoggingService.CreateErrorLog("UpdateWorkInstructionsToInvoiced", e, "");
                    return false;
                }
            }    
        }


        public PagedResults<WorkInstructionModel> GetActiveWorkInstructionsAllPaged(int rowCount, int page, string query)
        {
            var isCustomer = _userContext.IsInRole("Customer");
            var customerFK = isCustomer ? Context.UserProfiles.Where(a => a.User.UserName == _userContext.Name).Select(a => a.CustomerFK).Single() : null;

            var baseQuery = Context.WorkInstructions
                .ActiveWorkInstructions()
                .Where(a => a.JobFK != null)
                .Where(a => isCustomer ? a.Job.Contract.CustomerFK == customerFK : true)
                .OrderByDescending(o => o.WorkInstructionNumber)
                .WorkInstructionOverviewQuery(query)
                .Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionModel> GetRepeatWorkInstructionsAllPaged(int rowCount, int page, string query, Guid workInstructionID)
        {
            var baseQuery = Context.WorkInstructions.ActiveWorkInstructions()
                .Where(a => a.JobFK != null)
                .Where(w => w.ParentWIFK.Value.Equals(workInstructionID))
                .OrderByDescending(o => o.WorkInstructionNumber)
                .WorkInstructionOverviewQuery(query)
                .Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionModel> GetActiveWorkInstructionsNewPaged(int rowCount, int page, string query)
        {
            var isCustomer = _userContext.IsInRole("Customer");
            var customerFK = isCustomer ? Context.UserProfiles.Where(a => a.User.UserName == _userContext.Name).Select(a => a.CustomerFK).Single() : null;

            var baseQuery = Context.WorkInstructions.ActiveWorkInstructions()
                .Where(a => a.JobFK != null)
                .Where(a => isCustomer ? a.Job.Contract.CustomerFK == customerFK : true)
                .Where(o => o.Status.Status1.Equals("New")).OrderByDescending(o => o.WorkInstructionNumber).WorkInstructionOverviewQuery(query).Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionModel> GetActiveWorkInstructionsScheduledPaged(int rowCount, int page, string query)
        {
            var isCustomer = _userContext.IsInRole("Customer");
            var customerFK = isCustomer ? Context.UserProfiles.Where(a => a.User.UserName == _userContext.Name).Select(a => a.CustomerFK).Single() : null;

            var baseQuery = Context.WorkInstructions.ActiveWorkInstructions()
                .Where(a => a.JobFK != null)
                .Where(a => isCustomer ? a.Job.Contract.CustomerFK == customerFK : true)
                .Where(o => o.Status.Status1.Equals("Scheduled")).OrderByDescending(o => o.WorkInstructionNumber).WorkInstructionOverviewQuery(query).Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionModel> GetActiveWorkInstructionsReadyPaged(int rowCount, int page, string query)
        {
            var isCustomer = _userContext.IsInRole("Customer");
            var customerFK = isCustomer ? Context.UserProfiles.Where(a => a.User.UserName == _userContext.Name).Select(a => a.CustomerFK).Single() : null;

            var baseQuery = Context.WorkInstructions.ActiveWorkInstructions()
                .Where(a => a.JobFK != null)
                .Where(a => isCustomer ? a.Job.Contract.CustomerFK == customerFK : true)
                .Where(o => o.Status.Status1.Equals("Ready To Schedule")).OrderByDescending(o => o.WorkInstructionNumber).WorkInstructionOverviewQuery(query).Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionModel> GetActiveWorkInstructionsCompletePaged(int rowCount, int page, string query)
        {
            var isCustomer = _userContext.IsInRole("Customer");
            var customerFK = isCustomer ? Context.UserProfiles.Where(a => a.User.UserName == _userContext.Name).Select(a => a.CustomerFK).Single() : null;

            var baseQuery = Context.WorkInstructions.ActiveWorkInstructions()
                .Where(a => a.JobFK != null)
                .Where(a => isCustomer ? a.Job.Contract.CustomerFK == customerFK : true)
                .Where(o => o.Status.Status1.Equals("Complete")).OrderByDescending(o => o.WorkInstructionNumber).WorkInstructionOverviewQuery(query).Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionModel> GetActiveWorkInstructionsInvoicedPaged(int rowCount, int page, string query)
        {
            var isCustomer = _userContext.IsInRole("Customer");
            var customerFK = isCustomer ? Context.UserProfiles.Where(a => a.User.UserName == _userContext.Name).Select(a => a.CustomerFK).Single() : null;

            var baseQuery = Context.WorkInstructions.ActiveWorkInstructions()
                .Where(a => a.JobFK != null)
                .Where(a => isCustomer ? a.Job.Contract.CustomerFK == customerFK : true)
                .Where(o => o.Status.Status1.Equals("Invoiced")).OrderByDescending(o => o.WorkInstructionNumber).WorkInstructionOverviewQuery(query).Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionModel> GetInactiveWorkInstructionsMainPaged(int rowCount, int page, string query)
        {
            var isCustomer = _userContext.IsInRole("Customer");
            var customerFK = isCustomer ? Context.UserProfiles.Where(a => a.User.UserName == _userContext.Name).Select(a => a.CustomerFK).Single() : null;

            var baseQuery = Context.WorkInstructions.InactiveWorkInstructions()
                .Where(a => a.JobFK != null)
                .Where(a => isCustomer ? a.Job.Contract.CustomerFK == customerFK : true)
                .OrderByDescending(o => o.WorkInstructionNumber).WorkInstructionOverviewQuery(query).Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<WorkInstructionModel> GetActiveWorkInstructionsToSchedulePaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.WorkInstructions.Where(x => !x.IsScheduled).ActiveWorkInstructions().OrderByDescending(o => o.WorkInstructionNumber).WorkInstructionOverviewQuery(query).Select(WorkInstructionModel.EntityToModel);

            return PagedResults<WorkInstructionModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ShiftProgressInfoModel> GetShiftProgressInfoPaged(int rowCount, int page, string query, Guid shiftID)
        {
            var baseQuery = Context.ShiftProgressInfoes.Where(s => s.ShiftFK.Equals(shiftID)).ShiftProgressInfoQuery(query).Select(ShiftProgressInfoModel.EntityToModel).OrderBy(s => s.Time);

            return PagedResults<ShiftProgressInfoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public WorkInstructionJobOverviewModel GetCurrentJobOverview(Guid jobID)
        {
            return Context.Jobs.Where(x => x.JobPK.Equals(jobID)).Select(WorkInstructionJobOverviewModel.EntityToModel).SingleOrDefault();
        }

        public List<WorkInstructionDrawing> GetDrawings(Guid workInstructionID)
        {
            return Context.WorkInstructionDrawings.Where(a => a.WorkInstructionFK.Equals(workInstructionID)).ToList();
        }

        public List<WorkInstructionStatusModel> GetWorkInstructionStatus()
        {
            return Context.Status.Where(s => s.IsWorkInstructionStatus).OrderBy(s => s.Status1).Select(WorkInstructionStatusModel.EntityToModel).ToList();
        }

        public List<ShiftProgressModel> GetShiftProgresses(Guid shiftID)
        {            
            return Context.ShiftProgresses.Where(s => !s.Progress.Equals("NotStarted") && !s.ShiftProgressInfoes.Any(sp => sp.ShiftFK.Equals(shiftID))).Select(ShiftProgressModel.EntityToModel).ToList();
        }

        public Dictionary<Guid, string> GetJobTypes()
        {
            return Context.JobTypes.Where(jt => jt.IsActive).OrderBy(jt => jt.DisplayName).ToDictionary(k => k.JobTypePK, v => v.DisplayName);
        }

        public PagedResults<InstallLookup> GetPagedInstallLookup(int rowCount, int page, string query, Guid? jobID)
        {
            int intQuery;
            var isIntQuery = int.TryParse(query, out intQuery);
    
            var baseQuery = Context.WorkInstructions
                .Where(a => 
                    (jobID != null ? a.JobFK == jobID : a.JobFK == a.JobFK ) &&
                    a.IsActive &&
                    a.JobType.JobType1 == JobTypesEnum.Install &&
                    a.Status.StatusPK != WorkInstructionStatusesEnum.Cancelled &&
                    !Context.WorkInstructions.Where(b => b.InstallWIPK == a.WorkInstructionPK).Any()
                )
                .Where(a =>
                    (query == null || query.Trim() == "" ? true : false) ||
                    (isIntQuery ? a.WorkInstructionNumber == intQuery : false) ||
                    (isIntQuery ? a.Job.JobNumber == intQuery : false)
                )
                .OrderByDescending(o => o.WorkInstructionNumber)
                .Select(a => new InstallLookup
                {
                    Key = SqlFunctions.StringConvert((decimal)a.WorkInstructionNumber).Trim(),
                    Value = a.WorkInstructionPK,
                    JobNumber = a.Job.JobNumber,
                    JonTitle = a.Job.JobTitle,
                    ClientName = a.Job.Contract.Customer.CustomerName,
                    ContractTitle = a.Job.Contract.ContractTitle
                });

            int recordCount = baseQuery.Count();

            return new PagedResults<InstallLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
            };
        }

        public EquipmentItemChecklistModel GetEquipmentItemChecklist(Guid id)
        {
            return Context.EquipmentItemChecklists.Where(a => a.EquipmentItemChecklistPK == id).Select(EquipmentItemChecklistModel.EntityToModel).Single();
        }

        public List<WorkInstructionPriceLineModel> GetWorkInstructionPriceLines(Guid workInstructionID)
        {
            return  Context.WorkInstructionPriceLines.Where(a => a.IsActive && a.WorkInstructionFK == workInstructionID).Select(WorkInstructionPriceLineModel.EntityToModel).ToList();
        }

        public PagedResults<WorkInstructionPriceLineModel> GetPagedActiveWorkInstructionPriceLines(int rowCount, int page, string query, Guid workInstructionID)
        {
            var baseQuery = Context.WorkInstructionPriceLines
                .Where(a => a.IsActive && a.WorkInstructionFK == workInstructionID)
                .Where(a =>
                    a.PriceLine.Code.Contains(query) ||
                    a.PriceLine.Description.Contains(query)
                    )
                .OrderBy(a => a.PriceLine.Code)
                .Select(WorkInstructionPriceLineModel.EntityToModel);

            return PagedResults<WorkInstructionPriceLineModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public Guid CreateWorkInstructionPriceLine(WorkInstructionPriceLineModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var jobID = Context.WorkInstructions.Where(a => a.WorkInstructionPK == model.WorkInstructionID).Select(a => a.JobFK).Single();

            model.WorkInstructionPriceLineID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            var workInstructionPriceLine = WorkInstructionPriceLineModel.ModelToEntity(model);
            Context.WorkInstructionPriceLines.Add(workInstructionPriceLine);

            var jobPriceLine = new JobPriceLine();
            jobPriceLine.JobPriceLinePK = Guid.NewGuid();
            jobPriceLine.JobFK = (Guid)jobID;
            jobPriceLine.PriceLineFK = (Guid)model.PriceLineFK;
            jobPriceLine.WorkInstructionPriceLine = workInstructionPriceLine;
            jobPriceLine.Rate = model.Rate;
            jobPriceLine.Note = model.Note;
            jobPriceLine.IsActive = model.IsActive;
            jobPriceLine.CreatedByFK = _userContext.UserDetails.ID;
            jobPriceLine.CreatedDate = DateTime.Now;
            jobPriceLine.UpdatedByFK = _userContext.UserDetails.ID;
            jobPriceLine.UpdatedDate = DateTime.Now;
            Context.JobPriceLines.Add(jobPriceLine);

            SaveChanges();

            return model.WorkInstructionPriceLineID;
        }

        public Guid UpdateWorkInstructionPriceLine(WorkInstructionPriceLineModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var current = Context.WorkInstructionPriceLines.Where(a => a.WorkInstructionPriceLinePK == model.WorkInstructionPriceLineID).Single();
            current.Rate = model.Rate;
            current.Quantity = model.Quantity;
            current.Note = model.Note;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            Context.SaveChanges();

            return model.WorkInstructionID;
        }

        public WorkInstructionPriceLineModel SingleWorkInstructionPriceLine(Guid id)
        {
            return Context.WorkInstructionPriceLines.Where(a => a.WorkInstructionPriceLinePK == id).Select(WorkInstructionPriceLineModel.EntityToModel).SingleOrDefault();
        }

        public Guid GetWorkInstructionStatusID(string name)
        {
            return Context.Status.Where(a => a.IsWorkInstructionStatus && a.IsActive && a.Status1 == name).Select(a => a.StatusPK).Single();
        }

        public bool JobHasCompleteWorkInstruction(Guid JobID)
        {
            return Context.WorkInstructions.Where(x => x.JobFK == JobID).ActiveWorkInstructions().Where(o => o.Status.Status1.Equals("Complete")).Any();
        }

        public bool AddPriceLines(Guid workInstructionID, Guid[] priceLineIDs)
        {
            var workInstruction = Context.WorkInstructions.Where(x => x.WorkInstructionPK == workInstructionID).SingleOrDefault();

            foreach (var id in priceLineIDs ?? new Guid[1])
            {
                var priceLine = Context.PriceLines.Where(x => x.PriceLinePK == id).SingleOrDefault();

                if (priceLine != null)
                {
                    using (var tran = Context.Database.BeginTransaction())
                    {
                        try
                        {
                            WorkInstructionPriceLine wiPriceLine = new WorkInstructionPriceLine()
                            {
                                WorkInstructionPriceLinePK = Guid.NewGuid(),
                                PriceLineFK = id,
                                WorkInstructionFK = workInstructionID,
                                Quantity = 1,
                                Rate = priceLine.DefaultRate,
                                IsActive = true,
                                CreatedByFK = _userContext.UserDetails.ID,
                                CreatedDate = DateTime.Now,
                                UpdatedByFK = _userContext.UserDetails.ID,
                                UpdatedDate = DateTime.Now
                            };
                            Context.WorkInstructionPriceLines.Add(wiPriceLine);

                            JobPriceLine jobPriceLine = new JobPriceLine()
                            {
                                JobPriceLinePK = Guid.NewGuid(),
                                JobFK = (Guid)workInstruction.JobFK,
                                PriceLineFK = id,
                                WorkInstructionPriceLineFK = wiPriceLine.WorkInstructionPriceLinePK,
                                Quantity = 1,
                                Rate = priceLine.DefaultRate,
                                IsActive = true,
                                CreatedByFK = _userContext.UserDetails.ID,
                                CreatedDate = DateTime.Now,
                                UpdatedByFK = _userContext.UserDetails.ID,
                                UpdatedDate = DateTime.Now
                            };
                            Context.JobPriceLines.Add(jobPriceLine);

                            Context.SaveChanges();
                            tran.Commit();
                        }
                        catch (Exception e)
                        {
                            tran.Rollback();
                            _errorLoggingService.CreateErrorLog("CreateWIPriceLine", e, workInstructionID.ToString());
                            return false;
                        }
                    }            
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        //public void SetDefaultMethodStatement(Guid workInstructionID,Guid MethodStatementID)
        //{
        //    WorkInstructionMethodStatement newMethodStatement = new WorkInstructionMethodStatement();
        //    newMethodStatement.WorkInstructionMethodStatementPK = Guid.NewGuid();
        //    newMethodStatement.WorkInstructionFK = model.WorkInstructionID;
        //    newMethodStatement.MethodStatementFK = methodStatement.Key;
        //    newMethodStatement.IsActive = true;
        //    newMethodStatement.CreatedByFK = _userContext.UserDetails.ID;
        //    newMethodStatement.CreatedDate = DateTime.Now;
        //    newMethodStatement.UpdatedByFK = _userContext.UserDetails.ID;
        //    newMethodStatement.UpdatedDate = DateTime.Now;

        //    Context.WorkInstructionMethodStatements.Add(newMethodStatement);

        //    MethodStatement method = Context.MethodStatements.Where(m => m.MethodStatementPK.Equals(methodStatement.Key)).SingleOrDefault();

        //    Briefing briefing = new Briefing();
        //    briefing.BriefingPK = Guid.NewGuid();
        //    briefing.BriefingTypeFK = 4; // Method Statement
        //    briefing.JobPackFK = jp.JobPackPK;
        //    briefing.Name = method.MethodStatementTitle;
        //    briefing.Details = "";
        //    briefing.FileFK = method.FileFK;
        //    briefing.IsActive = true;
        //    briefing.CreatedByID = _userContext.UserDetails.ID;
        //    briefing.CreatedDate = DateTime.Now;
        //    briefing.UpdatedByID = _userContext.UserDetails.ID;
        //    briefing.UpdatedDate = DateTime.Now;

        //    Context.Briefings.Add(briefing);
        //}

    }
}
