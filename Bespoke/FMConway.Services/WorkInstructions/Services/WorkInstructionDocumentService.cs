﻿using FMConway.Services.WorkInstructions.Models;
using FMConway.Services.Files.Interface;
using FMConway.Services.Files.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.WorkInstructions.Services
{
    public class WorkInstructionDocumentService : Service, FMConway.Services.WorkInstructions.Interfaces.IWorkInstructionDocumentService
    {
        UserContext _userContext;
        IFileService _fileService;

        public WorkInstructionDocumentService(UserContext userContext, IFileService fileService)
        {
            _userContext = userContext;
            _fileService = fileService;
        }

        public List<DocumentModel> GetActiveDocumentsForWorkInstruction(Guid workInstructionID)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            List<DocumentModel> documents = Context.WorkInstructionDrawings.Where(wi => wi.WorkInstructionFK.Equals(workInstructionID) && wi.IsActive).Select(DocumentModel.EntityToModel).ToList();

            return documents;
        }

        public DocumentModel GetDocument(Guid id)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.WorkInstructionDrawings.Where(wi => wi.WorkInstructionDrawingPK.Equals(id)).Select(DocumentModel.EntityToModel).SingleOrDefault();

            return document;
        }

        public DocumentModel GetJobDocument(Guid jobID, Guid drawingID)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.JobDrawings.Where(j => j.JobFK == jobID && j.DrawingFK == drawingID).Select(DocumentModel.JobETM).SingleOrDefault();

            return document;
        }

        public void Create(Guid workInstructionID, string drawingTitle, string drawingRevision, string fileName, string contentType, byte[] documentData)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            var file = new FileModel()
            {
                FileDownloadName = fileName,
                ContentType = contentType,
                Data = documentData
            };

            FileModel newFile = _fileService.Add(file);

            Drawing drawing = new Drawing();
            drawing.DrawingPK = Guid.NewGuid();
            drawing.DrawingTitle = drawingTitle;
            drawing.Revision = drawingRevision;
            drawing.FileFK = newFile.ID;
            drawing.IsActive = true;
            drawing.CreatedByFK = _userContext.UserDetails.ID;
            drawing.CreatedDate = DateTime.Now;
            drawing.UpdatedByFK = _userContext.UserDetails.ID;
            drawing.UpdatedDate = DateTime.Now;

            Context.Drawings.Add(drawing);

            WorkInstructionDrawing wiDrawing = new WorkInstructionDrawing();
            wiDrawing.WorkInstructionDrawingPK = Guid.NewGuid();
            wiDrawing.WorkInstructionFK = workInstructionID;
            wiDrawing.DrawingFK = drawing.DrawingPK;
            wiDrawing.IsActive = true;
            wiDrawing.CreatedByFK = _userContext.UserDetails.ID;
            wiDrawing.CreatedDate = DateTime.Now;
            wiDrawing.UpdatedByFK = _userContext.UserDetails.ID;
            wiDrawing.UpdatedDate = DateTime.Now;

            Context.WorkInstructionDrawings.Add(wiDrawing);

            var jobPackID = Context.WorkInstructions.Where(w => w.WorkInstructionPK == workInstructionID).Select(s => s.JobPackFK).SingleOrDefault();

            Briefing briefing = new Briefing();
            briefing.BriefingPK = Guid.NewGuid();
            briefing.BriefingTypeFK = 5; // Document
            briefing.JobPackFK = jobPackID;
            briefing.Name = fileName;
            briefing.Details = "";
            briefing.FileFK = newFile.ID;
            briefing.IsActive = true;
            briefing.CreatedByID = _userContext.UserDetails.ID;
            briefing.CreatedDate = DateTime.Now;
            briefing.UpdatedByID = _userContext.UserDetails.ID;
            briefing.UpdatedDate = DateTime.Now;

            Context.Briefings.Add(briefing);

            SaveChanges();
        }

        public void Update(DocumentModel document)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            WorkInstructionDrawing current = Context.WorkInstructionDrawings.Where(x => x.WorkInstructionDrawingPK.Equals(document.DocumentID)).SingleOrDefault();

            current.IsActive = document.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            var jobPackID = Context.WorkInstructions.Where(w => w.WorkInstructionPK == current.WorkInstructionFK).Select(s => s.JobPackFK).SingleOrDefault();

            Briefing briefing = Context.Briefings.Where(w => w.JobPackFK == jobPackID && w.FileFK == document.FileID && w.BriefingTypeFK.Equals(5)).SingleOrDefault();
            briefing.IsActive = document.IsActive;
            briefing.UpdatedByID = _userContext.UserDetails.ID;
            briefing.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public void Inactivate(Guid id)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            DocumentModel document = Context.WorkInstructionDrawings.Where(wi => wi.WorkInstructionDrawingPK.Equals(id)).Select(DocumentModel.EntityToModel).SingleOrDefault();
            if (document != null)
            {
                document.IsActive = false;
                Update(document);
            }
        }
    }
}
