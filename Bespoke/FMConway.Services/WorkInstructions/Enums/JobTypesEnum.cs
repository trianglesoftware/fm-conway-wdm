﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Enums
{
    public class JobTypesEnum
    {
        public const string Install = "Install";
        public const string Deinstall = "De-install";
        public const string Maintenance = "Maintenance";
        public const string CompleteInstall = "Complete Install - Man on Site - De-install";
        public const string VmsInstall = "VMS - Install and De-install";
        public const string BatteryChange = "Battery Change";
        public const string Jetting = "Jetting";
        public const string Tankering = "Tankering";
        public const string CCTV = "CCTV";
        public const string Test = "Test";
        public const string CyclicalWorksRiskAssessment = "Cyclical Works";
    }
}
