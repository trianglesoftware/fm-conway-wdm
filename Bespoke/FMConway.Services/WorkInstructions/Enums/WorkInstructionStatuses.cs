﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Enums
{
    public class WorkInstructionStatusesEnum
    {
        public static Guid New = new Guid("BCF18FD2-B9AF-44B9-84C3-121854C844CB");
        public static Guid ReadyToSchedule = new Guid("9265273E-C0CD-4EB7-933E-46689478C5F0");
        public static Guid Scheduled = new Guid("CDD506A6-670B-41AE-AF9B-8D21F0326717");
        public static Guid Cancelled = new Guid("12872C2E-6FAB-4113-9B3C-DDEE97AFA1DD");
        public static Guid Complete = new Guid("08C6E233-F47C-4B9A-9FA2-E558540B3EEC");
        public static Guid Invoiced = new Guid("C2800F3E-9B2A-4E42-B7B2-89A0A897A322");
    }
}
