﻿using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Interfaces
{
    public interface IWorkInstructionDocumentService : IService
    {
        void Create(Guid workInstructionID, string drawingTitle, string drawingRevision, string fileName, string contentType, byte[] documentData);

        void Update(DocumentModel document);
        void Inactivate(Guid id);
        DocumentModel GetDocument(Guid id);
        DocumentModel GetJobDocument(Guid jobID, Guid id);
        List<DocumentModel> GetActiveDocumentsForWorkInstruction(Guid workInstructionID);
    }
}
