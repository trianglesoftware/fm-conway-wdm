﻿using FMConway.Services.Areas.Interfaces;
using FMConway.Services.Checklists.Interfaces;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Contracts.Interfaces;
using FMConway.Services.JobTypes.Interfaces;
using FMConway.Services.MethodStatements.Interfaces;
using FMConway.Services.ToolboxTalks.Interfaces;
using FMConway.Services.WorkInstructionDetails.Models;
using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.WorkInstructions.Interfaces
{
    public interface IWorkInstructionService : IService
    {
        //IWorkInstructionStatusService Status { get; set; }
        IMethodStatementService MethodStatements { get; set; }
        IToolboxTalkService ToolboxTalks { get; set; }
        IJobTypeService JobTypes { get; set; }
        IChecklistService Checklists { get; set; }
        IWorkInstructionDocumentService Documents { get; set; }

        Guid Create(WorkInstructionModel model, Dictionary<Guid, string> drawings, Dictionary<Guid, string> methodStatements, Dictionary<Guid, string> toolboxTalks, List<WorkInstructionPriceLineModel> priceLineModel);

        void Update(WorkInstructionModel model,
        Dictionary<Guid, string> drawings, Dictionary<Guid, string> existingDrawings,
        Dictionary<Guid, string> methodStatements, Dictionary<Guid, string> existingMethodStatements,
        Dictionary<Guid, string> toolboxTalks, Dictionary<Guid, string> existingToolboxTalk);


        bool AddShiftProgress(ShiftProgressInfoModel model);
        bool DuplicateWorkInstruction(Guid parentWorkInstructionID, DateTime date, Guid jobTypeID, Guid? checklistID, Guid? installWIID, string comments, string tmActivities, RepeatWorkInstructionAddressModel addressModel);

        WorkInstructionModel Single(Guid ID);

        PagedResults<WorkInstructionModel> GetActiveWorkInstructionsPaged(int rowCount, int page, string query, Guid jobID);
        List<WorkInstructionSimpleModel> GetActiveWorkInstructions(Guid jobID);
        bool UpdateWorkInstructionsToInvoiced(string[] tasks);
        PagedResults<WorkInstructionModel> GetActiveWorkInstructionsAllPaged(int rowCount, int page, string query);
        PagedResults<WorkInstructionModel> GetRepeatWorkInstructionsAllPaged(int rowCount, int page, string query, Guid workInstructionID);
        PagedResults<WorkInstructionModel> GetActiveWorkInstructionsNewPaged(int rowCount, int page, string query);
        PagedResults<WorkInstructionModel> GetActiveWorkInstructionsScheduledPaged(int rowCount, int page, string query);
        PagedResults<WorkInstructionModel> GetActiveWorkInstructionsCompletePaged(int rowCount, int page, string query);
        PagedResults<WorkInstructionModel> GetActiveWorkInstructionsInvoicedPaged(int rowCount, int page, string query);
        PagedResults<WorkInstructionModel> GetActiveWorkInstructionsReadyPaged(int rowCount, int page, string query);
        PagedResults<ShiftProgressInfoModel> GetShiftProgressInfoPaged(int rowCount, int page, string query, Guid shiftID);

        PagedResults<WorkInstructionModel> GetInactiveWorkInstructionsMainPaged(int rowCount, int page, string query);
        PagedResults<WorkInstructionModel> GetActiveWorkInstructionsToSchedulePaged(int rowCount, int page, string query);

        WorkInstructionJobOverviewModel GetCurrentJobOverview(Guid jobID);
        List<WorkInstructionDrawing> GetDrawings(Guid workInstructionID);
        List<WorkInstructionStatusModel> GetWorkInstructionStatus();
        List<ShiftProgressModel> GetShiftProgresses(Guid shiftID);
        EquipmentItemChecklistModel GetEquipmentItemChecklist(Guid id);

        List<WorkInstructionPriceLineModel> GetWorkInstructionPriceLines(Guid workInstructionID);
        PagedResults<WorkInstructionPriceLineModel> GetPagedActiveWorkInstructionPriceLines(int rowCount, int page, string v, Guid workInstructionID);
        WorkInstructionPriceLineModel SingleWorkInstructionPriceLine(Guid id);
        Guid CreateWorkInstructionPriceLine(WorkInstructionPriceLineModel workInstructionPriceLine);
        Guid UpdateWorkInstructionPriceLine(WorkInstructionPriceLineModel workInstructionPriceLine);

        Guid GetWorkInstructionStatusID(string name);

        PagedResults<InstallLookup> GetPagedInstallLookup(int rowCount, int page, string query, Guid? jobID);
        Dictionary<Guid, string> GetJobTypes();

        bool JobHasCompleteWorkInstruction(Guid jobID);

        bool AddPriceLines(Guid workInstructionID, Guid[] priceLineIDs);
    }
}
