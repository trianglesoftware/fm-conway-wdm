﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.DisposalSites.Models
{
    public class DisposalSiteLookup : LookupBase<Guid>
    {
        public string Name { get; set; }

        public static Expression<Func<DisposalSite, DisposalSiteLookup>> EntityToModel = a => new DisposalSiteLookup
        {
            Value = a.DisposalSitePK,
            Key = a.Name,
        };
    }
}
