﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.DisposalSites.Models
{
    public class DisposalSiteList
    {
        public Guid DisposalSiteID { get; set; }
        public string Name { get; set; }

        public static Expression<Func<DisposalSite, DisposalSiteList>> EntityToModel = a => new DisposalSiteList
        {
            DisposalSiteID = a.DisposalSitePK,
            Name = a.Name,
        };
    }
}
