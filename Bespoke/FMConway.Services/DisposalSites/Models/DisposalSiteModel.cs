﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.DisposalSites.Models
{
    public class DisposalSiteModel
    {
        public Guid DisposalSiteID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<DisposalSite, DisposalSiteModel>> EntityToModel = a => new DisposalSiteModel
        {
            DisposalSiteID = a.DisposalSitePK,
            Name = a.Name,
            IsActive = a.IsActive,
            CreatedByID = a.CreatedByFK,
            CreatedDate = a.CreatedDate,
            UpdatedByID = a.UpdatedByFK,
            UpdatedDate = a.UpdatedDate
        };
    }
}
