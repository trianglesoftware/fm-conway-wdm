﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.DisposalSites.Extensions
{
    public static class DisposalSiteExtensions
    {
        public static IQueryable<DisposalSite> DisposalSiteOverviewQuery(this IQueryable<DisposalSite> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
