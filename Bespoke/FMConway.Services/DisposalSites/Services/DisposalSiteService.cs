﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.DisposalSites.Extensions;
using FMConway.Services.DisposalSites.Interfaces;
using FMConway.Services.DisposalSites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.DisposalSites.Services
{
    public class DisposalSiteService : Service, IDisposalSiteService
    {
        UserContext _userContext;

        public DisposalSiteService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<DisposalSiteList> GetActiveDisposalSites()
        {
            return Context.DisposalSites.Where(a => a.IsActive).Select(DisposalSiteList.EntityToModel).OrderBy(a => a.Name).ToList();
        }

        public List<DisposalSiteList> GetInactiveDisposalSites()
        {
            return Context.DisposalSites.Where(a => !a.IsActive).Select(DisposalSiteList.EntityToModel).OrderBy(a => a.Name).ToList();
        }

        public void Create(DisposalSiteModel model)
        {
            var userDetails = _userContext.UserDetails;

            var disposalSite = new DisposalSite();
            disposalSite.DisposalSitePK = Guid.NewGuid();
            disposalSite.IsActive = true;
            disposalSite.Name = model.Name;
            disposalSite.CreatedDate = DateTime.Now;
            disposalSite.CreatedByFK = userDetails.ID;
            disposalSite.UpdatedDate = DateTime.Now;
            disposalSite.UpdatedByFK = userDetails.ID;

            Context.DisposalSites.Add(disposalSite);

            SaveChanges();
        }

        public void Update(DisposalSiteModel model)
        {
            var disposalSite = Context.DisposalSites.Where(a => a.DisposalSitePK == model.DisposalSiteID).Single();
            disposalSite.Name = model.Name;
            disposalSite.IsActive = model.IsActive;
            disposalSite.UpdatedDate = DateTime.Now;
            disposalSite.UpdatedByFK = _userContext.UserDetails.ID;

            SaveChanges();
        }

        public DisposalSiteModel Single(Guid id)
        {
            return Context.DisposalSites.Where(a => a.DisposalSitePK == id).Select(DisposalSiteModel.EntityToModel).Single();
        }

        public PagedResults<DisposalSiteLookup> GetPagedActiveDisposalSitesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.DisposalSites
                .Where(a => a.IsActive)
                .DisposalSiteOverviewQuery(query)
                .OrderBy(a => a.Name)
                .Select(DisposalSiteLookup.EntityToModel);

            return PagedResults<DisposalSiteLookup>.GetPagedResults(baseQuery, rowCount, page);
        }
    }
}
