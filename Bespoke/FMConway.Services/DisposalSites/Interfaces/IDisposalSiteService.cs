﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.DisposalSites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.DisposalSites.Interfaces
{
    public interface IDisposalSiteService : IService
    {
        void Create(DisposalSiteModel model);
        List<DisposalSiteList> GetActiveDisposalSites();
        List<DisposalSiteList> GetInactiveDisposalSites();
        DisposalSiteModel Single(Guid id);
        void Update(DisposalSiteModel model);

        PagedResults<DisposalSiteLookup> GetPagedActiveDisposalSitesLookup(int rowCount, int page, string query);
    }
}
