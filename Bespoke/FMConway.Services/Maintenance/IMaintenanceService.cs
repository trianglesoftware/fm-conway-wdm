﻿using FMConway.Services.CertificateTypes.Interfaces;
using FMConway.Services.Checklists.Interfaces;
using FMConway.Services.EquipmentTypes.Interfaces;
using FMConway.Services.ReasonCodes.Interfaces;
using FMConway.Services.JobTypes.Interfaces;
using FMConway.Services.LoadingSheetTemplates.Interfaces;
using FMConway.Services.MedicalExaminationTypes.Interfaces;
using FMConway.Services.PriceLines.Interfaces;
using FMConway.Services.UserProfiles;
using FMConway.Services.VehicleTypes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.EmployeeAbsenceReasons.Interfaces;
using FMConway.Services.VehicleAbsenceReasons.Interfaces;
using FMConway.Services.WeatherConditions.Interfaces;
using FMConway.Services.DisposalSites.Interfaces;

namespace FMConway.Services.Maintenance
{
    public interface IMaintenanceService : IService
    {
        IUserProfileService UserProfiles { get; set; }
        IChecklistService Checklists { get; set; }
        IJobTypeService JobTypes { get; set; }
        IVehicleTypeService VehicleTypes { get; set; }
        IEquipmentTypeService EquipmentTypes { get; set; }
        IMedicalExaminationTypeService MedicalExaminationTypes { get; set; }
        ICertificateTypeService CertificateTypes { get; set; }
        ILoadingSheetTemplateService LoadingSheetTemplates { get; set; }
        IPriceLineService PriceLines { get; set; }
        IReasonCodeService ReasonCodes { get; set; }
        IWeatherConditionService WeatherConditions { get; set; }
        IEmployeeAbsenceReasonService EmployeeAbsenceReasons { get; set; }
        IVehicleAbsenceReasonService VehicleAbsenceReasons { get; set; }
        IDisposalSiteService DisposalSites { get; set; }
    }
}
