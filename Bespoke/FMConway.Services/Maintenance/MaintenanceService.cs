﻿using FMConway.Services.CertificateTypes.Interfaces;
using FMConway.Services.Checklists.Interfaces;
using FMConway.Services.EquipmentTypes.Interfaces;
using FMConway.Services.ReasonCodes.Interfaces;
using FMConway.Services.JobTypes.Interfaces;
using FMConway.Services.LoadingSheetTemplates.Interfaces;
using FMConway.Services.MedicalExaminationTypes.Interfaces;
using FMConway.Services.PriceLines.Interfaces;
using FMConway.Services.UserProfiles;
using FMConway.Services.VehicleTypes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.EmployeeAbsenceReasons.Interfaces;
using FMConway.Services.VehicleAbsenceReasons.Interfaces;
using FMConway.Services.WeatherConditions.Interfaces;
using FMConway.Services.DisposalSites.Interfaces;

namespace FMConway.Services.Maintenance
{
    public class MaintenanceService : Service, FMConway.Services.Maintenance.IMaintenanceService
    {
        public IUserProfileService UserProfiles { get; set; }
        public IChecklistService Checklists { get; set; }
        public IJobTypeService JobTypes { get; set; }
        public IVehicleTypeService VehicleTypes { get; set; }
        public IEquipmentTypeService EquipmentTypes { get; set; }
        public IMedicalExaminationTypeService MedicalExaminationTypes { get; set; }
        public ICertificateTypeService CertificateTypes { get; set; }
        public ILoadingSheetTemplateService LoadingSheetTemplates { get; set; }
        public IPriceLineService PriceLines { get; set; }
        public IReasonCodeService ReasonCodes { get; set; }
        public IWeatherConditionService WeatherConditions { get; set; }
        public IEmployeeAbsenceReasonService EmployeeAbsenceReasons { get; set; }
        public IVehicleAbsenceReasonService VehicleAbsenceReasons { get; set; }
        public IDisposalSiteService DisposalSites { get; set; }

    }
}
