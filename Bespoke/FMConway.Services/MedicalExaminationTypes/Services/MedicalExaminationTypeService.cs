﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.MedicalExaminationTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.MedicalExaminationTypes.Extensions;
using FMConway.Services.Jobs.Models;

namespace FMConway.Services.MedicalExaminationTypes.Services
{
    public class MedicalExaminationTypeService : Service, FMConway.Services.MedicalExaminationTypes.Interfaces.IMedicalExaminationTypeService
    {
        UserContext _userContext;

        public MedicalExaminationTypeService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public List<MedicalExaminationTypeModel> GetActiveMedicalExaminationTypes()
        {
            return Context.MedicalExaminationTypes.Where(a => a.IsActive).Select(MedicalExaminationTypeModel.EntityToModel).ToList();
        }

        public List<MedicalExaminationTypeModel> GetInactiveMedicalExaminationTypes()
        {
            return Context.MedicalExaminationTypes.Where(a => !a.IsActive).Select(MedicalExaminationTypeModel.EntityToModel).ToList();
        }

        public void Create(MedicalExaminationTypeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.MedicalExaminationTypeID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;

            Context.MedicalExaminationTypes.Add(MedicalExaminationTypeModel.ModelToEntity(model));

            SaveChanges();
        }

        public void Update(MedicalExaminationTypeModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            MedicalExaminationType current = Context.MedicalExaminationTypes.Where(x => x.MedicalExaminationTypePK.Equals(model.MedicalExaminationTypeID)).SingleOrDefault();

            current.IsActive = model.IsActive;
            current.UpdatedDate = DateTime.Now;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.Name = model.Name == null ? "" : model.Name;

            SaveChanges();
        }

        public MedicalExaminationTypeModel Single(Guid id)
        {
            return Context.MedicalExaminationTypes.Where(a => a.MedicalExaminationTypePK.Equals(id)).Select(MedicalExaminationTypeModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<MedicalExaminationTypeLookup> GetPagedActiveMedicalExaminationTypesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.MedicalExaminationTypes.ActiveMedicalExaminationTypes().OrderBy(o => o.Name).MedicalExaminationTypeOverviewQuery(query).Select(MedicalExaminationTypeLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<MedicalExaminationTypeLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }
    }
}
