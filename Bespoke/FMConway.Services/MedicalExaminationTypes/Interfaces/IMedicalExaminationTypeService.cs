﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Models;
using FMConway.Services.MedicalExaminationTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MedicalExaminationTypes.Interfaces
{
    public interface IMedicalExaminationTypeService : IService
    {
        void Create(MedicalExaminationTypeModel model);
        List<MedicalExaminationTypeModel> GetActiveMedicalExaminationTypes();
        List<MedicalExaminationTypeModel> GetInactiveMedicalExaminationTypes();
        MedicalExaminationTypeModel Single(Guid id);
        void Update(MedicalExaminationTypeModel model);

        PagedResults<MedicalExaminationTypeLookup> GetPagedActiveMedicalExaminationTypesLookup(int rowCount, int page, string query);
    }
}
