﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.MedicalExaminationTypes.Models
{
    public class MedicalExaminationTypeModel
    {
        public Guid MedicalExaminationTypeID { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<MedicalExaminationType, MedicalExaminationTypeModel>> EntityToModel = entity => new MedicalExaminationTypeModel
        {
            MedicalExaminationTypeID = entity.MedicalExaminationTypePK,
            Name = entity.Name,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<MedicalExaminationTypeModel, MedicalExaminationType> ModelToEntity = model => new MedicalExaminationType
        {
            MedicalExaminationTypePK = model.MedicalExaminationTypeID,
            Name = model.Name,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
