﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MedicalExaminationTypes.Models
{
    public class MedicalExaminationTypeLookup : LookupBase<Guid>
    {
        public static Expression<Func<MedicalExaminationType, MedicalExaminationTypeLookup>> EntityToModel = entity => new MedicalExaminationTypeLookup()
        {
            Key = entity.Name,
            Value = entity.MedicalExaminationTypePK,
        };
    }
}
