﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.MedicalExaminationTypes.Extensions
{
    public static class MedicalExaminationTypeExtensions
    {
        public static IQueryable<MedicalExaminationType> ActiveMedicalExaminationTypes(this IQueryable<MedicalExaminationType> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<MedicalExaminationType> InactiveMedicalExaminationTypes(this IQueryable<MedicalExaminationType> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<MedicalExaminationType> MedicalExaminationTypeOverviewQuery(this IQueryable<MedicalExaminationType> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
