﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.Helpers;
using FMConway.Services.Jobs.Models;
using FMConway.Services.OutOfHours.Extentions;
using FMConway.Services.OutOfHours.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace FMConway.Services.OutOfHours.Services
{
    public class OutOfHoursService : Service, Interfaces.IOutOfHoursService
    {
        UserContext _userContext;
        IErrorLoggingService _errorLoggingService;

        public OutOfHoursService(UserContext userContext, IErrorLoggingService errorLoggingService)
        {
            _userContext = userContext;
            _errorLoggingService = errorLoggingService;
        }

        public OutOfHoursModel GetOutOfHoursShift(Guid workInstructionID)
        {
            return Context.OutOfHours.Where(a => a.WorkInstructionFK == workInstructionID).Select(OutOfHoursModel.EntityToModel).Single();
        }

        public PagedResults<OutOfHoursList> GetOutOfHoursMainPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.OutOfHours
                .Where(a => a.IsActive && a.WorkInstruction.JobFK == null && a.WorkInstruction.IsOutOfHours)
                .OrderByDescending(o => o.CreatedDate)
                .OutOfHoursOverviewQuery(query)
                .Select(OutOfHoursList.EntityToModel);

            return PagedResults<OutOfHoursList>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<OutOfHoursList> GetAssignedOutOfHoursPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.OutOfHours
                .Where(a => a.IsActive && a.WorkInstruction.JobFK != null && a.WorkInstruction.IsOutOfHours)
                .OrderByDescending(o => o.CreatedDate)
                .OutOfHoursOverviewQuery(query)
                .Select(OutOfHoursList.EntityToModel);

            return PagedResults<OutOfHoursList>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<OutOfHoursList> GetInactiveOutOfHoursPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.OutOfHours
                .Where(a => !a.IsActive && a.WorkInstruction.IsOutOfHours)
                .OrderByDescending(o => o.CreatedDate)
                .OutOfHoursOverviewQuery(query)
                .Select(OutOfHoursList.EntityToModel);

            return PagedResults<OutOfHoursList>.GetPagedResults(baseQuery, rowCount, page);
        }

        public ServiceResults Save(OutOfHoursModel model)
        {
            var results = new ServiceResults();

            try
            {
                var userDetails = _userContext.UserDetails;
                var outOfHour = Context.OutOfHours.Where(a => a.WorkInstructionFK == model.WorkInstructionID).Single();

                if (outOfHour.WorkInstruction.JobFK != null)
                {
                    results.Errors.Add("", "Out of hours locked because job has been assigned");
                    return results;
                }

                outOfHour.CustomerFK = (Guid)model.CustomerID;
                outOfHour.IsActive = model.IsActive;
                outOfHour.UpdatedDate = DateTime.Now;
                outOfHour.UpdatedByID = userDetails.ID;

                if (model.IsActive)
                {
                    if (model.NewJob)
                    {
                        var job = new Job();
                        job.JobPK = Guid.NewGuid();
                        job.ContractFK = (Guid)model.ContractID;
                        job.DepotFK = (Guid)model.DepotID;
                        job.CustomerProjectCode = model.CustomerProjectCode;
                        job.JobTitle = model.JobTitle ?? "";
                        job.Description = "";
                        job.StartDate = (DateTime)model.StartDate;
                        job.EndDate = model.FinishDate;
                        job.AddressLine1 = outOfHour.WorkInstruction.AddressLine1 ?? "";
                        job.AddressLine2 = outOfHour.WorkInstruction.AddressLine2 ?? "";
                        job.AddressLine3 = outOfHour.WorkInstruction.AddressLine3 ?? "";
                        job.City = outOfHour.WorkInstruction.City ?? "";
                        job.County = outOfHour.WorkInstruction.County ?? "";
                        job.Postcode = outOfHour.WorkInstruction.Postcode ?? "";
                        job.Comments = "";
                        job.IsActive = true;
                        job.CreatedByFK = userDetails.ID;
                        job.CreatedDate = DateTime.Now;
                        job.UpdatedByFK = userDetails.ID;
                        job.UpdatedDate = DateTime.Now;

                        outOfHour.WorkInstruction.Job = job;

                        Context.Jobs.Add(job);
                    }
                    else
                    {
                        outOfHour.WorkInstruction.JobFK = model.JobID;
                    }
                }

                SaveChanges();
            }
            catch (Exception ex)
            {
                _errorLoggingService.CreateErrorLog("OutOfHours-Save", ex, model.WorkInstructionID.ToString());
                results.Errors.Add("", ex.Message);
            }

            return results;
        }
    }
}
