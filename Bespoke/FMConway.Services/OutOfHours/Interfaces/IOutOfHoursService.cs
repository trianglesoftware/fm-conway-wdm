﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Helpers;
using FMConway.Services.Jobs.Models;
using FMConway.Services.OutOfHours.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.OutOfHours.Interfaces
{
    public interface IOutOfHoursService : IService
    {
        PagedResults<OutOfHoursList> GetOutOfHoursMainPaged(int rowCount, int page, string query);
        PagedResults<OutOfHoursList> GetAssignedOutOfHoursPaged(int rowCount, int page, string query);
        PagedResults<OutOfHoursList> GetInactiveOutOfHoursPaged(int rowCount, int page, string query);
        OutOfHoursModel GetOutOfHoursShift(Guid workInstructionID);
        ServiceResults Save(OutOfHoursModel model);
    }
}
