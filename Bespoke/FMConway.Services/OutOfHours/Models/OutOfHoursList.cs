﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.OutOfHours.Models
{
    public class OutOfHoursList
    {
        public Guid WorkInstructionID { get; set; }
        public string WorkInstructionNumber { get; set; }
        public DateTime StartDate { get; set; }
        public string Address { get; set; }
        public int? JobNumber { get; set; }
        public string Customer { get; set; }
        public string IssuedBy { get; set; }
        public string JobType { get; set; }

        public static Expression<Func<OutOfHour, OutOfHoursList>> EntityToModel = a => new OutOfHoursList
        {
            WorkInstructionID = a.WorkInstructionFK,
            WorkInstructionNumber = a.WorkInstruction.WorkInstructionNumber.ToString(),
            StartDate = a.WorkInstruction.WorkInstructionStartDate,
            Address = StoreFunctions.WorkInstructionAddressSpaceDelim(a.WorkInstructionFK),
            JobNumber = a.WorkInstruction.Job.JobNumber,
            Customer = a.Customer.CustomerName,
            IssuedBy = a.WorkInstruction.Employee.EmployeeName,
            JobType = a.WorkInstruction.JobType.DisplayName,
            
        };
    }
}
