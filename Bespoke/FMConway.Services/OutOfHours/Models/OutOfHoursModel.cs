﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.OutOfHours.Models
{
    public class OutOfHoursModel
    {
        public Guid WorkInstructionID { get; set; }
        public Guid? CustomerID { get; set; }
        public string CustomerLookup { get; set; }
        public Guid? JobID { get; set; }
        public string JobLookup { get; set; }
        public string WorkInstructionNumber { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }

        public bool NewJob { get; set; }
        public Guid? ContractID { get; set; }
        public string ContractLookup { get; set; }
        public Guid? DepotID { get; set; }
        public string DepotLookup { get; set; }
        public string CustomerProjectCode { get; set; }
        public string JobTitle { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }

        public static Expression<Func<OutOfHour, OutOfHoursModel>> EntityToModel = a => new OutOfHoursModel
        {
            WorkInstructionID = a.WorkInstructionFK,
            CustomerID = a.CustomerFK,
            CustomerLookup = a.Customer.CustomerName,
            JobID = a.WorkInstruction.JobFK,
            JobLookup = a.WorkInstruction.Job.JobNumber.ToString(),
            WorkInstructionNumber = a.WorkInstruction.WorkInstructionNumber.ToString(),
            IsActive = a.IsActive,
            IsLocked = a.WorkInstruction.JobFK != null,

            StartDate = a.WorkInstruction.WorkInstructionStartDate,
            FinishDate = a.WorkInstruction.WorkInstructionFinishDate,
        };
    }
}
