﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.OutOfHours.Extentions
{
    public static class OutOfHoursExtensions
    {
        public static IQueryable<OutOfHour> OutOfHoursOverviewQuery(this IQueryable<OutOfHour> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                //model = model.Where(bq => bq.Customer.Contains(query) || bq.TMRequested.Contains(query) || bq.Location.Contains(query) || bq.Equipment.Contains(query));
                return model;
            }

            return model;
        }

    }
}
