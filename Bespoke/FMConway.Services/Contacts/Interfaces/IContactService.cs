﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Contacts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contacts.Interfaces
{
    public interface IContactService : IService
    {
        Guid Create(ContactModel model);
        void Update(ContactModel model);
        ContactModel Single(Guid ID);

        PagedResults<ContactModel> GetActiveCustomerContactsOverview(int rowCount, int page, string query, Guid customerID);
    }
}
