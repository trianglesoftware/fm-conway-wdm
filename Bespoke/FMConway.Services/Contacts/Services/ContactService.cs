﻿using FMConway.Services.Collections.Paging;
using FMConway.Services.Contacts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Contacts.Extensions;

namespace FMConway.Services.Contacts.Services
{
    public class ContactService : Service, FMConway.Services.Contacts.Interfaces.IContactService
    {
        UserContext _userContext;

        public ContactService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(ContactModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.ContactID = Guid.NewGuid();
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Contacts.Add(ContactModel.ModelToEntity(model));

            SaveChanges();

            return model.ContactID;
        }

        public void Update(ContactModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Contact current = Context.Contacts.Where(x => x.ContactPK.Equals(model.ContactID)).Single();

            current.ContactName = model.ContactName == null ? "" : model.ContactName;
            current.JobTitle = model.JobTitle == null ? "" : model.JobTitle;
            current.PhoneNumber = model.PhoneNumber == null ? "" : model.PhoneNumber;
            current.MobileNumber = model.MobileNumber == null ? "" : model.MobileNumber;
            current.EmailAddress = model.EmailAddress == null ? "" : model.EmailAddress;
            current.CustomerFK = model.CustomerID;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        public ContactModel Single(Guid ID)
        {
            return Context.Contacts.Where(a => a.ContactPK.Equals(ID)).Select(ContactModel.EntityToModel).SingleOrDefault();
        }

        public PagedResults<ContactModel> GetActiveCustomerContactsOverview(int rowCount, int page, string query, Guid customerID)
        {
            var baseQuery = Context.Contacts.Where(x=>x.CustomerFK.Equals(customerID)).ActiveContacts().OrderBy(o => o.ContactName).ContactOverviewQuery(query).Select(ContactModel.EntityToModel);

            return PagedResults<ContactModel>.GetPagedResults(baseQuery, rowCount, page);
        }
    }
}
