﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.Contacts.Models
{
    public class ContactModel
    {
        public Guid ContactID { get; set; }
        [Required]
        public string ContactName { get; set; }
        public string JobTitle { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public Guid CustomerID { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Contact, ContactModel>> EntityToModel = entity => new ContactModel
        {
            ContactID = entity.ContactPK,
            ContactName = entity.ContactName,
            JobTitle = entity.JobTitle,
            PhoneNumber = entity.PhoneNumber,
            MobileNumber = entity.MobileNumber,
            EmailAddress = entity.EmailAddress,
            CustomerID = entity.CustomerFK,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        public static Func<ContactModel, Contact> ModelToEntity = model => new Contact
        {
            ContactPK = model.ContactID,
            ContactName = model.ContactName == null ? "" : model.ContactName,
            JobTitle = model.JobTitle == null ? "" : model.JobTitle,
            PhoneNumber = model.PhoneNumber == null ? "" : model.PhoneNumber,
            MobileNumber = model.MobileNumber == null ? "" : model.MobileNumber,
            EmailAddress = model.EmailAddress == null ? "" : model.EmailAddress,
            CustomerFK = model.CustomerID,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
