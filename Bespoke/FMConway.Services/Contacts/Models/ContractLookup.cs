﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contracts.Models
{
    public class ContractLookup : LookupBase<Guid>
    {
        public string ContractTitle { get; set; }

        public static Expression<Func<Contract, ContractLookup>> EntityToModel = a => new ContractLookup
        {
            Key = a.ContractNumber,
            Value = a.ContractPK,
            ContractTitle = a.ContractTitle
        };
    }

    public class ContractSchemeLookup : LookupBase<Guid>
    {
        public string Contract { get; set; }
        public Guid? SchemeID { get; set; }
        public string Scheme { get; set;}

        public static Expression<Func<Scheme, ContractSchemeLookup>> EntityToModel = a => new ContractSchemeLookup
        {
            Key = a.Contract.ContractNumber,
            Value = a.Contract.ContractPK,
            Contract = a.Contract.ContractTitle,
            SchemeID = a.SchemePK,
            Scheme = a.SchemeTitle
        };

        public static Expression<Func<Contract, ContractSchemeLookup>> NoSchemeETM = a => new ContractSchemeLookup
        {
            Key = a.ContractNumber,
            Value = a.ContractPK,
            Contract = a.ContractTitle,
            SchemeID = null,
            Scheme = a.ContractTitle
        };

    }

}
