﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Contacts.Extensions
{
    public static class ContactExtensions
    {
        public static IQueryable<Contact> ActiveContacts(this IQueryable<Contact> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<Contact> ContactOverviewQuery(this IQueryable<Contact> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ContactName.Contains(query) ||
                                          bq.JobTitle.Contains(query) ||
                                          bq.PhoneNumber.Contains(query));
            }

            return model;
        }
    }
}
