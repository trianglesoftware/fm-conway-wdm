﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FMConway.Services.Areas.Models
{
    public class AreaModel
    {
        public Guid AreaID { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string CallProtocol { get; set; }
        public string RCCNumber { get; set; }
        public string NCCNumber { get; set; }

        public Guid WorkInstructionID { get; set; }
        public Guid WorkInstructionAreaPK { get; set; }

        public bool IsActive { get; set; }
        public Guid CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UpdatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }

        public static Expression<Func<Area, AreaModel>> EntityToModel = entity => new AreaModel
        {
            AreaID = entity.AreaPK,
            Name = entity.Area1,
            CallProtocol = entity.CallProtocol,
            RCCNumber = entity.RCCNumber,
            NCCNumber = entity.NCCNumber,

            IsActive = entity.IsActive,
            CreatedByID = entity.CreatedByFK,
            CreatedDate = entity.CreatedDate,
            UpdatedByID = entity.UpdatedByFK,
            UpdatedDate = entity.UpdatedDate
        };

        //public static Expression<Func<WorkInstructionArea, AreaModel>> WorkInstructionAreaEntityToModel = entity => new AreaModel
        //{
        //    WorkInstructionAreaPK = entity.WorkInstructionAreaPK,
        //    WorkInstructionID = entity.WorkInstructionFK,
        //    AreaID = entity.Area.AreaPK,
        //    Name = entity.Area.Area1,
        //    CallProtocol = entity.AreaNOMCommunicationProcedure,
        //    RCCNumber = entity.RCCNumber,
        //    NCCNumber = entity.NCCNumber,

        //    IsActive = entity.IsActive,
        //    CreatedByID = entity.CreatedByFK,
        //    CreatedDate = entity.CreatedDate,
        //    UpdatedByID = entity.UpdatedByFK,
        //    UpdatedDate = entity.UpdatedDate
        //};

        public static Func<AreaModel, Area> ModelToEntity = model => new Area
        {
            AreaPK = model.AreaID,
            Area1 = model.Name,
            CallProtocol = model.CallProtocol,
            RCCNumber = model.RCCNumber,
            NCCNumber = model.NCCNumber,

            IsActive = model.IsActive,
            CreatedByFK = model.CreatedByID,
            CreatedDate = model.CreatedDate,
            UpdatedByFK = model.UpdatedByID,
            UpdatedDate = model.UpdatedDate
        };
    }
}
