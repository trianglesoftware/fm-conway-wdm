﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Areas.Models
{
    public class AreaLookup : LookupBase<Guid>
    {
        public string CallProtocol { get; set; }

        public static Expression<Func<Area, AreaLookup>> EntityToModel = entity => new AreaLookup()
        {
            Key = entity.Area1,
            Value = entity.AreaPK,
            CallProtocol = entity.CallProtocol
        };
    }
}
