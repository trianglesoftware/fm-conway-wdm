﻿using FMConway.Services.Areas.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Areas.Interfaces
{
    public interface IAreaService : IService
    {
        Guid Create(AreaModel model);
        //Guid CreateWorkInstructionArea(AreaModel model);
        void Update(AreaModel model);
        //void UpdateWorkInstructionArea(AreaModel model);
        AreaModel Single(Guid ID);
        //AreaModel SingleWorkInstructionArea(Guid ID);
        List<JobArea> Get(Guid jobID);

        List<AreaModel> GetAllActiveAreas();
        PagedResults<AreaModel> GetActiveAreasPaged(int rowCount, int page, string query);

        List<AreaModel> GetAllInactiveAreas();
        PagedResults<AreaModel> GetInactiveAreasPaged(int rowCount, int page, string query);

        //PagedResults<AreaModel> GetActiveWorkInstructionAreasPaged(int rowCount, int page, string query, Guid workInstructionID);

        PagedResults<AreaLookup> GetPagedActiveAreasLookup(int rowCount, int page, string query);
    }
}
