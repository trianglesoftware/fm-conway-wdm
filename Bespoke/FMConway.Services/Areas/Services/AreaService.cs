﻿using FMConway.Services.Areas.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Areas.Extensions;

namespace FMConway.Services.Areas.Services
{
    public class AreaService : Service, FMConway.Services.Areas.Interfaces.IAreaService
    {
        UserContext _userContext;

        public AreaService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Guid Create(AreaModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            model.AreaID = Guid.NewGuid();
            model.Name = model.Name == null ? "" : model.Name;
            model.RCCNumber = model.RCCNumber == null ? "" : model.RCCNumber;
            model.NCCNumber = model.NCCNumber == null ? "" : model.NCCNumber;
            model.CallProtocol = model.CallProtocol == null ? "" : model.CallProtocol;
            model.IsActive = true;
            model.CreatedByID = _userContext.UserDetails.ID;
            model.CreatedDate = DateTime.Now;
            model.UpdatedByID = _userContext.UserDetails.ID;
            model.UpdatedDate = DateTime.Now;

            Context.Areas.Add(AreaModel.ModelToEntity(model));

            SaveChanges();

            return model.AreaID;
        }

        //public Guid CreateWorkInstructionArea(AreaModel model)
        //{
        //    if (!_userContext.IsAuthenticated)
        //        throw new AuthenticationException();

        //    WorkInstructionArea newArea = new WorkInstructionArea();
        //    newArea.WorkInstructionAreaPK = Guid.NewGuid();
        //    newArea.WorkInstructionFK = model.WorkInstructionID;
        //    newArea.AreaFK = model.AreaID;
        //    newArea.RCCNumber = model.RCCNumber;
        //    newArea.NCCNumber = model.NCCNumber;
        //    newArea.AreaNOMCommunicationProcedure = model.CallProtocol;
        //    newArea.IsActive = true;
        //    newArea.CreatedByFK = _userContext.UserDetails.ID;
        //    newArea.CreatedDate = DateTime.Now;
        //    newArea.UpdatedByFK = _userContext.UserDetails.ID;
        //    newArea.UpdatedDate = DateTime.Now;

        //    Context.WorkInstructionAreas.Add(newArea);

        //    SaveChanges();

        //    return model.AreaID;
        //}

        public void Update(AreaModel model)
        {
            if (!_userContext.IsAuthenticated)
                throw new AuthenticationException();

            Area current = Context.Areas.Where(x => x.AreaPK.Equals(model.AreaID)).Single();

            current.Area1 = model.Name == null ? "" : model.Name;
            current.CallProtocol = model.CallProtocol == null ? "" : model.CallProtocol;
            current.RCCNumber = model.RCCNumber == null ? "" : model.RCCNumber;
            current.NCCNumber = model.NCCNumber == null ? "" : model.NCCNumber;

            current.IsActive = model.IsActive;
            current.UpdatedByFK = _userContext.UserDetails.ID;
            current.UpdatedDate = DateTime.Now;

            SaveChanges();
        }

        //public void UpdateWorkInstructionArea(AreaModel model)
        //{
        //    if (!_userContext.IsAuthenticated)
        //        throw new AuthenticationException();

        //    WorkInstructionArea current = Context.WorkInstructionAreas.Where(x => x.WorkInstructionAreaPK.Equals(model.WorkInstructionAreaPK)).Single();

        //    current.AreaFK = model.AreaID;
        //    current.AreaNOMCommunicationProcedure = model.CallProtocol == null ? "" : model.CallProtocol;
        //    current.RCCNumber = model.RCCNumber == null ? "" : model.RCCNumber;
        //    current.NCCNumber = model.NCCNumber == null ? "" : model.NCCNumber;

        //    current.IsActive = model.IsActive;
        //    current.UpdatedByFK = _userContext.UserDetails.ID;
        //    current.UpdatedDate = DateTime.Now;

        //    SaveChanges();
        //}

        public AreaModel Single(Guid ID)
        {
            return Context.Areas.Where(a => a.AreaPK.Equals(ID)).Select(AreaModel.EntityToModel).SingleOrDefault();
        }

        //public AreaModel SingleWorkInstructionArea(Guid ID)
        //{
        //    return Context.WorkInstructionAreas.Where(a => a.WorkInstructionAreaPK.Equals(ID)).Select(AreaModel.WorkInstructionAreaEntityToModel).SingleOrDefault();
        //}

        public List<AreaModel> GetAllActiveAreas()
        {
            return Context.Areas.ActiveAreas().Select(AreaModel.EntityToModel).ToList();
        }

        public PagedResults<AreaModel> GetActiveAreasPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Areas.ActiveAreas().OrderBy(o => o.Area1).AreaOverviewQuery(query).Select(AreaModel.EntityToModel);

            return PagedResults<AreaModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public List<AreaModel> GetAllInactiveAreas()
        {
            return Context.Areas.InactiveAreas().Select(AreaModel.EntityToModel).ToList();
        }

        public PagedResults<AreaModel> GetInactiveAreasPaged(int rowCount, int page, string query)
        {
            var baseQuery = Context.Areas.InactiveAreas().OrderBy(o => o.Area1).AreaOverviewQuery(query).Select(AreaModel.EntityToModel);

            return PagedResults<AreaModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        //public PagedResults<AreaModel> GetActiveWorkInstructionAreasPaged(int rowCount, int page, string query, Guid workInstructionID)
        //{
        //    var baseQuery = Context.WorkInstructionAreas.Where(x => x.WorkInstructionFK.Equals(workInstructionID)).OrderBy(o => o.Area.Area1).AreaOverviewQuery(query).Select(AreaModel.WorkInstructionAreaEntityToModel);

        //    return PagedResults<AreaModel>.GetPagedResults(baseQuery, rowCount, page);
        //}

        public PagedResults<AreaLookup> GetPagedActiveAreasLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Areas.ActiveAreas().OrderBy(o => o.Area1).AreaOverviewQuery(query).Select(AreaLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<AreaLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
			};
        }

        public List<JobArea> Get(Guid jobID)
        {
            return Context.JobAreas.Where(a => a.JobFK == jobID).ToList();
        }
    }
}
