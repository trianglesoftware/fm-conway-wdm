﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Areas.Extensions
{
    public static class AreaExtensions
    {
        public static IQueryable<Area> ActiveAreas(this IQueryable<Area> model)
        {
            return model.Where(a => a.IsActive);
        }

        public static IQueryable<Area> InactiveAreas(this IQueryable<Area> model)
        {
            return model.Where(a => !a.IsActive);
        }

        public static IQueryable<Area> AreaOverviewQuery(this IQueryable<Area> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Area1.Contains(query) ||
                                          bq.CallProtocol.Contains(query) ||
                                          bq.RCCNumber.Contains(query) ||
                                          bq.NCCNumber.Contains(query));
            }

            return model;
        }

        //public static IQueryable<Area> AreaOverviewQuery(this IQueryable<Area> model, string query)
        //{
        //    if (!string.IsNullOrEmpty(query))
        //    {
        //        model = model.Where(bq => bq.Area.Area1.Contains(query) ||
        //                                  bq.AreaNOMCommunicationProcedure.Contains(query) ||
        //                                  bq.RCCNumber.Contains(query) ||
        //                                  bq.NCCNumber.Contains(query));
        //    }

        //    return model;
        //}
    }
}
