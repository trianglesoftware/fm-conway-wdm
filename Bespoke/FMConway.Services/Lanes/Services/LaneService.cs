﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using FMConway.Services.Depots.Extensions;
using FMConway.Services.Lanes.Models;
using FMConway.Services.Lanes.Extensions;

namespace FMConway.Services.Lanes.Services
{
    public class LaneService : Service, FMConway.Services.Lanes.Interfaces.ILanesService
    {
        UserContext _userContext;

        public LaneService(UserContext userContext)
        {
            _userContext = userContext;
        }
		
        public PagedResults<LaneLookup> GetPagedActiveLanesLookup(int rowCount, int page, string query)
        {
            var baseQuery = Context.Lanes.OrderBy(o => o.Name).LaneOverviewQuery(query).Select(LaneLookup.EntityToModel);

            int recordCount = baseQuery.Count();

            return new PagedResults<LaneLookup>()
            {
                Page = page,
                PageCount = (int)Math.Ceiling((decimal)recordCount / rowCount),
                Data = baseQuery.Skip(rowCount * page).Take(rowCount).ToList()
            };
        }
    }
}
