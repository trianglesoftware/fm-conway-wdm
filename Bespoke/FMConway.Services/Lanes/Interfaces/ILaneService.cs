﻿using FMConway.Services.Depots.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Lanes.Models;

namespace FMConway.Services.Lanes.Interfaces
{
    public interface ILanesService : IService
    {
        PagedResults<LaneLookup> GetPagedActiveLanesLookup(int rowCount, int page, string query);
    }
}
