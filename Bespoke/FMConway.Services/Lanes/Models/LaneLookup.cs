﻿using FMConway.Services.Collections.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Lanes.Models
{
    public class LaneLookup : LookupBase<Guid>
    {
        public static Expression<Func<Lane, LaneLookup>> EntityToModel = entity => new LaneLookup()
        {
            Key = entity.Name,
            Value = entity.LanePK
        };
    }
}
