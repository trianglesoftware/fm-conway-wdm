﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Lanes.Extensions
{
    public static class LaneExtensions
    {
        public static IQueryable<Lane> LaneOverviewQuery(this IQueryable<Lane> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.Name.Contains(query));
            }

            return model;
        }
    }
}
