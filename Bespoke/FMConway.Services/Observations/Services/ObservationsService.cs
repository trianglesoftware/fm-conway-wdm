﻿using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;
using System.Security.Authentication;
using FMConway.Services.Observations.Services;
using FMConway.Services.Observations.Extensions;
using FMConway.Services.Observations.Models;
using FMConway.Services.WorkInstructions.Interfaces;

namespace FMConway.Services.Observations.Services
{
    public class ObservationsService : Service, FMConway.Services.Observations.Interfaces.IObservationsService
    {
        UserContext _userContext;

        public ObservationsService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public PagedResults<ObservationModel> GetObservationsPaged(int rowCount, int page, string query, Guid shiftID)
        {
            var baseQuery = Context.Observations.Where(o => o.ShiftFK.Value.Equals(shiftID) && o.IsActive).OrderBy(o => o.ObservationDate).ObservationOverviewQuery(query).Select(ObservationModel.EntityToModel);

            return PagedResults<ObservationModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ObservationModel> GetObservationsForJobPaged(int rowCount, int page, string query, Guid jobID)
        {
            var baseQuery = Context.Observations.Where(o => o.Shift.JobPacks.Any(jp => jp.WorkInstructions.Any(wi => wi.JobFK == jobID))).OrderBy(o => o.ObservationDate).ObservationOverviewQuery(query).Select(ObservationModel.EntityToModel);

            return PagedResults<ObservationModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ObservationModel> GetObservationsWithShifts(int rowCount, int page, string query)
        {
            var baseQuery = Context.Observations.Where(o => o.ShiftFK.HasValue).OrderByDescending(o => o.ObservationDate).ObservationOverviewQuery(query).Select(ObservationModel.EntityToModel);

            return PagedResults<ObservationModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ObservationModel> GetObservationsWithoutShifts(int rowCount, int page, string query)
        {
            var baseQuery = Context.Observations.Where(o => !o.ShiftFK.HasValue).OrderByDescending(o => o.ObservationDate).ObservationOverviewQuery(query).Select(ObservationModel.EntityToModel);

            return PagedResults<ObservationModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public PagedResults<ObservationPhotoModel> GetObservationPhotosPaged(int rowCount, int page, string query, Guid observationID)
        { 
            var baseQuery = Context.ObservationPhotos.Where(op => op.ObservationFK.Equals(observationID)).ObservationPhotoQuery(query).OrderBy(op => op.CreatedDate).Select(ObservationPhotoModel.ETM);

            return PagedResults<ObservationPhotoModel>.GetPagedResults(baseQuery, rowCount, page);
        }

        public ObservationModel SingleObservation(Guid observationID)
        {
            return Context.Observations.Where(o => o.ObservationPK.Equals(observationID) && o.IsActive).Select(ObservationModel.EntityToModel).SingleOrDefault();
        }

    }
}
