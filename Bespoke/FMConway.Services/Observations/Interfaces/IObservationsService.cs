﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Observations.Models;

namespace FMConway.Services.Observations.Interfaces
{
    public interface IObservationsService : IService
    {
        PagedResults<ObservationModel> GetObservationsPaged(int rowCount, int page, string query, Guid shiftID);

        PagedResults<ObservationModel> GetObservationsForJobPaged(int rowCount, int page, string query, Guid jobID);

        PagedResults<ObservationPhotoModel> GetObservationPhotosPaged(int rowCount, int page, string query, Guid observationID);

        PagedResults<ObservationModel> GetObservationsWithShifts(int rowCount, int page, string query);

        PagedResults<ObservationModel> GetObservationsWithoutShifts(int rowCount, int page, string query);

        ObservationModel SingleObservation(Guid observationID);
    }
}
