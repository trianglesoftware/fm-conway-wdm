﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Observations.Models
{
    public class ObservationModel
    {
        public Guid ObservationID { get; set; }
        public string ObservationDescription { get; set; }
        public DateTime ObservationDate { get; set; }
        public Guid? ShiftID { get; set; }
        public string Location { get; set; }
        public string PeopleInvolved { get; set; }
        public string CauseOfProblem { get; set; }
        
        public static Expression<Func<Observation, ObservationModel>> EntityToModel = entity => new ObservationModel() 
        {
            ObservationID = entity.ObservationPK,
            ObservationDescription = entity.ObservationDescription,
            ObservationDate = entity.ObservationDate,
            ShiftID = entity.ShiftFK,
            Location = entity.Location,
            PeopleInvolved = entity.PeopleInvolved,
            CauseOfProblem = entity.CauseOfProblem
        };
    }

    public class ObservationPhotoModel
    {
        public Guid ObservationID { get; set; }
        public Guid ObservationPhotoID { get; set; }
        public Guid FileID { get; set; }
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }

        public static Expression<Func<ObservationPhoto, ObservationPhotoModel>> ETM = e => new ObservationPhotoModel()
        {
            ObservationID = e.ObservationFK,
            ObservationPhotoID = e.ObservationPhotoPK,
            FileID = e.FileFK,
            Date = e.CreatedDate,
            FileName = e.File.FileDownloadName,
            Latitude = e.File.Latitude ?? 0.00m,
            Longitude = e.File.Longitude ?? 0.00m
        };
    }
}
