﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.Services.Observations.Extensions
{
    public static class ObservationsExtensions
    {
        public static IQueryable<Observation> ObservationOverviewQuery(this IQueryable<Observation> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.ObservationDescription.Contains(query) || bq.Location.Contains(query));
            }

            return model;
        }

        public static IQueryable<ObservationPhoto> ObservationPhotoQuery(this IQueryable<ObservationPhoto> model, string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                model = model.Where(bq => bq.File.FileDownloadName.Contains(query));
            }

            return model;
        }
    }
}
