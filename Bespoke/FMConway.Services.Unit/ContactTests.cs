﻿using ClancyPlant.Services.Contacts.Models;
using ClancyPlant.Services.Contacts.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace ClancyPlant.Services.Unit
{
    [TestFixture]
    public class ContactTests
    {
        ContactService _service;
        Mock<HWMartinEntities> _entities;
        Mock<DbSet<Contact>> _contact;
        Guid userID = new Guid("12345678-1234-1234-1234-123412345678");

        [SetUp]
        public void Setup()
        {
            var c = new Mock<AspUserContext>(null);
            c.Setup(s => s.IsAuthenticated).Returns(true);
            c.Setup(s => s.UserDetails.ID).Returns(userID);

            var data = new List<Contact>
            {
                new Contact { ContactPK = Guid.Empty }
            }.AsQueryable();

            _contact = new Mock<DbSet<Contact>>();
            _contact.As<IQueryable<Contact>>().Setup(m => m.Provider).Returns(data.Provider);
            _contact.As<IQueryable<Contact>>().Setup(m => m.Expression).Returns(data.Expression);
            _contact.As<IQueryable<Contact>>().Setup(m => m.ElementType).Returns(data.ElementType);
            _contact.As<IQueryable<Contact>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _entities = new Moq.Mock<HWMartinEntities>();
            _entities.Setup(s => s.Contacts).Returns(_contact.Object);

            _service = new ContactService(c.Object);
            _service.SetContext(_entities.Object, false);
        }

        [Test]
        public void Creating_new_contact()
        {
            var ob = new ContactModel();

            _service.Create(ob);

            Assert.IsNotNull(ob.ContactID);
            Assert.AreEqual(true, ob.IsActive);
            Assert.AreEqual(userID, ob.CreatedByID);
            Assert.AreEqual(userID, ob.UpdatedByID);

            _contact.Verify(v => v.Add(It.IsAny<Contact>()), Times.Once());
            _entities.Verify(v => v.SaveChanges());
        }

        [Test]
        public void Updating_contact()
        {
            var ob = new ContactModel() { ContactID = Guid.Empty };

            _service.Update(ob);

            var contact = _service.Single(Guid.Empty);

            Assert.AreEqual(Guid.Empty, contact.CreatedByID);
            Assert.AreEqual(userID, contact.UpdatedByID);

            _entities.Verify(v => v.SaveChanges());
        }

    }
}
