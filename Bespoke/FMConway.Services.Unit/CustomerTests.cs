﻿using ClancyPlant.Services.Customers.Models;
using ClancyPlant.Services.Customers.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;

namespace ClancyPlant.Services.Unit
{
    [TestFixture]
    public class CustomerTests
    {
        CustomerService _service;
        Mock<HWMartinEntities> _entities;
        Mock<DbSet<Customer>> _customer;
        Guid userID = new Guid("12345678-1234-1234-1234-123412345678");

        [SetUp]
        public void Setup()
        {
            var c = new Mock<AspUserContext>(null);
            c.Setup(s => s.IsAuthenticated).Returns(true);
            c.Setup(s => s.UserDetails.ID).Returns(userID);

            var data = new List<Customer>
            {
                new Customer { CustomerPK = Guid.Empty }
            }.AsQueryable();

            _customer = new Mock<DbSet<Customer>>();
            _customer.As<IQueryable<Customer>>().Setup(m => m.Provider).Returns(data.Provider);
            _customer.As<IQueryable<Customer>>().Setup(m => m.Expression).Returns(data.Expression);
            _customer.As<IQueryable<Customer>>().Setup(m => m.ElementType).Returns(data.ElementType);
            _customer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _entities = new Moq.Mock<HWMartinEntities>();
            _entities.Setup(s => s.Customers).Returns(_customer.Object);

            _service = new CustomerService(c.Object);
            _service.SetContext(_entities.Object, false);
        }

        [Test]
        public void Creating_new_customer()
        {
            var ob = new CustomerModel();

            _service.Create(ob);

            Assert.IsNotNull(ob.CustomerID);
            Assert.AreEqual(true, ob.IsActive);
            Assert.AreEqual(userID, ob.CreatedByID);
            Assert.AreEqual(userID, ob.UpdatedByID);

            _customer.Verify(v => v.Add(It.IsAny<Customer>()), Times.Once());
            _entities.Verify(v => v.SaveChanges());
        }

        [Test]
        public void Updating_customer()
        {
            var ob = new CustomerModel() { CustomerID = Guid.Empty };

            _service.Update(ob);

            var customer = _service.Single(Guid.Empty);

            Assert.AreEqual(Guid.Empty, customer.CreatedByID);
            Assert.AreEqual(userID, customer.UpdatedByID);

            _entities.Verify(v => v.SaveChanges());
        }

    }
}
