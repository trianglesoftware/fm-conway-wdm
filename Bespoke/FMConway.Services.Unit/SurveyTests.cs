﻿using ClancyPlant.Services.Surveys.Models;
using ClancyPlant.Services.Surveys.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Users;


namespace ClancyPlant.Services.Unit
{
    [TestFixture]
    public class SurveyTests
    {
        SurveyService _service;
        Mock<HWMartinEntities> _entities;
        Mock<DbSet<Survey>> _survey;

        [SetUp]
        public void Setup()
        {
            var c = new Mock<AspUserContext>(null);
            c.Setup(s => s.UserDetails.ID).Returns(Guid.Empty);

            _survey = new Mock<DbSet<Survey>>();

            _entities = new Moq.Mock<HWMartinEntities>();
            _entities.Setup(s => s.Surveys).Returns(_survey.Object);

            _service = new SurveyService(c.Object);
            _service.SetContext(_entities.Object, false);
        }

        [Test]
        public void Creating_new_survey()
        {
            var ob = new SurveyModel();

            _service.Create(ob);

            Assert.IsNotNull(ob.ID);
            Assert.AreEqual(true, ob.IsActive);
            Assert.AreEqual(Guid.Empty, ob.CreatedByID);
            Assert.AreEqual(Guid.Empty, ob.UpdatedByID);

            _survey.Verify(v => v.Add(It.IsAny<Survey>()), Times.Once());
            _entities.Verify(v => v.SaveChanges());
        }

    }
}
