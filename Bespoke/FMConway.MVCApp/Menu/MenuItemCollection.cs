﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.MVCApp.Extensions;

namespace FMConway.MVCApp.Menu
{
    public class MenuItemCollection
    {
        public List<MenuItem> Collection { get; set; }

        public MenuItemCollection()
        {
            Collection = new List<MenuItem>();
        }

        public MenuItemCollection(IEnumerable<MenuItem> collection)
        {
            this.Collection = collection.ToList();
        }
    }

    public class MenuItem
    {
        public string ID { get { return GetID(); } }
        public string DisplayName { get; set; }
        public string Url { get { return UrlDisabled ? "" : HasArea ? string.Format("/{0}/{1}/{2}", Area, Controller, Action) : string.Format("/{0}/{1}", Controller, Action); } }
        public string Icon { get; set; }

        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

        public int Order { get; set; }

        public bool HasArea { get { return !string.IsNullOrEmpty(Area); } }
        public bool HasController { get { return !string.IsNullOrEmpty(Controller); } }
        public bool HasAction { get { return !string.IsNullOrEmpty(Action); } }
        public bool HasIcon { get { return !string.IsNullOrEmpty(Icon); } }
        public bool UrlDisabled { get; set; }
        public bool HasChildren { get { return Children.Any(); } }
        public bool HasParent { get { return Parent != null; } }

        public List<MenuItem> Children { get; set; }
        public MenuItem Parent { get; private set; }

        public MenuItem(MenuItem parent)
            : this()
        {
            parent.Children.Add(this);
            this.Parent = parent;
        }

        public MenuItem()
        {
            Children = new List<MenuItem>();
        }

        private string GetID()
        {
            string id = "";

            if (HasArea)
                id += Area;

            if (HasController)
                id += string.IsNullOrEmpty(id) ? Controller : "-" + Controller;

            if (HasAction)
                id += string.IsNullOrEmpty(id) ? Action.PascalCaseToFriendly() : "-" + Action.PascalCaseToFriendly();

            return id;
        }
    }
}