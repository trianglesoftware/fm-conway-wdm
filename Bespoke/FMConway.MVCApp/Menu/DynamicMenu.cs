﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using FMConway.MVCApp.Extensions;

namespace FMConway.MVCApp.Menu
{
    public class DynamicMenu
    {

        public static MenuItemCollection Generate(HttpContextBase httpContext, string[] roles)
        {

            IEnumerable<Type> controllers = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.GetCustomAttribute<NavigationMenuAttribute>() != null);


            string menuCache = "menu_cache_";

            if (roles != null && roles.Any())
            {
                menuCache = menuCache + roles.OrderBy(o => o).Aggregate((a, b) => $"{a}_{b}");
            }

            var menuCollection = InfoCache.CacheWrapper.Get<MenuItemCollection>(menuCache);

            if (menuCollection != null)
            {
                return menuCollection;
            }

            menuCollection = new MenuItemCollection();

            foreach (var controller in controllers)
            {
                if (!Authorize(httpContext, controller))
                    continue;

                string area = controller.Namespace.Contains("Areas") ? System.Text.RegularExpressions.Regex.Match(controller.Namespace, @"(?<=\.Areas\.).*(?=\.)").Value : "";
                string controllerName = controller.Name.Replace("Controller", "");

                NavigationMenuAttribute rootActionDetails = controller.GetCustomAttribute<NavigationMenuAttribute>();

                bool hasRole = false;
                if (rootActionDetails.Roles != null && rootActionDetails.Roles.Any())
                {
                    foreach (var role in rootActionDetails.Roles)
                    {
                        if (httpContext.User.IsInRole(role))
                        {
                            hasRole = true;
                            break;
                        }

                    }

                    if (!hasRole)
                        continue;
                }


                var rootMenuItem = new MenuItem()
                {
                    DisplayName = string.IsNullOrEmpty(rootActionDetails.DisplayName) ? controllerName : rootActionDetails.DisplayName,
                    Area = area,
                    Order = rootActionDetails.Order,
                    Icon = rootActionDetails.Icon,
                    Controller = controllerName,
                    UrlDisabled = true
                };

                menuCollection.Collection.Add(rootMenuItem);

                IEnumerable<MethodInfo> actions = controller.GetMethods().Where(m => m.GetCustomAttribute<NavigationMenuItemAttribute>() != null);

                MethodInfo defaultAction = actions.Where(a => a.GetCustomAttribute<NavigationMenuItemAttribute>().IsDefault).SingleOrDefault();
                if (defaultAction != null)
                {
                    actions = actions.Where(action => action != defaultAction);
                    rootMenuItem.Controller = controllerName;
                    rootMenuItem.Action = defaultAction.Name;
                    rootMenuItem.UrlDisabled = false;
                }

                foreach (var action in actions)
                {
                    if (!Authorize(httpContext, action))
                        continue;

                    NavigationMenuItemAttribute actionDetails = action.GetCustomAttribute<NavigationMenuItemAttribute>();

                    MenuItem subMenuItem = new MenuItem(rootMenuItem)
                    {
                        DisplayName = string.IsNullOrEmpty(actionDetails.DisplayName) ? action.Name.PascalCaseToFriendly() : actionDetails.DisplayName,
                        Area = area,
                        Controller = controllerName,
                        Action = action.Name,
                        Order = actionDetails.Order,
                        Icon = actionDetails.Icon
                    };

                }
            }


            InfoCache.CacheWrapper.Add(menuCache, menuCollection);

            return menuCollection;
        }

        private static bool Authorize(HttpContextBase httpContext, MemberInfo obj)
        {
            AllowAnonymousAttribute aaa = obj.GetCustomAttribute<AllowAnonymousAttribute>();
            if (aaa != null)
                return true;

            IEnumerable<AuthorizeAttribute> auths = obj.GetCustomAttributes<AuthorizeAttribute>();
            if (auths.Count() > 0)
            {
                AuthorizeAttribute auth = auths.Where(a => !a.GetType().IsSubclassOf(typeof(DMAuthorizeAttribute))).SingleOrDefault();
                if (auth != null)
                {

                    if (!HttpContext.Current.User.Identity.IsAuthenticated)
                        return false;

                    bool inRole = false;
                    foreach (string role in auth.Roles.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (HttpContext.Current.User.IsInRole(role))
                            inRole = true;
                    }

                    if (!inRole)
                        return false;

                }

                IEnumerable<DMAuthorizeAttribute> customAuths = auths.Where(a => a.GetType().IsSubclassOf(typeof(DMAuthorizeAttribute))).Cast<DMAuthorizeAttribute>();

                if (customAuths != null)
                {
                    foreach (DMAuthorizeAttribute customAuth in customAuths)
                    {
                        if (!customAuth.IsAuthorised(httpContext))
                            return false;
                    }
                }
            }
            return true;
        }

    }
}