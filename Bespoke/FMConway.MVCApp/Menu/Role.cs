﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Menu
{
    public static class Role
    {
        public const string Administrator = "Administrator";
        public const string Supervisor = "Supervisor";
        public const string Manager = "Manager";
        public const string Operation = "Operation";
    }
}