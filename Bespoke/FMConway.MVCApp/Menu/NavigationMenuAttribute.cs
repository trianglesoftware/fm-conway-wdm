﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Menu
{
    public class NavigationMenuAttribute : Attribute
    {
        public string Icon { get; set; }
        public string DisplayName { get; private set; }
        public string[] Roles { get; set; }
        public int Order { get; private set; }

        #region constructors

        public NavigationMenuAttribute(int order)
        {
            this.Order = order;
        }

        public NavigationMenuAttribute(int order, string icon)
            : this(order)
        {
            this.Icon = icon;
        }

        public NavigationMenuAttribute(string displayName, int order)
            : this(order)
        {
            this.DisplayName = displayName;
        }

        public NavigationMenuAttribute(string displayName, int order, string icon, string roles = "")
            : this(displayName, order)
        {
            this.Icon = icon;
            this.Roles = roles.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
        }

        #endregion

    }
}