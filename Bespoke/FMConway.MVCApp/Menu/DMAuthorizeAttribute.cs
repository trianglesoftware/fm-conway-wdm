﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Menu
{
    public abstract class DMAuthorizeAttribute : AuthorizeAttribute
    {

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return IsAuthorised(httpContext);
        }

        public abstract bool IsAuthorised(HttpContextBase httpContext);

    }
}