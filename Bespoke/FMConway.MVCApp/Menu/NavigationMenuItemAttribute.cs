﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Menu
{
    public class NavigationMenuItemAttribute : Attribute
    {
        public string DisplayName { get; private set; }
        public string Icon { get; set; }

        public int Order { get; private set; }
        public bool IsDefault { get; private set; }

        #region constructors

        public NavigationMenuItemAttribute(int order)
        {
            this.Order = order;
        }

        public NavigationMenuItemAttribute(int order, string icon)
            : this(order)
        {
            this.Icon = icon;
        }

        public NavigationMenuItemAttribute(int order, bool isDefault)
            : this(order)
        {
            this.IsDefault = isDefault;
        }

        public NavigationMenuItemAttribute(int order, bool isDefault, string icon)
            : this(order, icon)
        {
            this.IsDefault = isDefault;
        }

        public NavigationMenuItemAttribute(string displayName, int order)
            : this(order)
        {
            this.DisplayName = displayName;
        }

        public NavigationMenuItemAttribute(string displayName, int order, string icon)
            : this(order, icon)
        {
            this.DisplayName = displayName;
        }

        public NavigationMenuItemAttribute(string displayName, int order, bool isDefault)
            : this(displayName, order)
        {
            this.IsDefault = isDefault;
        }

        public NavigationMenuItemAttribute(string displayName, int order, bool isDefault, string icon)
            : this(displayName, order, icon)
        {
            this.IsDefault = isDefault;
        }

        #endregion
    }
}