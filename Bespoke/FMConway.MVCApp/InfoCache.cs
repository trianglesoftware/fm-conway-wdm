﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp
{
    public static class InfoCache
    {

        public static class CacheWrapper
        {
            public static void Add<T>(string key, T item)
            {
                HttpContext.Current.Cache[key] = item;
            }

            public static void Add<T>(string key, T item, DateTime expires)
            {
                HttpContext.Current.Cache.Add(key, item, null, expires, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
            }

            public static T Get<T>(string key)
            {
                T item = (T)HttpContext.Current.Cache[key];

                return item;
            }
            public static T GetOrCallAndStore<T>(string key, Func<T> sourceFunc)
            {
                var cachedValue = HttpContext.Current.Cache[key];
                if (cachedValue != null)
                {
                    return (T)cachedValue;
                }
                else
                {
                    T res = sourceFunc();
                    Add(key, res);
                    return res;
                }
            }

            public static void Remove(string key)
            {
                if (HttpContext.Current.Cache[key] != null)
                {
                    HttpContext.Current.Cache.Remove(key);
                }
            }
        }

        public static class SessionWrapper
        {
            public class SessionCache<T>
            {
                public TimeSpan Expires { get; set; }
                public DateTime Created { get; set; }
                public T Item { get; set; }
            }

            public static void Add<T>(string key, T item, TimeSpan expires)
            {
                HttpContext.Current.Session[key] = new SessionCache<T>
                {
                    Expires = expires,
                    Created = DateTime.Now,
                    Item = item
                };
            }

            public static void Add<T>(string key, T item)
            {
                HttpContext.Current.Session[key] = item;
            }

            public static T Get<T>(string key)
            {
                object co = HttpContext.Current.Session[key];

                if (co is SessionCache<T> sc)
                {
                    if (sc.Created + sc.Expires <= DateTime.Now)
                    {
                        HttpContext.Current.Session.Remove(key);
                        return default(T);
                    }

                    return sc.Item;
                }

                return (T)co;
            }
        }

        public static class ItemWrapper
        {

            public static void Add<T>(string key, T item)
            {
                HttpContext.Current.Items[key] = item;
            }

            public static T Get<T>(string key)
            {
                T item = (T)HttpContext.Current.Items[key];

                return item;
            }
        }
    }
}