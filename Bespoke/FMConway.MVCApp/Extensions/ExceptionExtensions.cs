﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace FMConway.MVCApp.Extensions
{
    public static class ExceptionExtensions
    {
        public static string GetVeboseException(this Exception ex, bool includeStackTrace = false)
        {
            StringBuilder exception = new StringBuilder();

            exception.AppendLine(ex.Message);
            exception.AppendLine();

            if (includeStackTrace)
            {
                exception.AppendLine(ex.StackTrace);
                exception.AppendLine();
            }

            Exception inner = ex.InnerException;

            while (inner != null)
            {

                exception.AppendLine(inner.Message);
                exception.AppendLine();

                if (includeStackTrace)
                {
                    exception.AppendLine(inner.StackTrace);
                    exception.AppendLine();
                }

                inner = inner.InnerException;
            }

            return exception.ToString();
        }

    }
}