﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FMConway.MVCApp.Extensions
{
    public static class HtmlControlExtensions
    {
        public static HtmlString RenderControl(this HtmlControl contr)
        {            
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    contr.RenderControl(htw);
                    return new HtmlString(sw.ToString());
                }
            }
        }

        public static void AddAttributes(this HtmlControl control, RouteValueDictionary htmlAttributes)
        {
            if (htmlAttributes != null)
            {
                foreach (var attribute in htmlAttributes)
                {
                    control.Attributes.Add(attribute.Key, attribute.Value.ToString());
                }
            }
        }
    }
}