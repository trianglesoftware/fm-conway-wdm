﻿using FMConway.MVCApp.Extensions;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using WebGrease.Css.Extensions;

namespace System.Web.Mvc.Html
{
    public static class HtmlHelperExtensions
    {
        public static string GeneralValidationMessage = "Your form has failed validation, please correct any issues to save";

        public static string ToString(this decimal? number, string format, decimal? defaultValue = null)
        {
            if (number != null)
            {
                if (format != null)
                {
                    return number.Value.ToString(format);
                }
                return number.ToString();
            }
            else
            {
                if (defaultValue != null)
                {
                    return defaultValue.ToString(format);
                }
                else
                {
                    return null;
                }
            }
        }

        // requires .input-resize class to be placed in parent container
        public static HtmlString SetLabelSize(this HtmlString formGroup, int labelSizePixels = 170)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(WebUtility.HtmlDecode(formGroup.ToString().Trim()));
            doc.FindControlLabel().SetAttributeValue("style", "width: " + labelSizePixels + "px");
            doc.AllNodes().Where(a => a.HasClass("controls")).ForEach(a => a.SetAttributeValue("style", "margin-left: " + (labelSizePixels + 10) + "px"));

            return new HtmlString(doc.DocumentNode.OuterHtml);
        }

        public static HtmlString HiddenIf(this HtmlString html, bool hidden, bool lookupRoot = false)
        {
            if (hidden)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(WebUtility.HtmlDecode(html.ToString().Trim()));

                var firstChild = doc.DocumentNode.FirstChild;
                if (firstChild.HasClass("lookup") && !lookupRoot)
                {
                    // can't hide parent as contains modal so just hide the control-group. In the theming update modals are now created at end of the body,
                    // but there maybe existing code which still relys on the control-group been hidden, so keep as is for now.
                    // option to force lookupRoot to be hidden instead of control-group child
                    foreach (var node in firstChild.ChildNodes.Where(a => a.HasClass("control-group")))
                    {
                        node.SetAttributeValue("style", "display: none");
                    }
                }
                else
                {
                    firstChild.SetAttributeValue("style", "display: none");
                }

                html = new HtmlString(doc.DocumentNode.OuterHtml);
            }

            return html;
        }

        public static HtmlString DisabledIf(this HtmlString html, bool disabled, bool dataDisabled = false)
        {
            if (disabled)
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(WebUtility.HtmlDecode(html.ToString().Trim()));
                var nodes = doc.FindFormControls();

                foreach (var node in nodes)
                {
                    if (dataDisabled)
                    {
                        node.SetAttributeValue("data-disabled", "true");
                    }
                    else
                    {
                        node.SetAttributeValue("disabled", "");
                    }
                }

                html = new HtmlString(doc.DocumentNode.OuterHtml);
            }

            return html;
        }

        public static HtmlString ReadonlyIf(this HtmlString html, bool readOnly)
        {
            if (readOnly)
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(WebUtility.HtmlDecode(html.ToString().Trim()));
                var nodes = doc.FindFormControls();

                foreach (var node in nodes)
                {
                    var type = node.GetAttributeValue("type", null);
                    if (type == "checkbox")
                    {
                        doc.AllNodes("label").Where(a => a.GetAttributeValue("for", null) == node.Id).ForEach(a => a.SetAttributeValue("data-disabled", "true"));
                        node.SetAttributeValue("tabindex", "-1");
                    }

                    node.SetAttributeValue("readonly", "");
                }

                html = new HtmlString(doc.DocumentNode.OuterHtml);
            }

            return html;
        }

        public static HtmlString RequiredIf(this HtmlString html, bool required)
        {
            if (required)
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(WebUtility.HtmlDecode(html.ToString().Trim()));
                var label = doc.FindControlLabel();
                label.AppendChild(HtmlNode.CreateNode("<span class='required'>*</span>"));

                html = new HtmlString(doc.DocumentNode.OuterHtml);
            }

            return html;
        }

    }
}