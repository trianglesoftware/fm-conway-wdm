﻿using FMConway.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Extensions
{
    public static class ModelStateExtensions
    {
        public static void AddModelErrors(this ModelStateDictionary modelState, List<ServiceError> serviceErrors)
        {
            serviceErrors.ForEach(a => modelState.AddModelError(a.Key, a.Error));
        }
    }
}