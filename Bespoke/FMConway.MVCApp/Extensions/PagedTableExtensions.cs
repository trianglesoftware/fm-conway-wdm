﻿using FMConway.MVCApp.Bootstrap.Collections;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Extensions
{
    public static class PagedTableExtensions
    {
        public static DataTable ToDataTable(this PagedTable pagedTable)
        {
            var dataTable = new DataTable();

            foreach (var header in pagedTable.ColumnHeaders)
            {
                dataTable.Columns.Add(header, typeof(string));
            }

            foreach (var row in pagedTable.Rows)
            {
                dataTable.Rows.Add(row.TableData);
            }

            return dataTable;
        }

        public static void RemoveColumn(this PagedTable pagedTable, string columnName)
        {
            var columnHeaders = pagedTable.ColumnHeaders.ToList();
            var columnIndex = columnHeaders.IndexOf(columnName);
            columnHeaders.RemoveAt(columnIndex);
            pagedTable.ColumnHeaders = columnHeaders.ToArray();

            foreach (var row in pagedTable.Rows)
            {
                var tableData = row.TableData.ToList();
                tableData.RemoveAt(columnIndex);
                row.TableData = tableData.ToArray();
            }
        }
    }
}