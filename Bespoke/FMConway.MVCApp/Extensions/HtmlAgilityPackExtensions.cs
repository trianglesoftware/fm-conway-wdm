﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Extensions
{
    public static class HtmlAgilityPackExtensions
    {
        public static IEnumerable<HtmlNode> AllNodes(this HtmlDocument doc, string name = null)
        {
            return name == null ? doc.DocumentNode.DescendantsAndSelf() : doc.DocumentNode.DescendantsAndSelf(name);
        }

        // returns form-control nodes of form-group or first child
        public static List<HtmlNode> FindFormControls(this HtmlDocument doc)
        {
            var nodes = new List<HtmlNode>();
            if (doc.DocumentNode.FirstChild.HasClass("control-group"))
            {
                var validInputTypes = new List<string> { "text", "checkbox" };

                nodes.AddRange(doc.DocumentNode.Descendants().Where(a =>
                    (a.Name == "input" && validInputTypes.Contains(a.GetAttributeValue("type", null))) ||
                    a.Name == "textarea" ||
                    a.Name == "select"));
            }
            else
            {
                nodes.Add(doc.DocumentNode.FirstChild);
            }

            return nodes;
        }

        public static HtmlNode FindControlLabel(this HtmlDocument doc)
        {
            return doc.DocumentNode.DescendantsAndSelf().Where(a => a.HasClass("control-label")).First();
        }

        public static void ForEach(this HtmlNodeCollection htmlNodeCollection, Action<HtmlNode> action)
        {
            if (htmlNodeCollection == null)
            {
                return;
            }

            foreach (var htmlNode in htmlNodeCollection)
            {
                action(htmlNode);
            }
        }
    }
}