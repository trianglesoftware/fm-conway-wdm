﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Depots.Models;
using FMConway.Services.EquipmentTypes.Models;
using FMConway.Services.Jobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Extensions
{
    public static class SelectListExtensions
    {
        private static List<SelectListItem> SelectList<T>(IEnumerable<T> data, Func<T, object> value, Func<T, object> text, object selectedValue, string optionLabel)
        {
            var selectListItems = new List<SelectListItem>();

            if (optionLabel != null)
            {
                var selectListItem = new SelectListItem();
                selectListItem.Value = null;
                selectListItem.Text = optionLabel;

                selectListItems.Add(selectListItem);
            }

            if (data == null)
            {
                return selectListItems;
            }

            foreach (var item in data)
            {
                var selectListItem = new SelectListItem();
                selectListItem.Value = value(item).ToString();
                selectListItem.Text = text(item).ToString();
                selectListItem.Selected = selectedValue != null && value(item).Equals(selectedValue) ? true : false;

                selectListItems.Add(selectListItem);
            }

            return selectListItems;
        }

        #region checkboxes

        public static List<SelectListItem> NullableYesNoCheckBox()
        {
            var selectListItems = YesNoCheckBox();
                
            selectListItems.Insert(0, new SelectListItem
            {
                Text = "",
                Value = ""
            });

            return selectListItems;
        }

        public static List<SelectListItem> YesNoCheckBox()
        {
            return new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Yes",
                    Value = "True"
                },
                new SelectListItem
                {
                    Text = "No",
                    Value = "False"
                }
            };
        }

        #endregion

        #region extensions

        public static List<SelectListItem> ToSelectList<T>(this IEnumerable<T> data, Func<T, object> value, Func<T, object> text, object selectedValue = null, string optionLabel = null)
        {
            return SelectList(data, value, text, selectedValue, optionLabel);
        }

        public static List<SelectListItem> ToSelectList<T, Y>(this Dictionary<T, Y> data)
        {
            return new SelectList(data, "Key", "Value").ToList();
        }

        public static List<SelectListItem> ToSelectList(this IEnumerable<JobSpeedModel> data, object selectedValue = null, string optionLabel = null)
        {
            return SelectList(data, a => a.JobSpeedPK, a => a.Name, selectedValue, optionLabel);
        }

        public static List<SelectListItem> ToSelectList(this IEnumerable<VmsOrAssetModel> data, object selectedValue = null, string optionLabel = null)
        {
            return SelectList(data, a => a.VmsOrAssetID, a => a.Name, selectedValue, optionLabel);
        }

        public static List<SelectListItem> ToSelectList(this IEnumerable<ContractModel> data, object selectedValue = null, string optionLabel = null)
        {
            return SelectList(data, a => a.ID, a => a.ContractTitle, selectedValue, optionLabel);
        }

        public static List<SelectListItem> ToSelectList(this IEnumerable<DepotModel> data, object selectedValue = null, string optionLabel = null)
        {
            return SelectList(data, a => a.DepotID, a => a.DepotName, selectedValue, optionLabel);
        }

        public static List<SelectListItem> ToSelectList(this IEnumerable<JobStatusModel> data, object selectedValue = null, string optionLabel = null)
        {
            return SelectList(data, a => a.StatusID, a => a.Status, selectedValue, optionLabel);
        }

        #endregion
    }
}