﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Extensions.TableGenerator.Attributes
{
    public class ColumnIgnoreAttribute : Attribute
    {
        public List<string> Columns { get; private set; }

        public ColumnIgnoreAttribute(params string[] columns)
        {
            this.Columns = columns.ToList();
        }
    }

}