﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using FMConway.MVCApp.Extensions;
using FMConway.MVCApp.Extensions.TableGenerator.Attributes;

namespace FMConway.MVCApp.Extensions.TableGenerator
{
    public static class HtmlExtensions
    {

        public static HtmlString GenerateTable(this HtmlHelper helper, object model, string name)
        {
            return GenerateTable(helper, model, name, null, null);
        }

        public static HtmlString GenerateTable(this HtmlHelper helper, object model, string name, object htmlAttributes)
        {
            return GenerateTable(helper, model, name, htmlAttributes, null);
        }

        public static HtmlString GenerateTable(this HtmlHelper helper, object model, string name, object htmlAttributes, params RowAction[] rowActions)
        {
            List<PropertyInfo> validProperties = new List<PropertyInfo>();
            HashSet<string> ignore = new HashSet<string>();

            PropertyInfo tableData = model.GetType().GetProperties().Where(p => p.GetCustomAttribute<TableAttribute>() != null && p.GetCustomAttribute<TableAttribute>().Name.Equals(name)).SingleOrDefault();

            Type dataType = tableData.PropertyType.GetGenericArguments()[0];

            var columnIgnore = tableData.GetCustomAttributes(typeof(FMConway.MVCApp.Extensions.TableGenerator.Attributes.ColumnIgnoreAttribute))
                                            .Cast<FMConway.MVCApp.Extensions.TableGenerator.Attributes.ColumnIgnoreAttribute>().SingleOrDefault();
            if (columnIgnore != null)
            {
                ignore.UnionWith(columnIgnore.Columns);
            }

            var table = new HtmlGenericControl("table");

            var rvd = new System.Web.Routing.RouteValueDictionary(htmlAttributes);
            foreach (var keyVal in rvd)
            {
                table.Attributes.Add(keyVal.Key, keyVal.Value.ToString());
            }

            var tableHeader = new HtmlGenericControl("thead");
            table.Controls.Add(tableHeader);

            validProperties = GetHeaders(dataType, ignore, tableHeader, hasRowActions: rowActions != null && rowActions.Count() > 1);

            var tableBody = new HtmlGenericControl("tbody");
            table.Controls.Add(tableBody);

            if (validProperties.Any())
            {
                GetData(tableData, model, tableBody, validProperties, rowActions);
            }

            return table.RenderControl();
        }

        /// <summary>
        /// Adds each dataType property as a table header 
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="ignoredProperties"></param>
        /// <param name="tableHeader"></param>
        /// <param name="hasRowActions"></param>
        /// <returns></returns>
        private static List<PropertyInfo> GetHeaders(Type dataType, IEnumerable<string> ignoredProperties, HtmlControl tableHeader, bool hasRowActions)
        {
            var validProperties = new List<PropertyInfo>();

            var tableHeaderRow = new HtmlGenericControl("tr");
            tableHeader.Controls.Add(tableHeaderRow);

            foreach (PropertyInfo property in dataType.GetProperties())
            {
                if (ignoredProperties.Any(p => p.Equals(property.Name, StringComparison.CurrentCultureIgnoreCase)))
                    continue;

                var column = new HtmlGenericControl("th");
                column.InnerText = property.Name.PascalCaseToFriendly();
                tableHeaderRow.Controls.Add(column);

                validProperties.Add(property);
            }

            if (hasRowActions)
            {
                var column = new HtmlGenericControl("th");
                tableHeaderRow.Controls.Add(column);
            }

            return validProperties;
        }

        /// <summary>
        /// Retrieves the table data from the given model and adds each column of data to the table body where the data properties match the valid properties.
        /// </summary>
        /// <param name="tableData"></param>
        /// <param name="model"></param>
        /// <param name="tableBody"></param>
        /// <param name="validProperties"></param>
        /// <param name="rowActions"></param>
        private static void GetData(PropertyInfo tableData, object model, HtmlControl tableBody, IEnumerable<PropertyInfo> validProperties, IEnumerable<RowAction> rowActions)
        {
            var collection = tableData.GetValue(model) as System.Collections.IEnumerable;

            if (collection == null)
                return;

            foreach (var item in collection)
            {
                var dataRow = new HtmlGenericControl("tr");

                tableBody.Controls.Add(dataRow);

                foreach (PropertyInfo vProp in validProperties)
                {
                    var data = new HtmlGenericControl("td");

                    object val = vProp.GetValue(item);
                    var type = vProp.PropertyType;

                    if (val != null)
                    {
                        data.InnerText = val.ToString();

                        // note that data type attributes will also validate properties when posting
                        var dataTypeAttribute = vProp.GetCustomAttribute<DataTypeAttribute>();
                        if (dataTypeAttribute != null)
                        {
                            data.InnerText = string.Format(dataTypeAttribute.DisplayFormat.DataFormatString, val);
                        }

                        // if you want to set the display format but not apply validation when posting use this. extend if required.
                        var displayFormatAttribute = vProp.GetCustomAttribute<DisplayFormatAttribute>();
                        if (displayFormatAttribute != null)
                        {
                            if (type == typeof(decimal))
                            {
                                var value = (decimal)val;
                                data.InnerText = value.ToString(displayFormatAttribute.DataFormatString);
                            }
                            else if (type == typeof(decimal?))
                            {
                                if (val != null)
                                {
                                    var value = (decimal)val;
                                    data.InnerText = value.ToString(displayFormatAttribute.DataFormatString);
                                }
                            }
                        }
                    }

                    dataRow.Controls.Add(data);

                }

                if (rowActions != null && rowActions.Any())
                {
                    dataRow.Attributes["class"] = "dAct";
                    dataRow.Attributes["data-da"] = MapUrlParameter(rowActions.First().Link, item);

                    RenderRowActions(rowActions.Skip(1), dataRow, item);
                }

            }
        }

        private static string MapUrlParameter(string url, object data)
        {
            string actionLink = url;
            MatchCollection parameters = Regex.Matches(actionLink, "(?<={).*?(?=})");

            foreach (Match parameter in parameters.Cast<Match>().Distinct())
            {
                PropertyInfo prop = data.GetType().GetProperties().Where(p => p.Name.Equals(parameter.Value, StringComparison.CurrentCultureIgnoreCase)).SingleOrDefault();

                if (prop == null)
                    throw new ArgumentException("Missing parameter " + parameter.Value);

                actionLink = actionLink.Replace("{" + parameter.Value + "}", prop.GetValue(data).ToString());

            }

            return actionLink;
        }

        private static void RenderRowActions(IEnumerable<RowAction> rowActions, HtmlControl tableRow, object data)
        {
            if (!rowActions.Any())
                return;

            var tableData = new HtmlGenericControl("td");

            foreach (var rowAction in rowActions)
            {
                string actionLink = MapUrlParameter(rowAction.Link, data);
                MatchCollection parameters = Regex.Matches(actionLink, "(?<={).*?(?=})");

                var link = new HtmlAnchor()
                {
                    HRef = actionLink,
                    Title = rowAction.Title
                };

                if (!string.IsNullOrEmpty(rowAction.Icon))
                {
                    var icon = new HtmlGenericControl("i");
                    icon.Attributes["class"] = rowAction.Icon;

                    link.Controls.Add(icon);
                }
                else
                {
                    link.InnerText = rowAction.Title;
                    link.Attributes["class"] = "btn";
                }

                tableData.Controls.Add(link);

            }

            tableRow.Controls.Add(tableData);
        }
    }

    public class RowAction
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Icon { get; set; }

    }
}