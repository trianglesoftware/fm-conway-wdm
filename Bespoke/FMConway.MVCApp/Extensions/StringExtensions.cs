﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Extensions
{
    public static class StringExtensions
    {
        public static string PascalCaseToFriendly(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";

            return System.Text.RegularExpressions.Regex.Replace(str, "[a-z][A-Z]", m =>
            {
                return m.Value[0] + " " + m.Value[1];
            });
        }

        public static string PascalCaseToCss(this string str)
        {

            if (string.IsNullOrEmpty(str))
                return "";

            return System.Text.RegularExpressions.Regex.Replace(str, "[a-z][A-Z]", m =>
            {
                return m.Value[0] + "-" + m.Value[1];
            }).ToLower();
        }
    }
}