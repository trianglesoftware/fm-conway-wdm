﻿
function Table(selector, options) {

    var me = this;

    this.container = selector instanceof jQuery ? selector : $(selector);
    this.rows = [];
    this.headerIndex = {};
    this.originalTableWidth = 0;
    this.cookieName = "table_con";

    this.options = {
        debug: false,
        isMultiSelect: true,
        headers: [],
        headerFormatter: null,
        singleClick: false,
        columnWidth: 200,
        columnDrag: true,
        allowSort: false,
        displayOverview: true,
        rowHeight: 20,
        saveSettings: true,
        noRecordsText: "No records available.",
        sort: {
            string: function (a, b, descend) {

                me.log("sort vals", arguments);

                var com = a.localeCompare(b);

                return descend ? com * -1 : com;
            },
            num: function (a, b, descend) {
                if (a < b)
                    return descend ? 1 : -1;
                if (a > b)
                    return descend ? -1 : 1;

                return 0;
            },
            date: function (a, b, descend) {


                if ((a === undefined || a === null || a.length === 0) && (b === undefined || b === null || b.length === 0)) {
                    return 0;
                }

                if ((a === undefined || a === null || a.length === 0) && b !== undefined && b !== null && b.length > 0) {
                    return descend ? 1 : -1;
                }

                if ((b === undefined || b === null || b.length === 0) && a !== undefined && a !== null && a.length > 0) {
                    return descend ? -1 : 1;
                }

                var aSplit = a.split("/");
                var aDate = new Date(aSplit[2] + "-" + aSplit[1] + "-" + aSplit[0]);

                var bSplit = b.split("/");
                var bDate = new Date(bSplit[2] + "-" + bSplit[1] + "-" + bSplit[0]);

                if (aDate < bDate)
                    return descend ? 1 : -1;
                if (aDate > bDate)
                    return descend ? -1 : 1;

                return 0;
            }
        }
    }

    this.options = $.extend(true, this.options, options);

    this.container.append($('<div class="it-table"><div class="it-ov"><div class="ov-col"></div></div><div class="it-cont"><span class="db" style="display:none;"></span><div class="bg"></div><div class="it-load"><div class="it-load-bg"></div><div class="it-load-con"><div><img src="/content/images/loader.svg"/></div></div></div><div class="it-head clearfix"></div><div class="it-body"><div class="it-table-con"><table rules="none"><tbody></tbody></table></div></div></div></div>'));
    this.header = this.container.find(".it-head");

    this.tableContainer = this.container.find(".it-body");
    this.overview = this.container.find(".it-ov .ov-col");
    this.table = this.container.find("table");
    this.body = this.container.find("tbody");
    this.loader = this.container.find(".it-load");

    this.setHeaders(this.options.headers);

    this.events();

    if (!this.options.saveSettings) {

        Cookies.remove(this.cookieName);

    }

    this.showNoRows();
    return this;

}

Table.prototype.log = function (m, o) {

    if (this.options.debug) {
        console.log(m + "[" + this.uniqueID() + "]", o);
    }
}

Table.prototype.showLoader = function (show) {

    if (show) {
        this.loader.show();
    } else {
        this.loader.hide();
    }

}

Table.prototype.sortColumn = function (i, sortType, descend) {


    this.log("sorting", arguments);

    var me = this;

    var rows = this.body.children(":not(.cs)");

    rows.sort(function (a, b) {

        me.log("sort func", me.options.sort[sortType]);

        var aVal = $($(a).children()[i]).find(".val").text();
        var bVal = $($(b).children()[i]).find(".val").text();

        return me.options.sort[sortType](aVal, bVal, descend);

    });
    for (var r = 0; r < rows.length; r++) {
        var removed = this.rows.splice($(rows[r]).index() - 1, 1)[0];
        this.rows.splice(r, 0, removed);
        this.body.children().eq(r + 1).before(rows[r]);
    }
}

Table.prototype.setHeaders = function (headers) {

    var me = this;
    var options = this.options;
    options.headers = headers;

    var hCon = this.header;
    var totalWidth = 0;

    hCon.empty();
    this.body.find("> .cs").remove();

    var settings = [];

    var c = Cookies.get(this.cookieName);

    if (c !== undefined && c !== null) {

        c = JSON.parse(c);

        var uid = this.uniqueID();

        if (c.hasOwnProperty(uid)) {

            settings = c[uid];

            this.log("settings", settings);
        }
    }

    headers.forEach(function (h, i) {

        if (h.hidden)
            return true;

        var width = settings.length > i ? settings[i].width : (h.width === undefined) ? options.columnWidth : h.width != 'auto' ? h.width : me.calculateAutoWidth();
        totalWidth += width;
        options.headers[i].width = width;

        var html = null;

        if (me.options.headerFormatter != null) {
            html = me.options.headerFormatter(i, h);
        } else {
            html = $(me).triggerHandler("headerFormatter", [i, h]);
        }

        var col = $("<div>").width(width).addClass("it-head-" + h.name.toLowerCase().replace(/[ ]*/g, ''));
        col.data("col", i);

        if (h.class !== undefined) {
            for (var i = 0; i < h.class.length; i++) {
                col.addClass(h.class[i]);
            }
        }

        if (h.attr !== undefined) {
            for (var i in h.attr) {
                col.attr("data-" + i, h.attr[i]);
            }
        }

        if (html === undefined || html === null) {
            col.append($("<label class='it-header'>").text(h.display !== undefined ? h.display : h.name));

        } else {
            col.append(html);
        }

        if (me.options.allowSort && (!h.hasOwnProperty("sort") || h.sort)) {

            if (h.hasOwnProperty("type")) {
                col.attr("data-type", h.type);
            } else {
                col.attr("data-type", "string");
            }

            col.css("cursor", "pointer");

            col.data("descend", true);

            col.on("click", function () {

                me.sortColumn($(this).data("col"), $(this).attr("data-type"), $(this).data("descend"));

                $(this).data("descend", !$(this).data("descend"));

            })
        }


        if (me.options.columnDrag && (h.drag === undefined || h.drag)) {
            col.append($("<span class='it-drag'>"));
        }

        hCon.append(col);

        me.headerIndex[h.hasOwnProperty("alias") ? h.alias : h.name] = i;

    });

    this.originalTableWidth = totalWidth;
    this.setTableWidth(totalWidth);
    this.clearRows();
}

Table.prototype.getHeaderIndex = function (name) {

    var idx = -1;
    var found = false;

    for (var i = 0; i < this.options.headers.length; i++) {

        var header = this.options.headers[i];

        if (header.hidden)
            continue;

        if (header.name === name) {
            found = true;
        }

        idx++;

        if (found)
            break;

    }

    return found ? idx : -1;
}

Table.prototype.getHeader = function (name) {

    for (var i = 0; i < this.options.headers.length; i++) {

        var header = this.options.headers[i];

        if (header.name === name) {
            return header;
        }

    }

}

Table.prototype.getVisibleHeaders = function () {

    return $.grep(this.options.headers, function (el, i) {
        return !el.hidden;
    });

}

Table.prototype.updateHeaders = function () {
    this.setHeaders(this.options.headers);
}

Table.prototype.setTableWidth = function (width) {

    this.log("setting table width", width);
    this.header.width(width + 100);
    this.table.width(width);
}

Table.prototype.activeRow = function () {

    console.log("ACTIVE ROW", this.body.find(".active").index());

    return this.rows[this.body.find(".active").index() - 1];
}

Table.prototype.updateRow = function (rowItem, index) {
    this.addRows([rowItem], index);
    this.deleteRowSilent(index + 1);
}

Table.prototype.updateRows = function (rowItems, filter) {

    var items = [];

    var ri = rowItems;

    for (var j = ri.length - 1; j >= 0; j--) {

        for (var i = 0; i < this.rows.length; i++) {

            var a = this.rows[i];
            var b = ri[j];

            if (filter(a, b)) {
                items.push({ idx: i, obj: b });
                ri.splice(j, 1);
                break;
            }

        }

    }
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        this.updateRow(item.obj, item.idx);
    }
}

Table.prototype.addRows = function (rowItems, index) {

    var me = this;
    var table = this;
    var body = this.body;
    var headers = this.options.headers;

    //console.log(rowItems);

    me.log("addRows", rowItems);

    if (this.header.length === 0) {
        throw "Headers not set.";
    }

    if (rowItems !== undefined && rowItems !== null && rowItems.length > 0) {
        this.body.find(".norow").remove();
    }

    if (index !== undefined)
        Array.prototype.splice.apply(this.rows, [index, 0].concat(rowItems));
    else
        this.rows = this.rows.concat(rowItems);

    if (body.find("> .cs").length === 0) {

        var row = $("<tr>", { class: "cs" }).css("visibility", "collapse").appendTo(body);

        headers.forEach(function (h, hidx) {

            if (h.hidden)
                return true;
            var td = $("<td>").appendTo(row);
            td.append("<span>");
            td.width(h.width - 1);

            me.log("setting cs width", h.width);
        });

        row.append("<td>");
    }

    rowItems.forEach(function (r, ridx) {

        var row = $("<tr>");
        row.data("row", r);
        row.attr("data-rowdata", r.hiddenData);

        //console.log(r);

        if (index !== null) {
            var cur = body.find('tr').eq(index + 1);
            if (cur !== undefined && cur !== null && cur.length == 1) {
                cur.before(row);
                index++;
            }
            else {
                row.appendTo(body);
                index = body.children().length;
            }
        } else {
            row.appendTo(body);
        }

        if (r.id !== undefined) {
            row.attr("data-id", r.id);
        }

        if (r.attr !== undefined) {
            for (var i in r.attr) {
                row.attr("data-" + i, r.attr[i]);
            }
        }

        if (r.class !== undefined) {
            row.addClass(r.class);
        }

        headers.forEach(function (h, hidx) {

            if (h.hidden)
                return true;
            var col = h.hasOwnProperty("alias") ? h.alias.toLowerCase().replace(/[ ]*/g, '') : h.name.toLowerCase().replace(/[ ]*/g, '');

            var td = $("<td>");

            if (h.right) {
                td.attr("style", "text-align:right;");
            }

            if (h.class !== undefined) {
                for (var i = 0; i < h.class.length; i++) {
                    td.addClass(h.class[i]);
                }
            }

            td.addClass("it-col-" + col);

            //if (h.class !== undefined) {
            //    for (var i = 0; i < h.class.length; i++) {
            //        td.addClass(h.class[i]);
            //    }
            //}

            //if (h.attr !== undefined) {
            //    for (var i in h.attr) {
            //        td.attr("data-" + i, h.attr[i]);
            //    }
            //}

            var html = $(table).triggerHandler("columnFormatter", [hidx, r, ridx]);
            var d = null;

            for (var di = 0; di < r.data.length; di++) {
                var data = r.data[di];

                if (data !== null && data[col] !== undefined) {
                    d = data;
                    break;
                }
            }

            if (d == null)
                d = r.data[hidx];

            if (d !== null) {

                if (html === undefined || html === null) {
                    var sp = $("<span>", { class: "val" });
                    sp.css("height", me.options.rowHeight + "px");
                    td.append(sp);
                    //sp.text(d[col] !== undefined ? d[col] : d.value !== undefined ? d.value : d);
                    sp.html(d[col] !== undefined ? d[col] : d.value !== undefined ? d.value : d);

                } else {
                    td.append($(html).css("height", me.options.rowHeight + "px"));
                }


                if (d.hasOwnProperty("class")) {
                    td.addClass(d.class);
                }

            }

            td.appendTo(row);

        });


        row.append("<td>");

    });

    $(table).trigger("rowsAdded");

}

Table.prototype.addBlankRow = function (id, index, attr) {
    var headers = this.header.children('div[class^="it-head-"]').length;
    var data = [];
    for (var i = 0; i < headers; i++) {
        data.push({ value: '' });
    }
    var row = {
        id: id,
        attr: attr,
        data: data,
    };
    this.addRows([row], index);
}

Table.prototype.deleteRow = function (idx) {

    $(this).trigger("deleted", [idx, this.rows[idx]]);
    this.deleteRowSilent(idx);
}

Table.prototype.deleteRowSilent = function (idx) {
    this.body.children()[idx + 1].remove();
    this.rows.splice(idx, 1);
    //console.log('rows', this.rows);
    if (this.rows.length === 0)
        this.showNoRows();
}

Table.prototype.deleteRows = function (filter) {
    for (var i = this.rows.length - 1; i >= 0; i--) {
        if (filter(this.rows[i])) {
            this.deleteRow(i);
        }
    }
}

Table.prototype.showNoRows = function () {

    var vis = this.getVisibleHeaders();

    var r = $("<tr>", { class: "norow" }).append($("<td>", { colspan: vis.length }).append($("<span>").text(this.options.noRecordsText)));
    this.body.append(r);
}

Table.prototype.clearRows = function () {
    this.body.empty();
    this.rows.splice(0, this.rows.length);
}

Table.prototype.uniqueID = function () {


    var url = window.location.pathname.split('/');

    if (url.length >= 4) {
        var id = this.container.attr("id");

        return url[1] + url[2] + url[3] + id;
    }



}

Table.prototype.saveSettings = function () {

    if (!this.options.saveSettings) {
        return;
    }

    var tcon = Cookies.get(this.cookieName);



    if (tcon === undefined || tcon === null) {
        tcon = {};
    } else {
        tcon = JSON.parse(tcon);

    }

    var tconID = this.uniqueID();

    if (tconID !== undefined && tconID !== null) {

        var data = [];

        for (var h = 0; h < this.options.headers.length; h++) {
            var he = this.options.headers[h];

            data.push({ width: he.width });
        }

        tcon[tconID] = data;

        this.log("settings for " + tconID, tcon);


        Cookies.set(this.cookieName, JSON.stringify(tcon));

    }
}

Table.prototype.setActive = function (id, loud) {
    var b = this.body;

    //setTimeout(function () {

    var ev = loud ? "click" : "silentclick";

    //console.log("ev", ev);

    b.find("tr[data-id=" + id + "]").trigger(ev);

    //}, 100);
}

Table.prototype.displayOverview = function () {

    var me = this;
    var hasData = false;

    this.overview.empty();
    this.overview.parent().removeClass("dis");

    //console.log(this);

    if (!this.options.displayOverview) {
        return;
    }

    var vHeight = this.tableContainer.height() - 53/*top margin*/;
    var cHeight = this.body.height();
    var rHeight = this.body.find("tr:not(.cs)").first().height();

    //console.log("Heights", [vHeight, cHeight, rHeight]);

    if (cHeight < vHeight) {
        //If there is no scroll bar then do not show overview
        return;
    }

    var ratio = vHeight / cHeight * rHeight;

    for (var i = 0; i < this.rows.length; i++) {

        var r = this.rows[i];

        if (r.overview !== undefined && r.overview.display) {

            hasData = true;

            var loc = ratio * i;

            var ov = $("<div>", { class: "ov" })
                .css("top", loc);

            if (r.overview.color)
                ov.css("background-color", r.overview.color);

            if (r.overview.title)
                ov.attr("title", r.overview.title);

            (function idWrap(id) {

                $(ov).on("click", function () {
                    me.setActive(id, true);
                });

            })(r.id);

            this.overview.append(ov);

        }

    }

    if (hasData)
        this.overview.parent().addClass("dis");
}

Table.prototype.events = function () {

    var me = this;
    var tc = this.table;
    var head = this.header;
    var bod = this.tableContainer;

    var scrollTimeout = null;

    var start = 0;

    this.tableContainer.scroll(function () {
        head.offset({ left: tc.offset().left });

        var con = this;

        clearTimeout(scrollTimeout);
        scrollTimeout = setTimeout(function () {

            if ($(con).height() + $(con)[0].scrollTop >= $(tc).height()) {
                $(me).trigger("scrollBottom");
            }

        }, 300);

    });

    if (!this.options.singleClick) {

        this.table.on("click silentclick", "tr:not(.norow)", function (e) {

            if (e.shiftKey && !$(this).hasClass("active") && me.options.isMultiSelect) {

                var start = $(this).index();
                var startIndex = $(this).prevAll('.active:first').index();

                for (var i = start < startIndex ? start : startIndex; i <= (start > startIndex ? start : startIndex); i++) {
                    $(tc.find("tbody").children()[i]).addClass("active");
                }

            } else if (e.ctrlKey && me.options.isMultiSelect) {

                $(this).toggleClass("active");

            } else {

                tc.find("tr.active").removeClass("active");
                $(this).addClass("active");

                var t = this.offsetTop;
                var st = bod.scrollTop();
                var h = bod.height();

                //console.log("scroll", [t, st, h, ot, this]);

                if (t - st < 0 || t - st > h)
                    bod.scrollTop(t);
            }

            var selected = [];

            tc.find("tr.active").each(function () {
                var r = me.rows[$(this).index() - 1];
                selected.push({ row: this, data: r });
            });

            if (e.type === "click")
                $(me).trigger("selected", { selected: selected, hasItems: selected.length > 0 });

        });

        this.table.on("dblclick", "tr", function (e) {
            var r = $(this).data("row"); //me.rows[$(this).index()];

            //console.log($(this).data());
            //console.log("r", r);
            $(me).trigger("doubleClick", { row: this, data: r });
        });

    } else {
        this.table.on("click", "tr", function (e) {
            var r = $(this).data("row"); //me.rows[$(this).index()];

            //console.log($(this).data());
            //console.log("r", r);
            $(me).trigger("doubleClick", { row: this, data: r });
        });
    }


    this.header.on("dblclick", "span", function (e) {

        var header = $(this).parent();

        var l = header.find(".it-header").text();

        var ts = $(head).find(".it-ts");
        if (ts.length == 0) {
            ts = $("<div>").addClass("it-ts");
            ts.hide();
            $(head).append(ts);
        }

        ts.text(l);

        var width = ts.width() + $(this).width() + 10;
        var dif = (width) - header.width();

        $(this).parent().width(width);
        var hw = head.width() + dif;

        tc.width(hw - 100 /*header width padding*/);
        head.width(hw);

        var col = $(tc.find("tr").first().children()[header.index()]);
        col.width(width);

        me.options.headers[header.index()].width = width + 1;

        me.saveSettings();

    });

    this.header.on("mousedown", "span", function (e) {
        var con = me.container.find(".it-cont");

        var drag = con.find(".db");
        drag.show();
        drag.offset({ left: e.pageX + 2 });

        var header = $(this).parent();
        start = e.pageX;

        $(document).on("mousemove.table", function (ef) {
            drag.offset({ left: ef.pageX });
        });

        $(document).on("mouseup.table", function (eg) {
            $(document).off("mousemove.table");
            $(document).off("mouseup.table");

            drag.hide();

            var dif = ((start - eg.pageX) * -1);
            var width = header.width() + dif;

            if (width < 50) {
                dif += 50 - width;
                width = 50;
            }

            header.width(width);

            var hw = head.width() + dif;

            tc.width(hw - 100 /*header width padding*/);
            head.width(hw);

            var col = $(tc.find("tr").first().children()[header.index()]);
            col.width(width);

            me.options.headers[header.index()].width = width + 1;

            me.saveSettings();
        });

        return true;
    });

    $(this).on("rowsAdded", function () {

        me.displayOverview();

    });

}

Table.prototype.calculateAutoWidth = function () {

    var me = this;

    var autoHeaders = 0;
    var staticWidth = 0;

    this.options.headers.forEach(function (h) {

        if (h.width == 'auto')
            autoHeaders++;
        else
            staticWidth += h.width == undefined ? me.options.columnWidth : h.width;
    });

    this.log("auto width", [autoHeaders, staticWidth]);

    if (autoHeaders == 0)
        return 0;

    var width = this.container.width();
    this.log("table width", width);
    var autowidth = (width - staticWidth) / autoHeaders;

    if (autowidth < this.options.columnWidth)
        return this.options.columnWidth
    else
        return autowidth;

}

//EVENTS
//columnFormatter (event,columnIndex,rowData,rowIndex) called on addRow
//EXAMPLE
//function (event, columnIndex, rowData, rowIndex) {

//    if (this.options.headers[columnIndex].name === "#") {
//        return '<span>' + (rowIndex + 1) + '</span>';
//    }

//})


//headerFormatter (event,headerIndex,header) called on setHeaders
//selected (event, {selected:[{row: <tr>, data:rowData},...]) 'Triggered on mouse click'
//doubleClick (event, {row: <tr>, data:rowData}) 'Triggered on mouse dblclick'
//deleted (event, rowIndex, rowData) 'Triggered on deleteRow(idx)'

//ROW DATA
//{ id:[], attr:{}, data: [{ [value|headerName: "290"]|[val] }, ...], class:["","",""] }

//HEADER DATA
//{ 
//      name: [text],\
//      alias: [text], if exists is used instead of 'name' when matching row data by name
//      width:[num],
//      drag: [true | false],
//      right:[true | false],
//      class:["","",""],
//      type: [string | num | ...] used when sorting, type maps to options.sort.add custom sort func to both type and option.sort  
//      hidden:[true | false]							 
//}

//FUNCTIONS
//addRows([ROW DATA, ...], index)
//deleteRow(idx)
//deleteRows(filter)
//clearRows()
//setHeaders()
//getHeaderIndex(name)					  

//var tableOptions = {
//    noRecordsText:[TEXT], 'noRecordsText is displayed when the table contains no records',																							
//    isMultiSelect:[true|false],
//    headers: [HEADER DATA, ...], 
//    headerFormatter:function(index,header) returns html|jquery obj
//    columnWidth: 200, 'Default width of all columns'
//    columnDrag: true, 'overrides header drag if false!',
//    allowSort:[true | false],
//    sort:{ string:func(), num:func() }
//    debug:[true | false] outputs logging to console
//    saveSettings:[true | false] if true saves configuration to cookie													 																   
//};

//var t = new Table(containerSelector, tableOptions);

