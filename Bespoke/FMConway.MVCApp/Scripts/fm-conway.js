﻿
// global settings
moment.locale('en-gb');

function padTo(value, digits) {
    var number;
    if (value == null) {
        return null;
    } else if ($.type(value) === "string") {
        number = Number(value);
    } else {
        number = value;
    }
    var str = number.toString();
    if (str.indexOf('.') >= 0) {
        digits = Math.max(digits, str.split('.')[1].length);
    }
    return Number(str).toFixed(digits);
}

// when select table row check if single checkbox in last column and click (if this causes too many issues just remove it)
$('body').on('click', 'table tr td:not(:last-child)', function (a, b) {
    var base = $(this);
    var tr = base.closest('tr');
    var table = tr.closest('table');
    var lastTd = tr.children('td:last-child');

    if (table.hasClass('skip-autoselect')) {
        return;
    }
    
    var checkboxCount = lastTd.find('.multi-select-css-label,.single-select-css-icon');
    if (checkboxCount.length != 1) {
        return;
    }

    // sometimes the click event is bound to the label, othertimes to the input (ugh). so check both.
    lastTd.find('input,label,i').each(function () {
        var ev = $._data(this, 'events');
        if (ev && ev.click) {
            $(this).click();
            return false;
        }
    });
});

// hasAttr
$.fn.hasAttr = function (name) {
    var attr = $(this).attr(name);
    return typeof attr !== typeof undefined && attr !== false;
};

// auto-resize (https://stackoverflow.com/a/25621277/3961743)
$('textarea.auto-resize').each(function () {
    this.setAttribute("style", "height:" + (this.scrollHeight + 10) + "px;overflow-y:hidden;");
}).on("input", function () {
    var base = this;
    this.style.height = "auto";
    this.style.height = (this.scrollHeight + 10) + "px";

    if (!this.timeout) {
        this.timeout = setTimeout(function () {
            base.timeout = null;
            $(".masonry").masonry();
        }, 500);
    }
}).trigger("input");

// formats (data-type)
$('body').on('keydown paste', 'input[data-type="numbers"],input[data-type="positive-numbers"]', function (e) {
    var base = $(this);
    var positiveOnly = $(this).attr('data-type') == 'positive-numbers';

    if (e.type == 'paste') {
        setTimeout(function () {
            var trimmed = base.val().trimAllExcept('1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-');
            if (positiveOnly) {
                trimmed = trimmed.replace('-', '');
            }
            base.val(trimmed.trimDouble(true));
        }, 1);
    }

    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode >= 35 && e.keyCode <= 40) ||
        ((e.keyCode == 88 || e.keyCode == 67 || e.keyCode == 86) && (e.ctrlKey === true || e.metaKey === true)) ||
        (!positiveOnly && (e.keyCode == 173 || e.keyCode == 189) && e.target.selectionStart == 0 && !e.target.value.includes("-"))) {
        return;
    } else if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$('body').on('keydown paste', 'input[data-type="decimals"],input[data-type="positive-decimals"]', function (e) {
    var base = $(this);
    var positiveOnly = $(this).attr('data-type') == 'positive-decimals';

    if (e.type == 'paste') {
        setTimeout(function () {
            var trimmed = base.val().trimAllExcept('1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', '-');
            if (positiveOnly) {
                trimmed = trimmed.replace('-', '');
            }
            base.val(trimmed.trimDouble(true));
        }, 1);
    }

    if (e.keyCode == 190 && e.target.value.includes(".")) {
        e.preventDefault();
    } else if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        ((e.keyCode == 65 || e.keyCode == 89 || e.keyCode == 90) && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode >= 35 && e.keyCode <= 40) ||
        ((e.keyCode == 88 || e.keyCode == 67 || e.keyCode == 86) && (e.ctrlKey === true || e.metaKey === true)) ||
        (!positiveOnly && (e.keyCode == 173 || e.keyCode == 189) && e.target.selectionStart == 0 && !e.target.value.includes("-"))) {
        return;
    } else if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

// trim double spaces (optional regular trim)
Object.defineProperty(String.prototype, "trimDouble", {
    value: function trimDouble(includeTrim) {
        includeTrim = includeTrim == null ? false : includeTrim;
        var trimmed = this.replace(/\s\s+/g, ' ');
        return includeTrim ? trimmed.trim() : trimmed;
    },
    writable: true,
    configurable: true
});

// trim all except (char1, char2, etc)
Object.defineProperty(String.prototype, "trimAllExcept", {
    value: function trimAllExcept() {
        var args = [].slice.call(arguments);
        var result = '';

        for (var char in this) {
            var accepted = args.includes(this[char]);
            if (accepted) {
                result += this[char];
            }
        }

        return result;
    },
    writable: true,
    configurable: true
});








