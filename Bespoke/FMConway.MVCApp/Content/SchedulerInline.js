
// global defaults
moment.locale('en-gb');
alertify.defaults.glossary.title = 'Information';

// global variables
var data = {};

// dragover (drag operative / vehicle over scheduler)
$('#scheduler').on('dragover', '.inlinejob:not(.confirmed) .ve, .inlinejob:not(.confirmed) .op', function (ev) {
    ev.preventDefault();
    var target = $(ev.target).hasClass("items") ? $(ev.target) : $(ev.target).parents(".items");

    if (canDrop(data, target)) {
        if (target.find(".placeholder").length == 0) {
            target.append($("<div>").addClass("placeholder").append(data.description));
        }
        showHide(target);
    }
});

$('#scheduler').on('dragleave', '.inlinejob:not(.confirmed) .ve, .inlinejob:not(.confirmed) .op', function (ev) {
    ev.preventDefault();

    var target = $(ev.target).hasClass("items") ? $(ev.target) : $(ev.target).parents(".items");

    target.find(".placeholder").remove();
    showHide(target);
});

function drag(ev) {
    var isOperative = $(ev.target).parents("#operatives").length > 0;

    data = {
        id: $(ev.target).data("id"),
        isOperative: isOperative,
        description: $(ev.target).html()
    };

    ev.dataTransfer.setData("text", data);
}

// drop
$('#scheduler').on('drop', '.inlinejob:not(.confirmed) .ve, .inlinejob:not(.confirmed) .op', function (ev) {
    ev.preventDefault();

    var target = $(ev.target).hasClass("items") ? $(ev.target) : $(ev.target).parents(".items");
    console.log(target)
    var date = target.closest('.group.day').data('date');

    if (canDrop(data, target) && canDropFinalCheck(data.isOperative, data.id, data.description, date)) {

        var holder = target.find(".placeholder");
        holder.removeClass("placeholder");

        dropComplete(holder, data.isOperative, data.id, data.description);

        // add employees vehicle (works in both directions)
        if (data.isOperative) {
            var vehicleID = $('#operatives ul li[data-id="' + data.id + '"]').data('vehicleid');
            if (vehicleID.length) {
                var vehicleItems = target.closest('.inlinejob').find('.items.ve');
                var alreadyExists = vehicleItems.find('#' + vehicleID).length;
                if (!alreadyExists) {
                    var description = $('#vehicles ul li[data-id="' + vehicleID + '"]').html();
                    if (canDropFinalCheck(!data.isOperative, vehicleID, description, date)) {
                        var div = $("<div>");
                        vehicleItems.append(div);
                        dropComplete(div, !data.isOperative, vehicleID, description);
                        showHide(vehicleItems);
                    }
                }
            }
        } else {
            var employeeLI = $('#operatives ul li[data-vehicleid="' + data.id + '"]');
            if (employeeLI.length) {
                var employeeID = employeeLI.data('id');
                var operativeItems = target.closest('.inlinejob').find('.items.op');
                var alreadyExists = operativeItems.find('#' + employeeID).length;
                if (!alreadyExists) {
                    var description = employeeLI.html();
                    if (canDropFinalCheck(!data.isOperative, employeeID, description, date)) {
                        var div = $("<div>");
                        operativeItems.append(div);
                        dropComplete(div, !data.isOperative, employeeID, description);
                        showHide(operativeItems);
                    }
                }
            }
        }
    }

    showHide(target);
});

function dropComplete(holder, isOperative, id, description) {
    holder.attr("data-id", id);
    holder.attr("id", id);
    holder.html(description);

    if (isOperative) {
        holder.addClass("Operative");
    }
    else {
        holder.addClass("Vehicle");
    }

    holder.append($("<div>").addClass("rem fa fa-remove"));
}

// this function is called repeatidly whilst place holder is hovered over
function canDrop(data, target) {
   
    if ((data.isOperative && (target.hasClass("ve") || target.parents(".ve").length > 0)) || (!data.isOperative && (target.hasClass("op") || target.parents(".op").length > 0))) {
        return false;
    }

    var findExistsingSelector = "div[data-id='" + data.id + "']";
    if (target.find(findExistsingSelector).length > 0) {
        return false;
    }


    var activeJob = target.hasClass('active') || target.parents('.inlinejob.active').length;
    if (!activeJob) {
        return false;
    }

    var confirmed = !target.parents('.inlinejob.NoShift').length;
    if (confirmed) {
        return false;
    }

    return true;
}

// this function is called one time after the user has dropped but before applying any changes
function canDropFinalCheck(isOperative, id, description, date) {
    return true;
}

function showHide(itemContainer) {
    if (itemContainer.find("div:not(.no)").length == 0)
        itemContainer.find(".no").show();
    else
        itemContainer.find(".no").hide();
}

function refreshJobWidth(job) {

    var width = 0;

    $(job).children().each(function () {

        if ($(this).width() > width)
            width = $(this).width();

    });

}

$(function () {




    //highlight operatives/vehicles in use on Day when Job is selected
    $('.inlinejob').on('click', function () {

        var day = $(this).closest('.day');

        var operativesList = [];
        var vehicleList = [];
        var dayOperatives = [];
        var dayVehicles = [];
        var o = 0;
        var v = 0;

        $('#operatives').each(function () {
            $(this).find('li').each(function () {
                operativesList.push($(this));
            });
        });

        $('#vehicles').each(function () {
            $(this).find('li').each(function () {
                vehicleList.push($(this));
            });
        });

        day.find('.inlinejobs .Operative').each(function () {
            dayOperatives.push($(this).data('id'));
        });

        day.find('.inlinejobs .Vehicle').each(function () {
            dayVehicles.push($(this).data('id'));
        });

        for (o = 0; o < operativesList.length; o++) {
            var opID = operativesList[o].data('id');
            if (!!~jQuery.inArray(opID, dayOperatives)) {
                operativesList[o].addClass('operativeInUse');
            } else {
                operativesList[o].removeClass('operativeInUse');
            }
        }

        for (v = 0; v < vehicleList.length; v++) {
            var veID = vehicleList[v].data('id');
            if (!!~jQuery.inArray(veID, dayVehicles)) {
                vehicleList[v].addClass('vehicleInUse');
            } else {
                vehicleList[v].removeClass('vehicleInUse');
            }
        }

    });

    // work instruction
    $('.inlinejob').on('click', function (a) {
        var target = $(a.target);

        if (target.hasClass('fa-remove') || target.hasClass('saveButton') || target.hasClass('confirmButton')) {
            return;
        }
        
        var base = $(this);
        var container = $('#container');
        var id = $(this).data('workinstructionid');

        $('.inlinejob').not(this).removeClass('active');

        if (base.hasClass('active')) {
            base.removeClass('active');
            base.next().find(".inlinejob").removeClass('active');
            container.attr('data-workinstructionid', '');
        } else {
            base.addClass('active');
            base.next().find(".inlinejob").addClass('active');
            container.attr('data-workinstructionid', id);
        }

        refreshOperatives();
        refreshVehicles();
    });

    $('.workinstruction-hyperlink').on('click', function (a) {
        a.stopImmediatePropagation();
    });

    $('#qualified-staff').on('change', function () {
        refreshOperatives();
    });

    $('#available-staff').on('change', function () {
        refreshOperatives();
    });

    $('#employee-search').on('input', function () {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(function () {
            refreshOperatives();
        }, 200);
    });

    function refreshOperatives() {
       
        var activeJob = $('.inlinejob.active');
        var contractDepoID = activeJob.data('depoid');
    
        if (activeJob.length) {
            
            var employeeSearch = $('#employee-search').val();
            var employeeSearchUpperCase = employeeSearch.toUpperCase();

            $('#operatives > ul > li').each(function () {
                // rename
                var employeeName = $(this).data('name');
                var employeeNameUpper = employeeName.toUpperCase().trim();
                var employeeIsAgency = parseBool($(this).data('isagency'));
                var employeeDepoID = $(this).data('depoid');
                var employeeDepotName = $(this).data('depo');
                var employeeDepotNameUpper = employeeDepotName.toUpperCase().trim();
                var employeeType = $(this).data('type');

                employeeName += '<br /><span class="operatives-type">[' + employeeType + ']</span>';

                var name;
                if (employeeIsAgency && employeeDepoID != contractDepoID) {
                    name = employeeName + ' <span class="operatives-depo">(' + employeeDepotName + ' Agcy)</span>';
                } else if (!employeeIsAgency && employeeDepoID != contractDepoID) {
                    name = employeeName + ' <span class="operatives-depo">(' + employeeDepotName + ')</span>';
                } else if (employeeIsAgency && employeeDepoID == contractDepoID) {
                    name = employeeName + ' <span class="operatives-depo">(Agcy)</span>';
                } else {
                    name = employeeName;
                }

                $(this).html(name);

                // filter
                var show = true;

                if (show && employeeSearch) {
                    if ((employeeNameUpper.indexOf(employeeSearchUpperCase) == -1) &&
                        (employeeDepotNameUpper.indexOf(employeeSearchUpperCase) == -1)) {
                        show = false;
                    }
                }

                var onlyQualifiedStaff = $('#qualified-staff').prop('checked');
                if (show && onlyQualifiedStaff) {
                    var staffQualifications = $(this).data('qualifications');
                    var requiredQualifications = activeJob.find('.job-certificate-type').map(function () {
                        return $(this).val();
                    }).get();
                    var missingQualifications = requiredQualifications.filter(function (a) {
                        return staffQualifications.indexOf(a) < 0;
                    });
                    if (missingQualifications.length) {
                        show = false;
                    }
                }

                var onlyAvailableStaff = $('#available-staff').prop('checked');
                if (show && onlyAvailableStaff) {
                    var activeJobDate = activeJob.closest('.group.day').attr('data-date');
                    var staffAbsences = $(this).data('absences');
                    var absenceThisDay = staffAbsences.filter(function (a) {
                        return a == activeJobDate;
                    });
                    if (absenceThisDay.length) {
                        show = false;
                    }
                }

                var depotID = $('#DepotID').val();
                if (show && depotID != "" && $(this).attr('data-depoid') != depotID) {
                    show = false;
                }

                if (show) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });

            // order by
            // available from this depo
            // available from other depo
            // available from this depo + agency staff
            // available from other depo + agency staff

            var ul = $('#operatives ul');
            var operatives = ul.find('li');

            [].sort.call(operatives, function (a, b) {
                // agency
                var agencySort = 0;
                var agencyA = parseBool($(a).data('isagency'));
                var agencyB = parseBool($(b).data('isagency'));

                if (agencyA && agencyB) {
                    agencySort = 0;
                } else if (agencyA && !agencyB) {
                    agencySort = 1;
                } else if (!agencyA && agencyB) {
                    agencySort = -1;
                } else if (!agencyA && !agencyB) {
                    agencySort = 0;
                }

                // depo
                var depoSort = 0;
                var depoA = $(a).data('depoid') == contractDepoID;
                var depoB = $(b).data('depoid') == contractDepoID;

                if (depoA && depoB) {
                    depoSort = 0;
                } else if (depoA && !depoB) {
                    depoSort = -1;
                } else if (!depoA && depoB) {
                    depoSort = 1;
                } else if (!depoA && !depoB) {
                    depoSort = 0;
                }

                // name
                var nameSort = $(a).data('name').localeCompare($(b).data('name'));

                // 1 = down, 0 no change, -1 up
                return agencySort || depoSort || nameSort;
            });

            ul.append(operatives);
        } else {
            $('#operatives > ul > li').hide();
        }
    }

    $('#available-vehicle').on('change', function () {
        refreshVehicles();
    });

    $('#vehicle-search').on('input', function () {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(function () {
            refreshVehicles();
        }, 200);
    });

    function refreshVehicles() {
        var activeJob = $('.inlinejob.active');

        if (activeJob.length) {
            var vehicleSearch = $('#vehicle-search').val();
            var vehicleSearchUpperCase = vehicleSearch.toUpperCase();

            $('#vehicles > ul > li').each(function () {
                var description = $(this).text();
                var descriptionUpper = description.toUpperCase();

                // filter
                var show = true;

                if (show && vehicleSearch) {
                    if (descriptionUpper.indexOf(vehicleSearchUpperCase) == -1) {
                        show = false;
                    }
                }

                var onlyAvailableVehicles = $('#available-vehicle').prop('checked');
                if (show && onlyAvailableVehicles) {
                    var activeJobDate = activeJob.closest('.group.day').attr('data-date');
                    var vehicleAbsences = $(this).data('absences');
                    var absenceThisDay = vehicleAbsences.filter(function (a) {
                        return a == activeJobDate;
                    });
                    if (absenceThisDay.length) {
                        show = false;
                    }
                }

                if (show) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        } else {
            $('#vehicles > ul > li').hide();
        }
    }

    $("#scheduler").on("click", ".rem", function (e) {

        var itemContainer = $(this).parents(".items");
        $(this).parent().remove();
        showHide(itemContainer);

    });

    $("#Contracts").on('change', function () {
        $("#ContractID").val($(this).val());
    });

    $("#Depots").on('change', function () {
        $("#DepotID").val($(this).val());
    });

    getShiftProgress();

    setInterval(getShiftProgress, 10000);

    $(".unconfirmButton").on("click", function (e) {
        e.stopImmediatePropagation();

        var base = $(this);

        alertify.confirm('Are you sure you want to unconfirm this Work Instruction?', function () {
            var job = base.closest(".inlinejob");
            var scheduleID = job.data('id');

            var updatedDate = job.find('.schedule-updated-date').val();
            var updatedDateFormatted = moment(updatedDate, 'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');

            $.ajax({
                type: "POST",
                url: "/Operation/Schedule/UnconfirmSchedule",
                data: { scheduleID: scheduleID, updatedDate: updatedDateFormatted },
                success: function (data) {
                    if (data.Response === "Success") {
                        var newUpdatedDateFormatted = moment(data.UpdatedDate).format('DD/MM/YYYY HH:mm:ss');
                        job.find('.schedule-updated-date').val(newUpdatedDateFormatted);

                        unconfirmed(job);

                        job.removeClass('NotStarted');
                        job.addClass("NoShift");
                        job.closest('tr').prev().removeClass('NotStarted')
                        job.closest('tr').prev().addClass("NoShift");
                        job.find('.foInline .confirm').show();

                        var jobTime = $('#' + job.attr('id') + "_time");
                        jobTime.text('');
                    } else if (data.Response === "Outdated") {
                        alertify.alert("Operation Cancelled", "This WI schedule has been updated by another user.<br>Please use Ctrl + F5 to refresh the scheduler.");
                    } else {
                        alertify.alert("Error!", "Failed to unconfirm. Shift has potentially started, refresh scheduler.");
                    }
                },
                error: function (result) {
                    alertify.alert("Error!", "Failed to unconfirm.");
                }
            });
        });

        function unconfirmed(job) {
            var rem = job.find(".rem");
            rem.show();

            var fo = job.find('.foInline');
     
            var confirmbutton = fo.find(".confirm");
            confirmbutton.show();

            var savebutton = fo.find(".save");
            savebutton.show();

            var unconfirmbutton = fo.find(".unconfirm");
            unconfirmbutton.hide();
        };
    });

    $(".confirmButton").on("click", function () {
        var jobElement = this.closest(".inlinejob");
        var job = $(jobElement);
        
 
        var workInstructionNumber = $(this).closest('.inlinejob').find('.workinstruction-hyperlink').text();

        // vehicles
        var vehicles = jobElement.getElementsByClassName("Vehicle");

        var vehiclesToSend = [];

        for (var i = 0; i < vehicles.length; i++) {
            vehiclesToSend.push(vehicles[i].id);
        }
        console.log(vehiclesToSend)
        // operatives
        var operatives = jobElement.getElementsByClassName("Operative");
        var operativesToSend = [];

        for (var i = 0; i < operatives.length; i++) {
            operativesToSend.push(operatives[i].id);
        }

        // validate
        if (operatives.length == 0 || vehicles.length == 0) {
            alertify.alert("Incomplete!", "Please add vehicle(s) and operative(s) to job " + workInstructionNumber);
            return;
        }

        var hasForeman = false;
        $(jobElement).find('.Operative').each(function () {
            var id = $(this).data('id');
            var type = $('#operatives ul li[data-id="' + id + '"]').data('type');
            if (type == 'Crew Foreman') {
                hasForeman = true;
                return false;
            }
        });

        var isHighSpeedJob = $(jobElement).find('.job-speed').find('span').hasClass('fa-road');
        if (!hasForeman && isHighSpeedJob) {
            alertify.alert("Incomplete!", "Please add a Crew Foreman to job " + workInstructionNumber);
            return;
        }

        // save
        var updatedDate = job.find('.schedule-updated-date').val();
        var updatedDateFormatted = moment(updatedDate, 'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');

        var dataToSend = JSON.stringify({
            scheduleID: job.attr("data-id"),
            operatives: operativesToSend,
            vehicles: vehiclesToSend,
            updatedDate: updatedDateFormatted,
            confirmed: true
        });

        $.ajax({
            type: "POST",
            url: "/Operation/Schedule/ConfirmSchedule",
            data: dataToSend,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: function (data) {
                if (data.Response === "Success") {
                    var updatedDateFormatted = moment(data.UpdatedDate).format('DD/MM/YYYY HH:mm:ss');
                    job.find('.schedule-updated-date').val(updatedDateFormatted);

                    confirmed(jobElement);

                    job.removeClass('NoShift');
                    job.addClass('NotStarted');
                    job.closest('tr').prev().removeClass('NoShift')
                    job.closest('tr').prev().addClass("NotStarted");
                    job.find('.foInline .confirm').hide();

                    var jobTime = $('#' + jobElement.id + "_time");
                    jobTime.text("Checks Time: N/A");

                    job.removeClass("active");
                    $('#container').attr('data-workinstructionid', '');
                    refreshOperatives();
                    refreshVehicles();
                } else if (data.Response === "Vehicles") {
                    alertify.alert("Incomplete!", "Please ensure that only one vehicle is added.");
                } else if (data.Response === "Outdated") {
                    alertify.alert("Operation Cancelled", "This WI schedule has been updated by another user.<br>Please use Ctrl + F5 to refresh the scheduler.");
                } else if (data.Response === "WorkInstructionInactive") {
                    alertify.alert("Operation Cancelled", "This WI has been made inactive, re-enable it to enable scheduling or refresh the scheduler to remove it.");
                } else {
                    alertify.alert("Error!", "Failed to confirm. Please try again.");
                }
            },
            error: function (result) {
                alertify.alert("Error!", "Failed to confirm. Please try again.");
            }
        });
        var rem = job.find(".rem");
        rem.show();


        function confirmed(job) {
            var selected = $("#" + job.id);
            var rem = $(selected).find(".rem");
            rem.hide();
 
            var fo = selected.find('.foInline');
         
            var confirmbutton = fo.find(".confirm");
            confirmbutton.hide();
          
            var savebutton = fo.find(".save");
            savebutton.hide();
            
            var unconfirmbutton = fo.find(".unconfirm");
            unconfirmbutton.show();
            
        };

    });

    $('.dayColumn [id$="confirmAll"]').click(function () {

        var date = $(this).prev('input').val();
        var day = $(this).parent('div').text().trim();
        day = day.split(" ")[0];
        var selectedDay = $(this).closest('.day');
        alertify.confirm('Confirm', 'Are you sure you want to send all shifts on ' + day + ' ' + date + ' to tablet?',
            function () {
                selectedDay.find('.job').each(function (i, obj) {
                    if ($(this).find('.confirm')) {
                        $(this).find('.confirm').find('.confirmButton').click();
                    }
                });saveButton
            },
            function () {
            });
    });

    $(".saveButton").on("click", function () {
        //alert("Test");
        var jobElement = this.closest(".inlinejob");
        var job = $(jobElement);

        // vehicles
        var vehicles = jobElement.getElementsByClassName("Vehicle");
        var vehiclesToSend = [];

        for (var i = 0; i < vehicles.length; i++) {
            vehiclesToSend.push(vehicles[i].id);
        }

        // operatives
        var operatives = jobElement.getElementsByClassName("Operative");
        var operativesToSend = [];

        for (var i = 0; i < operatives.length; i++) {
            operativesToSend.push(operatives[i].id);
        }

        // validate
        var hasForeman = false;
        $(jobElement).find('.Operative').each(function () {
            var id = $(this).data('id');
            var type = $('#operatives ul li[data-id="' + id + '"]').data('type');
            if (type == 'Crew Foreman') {
                hasForeman = true;
                return false;
            }
        });

        var isHighSpeedJob = $(jobElement).find('.job-speed').find('span').hasClass('fa-road');

        if (!hasForeman && isHighSpeedJob) {
            alertify.alert("Incomplete!", "Please add a Crew Foreman to the job.");
            return;
        }

        // save
        var updatedDate = job.find('.schedule-updated-date').val();
        var updatedDateFormatted = moment(updatedDate, 'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');

        var dataToSend = JSON.stringify({
            scheduleID: job.attr("data-id"),
            operatives: operativesToSend,
            vehicles: vehiclesToSend,
            updatedDate: updatedDateFormatted,
            confirmed: false
        });

        $.ajax({
            type: "POST",
            url: "/Operation/Schedule/ConfirmSchedule",
            data: dataToSend,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: function (data) {
                if (data.Response === "Success") {
                    var updatedDateFormatted = moment(data.UpdatedDate).format('DD/MM/YYYY HH:mm:ss');
                    job.find('.schedule-updated-date').val(updatedDateFormatted);

                    save(jobElement);

                    job.removeClass("active");
                    $('#container').attr('data-workinstructionid', '');
                    refreshOperatives();
                    refreshVehicles();
                } else if (data.Response === "Vehicles") {
                    alertify.alert("Incomplete!", "Please ensure that only one vehicle is added.");
                } else if (data.Response === "Outdated") {
                    alertify.alert("Operation Cancelled", "This WI schedule has been updated by another user.<br>Please use Ctrl + F5 to refresh the scheduler.");
                } else if (data.Response === "WorkInstructionInactive") {
                    alertify.alert("Operation Cancelled", "This WI has been made inactive, re-enable it to enable scheduling or refresh the scheduler to remove it.");
                } else {
                    alertify.alert("Error!", "Failed to save. Please try again.");
                }
            },
            error: function (result) {
                alertify.alert("Error!", "Failed to save. Please try again.");
            }
        });

        function save(job) {
            var selected = $("#" + job.id);
            var fo = selected.find('.foInline');
            var button = fo.find(".confirm");
            button.show();
        };

    });

    function getShiftProgress() {
        var updatedTime = $('#UpdatedTime');
        var contract = $('#ContractID');

        var lastUpdate = updatedTime.val();
        var lastUpdateFormatted = moment(lastUpdate, 'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');

        var currentDateFormatted = moment().format('DD/MM/YYYY HH:mm:ss');
        updatedTime.val(currentDateFormatted);

        //console.log(lastUpdateFormatted);

        $.post("shiftprogressupdate", { lastUpdate: lastUpdateFormatted, contractID: contract.val() }, function (data) {

            if (data != 0) {

                $.each(data, function (i, j) {

                    var jobBox = $('#' + j.ScheduleID);
                    if (!jobBox.length) {
                        return;
                    }

                    var jobTime = $('#' + j.ScheduleID + "_time");
                    var times = jobTime.closest('.times');
                    var unconfirm = jobBox.find('.unconfirm');

                    if (j.ShiftProgress === "NotStarted") {
                        return;
                    }

                    jobBox.removeClass("NoShift");
                    jobBox.removeClass("NotStarted");
                    jobBox.removeClass("ChecksCompleted");
                    jobBox.removeClass("TasksStarted");
                    jobBox.removeClass("Completed");
                    jobBox.removeClass("Cancelled");

                    jobBox.addClass(j.ShiftProgress);

                    var str = "";
                    if (j.ShiftProgress === "ChecksCompleted") {
                        str = "Checks Completed: " + j.CompletedTime;
                        jobTime.text(str);
                        times.show();
                        unconfirm.hide();
                    }
                    else if (j.ShiftProgress === "TasksStarted") {
                        str = "Tasks Started: " + j.CompletedTime;
                        jobTime.text(str);
                        times.show();
                        unconfirm.hide();
                    }
                    else if (j.ShiftProgress === "Completed") {
                        str = "Completed: " + j.CompletedTime;
                        jobTime.text(str);
                        times.show();
                        unconfirm.hide();
                    }
                    else if (j.ShiftProgress === "Cancelled") {
                        str = "Cancelled: " + j.CompletedTime;
                        jobTime.text(str);
                        times.show();
                        unconfirm.hide();
                    }
                });
            };
        });
    };


    function hideNextAndCurrentRow(row) {
        var nextRow = row.next();
        nextRow.hide();
        row.hide();
    }

    function showCurrentAndNextIfOpen(row) {
        row.show();
        var nextRow = row.next('tr');
        if (row.hasClass("open")) {
            nextRow.show();
        }
        else {
            nextRow.hide();
        }
    }


    // status legend
    $('.le .c').on('click', function () {
        var base = $(this);
        base.toggleClass('active');

        var statuses = $('.le .c.active').map(function () {
            return {
                name: $(this).data('status'),
                jobType: $(this).data('jobtype')
            }
        });

        $('.row1-visibility').each(function () {
            var job = $(this);
            if (statuses.length) {
                statuses.each(function (a) {
                    if (job.hasClass(this.name)) {
                        showCurrentAndNextIfOpen(job)
                        return false;
                    }
                    else {
                        hideNextAndCurrentRow(job)
                    }
                });
            } else {
                showCurrentAndNextIfOpen(job)
            }
        });
    });


    $('.le .n').on('click', function () {
        var base = $(this);
        base.toggleClass('active');

        var statuses = $('.le .n.active').map(function () {
            return {
                shiftType: $(this).data('shifttype'),
            }
        });
        console.log(statuses)
        console.log($('.row1-visibility').length)
        $('.row1-visibility').each(function () {
            var job = $(this);
            if (statuses.length) {
                statuses.each(function (a) {
                    if (job.hasClass(this.shiftType)) {
                        showCurrentAndNextIfOpen(job)
                        return false;
                    }
                    else {
                        hideNextAndCurrentRow(job)
                    }
                });

            } else {
                showCurrentAndNextIfOpen(job)
                //return false;
            }
        });
    });

    $('.img-showdetails').on('click', function (e) {
        e.stopPropagation();
        var id = $(this).data('id');
        $(".row2-visibility-" + id).toggle();


        var isPlus = this.src.indexOf('plus.png') == -1;
        var img = isPlus ? "plus.png" : "minus.png";

        if (isPlus) {
            $(this).closest('tr').addClass("closed")
            $(this).closest('tr').removeClass("open")
        }
        else {
            $(this).closest('tr').addClass("open")
            $(this).closest('tr').removeClass("closed")
        }


        $(this).attr("src", "/images/" + img);

    });


    $('.expand').on('click', function () {
        var text = $(this).html();

        if (text == "Show All") {
            $(this).html('Hide All')
            $(this).addClass("showAllSelected");
            $(".img-showdetails").attr("src", "/images/minus.png");
            $(".row1-visibility").addClass("open")
            $(".row1-visibility").removeClass("closed")
            $(".row2-visibility").show();
        }
        else {
            $(this).html('Show All');
            $(this).removeClass("showAllSelected");
            $(".img-showdetails").attr("src", "/images/plus.png");
            $(".row1-visibility").removeClass("open")
            $(".row1-visibility").addClass("closed")
            $(".row2-visibility").hide();
        }
    });


    // summary days
    $('.summary-days a').on('click', function () {
        activeSummaryDay(this);
    });

    function activeSummaryDay(summaryDay) {
        var summaryDay = $(summaryDay);

        if (!summaryDay.length) {
            return;
        }

        $('.summary-days a').removeClass('active');
        summaryDay.addClass('active');

        // add anchor to form href (so when page posts / reloads the current anchor is kept)
        var form = $('.form-horizontal');
        var action = form.attr('action');
        var hashIndex = action.indexOf('#');
        if (hashIndex >= 0) {
            action = action.substring(0, hashIndex);
        }
        action = action + summaryDay.attr('href');
        form.attr('action', action);

        // temporarily switch off scrolling deactivation whilst anchor scrolls
        $('#scheduler').off('scroll.summary');

        setTimeout(function () {
            $('#scheduler').on('scroll.summary', function () {
                $('#schedInfo .summary-days a').removeClass('active');
            });
        }, 500);
    }

    $('.popup-tooltip').on('click', function (e) {
        e.stopImmediatePropagation();

        var existingContainer = $('.popup-container');
        if (existingContainer.length) {
            var existingContainerButton = existingContainer.data('button');
            existingContainer.remove();
            $('#scheduler').off('scroll.pt');
            $('body').off('click.pt');

            // if same job description is clicked, just close and return
            if ($(this).is(existingContainerButton)) {
                return;
            }
        }

        var button = $(this);
        var header = button.data('popup-tooltip-header');
        var text = button.data('popup-tooltip-text');

        var html =
            '<div class="popup-container">' +
            '   <div class="popup-header clearfix">' + (header || '') + '<i aria-hidden="true" class="fa fa-remove popup-close"></i></div>' +
            '   <div class="popup-body">' + (text || 'No job description') + '</div>' +
            '</div>';
        html = $(html);

        $('body').append(html);

        // add button reference to container data
        html.data('button', button);

        // position
        var buttonOffset = button.offset();
        var halfModalWidth = html.outerWidth() / 2;
        var halfButtonWidth = button.outerWidth() / 2;

        var modalTop = buttonOffset.top + button.outerHeight() + 3;
        var modalLeft = buttonOffset.left - halfModalWidth + halfButtonWidth;

        html.css('top', modalTop);
        html.css('left', modalLeft);

        // click x
        html.on('click', '.popup-close', function () {
            closePopupModal();
        });

        // scroll scheduler
        $('#scheduler').on('scroll.pt', function () {
            closePopupModal();
        });

        // click outside of modal
        $('body').on('click.pt', function (e) {
            if (!$(e.target).closest('.popup-container').length) {
                closePopupModal();
            }
        });

        function closePopupModal() {
            html.remove();
            $('#scheduler').off('scroll.pt');
            $('body').off('click.pt');
        }
    });

    // order-number
    $('.order-number').on('click', function (e) {
        e.stopImmediatePropagation();

        var existingContainer = $('.order-number-modal');
        if (existingContainer.length) {
            var existingContainerButton = existingContainer.data('button');
            existingContainer.remove();
            $('#scheduler').off('scroll.on');
            $('body').off('click.on');

            // if same job description is clicked, just close and return
            if ($(this).is(existingContainerButton)) {
                return;
            }
        }

        var button = $(this);
        var orderNumber = +button.attr('data-order-number');

        var html =
            '<div class="order-number-modal">' +
            '   <div class="header clearfix"><span>Order in which task appears on the app</span><i aria-hidden="true" class="fa fa-remove close-button"></i></div>' +
            '   <div class="container">' +
            '       <div class="row">' +
            '           <div class="selection" data-selection="1">1</div>' +
            '           <div class="selection" data-selection="2">2</div>' +
            '           <div class="selection" data-selection="3">3</div>' +
            '       </div> ' +
            '       <div class="row">' +
            '           <div class="selection" data-selection="4">4</div>' +
            '           <div class="selection" data-selection="5">5</div>' +
            '           <div class="selection" data-selection="6">6</div>' +
            '       </div> ' +
            '       <div class="row">' +
            '           <div class="selection" data-selection="7">7</div>' +
            '           <div class="selection" data-selection="8">8</div>' +
            '           <div class="selection" data-selection="9">9</div>' +
            '       </div> ' +
            '   </div> ' +
            '</div>';
        html = $(html);

        if (orderNumber) {
            var selection = html.find('.selection').filter(function (a) {
                return $(this).data('selection') == orderNumber;
            }).eq(0);

            selection.addClass('active');
        }

        $('body').append(html);

        // add button reference to container data
        html.data('button', button);

        // position
        var buttonOffset = button.offset();
        var halfModalWidth = html.outerWidth() / 2;
        var halfButtonWidth = button.outerWidth() / 2;

        var modalTop = buttonOffset.top + button.outerHeight() + 3;
        var modalLeft = buttonOffset.left - halfModalWidth + halfButtonWidth;

        html.css('top', modalTop);
        html.css('left', modalLeft);

        // selection
        html.on('click', '.selection', function () {
            var selection = $(this);
            var workInstructionID = button.closest('.job').data('workinstructionid');
            var orderNumber = selection.data('selection');

            $.ajax({
                type: "POST",
                url: "/Operation/Schedule/SaveOrderNumber",
                data: { workInstructionID: workInstructionID, orderNumber: orderNumber },
                success: function () {
                    button.attr('data-order-number', orderNumber);
                    button.text(orderNumber);
                    closeModal();
                },
                error: function () {
                    alertify.alert("Error!", "An error has occurred. Please try again.");
                }
            });
        });

        // click x
        html.on('click', '.close-button', function () {
            closeModal();
        });

        // scroll scheduler
        $('#scheduler').on('scroll.on', function () {
            closeModal();
        });

        // click outside of modal
        $('body').on('click.pt', function (e) {
            if (!$(e.target).closest('.order-number-modal').length) {
                closeModal();
            }
        });

        function closeModal() {
            html.remove();
            $('#scheduler').off('scroll.on');
            $('body').off('click.on');
        }
    });

    // onload
    activeSummaryDay('.summary-days a[href="#' + window.location.hash.substr(1) + '"]');

});

// helpers
function parseBool(val) {
    if ((typeof val === 'string' && (val.toLowerCase() === 'true' || val.toLowerCase() === 'yes')) || val === 1)
        return true;
    else if ((typeof val === 'string' && (val.toLowerCase() === 'false' || val.toLowerCase() === 'no')) || val === 0)
        return false;
    return null;
}
