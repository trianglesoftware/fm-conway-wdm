﻿
var triangle = {
    ui: {
        lookup: function (id, url, rowCount, isMultiSelect) {

            this.currentPage = 0;
            this.totalPages = 0;

            this.id = id;
            this.rowCount = rowCount;
            this.url = url;
            this.isMultiSelect = isMultiSelect;
            this.selectedRows = [];
            this.value = $(id).val();

            this.searchTimeout = null;
            this.searchQuery = "";

            this.disabled = $(this.id + "-lookup a").hasClass("disabled");

            var thisLookup = this;

            this.SortByName = function (a, b) {
                var aName = a.key.toLowerCase();
                var bName = b.key.toLowerCase();
                return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
            }

            this.GetPage = function (page) {
                thisLookup.currentPage = page;

                var lookupData = null;

                var modalFormBody = $(thisLookup.id + "-modal .modal-body");
                modalFormBody.empty();

                var loader = $("<div>").addClass("ajax-loader").appendTo(modalFormBody);
                $("<img>").prop("src", "/Content/images/loader.gif").appendTo(loader);

                var params = { page: page, rowCount: thisLookup.rowCount, query: thisLookup.searchQuery };

                if (thisLookup.UrlParameters != null) {
                    var customerParams = thisLookup.UrlParameters();
                    params = this.MergeObjects(params, customerParams);
                }

                $.get(thisLookup.url, $.param(params, true), function (data) {
                    lookupData = data;

                    if (lookupData != null) {

                        thisLookup.totalPages = lookupData.PageCount;

                        // generate table with columns

                        var table = $("<table>").addClass("table table-striped table-bordered");
                        modalFormBody.append(table);

                        var tableHeader = $("<tr>");
                        table.append(tableHeader);

                        $.each(lookupData.ColumnHeaders, function (i, col) {
                            tableHeader.append($("<th>").html(col));
                        });

                        //Action column
                        tableHeader.append($("<th>"));

                        // add row data                  
                        $.each(lookupData.Rows, function (i, rowData) {

                            var row = $("<tr>");
                            table.append(row);

                            if (rowData.TableData != null) {
                                $.each(rowData.TableData, function (i, data) {
                                    row.append($("<td>").html(data));
                                });
                            }

                            // Add selection value
                            if (thisLookup.isMultiSelect) {

                                var selector = $("<input type='checkbox' />").addClass("multi-select css-checkbox").val(rowData.Key).data("key", rowData.Value).data("rowData", rowData.Data).prop("id", rowData.Key);
                                var lab = $("<label>").addClass("multi-select-css-label").prop("for", rowData.Key);

                                for (var i = 0; i < thisLookup.selectedRows.length; i++) {
                                    if (thisLookup.selectedRows[i].key === rowData.Value) {
                                        selector.attr("checked", "checked");
                                        lab.addClass("checked");
                                        break;
                                    }
                                }

                                row.append($("<td>").append(selector).append(lab));

                            } else {

                                //I think bootstrap looks for the attribute 'data-dismiss' and will not go looking in the object cache for it; thats why I havn't used 'data()' to set the dismiss value
                                var icon = $("<i>").addClass("action-select single-select-css-icon").data("key", rowData.Key).data("value", rowData.Value).data("rowData", rowData.Data).attr("data-dismiss", "modal").attr("title", "Select").appendTo($("<td>").appendTo(row));
                            }

                        });

                        //hookup table events
                        $(thisLookup.id + "-modal .modal-body .multi-select").click(function () {

                            var cont = $(this);

                            if (cont.is(":checked")) {
                                thisLookup.selectedRows.push({ key: cont.data("key"), value: cont.val() });
                                $('label[for=' + cont.val() + ']').addClass("checked");
                            } else {
                                for (var i = 0; i < thisLookup.selectedRows.length; i++) {
                                    if (thisLookup.selectedRows[i].value === cont.val()) {
                                        thisLookup.selectedRows.splice(i, 1);
                                        $('label[for=' + cont.val() + ']').removeClass("checked");
                                        break;
                                    }
                                }
                            }

                        });

                        $(thisLookup.id + "-modal .modal-body .action-select").on("click", function () {
                            thisLookup.Selected($(this).data("key"), $(this).data("value"), $(this).data("rowData"));
                        });

                        // generate pager
                        if (thisLookup.totalPages > 1) {
                            thisLookup.GeneratePager(modalFormBody);
                        }
                    }
                }).success(function () {
                    $(".modal-header .search").removeAttr("disabled");
                })
                .fail(function () {

                    var error = $("<div>").addClass("ajax-error").appendTo(modalFormBody);

                    $("<p>").text("An error has occured. Please try again.").appendTo(error);

                }).always(function () { loader.hide(); });

            }

            this.GetNextPage = function () {
                var nextPage = thisLookup.currentPage + 1;
                if (nextPage < thisLookup.totalPages)
                    thisLookup.GetPage(nextPage);
            }

            this.GetPrevPage = function () {
                var prevPage = thisLookup.currentPage - 1;
                if (prevPage >= 0)
                    thisLookup.GetPage(prevPage);
            }

            this.Selected = function (key, value, data) {

                $(thisLookup.id).val(value);
                thisLookup.value = value;

                var output = $(thisLookup.id + "-lookup .selected");
                output.val(key);

                thisLookup.AddRemovalButtons();

                if (thisLookup.SelectedEvent != null)
                    thisLookup.SelectedEvent(key, value, data);
            }

            this.MultipleSelected = function (keyValues) {

                var list = $(thisLookup.id);
                list.empty();

                var childCount = 0;

                $.each(keyValues, function () {

                    var listItem = $("<li>").addClass("selected").text(this.value);

                    $("<input type='hidden' class='hidden-key'>").prop("name", thisLookup.id.replace('#', '').replace('-', '.') + "[" + childCount + "].Key").val(this.key).appendTo(listItem);
                    $("<input type='hidden' class='hidden-value'>").prop("name", thisLookup.id.replace('#', '').replace('-', '.') + "[" + childCount + "].Value").val(this.value).appendTo(listItem);

                    list.append(listItem);
                    childCount++;
                });

                thisLookup.AddRemovalButtons();

                if (thisLookup.MultipleSelectedEvent != null)
                    thisLookup.MultipleSelectedEvent(keyValues);

            }

            this.GeneratePager = function (modalFormBody) {
                var pager = $("<div class='pagination pagination-centered'>");
                modalFormBody.append(pager);

                var list = $("<ul>");
                pager.append(list);

                var backwards = $("<li>").append($("<a>").html("&laquo;").attr("href", "#").addClass("pager-action").data("page", "--"));

                if (thisLookup.currentPage == 0)
                    backwards.addClass("disabled");

                list.append(backwards);

                var startPage = 1;
                var endPage = 5;

                if (thisLookup.currentPage >= 3) {

                    startPage = thisLookup.currentPage - 1;
                    endPage = thisLookup.currentPage + 3;

                    if (endPage > thisLookup.totalPages) {
                        endPage = thisLookup.totalPages;
                        startPage = endPage - 4;
                    }

                }

                if (endPage > thisLookup.totalPages)
                    endPage = thisLookup.totalPages;

                for (var i = startPage; i <= endPage; i++) {
                    if (i == thisLookup.currentPage + 1) {
                        list.append($("<li>").addClass("active").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i)));
                        continue;
                    }

                    list.append($("<li>").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i - 1)));
                }

                var forwards = $("<li>").append($("<a>").html("&raquo;").attr("href", "#").addClass("pager-action").data("page", "++"));

                if (thisLookup.currentPage == thisLookup.totalPages)
                    forwards.addClass("disabled");

                list.append(forwards);

                $(thisLookup.id + "-modal .modal-body .pagination .pager-action").on("click", function () {

                    var page = $(this).data("page");

                    if ($(this).parent().hasClass("active"))
                        return;

                    switch (page) {
                        case "--": thisLookup.GetPrevPage(); break;
                        case "++": thisLookup.GetNextPage(); break;
                        default: thisLookup.GetPage(page); break;
                    }
                });
            }

            this.Enable = function () {
                thisLookup.disabled = false;
                $(thisLookup.id + "-lookup .lookup-button").removeClass("disabled").removeAttr("disabled").attr("data-toggle", "modal");

            }

            this.Disable = function () {
                thisLookup.disabled = true;
                $(thisLookup.id + "-lookup .lookup-button").addClass("disabled").attr("disabled", "disabled").removeAttr("data-toggle");

                thisLookup.Clear();
            }

            this.Clear = function () {
                if (thisLookup.isMultiSelect)
                    $(thisLookup.id + '-lookup .mli-remove').click();
                else {
                    $(thisLookup.id + '-lookup .li-remove').click();
                    $(thisLookup.id).val('');
                }
            }

            this.AddRemovalButtons = function () {

                $.each($(thisLookup.id + "-lookup .selected"), function (idx, select) {

                    if (thisLookup.isMultiSelect) {
                        if ($(select).text() != "")
                            $("<a>").addClass("mli-remove").attr("href", "#").appendTo($(select)).append($("<i>").addClass("icon-remove icon-inactive"));
                    } else {
                        if ($(select).val() != "")
                            $("<a>").addClass("li-remove").attr("href", "#").appendTo($(select).parent()).append($("<i>").addClass("icon-remove icon-inactive"));
                    }

                });

                thisLookup.HookupRemovalEvents();
            }

            this.HookupRemovalEvents = function () {

                if (thisLookup.isMultiSelect)

                    $(this.id + '-lookup .mli-remove').on('click', function () {

                        $(this).parent("li").remove();

                        $.each($(thisLookup.id).children("li"), function (index, child) {

                            $(child).find('.hidden-key').prop("name", thisLookup.id.replace('#', '').replace('-', '.') + "[" + index + "].Key")
                            $(child).find('.hidden-value').prop("name", thisLookup.id.replace('#', '').replace('-', '.') + "[" + index + "].Value")
                        });

                        if (thisLookup.RemovedEvent != null) {

                            var key = $(this).parent("li").find('.hidden-key').prop("name");
                            var value = $(this).parent("li").find('.hidden-value').prop("name")
                            thisLookup.RemovedEvent(key, value);
                        }

                        return false;
                    });
                else
                    $(this.id + '-lookup .li-remove').on('click', function (e) {

                        var key = $(this).parent().find("input").val();

                        $(this).parent().find("input").val('');

                        var value = $(thisLookup.id).val();
                        $(thisLookup.id).val('');
                        thisLookup.value = null;

                        $(this).unbind('click');
                        $(this).remove();

                        if (thisLookup.RemovedEvent != null)
                            thisLookup.RemovedEvent(key, value);

                        return false;
                    });
            }

            this.SelectedEvent;
            this.RemovedEvent;
            this.MultipleSelectedEvent;

            this.UrlParameters;

            this.MergeObjects = function (obj1, obj2) {
                var obj3 = {};
                for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
                for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
                return obj3;
            }

            $(this.id + "-lookup a").on("click", function () {

                if (thisLookup.disabled)
                    return;

                //reset props and retrive first page
                thisLookup.searchQuery = "";
                thisLookup.searchTimeout = null;
                thisLookup.selectedRows = [];

                if (thisLookup.isMultiSelect) {

                    $.each($(thisLookup.id).children("li"), function () {

                        thisLookup.selectedRows.push({ key: $(this).find(".hidden-key").val(), value: $(this).find(".hidden-value").val() });
                    });
                }

                thisLookup.GetPage(0);
            });

            $(this.id + "-modal .modal-footer .action-select").on("click", function () {

                thisLookup.MultipleSelected(thisLookup.selectedRows.sort(thisLookup.SortByName));
            });

            $(this.id + "-modal .modal-header .search").keyup(function () {

                thisLookup.searchQuery = $(this).val();

                clearTimeout(thisLookup.searchTimeout);
                thisLookup.searchTimeout = setTimeout(function () { thisLookup.GetPage(0) }, 1000);

            });

            this.AddRemovalButtons();

        },
        editableLookup: function (id, url, rowCount) {
            this.currentPage = 0;
            this.totalPages = 0;

            this.id = id;
            this.rowCount = rowCount;
            this.url = url;

            this.selectedRows = [];

            this.searchTimeout = null;
            this.searchQuery = "";

            this.disabled = $(this.id + "-lookup a").hasClass("disabled");

            var thisLookup = this;

            this.SortByName = function (a, b) {
                var aName = a.key.toLowerCase();
                var bName = b.key.toLowerCase();
                return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
            }

            this.GetPage = function (page) {
                thisLookup.currentPage = page;

                var lookupData = null;

                var modalFormBody = $(thisLookup.id + "-modal .modal-body");
                modalFormBody.empty();

                var loader = $("<div>").addClass("ajax-loader").appendTo(modalFormBody);
                $("<img>").prop("src", "/Content/images/loader.gif").appendTo(loader);

                var params = { page: page, rowCount: thisLookup.rowCount, query: thisLookup.searchQuery };

                if (thisLookup.UrlParameters != null) {
                    var customerParams = thisLookup.UrlParameters();
                    params = this.MergeObjects(params, customerParams);
                }

                $.get(thisLookup.url, params, function (data) {
                    lookupData = data;

                    if (lookupData != null) {

                        thisLookup.totalPages = lookupData.PageCount;

                        // generate table with columns

                        var table = $("<table>").addClass("table table-striped table-bordered");
                        modalFormBody.append(table);

                        var tableHeader = $("<tr>");
                        table.append(tableHeader);

                        $.each(lookupData.ColumnHeaders, function (i, col) {
                            tableHeader.append($("<th>").text(col));
                        });

                        //Action column
                        tableHeader.append($("<th>"));

                        // add row data                  
                        $.each(lookupData.Rows, function (i, rowData) {

                            var row = $("<tr>");
                            table.append(row);
                            if (rowData.TableData != null) {
                                $.each(rowData.TableData, function (i, data) {
                                    row.append($("<td>").text(data));
                                });
                            }

                            //I think bootstrap looks for the attribute 'data-dismiss' and will not go looking in the object cache for it; thats why I havn't used 'data()' to set the dismiss value
                            var icon = $("<i>").addClass("action-select single-select-css-icon").data("key", rowData.Key).data("value", rowData.Value).data("rowData", rowData.Data).attr("data-dismiss", "modal").attr("title", "Select").appendTo($("<td>").appendTo(row));
                        });

                        //hookup table events
                        $(thisLookup.id + "-modal .modal-body .action-select").on("click", function () {
                            thisLookup.Selected($(this).data("key"), $(this).data("value"), $(this).data("rowData"));
                        });

                        // generate pager
                        if (thisLookup.totalPages > 1) {
                            thisLookup.GeneratePager(modalFormBody);
                        }
                    }
                }).success(function () {
                    $(".modal-header .search").removeAttr("disabled");
                })
                .fail(function () {

                    var error = $("<div>").addClass("ajax-error").appendTo(modalFormBody);

                    $("<p>").text("An error has occured. Please try again.").appendTo(error);

                }).always(function () { loader.hide(); });

            }

            this.GetNextPage = function () {
                var nextPage = thisLookup.currentPage + 1;
                if (nextPage < thisLookup.totalPages)
                    thisLookup.GetPage(nextPage);
            }

            this.GetPrevPage = function () {
                var prevPage = thisLookup.currentPage - 1;
                if (prevPage >= 0)
                    thisLookup.GetPage(prevPage);
            }

            this.Selected = function (key, value, data) {

                $(thisLookup.id).val(value);

                if (thisLookup.SelectedEvent != null)
                    thisLookup.SelectedEvent(key, value, data);
            }

            this.GeneratePager = function (modalFormBody) {
                var pager = $("<div class='pagination pagination-centered'>");
                modalFormBody.append(pager);

                var list = $("<ul>");
                pager.append(list);

                var backwards = $("<li>").append($("<a>").html("&laquo;").attr("href", "#").addClass("pager-action").data("page", "--"));

                if (thisLookup.currentPage == 0)
                    backwards.addClass("disabled");

                list.append(backwards);

                var startPage = 1;
                var endPage = 5;

                if (thisLookup.currentPage > 3) {

                    startPage = thisLookup.currentPage - 1;
                    endPage = thisLookup.currentPage + 3;

                    if (endPage > thisLookup.totalPages) {
                        endPage = thisLookup.totalPages;
                        startPage = endPage - 4;
                    }
                }
                if (endPage > thisLookup.totalPages)
                    endPage = thisLookup.totalPages;
                for (var i = startPage; i <= endPage; i++) {
                    if (i == thisLookup.currentPage + 1) {
                        list.append($("<li>").addClass("active").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i)));
                        continue;
                    }

                    list.append($("<li>").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i - 1)));
                }

                var forwards = $("<li>").append($("<a>").html("&raquo;").attr("href", "#").addClass("pager-action").data("page", "++"));

                if (thisLookup.currentPage == thisLookup.totalPages)
                    forwards.addClass("disabled");

                list.append(forwards);

                $(thisLookup.id + "-modal .modal-body .pagination .pager-action").on("click", function () {

                    var page = $(this).data("page");

                    if ($(this).parent().hasClass("active"))
                        return;

                    switch (page) {
                        case "--": thisLookup.GetPrevPage(); break;
                        case "++": thisLookup.GetNextPage(); break;
                        default: thisLookup.GetPage(page); break;
                    }
                });
            }

            this.Enable = function () {
                thisLookup.disabled = false;
                $(thisLookup.id + "-lookup .lookup-button").removeClass("disabled").removeAttr("disabled").attr("data-toggle", "modal");

            }

            this.Disable = function () {
                thisLookup.disabled = true;
                $(thisLookup.id + "-lookup .lookup-button").addClass("disabled").attr("disabled", "disabled").removeAttr("data-toggle");
                $(thisLookup.id + '-lookup .li-remove').click();
            }

            this.SelectedEvent;
            this.RemovedEvent;
            this.MultipleSelectedEvent;

            this.UrlParameters;

            this.MergeObjects = function (obj1, obj2) {
                var obj3 = {};
                for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
                for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
                return obj3;
            }

            $(this.id + "-lookup a").on("click", function () {

                if (thisLookup.disabled)
                    return;

                //reset props and retrive first page
                thisLookup.searchQuery = "";
                thisLookup.searchTimeout = null;
                thisLookup.selectedRows = [];

                if (thisLookup.isMultiSelect) {

                    $.each($(thisLookup.id).children("li"), function () {

                        thisLookup.selectedRows.push({ key: $(this).find(".hidden-key").val(), value: $(this).find(".hidden-value").val() });
                    });
                }

                thisLookup.GetPage(0);
            });

            $(this.id + "-modal .modal-footer .action-select").on("click", function () {

                thisLookup.MultipleSelected(thisLookup.selectedRows.sort(thisLookup.SortByName));
            });

            $(this.id + "-modal .modal-header .search").keyup(function () {

                thisLookup.searchQuery = $(this).val();

                clearTimeout(thisLookup.searchTimeout);
                thisLookup.searchTimeout = setTimeout(function () { thisLookup.GetPage(0) }, 1000);

            });

        },
        table: function (containerID, url, rowCount, urlParams, loadedEvent, focusOnSearch) {

            this.url = url;
            this.containerID = containerID;
            this.rowCount = rowCount;
            this.currentPage = 0;
            this.totalPages = 0;
            this.urlParams = urlParams;
            this.searchBarRevert = true;
            this.searchQuery = null;
            this.searchBarExpandedSize = 500;
            this.searchBarSize = 200;
            this.LoadedEvent = loadedEvent;

            var thisTable = this;

            this.GetPage = function (page) {

                this.currentPage = page;

                var container = $(thisTable.containerID);

                if (container == null || container == undefined)
                    throw "Cannot find elememnt '" + thisTable.containerID + "'. Please specify a containerID."

                container.empty();

                var loader = $("<div>").addClass("ajax-loader").appendTo(container);
                $("<img>").prop("src", "/Content/images/loader.gif").appendTo(loader);


                var params = { rowCount: thisTable.rowCount, page: page, query: thisTable.searchQuery };

                if (thisTable.urlParams != null) {
                    params = this.MergeObjects(params, thisTable.urlParams);
                }

                $.get(thisTable.url, $.param(params, true), function (data) {

                    thisTable.totalPages = data.PageCount;

                    thisTable.SearchBar(container);

                    var table = $("<table>").addClass('table table-striped table-bordered').appendTo(container);

                    var hBody = $("<thead>").appendTo(table)
                    var tBody = $("<tbody>").appendTo(table)

                    var header = $("<tr>").appendTo(hBody);

                    $.each(data.ColumnHeaders, function (index, columnHeader) {

                        header.append($("<th>").addClass(columnHeader.replace(' ', '-').toLowerCase()).text(columnHeader));
                    });

                    var includesActions = false;

                    $.each(data.Rows, function (index, rowData) {

                        var row = $("<tr>").appendTo(tBody);

                        if (rowData.TableData != null) {
                            $.each(rowData.TableData, function (index, dta) {
                                row.append($("<td>").html(dta));
                            });
                        }
                        row.data("rowData", rowData.Data);

                        if (rowData.Actions != null && rowData.Actions.length > 0) {

                            includesActions = true;

                            $.each(rowData.Actions, function (index, dta) {

                                var action = $("<a>").addClass(dta.Class).prop("href", dta.Url).prop("title", dta.Title).append($("<i>").addClass(dta.IconClass));

                                row.append($("<td>").append(action));
                            });
                        }

                    });

                    if (includesActions)
                        header.append($("<th>").addClass("table-actions"));

                    if (thisTable.totalPages > 1)
                        thisTable.GeneratePager(container);

                    if (thisTable.LoadedEvent != null)
                        thisTable.LoadedEvent();

                }).fail(function () {

                    var error = $("<div>").addClass("ajax-error").appendTo(container);

                    $("<p>").text("An error has occured. Please try again.").appendTo(error);

                }).always(function () { loader.hide(); });


            }

            this.GeneratePager = function (container) {
                var pager = $("<div class='pagination pagination-centered'>").appendTo(container);

                var list = $("<ul>");
                pager.append(list);

                var backwards = $("<li>").append($("<a>").html("&laquo;").attr("href", "#").addClass("pager-action").data("page", "--"));

                if (thisTable.currentPage == 0)
                    backwards.addClass("disabled");

                list.append(backwards);

                var startPage = 1;
                var endPage = 5;

                if (thisTable.currentPage >= 3) {

                    startPage = thisTable.currentPage - 1;
                    endPage = thisTable.currentPage + 3;

                    if (endPage > thisTable.totalPages) {
                        endPage = thisTable.totalPages;
                        startPage = endPage - 4;
                    }
                }

                if (endPage > thisTable.totalPages)
                    endPage = thisTable.totalPages;

                for (var i = startPage; i <= endPage; i++) {
                    if (i == thisTable.currentPage + 1) {
                        list.append($("<li>").addClass("active").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i - 1)));
                        continue;
                    }

                    list.append($("<li>").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i - 1)));
                }

                var forwards = $("<li>").append($("<a>").html("&raquo;").attr("href", "#").addClass("pager-action").data("page", "++"));

                if (thisTable.currentPage == thisTable.totalPages)
                    forwards.addClass("disabled");

                list.append(forwards);

                $(thisTable.containerID + " .pagination .pager-action").on("click", function () {

                    var page = $(this).data("page");

                    if ($(this).parent().hasClass("active"))
                        return;

                    switch (page) {
                        case "--": thisTable.GetPrevPage(); break;
                        case "++": thisTable.GetNextPage(); break;
                        default: thisTable.GetPage(page); break;
                    }
                });
            }

            this.GetPrevPage = function () {
                var prevPage = thisTable.currentPage - 1;
                if (prevPage >= 0)
                    thisTable.GetPage(prevPage);
            }

            this.GetNextPage = function () {
                var nextPage = thisTable.currentPage + 1;
                if (nextPage < thisTable.totalPages)
                    thisTable.GetPage(nextPage);
            }

            this.MergeObjects = function (obj1, obj2) {
                var obj3 = {};
                for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
                for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
                return obj3;
            }

            this.SearchBar = function (container) {

                var searchContainer = $("<div>").addClass("search-container").appendTo(container);
                var searchQuery = $("<div>").addClass("search-query").append().width(thisTable.searchBarSize).appendTo(searchContainer);
                var searchIcon = $("<i>").addClass("icon-search").appendTo(searchQuery);
                var searchInput = $("<input type='text'>").addClass("search-query-input").attr("placeholder", "Search").val(thisTable.searchQuery).appendTo(searchQuery);

                if (thisTable.searchQuery != null && thisTable.searchQuery != "") {
                    searchQuery.attr("style", "width:" + thisTable.searchBarExpandedSize + "px");
                    thisTable.searchBarRevert = false;
                }

                if(focusOnSearch)
                $(searchInput).focus();

                searchIcon.on("click", function () {

                    thisTable.GetPage(0);
                });

                searchInput.on("focus.table", function () {

                    $(this).parent().animate({ width: thisTable.searchBarExpandedSize }, 400);

                });

                searchInput.on("keydown.table", function (e) {

                    //e.preventDefault();

                    var input = $(this).val();

                    var canRevert = input == "";

                    thisTable.searchBarRevert = canRevert;

                    thisTable.searchQuery = input;

                    if (e.which == 13/*Enter*/) {
                        thisTable.GetPage(0);
                    }

                });

                searchInput.on("blur.table", function () {

                    if (thisTable.searchBarRevert) {

                        $(this).parent().animate({ width: thisTable.searchBarSize }, 400);
                    }
                });


            }

            thisTable.GetPage(0);

        },
        collectionEditor: function (collectionId) {

            var thisCollectionEditor = this;
            this.IsEditing = false;
            this.Container = null;

            var obj = $(collectionId);
            if (obj == null)
                return;

            this.ID = collectionId;
            this.Container = obj;

            this.AddControl = null;
            this.Delete = null;
            this.AddEvent = null;
            this.Editor = null;

            this.Cancel = function () {

                var editor = thisCollectionEditor.Editor;

                if (editor == null)
                    return;

                $(thisCollectionEditor.Container).appendTo(editor.parent());
                editor.remove();

                $.each(thisCollectionEditor.Container.children(), function (idx, item) {
                    $(item).unbind("mouseenter.editbox");
                    $(item).unbind("mouseleave.editbox");
                    $(item).unbind("click.editbox");
                    $(item).removeClass("editable");
                    $($(item).find(".edit-box")[0]).remove();
                });

                $(".editable-new").remove();

                thisCollectionEditor.IsEditing = false;
            }

            this.Edit = function () {

                if (!thisCollectionEditor.IsEditing) {
                    thisCollectionEditor.IsEditing = true;

                    var editor = $("<span>").addClass("edit").appendTo($(thisCollectionEditor.Container).parent());
                    var controls = $("<span>").addClass("edit-controls").appendTo(editor);
                    var cntrCancel = $("<a>").text("X").appendTo(controls);
                    cntrCancel.on("click", function () {
                        thisCollectionEditor.Cancel();
                    });

                    $(thisCollectionEditor.Container).appendTo(editor);

                    $.each(thisCollectionEditor.Container.children(), function (idx, item) {

                        thisCollectionEditor.MakeEditable(item);

                    });


                    if (thisCollectionEditor.AddControl != null) {

                        var editControl = thisCollectionEditor.AddControl().addClass("editable-new");

                        if (thisCollectionEditor.AddEvent != null)
                            editControl.on("click", function () { thisCollectionEditor.AddEvent(this, thisCollectionEditor.Container); });

                        thisCollectionEditor.Container.append(editControl);

                    }

                    thisCollectionEditor.Editor = editor;

                }
            }

            this.MakeEditable = function (item) {

                $(item).addClass("editable");

                var container = $("<span>").addClass("edit-box").width($(item).width()).height($(item).height()).appendTo(item);

                $(item).bind("mouseenter.editbox",
                    function () {
                        /*IN*/

                        $($(this).find(".edit-box a img")[0]).show();
                    });

                $(item).bind("mouseleave.editbox",
                    function () {

                        $($(this).find(".edit-box a img")[0]).hide();
                    });

                var deleteBox = $("<a>").width($(item).width()).height($(item).height()).appendTo(container);
                var deleteImage = $("<img>").prop("src", "/content/images/edit-delete-small.png").appendTo(deleteBox);

                $(deleteImage).bind("click.editbox", function () {
                    if (thisCollectionEditor.Delete != null)
                        thisCollectionEditor.Delete(item);

                    return false;
                });
            }

            this.Add = function (control) {
                if (control != null) {

                    if (thisCollectionEditor.IsEditing) {
                        $(thisCollectionEditor.ID + " .editable-new").before(control);
                    } else {
                        $(thisCollectionEditor.Container).append(control);
                    }

                    thisCollectionEditor.MakeEditable(control);
                }

            }

        }
    }
}



