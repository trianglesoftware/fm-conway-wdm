﻿function InfoModal(modalSelector) {

    this.exists = false;

    if (modalSelector) {
        this.modal = $(modalSelector);
        this.exists = true;
    } else {

        this.modal = $('<div class="modal fade" tabindex="-1" role="dialog">')
            .append($('<div class="modal-dialog">')
                .append($('<div class="modal-content">')
                    .append($('<div class="modal-header">')
                        .append('<h3 class="modal-title">')
                        .append($('<i class="close icon-remove icon-themed" data-dismiss="modal">'))
                    )
                    .append($('<div class="modal-body">'))
                    .append($('<div class="modal-footer">'))
                )

            );
    }

    this.dialog = this.modal.find(".modal-dialog");
    this.header = this.modal.find(".modal-header");
    this.body = this.modal.find(".modal-body");
    this.footer = this.modal.find(".modal-footer");
    this.title = this.header.find(".modal-title");

    this.addButton = function (value, dismiss) {

        var btn = $("<input type='button'>").attr("id", value.replace(/[ ]/g, '-')).addClass("btn").val(value).prependTo(this.footer);

        if (typeof dismiss != "boolean" || dismiss) {
            btn.attr("data-dismiss", "modal");
        }

        return btn;
    }

    this.removeButton = function (value) {

        var id = "#" + value.replace(/[ ]/g, '-');
        $(this.footer).find(id).remove();
    }

    this.setTitle = function (text) {
        this.title.text(text);
    }

    this.setMessage = function (message) {
        this.body.empty();
        this.body.append($("<p>").text(message));
    }

    this.show = function (looseFocus) {

        if (looseFocus) {
            this.modal.removeAttr("data-backdrop");
        } else {
            this.modal.attr("data-backdrop", "static");
        }

        this.modal.modal("show");
    }

    this.hide = function () {
        this.modal.modal("hide");
    }

    /*Setup drag*/

    var _mouseDown = false;
    var _current = this;

    var _startX = 0;
    var _startY = 0;
    var _dialogOffset = { top: 0, left: 0 };

    this.modal.find(".modal-header").on("mousedown", function (e) {

        if ($(e.target).parents(".search").length > 0)
            return;

        _mouseDown = true;
        _startX = e.pageX;
        _startY = e.pageY;

    });

    this.modal.on('show.bs.modal', function () {

        _startX = 0;
        _startY = 0;
        _dialogOffset = { top: 0, left: 0 };
        _current.dialog.removeAttr("style");

    });

    $(document).on("mousemove", function (e) {

        if (!_mouseDown)
            return;

        _current.dialog.css("top", (_dialogOffset.top + ((_startY - e.pageY) * -1)) + "px");
        _current.dialog.css("left", (_dialogOffset.left + ((_startX - e.pageX) * -1)) + "px");

    });

    $(document).on("mouseup", function (e) {

        if (_mouseDown)
            _dialogOffset = { top: _dialogOffset.top + ((_startY - e.pageY) * -1), left: _dialogOffset.left + ((_startX - e.pageX) * -1) };

        _mouseDown = false;
    });

    //return this;
}

var triangle = {
    ui: {

        lookupCore: function (id) {

            var thisLookup = this;

            this.currentPage = 0;
            this.totalPages = 0;

            this.id = id;
            this.lookupID = `${this.id}-lookup`;
            this.rowCount = 0;
            this.url = null;
            this.isMultiSelect = false;
            this.selectedRows = [];

            this.value = function () {
                return $(id).val()
            };

            this.modal = new InfoModal();
            this.modal.header.append($('<input type="text" disabled="disabled" class="search" placeholder="Search">'));


            this.modal.addButton("Close", true);

            this.searchTimeout = null;
            this.searchQuery = "";

            this.disabled = $(this.lookupID + " a").hasClass("disabled");

            this.title = $(this.lookupID).data("title");

            this.modal.setTitle(this.title);

            this.SortByName = function (a, b) {
                var aName = a.key.toLowerCase();
                var bName = b.key.toLowerCase();
                return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
            }

            this.GetPage = function (page) {
                thisLookup.currentPage = page;

                var lookupData = null;

                var modalFormBody = this.modal.body; //$(thisLookup.id + "-modal .modal-body");
                modalFormBody.empty();

                var loader = $("<div>").addClass("ajax-loader").appendTo(modalFormBody);
                $("<img>").prop("src", "/Content/images/loader.gif").appendTo(loader);

                var params = { page: page, rowCount: thisLookup.rowCount, query: thisLookup.searchQuery };

                if (thisLookup.UrlParameters != null) {
                    var customerParams = thisLookup.UrlParameters;
                    params = this.MergeObjects(params, customerParams);
                }

                $.get(thisLookup.url, $.param(params, true), function (data) {
                    lookupData = data;

                    if (lookupData != null) {

                        thisLookup.totalPages = lookupData.PageCount;

                        // generate table with columns

                        var table = $("<table>").addClass("table table-striped table-bordered");
                        modalFormBody.append(table);

                        var tableHeader = $("<tr>");
                        table.append(tableHeader);

                        $.each(lookupData.ColumnHeaders, function (i, col) {
                            tableHeader.append($("<th>").html(col));
                        });

                        //Action column
                        tableHeader.append($("<th>"));

                        // add row data                  
                        $.each(lookupData.Rows, function (i, rowData) {

                            var row = $("<tr>");
                            table.append(row);

                            if (rowData.TableData != null) {
                                $.each(rowData.TableData, function (i, data) {
                                    row.append($("<td>").html(data));
                                });
                            }

                            // Add selection value
                            if (thisLookup.isMultiSelect) {

                                var selector = $("<input type='checkbox' />").addClass("multi-select css-checkbox").val(rowData.Key).data("key", rowData.Value).data("rowData", rowData.Data).prop("id", rowData.Value);
                                var lab = $("<label>").addClass("multi-select-css-label").prop("for", rowData.Value);

                                for (var i = 0; i < thisLookup.selectedRows.length; i++) {
                                    if (thisLookup.selectedRows[i].key === rowData.Value) {
                                        selector.attr("checked", "checked");
                                        lab.addClass("checked");
                                        break;
                                    }
                                }

                                row.append($("<td>").append(selector).append(lab));

                            } else {

                                //I think bootstrap looks for the attribute 'data-dismiss' and will not go looking in the object cache for it; thats why I havn't used 'data()' to set the dismiss value
                                var icon = $("<i>").addClass("action-select single-select-css-icon").data("key", rowData.Key).data("value", rowData.Value).data("rowData", rowData.Data).attr("data-dismiss", "modal").attr("title", "Select").appendTo($("<td>").appendTo(row));
                            }

                        });

                        //hookup table events
                        thisLookup.modal.body.find(".multi-select").click(function () {

                            var cont = $(this);

                            if (cont.is(":checked")) {
                                thisLookup.selectedRows.push({ key: cont.data("key"), value: cont.val() });
                                $('label[for=' + cont.data("key") + ']').addClass("checked");
                            } else {
                                for (var i = 0; i < thisLookup.selectedRows.length; i++) {
                                    if (thisLookup.selectedRows[i].value === cont.val()) {
                                        thisLookup.selectedRows.splice(i, 1);
                                        $('label[for=' + cont.data("key") + ']').removeClass("checked");
                                        break;
                                    }
                                }
                            }

                        });

                        thisLookup.modal.body.find(".action-select").on("click", function () {
                            thisLookup.Selected($(this).data("key"), $(this).data("value"), $(this).data("rowData"));
                        });

                        // generate pager
                        if (thisLookup.totalPages > 1) {
                            thisLookup.GeneratePager(modalFormBody);
                        }
                    }
                }).success(function () {
                    thisLookup.modal.header.find(".search").removeAttr("disabled");
                })
                    .fail(function () {

                        var error = $("<div>").addClass("ajax-error").appendTo(modalFormBody);

                        $("<p>").text("An error has occured. Please try again.").appendTo(error);

                    }).always(function () { loader.hide(); });

            }

            this.GetNextPage = function () {
                var nextPage = thisLookup.currentPage + 1;
                if (nextPage < thisLookup.totalPages)
                    thisLookup.GetPage(nextPage);
            }

            this.GetPrevPage = function () {
                var prevPage = thisLookup.currentPage - 1;
                if (prevPage >= 0)
                    thisLookup.GetPage(prevPage);
            }

            this.GetLastPage = function () {
                thisLookup.GetPage(thisLookup.totalPages - 1);
            }

            this.GetFirstPage = function () {
                thisLookup.GetPage(0);
            }

            this.Selected = function (key, value, data) {

                $(thisLookup.id).val(value);
                //thisLookup.value = value;

                var output = $(thisLookup.id + "-lookup .selected");
                output.val(key);

                if (thisLookup.AddRemovalButtons)
                    thisLookup.AddRemovalButtons();

                if (thisLookup.SelectedEvent != null)
                    thisLookup.SelectedEvent(key, value, data);
            }

            this.MultipleSelected = function (keyValues) {

                var list = $(thisLookup.id);
                list.empty();

                var childCount = 0;

                $.each(keyValues, function () {

                    var listItem = $("<li>").addClass("selected").text(this.value);

                    $("<input type='hidden' class='hidden-key'>").prop("name", thisLookup.id.replace('#', '').replace('-', '.') + "[" + childCount + "].Key").val(this.key).appendTo(listItem);
                    $("<input type='hidden' class='hidden-value'>").prop("name", thisLookup.id.replace('#', '').replace('-', '.') + "[" + childCount + "].Value").val(this.value).appendTo(listItem);

                    list.append(listItem);
                    childCount++;
                });

                if (thisLookup.AddRemovalButtons)
                    thisLookup.AddRemovalButtons();

                if (thisLookup.MultipleSelectedEvent != null)
                    thisLookup.MultipleSelectedEvent(keyValues);

            }

            this.GeneratePager = function (modalFormBody) {
                var pager = $("<div class='pagination pagination-centered'>");
                modalFormBody.append(pager);

                var list = $("<ul>");
                pager.append(list);

                var backwards = $("<li>").append($("<a>").html("&laquo;").attr("href", "#").addClass("pager-action").data("page", "--"));
                var backwardOnce = $("<li>").append($("<a>").html("&lsaquo;").attr("href", "#").addClass("pager-action").data("page", "-"));

                if (thisLookup.currentPage == 0) {
                    backwards.addClass("disabled");
                    backwardOnce.addClass("disabled");
                }

                list.append(backwards);
                list.append(backwardOnce);

                var startPage = 1;
                var endPage = 5;

                if (thisLookup.currentPage >= 3) {

                    startPage = thisLookup.currentPage - 1;
                    endPage = thisLookup.currentPage + 3;

                    if (endPage > thisLookup.totalPages) {
                        endPage = thisLookup.totalPages;
                        startPage = endPage - 4;
                    }

                }

                if (endPage > thisLookup.totalPages)
                    endPage = thisLookup.totalPages;

                for (var i = startPage; i <= endPage; i++) {
                    if (i == thisLookup.currentPage + 1) {
                        list.append($("<li>").addClass("active").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i)));
                        continue;
                    }

                    list.append($("<li>").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i - 1)));
                }

                var forwardOnce = $("<li>").append($("<a>").html("&rsaquo;").attr("href", "#").addClass("pager-action").data("page", "+"));
                var forwards = $("<li>").append($("<a>").html("&raquo;").attr("href", "#").addClass("pager-action").data("page", "++"));

                if (thisLookup.currentPage == thisLookup.totalPages - 1) {
                    forwards.addClass("disabled");
                    forwardOnce.addClass("disabled");
                }

                list.append(forwardOnce);
                list.append(forwards);

                //$(thisLookup.id + "-modal .modal-body .pagination .pager-action").on("click", function () {
                this.modal.body.find(".pagination .pager-action").on("click", function () {

                    var page = $(this).data("page");

                    if ($(this).parent().hasClass("active") || $(this).parent().hasClass("disabled"))
                        return;

                    switch (page) {
                        case "--": thisLookup.GetFirstPage(); break;
                        case "++": thisLookup.GetLastPage(); break;
                        case "-": thisLookup.GetPrevPage(); break;
                        case "+": thisLookup.GetNextPage(); break;
                        default: thisLookup.GetPage(page); break;
                    }
                });
            }

            this.Enable = function () {
                thisLookup.disabled = false;
                $(thisLookup.id + "-lookup .lookup-button").removeClass("disabled").removeAttr("disabled").attr("data-backdrop", "false").attr("data-toggle", "modal");

            }

            this.Disable = function () {
                thisLookup.disabled = true;
                $(thisLookup.id + "-lookup .lookup-button").addClass("disabled").attr("disabled", "disabled").removeAttr("data-toggle");

                thisLookup.Clear();
            }

            this.Clear = function () {
                if (thisLookup.isMultiSelect)
                    $(thisLookup.id + '-lookup .mli-remove').click();
                else {
                    $(thisLookup.id + '-lookup .li-remove').click();
                    $(thisLookup.id).val('');
                }
            }

            this.AddRemovalButtons = function () {

                $.each($(thisLookup.id + "-lookup .selected"), function (idx, select) {

                    if (thisLookup.isMultiSelect) {
                        if ($(select).text() != "")
                            $("<a>").addClass("mli-remove").attr("href", "#").appendTo($(select)).append($("<i>").addClass("icon-remove icon-inactive"));
                    } else {
                        if ($(select).val() != "" && $(select).parent().find('.li-remove').length == 0)
                            $("<a>").addClass("li-remove").attr("href", "#").appendTo($(select).parent()).append($("<i>").addClass("icon-remove icon-inactive"));
                    }

                });

                thisLookup.HookupRemovalEvents();
            }

            this.HookupRemovalEvents = function () {

                if (thisLookup.isMultiSelect) {


                    $(this.id + '-lookup .mli-remove').on('click', function () {

                        $(this).parent("li").remove();

                        $.each($(thisLookup.id).children("li"), function (index, child) {

                            $(child).find('.hidden-key').prop("name", thisLookup.id.replace('#', '').replace('-', '.') + "[" + index + "].Key")
                            $(child).find('.hidden-value').prop("name", thisLookup.id.replace('#', '').replace('-', '.') + "[" + index + "].Value")
                        });

                        if (thisLookup.RemovedEvent != null) {

                            var key = $(this).parent("li").find('.hidden-key').prop("name");
                            var value = $(this).parent("li").find('.hidden-value').prop("name")
                            thisLookup.RemovedEvent(key, value);
                        }

                        $(".masonry").masonry();

                        return false;
                    });
                }
                else {
                    $(this.id + '-lookup .li-remove').on('click', function (e) {

                        var key = $(this).parent().find("input").val();

                        $(this).parent().find("input").val('');

                        var value = $(thisLookup.id).val();
                        $(thisLookup.id).val('');
                        //thisLookup.value = null;

                        $(this).unbind('click');
                        $(this).remove();

                        if (thisLookup.RemovedEvent != null)
                            thisLookup.RemovedEvent(key, value);

                        $(".masonry").masonry();

                        return false;
                    });
                }
            }

            this.SelectedEvent;

            this.RemovedEvent;

            this.MultipleSelectedEvent;

            this.UrlParameters;

            this.MergeObjects = function (obj1, obj2) {
                var obj3 = {};
                for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
                for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
                return obj3;
            }

            this.HookupEvents = function () {

                if (this.isMultiSelect)
                    this.modal.addButton("Select", true).addClass("btn-primary action-select");

                $(this.lookupID + " a").on("click", function () {

                    if (thisLookup.disabled)
                        return;

                    //reset props and retrive first page
                    thisLookup.searchQuery = "";
                    thisLookup.searchTimeout = null;
                    thisLookup.selectedRows = [];

                    if (thisLookup.isMultiSelect) {

                        $.each($(thisLookup.id).children("li"), function () {

                            thisLookup.selectedRows.push({ key: $(this).find(".hidden-key").val(), value: $(this).find(".hidden-value").val() });
                        });
                    }

                    thisLookup.GetPage(0);

                    thisLookup.modal.show();
                });

                this.modal.footer.find(".action-select").on("click", function () {

                    thisLookup.MultipleSelected(thisLookup.selectedRows.sort(thisLookup.SortByName));
                });

                this.modal.header.find(".search").keyup(function () {

                    thisLookup.searchQuery = $(this).val();

                    clearTimeout(thisLookup.searchTimeout);
                    thisLookup.searchTimeout = setTimeout(function () { thisLookup.GetPage(0) }, 1000);

                });
            }

        },
        lookup: function (id, url, rowCount, isMultiSelect) {

            var core = new triangle.ui.lookupCore(id);
            core.rowCount = rowCount;
            core.url = url;
            core.isMultiSelect = isMultiSelect;
            core.HookupEvents();
            core.AddRemovalButtons();

            return core;
        },
        editableLookup: function (id, url, rowCount) {

            var core = new triangle.ui.lookupCore(id);
            core.rowCount = rowCount;
            core.url = url;

            core.AddRemovalButtons = null;

            core.HookupEvents();

            return core;
        },
        table: function (containerID, url, rowCount, urlParams, loadedEvent, focusOnSearch, incWrap) {

            this.url = url;
            this.containerID = containerID;
            this.container = $(containerID);

            if (this.container == null || this.container == undefined)
                throw "Cannot find elememnt '" + containerID + "'. Please specify a containerID."

            this.rowCount = rowCount;
            this.currentPage = 0;
            this.totalPages = 0;
            this.urlParams = urlParams;
            this.searchBarRevert = true;
            this.searchQuery = null;
            this.searchBarExpandedSize = 500;
            this.searchBarSize = 200;
            this.LoadedEvent = loadedEvent;
            this.incWrap = incWrap;

            var thisTable = this;

            var tableOptions = {
                columnWidth: 200,
                debug: false,
                singleClick: true
            };

            this.actionContainer = $("<div>").appendTo(this.container);

            this.table = new Table(containerID, tableOptions);

            $(this.table).on("doubleClick", function (e, row, data) {
                //console.log('row', row);
                //console.log('data', data);

                if (row.data.actions != null && row.data.actions.length) {
                    location.href = row.data.actions[0].Url;
                }
            });

            this.GetPage = function (page) {

                var me = this;

                this.currentPage = page;

                me.table.showLoader(true);

                var params = { rowCount: this.rowCount, page: page, query: this.searchQuery };

                if (this.urlParams != null) {
                    params = this.MergeObjects(params, this.urlParams);
                }

                $.get(this.url, $.param(params, true), function (data) {

                    me.totalPages = data.PageCount;

                    me.table.clearRows();

                    var headers = [];

                    $.each(data.ColumnHeaders, function (index, columnHeader) {

                        headers.push({ name: columnHeader, width: 'auto' });

                    });

                    me.table.setHeaders(headers);

                    var actionCount = 0;

                    if (data.Rows.length > 0) {

                        var rows = [];


                        $.each(data.Rows, function (index, rowData) {

                            var row = { data: [], actions: rowData.Actions, hiddenData: [] };

                            if (rowData.TableData != null) {
                                $.each(rowData.TableData, function (index, dta) {
                                    row.data.push(dta);
                                });
                            }

                            if (rowData.Data != null) {
                                $.each(rowData.Data, function (index, dta) {
                                    row.hiddenData.push(dta);
                                });
                            }

                            //row.attr("data-rowData", rowData.Data);

                            rows.push(row)

                            //if (rowData.Actions != null && rowData.Actions.length > 0) {

                            //    //actionCount = rowData.Actions.length;

                            //    $.each(rowData.Actions, function (index, dta) {
                            //        if (index == 0) {
                            //            // purely used to detect rows with hyperlink action in css
                            //            row.addClass('link');

                            //            row.on("click", function () {
                            //                location.href = dta.Url;
                            //            });

                            //            // not a perfect solution but reasonably emulates middle clicking <a> tag functionality (open in new window)
                            //            row.on("mousedown", function (e1) {
                            //                if (e1.which == 2) {
                            //                    row.on("mouseup.middleclick", function () {
                            //                        row.off("mouseup.middleclick");
                            //                        window.open(dta.Url, "_blank");
                            //                    });
                            //                    setTimeout(function () {
                            //                        row.off("mouseup.middleclick");
                            //                    }, 250);
                            //                }

                            //            });

                            //        } else {
                            //            actionCount++;
                            //            var action = $("<a>").addClass(dta.Class).prop("href", dta.Url).prop("title", dta.Title).append($("<i>").addClass(dta.IconClass));
                            //            row.append($("<td>").append(action));
                            //        }
                            //    });
                            //}

                        });


                        me.table.addRows(rows);
                    } else {
                        me.table.showNoRows();
                    }


                    //else {

                    //    var row = $("<tr>").appendTo(tBody);

                    //    $("<td>").css("text-align", "center").attr("colspan", data.ColumnHeaders.length).text("No results found.").appendTo(row);

                    //}

                    //if (actionCount > 0)
                    //    header.append($("<th>").addClass("table-actions").attr("colspan", actionCount));

                    if (me.totalPages > 1)
                        me.GeneratePager(me.container);

                    if (me.LoadedEvent != null)
                        me.LoadedEvent();

                }).fail(function () {

                    var error = $("<div>").addClass("ajax-error").appendTo(container);

                    $("<p>").text("An error has occured. Please try again.").appendTo(error);

                }).always(function () {
                    // loader.hide();
                    //me.loader.hide();
                    me.table.showLoader(false);
                });


            }

            this.GeneratePager = function (container) {

                container.find(".pagination").remove();

                var pager = $("<div class='pagination pagination-centered'>").appendTo(container);

                var list = $("<ul>");
                pager.append(list);

                var backwardOnce = $("<li>").append($("<a>").html("&lsaquo;").attr("href", "#").addClass("pager-action").data("page", "-"));

                var backwards = $("<li>").append($("<a>").html("&laquo;").attr("href", "#").addClass("pager-action").data("page", "--"));

                if (thisTable.currentPage == 0) {
                    backwards.addClass("disabled");
                    backwardOnce.addClass("disabled");
                }

                list.append(backwards);
                list.append(backwardOnce);

                var startPage = 1;
                var endPage = 5;

                if (thisTable.currentPage >= 3) {

                    startPage = thisTable.currentPage - 1;
                    endPage = thisTable.currentPage + 3;

                    if (endPage > thisTable.totalPages) {
                        endPage = thisTable.totalPages;
                        startPage = endPage - 4 <= 0 ? 0 : endPage - 4;
                    }
                }

                if (endPage > thisTable.totalPages)
                    endPage = thisTable.totalPages;

                for (var i = startPage; i <= endPage; i++) {
                    if (i == thisTable.currentPage + 1) {
                        list.append($("<li>").addClass("active").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i - 1)));
                        continue;
                    }

                    list.append($("<li>").append($("<a>").text(i).attr("href", "#").addClass("pager-action").data("page", i - 1)));
                }

                var forwardOnce = $("<li>").append($("<a>").html("&rsaquo;").attr("href", "#").addClass("pager-action").data("page", "+"));
                var forwards = $("<li>").append($("<a>").html("&raquo;").attr("href", "#").addClass("pager-action").data("page", "++"));

                if (thisTable.currentPage == thisTable.totalPages - 1) {
                    forwards.addClass("disabled");
                    forwardOnce.addClass("disabled");
                }

                list.append(forwardOnce);
                list.append(forwards);

                $(thisTable.containerID + " .pagination .pager-action").on("click", function () {

                    var page = $(this).data("page");

                    if ($(this).parent().hasClass("active") || $(this).parent().hasClass("disabled"))
                        return;

                    switch (page) {
                        case "--": thisTable.GetFirstPage(); break;
                        case "++": thisTable.GetLastPage(); break;
                        case "-": thisTable.GetPrevPage(); break;
                        case "+": thisTable.GetNextPage(); break;
                        default: thisTable.GetPage(page); break;
                    }
                });
            }

            this.GetPrevPage = function () {
                var prevPage = thisTable.currentPage - 1;
                if (prevPage >= 0)
                    thisTable.GetPage(prevPage);
            }

            this.GetNextPage = function () {
                var nextPage = thisTable.currentPage + 1;
                if (nextPage < thisTable.totalPages)
                    thisTable.GetPage(nextPage);
            }

            this.GetLastPage = function () {
                thisTable.GetPage(thisTable.totalPages - 1);
            }

            this.GetFirstPage = function () {
                thisTable.GetPage(0);
            }

            this.MergeObjects = function (obj1, obj2) {
                var obj3 = {};
                for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
                for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
                return obj3;
            }

            this.SearchBar = function (container) {

                var me = this;

                var searchContainer = $("<div>").addClass("search-container").appendTo(container);
                var searchQuery = $("<div>").addClass("search-query").append().appendTo(searchContainer);
                var searchIcon = $("<i>").addClass("icon-search").appendTo(searchQuery);
                var searchInput = $("<input type='text'>").addClass("search-query-input").attr("placeholder", "Search").val(this.searchQuery).appendTo(searchQuery);

                //if (this.searchQuery != null && this.searchQuery != "") {
                //    searchQuery.attr("style", "width:" + this.searchBarExpandedSize + "px");
                //    this.searchBarRevert = false;
                //}

                if (focusOnSearch)
                    $(searchInput).focus();

                searchIcon.on("click", function () {

                    this.GetPage(0);
                });

                //searchInput.on("focus.table", function () {

                //    $(this).parent().animate({ width: me.searchBarExpandedSize }, 400);

                //});

                searchInput.on("keydown.table", function (e) {

                    //e.preventDefault();

                    var input = $(this).val();

                    var canRevert = input == "";

                    me.searchBarRevert = canRevert;

                    me.searchQuery = input;

                    if (e.which == 13/*Enter*/) {
                        me.GetPage(0);
                    }

                });

                //searchInput.on("blur.table", function () {

                //    if (me.searchBarRevert) {

                //        $(this).parent().animate({ width: me.searchBarSize }, 400);
                //    }
                //});


            }

            this.ExcelExport = function (container) {

                var me = this;

                var params = {
                    rowCount: 9999999,
                    page: 0,
                    query: this.searchQuery || '',
                    excelexport: 1
                };

                if (this.urlParams != null) {
                    params = this.MergeObjects(params, this.urlParams);
                }

                var paramsString = $.param(params, true);

                var html =
                    '<div class="excel-export-container">' +
                    '<a href="' + this.url + '?' + paramsString + '">' +
                    '<img src="/Images/export-excel.png">' +
                    '</a>' +
                    '</div>';
                html = $(html);

                html.appendTo(container);
            }

            this.GetPage(0);

            this.SearchBar(this.actionContainer);
            this.ExcelExport(this.actionContainer);
        },
        collectionEditor: function (collectionId) {

            var thisCollectionEditor = this;
            this.IsEditing = false;
            this.Container = null;

            var obj = $(collectionId);
            if (obj == null)
                return;

            this.ID = collectionId;
            this.Container = obj;

            this.AddControl = null;
            this.Delete = null;
            this.AddEvent = null;
            this.Editor = null;

            this.Cancel = function () {

                var editor = thisCollectionEditor.Editor;

                if (editor == null)
                    return;

                $(thisCollectionEditor.Container).appendTo(editor.parent());
                editor.remove();

                $.each(thisCollectionEditor.Container.children(), function (idx, item) {
                    //$(item).unbind("mouseenter.editbox");
                    //$(item).unbind("mouseleave.editbox");
                    $(item).unbind("click.editbox");
                    $(item).removeClass("editable");
                    $($(item).find(".edit-box")[0]).remove();
                });

                $(".editable-new").remove();

                thisCollectionEditor.IsEditing = false;

                $(".masonry").masonry();
            }

            this.Edit = function () {

                if (!thisCollectionEditor.IsEditing) {
                    thisCollectionEditor.IsEditing = true;

                    var editor = $("<span>").addClass("edit").appendTo($(thisCollectionEditor.Container).parent());
                    var controls = $("<span>").addClass("edit-controls").appendTo(editor);
                    var cntrCancel = $("<a>").text("X").appendTo(controls);
                    cntrCancel.on("click", function () {
                        thisCollectionEditor.Cancel();
                    });

                    $(thisCollectionEditor.Container).appendTo(editor);

                    $.each(thisCollectionEditor.Container.children(), function (idx, item) {
                        safe = 0;
                        thisCollectionEditor.MakeEditable(item, safe);

                    });


                    //if (thisCollectionEditor.AddControl != null) {

                    var addImage = $("<i>", { class: "fa fa-plus" });//.prop("src", "/content/images/edit-add.png").width(48).height(48);
                    var link = $("<a>").append(addImage);

                    var editControl = $("<li>").addClass("editable-new").append(link);

                    //if (thisCollectionEditor.AddEvent != null)
                    editControl.on("click", function () {
                        $(thisCollectionEditor).triggerHandler("add", thisCollectionEditor.Container);
                    });


                    editControl.prependTo(thisCollectionEditor.Container);

                    //}

                    thisCollectionEditor.Editor = editor;

                    $(".masonry").masonry();
                }
            }

            this.SafeEdit = function () {

                if (!thisCollectionEditor.IsEditing) {
                    thisCollectionEditor.IsEditing = true;

                    var editor = $("<span>").addClass("edit").appendTo($(thisCollectionEditor.Container).parent());
                    var controls = $("<span>").addClass("edit-controls").appendTo(editor);
                    var cntrCancel = $("<a>").text("X").appendTo(controls);
                    cntrCancel.on("click", function () {
                        thisCollectionEditor.Cancel();
                    });

                    $(thisCollectionEditor.Container).appendTo(editor);

                    $.each(thisCollectionEditor.Container.children(), function (idx, item) {
                        safe = 1;
                        thisCollectionEditor.MakeEditable(item, safe);
                    });


                    if (thisCollectionEditor.AddControl != null) {

                        var editControl = thisCollectionEditor.AddControl().addClass("editable-new");

                        if (thisCollectionEditor.AddEvent != null)
                            editControl.on("click", function () { thisCollectionEditor.AddEvent(this, thisCollectionEditor.Container); });

                        editControl.prependTo(thisCollectionEditor.Container);

                    }

                    thisCollectionEditor.Editor = editor;

                }
            }

            this.MakeEditable = function (item, safe) {

                $(item).addClass("editable");

                var container = $("<span>").addClass("edit-box").appendTo(item);

                //if (safe != 1) {
                //    $(item).bind("mouseenter.editbox",
                //        function () {
                //            /*IN*/

                //            $($(this).find(".edit-box a img")[0]).show();
                //        });

                //    $(item).bind("mouseleave.editbox",
                //        function () {

                //            $($(this).find(".edit-box a img")[0]).hide();
                //        });
                //}

                var deleteBox = $("<a>").appendTo(container);
                var deleteImage = $("<i>", { class: "fa fa-times" }).appendTo(deleteBox);//.prop("src", "/content/images/edit-delete-small.png");

                $(deleteImage).bind("click.editbox", function () {
                    if (thisCollectionEditor.Delete != null)
                        thisCollectionEditor.Delete(item);

                    return false;
                });
            }

            this.Add = function (control) {

                if (control != null) {

                    if (thisCollectionEditor.IsEditing) {
                        $(thisCollectionEditor.ID + " .editable-new").after(control);
                    } else {
                        $(thisCollectionEditor.Container).append(control);
                    }

                    thisCollectionEditor.MakeEditable(control);
                }

            }

        },
        scroller: function (menu) {

            this.menu = menu;

            this.container = $(this.menu).parent();
            this.containerTop = $(this.container).position().top;
            this.containerHeight = 0;
            this.containerWidth = 0;

            this.menuTop = $(this.menu).position().top;
            this.menuWidth = 0;
            this.menuHeight = 0;
            this.menuBottom = 0;
            this.menuLeak = 0;

            this.isLoading = false;

            this.windowHeight = 0;

            this.scrollBarHeight = 0;

            this.scrollBarContainer = $("<div class='nav-scroll'>");
            this.rail = $("<div class='scroll-rail'>");
            this.railMargin = 20;
            this.railMin = 0;
            this.railMax = 0;
            this.movement = 0;

            this.scrollBar = $("<div class='scroll-bar'>").append($("<div class='bar'>"));

            this.scrollOrigin = 0;
            this.scrollBarVisible = false;

            this.wheel = false;

            this.toggelEventualy = null;

            var currentScroller = this;

            this.init = function () {

                currentScroller.scrollOrigin = 0;
                currentScroller.menuTop = $(this.menu).position().top;
                currentScroller.menuWidth = $(currentScroller.menu).width();
                currentScroller.menuHeight = $(currentScroller.menu).height();
                currentScroller.containerWidth = $(currentScroller.container).width();
                currentScroller.containerHeight = $(currentScroller.container).height();
                currentScroller.windowHeight = $(window).innerHeight();
                currentScroller.menuBottom = currentScroller.menuTop + currentScroller.menuHeight;
                currentScroller.rail.height(currentScroller.containerHeight - 50);
                currentScroller.menuLeak = currentScroller.menuHeight - currentScroller.containerHeight;
                currentScroller.railMin = currentScroller.containerTop + currentScroller.railMargin;
                currentScroller.railMax = currentScroller.railMin + $(currentScroller.rail).innerHeight();

            }

            this.makeUnselectable = function (node) {
                if (node.nodeType == 1) {
                    node.setAttribute("unselectable", "on");
                }
                var child = node.firstChild;
                while (child) {
                    makeUnselectable(child);
                    child = child.nextSibling;
                }
            }

            $(window).load(function () {
                currentScroller.Toggle();
            });

            $(window).resize(function () {

                if (currentScroller.scrollBarVisible) {
                    $(currentScroller.menu).width(currentScroller.containerWidth - 15);
                }

                currentScroller.Toggle();
            });

            this.Toggle = function () {
                setTimeout(function () {
                    currentScroller.init();

                    if (currentScroller.menuHeight > currentScroller.containerHeight) {
                        currentScroller.Show();
                    } else {
                        currentScroller.Hide();
                    }
                }, 10);
            }

            this.Show = function () {
                currentScroller.isLoading = true;

                if (!currentScroller.scrollBarVisible) {
                    $(currentScroller.menu).animate({ width: currentScroller.containerWidth - 15 }, 200);
                    currentScroller.scrollBarVisible = true;
                    currentScroller.showBar($(currentScroller.menu));
                } else {
                    if (currentScroller.menuTop <= currentScroller.containerHeight - currentScroller.menuHeight)
                        $(currentScroller.menu).animate({ top: currentScroller.containerHeight - currentScroller.menuHeight }, 200);

                    currentScroller.calculateScrollBarMovement();

                }

                var scrolling = 0;

                $(currentScroller.menu).off("mousewheel.scrollbar");
                $(currentScroller.menu).on("mousewheel.scrollbar", function (e, da, dx, dy) {

                    if (!currentScroller.isLoading) {
                        if (!currentScroller.wheel) {

                            currentScroller.scrollOrigin = 0;
                            currentScroller.wheel = true;

                        }

                        scrolling += 20 * da * -1;
                        currentScroller.process(scrolling);
                    }
                    return false;
                });

                currentScroller.isLoading = false;
            }

            this.Hide = function () {

                if (!currentScroller.scrollBarVisible)
                    return;

                currentScroller.scrollBarVisible = false;
                $(currentScroller.menu).animate({ width: currentScroller.containerWidth }, 200);
                $(currentScroller.menu).off("mousewheel.scrollbar");
                currentScroller.scrollBarContainer.remove();

                $(menu).animate({ top: 0 }, 300);

            }

            this.calculateScrollBarMovement = function () {
                var sbOff = ($(currentScroller.rail).height() / currentScroller.menuHeight) * currentScroller.menuLeak;

                currentScroller.scrollBarHeight = $(currentScroller.rail).height() - sbOff;

                if (currentScroller.scrollBar.position().top + currentScroller.scrollBarHeight > currentScroller.rail.height())
                    currentScroller.scrollBar.animate({ top: currentScroller.rail.height() - currentScroller.scrollBarHeight + currentScroller.railMargin });

                currentScroller.scrollBar.height(currentScroller.scrollBarHeight);

                currentScroller.movement = currentScroller.railMax - (currentScroller.railMin + currentScroller.scrollBarHeight);
            }

            this.showBar = function () {
                currentScroller.scrollBarContainer.remove();

                currentScroller.scrollBarContainer.append(currentScroller.rail);

                //var sbOff = ($(currentScroller.rail).height() / currentScroller.menuHeight) * currentScroller.menuLeak;

                //currentScroller.scrollBarHeight = $(currentScroller.rail).height() - sbOff;
                currentScroller.scrollBar.offset({ top: 20 });

                //currentScroller.scrollBar.height(currentScroller.scrollBarHeight);

                //currentScroller.movement = currentScroller.railMax - (currentScroller.railMin + currentScroller.scrollBarHeight);

                currentScroller.calculateScrollBarMovement();

                $(currentScroller.menu).animate({ top: 0 }, 200);

                currentScroller.scrollBarContainer.append(currentScroller.scrollBar.mousedown(function () {
                    //For browser IE4+
                    //document.onselectstart = new Function("return false");

                    //For browser NS6
                    if (window.sidebar) {
                        document.onmousedown = function (e) {
                            return false;
                        };
                    }

                    currentScroller.scroll();
                }));

                currentScroller.scrollBarContainer.insertAfter($(currentScroller.menu));

            }

            this.scroll = function () {

                $("body").on("mousemove.scrollbar", function (e) {

                    if (currentScroller.wheel) {
                        currentScroller.scrollOrigin = 0;
                        currentScroller.wheel = false;
                    }

                    currentScroller.process(e.clientY);

                }).on("mouseup.scrollbar", function () { document.onmousedown = null; $("body").off("mousemove.scrollbar").off("mouseup.scrollbar"); });

            }

            this.process = function (position) {

                var containerTopPadding = parseInt($(currentScroller.container).css("padding-top"));
                var containerTopMargin = parseInt($(currentScroller.container).css("margin-top"));

                var menuTopPadding = parseInt($(currentScroller.menu).css("padding-top"));
                var menuTopMargin = parseInt($(currentScroller.menu).css("margin-top"));

                var scrollBarTop = currentScroller.containerTop + $(currentScroller.scrollBar).position().top;

                if (currentScroller.scrollOrigin == 0)
                    currentScroller.scrollOrigin = position;

                var offsetTop = scrollBarTop + position - currentScroller.scrollOrigin;
                var offsetBottom = scrollBarTop + currentScroller.scrollBarHeight + position - currentScroller.scrollOrigin;

                if (currentScroller.railMin >= offsetTop)
                    offset = currentScroller.railMin;
                else {
                    offset = offsetBottom >= currentScroller.railMax ? currentScroller.railMax - currentScroller.scrollBarHeight : offsetTop;
                }

                $(currentScroller.scrollBar).offset({ top: offset + $(window).scrollTop() });

                var progress = 100 / currentScroller.movement * ($(currentScroller.scrollBar).position().top - currentScroller.railMargin);

                var menuOffset = currentScroller.containerTop + containerTopMargin + containerTopPadding + menuTopMargin + menuTopPadding + $(window).scrollTop() + ((currentScroller.menuLeak / 100 * progress) * -1);

                $(menu).offset({ top: menuOffset });

                currentScroller.scrollOrigin = position;

            }
        },
        menu: {
            init: function () {

                $("nav > ul > li > .menu-item").click(function (e) {

                    var subNav = $(this).parent().find("ul");

                    if (subNav.length) {

                        if (subNav.is(":visible")) {
                            subNav.slideUp(300, function () { toggleMenuScroller(); });
                            openNav = null;
                        } else {
                            //if (openNav != null)
                            //    openNav.slideUp(300);

                            subNav.slideDown(500, function () { toggleMenuScroller(); });
                            openNav = subNav;
                        }
                    }

                });

                $("nav > ul > li > ul > li").click(function (e) {
                    document.cookie = "menu=" + $(this).parent().parent().attr("id") + ";path=/";
                });

                $("nav > ul > li ul").hide();

                var menuId = /(^| )menu=([^;]+)/g.exec(document.cookie);

                if (menuId !== null) {

                    $(`nav #${menuId[2]} ul`).show();
                }

            }
        }
    }
}


$(function () {

    $("form").on("submit", function (e) {
        var base = $(this);
        var ignoreHiddenFields = base.hasClass('validation-ignore-hidden');

        var requiredInputs = $(this).find('.control-group[data-required="true"] input, .control-group[data-required="true"] select').filter(function () {
            var isHidden = !$(this).is(':visible');
            return !(ignoreHiddenFields && isHidden) && !this.value
        });

        console.log("required inp", requiredInputs);

        if (requiredInputs.length > 0) {
            if ($(".required-fields").length == 0) {
                $(this).prepend('<div class="alert alert-warning required-fields">Please complete all required fields.</div>');
            }

            e.preventDefault();
            e.stopImmediatePropagation();
        }
    });

});

