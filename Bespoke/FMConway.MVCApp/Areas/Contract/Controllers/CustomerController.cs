﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Customers;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Customers.Interfaces;
using FMConway.Services.Customers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Customers", 10, "fa fa-address-book", "Administrator, Supervisor, Manager, Customers")]
    public class CustomerController : BaseController
    {
        ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        #region Customer table actions
        Func<CustomerModel, TableAction> updateAction = customer =>
        {
            return new TableAction()
            {
                Url = "/Contract/Customer/Update/" + customer.CustomerID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<CustomerModel, TableAction> viewAction = customer =>
        {
            return new TableAction()
            {
                Url = "/Contract/Customer/Summary/" + customer.CustomerID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        public ActionResult ActiveCustomers(int rowCount, int page, string query)
        {
            PagedResults<CustomerModel> results = _customerService.GetActiveCustomersPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = CustomerOverviewTable(results, new List<Func<CustomerModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        public ActionResult InactiveCustomers(int rowCount, int page, string query)
        {
            PagedResults<CustomerModel> results = _customerService.GetInactiveCustomersPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = CustomerOverviewTable(results, new List<Func<CustomerModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable CustomerOverviewTable(PagedResults<CustomerModel> results, List<Func<CustomerModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Customer Name", "Account Number", "Customer Code", "Address", "Phone Number", "Email Address", "Username" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.CustomerName,
                        s.AccountNumber,
                        s.CustomerCode,
                        s.AddressLine1 + " " + s.AddressLine2 + " " + s.AddressLine3 + " " + s.City + " " + s.County + " " + s.Postcode,
                        s.PhoneNumber,
                        s.EmailAddress,
                        s.UserName
                    },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            CustomerViewModel model = new CustomerViewModel() { };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        [HttpPost]
        public ActionResult New(CustomerViewModel model)
        {

            //if (string.IsNullOrEmpty(model.Customer.CustomerName))
            //{
            //    ModelState.AddModelError("Customer.CustomerName", "Customer Name is required");
            //}

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Guid customerID = _customerService.Create(model.Customer);

            return RedirectToAction("Update", new { id = customerID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        public ActionResult Update(Guid id)
        {
            CustomerViewModel model = new CustomerViewModel()
            {
                Customer = _customerService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        [HttpPost]
        public ActionResult Update(CustomerViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _customerService.Update(model.Customer);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            CustomerViewModel model = new CustomerViewModel()
            {
                Customer = _customerService.Single(id)
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        [HttpPost]
        public ActionResult Summary()
        {
            return RedirectToAction("Active");
        }

        public ActionResult GetCustomerLookup(int rowCount, int page, string query)
        {
            PagedResults<CustomerLookup> lookup = _customerService.GetPagedActiveCustomersLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Customer" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerUserLookup(int rowCount, int page, string query)
        {
            var lookup = _customerService.GetPagedActiveCustomerUsersLookup(rowCount, page, query);

            var model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Customer" },
                Rows = lookup.Data.Select(s => new LookupRow
                {
                    Key = s.Key,
                    Value = s.Value,
                    TableData = new string[]
                    {
                        s.Key
                    }
                }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
