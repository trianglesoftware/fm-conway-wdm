﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Observations;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Interfaces;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.Maintenance;
using FMConway.Services.Observations.Interfaces;
using FMConway.Services.Observations.Models;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.WorkInstructions.Interfaces;
using FMConway.Services.WorkInstructionDetails.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    //[NavigationMenu("Observations", 141, "fa fa-eye", "Administrator, Supervisor, Manager")]
    public class ObservationsController : BaseController
    {
        IObservationsService _observationsService;
        IWorkInstructionService _workInstructionService;
        IMaintenanceService _maintenanceService;
        ILoadingSheetService _loadingSheetService;
        IWorkInstructionDetailsService _workInstructionDetailsService;
        IErrorLoggingService _errorLoggingService;

        public ObservationsController(IWorkInstructionService workInstructionService, IMaintenanceService maintenanceService, ILoadingSheetService loadingSheetService, IWorkInstructionDetailsService workInstructionDetailsService, IErrorLoggingService errorLoggingService, IObservationsService observationsService)
        {
            _observationsService = observationsService;
            _workInstructionService = workInstructionService;
            _maintenanceService = maintenanceService;
            _loadingSheetService = loadingSheetService;
            _workInstructionDetailsService = workInstructionDetailsService;
            _errorLoggingService = errorLoggingService;
        }

        #region Lists

        [Authorize(Roles = "Administrator, Supervisor, Manager")]
        [NavigationMenuItem(10, "fa fa-folder-open")]
        public ActionResult WithShifts()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager")]
        [NavigationMenuItem(20, "fa fa-folder-minus")]
        public ActionResult WithoutShifts()
        {
            return View();
        }

        #endregion

        #region Tables

        [Authorize(Roles = "Administrator, Supervisor, Manager")]
        public ActionResult ObservationsWithShifts(int rowCount, int page, string query)
        {
            PagedResults<ObservationModel> results = _observationsService.GetObservationsWithShifts(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = ObservationOverviewTable(results, new List<Func<ObservationModel, TableAction>>() { viewObservationAction });

            foreach (var item in model.Rows)
            {
                foreach (var action in item.Actions)
                {
                    action.Url += "?fromID=null&from=menu";
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager")]
        public ActionResult ObservationsWithoutShifts(int rowCount, int page, string query)
        {
            PagedResults<ObservationModel> results = _observationsService.GetObservationsWithoutShifts(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = ObservationOverviewTable(results, new List<Func<ObservationModel, TableAction>>() { viewObservationAction });

            foreach (var item in model.Rows)
            {
                foreach (var action in item.Actions)
                {
                    action.Url += "?fromID=null&from=menu";
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetObservations(int rowCount, int page, string query, Guid shiftID, Guid workInstructionID)
        {
            try
            {
                PagedResults<ObservationModel> results = _observationsService.GetObservationsPaged(rowCount, page, HttpUtility.HtmlEncode(query), shiftID);

                PagedTable model = ObservationOverviewTable(results, new List<Func<ObservationModel, TableAction>>() { viewObservationAction });

                foreach (var item in model.Rows)
                {
                    foreach (var action in item.Actions)
                    {
                        action.Url += "?fromID=" + workInstructionID.ToString() + "&from=wi";
                    }
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetObservations", e, shiftID.ToString());
                return View();
            }
        }

        public ActionResult GetObservationsForJob(int rowCount, int page, string query, Guid jobID)
        {
            try
            {
                PagedResults<ObservationModel> results = _observationsService.GetObservationsForJobPaged(rowCount, page, HttpUtility.HtmlEncode(query), jobID);

                PagedTable model = ObservationOverviewTable(results, new List<Func<ObservationModel, TableAction>>() { viewObservationAction });

                foreach (var item in model.Rows)
                {
                    foreach (var action in item.Actions)
                    {
                        action.Url += "?fromID=" + jobID.ToString() + "&from=job";
                    }
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetObservationsForJob", e, jobID.ToString());
                return View();
            }
        }

        public ActionResult GetObservationPhotos(int rowCount, int page, string query, Guid observationID)
        {
            try
            {
                PagedResults<ObservationPhotoModel> results = _observationsService.GetObservationPhotosPaged(rowCount, page, query, observationID);

                PagedTable model = ObservationPhotoOverviewTable(results, new List<Func<ObservationPhotoModel, TableAction>>() { viewObservationPhoto });

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetObservationPhotos", e, observationID.ToString());
                return View();
            }
        }
        
        #endregion

        #region Table actions

        Func<ObservationModel, TableAction> viewObservationAction = observation =>
        {
            return new TableAction()
            {
                Url = "/Contract/Observations/ViewObservation/" + observation.ObservationID,
                Title = "View",
                IconClass = "icon-pencil"
            };
        };

        Func<ObservationPhotoModel, TableAction> viewObservationPhoto = ap =>
        {
            return new TableAction()
            {
                Url = "/File/CreateDocument/" + ap.FileID,
                Title = "View",
                IconClass = "icon-pencil"
            };
        };

        #endregion

        #region Table overviews

        private PagedTable ObservationOverviewTable(PagedResults<ObservationModel> results, List<Func<ObservationModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Details", "Location", "Date" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.ObservationDescription, s.Location, s.ObservationDate.ToString() },
                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable ObservationPhotoOverviewTable(PagedResults<ObservationPhotoModel> results, List<Func<ObservationPhotoModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Date", "Latitude", "Longitude" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.FileName, s.Latitude.ToString(), s.Longitude.ToString() },
                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        #endregion

        #region Views

        public ActionResult ViewObservation(Guid id, Guid? fromID, string from)
        {
            var observation = _observationsService.SingleObservation(id);

            ObservationViewModel ovm = new ObservationViewModel()
            {               
                ObservationID = id,
                ObservationDate = observation.ObservationDate,
                ObservationDescription = observation.ObservationDescription,
                Location = observation.Location,
                PeopleInvolved = observation.PeopleInvolved,
                CauseOfProblem = observation.CauseOfProblem
            };

            if (from == "wi")
            {
                ovm.WorkInstructionID = fromID.Value;
            }
            else if (from == "job")
            {
                ovm.JobID = fromID.Value;
            }

            return View(ovm);
        }

        #endregion

    }
}
