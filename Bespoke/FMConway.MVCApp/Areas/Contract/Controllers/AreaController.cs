﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Areas;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Areas.Interfaces;
using FMConway.Services.Areas.Models;
using FMConway.Services.Collections.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    //[NavigationMenu("Area Protocols", 100, "fa fa-circle-exclamation", "Administrator, Supervisor, Manager, Areas")]
    public class AreaController : BaseController
    {
        IAreaService _areaService;

        public AreaController(IAreaService areaService)
        {
            _areaService = areaService;
        }

        #region Area table actions
        Func<AreaModel, TableAction> updateAction = area =>
        {
            return new TableAction()
            {
                Url = "/Contract/Area/Update/" + area.AreaID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<AreaModel, TableAction> updateWorkInstructionAction = area =>
        {
            return new TableAction()
            {
                Url = "/Contract/Area/UpdateWorkInstructionArea/" + area.WorkInstructionAreaPK,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<AreaModel, TableAction> viewAction = area =>
        {
            return new TableAction()
            {
                Url = "/Contract/Area/Summary/" + area.AreaID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        //[NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        //[NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        public ActionResult ActiveAreas(int rowCount, int page, string query)
        {
            PagedResults<AreaModel> results = _areaService.GetActiveAreasPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = AreaOverviewTable(results, new List<Func<AreaModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //[Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        //public ActionResult ActiveWorkInstructionAreas(int rowCount, int page, string query, Guid workInstructionID)
        //{
        //    PagedResults<AreaModel> results = _areaService.GetActiveWorkInstructionAreasPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

        //    PagedTable model = AreaOverviewTable(results, new List<Func<AreaModel, TableAction>>() { updateWorkInstructionAction });

        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        public ActionResult InactiveAreas(int rowCount, int page, string query)
        {
            PagedResults<AreaModel> results = _areaService.GetInactiveAreasPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = AreaOverviewTable(results, new List<Func<AreaModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable AreaOverviewTable(PagedResults<AreaModel> results, List<Func<AreaModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Area Protocol Name", "Call Protocol", "RCC Number", "NCC Number" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name, s.CallProtocol, s.RCCNumber, s.NCCNumber },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        //[NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            AreaViewModel model = new AreaViewModel() { };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        [HttpPost]
        public ActionResult New(AreaViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Guid areaID = _areaService.Create(model.Area);

            return RedirectToAction("Active");
        }

        //[Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        //public ActionResult NewWorkInstructionArea(Guid workInstructionID)
        //{
        //    AreaViewModel model = new AreaViewModel() { Area = new AreaModel() { WorkInstructionID = workInstructionID } };
        //    return View(model);
        //}

        //[Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        //[HttpPost]
        //public ActionResult NewWorkInstructionArea(AreaViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    Guid areaID = _areaService.CreateWorkInstructionArea(model.Area);

        //    return RedirectToAction("Update", "WorkInstruction", new { id = model.Area.WorkInstructionID });
        //}

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        public ActionResult Update(Guid id)
        {
            AreaViewModel model = new AreaViewModel()
            {
                Area = _areaService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        [HttpPost]
        public ActionResult Update(AreaViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _areaService.Update(model.Area);
            }
            return RedirectToAction("Active");
        }

        //[Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        //public ActionResult UpdateWorkInstructionArea(Guid id)
        //{
        //    AreaViewModel model = new AreaViewModel()
        //    {
        //        Area = _areaService.SingleWorkInstructionArea(id),
        //    };

        //    return View(model);
        //}

        //[Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        //[HttpPost]
        //public ActionResult UpdateWorkInstructionArea(AreaViewModel model, string Cancel)
        //{
        //    if (Cancel == null)
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return View(model);
        //        }
        //        _areaService.UpdateWorkInstructionArea(model.Area);
        //    }
        //    return RedirectToAction("Update", "WorkInstruction", new { id = model.Area.WorkInstructionID });
        //}

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            AreaViewModel model = new AreaViewModel()
            {
                Area = _areaService.Single(id)
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Areas")]
        [HttpPost]
        public ActionResult Summary(AreaViewModel model)
        {
            _areaService.Update(model.Area);
            return RedirectToAction("Inactive");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Operation, Areas")]
        public ActionResult GetAreaLookup(int rowCount, int page, string query)
        {
            PagedResults<AreaLookup> lookup = _areaService.GetPagedActiveAreasLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Area Protocol" , "Call Protocol" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key, s.CallProtocol } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAreaDetails(Guid areaID)
        {
            var area = _areaService.Single(areaID);

            return Json(new { call = area.CallProtocol, rcc = area.RCCNumber, ncc = area.NCCNumber });
        }
    }
}
