﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Equipment;
using FMConway.MVCApp.Areas.Contract.ViewModels.LoadingSheets;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Equipments.Interfaces;
using FMConway.Services.Equipments.Models;
using FMConway.Services.LoadingSheets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Equipment", 100, "fa fa-screwdriver-wrench", "Administrator, Supervisor, Manager, Equipment")]
    public class EquipmentController : BaseController
    {
        IEquipmentService _equipmentService;

        public EquipmentController(IEquipmentService equipmentService)
        {
            _equipmentService = equipmentService;
        }

        #region Equipment table actions
        Func<EquipmentModel, TableAction> updateAction = equipment =>
        {
            return new TableAction()
            {
                Url = "/Contract/Equipment/Update/" + equipment.EquipmentID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<EquipmentModel, TableAction> viewAction = equipment =>
        {
            return new TableAction()
            {
                Url = "/Contract/Equipment/Summary/" + equipment.EquipmentID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };

        Func<LoadingSheetEquipmentModel, TableAction> deleteAction = equipment =>
        {
            return new TableAction()
            {
                Url = "/Contract/LoadingSheet/RemoveEquipment/" + equipment.LoadingSheetEquipmentID,
                Title = "Delete",
                IconClass = "icon-remove"
            };
        };

        Func<LoadingSheetEquipmentModel, TableAction> viewTemplateAction = equipment =>
        {
            return new TableAction()
            {
                Url = "/Contract/Equipment/Summary/" + equipment.EquipmentID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };

        Func<LoadingSheetEquipmentModel, TableAction> deleteTemplateAction = equipment =>
        {
            return new TableAction()
            {
                Url = "/Contract/LoadingSheet/RemoveTemplateEquipment/" + equipment.LoadingSheetEquipmentID,
                Title = "Delete",
                IconClass = "icon-remove"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        public ActionResult ActiveEquipment(int rowCount, int page, string query)
        {
            PagedResults<EquipmentModel> results = _equipmentService.GetActiveEquipmentPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = EquipmentOverviewTable(results, new List<Func<EquipmentModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Operation, Equipment")]
        public ActionResult ActiveLoadingSheetEquipment(int rowCount, int page, string query, Guid loadingSheetID)
        {
            PagedResults<LoadingSheetEquipmentModel> results = _equipmentService.GetActiveLoadingSheetEquipmentPaged(rowCount, page, HttpUtility.HtmlEncode(query), loadingSheetID);

            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Equipment Name", "Equipment Type", "Quantity" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name, s.EquipmentType.Name, s.Quantity.ToString() },
                    Actions = s.EquipmentID.HasValue ? new List<TableAction> {
                        new TableAction("/Contract/Equipment/Summary/?id=" + s.EquipmentID.Value + "&loadingSheetID=" + loadingSheetID + "&sheetOrder=" + s.SheetOrder, "View", "icon-eye-open"),
                        new TableAction("/Contract/LoadingSheet/RemoveEquipment/" + s.LoadingSheetEquipmentID, "Delete", "icon-remove")
                    } :
                    new List<TableAction> { null, new TableAction("/Contract/LoadingSheet/RemoveEquipment/" + s.LoadingSheetEquipmentID, "Delete", "icon-remove") }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Operation, Equipment")]
        public ActionResult ActiveLoadingSheetTemplateEquipment(int rowCount, int page, string query, Guid loadingSheetTemplateID)
        {
            PagedResults<LoadingSheetEquipmentModel> results = _equipmentService.GetActiveLoadingSheetTemplateEquipmentPaged(rowCount, page, HttpUtility.HtmlEncode(query), loadingSheetTemplateID);

            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Equipment Name", "Equipment Type", "Quantity" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name, s.EquipmentType.Name, s.Quantity.ToString() },
                    Actions = s.EquipmentID.HasValue ? new List<TableAction> {
                        new TableAction("/Contract/Equipment/Summary/" + s.EquipmentID.Value, "View", "icon-eye-open"),
                        new TableAction("/Contract/LoadingSheet/RemoveTemplateEquipment/" + s.LoadingSheetEquipmentID, "Delete", "icon-remove")
                    } :
                    new List<TableAction> { null, new TableAction("/Contract/LoadingSheet/RemoveTemplateEquipment/" + s.LoadingSheetEquipmentID, "Delete", "icon-remove") }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        public ActionResult ActiveEquipmentLS(int rowCount, int page, string query, Guid loadingSheetID)
        {
            PagedResults<EquipmentModel> results = _equipmentService.GetActiveEquipmentPaged(rowCount, page, HttpUtility.HtmlEncode(query));
            PagedTable model = EquipmentLSOverviewTable(results);

            // With the equipment images, the below is required to utilise the max possible json length.
            // If the list still fails to load (because of higher res images), then consider compressing images and/or loading these images 
            // individually after the table has loaded.
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            var result = new ContentResult()
            {
                Content = serializer.Serialize(model),
                ContentType = "application/json"
            };
            return result;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        public ActionResult ActiveEquipmentLSTemplate(int rowCount, int page, string query, Guid loadingSheetTemplateID)
        {
            PagedResults<EquipmentModel> results = _equipmentService.GetActiveEquipmentPaged(rowCount, page, HttpUtility.HtmlEncode(query));
            PagedTable model = EquipmentLSOverviewTable(results);

            // With the equipment images, the below is required to utilise the max possible json length.
            // If the list still fails to load (because of higher res images), then consider compressing images and/or loading these images 
            // individually after the table has loaded.
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            var result = new ContentResult()
            {
                Content = serializer.Serialize(model),
                ContentType = "application/json"
            };
            return result;
        }
        
        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        public ActionResult InactiveEquipment(int rowCount, int page, string query)
        {
            PagedResults<EquipmentModel> results = _equipmentService.GetInactiveEquipmentPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = EquipmentOverviewTable(results, new List<Func<EquipmentModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable EquipmentOverviewTable(PagedResults<EquipmentModel> results, List<Func<EquipmentModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Equipment Name", "Equipment Type" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name, s.EquipmentType.Name },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable LoadingSheetEquipmentOverviewTable(PagedResults<LoadingSheetEquipmentModel> results, List<Func<LoadingSheetEquipmentModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Equipment Name", "Equipment Type", "Quantity" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name, s.EquipmentType.Name, s.Quantity.ToString() },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable EquipmentLSOverviewTable(PagedResults<EquipmentModel> results)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] {
                    "Image",
                    "Equipment Name",
                    "Equipment Type",
                    ""
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.File.Data == null ? "" : "<img alt='' src='data:image/jpeg;base64," + Convert.ToBase64String(s.File.Data) + "' width='100' height='100'/>",
                        s.Name,
                        s.EquipmentType.Name,
                        "<input id=" + s.EquipmentID + " class='multi-select css-checkbox' type='checkbox' value=" + s.EquipmentID + "><label class='multi-select-css-label' for=" + s.EquipmentID + "></label>"
                    }
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            EquipmentViewModel model = new EquipmentViewModel() { };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        [HttpPost]
        public ActionResult New(EquipmentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Guid equipmentID = _equipmentService.Create(model.Equipment, model.Image);

            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        public ActionResult Update(Guid id)
        {
            EquipmentViewModel model = new EquipmentViewModel()
            {
                Equipment = _equipmentService.Single(id),
                ImageData = Convert.ToBase64String(_equipmentService.GetImageData(id))
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        [HttpPost]
        public ActionResult Update(EquipmentViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _equipmentService.Update(model.Equipment, model.Image);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        public ActionResult Summary(Guid id, Guid? loadingSheetID, int? sheetOrder)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            var imageData = Convert.ToBase64String(_equipmentService.GetImageData(id));
            decimal quantity = 0;

            if (loadingSheetID != null && loadingSheetID != Guid.Empty) { quantity = _equipmentService.GetLoadingSheetQuantity(id, loadingSheetID.Value, sheetOrder.GetValueOrDefault()); }

            EquipmentViewModel model = new EquipmentViewModel()
            {
                Equipment = _equipmentService.Single(id),
                ImageData = imageData == "" ? "No Image Selected" : imageData,
                SheetOrder = sheetOrder.GetValueOrDefault(),
                Quantity = quantity
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        [HttpPost]
        public ActionResult Summary(EquipmentViewModel model)
        {
            _equipmentService.Update(model.Equipment, null);
            _equipmentService.UpdateQuantity(model.Equipment, model.LoadingSheetID, model.SheetOrder, model.Quantity);

            if (!model.Equipment.IsActive)
            {
                return RedirectToAction("Inactive");
            }
            return RedirectToAction("Update", "LoadingSheet", new { id = model.LoadingSheetID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        public ActionResult AddLoadingSheetEquipment(Guid loadingSheetID)
        {
            var model = new EquipmentViewModel()
            {
                LoadingSheetID = loadingSheetID
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Equipment")]
        public ActionResult AddLoadingSheetTemplateEquipment(Guid loadingSheetTemplateID)
        {
            var model = new EquipmentViewModel()
            {
                LoadingSheetID = loadingSheetTemplateID
            };

            return View(model);
        }

        public ActionResult GetEquipmentLookup(int rowCount, int page, string query)
        {
            PagedResults<EquipmentLookup> lookup = _equipmentService.GetPagedActiveEquipmentLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Equipment", "Type" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key, s.EquipmentType } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadingSheetEquipmentLastOrderNumber(Guid id)
        {
            var result = _equipmentService.GetLoadingSheetEquipmentLastOrderNumber(id);
            return Json(new { OrderNumber = result });
        }
    }
}
