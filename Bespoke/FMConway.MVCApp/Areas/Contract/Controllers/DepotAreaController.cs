﻿using FMConway.MVCApp.Areas.Contract.ViewModels.DepotAreas;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.DepotAreas.Interfaces;
using FMConway.Services.DepotAreas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Depot Areas", 111, "fa fa-street-view", "Administrator, Supervisor, Manager, Depots")]
    public class DepotAreaController : BaseController
    {
        IDepotAreaService _depotAreaService;

        public DepotAreaController(IDepotAreaService depotAreaService)
        {
            _depotAreaService = depotAreaService;
        }

        #region Depot area table actions
        Func<DepotAreaModel, TableAction> updateAction = depotArea =>
        {
            return new TableAction()
            {
                Url = "/Contract/DepotArea/Update/" + depotArea.DepotAreaID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<DepotAreaModel, TableAction> viewAction = depotArea =>
        {
            return new TableAction()
            {
                Url = "/Contract/DepotArea/Summary/" + depotArea.DepotAreaID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        public ActionResult ActiveDepotAreas(int rowCount, int page, string query)
        {
            PagedResults<DepotAreaModel> results = _depotAreaService.GetActiveDepotAreasPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = DepotAreaOverviewTable(results, new List<Func<DepotAreaModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        public ActionResult InactiveDepotAreas(int rowCount, int page, string query)
        {
            PagedResults<DepotAreaModel> results = _depotAreaService.GetInactiveDepotAreasPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = DepotAreaOverviewTable(results, new List<Func<DepotAreaModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable DepotAreaOverviewTable(PagedResults<DepotAreaModel> results, List<Func<DepotAreaModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Depot Area" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            DepotAreaViewModel model = new DepotAreaViewModel() { };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [HttpPost]
        public ActionResult New(DepotAreaViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Guid depotAreaID = _depotAreaService.Create(model.DepotArea);

            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        public ActionResult Update(Guid id)
        {
            DepotAreaViewModel model = new DepotAreaViewModel()
            {
                DepotArea = _depotAreaService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [HttpPost]
        public ActionResult Update(DepotAreaViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (string.IsNullOrEmpty(model.DepotArea.Name))
                {
                    ModelState.AddModelError("DepotArea.Name", "Name is required");
                }

                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _depotAreaService.Update(model.DepotArea);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            DepotAreaViewModel model = new DepotAreaViewModel()
            {
                DepotArea = _depotAreaService.Single(id)
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [HttpPost]
        public ActionResult Summary()
        {
            return RedirectToAction("Inactive");
        }

        public ActionResult GetDepotAreaLookup(int rowCount, int page, string query)
        {
            PagedResults<DepotAreaLookup> lookup = _depotAreaService.GetPagedActiveDepotAreasLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Depot Area" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
