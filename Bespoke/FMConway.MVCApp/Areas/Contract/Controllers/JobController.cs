﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Jobs;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Extensions;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.ReasonCodes.Interfaces;
using FMConway.Services.ReasonCodes.Models;
using FMConway.Services.Jobs.Interfaces;
using FMConway.Services.Jobs.Models;
using FMConway.Services.Schemes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Jobs", 30, "fa fa-clipboard-list", "Administrator, Supervisor, Manager, Jobs")]
    public class JobController : BaseController
    {
        IJobService _jobService;
        ISchemeService _schemeService;
        IErrorLoggingService _errorLoggingService;
        IReasonCodeService _reasonCodeService;

        public JobController(IJobService jobService, ISchemeService schemeService, IErrorLoggingService errorLoggingService, IReasonCodeService reasonCodeService)
        {
            _jobService = jobService;
            _schemeService = schemeService;
            _errorLoggingService = errorLoggingService;
            _reasonCodeService = reasonCodeService;
        }

        #region Job table actions
        Func<JobModel, TableAction> updateAction = job =>
        {
            return new TableAction()
            {
                Url = "/Contract/Job/Update/" + job.JobID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<JobModel, TableAction> viewAction = job =>
        {
            return new TableAction()
            {
                Url = "/Contract/Job/Summary/" + job.JobID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        [NavigationMenuItem(10, "fa fa-bolt")]
        public ActionResult LiveHires()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        [NavigationMenuItem(10, "fa fa-check-to-slot")]
        public ActionResult Completed()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        [NavigationMenuItem("Cancelled / Aborted", 20, "fa fa-xmark")]
        public ActionResult Cancelled()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        public ActionResult LiveHireJobs(int rowCount, int page, string query)
        {
            PagedResults<JobModel> results = _jobService.GetLiveHireJobsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = JobOverviewTable(results, new List<Func<JobModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        public ActionResult CompletedJobs(int rowCount, int page, string query)
        {
            PagedResults<JobModel> results = _jobService.GetCompleteJobsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = JobOverviewTable(results, new List<Func<JobModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        public ActionResult ActiveJobsForScheme(int rowCount, int page, string query, Guid schemeID)
        {
            PagedResults<JobModel> results = _jobService.GetActiveJobsForSchemePaged(rowCount, page, HttpUtility.HtmlEncode(query), schemeID);

            PagedTable model = JobOverviewForSchemaTable(results, new List<Func<JobModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        public ActionResult InactiveJobs(int rowCount, int page, string query)
        {
            PagedResults<JobModel> results = _jobService.GetInactiveJobsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = JobOverviewTable(results, new List<Func<JobModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable JobOverviewTable(PagedResults<JobModel> results, List<Func<JobModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Job Number",
                    "Contract Number",
                    "Sub Department", //Depot
                    "Customer Project Code",
                    "Customer",
                    "Contract Manager",
                    "Contract / Scheme",
                    "Title",
                    "Start",
                    "Finish",
                    "Description",
                    "Phone",
                    "Address"
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.JobNumber.ToString(),
                        s.ContractNumber,
                        s.DepotLookup,
                        s.CustomerProjectCode,
                        s.Customer,
                        s.ContractsManager,
                        s.SchemeID.HasValue ? s.Scheme.SchemeTitle : s.ContractTitle,
                        s.JobTitle,
                        s.StartDate.ToShortDateString(),
                        s.EndDate?.ToShortDateString(),
                        s.Description,
                        s.CustomerPhone,
                        JobModelToInlineAddress(s)
                    },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable JobOverviewForSchemaTable(PagedResults<JobModel> results, List<Func<JobModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Job Number",
                    "Contract Number",
                    "Customer",
                    "Contract Manager",
                    "Title",
                    "Start",
                    "Finish",
                    "Phone"
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.JobNumber.ToString(),
                        s.ContractNumber,
                        s.Customer,
                        s.ContractsManager,
                        s.JobTitle,
                        s.StartDate.ToShortDateString(),
                        s.EndDate?.ToShortDateString(),
                        s.CustomerPhone
                    },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private string JobModelToInlineAddress(JobModel model)
        {
            return String.Join(" ", new[]
            {
                model.AddressLine1,
                model.AddressLine2,
                model.AddressLine3,
                model.City,
                model.County,
                model.Postcode
            }.Where(a => !String.IsNullOrWhiteSpace(a)));
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New(Guid? schemeID)
        {
            JobViewModel model = new JobViewModel();

            model.Job = new JobModel()
            {
                StartDate = DateTime.Today,
                EndDate = DateTime.Today
            };

            if (schemeID != null)
            {
                var schemeAddress = _schemeService.Single((Guid)schemeID);
                model.Job.AddressLine1 = schemeAddress.AddressLine1;
                model.Job.AddressLine2 = schemeAddress.AddressLine2;
                model.Job.AddressLine3 = schemeAddress.AddressLine3;
                model.Job.City = schemeAddress.City;
                model.Job.County = schemeAddress.County;
                model.Job.Postcode = schemeAddress.Postcode;
            }

            model.IsSuccessfulStates = SelectListExtensions.NullableYesNoCheckBox();

            if (schemeID.HasValue)
            {
                model.Job.SchemeID = schemeID.Value;
                model.Job.Scheme = _schemeService.Single(schemeID.Value);
            }

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        [HttpPost]
        public ActionResult New(JobViewModel model)
        {
            List<JobDocument> documents = TempData["SubmittedDocuments"] as List<JobDocument>;

            if (documents == null)
                documents = new List<JobDocument>();

            if (model.UploadedDocuments != null)
            {
                //Remove any documents that have been deleted
                foreach (var document in documents.Reverse<JobDocument>())
                {
                    if (!model.UploadedDocuments.Any(ud => ud.Title.Equals(document.Title)))
                    {
                        documents.Remove(document);
                    }
                }

                //Delete the documents where the user has uploaded new files / or remove files.
                foreach (var document in documents.Reverse<JobDocument>())
                {
                    if (model.UploadedDocuments.Where(ud => !ud.Loaded).Any(ud => ud.Title.Equals(document.Title)))
                        documents.Remove(document);
                }

                //Lets add the new documents
                documents.AddRange(model.UploadedDocuments.Where(ud => !ud.Loaded));

                var supportedMineTypes = new List<string>
                    {
                        MimeMapping.GetMimeMapping(".pdf"),
                        MimeMapping.GetMimeMapping(".jpg"),
                        MimeMapping.GetMimeMapping(".png")
                    };

                foreach (var doc in model.UploadedDocuments)
                {
                    if (doc.Document != null)
                    {
                        if (!supportedMineTypes.Contains(doc.Document.ContentType))
                        {
                            ModelState.AddModelError("Invalid file format.", "Drawing file format invalid. Only pdf, jpg and png supported.");
                        }
                    }
                }
            }
            documents.ForEach(doc => doc.Loaded = true);

            if (model.Job.IsSuccessful.HasValue && !model.Job.IsSuccessful.Value && model.Job.ReasonCodeID == null)
            {
                ModelState.AddModelError($"{nameof(model.Job)}.{nameof(model.Job.ReasonCodeID)}", "The Reason Code field is required.");
            }

            if (!ModelState.IsValid)
            {
                TempData["SubmittedDocuments"] = documents;
                model.UploadedDocuments = documents;

                List<SelectListItem> jobStatus = new List<SelectListItem>();
                jobStatus.Add(new SelectListItem() { Text = "" });

                model.IsSuccessfulStates = SelectListExtensions.NullableYesNoCheckBox();

                return View(model);
            }

            Guid jobID = _jobService.Create(model.Job, model.JobTypes, model.Areas, model.MethodStatements, model.ToolboxTalks, model.JobCertificates);

            if (documents != null)
            {
                foreach (var document in documents)
                {
                    byte[] documentData = new byte[document.Document.ContentLength];
                    document.Document.InputStream.Read(documentData, 0, documentData.Length);
                    _jobService.Documents.Create(jobID, document.Title, document.Revision ?? "", document.Document.FileName, document.Document.ContentType, documentData);
                }
            }

            return RedirectToAction("Update", "Job", new { id = jobID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        public ActionResult Update(Guid id)
        {
            var jobTypes = _jobService.JobTypes.Get(id).OrderBy(s => s.JobType.Name).ToDictionary(k => k.JobTypeID, v => v.JobType.Name);
            var jobAreas = _jobService.JobAreas.Get(id).OrderBy(s => s.Area.Area1).ToDictionary(k => k.AreaFK, v => v.Area.Area1);
            var methodStatements = _jobService.MethodStatements.GetForJob(id).OrderBy(s => s.MethodStatement.MethodStatementTitle).ToDictionary(k => k.MethodStatementFK, v => v.MethodStatement.MethodStatementTitle);
            var toolboxTalks = _jobService.ToolboxTalks.GetForJob(id).OrderBy(s => s.ToolboxTalk.ToolboxTalk1).ToDictionary(k => k.ToolboxTalkFK, v => v.ToolboxTalk.ToolboxTalk1);
            var jobCertificates = _jobService.AreaCertificates.GetForJob(id).OrderBy(a => a.CertificateType).ToDictionary(a => a.CertificateTypeID, a => a.CertificateType);
            var documents = _jobService.Documents.GetActiveDocumentsForJob(id).Select(s => new JobDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Title = s.DocumentTitle, Revision = s.Revision, Loaded = true }).ToList();

            var currentJob = _jobService.Single(id);
            JobViewModel model = new JobViewModel()
            {
                Job = currentJob,
                IsSuccessfulStates = SelectListExtensions.NullableYesNoCheckBox(),
                JobTypes = jobTypes,
                Areas = jobAreas,
                MethodStatements = methodStatements,
                ToolboxTalks = toolboxTalks,
                JobCertificates = jobCertificates,
                Documents = documents
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Jobs")]
        [HttpPost]
        public ActionResult Update(JobViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                var currentDocuments = _jobService.Documents.GetActiveDocumentsForJob(model.Job.JobID).Select(s => new JobDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Title = s.DocumentTitle, Revision = s.Revision, Loaded = true }).ToList();
                var documents = currentDocuments.ToList();

                #region documents
                if (model.Documents != null)
                {
                    //Remove any documents that have been deleted
                    foreach (var document in documents.Reverse<JobDocument>())
                    {
                        if (!model.Documents.Any(ud => ud.Title.Equals(document.Title)))
                        {
                            documents.Remove(document);
                        }
                    }
                    //Delete the documents where the user has uploaded new files / or remove files.
                    foreach (var document in documents.Reverse<JobDocument>())
                    {
                        if (model.Documents.Where(ud => !ud.Loaded).Any(ud => ud.Title.Equals(document.Title)))
                            documents.Remove(document);
                    }
                    //Lets add the new documents
                    documents.AddRange(model.Documents.Where(ud => !ud.Loaded));

                    var supportedMineTypes = new List<string>
                    {
                        MimeMapping.GetMimeMapping(".pdf"),
                        MimeMapping.GetMimeMapping(".jpg"),
                        MimeMapping.GetMimeMapping(".png"),
                        MimeMapping.GetMimeMapping(".doc"),
                        MimeMapping.GetMimeMapping(".docx"),
                    };

                    foreach (var doc in model.Documents)
                    {
                        if (doc.Document != null)
                        {
                            if (!supportedMineTypes.Contains(doc.Document.ContentType))
                            {
                                ModelState.AddModelError("Invalid file format.", "Drawing file format invalid. Only pdf, jpg, png, doc and docx are supported.");
                            }
                        }
                    }
                }
                else
                {
                    documents.Clear();
                }

                documents.ForEach(doc => doc.Loaded = true);
                #endregion

                if (model.Job.IsSuccessful.HasValue && !model.Job.IsSuccessful.Value && model.Job.ReasonCodeID == null)
                {
                    ModelState.AddModelError($"{nameof(model.Job)}.{nameof(model.Job.ReasonCodeID)}", "The Reason Code field is required.");
                }

                if (!ModelState.IsValid)
                {
                    int index = 0;
                    documents.ForEach(d => { ModelState.Remove(string.Format("Documents[{0}].Loaded", index)); index++; });

                    model.Documents = documents;

                    model.IsSuccessfulStates = SelectListExtensions.NullableYesNoCheckBox();

                    return View(model);
                }

                var existingJobTypes = _jobService.JobTypes.Get(model.Job.JobID).OrderBy(s => s.JobType.Name).ToDictionary(k => k.JobTypeID, v => v.JobType.Name);
                var existingJobAreas = _jobService.JobAreas.Get(model.Job.JobID).OrderBy(s => s.Area.Area1).ToDictionary(k => k.AreaFK, v => v.Area.Area1);
                var existingMethodStatements = _jobService.MethodStatements.GetForJob(model.Job.JobID).OrderBy(s => s.MethodStatement.MethodStatementTitle).ToDictionary(k => k.MethodStatementFK, v => v.MethodStatement.MethodStatementTitle);
                var existingToolboxTalks = _jobService.ToolboxTalks.GetForJob(model.Job.JobID).OrderBy(s => s.ToolboxTalk.ToolboxTalk1).ToDictionary(k => k.ToolboxTalkFK, v => v.ToolboxTalk.ToolboxTalk1);
                var existingJobCertificates = _jobService.AreaCertificates.GetForJob(model.Job.JobID).OrderBy(a => a.CertificateType).ToDictionary(a => a.JobCertificateID, a => a.CertificateType);

                _jobService.Update(model.Job, model.JobTypes, existingJobTypes, model.Areas, existingJobAreas, model.MethodStatements, existingMethodStatements, model.ToolboxTalks, existingToolboxTalks, model.JobCertificates, existingJobCertificates);

                #region Documents
                if (documents.Any())
                {
                    //Add new Documents
                    foreach (var document in documents.Where(d => !d.DocumentID.HasValue))
                    {
                        byte[] documentData = new byte[document.Document.ContentLength];
                        document.Document.InputStream.Read(documentData, 0, documentData.Length);
                        _jobService.Documents.Create(model.Job.JobID, document.Title, document.Revision ?? "", document.Document.FileName, document.Document.ContentType, documentData);
                    }

                    //Delete removed docs
                    var docsToDelete = currentDocuments.Where(cd => !documents.Where(d => d.DocumentID.HasValue).Any(d => d.DocumentID.Value.Equals(cd.DocumentID.Value))).ToList();
                    docsToDelete.ForEach(d => _jobService.Documents.Inactivate(d.DocumentID.Value));
                }
                else if (currentDocuments != null & currentDocuments.Any())
                {
                    currentDocuments.ForEach(d => _jobService.Documents.Inactivate(d.DocumentID.Value));
                }
                #endregion
            }
            return RedirectToAction("Update", new { id = model.Job.JobID });
        }

        public ActionResult GetDrawingsForJobLookup(int rowCount, int page, string query, Guid jobID)
        {
            PagedResults<DrawingLookup> lookup = _jobService.GetPagedActiveDrawingsForJobLookup(rowCount, page, query, jobID);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Drawing", "Revision" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key, s.Revision } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddDrawingFromJob(Guid workInstructionID, string drawingIDs)
        {
            var drawings = JsonConvert.DeserializeObject<List<Guid>>(drawingIDs);

            var res = _jobService.AddDrawingromJob(workInstructionID, drawings);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJobsLookup(int rowCount, int page, string query, Guid? customerID)
        {
            PagedResults<JobLookup> lookup = _jobService.GetPagedActiveJobsLookup(rowCount, page, query, customerID);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Job Number", "Title" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Number.ToString(), Value = s.Value, TableData = new string[] { s.Number.ToString(), s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPriceLinesForJob(int rowCount, int page, string query, Guid jobID)
        {
            try
            {
                var results = _jobService.GetPagedActiveJobPriceLines(rowCount, page, HttpUtility.HtmlEncode(query), jobID);
                var totalRate = _jobService.GetActiveJobPriceLinesTotalRate(jobID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[]
                    {
                        "Task",
                        "Code",
                        "Description",
                        "Quantity",
                        "Rate",
                        "Total",
                        "Notes?"
                    },
                    Rows = results.Data.Select(a => new TableRow
                    {
                        TableData = new string[]
                        {
                            a.WorksInstructionNumber?.ToString(),
                            a.Code,
                            a.Description,
                            a.Quantity.GetValueOrDefault().ToString("F"),
                            a.Rate?.ToString("0.00##"),
                            a.Total.GetValueOrDefault().ToString("F"),
                            !String.IsNullOrWhiteSpace(a.Note) ? "Yes" : ""
                        },
                        Actions = new List<TableAction>
                        {
                            new TableAction("/Contract/Job/UpdatePriceLine/" + a.JobPriceLineID, "View", "icon-eye-open")
                        }
                    }).ToList()
                };

                if (model.Rows.Any())
                {
                    model.Rows.Add(new TableRow
                    {
                        TableData = new string[]
                        {
                        "",
                        "",
                        "",
                        "",
                        "Total",
                        totalRate.ToString("0.00##"),
                        ""
                        }
                    });
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetObservationsForJob", e, jobID.ToString());
                return View();
            }
        }

        public ActionResult NewPriceLine(Guid jobID)
        {
            var model = new JobPriceLineViewModel();
            model.JobPriceLine.JobID = jobID;

            return View("NewPriceLine", model);
        }

        [HttpPost]
        public ActionResult NewPriceLine(JobPriceLineViewModel model)
        {
            if (ModelState.IsValid)
            {
                _jobService.CreateJobPriceLine(model.JobPriceLine);
                return RedirectToAction("Update", "Job", new { id = model.JobPriceLine.JobID });
            }

            return View("NewPriceLine", model);
        }

        public ActionResult UpdatePriceLine(Guid id)
        {
            var model = new JobPriceLineViewModel();
            model.JobPriceLine = _jobService.SingleJobPriceLine(id);

            return View("UpdatePriceLine", model);
        }

        [HttpPost]
        public ActionResult UpdatePriceLine(JobPriceLineViewModel model)
        {
            if (ModelState.IsValid)
            {
                _jobService.UpdateJobPriceLine(model.JobPriceLine);
                return RedirectToAction("Update", "Job", new { id = model.JobPriceLine.JobID });
            }

            return View("UpdatePriceLine", model);
        }
        
        public ActionResult GetReasonCodeLookup(int rowCount, int page, string query)
        {
            PagedResults<ReasonCodeLookup> lookup = _reasonCodeService.GetPagedActiveReasonCodesLookup(rowCount, page, query);
            LookupModel model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[]
                {
                    "Code",
                    "Description"
                },
                Rows = lookup.Data.Select(s => new LookupRow
                {
                    Key = s.Key,
                    Value = s.Value,
                    TableData = new string[]
                    {
                        s.Key,
                        s.Description
                    }
                }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPriceLines(Guid[] priceLineIDs, Guid jobID)
        {
            var res = _jobService.AddPriceLines(jobID, priceLineIDs);

            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}
