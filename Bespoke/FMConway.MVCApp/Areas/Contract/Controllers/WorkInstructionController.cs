﻿using FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions;
using FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructionDetails;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Interfaces;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.Maintenance;
using FMConway.Services.WorkInstructions.Interfaces;
using FMConway.Services.WorkInstructions.Models;
using FMConway.Services.WorkInstructionDetails.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.UserProfiles;
using Triangle.Membership.Services.Sql;
using FMConway.Services.Employees.Models;
using FMConway.Services.Employees.Interfaces;
using FMConway.Services.WorkInstructions.Extensions;
using FMConway.MVCApp.Extensions;
using FMConway.Services.Pdf.Services;
using FMConway.Services.Helpers;
using FMConway.Services.WorkInstructions.Enums;
using FMConway.MVCApp.Areas.Contract.ViewModels.Jobs;
using FMConway.Services.MethodStatements.Interfaces;
using FMConway.Services.MethodStatements.Services;
using FMConway.MVCApp.Areas.Contract.ViewModels.MethodStatements;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Tasks", 40, "fa fa-list-check", "Administrator, Supervisor, Manager, Tasks, Customer")]
    public class WorkInstructionController : BaseController
    {
        IWorkInstructionService _workInstructionService;
        IMaintenanceService _maintenanceService;
        ILoadingSheetService _loadingSheetService;
        IErrorLoggingService _errorLoggingService;
        IUserProfileService _userProfileService;
        IEmployeeService _employeeService;
        IJobService _jobService;
        PdfService _pdfService;
        IMethodStatementService _methodStatementService;

        public WorkInstructionController(IWorkInstructionService workInstructionService, IMaintenanceService maintenanceService, ILoadingSheetService loadingSheetService, IErrorLoggingService errorLoggingService, IUserProfileService userProfileService, IEmployeeService employeeService, IJobService jobService, PdfService pdfService, IMethodStatementService methodStatementService)
        {
            _workInstructionService = workInstructionService;
            _maintenanceService = maintenanceService;
            _loadingSheetService = loadingSheetService;
            _errorLoggingService = errorLoggingService;
            _userProfileService = userProfileService;
            _employeeService = employeeService;
            _jobService = jobService;
            _pdfService = pdfService;
            _methodStatementService = methodStatementService;
        }

        #region Work Instruction table actions
        Func<WorkInstructionModel, TableAction> updateAction = workInstruction =>
        {
            return new TableAction()
            {
                Url = "/Contract/WorkInstruction/Update/" + workInstruction.WorkInstructionID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };
        Func<WorkInstructionModel, TableAction> scheduleAction = workInstruction =>
        {
            return new TableAction()
            {
                Url = "/Operation/Schedule/New/" + workInstruction.WorkInstructionID,
                Title = "Add To Schedule",
                IconClass = "icon-plus"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult RepeatedWorkInstructions(Guid parentWIFK)
        {
            var wi = _workInstructionService.Single(parentWIFK);
            WorkInstructionViewModel model = new WorkInstructionViewModel()
            {
                WorkInstruction = wi
            };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult ActiveWorkInstructionsForJob(int rowCount, int page, string query, Guid jobID)
        {
            PagedResults<WorkInstructionModel> results = _workInstructionService.GetActiveWorkInstructionsPaged(rowCount, page, HttpUtility.HtmlEncode(query), jobID);

            PagedTable model = WorkInstructionOverviewTableForJob(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult ActiveWorkInstructionsForJobRaw(Guid jobID)
        {
            List<WorkInstructionSimpleModel> results = _workInstructionService.GetActiveWorkInstructions(jobID);
            foreach (var res in results)
            {
                res.DateString = res.Date.ToString();
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult UpdateWorkInstructionsToInvoied(string[] tasks)
        {
            var res = _workInstructionService.UpdateWorkInstructionsToInvoiced(tasks);

            return Json(res, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult JobHasCompleteWorkInstruction(Guid jobID)
        {
            bool result = _workInstructionService.JobHasCompleteWorkInstruction(jobID);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult ActiveWorkInstructions(int rowCount, int page, string query, Guid jobID)
        {
            PagedResults<WorkInstructionModel> results = _workInstructionService.GetActiveWorkInstructionsPaged(rowCount, page, HttpUtility.HtmlEncode(query), jobID);

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        public ActionResult ActiveWorkInstructionsAll(int rowCount, int page, string query)
        {
            var results = _workInstructionService.GetActiveWorkInstructionsAllPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        public ActionResult ActiveWorkInstructionsNew(int rowCount, int page, string query)
        {
            var results = _workInstructionService.GetActiveWorkInstructionsNewPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        public ActionResult ActiveWorkInstructionsScheduled(int rowCount, int page, string query)
        {
            var results = _workInstructionService.GetActiveWorkInstructionsScheduledPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        public ActionResult ActiveWorkInstructionsReady(int rowCount, int page, string query)
        {
            var results = _workInstructionService.GetActiveWorkInstructionsReadyPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            model.RemoveColumn(ColumnHeaders.Engineers);
            model.RemoveColumn(ColumnHeaders.Vehicles);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        public ActionResult ActiveWorkInstructionsComplete(int rowCount, int page, string query)
        {
            var results = _workInstructionService.GetActiveWorkInstructionsCompletePaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        public ActionResult ActiveWorkInstructionsInvoiced(int rowCount, int page, string query)
        {
            var results = _workInstructionService.GetActiveWorkInstructionsInvoicedPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        public ActionResult InactiveWorkInstructionsMain(int rowCount, int page, string query)
        {
            PagedResults<WorkInstructionModel> results = _workInstructionService.GetInactiveWorkInstructionsMainPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Operation, WorkInstructions, Administrator, Supervisor, Manager")]
        public ActionResult ActiveWorkInstructionsToSchedule(int rowCount, int page, string query)
        {
            PagedResults<WorkInstructionModel> results = _workInstructionService.GetActiveWorkInstructionsToSchedulePaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { scheduleAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult RepeatWIList(int rowCount, int page, string query, Guid workInstructionID)
        {
            PagedResults<WorkInstructionModel> results = null;

            results = _workInstructionService.GetRepeatWorkInstructionsAllPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

            PagedTable model = WorkInstructionOverviewTable(results, new List<Func<WorkInstructionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Operation, WorkInstructions, Customer")]
        public ActionResult ShiftProgressList(int rowCount, int page, string query, Guid shiftID, Guid workInstructionID)
        {
            PagedResults<ShiftProgressInfoModel> results = _workInstructionService.GetShiftProgressInfoPaged(rowCount, page, HttpUtility.HtmlEncode(query), shiftID);

            PagedTable model = ShiftProgressOverviewTable(results, workInstructionID);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable WorkInstructionOverviewTable(PagedResults<WorkInstructionModel> results, List<Func<WorkInstructionModel, TableAction>> actions, bool shortAddress = false)
        {
            PagedTable model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Task",
                    "Start Date",
                    ColumnHeaders.Engineers,
                    ColumnHeaders.Vehicles,
                    "Address",
                    "Job",
                    "Customer",
                    "Contract Manager",
                    "Contract Number",
                    "Job Title",
                    "Issued By",
                    "Scheduled",
                    "Status",
                    "Successful",
                    "Job Type"
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.WorkInstructionNumber.ToString(),
                        s.StartDate.ToShortDateString(),
                        s.EmployeeNames,
                        s.VehicleRegistrations,
                        shortAddress ? s.AddressLine1 : s.AddressInLine(),
                        s.JobNumber.ToString(),
                        s.Client,
                        s.ContractManager,
                        s.ContractNumber,
                        s.JobTitle,
                        s.Employee.EmployeeName,
                        s.IsScheduled ? "Yes" : "No",
                        s.Status,
                        s.IsSuccessful != null ? (s.IsSuccessful.Value ? "Yes" : "No") : "",
                        s.JobType
                    },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable WorkInstructionOverviewTableForJob(PagedResults<WorkInstructionModel> results, List<Func<WorkInstructionModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Task",
                    "Date",
                    "Task Type",
                    "Status"
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.WorkInstructionNumber.ToString(),
                        s.StartDate.ToString(),
                        s.JobType,
                        s.Status
                    },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable ShiftProgressOverviewTable(PagedResults<ShiftProgressInfoModel> results, Guid workInstructionID)
        {
            var isCustomer = User.IsInRole("Customer");

            foreach (var item in results.Data)
            {
                item.Progress = Regex.Replace(item.Progress, "(?!^)([A-Z])", " $1");
            }

            var model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Status",
                    "Date"
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.Progress.ToString(),
                        s.Time.ToString()
                    },
                }).ToList()
            };

            if (!isCustomer)
            {
                foreach (var row in model.Rows)
                {
                    if (row.TableData[0] == "Checks Completed")
                    {
                        row.Actions = new List<TableAction>
                        {
                            new TableAction("/Contract/WorkInstructionDetails/ChecksCompleted/" + workInstructionID, "View", "icon-eye-open")
                        };
                    }
                }
            }

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult New(Guid jobID)
        {
            var job = _workInstructionService.GetCurrentJobOverview(jobID);

            var sqlMembership = new SqlMembershipService();
            var currentUser = sqlMembership.GetUserByUsername(User.Identity.Name);
            var currentUserProfile = _userProfileService.GetUserProfile(currentUser.ID);
            var currentEmployee = _employeeService.Single((Guid)currentUserProfile.EmployeeID);

            var model = new NewWorkInstructionViewModel
            {
                WorkInstruction = new WorkInstructionModel
                {
                    JobID = jobID,
                    DepotID = job.DepotID,
                    DepotLookup = job.DepotLookup,
                    StartDate = DateTime.Today.Date,
                    FinishDate = DateTime.Today.Date,
                    Client = job.Client,
                    ContractNumber = job.ContractNumber,
                    ContractTitle = job.ContractTitle,
                    CustomerProjectCode = job.CustomerProjectCode,
                    SchemeTitle = job.SchemeTitle,
                    JobTitle = job.JobTitle,
                    StatusID = new Guid("BCF18FD2-B9AF-44B9-84C3-121854C844CB"),
                    Status = "New",
                    EmployeeID = currentEmployee.EmployeeID,
                    Employee = new EmployeeModel
                    {
                        EmployeeName = currentEmployee.EmployeeName
                    },
                    AddressLine1 = job.AddressLine1,
                    AddressLine2 = job.AddressLine2,
                    AddressLine3 = job.AddressLine3,
                    City = job.City,
                    County = job.County,
                    Postcode = job.Postcode,
                    OrderNumber = 1
                },

                StartHour = 9,
                FinishHour = 17,

                //FirstConeHour = 9,
                //FirstConeMin = 0,
                //LastConeHour = 17,
                //LastConeMin = 0,

                IsSuccessfulStates = SelectListExtensions.NullableYesNoCheckBox()
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        [HttpPost]
        public ActionResult New(NewWorkInstructionViewModel model)
        {
            if (model.WorkInstruction.StatusID == _workInstructionService.GetWorkInstructionStatusID(WorkInstructionStatuses.Complete) && model.WorkInstruction.IsSuccessful == null)
            {
                ModelState.AddModelError($"{nameof(model.WorkInstruction)}.{nameof(model.WorkInstruction.IsSuccessful)}", "The Was Work Instruction Successful field is required.");
            }

            if (model.WorkInstruction.IsSuccessful.HasValue && !model.WorkInstruction.IsSuccessful.Value && model.WorkInstruction.ReasonCodeID == null)
            {
                ModelState.AddModelError($"{nameof(model.WorkInstruction)}.{nameof(model.WorkInstruction.ReasonCodeID)}", "The Reason Code is required.");
            }

            if (model.WorkInstruction.JobType == JobTypesEnum.Deinstall || model.WorkInstruction.JobType == JobTypesEnum.VmsInstall)
            {
                if (model.WorkInstruction.InstallWIID == null)
                {
                    ModelState.AddModelError($"{nameof(model.WorkInstruction)}.{nameof(model.WorkInstruction.InstallWI)}", "Install is required.");
                }
            }

            var job = _workInstructionService.GetCurrentJobOverview((Guid)model.WorkInstruction.JobID);

            model.WorkInstruction.Client = job.Client;
            model.WorkInstruction.ContractNumber = job.ContractNumber;
            model.WorkInstruction.ContractTitle = job.ContractTitle;
            model.WorkInstruction.CustomerProjectCode = job.CustomerProjectCode;
            model.WorkInstruction.SchemeTitle = job.SchemeTitle;
            model.WorkInstruction.JobTitle = job.JobTitle;
            model.WorkInstruction.StatusID = new Guid("BCF18FD2-B9AF-44B9-84C3-121854C844CB");
            model.WorkInstruction.Status = "New";
    
            if (!ModelState.IsValid)
            {
                model.IsSuccessfulStates = SelectListExtensions.NullableYesNoCheckBox();

                return View(model);
            }

            var startDate = model.WorkInstruction.StartDate;
            if (model.FirstConeHour != null || model.FirstConeMin != null)
            {
                model.WorkInstruction.FirstCone = startDate.AddHours(model.FirstConeHour ?? 0).AddMinutes(model.FirstConeMin ?? 0);
            }
            if (model.LastConeHour != null || model.LastConeMin != null)
            {
                model.WorkInstruction.LastCone = startDate.AddHours(model.LastConeHour ?? 0).AddMinutes(model.LastConeMin ?? 0);
            }
            model.WorkInstruction.StartDate = startDate.AddHours(model.StartHour).AddMinutes(model.StartMin);
            model.WorkInstruction.FinishDate = startDate.AddHours(model.FinishHour).AddMinutes(model.FinishMin);

            if (model.WorkInstruction.FinishDate < model.WorkInstruction.StartDate)
            {
                model.WorkInstruction.FinishDate = model.WorkInstruction.FinishDate.AddDays(1);
            }

            model.WorkInstruction.StatusID = new Guid("BCF18FD2-B9AF-44B9-84C3-121854C844CB");

            List<WorkInstructionPriceLineModel> priceLineList = new List<WorkInstructionPriceLineModel>();

            if (model.Documents != null)
            {
                model.Drawings = model.Documents.ToDictionary(k => k.FileID ?? default(Guid), v => v.Title);
            }

            Guid workInstructionID = _workInstructionService.Create(model.WorkInstruction, model.Drawings, model.MethodStatements, model.ToolboxTalks, priceLineList);

            if (workInstructionID == Guid.Empty)
            {
                return View(model);
            }

            return RedirectToAction("Update", new { id = workInstructionID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions, Customer")]
        public ActionResult Update(Guid id)
        {
            var currentWorkInstruction = _workInstructionService.Single(id);
            Dictionary<Guid, string> drawings = _workInstructionService.GetDrawings(id).OrderBy(s => s.Drawing.DrawingTitle).ToDictionary(k => k.DrawingFK, v => v.Drawing.DrawingTitle);            
            Dictionary<Guid, string> methodStatements = _workInstructionService.MethodStatements.GetForWI(id).OrderBy(s => s.MethodStatement.MethodStatementTitle).ToDictionary(k => k.MethodStatementFK, v => v.MethodStatement.MethodStatementTitle);            
            Dictionary<Guid, string> toolboxTalks = _workInstructionService.ToolboxTalks.GetForWI(id).OrderBy(s => s.ToolboxTalk.ToolboxTalk1).ToDictionary(k => k.ToolboxTalkFK, v => v.ToolboxTalk.ToolboxTalk1);
            var priceLines = _workInstructionService.GetWorkInstructionPriceLines(id);
            var documents = _workInstructionService.Documents.GetActiveDocumentsForWorkInstruction(id).Select(s => new WorkInstructionDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Title = s.DocumentTitle, Revision = s.Revision, DrawingID = s.DrawingID, Loaded = true }).ToList();
            
            List<SelectListItem> loadingSheetTemplates = new List<SelectListItem>();
            loadingSheetTemplates.Add(new SelectListItem() { Text = "" });

            WorkInstructionViewModel model = new WorkInstructionViewModel()
            {
                WorkInstruction = currentWorkInstruction,
                WorkInstructionStatus = _workInstructionService.GetWorkInstructionStatus().Select(x => new SelectListItem
                {
                    Text = x.Status,
                    Value = x.StatusID.ToString(),
                    Selected = x.StatusID != null && currentWorkInstruction.StatusID == x.StatusID ? true : false
                }),

                Drawings = drawings,
                MethodStatements = methodStatements,
                ToolboxTalks = toolboxTalks,
                ShiftID = currentWorkInstruction.ShiftID,

                FirstConeHour = currentWorkInstruction.FirstCone?.Hour,
                FirstConeMin = currentWorkInstruction.FirstCone?.Minute,
                LastConeHour = currentWorkInstruction.LastCone?.Hour,
                LastConeMin = currentWorkInstruction.LastCone?.Minute,

                StartHour = currentWorkInstruction.StartDate.Hour,
                StartMin = currentWorkInstruction.StartDate.Minute,
                FinishHour = currentWorkInstruction.FinishDate.Hour,
                FinishMin = currentWorkInstruction.FinishDate.Minute,

                LoadingSheetTemplates = loadingSheetTemplates.Union(_maintenanceService.LoadingSheetTemplates.GetActiveLoadingSheetTemplates().Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.LoadingSheetTemplateID.ToString()
                })),

                IsSuccessfulStates = SelectListExtensions.NullableYesNoCheckBox(),
                Documents = documents
            };

            if (model.WorkInstruction.Status == "Complete" || model.WorkInstruction.Status == "Scheduled" || model.WorkInstruction.Status == "Invoiced")
            {
                model.ShowTasks = true;
                if (model.WorkInstruction.Status == "Complete" || model.WorkInstruction.Status == "Invoiced")
                {
                    // Only add "Invoiced" status if the task is at "Complete" status
                    IEnumerable<SelectListItem> invoicedStatus = new SelectListItem[]
                    {
                        new SelectListItem
                        {
                            Text = "Invoiced",
                            Value = "c2800f3e-9b2a-4e42-b7b2-89a0a897a322",
                            Selected = false
                        }
                    };

                    model.WorkInstructionStatus = model.WorkInstructionStatus.Concat(invoicedStatus);
                }
            }
            else
            {
                model.ShowTasks = true;
            }

            if (currentWorkInstruction.InstallWIID.HasValue)
            {
                model.WorkInstruction.InstallWI = _workInstructionService.Single(currentWorkInstruction.InstallWIID.Value).WorkInstructionNumber.ToString();
            }

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        [HttpPost]
        public ActionResult Update(WorkInstructionViewModel model, string Cancel, string Template)
        {
            List<WorkInstructionDocument> documents = TempData["SubmittedDocuments"] as List<WorkInstructionDocument>;

            if (documents == null)
                documents = new List<WorkInstructionDocument>();

            if (model.DocumentsFromJob != null)
            {
                foreach (var docID in model.DocumentsFromJob)
                {
                    var jobDoc = _jobService.GetJobDrawing(docID);
                    var wiDoc = new WorkInstructionDocument()
                    {
                        DrawingID = jobDoc.DrawingID,
                        Title = jobDoc.Title
                    };
                    if (model.Documents == null)
                        model.Documents = new List<WorkInstructionDocument>();
                    model.Documents.Add(wiDoc);
                }
            }

            if (model.UploadedDocuments != null)
            {
                //Remove any documents that have been deleted
                foreach (var document in documents.Reverse<WorkInstructionDocument>())
                {
                    if (!model.UploadedDocuments.Any(ud => ud.Title.Equals(document.Title)))
                    {
                        documents.Remove(document);
                    }
                }

                //Delete the documents where the user has uploaded new files / or remove files.
                foreach (var document in documents.Reverse<WorkInstructionDocument>())
                {
                    if (model.UploadedDocuments.Where(ud => !ud.Loaded).Any(ud => ud.Title.Equals(document.Title)))
                        documents.Remove(document);
                }

                //Lets add the new documents
                documents.AddRange(model.UploadedDocuments.Where(ud => !ud.Loaded));


                var supportedMineTypes = new List<string>
                    {
                        MimeMapping.GetMimeMapping(".pdf"),
                        MimeMapping.GetMimeMapping(".jpg"),
                        MimeMapping.GetMimeMapping(".png"),
                        MimeMapping.GetMimeMapping(".doc"),
                        MimeMapping.GetMimeMapping(".docx")
                    };

                foreach (var doc in model.UploadedDocuments)
                {
                    if (doc.Document != null)
                    {
                        if (!supportedMineTypes.Contains(doc.Document.ContentType))
                        {
                            ModelState.AddModelError("Invalid file format.", "Drawing file format invalid. Only pdf, jpg, png, doc and docx are supported.");
                        }
                    }
                }
            }
            documents.ForEach(doc => doc.Loaded = true);

            if (Template != null)
            {
                var loadingSheetID = _loadingSheetService.CreateFromTemplate(model.LoadingSheetTemplateID, model.WorkInstruction.WorkInstructionID);

                return RedirectToAction("Update", "LoadingSheet", new { ID = loadingSheetID });
            }
            if (Cancel == null)
            {
                ModelState.Remove("LoadingSheetTemplateID");

                var isCompleted = model.WorkInstruction.StatusID == _workInstructionService.GetWorkInstructionStatusID(WorkInstructionStatuses.Complete);

                if (isCompleted && model.WorkInstruction.IsSuccessful != null && model.WorkInstruction.SRWNumber == null)
                {
                    ModelState.AddModelError($"{nameof(model.WorkInstruction)}.{nameof(model.WorkInstruction.SRWNumber)}", "The SRW / Permit Number field is required.");
                }

                //if (model.WorkInstruction.StatusID == _workInstructionService.GetWorkInstructionStatusID(WorkInstructionStatuses.Complete) && model.WorkInstruction.IsSuccessful == null)
                //{
                //    ModelState.AddModelError($"{nameof(model.WorkInstruction)}.{nameof(model.WorkInstruction.IsSuccessful)}", "The Was Work Instruction Successful field is required.");
                //}

                if (model.WorkInstruction.IsSuccessful.HasValue && !model.WorkInstruction.IsSuccessful.Value && model.WorkInstruction.ReasonCodeID == null)
                {
                    ModelState.AddModelError($"{nameof(model.WorkInstruction)}.{nameof(model.WorkInstruction.ReasonCodeID)}", "The Reason Code field is required.");
                }

                if (model.WorkInstruction.StatusID == WorkInstructionStatusesEnum.Invoiced && model.WorkInstruction.InvoiceDate == null)
                {
                    ModelState.AddModelError("InvoiceDate", "Invoice Date required");
                }

                if (!ModelState.IsValid)
                {

                    TempData["SubmittedDocuments"] = documents;
                    model.UploadedDocuments = documents;

                    model.WorkInstructionStatus = _workInstructionService.GetWorkInstructionStatus().Select(x => new SelectListItem
                    {
                        Text = x.Status,
                        Value = x.StatusID.ToString(),
                        Selected = x.StatusID == model.WorkInstruction.StatusID
                    });

                    model.LoadingSheetTemplates = _maintenanceService.LoadingSheetTemplates.GetActiveLoadingSheetTemplates().Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.LoadingSheetTemplateID.ToString()
                    });

                    model.IsSuccessfulStates = SelectListExtensions.NullableYesNoCheckBox();

                    return View(model);
                }

                var startDate = model.WorkInstruction.StartDate.Date;
                if (model.FirstConeHour != null || model.FirstConeMin != null)
                {
                    model.WorkInstruction.FirstCone = startDate.AddHours(model.FirstConeHour ?? 0).AddMinutes(model.FirstConeMin ?? 0);
                }
                if (model.LastConeHour != null || model.LastConeMin != null)
                {
                    model.WorkInstruction.LastCone = startDate.AddHours(model.LastConeHour ?? 0).AddMinutes(model.LastConeMin ?? 0);
                }

                model.WorkInstruction.StartDate = startDate.AddHours(model.StartHour).AddMinutes(model.StartMin);
                model.WorkInstruction.FinishDate = startDate.AddHours(model.FinishHour).AddMinutes(model.FinishMin);

                if (model.WorkInstruction.FinishDate < model.WorkInstruction.StartDate)
                {
                    model.WorkInstruction.FinishDate = model.WorkInstruction.FinishDate.AddDays(1);
                }

                var existingDrawings = _workInstructionService.GetDrawings(model.WorkInstruction.WorkInstructionID).OrderBy(s => s.Drawing.DrawingTitle).ToDictionary(k => k.DrawingFK, v => v.Drawing.DrawingTitle);
                var existingMethodStatements = _workInstructionService.MethodStatements.GetForWI(model.WorkInstruction.WorkInstructionID).OrderBy(s => s.MethodStatement.MethodStatementTitle).ToDictionary(k => k.MethodStatementFK, v => v.MethodStatement.MethodStatementTitle);
                var existingToolboxTalks = _workInstructionService.ToolboxTalks.GetForWI(model.WorkInstruction.WorkInstructionID).OrderBy(s => s.ToolboxTalk.ToolboxTalk1).ToDictionary(k => k.ToolboxTalkFK, v => v.ToolboxTalk.ToolboxTalk1);

                if (model.Documents != null)
                {
                    if (model.Drawings  == null)
                        model.Drawings = new Dictionary<Guid, string>();
                    foreach (var doc in model.Documents)
                    {
                        model.Drawings.Add(doc.DrawingID ?? default(Guid), doc.Title);
                    }
                }

                _workInstructionService.Update(model.WorkInstruction, model.Drawings, existingDrawings, model.MethodStatements, existingMethodStatements, model.ToolboxTalks, existingToolboxTalks);

                if (documents != null)
                {
                    foreach (var document in documents)
                    {
                        byte[] documentData = new byte[document.Document.ContentLength];
                        document.Document.InputStream.Read(documentData, 0, documentData.Length);
                        _workInstructionService.Documents.Create(model.WorkInstruction.WorkInstructionID, document.Title, document.Revision ?? "", document.Document.FileName, document.Document.ContentType, documentData);
                    }
                }

            }

            return RedirectToAction("Update", new { id = model.WorkInstruction.WorkInstructionID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult RepeatWorkInstruction(Guid id)
        {
            var currentWorkInstruction = _workInstructionService.Single(id);
            
            RepeatWorkInstructionViewModel model = new RepeatWorkInstructionViewModel()
            {
                WorkInstructionID = currentWorkInstruction.WorkInstructionID,
                JobID =  (Guid)currentWorkInstruction.JobID,
                WorkInstructionNumber = currentWorkInstruction.WorkInstructionNumber,
                StartDate = currentWorkInstruction.StartDate,
                Comments = currentWorkInstruction.Comments,
                EmployeeName = currentWorkInstruction.Employee.EmployeeName,
                TrafficManagementActivities = currentWorkInstruction.TrafficManagementActivities,
                IsActive = true,
                //JobTypeID = currentWorkInstruction.JobTypeID,
                //JobType = currentWorkInstruction.JobType,
                ChecklistID = currentWorkInstruction.ChecklistID,
                Checklist = currentWorkInstruction.Checklist,
                Client = currentWorkInstruction.Client,
                ContractNumber = currentWorkInstruction.ContractNumber,
                SchemeTitle = currentWorkInstruction.SchemeTitle,
                ContractTitle = currentWorkInstruction.ContractTitle,
                sequenceStartDate = DateTime.Now.Date,
                sequenceEndDate = DateTime.Now.AddDays(6).Date,
                AddressLine1 = currentWorkInstruction.AddressLine1,
                AddressLine2 = currentWorkInstruction.AddressLine2,
                AddressLine3 = currentWorkInstruction.AddressLine3,
                City = currentWorkInstruction.City,
                County = currentWorkInstruction.County,
                Postcode = currentWorkInstruction.Postcode
            };

            List<SelectListItem> repeatTypes = new List<SelectListItem>();
            repeatTypes.Add(new SelectListItem() { Text = "", Value = "0" });
            repeatTypes.Add(new SelectListItem() { Text = "Every Day", Value = "1"});            
            repeatTypes.Add(new SelectListItem() { Text = "Every Month", Value = "2" });
            repeatTypes.Add(new SelectListItem() { Text = "Every Other Month", Value = "3" });
            repeatTypes.Add(new SelectListItem() { Text = "Every Year", Value = "4" });
            repeatTypes.Add(new SelectListItem() { Text = "Custom", Value = "5" });
            repeatTypes.Add(new SelectListItem() { Text = "Select Day", Value = "6" });
            model.RepeatType = repeatTypes;

            List<SelectListItem> weekendInstruction = new List<SelectListItem>();
            weekendInstruction.Add(new SelectListItem() { Text = "", Value = "0" });
            weekendInstruction.Add(new SelectListItem() { Text = "Do Nothing", Value = "1" });
            weekendInstruction.Add(new SelectListItem() { Text = "Move to Monday", Value = "2" });
            weekendInstruction.Add(new SelectListItem() { Text = "Move to Friday", Value = "3" });
            model.WeekendInstruction = weekendInstruction;

            List<SelectListItem> dayFrequency = new List<SelectListItem>();
            dayFrequency.Add(new SelectListItem() { Text = "", Value = "0" });
            dayFrequency.Add(new SelectListItem() { Text = "Weekly", Value = "1" });
            dayFrequency.Add(new SelectListItem() { Text = "Every Other Week", Value = "2" });
            dayFrequency.Add(new SelectListItem() { Text = "1st Occurrence of the Month", Value = "3" });            
            model.DayFrequency = dayFrequency;

            List<SelectListItem> jobTypes = new List<SelectListItem>();

            Dictionary<Guid, string> jobTypeDict = _workInstructionService.GetJobTypes();

            jobTypes.Add(new SelectListItem() { Text = "", Value = Guid.Empty.ToString() });

            foreach (var item in jobTypeDict)
            {
                if (currentWorkInstruction.JobTypeID.ToString() == item.Key.ToString())
                {
                    jobTypes.Add(new SelectListItem() { Text = item.Value, Value = item.Key.ToString(), Selected = true });
                }
                else
                {
                    jobTypes.Add(new SelectListItem() { Text = item.Value, Value = item.Key.ToString() });
                }
            }

            model.JobType = jobTypes;

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        [HttpPost]
        public ActionResult RepeatWorkInstruction(RepeatWorkInstructionViewModel model, string cancel)
        {
            bool success = true;

            if (new Guid(model.JobTypeID) == Guid.Empty)
            {
                ModelState.AddModelError("JobType", "Please select a job type.");
            }

            if (cancel == null && ModelState.IsValid)
            {
                var addressModel = model.GetAddressModel();

                switch (model.RepeatID)
                {
                    case "0":
                        success = false;
                        break;

                    case "1": // Every Day

                        while (model.sequenceStartDate.Date <= model.sequenceEndDate.Date)
                        {
                            success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, model.sequenceStartDate.Date, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);

                            model.sequenceStartDate = model.sequenceStartDate.AddDays(1);
                        }

                        break;

                    case "2": // Every Month
                    case "3": // Every Other Month
                    case "4": // Every Year
                        while (model.sequenceStartDate.Date <= model.sequenceEndDate.Date)
                        {
                            DateTime date = model.sequenceStartDate.Date;

                            if (model.WeekendInstructionID == "0") // None selected
                            {
                                success = false;
                                break;
                            }
                            else if (model.WeekendInstructionID == "2") // Move to next Monday
                            {
                                if (((int)date.DayOfWeek).Equals(0)) // On a Sunday
                                {
                                    date = date.AddDays(1);
                                }
                                else if (((int)date.DayOfWeek).Equals(6)) // On a Saturday
                                {
                                    date = date.AddDays(2);
                                }

                                if (date.Date >= model.sequenceEndDate.Date)
                                {
                                    break;
                                }
                            }
                            else if (model.WeekendInstructionID == "3") // Move to previous Friday
                            {
                                if (((int)date.DayOfWeek).Equals(0)) // On a Sunday
                                {
                                    date = date.AddDays(-2);
                                }
                                else if (((int)date.DayOfWeek).Equals(6)) // On a Saturday
                                {
                                    date = date.AddDays(-1);
                                }

                                if (date.Date <= model.sequenceStartDate.Date)
                                {
                                    break;
                                }
                            }

                            success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, date.Date, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);

                            if (model.RepeatID != "4")
                            {
                                model.sequenceStartDate = model.sequenceStartDate.AddMonths(1);

                                if (model.RepeatID == "3")
                                {
                                    model.sequenceStartDate = model.sequenceStartDate.AddMonths(1);
                                }
                            }
                            else
                            {
                                model.sequenceStartDate = model.sequenceStartDate.AddYears(1);
                            }
                        }

                        break;

                    case "5": // Custom

                        if (model.DayFrequencyID == "0")
                        {
                            success = false;
                            break;
                        }
                        else 
                        {
                            while (model.sequenceStartDate.Date <= model.sequenceEndDate.Date)
                            {
                                DateTime date = model.sequenceStartDate.Date;

                                if (!((int)model.sequenceStartDate.DayOfWeek).Equals(0)) // Start for this week 
                                {
                                    int month = model.sequenceStartDate.Month;
                                    int dow = (int)model.sequenceStartDate.DayOfWeek;

                                    int daysToAdd = 7 - dow;

                                    date = date.AddDays(daysToAdd);

                                    if (date.Month != month)
                                    {
                                        date = date.AddDays(-7);
                                    }
                                }
                               
                                if (model.Sunday)
                                {
                                    success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, date, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);
                                }

                                if (model.Monday)
                                {
                                    DateTime newDate = date.AddDays(1);
                                    if (newDate <= model.sequenceEndDate.Date)
                                    {
                                        success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, newDate, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);
                                    }
                                }

                                if (model.Tuesday)
                                {
                                    DateTime newDate = date.AddDays(2);
                                    if (newDate <= model.sequenceEndDate.Date)
                                    {
                                        success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, newDate, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);
                                    }
                                }

                                if (model.Wednesday)
                                {
                                    DateTime newDate = date.AddDays(3);
                                    if (newDate <= model.sequenceEndDate.Date)
                                    {
                                        success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, newDate, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);
                                    }
                                }

                                if (model.Thursday)
                                {
                                    DateTime newDate = date.AddDays(4);
                                    if (newDate <= model.sequenceEndDate.Date)
                                    {
                                        success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, newDate, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);
                                    }
                                }

                                if (model.Friday)
                                {
                                    DateTime newDate = date.AddDays(5);
                                    if (newDate <= model.sequenceEndDate.Date)
                                    {
                                        success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, newDate, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);
                                    }
                                }

                                if (model.Saturday)
                                {
                                    DateTime newDate = date.AddDays(6);
                                    if (newDate <= model.sequenceEndDate.Date)
                                    {
                                        success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, newDate, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);
                                    }
                                }

                                if (model.DayFrequencyID != "3")
                                {
                                    model.sequenceStartDate = model.sequenceStartDate.AddDays(7); // Weekly

                                    if (model.DayFrequencyID == "2")
                                    {
                                        model.sequenceStartDate = model.sequenceStartDate.AddDays(7); // Fortnightly
                                    }
                                }
                                else // First occurence of the month
                                {
                                    model.sequenceStartDate = model.sequenceStartDate.AddMonths(1);
                                }
                            }
                        }  

                        break;

                    case "6": // On day
                        success = _workInstructionService.DuplicateWorkInstruction(model.WorkInstructionID, model.sequenceStartDate.Date, new Guid(model.JobTypeID), model.ChecklistID, model.InstallWIID, model.Comments, model.TrafficManagementActivities, addressModel);
                        break;

                    default:
                        break;

                }

                if (!success)
                {
                    ModelState.AddModelError("Repeat", "Failed to repeat Work Instruction.");
                }
            }

            if (!ModelState.IsValid)
            {
                var currentWorkInstruction = _workInstructionService.Single(model.WorkInstructionID);

                model = new RepeatWorkInstructionViewModel()
                {
                    WorkInstructionID = currentWorkInstruction.WorkInstructionID,
                    JobID = (Guid)currentWorkInstruction.JobID,
                    WorkInstructionNumber = currentWorkInstruction.WorkInstructionNumber,
                    StartDate = currentWorkInstruction.StartDate,
                    Comments = currentWorkInstruction.Comments,
                    EmployeeName = currentWorkInstruction.Employee.EmployeeName,
                    TrafficManagementActivities = currentWorkInstruction.TrafficManagementActivities,
                    IsActive = true,
                    Client = currentWorkInstruction.Client,
                    ContractNumber = currentWorkInstruction.ContractNumber,
                    SchemeTitle = currentWorkInstruction.SchemeTitle,
                    ContractTitle = currentWorkInstruction.ContractTitle,
                    sequenceStartDate = DateTime.Now.Date,
                    sequenceEndDate = DateTime.Now.AddDays(7).Date,
                    AddressLine1 = currentWorkInstruction.AddressLine1,
                    AddressLine2 = currentWorkInstruction.AddressLine2,
                    AddressLine3 = currentWorkInstruction.AddressLine3,
                    City = currentWorkInstruction.City,
                    County = currentWorkInstruction.County,
                    Postcode = currentWorkInstruction.Postcode
                };

                List<SelectListItem> repeatTypes = new List<SelectListItem>();
                repeatTypes.Add(new SelectListItem() { Text = "", Value = "0" });
                repeatTypes.Add(new SelectListItem() { Text = "Every Day", Value = "1" });
                repeatTypes.Add(new SelectListItem() { Text = "Every Month", Value = "2" });
                repeatTypes.Add(new SelectListItem() { Text = "Every Other Month", Value = "3" });
                repeatTypes.Add(new SelectListItem() { Text = "Every Year", Value = "4" });
                repeatTypes.Add(new SelectListItem() { Text = "Custom", Value = "5" });
                repeatTypes.Add(new SelectListItem() { Text = "Select Day", Value = "6" });
                model.RepeatType = repeatTypes;

                List<SelectListItem> weekendInstruction = new List<SelectListItem>();
                weekendInstruction.Add(new SelectListItem() { Text = "", Value = "0" });
                weekendInstruction.Add(new SelectListItem() { Text = "Do Nothing", Value = "1" });
                weekendInstruction.Add(new SelectListItem() { Text = "Move to Monday", Value = "2" });
                weekendInstruction.Add(new SelectListItem() { Text = "Move to Friday", Value = "3" });
                model.WeekendInstruction = weekendInstruction;

                List<SelectListItem> dayFrequency = new List<SelectListItem>();
                dayFrequency.Add(new SelectListItem() { Text = "", Value = "0" });
                dayFrequency.Add(new SelectListItem() { Text = "Weekly", Value = "1" });
                dayFrequency.Add(new SelectListItem() { Text = "Every Other Week", Value = "2" });
                dayFrequency.Add(new SelectListItem() { Text = "1st Occurrence of the Month", Value = "3" });
                model.DayFrequency = dayFrequency;

                if (model.RepeatID == "0" || model.RepeatID == null)
                {
                    ModelState.AddModelError("Repeat Type", "Please select a repeat type.");
                }
                else
                {
                    ModelState.AddModelError("Error Message", "Please try again.");
                }

                if (model.WeekendInstructionID == "0" && (model.RepeatID == "2" || model.RepeatID == "3" || model.RepeatID == "4"))
                {
                    ModelState.AddModelError("Weekend Type", "Please select a weekend behaviour.");
                }

                if (model.WeekendInstructionID == "0" && model.RepeatID == "5" && model.DayFrequencyID == "0")
                {
                    ModelState.AddModelError("Day Frequency", "Please select a frequency.");
                }

                List<SelectListItem> jobTypes = new List<SelectListItem>();

                Dictionary<Guid, string> jobTypeDict = _workInstructionService.GetJobTypes();

                jobTypes.Add(new SelectListItem() { Text = "", Value = Guid.Empty.ToString() });

                foreach (var item in jobTypeDict)
                {
                    if (currentWorkInstruction.JobTypeID.ToString() == item.Key.ToString())
                    {
                        jobTypes.Add(new SelectListItem() { Text = item.Value, Value = item.Key.ToString(), Selected = true });
                    }
                    else
                    {
                        jobTypes.Add(new SelectListItem() { Text = item.Value, Value = item.Key.ToString() });
                    }
                }

                model.JobType = jobTypes;

                return View(model);
            }
            else // Successful
            {
                // Redirect to list with a parent WIFK
                return RedirectToAction("RepeatedWorkInstructions", new { parentWIFK = model.WorkInstructionID });
                //return RedirectToAction("Active");
            }
        }
        
        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult NewShiftProgress(Guid shiftID, Guid workInstructionID)
        {
            ShiftProgressVM model = new ShiftProgressVM() 
            {
                WorkInstructionID = workInstructionID,
                ShiftID = shiftID,
                StartedDate = DateTime.Now,
                StartedHour = DateTime.Now.TimeOfDay.Hours,
                StartedMin = DateTime.Now.TimeOfDay.Minutes,
                ShiftProgresses = _workInstructionService.GetShiftProgresses(shiftID).Select(x => new SelectListItem
                {
                    Text = x.Progress,
                    Value = x.ShiftProgressID.ToString()
                }),

            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        [HttpPost]
        public ActionResult NewShiftProgress(ShiftProgressVM model)
        {
            bool result = _workInstructionService.AddShiftProgress(new ShiftProgressInfoModel 
            {
                ShiftProgressID = model.ShiftProgressID,
                ShiftID = model.ShiftID,
                CreatedDate = new DateTime(model.StartedDate.Year, model.StartedDate.Month, model.StartedDate.Day, model.StartedHour, model.StartedMin, 0)
                
            });

            if (result)
            {
                return RedirectToAction("Update", new { id = model.WorkInstructionID });
            }
            else
            {
                ModelState.AddModelError("NewShiftProgress", "Failed to add new shift progress.");
                model.StartedDate = DateTime.Now;
                model.StartedHour = DateTime.Now.TimeOfDay.Hours;
                model.StartedMin = DateTime.Now.TimeOfDay.Minutes;
                model.ShiftProgresses = _workInstructionService.GetShiftProgresses(model.ShiftID).Select(x => new SelectListItem
                {
                    Text = x.Progress,
                    Value = x.ShiftProgressID.ToString()
                });
                return View(model);
            }
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Operation, WorkInstructions")]
        public ActionResult GetInstallWILookup(Guid? jobID, int rowCount, int page, string query)
        {
            var lookup = _workInstructionService.GetPagedInstallLookup(rowCount, page, query, jobID);

            LookupModel model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[]
                {
                    "Task",
                    "Job",
                    "Job Title",
                    "Client",
                    "Contract"

                },
                Rows = lookup.Data.Select(s => new LookupRow
                {
                    Key = s.Key,
                    Value = s.Value,
                    TableData = new string[]
                    {
                        s.Key,
                        s.JobNumber.ToString(),
                        s.JonTitle,
                        s.ClientName,
                        s.ContractTitle
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPriceLinesForWorkInstruction(int rowCount, int page, string query, Guid workInstructionID)
        {
            try
            {
                PagedResults<WorkInstructionPriceLineModel> results = _workInstructionService.GetPagedActiveWorkInstructionPriceLines(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[]
                    {
                        "Code",
                        "Description",
                        "Quantity",
                        "Rate",
                        "Total",
                        "Notes?"
                    },
                    Rows = results.Data.Select(a => new TableRow
                    {
                        TableData = new string[]
                        {
                            a.Code,
                            a.Description,
                            a.Quantity.HasValue ? a.Quantity?.ToString("F") : "0.00",
                            a.Rate?.ToString("0.00##"),
                            a.Total.ToString("F"),
                            !String.IsNullOrWhiteSpace(a.Note) ? "Yes" : ""
                        },
                        Actions = new List<TableAction>
                        {
                            new TableAction("/Contract/WorkInstruction/UpdatePriceLine/" + a.WorkInstructionPriceLineID, "View", "icon-eye-open")
                        }
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetObservationsForJob", e, workInstructionID.ToString());
                return View();
            }
        }

        public ActionResult NewPriceLine(Guid workInstructionID)
        {
            var model = new WorkInstructionPriceLineViewModel();
            model.WorkInstructionPriceLine.WorkInstructionID = workInstructionID;

            return View("NewPriceLine", model);
        }

        [HttpPost]
        public ActionResult NewPriceLine(WorkInstructionPriceLineViewModel model)
        {
            if (ModelState.IsValid)
            {
                _workInstructionService.CreateWorkInstructionPriceLine(model.WorkInstructionPriceLine);
                return RedirectToAction("Update", "WorkInstruction", new { id = model.WorkInstructionPriceLine.WorkInstructionID });
            }

            return View("NewPriceLine", model);
        }

        public ActionResult UpdatePriceLine(Guid id)
        {
            var model = new WorkInstructionPriceLineViewModel();
            model.WorkInstructionPriceLine = _workInstructionService.SingleWorkInstructionPriceLine(id);

            return View("UpdatePriceLine", model);
        }

        [HttpPost]
        public ActionResult UpdatePriceLine(WorkInstructionPriceLineViewModel model)
        {
            if (ModelState.IsValid)
            {
                _workInstructionService.UpdateWorkInstructionPriceLine(model.WorkInstructionPriceLine);
                return RedirectToAction("Update", "WorkInstruction", new { id = model.WorkInstructionPriceLine.WorkInstructionID });
            }

            return View("UpdatePriceLine", model);
        }

        public ActionResult GetBriefingSheet(Guid workInstructionID)
        {
            var workInstruction = _workInstructionService.Single(workInstructionID);
            var pdf = _pdfService.GetBriefingSheet(workInstructionID);
            var filename = $"Task {workInstruction.WorkInstructionNumber} - Briefing Sheet.pdf";
            return File(pdf, ContentType.Pdf, filename);
        }

        public ActionResult GetOnHireNote(Guid workInstructionID)
        {
            var imagesPath = Server.MapPath(@"~\Content\images");
            var pdf = _pdfService.GetOnOffHireNote(workInstructionID, imagesPath, true);
            var workInstruction = _workInstructionService.Single(workInstructionID);
            var filename = $"Task {workInstruction.WorkInstructionNumber} - On Hire Note.pdf";
            return File(pdf, ContentType.Pdf, filename);
        }

        public ActionResult GetOffHireNote(Guid workInstructionID)
        {
            var imagesPath = Server.MapPath(@"~\Content\images");
            var pdf = _pdfService.GetOnOffHireNote(workInstructionID, imagesPath, false);
            var workInstruction = _workInstructionService.Single(workInstructionID);
            var filename = $"Task {workInstruction.WorkInstructionNumber} - Off Hire Note.pdf";
            return File(pdf, ContentType.Pdf, filename);
        }

        public ActionResult AddPriceLines(Guid[] priceLineIDs, Guid workInstructionID)
        {
            var res = _workInstructionService.AddPriceLines(workInstructionID, priceLineIDs);

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetDefaultMethodStatement(Guid JobTypeID)
        {
            var id = Services.MethodStatements.Enums.Enums.DefaultMethodStatement;
            var typeList = Services.MethodStatements.Enums.Enums.DefaultJobTypes();

            var model = new DefaultMethodStatement();
            model.MethodStatementModel = _methodStatementService.Single(id);

            if (typeList.Contains(JobTypeID))
            {
                model.Status = true;
            }
            else
            {
                model.Status = false;
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
