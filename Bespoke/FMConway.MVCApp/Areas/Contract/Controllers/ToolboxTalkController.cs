﻿using FMConway.MVCApp.Areas.Contract.ViewModels.ToolboxTalks;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.ToolboxTalks.Interfaces;
using FMConway.Services.ToolboxTalks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Toolbox Talks", 120, "fa fa-toolbox", "Administrator, Supervisor, Manager, ToolboxTalks")]
    public class ToolboxTalkController : BaseController
    {
        IToolboxTalkService _toolboxTalkService;

        public ToolboxTalkController(IToolboxTalkService toolboxTalkService)
        {
            _toolboxTalkService = toolboxTalkService;
        }

        #region Toolbo Talk table actions
        Func<ToolboxTalkModel, TableAction> updateAction = toolboxTalk =>
        {
            return new TableAction()
            {
                Url = "/Contract/ToolboxTalk/Update/" + toolboxTalk.ToolboxTalkID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<ToolboxTalkModel, TableAction> viewAction = toolboxTalk =>
        {
            return new TableAction()
            {
                Url = "/Contract/ToolboxTalk/Summary/" + toolboxTalk.ToolboxTalkID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        public ActionResult ActiveToolboxTalks(int rowCount, int page, string query)
        {
            PagedResults<ToolboxTalkModel> results = _toolboxTalkService.GetActiveToolboxTalksPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = ToolboxTalkOverviewTable(results, new List<Func<ToolboxTalkModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        public ActionResult InactiveToolboxTalks(int rowCount, int page, string query)
        {
            PagedResults<ToolboxTalkModel> results = _toolboxTalkService.GetInactiveToolboxTalksPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = ToolboxTalkOverviewTable(results, new List<Func<ToolboxTalkModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable ToolboxTalkOverviewTable(PagedResults<ToolboxTalkModel> results, List<Func<ToolboxTalkModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Toolbox Talk Title", "Revision" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Title, s.Revision },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            ToolboxTalkViewModel model = new ToolboxTalkViewModel() { };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        [HttpPost]
        public ActionResult New(ToolboxTalkViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Guid toolboxTalkID = _toolboxTalkService.Create(model.ToolboxTalk);

            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        public ActionResult Update(Guid id)
        {
            ToolboxTalkViewModel model = new ToolboxTalkViewModel()
            {
                ToolboxTalk = _toolboxTalkService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        [HttpPost]
        public ActionResult Update(ToolboxTalkViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _toolboxTalkService.Update(model.ToolboxTalk);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            ToolboxTalkViewModel model = new ToolboxTalkViewModel()
            {
                ToolboxTalk = _toolboxTalkService.Single(id)
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, ToolboxTalks")]
        [HttpPost]
        public ActionResult Summary()
        {
            return RedirectToAction("Inactive");
        }

        public ActionResult GetToolboxTalkLookup(int rowCount, int page, string query)
        {
            PagedResults<ToolboxTalkLookup> lookup = _toolboxTalkService.GetPagedActiveToolboxTalksLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Toolbox Talk", "Revision" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key, s.Revision } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
