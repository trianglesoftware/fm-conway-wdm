﻿using FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions;
using FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructionDetails;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Interfaces;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.Maintenance;
using FMConway.Services.WorkInstructions.Interfaces;
using FMConway.Services.WorkInstructions.Models;
using FMConway.Services.WorkInstructionDetails.Interfaces;
using FMConway.Services.WorkInstructionDetails.Models;
using FMConway.Services.ErrorLogging.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    public class WorkInstructionDetailsController : BaseController
    {
        IWorkInstructionService _workInstructionService;
        IMaintenanceService _maintenanceService;
        ILoadingSheetService _loadingSheetService;
        IWorkInstructionDetailsService _workInstructionDetailsService;
        IErrorLoggingService _errorLoggingService;

        public WorkInstructionDetailsController(IWorkInstructionService workInstructionService, IMaintenanceService maintenanceService, ILoadingSheetService loadingSheetService, IWorkInstructionDetailsService workInstructionDetailsService, IErrorLoggingService errorLoggingService)
        {
            _workInstructionService = workInstructionService;
            _maintenanceService = maintenanceService;
            _loadingSheetService = loadingSheetService;
            _workInstructionDetailsService = workInstructionDetailsService;
            _errorLoggingService = errorLoggingService;
        }

        public ActionResult Details(Guid id)
        {
            var currentWorkInstruction = _workInstructionService.Single(id);

            var model = new DetailsViewModel
            { 
                JobID = currentWorkInstruction.JobID,
                WorkInstructionID = currentWorkInstruction.WorkInstructionID,
				ShiftID = currentWorkInstruction.ShiftID.Value
            };

            return View(model);
        }

        public ActionResult ChecksCompleted(Guid id)
        {
            var workInstruction = _workInstructionService.Single(id);

            var model = new ChecksCompletedViewModel
            {
                JobID = (Guid)workInstruction.JobID,
                WorkInstructionID = workInstruction.WorkInstructionID,
                ShiftID = workInstruction.ShiftID.Value
            };

            return View(model);
        }

        public ActionResult EquipmentItemChecklist(Guid id)
        {
            var model = new EquipmentItemChecklistViewModel();
            model.EquipmentItemChecklist = _workInstructionService.GetEquipmentItemChecklist(id);

            return View(model);
        }

        #region Paged Tables

        public ActionResult GetShiftVehicles(int rowCount, int page, string query, Guid shiftID)
        {
            try
            {
                var results = _workInstructionDetailsService.GetShiftVehiclesPaged(rowCount, page, HttpUtility.HtmlEncode(query), shiftID);

                var model = new PagedTable()
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[]
                    {
                        "Vehicle",
                        "Registration",
                        "Driver",
                        "Mileage",
                        "Reason Not On"
                    },
                    Rows = results.Data.Select(s => new TableRow
                    {
                        TableData = new string[]
                        {
                            s.VehicleMake,
                            s.Registration,
                            s.Driver,
                            s.StartMileage.Equals(0) ? "Unknown" : s.StartMileage.ToString(),
                            s.NotOnShift ? s.ReasonNotOnShift : "N/A"
                        },
                        Actions = new List<TableAction>
                        {
                            new TableAction("/Contract/WorkInstructionDetails/ViewShiftVehicle/" + s.ShiftVehicleID, "View", "icon-eye-open")
                        }
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetVehicles", e, shiftID.ToString());
            }
            return View();
        }

        public ActionResult GetShiftStaff(int rowCount, int page, string query, Guid shiftID)
        {
            try
            {
                PagedResults<ShiftStaffModel> results = _workInstructionDetailsService.GetShiftStaffPaged(rowCount, page, HttpUtility.HtmlEncode(query), shiftID);

                PagedTable model = ShiftStaffOverviewTable(results);

                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetStaff", e, shiftID.ToString());
            } 
            return View();
        }

        public ActionResult GetVehicleDefectPhotos(int rowCount, int page, string query, Guid vehicleChecklistDefectID)
        {
            try
            {
                PagedResults<VehicleDefectPhotoModel> results = _workInstructionDetailsService.GetVehicleDefectPhotosPaged(rowCount, page, query, vehicleChecklistDefectID);

                PagedTable model = VehicleChecklistPhotoOverviewTable(results, new List<Func<VehicleDefectPhotoModel, TableAction>>() { viewVehicleDefectPhoto});

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetVehicleDefectPhotos", e, vehicleChecklistDefectID.ToString());
            }
            return View();
        }

        public ActionResult GetShiftVehicleMileagePhotos(int rowCount, int page, string query, Guid shiftVehicleID)
        {
            try
            {
                var results = _workInstructionDetailsService.GetShiftVehiclePhotosPaged(rowCount, page, query, shiftVehicleID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[]
                    {
                        "Name"
                    },
                    Rows = results.Data.Select(s => new TableRow
                    {
                        TableData = new string[]
                        {
                            s.FileName
                        },
                        Actions = new List<TableAction>
                        {
                            new TableAction("/File/CreateDocument/" + s.FileID, "View", "icon-pencil")
                        }
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetShiftVehicleMileagePhotos", e, shiftVehicleID.ToString());
            }
            return View();
        }

        public ActionResult GetMaintenanceDetailPhotos(int rowCount, int page, string query, Guid maintenanceCheckDetailID)
        {
            try
            {
                PagedResults<MaintenanceDetailPhotoModel> results = _workInstructionDetailsService.GetMaintenanceCheckPhotosPaged(rowCount, page, query, maintenanceCheckDetailID);

                PagedTable model = MaintenanceCheckPhotoOverviewTable(results, new List<Func<MaintenanceDetailPhotoModel, TableAction>>() { viewMaintenanceDetailPhoto});

                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetMaintenanceDetailPhotos", e, maintenanceCheckDetailID.ToString());
            }
            return View();
        }

        public ActionResult GetNotes(int rowCount, int page, string query, Guid workInstructionID)
        {
            PagedResults<NoteModel> results = _workInstructionDetailsService.GetNotesPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

            PagedTable model = NoteOverviewTable(results);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleChecklistAnswers(int rowCount, int page, string query, Guid shiftID, Guid workInstructionID)
        {
            try
            {
                PagedResults<VehicleChecklistAnswersModel> results = _workInstructionDetailsService.GetVehicleChecklistAnswersPaged(rowCount, page, HttpUtility.HtmlEncode(query), shiftID);

                PagedTable model = VehicleChecklistAnswersOverviewTable(results, new List<Func<VehicleChecklistAnswersModel, TableAction>>() { viewVehicleChecklistAnswerAction });

                foreach (var item in model.Rows)
                {
                    if (item.Actions != null)
                    {
                        foreach (var action in item.Actions)
                        {
                            action.Url += "?workInstructionID=" + workInstructionID.ToString();
                        }
                    }
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetVehicleChecklistAnswers", e, shiftID.ToString());
                return View();
            }

        }

        public ActionResult GetMaintenanceCheckAnswers(int rowCount, int page, string query, Guid workInstructionID)
        {
            try
            {
                PagedResults<ChecklistAnswerModel> results = _workInstructionDetailsService.GetMaintenanceCheckAnswersPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

                PagedTable model = MaintenanceCheckOverviewTable(results, new List<Func<ChecklistAnswerModel, TableAction>>() { viewMaintenanceCheckAnswerAction });

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetMaintenanceCheckAnswers", e, workInstructionID.ToString());
                return View();
            } 
        }

        public ActionResult GetBriefingSignatures(int rowCount, int page, string query, Guid workInstructionID)
        {
            try
            {
                PagedResults<BriefingSignatureModel> results = _workInstructionDetailsService.GetBriefingSignaturesPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

                PagedTable model = BriefingSignatureOverviewTable(results, new List<Func<BriefingSignatureModel, TableAction>>() { viewBriefingSignatureAction });

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetBriefingSignatures", e, workInstructionID.ToString());
                return View();
            }
        }

        public ActionResult GetLoadingSheetSignatures(int rowCount, int page, string query, Guid workInstructionID)
        {
            try
            {
                PagedResults<LoadingSheetSignatureModel> results = _workInstructionDetailsService.GetLoadingSheetSignaturesPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

                PagedTable model = LoadingSheetSignatureOverviewTable(results, new List<Func<LoadingSheetSignatureModel, TableAction>>() { viewLoadingSheetSignatureAction });

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetLoadingSheetSignatures", e, workInstructionID.ToString());
                return View();
            }
        }

        public ActionResult GetLoadingSheetEquipmentItems(int rowCount, int page, string query, Guid workInstructionID)
        {
            try
            {
                var results = _workInstructionDetailsService.GetLoadingSheetEquipmentItemsPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[]
                    {
                        "Loading Sheet",
                        "Equipment",
                        "Asset",
                        "Item Number"
                    },
                    Rows = results.Data.Select(a => new TableRow
                    {
                        TableData = new string[]
                        {
                            a.LoadingSheet.ToString(),
                            a.Equipment,
                            a.VmsOrAsset,
                            a.ItemNumber.ToString()
                        },
                        Actions = new List<TableAction>
                        {
                            new TableAction("/Contract/WorkInstructionDetails/EquipmentItemChecklist/" + a.EquipmentitemChecklistPK, "View", "icon-eye-open")
                        }
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetLoadingSheetEquipmentItems", e, workInstructionID.ToString());
            }
            return View();
        }

        public ActionResult GetEquipmentItemChecklistAnswersPaged(int rowCount, int page, string query, Guid equipmentItemChecklistID)
        {
            try
            {
                var results = _workInstructionDetailsService.GetEquipmentItemChecklistAnswersPaged(rowCount, page, HttpUtility.HtmlEncode(query), equipmentItemChecklistID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[]
                    {
                        "Question",
                        "Answer",
                        "Reason"
                    },
                    Rows = results.Data.Select(a => new TableRow
                    {
                        TableData = new string[]
                        {
                            a.Question,
                            a.Answer ? "Yes" : "No",
                            a.Reason
                        },
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetEquipmentItemChecklistAnswersPaged", e, equipmentItemChecklistID.ToString());
            }
            return View();
        }

        #endregion

        #region Table actions

        Func<VehicleChecklistAnswersModel, TableAction> viewVehicleChecklistAnswerAction = answer => 
            {
                return new TableAction()
                {
                    Url = "/Contract/WorkInstructionDetails/ViewVehicleDefect/" + answer.VehicleChecklistAnswerID.Value,
                    Title = "View",
                    IconClass = "icon-pencil"
                };
            };

        Func<VehicleDefectPhotoModel, TableAction> viewVehicleDefectPhoto = p =>
        {
            return new TableAction()
            {
                Url = "/File/CreateDocument/" + p.FileID,
                Title = "View",
                IconClass = "icon-pencil"
            };
        };

        Func<ChecklistAnswerModel, TableAction> viewMaintenanceCheckAnswerAction = answer =>
            {
                return new TableAction() 
                {
                    Url = "/Contract/WorkInstructionDetails/ViewMaintenanceDetail/" + answer.ChecklistAnswerID.Value,
                    Title = "View",
                    IconClass = "icon-pencil"
                };
            };

        Func<MaintenanceDetailPhotoModel, TableAction> viewMaintenanceDetailPhoto = p =>
        {
            return new TableAction()
            {
                Url = "/File/CreateDocument/" + p.FileID,
                Title = "View",
                IconClass = "icon-pencil"
            };
        };

        Func<BriefingSignatureModel, TableAction> viewBriefingSignatureAction = signature =>
            {
                return new TableAction() 
                {
                    Url = "/File/CreateDocument/" + signature.FileID,
                    Title = "View",
                    IconClass = "icon-pencil"
                };
            };

        Func<LoadingSheetSignatureModel, TableAction> viewLoadingSheetSignatureAction = signature =>
        {
            return new TableAction()
            {
                Url = "/File/CreateDocument/" + signature.FileID,
                Title = "View",
                IconClass = "icon-pencil"
            };
        };

        #endregion

        #region Tables Overviews

        private PagedTable ShiftStaffOverviewTable(PagedResults<ShiftStaffModel> results)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name", "Agency", "Start Time", "End Time", "Reason Not On" },
                Rows = results.Data.Select(s => new TableRow 
                {
                    TableData = new string[] {
                        s.IsAgency ? s.AgencyEmployeeName : s.EmployeeName,
                        s.IsAgency ? "Yes" : "",
                        s.StartTime.HasValue ? s.StartTime.Value.ToString("MM/dd/yyyy H:mm") : "",
                        s.EndTime.HasValue ? s.EndTime.Value.ToString("MM/dd/yyyy H:mm") : "",
                        s.NotOnShift ? s.ReasonNotOnShift : "N/A"
                    }
                }).ToList()
            };

            return model;
        }

        private PagedTable NoteOverviewTable(PagedResults<NoteModel> results)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Comment", "Date" },
                Rows = results.Data.Select(s => new TableRow 
                {
                    TableData = new string[]{s.NoteDetail, s.NoteDate.ToString()}
                }).ToList()
            };

            return model;
        }

        private PagedTable VehicleChecklistAnswersOverviewTable(PagedResults<VehicleChecklistAnswersModel> results, List<Func<VehicleChecklistAnswersModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Question", "Vehicle", "Answer", "Defect" },
                Rows = results.Data.Select(s => new TableRow 
                {
                    TableData = new string[]{
                        s.QuestionOrder + ". " + s.Question, 
                        s.VehicleMake + " - " + s.VehicleRegistration, 
                        s.Answer.HasValue ? (s.Answer.Value == 0 ? "No" : s.Answer.Value == 1 ? "Yes" : "N/A") : "No Answer", 
                        s.Answer.HasValue ? s.VehicleChecklistDefectID.HasValue ? "Yes" : "No" : "Unknown"},
                    Actions = (s.VehicleChecklistDefectID.HasValue) ? actions.Select(a => a(s)).ToList() : null
                }).ToList()
            };

            return model;
        }

        private PagedTable VehicleChecklistPhotoOverviewTable(PagedResults<VehicleDefectPhotoModel> results, List<Func<VehicleDefectPhotoModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Date", "Longitude", "Latitude" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.FileName, s.Longitude.ToString(), s.Latitude.ToString() },
                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable MaintenanceCheckOverviewTable(PagedResults<ChecklistAnswerModel> results, List<Func<ChecklistAnswerModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Question", "Answer", "Time", "Detail" },
                Rows = results.Data.Select(s => new TableRow 
                {
                    TableData = new string[]{ 
                        s.QuestionOrder + ". " + s.ChecklistQuestion,
                        s.ChecklistAnswer != null ? (s.ChecklistAnswer == 1 ? "Yes" : "No") : "",
                        s.ChecklistTime.ToString(), 
                        s.ChecklistDetailID.HasValue ? "Yes" : "No" },
                    Actions = (s.ChecklistDetailID.HasValue) ? actions.Select(a => a(s)).ToList() : null
                }).ToList()
            };

            return model;
        }

        private PagedTable MaintenanceCheckPhotoOverviewTable(PagedResults<MaintenanceDetailPhotoModel> results, List<Func<MaintenanceDetailPhotoModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Date", "Longitude", "Latitude" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.FileName, s.Longitude.ToString(), s.Latitude.ToString() },
                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable BriefingSignatureOverviewTable(PagedResults<BriefingSignatureModel> results, List<Func<BriefingSignatureModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name", "Employee", "Start Time" },
                Rows = results.Data.Select(s => new TableRow 
                {
                    TableData = new string[]{s.BriefingName, s.EmployeeName, s.BriefingStart.ToString()},
                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable LoadingSheetSignatureOverviewTable(PagedResults<LoadingSheetSignatureModel> results, List<Func<LoadingSheetSignatureModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Number" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Number.ToString() },
                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        #endregion  

        #region Table action views

        public ActionResult ViewVehicleDefect(Guid id, Guid workInstructionID)
        {
            var defect = _workInstructionDetailsService.SingleVehicleDefect(id);

            VehicleDefectViewModel vdvm = new VehicleDefectViewModel() 
            {
                WorkInstructionID = workInstructionID,
                VehicleChecklistAnswerID = defect.VehicleChecklistAnswerID,
                VehicleChecklistDefectID = defect.VehicleChecklistDefectID,
                Question = defect.VehicleChecklistQuestion,
                Description = defect.Description,
            };

            return View(vdvm);
        }

        public ActionResult ViewMaintenanceDetail(Guid id)
        {
            var detail = _workInstructionDetailsService.SingleMaintenanceDetail(id);

            MaintenanceDetailViewModel mcvm = new MaintenanceDetailViewModel() 
            {
                WorkInstructionID = detail.WorkInstructionID,
                MaintenanceCheckAnswerID = detail.MaintenanceCheckAnswerID,
                MaintenanceCheckDetailID = detail.MaintenanceCheckDetailID,
                Description = detail.Description,
                Question = detail.MaintenanceQuestion
            };

            return View(mcvm);
        }

        public ActionResult ViewShiftVehicle(Guid id)
        {
            var shiftVehicle = _workInstructionDetailsService.SingleShiftVehicle(id);

            var vdvm = new ShiftVehicleViewModel
            {
                ShiftVehicleID = shiftVehicle.ShiftVehicleID,
                ShiftID = shiftVehicle.ShiftID,
                VehicleID = shiftVehicle.VehicleID,
                WorkInstructionID = shiftVehicle.WorkInstructionID,
                Vehicle = shiftVehicle.Vehicle,
                Registration = shiftVehicle.Registration,
                Driver = shiftVehicle.Driver,
                StartMileage = shiftVehicle.StartMileage,
                EndMileage = shiftVehicle.EndMileage,
                ReasonNotOn = shiftVehicle.ReasonNotOn
            };

            return View(vdvm);
        }

        #endregion

    }
}