﻿using FMConway.MVCApp.Areas.Contract.ViewModels.AreaCertificates;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.Services.Collections.Paging;
using FMConway.Services.AreaCertificates.Interfaces;
using FMConway.Services.AreaCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    public class AreaCertificateController : BaseController
    {
        IAreaCertificateService _areaCertificateService;

        public AreaCertificateController(IAreaCertificateService areaCertificateService)
        {
            _areaCertificateService = areaCertificateService;
        }

        #region Area Certificates table actions
        Func<AreaCertificateModel, TableAction> updateAction = areaCertificate =>
        {
            return new TableAction()
            {
                Url = "/Contract/AreaCertificate/Update/" + areaCertificate.AreaCertificateID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, AreaCertificates")]
        public ActionResult ActiveAreaCertificates(int rowCount, int page, string query, Guid employeeID)
        {
            PagedResults<AreaCertificateModel> results = _areaCertificateService.GetActiveAreaCertificatesPaged(rowCount, page, HttpUtility.HtmlEncode(query), employeeID);

            PagedTable model = AreaCertificateOverviewTable(results, new List<Func<AreaCertificateModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable AreaCertificateOverviewTable(PagedResults<AreaCertificateModel> results, List<Func<AreaCertificateModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name", "Certificate Type", "Expiry Date" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.CertificateName, s.CertificateType.Name, s.ExpiryDate.ToShortDateString() },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, AreaCertificates")]
        public ActionResult Create(Guid employeeID)
        {
            AreaCertificateViewModel model = new AreaCertificateViewModel()
            {
                AreaCertificate = new AreaCertificateModel()
                {
                    EmployeeID = employeeID,
                    ExpiryDate = DateTime.Today
                }
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, AreaCertificates")]
        [HttpPost]
        public ActionResult Create(AreaCertificateViewModel model, string cancel)
        {
            if (cancel == null)
            {
                List<AreaCertificateDocument> documents = TempData["SubmittedDocuments"] as List<AreaCertificateDocument>;

                if (documents == null)
                    documents = new List<AreaCertificateDocument>();

                if (model.UploadedDocuments != null)
                {
                    //Remove any documents that have been deleted
                    foreach (var document in documents.Reverse<AreaCertificateDocument>())
                    {
                        if (!model.UploadedDocuments.Any(ud => ud.Number.Equals(document.Number)))
                        {
                            documents.Remove(document);
                        }
                    }
                    //Delete the documents where the user has uploaded new files / or remove files.
                    foreach (var document in documents.Reverse<AreaCertificateDocument>())
                    {
                        if (model.UploadedDocuments.Where(ud => !ud.Loaded).Any(ud => ud.Number.Equals(document.Number)))
                            documents.Remove(document);
                    }
                    //Lets add the new documents
                    documents.AddRange(model.UploadedDocuments.Where(ud => !ud.Loaded));
                }
                documents.ForEach(doc => doc.Loaded = true);

                if (!ModelState.IsValid)
                {
                    TempData["SubmittedDocuments"] = documents;
                    model.UploadedDocuments = documents;

                    return View(model);
                }

                var areaCertificateID = _areaCertificateService.Create(model.AreaCertificate);

                if (documents != null)
                {
                    foreach (var document in documents)
                    {
                        byte[] documentData = new byte[document.Document.ContentLength];
                        document.Document.InputStream.Read(documentData, 0, documentData.Length);
                        _areaCertificateService.Documents.Create(areaCertificateID, document.Number, document.Document.FileName, document.Document.ContentType, documentData);
                    }
                }
            }

            return RedirectToAction("Update", "Employee", new { id = model.AreaCertificate.EmployeeID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, AreaCertificates")]
        public ActionResult Update(Guid id)
        {
            List<AreaCertificateDocument> documents = _areaCertificateService.Documents.GetActiveDocumentsForAreaCertificate(id).Select(s => new AreaCertificateDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Number = s.Title, Loaded = true }).ToList();
            
            AreaCertificateViewModel model = new AreaCertificateViewModel()
            {
                AreaCertificate = _areaCertificateService.Single(id),
                Documents = documents
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, AreaCertificates")]
        [HttpPost]
        public ActionResult Update(AreaCertificateViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                List<AreaCertificateDocument> currentDocuments = _areaCertificateService.Documents.GetActiveDocumentsForAreaCertificate(model.AreaCertificate.AreaCertificateID).Select(s => new AreaCertificateDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Number = s.Title, Loaded = true }).ToList();
                List<AreaCertificateDocument> documents = currentDocuments.ToList();

                #region documents
                
                if (model.Documents != null)
                {
                    //Remove any documents that have been deleted
                    foreach (var document in documents.Reverse<AreaCertificateDocument>())
                    {
                        if (!model.Documents.Any(ud => ud.Number.Equals(document.Number)))
                        {
                            documents.Remove(document);
                        }
                    }
                    //Delete the documents where the user has uploaded new files / or remove files.
                    foreach (var document in documents.Reverse<AreaCertificateDocument>())
                    {
                        if (model.Documents.Where(ud => !ud.Loaded).Any(ud => ud.Number.Equals(document.Number)))
                            documents.Remove(document);
                    }
                    //Lets add the new documents
                    documents.AddRange(model.Documents.Where(ud => !ud.Loaded));
                }
                else
                {
                    documents.Clear();
                }

                documents.ForEach(doc => doc.Loaded = true);
                #endregion

                if (!ModelState.IsValid)
                {
                    int index = 0;
                    documents.ForEach(d => { ModelState.Remove(string.Format("Documents[{0}].Loaded", index)); index++; });

                    model.Documents = documents;

                    return View(model);
                }
                _areaCertificateService.Update(model.AreaCertificate);

                #region Documents
                if (documents.Any())
                {
                    //Add new Documents
                    foreach (var document in documents.Where(d => !d.DocumentID.HasValue))
                    {
                        byte[] documentData = new byte[document.Document.ContentLength];
                        document.Document.InputStream.Read(documentData, 0, documentData.Length);
                        _areaCertificateService.Documents.Create(model.AreaCertificate.AreaCertificateID, document.Number, document.Document.FileName, document.Document.ContentType, documentData);
                    }

                    //Delete removed docs
                    var docsToDelete = currentDocuments.Where(cd => !documents.Where(d => d.DocumentID.HasValue).Any(d => d.DocumentID.Value.Equals(cd.DocumentID.Value))).ToList();
                    docsToDelete.ForEach(d => _areaCertificateService.Documents.Inactivate(d.DocumentID.Value));
                }
                else if (currentDocuments != null & currentDocuments.Any())
                {
                    currentDocuments.ForEach(d => _areaCertificateService.Documents.Inactivate(d.DocumentID.Value));
                }
                #endregion
            }
            return RedirectToAction("Update", "Employee", new { id = model.AreaCertificate.EmployeeID });
        }
    }
}
