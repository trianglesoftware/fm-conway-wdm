﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Contracts;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Contracts.Interfaces;
using FMConway.Services.Contracts.Models;
using FMConway.Services.Maintenance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Contracts", 20, "fa fa-rectangle-list", "Administrator, Supervisor, Manager, Contracts")]
    public class ContractController : BaseController
    {
        IContractService _contractService;
        IMaintenanceService _maintenanceService;

        public ContractController(IContractService contractService, IMaintenanceService maintenanceService)
        {
            _contractService = contractService;
            _maintenanceService = maintenanceService;
        }

        #region Contact table actions
        Func<ContractModel, TableAction> updateAction = contract =>
        {
            return new TableAction()
            {
                Url = "/Contract/Contract/Update/" + contract.ID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<ContractModel, TableAction> viewAction = contract =>
        {
            return new TableAction()
            {
                Url = "/Contract/Contract/Summary/" + contract.ID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        public ActionResult ActiveContracts(int rowCount, int page, string query)
        {
            PagedResults<ContractModel> results = _contractService.GetActiveContractsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = ContractOverviewTable(results, new List<Func<ContractModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        public ActionResult InactiveContracts(int rowCount, int page, string query)
        {
            PagedResults<ContractModel> results = _contractService.GetInactiveContractsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = ContractOverviewTable(results, new List<Func<ContractModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable ContractOverviewTable(PagedResults<ContractModel> results, List<Func<ContractModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Contract Number", "Contract Title", "Customer name", "Date Awarded", "Expiry Date" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.ContractNumber, s.ContractTitle, s.Customer.CustomerName, s.DateAwarded?.ToShortDateString(), s.DateExpires?.ToShortDateString() },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            List<SelectListItem> contractStatus = new List<SelectListItem>();
            contractStatus.Add(new SelectListItem() { Text = "" });

            List<SelectListItem> contractType = new List<SelectListItem>();
            contractType.Add(new SelectListItem() { Text = "" });

            List<SelectListItem> billingRequirements = new List<SelectListItem>();
            billingRequirements.Add(new SelectListItem() { Text = "" });

            ContractViewModel model = new ContractViewModel
            {
                Contract = new ContractModel
                {
                    IsCleansingLogMandatory = true
                },

                ContractStatus = contractStatus.Union(_contractService.Status.GetContractStatus().Select(x => new SelectListItem
                {
                    Text = x.Status,
                    Value = x.StatusID.ToString()
                })),

                ContractType = contractType.Union(_contractService.ContractType.GetContractType().Select(x => new SelectListItem
                {
                    Text = x.ContractTypeDescription,
                    Value = x.ContractTypeID.ToString()
                })),

                BillingRequirements = billingRequirements.Union(_contractService.BillingRequirements.GetBillingRequirements().Select(x => new SelectListItem
                {
                    Text = x.BillingRequirement,
                    Value = x.ID.ToString()
                }))
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        [HttpPost]
        public ActionResult New(ContractViewModel model)
        {
            List<ContractDocument> documents = TempData["SubmittedDocuments"] as List<ContractDocument>;

            if (documents == null)
                documents = new List<ContractDocument>();

            if (model.UploadedDocuments != null)
            {
                //Remove any documents that have been deleted
                foreach (var document in documents.Reverse<ContractDocument>())
                {
                    if (!model.UploadedDocuments.Any(ud => ud.Number.Equals(document.Number)))
                    {
                        documents.Remove(document);
                    }
                }
                //Delete the documents where the user has uploaded new files / or remove files.
                foreach (var document in documents.Reverse<ContractDocument>())
                {
                    if (model.UploadedDocuments.Where(ud => !ud.Loaded).Any(ud => ud.Number.Equals(document.Number)))
                        documents.Remove(document);
                }
                //Lets add the new documents
                documents.AddRange(model.UploadedDocuments.Where(ud => !ud.Loaded));
            }
            documents.ForEach(doc => doc.Loaded = true);

            if (!ModelState.IsValid)
            {
                TempData["SubmittedDocuments"] = documents;
                model.UploadedDocuments = documents;

                List<SelectListItem> contractStatus = new List<SelectListItem>();
                contractStatus.Add(new SelectListItem() { Text = "" });

                model.ContractStatus = contractStatus.Union(_contractService.Status.GetContractStatus().Select(x => new SelectListItem
                {
                    Text = x.Status,
                    Value = x.StatusID.ToString(),
                    Selected = model.Contract.StatusID != null && model.Contract.StatusID == x.StatusID ? true : false
                }));

                List<SelectListItem> contractType = new List<SelectListItem>();
                contractType.Add(new SelectListItem() { Text = "" });

                model.ContractType = contractType.Union(_contractService.ContractType.GetContractType().Select(x => new SelectListItem
                {
                    Text = x.ContractTypeDescription,
                    Value = x.ContractTypeID.ToString(),
                    Selected = model.Contract.ContractTypeID != null && model.Contract.ContractTypeID == x.ContractTypeID ? true : false
                }));

                List<SelectListItem> billingRequirements = new List<SelectListItem>();
                billingRequirements.Add(new SelectListItem() { Text = "" });

                model.BillingRequirements = billingRequirements.Union(_contractService.BillingRequirements.GetBillingRequirements().Select(x => new SelectListItem
                {
                    Text = x.BillingRequirement,
                    Value = x.ID.ToString(),
                    Selected = model.Contract.BillingRequirementID != null && model.Contract.BillingRequirementID == x.ID ? true : false
                }));
                return View(model);
            }

            Guid contractID = _contractService.Create(model.Contract/*, model.TenderChecklists*/);

            if (documents != null)
            {
                foreach (var document in documents)
                {
                    byte[] documentData = new byte[document.Document.ContentLength];
                    document.Document.InputStream.Read(documentData, 0, documentData.Length);
                    _contractService.Documents.Create(contractID, document.Number, document.Document.FileName, document.Document.ContentType, documentData);
                }
            }

            return RedirectToAction("Update", new { id = contractID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        public ActionResult Update(Guid id)
        {
            List<ContractDocument> documents = _contractService.Documents.GetActiveDocumentsForContract(id).Select(s => new ContractDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Number = s.DocumentNumber, Loaded = true }).ToList();
            
            var currentContract = _contractService.Single(id);
            ContractViewModel model = new ContractViewModel()
            {
                Contract = currentContract,
                ContractStatus = _contractService.Status.GetContractStatus().ToList().Select(x => new SelectListItem
                {
                    Text = x.Status,
                    Value = x.StatusID.ToString(),
                    Selected = x.StatusID.Equals(currentContract.StatusID)
                }),

                ContractType = _contractService.ContractType.GetContractType().ToList().Select(x => new SelectListItem
                {
                    Text = x.ContractTypeDescription,
                    Value = x.ContractTypeID.ToString(), 
                    Selected = x.ContractTypeID.Equals(currentContract.ContractTypeID)
                }),

                BillingRequirements = _contractService.BillingRequirements.GetBillingRequirements().ToList().Select(x => new SelectListItem
                {
                    Text = x.BillingRequirement,
                    Value = x.ID.ToString(),
                    Selected = x.ID.Equals(currentContract.BillingRequirementID)
                }),
                Documents = documents,
                //TenderChecklists = tenderChecklists
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        [HttpPost]
        public ActionResult Update(ContractViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                //Dictionary<Guid, string> tenderChecklists = TempData["TenderChecklists"] as Dictionary<Guid, string>;
                List<ContractDocument> currentDocuments = _contractService.Documents.GetActiveDocumentsForContract(model.Contract.ID).Select(s => new ContractDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Number = s.DocumentNumber, Loaded = true }).ToList();
                List<ContractDocument> documents = currentDocuments.ToList();

                #region documents
                
                if (model.Documents != null)
                {
                    //Remove any documents that have been deleted
                    foreach (var document in documents.Reverse<ContractDocument>())
                    {
                        if (!model.Documents.Any(ud => ud.Number.Equals(document.Number)))
                        {
                            documents.Remove(document);
                        }
                    }
                    //Delete the documents where the user has uploaded new files / or remove files.
                    foreach (var document in documents.Reverse<ContractDocument>())
                    {
                        if (model.Documents.Where(ud => !ud.Loaded).Any(ud => ud.Number.Equals(document.Number)))
                            documents.Remove(document);
                    }
                    //Lets add the new documents
                    documents.AddRange(model.Documents.Where(ud => !ud.Loaded));
                }
                else
                {
                    documents.Clear();
                }

                documents.ForEach(doc => doc.Loaded = true);
                #endregion

                if (!ModelState.IsValid)
                {                    
                    int index = 0;
                    documents.ForEach(d => { ModelState.Remove(string.Format("Documents[{0}].Loaded", index)); index++; });

                    model.Documents = documents;
                    
                    model.ContractStatus = _contractService.Status.GetContractStatus().Select(x => new SelectListItem
                    {
                        Text = x.Status,
                        Value = x.StatusID.ToString()
                    });

                    model.ContractType = _contractService.ContractType.GetContractType().Select(x => new SelectListItem
                    {
                        Text = x.ContractTypeDescription,
                        Value = x.ContractTypeID.ToString()
                    });

                    model.BillingRequirements = _contractService.BillingRequirements.GetBillingRequirements().Select(x => new SelectListItem
                    {
                        Text = x.BillingRequirement,
                        Value = x.ID.ToString()
                    });

                    return View(model);
                }
                _contractService.Update(model.Contract/*, model.TenderChecklists, tenderChecklists*/);

                #region Documents
                if (documents.Any())
                {
                    //Add new Documents
                    foreach (var document in documents.Where(d => !d.DocumentID.HasValue))
                    {
                        byte[] documentData = new byte[document.Document.ContentLength];
                        document.Document.InputStream.Read(documentData, 0, documentData.Length);
                        _contractService.Documents.Create(model.Contract.ID, document.Number, document.Document.FileName, document.Document.ContentType, documentData);
                    }

                    //Delete removed docs
                    var docsToDelete = currentDocuments.Where(cd => !documents.Where(d => d.DocumentID.HasValue).Any(d => d.DocumentID.Value.Equals(cd.DocumentID.Value))).ToList();
                    docsToDelete.ForEach(d => _contractService.Documents.Inactivate(d.DocumentID.Value));
                }
                else if (currentDocuments != null & currentDocuments.Any())
                {
                    currentDocuments.ForEach(d => _contractService.Documents.Inactivate(d.DocumentID.Value));
                }
                #endregion
            }
            return RedirectToAction("Update", new { id = model.Contract.ID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        public ActionResult Summary(Guid id)
        {
            List<ContractDocument> documents = _contractService.Documents.GetActiveDocumentsForContract(id).Select(s => new ContractDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Number = s.DocumentNumber, Loaded = true }).ToList();
            //Dictionary<Guid, string> tenderChecklists = _maintenanceService.Checklists.Get(id).OrderBy(s => s.Checklist.ChecklistName).ToDictionary(k => k.ChecklistID, v => v.Checklist.ChecklistName);
            
            ContractViewModel model = new ContractViewModel()
            {
                Contract = _contractService.Single(id),
                Documents = documents,
                //TenderChecklists = tenderChecklists
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Contracts")]
        [HttpPost]
        public ActionResult Summary()
        {
            return RedirectToAction("Active");
        }

        public ActionResult GetContractLookup(int rowCount, int page, string query)
        {
            var lookup = _contractService.GetPagedActiveContractsLookup(rowCount, page, query);

            var model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[]
                {
                    "Number",
                    "Title"
                },
                Rows = lookup.Data.Select(s => new LookupRow()
                {
                    Key = s.Key,
                    Value = s.Value,
                    TableData = new string[]
                    {
                        s.Key,
                        s.ContractTitle
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetContractSchemeLookup(int rowCount, int page, string query, Guid? customerID)
        {
            var lookup = _contractService.GetPagedActiveContractSchemesLookup(rowCount, page, query, customerID);

            var model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[]
                {
                    "Contract", "Phase"
                },
                Rows = lookup.Data.Select(s => new LookupRow()
                {
                    Key = s.Key,
                    Value = s.Value,
                    TableData = new string[]
                    {
                        s.Contract,
                        s.Scheme
                    },
                    Data = new string[] { s.SchemeID.HasValue ? s.SchemeID.ToString() : "" }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);

        }

    }
}
