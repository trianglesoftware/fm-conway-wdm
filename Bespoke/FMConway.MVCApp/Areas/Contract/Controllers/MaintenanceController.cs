﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.CertificateTypes;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.Checklists;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.EquipmentTypes;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.ReasonCodes;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.JobTypes;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.LoadingSheetTemplates;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.MedicalExaminationTypes;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.PriceLines;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.Users;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.VehicleTypes;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.MVCApp.Utility;
using FMConway.Services.CertificateTypes.Models;
using FMConway.Services.Checklists.Models;
using FMConway.Services.Collections.Paging;
using FMConway.Services.EquipmentTypes.Models;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.LoadingSheetTemplates.Models;
using FMConway.Services.Maintenance;
using FMConway.Services.MedicalExaminationTypes.Models;
using FMConway.Services.UserProfiles.Models;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Triangle.Membership.Interface;
using Triangle.Membership.Services.Sql;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.EmployeeAbsenceReasons;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.VehicleAbsenceReasons;
using FMConway.MVCApp.Extensions;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.WeatherConditions;
using System.Data.Entity;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.DisposalSites;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Authorize(Roles = "Administrator, Supervisor, Manager")]
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Maintenance", 1200, "fa fa-gears", "Manager")]
    public class MaintenanceController : BaseController
    {
        IMaintenanceService _maintenanceService;
        ILoadingSheetService _loadingSheetService;
        IUserService _userService;
        IErrorLoggingService _errorLoggingService;

        public MaintenanceController(IMaintenanceService maintenanceService, IUserService userService, ILoadingSheetService loadingSheetService, IErrorLoggingService errorLoggingService)
        {
            _maintenanceService = maintenanceService;
            _loadingSheetService = loadingSheetService;
            _userService = userService;
            _errorLoggingService = errorLoggingService;
        }

        #region System Users

        [NavigationMenuItem(10)]
        [Authorize(Roles = "Manager")]
        public ActionResult SystemUsers()
        {
            List<SqlUser> users = _userService.GetAllUsers().Cast<SqlUser>().ToList();

            var model = new SystemUserModel()
            {
                Users = users
            };

            return View(model);
        }

        #region system user actions

        Func<SqlUser, TableAction> systemUserViewAction = user =>
        {
            return new TableAction()
            {
                Url = "/Contract/Maintenance/UpdateSystemUser/" + user.ID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        #endregion

        public ActionResult ActiveSystemUsers(int rowCount, int page, string query)
        {
            var pageCount = 0;
            var results = _userService.GetActiveUsers(rowCount, page, query, out pageCount).Cast<SqlUser>().ToList();
            return PagedSystemUsers(results, page, pageCount);
        }

        public ActionResult InactiveSystemUsers(int rowCount, int page, string query)
        {
            var pageCount = 0;
            var results = _userService.GetUsersRequiringApproval(rowCount, page, query, out pageCount).Cast<SqlUser>().ToList();
            return PagedSystemUsers(results, page, pageCount);
        }

        public ActionResult LockedOutSystemUsers(int rowCount, int page, string query)
        {
            var pageCount = 0;
            var results = _userService.GetLockedOutUsers(rowCount, page, query, out pageCount).Cast<SqlUser>().ToList();
            return PagedSystemUsers(results, page, pageCount);
        }

        private ActionResult PagedSystemUsers(IEnumerable<SqlUser> results, int page, int pageCount)
        {
            PagedTable model = new PagedTable()
            {
                Page = page,
                PageCount = pageCount,
                ColumnHeaders = new string[] { "User Name", "Role", "Email", "Last Login Date", "Last Password Changed Date" },
                Rows = results.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.UserName,
                        s.RoleLookup,
                        s.Email,
                        !s.LastLoginDate.HasValue ? "" : s.LastLoginDate.Value.ToShortDateString(),
                        !s.LastPasswordChangedDate.HasValue ? "" : s.LastPasswordChangedDate.Value.ToShortDateString(),
                    },
                    Actions = new List<TableAction>() { systemUserViewAction(s) }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewSystemUser()
        {
            var viewModel = new NewSystemUserModel();

            return View(viewModel);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewSystemUser(NewSystemUserModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.User.UserName))
            {
                if (_userService.UserNameExists(model.User.UserName))
                {
                    ModelState.AddModelError("User.UserName", "The user name '" + model.User.UserName + "' already exists.");
                }
            }

            if (!string.IsNullOrWhiteSpace(model.User.Email))
            {
                if (!EmailUtility.ValidateEmail(model.User.Email))
                {
                    ModelState.AddModelError("User.Email", "Please provide a valid email.");
                }
            }

            if (model.RoleLookup == "Customer")
            {
                if (model.CustomerID == null)
                {
                    ModelState.AddModelError("CustomerName", "Customer is a required field.");
                }
                else if (!_maintenanceService.UserProfiles.IsCustomerAvailable((Guid)model.CustomerID, model.User.ID))
                {
                    ModelState.AddModelError("CustomerName", "Customer is already assigned to another user.");
                }
            }
            else
            {
                if (model.EmployeeID == null)
                {
                    ModelState.AddModelError("EmployeeName", "Employee is a required field.");
                }
                else if (!_maintenanceService.UserProfiles.IsEmployeeAvailable((Guid)model.EmployeeID, model.User.ID))
                {
                    ModelState.AddModelError("EmployeeName", "Employee is already assigned to another user.");
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var newUser = _userService.Add(model.User, model.Password, model.ConfirmationPassword);

                    var userProfile = new UserProfileModel
                    {
                        UserID = newUser.ID,
                        EmployeeID = model.EmployeeID,
                        CustomerID = model.CustomerID
                    };

                    _maintenanceService.UserProfiles.CreateUserProfile(userProfile);

                    Roles.AddUserToRole(model.User.UserName, model.RoleLookup);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", $"An error has occurred - {ex.Message}.");
                    _errorLoggingService.CreateErrorLog(nameof(NewSystemUser), ex, null);

                    return View(model);
                }

                return RedirectToAction("SystemUsers");
            }

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateSystemUser(Guid id)
        {
            SqlUser user = _userService.GetUserByID(id) as SqlUser;
            UserProfileModel userProfile = _maintenanceService.UserProfiles.GetUserProfile(id);

            var viewModel = new UpdateSystemUserModel();
            viewModel.User = user;
            viewModel.RoleID = user.RoleID;
            viewModel.RoleLookup = user.RoleLookup;
            viewModel.OriginalRoleID = user.RoleID;
            viewModel.OriginalRoleLookup = user.RoleLookup;
            viewModel.EmployeeID = userProfile.EmployeeID;
            viewModel.EmployeeName = userProfile.Employee?.EmployeeName;
            viewModel.CustomerID = userProfile.CustomerID;
            viewModel.CustomerName = userProfile.Customer?.CustomerName;

            return View(viewModel);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateSystemUser(UpdateSystemUserModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.User.Email))
            {
                if (!EmailUtility.ValidateEmail(model.User.Email))
                {
                    ModelState.AddModelError("User.Email", "Please provide a valid email.");
                }
            }

            if (model.RoleLookup == "Customer")
            {
                if (model.CustomerID == null)
                {
                    ModelState.AddModelError("CustomerName", "Customer is a required field.");
                }
                else if (!_maintenanceService.UserProfiles.IsCustomerAvailable((Guid)model.CustomerID, model.User.ID))
                {
                    ModelState.AddModelError("CustomerName", "Customer is already assigned to another user.");
                }
            }
            else
            {
                if (model.EmployeeID == null)
                {
                    ModelState.AddModelError("EmployeeName", "Employee is a required field.");
                }
                else if (!_maintenanceService.UserProfiles.IsEmployeeAvailable((Guid)model.EmployeeID, model.User.ID))
                {
                    ModelState.AddModelError("EmployeeName", "Employee is already assigned to another user.");
                }
            }

            if (!string.IsNullOrWhiteSpace(model.Password) && !string.IsNullOrWhiteSpace(model.ConfirmPassword) && model.Password != model.ConfirmPassword)
            {
                ModelState.AddModelError("User.Password", "Please ensure the passwords match.");
            }

            if ((model.OriginalRoleLookup != "Customer" && model.RoleLookup == "Customer") || (model.OriginalRoleLookup == "Customer" && model.RoleLookup != "Customer"))
            {
                ModelState.AddModelError("RoleID", "Users cannot switch between employee and customer roles.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userProfile = new UserProfileModel
                    {
                        UserID = model.User.ID,
                        EmployeeID = model.EmployeeID,
                        CustomerID = model.CustomerID
                    };

                    _maintenanceService.UserProfiles.UpdateUserProfile(userProfile);

                    if (model.RoleID != model.OriginalRoleID)
                    {
                        if (model.OriginalRoleID != Guid.Empty)
                        {
                            Roles.RemoveUserFromRole(model.User.UserName, model.OriginalRoleLookup);
                        }

                        Roles.AddUserToRole(model.User.UserName, model.RoleLookup);
                    }

                    if (!string.IsNullOrWhiteSpace(model.Password) && !string.IsNullOrWhiteSpace(model.ConfirmPassword) && model.Password == model.ConfirmPassword)
                    {
                        _userService.ChangePassword(model.User.UserName, model.Password);
                    }

                    _userService.Save(model.User);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", $"An error has occurred - {ex.Message}.");
                    _errorLoggingService.CreateErrorLog(nameof(UpdateSystemUser), ex, model.User?.ID.ToString());

                    return View(model);
                }

                return RedirectToAction("SystemUsers");
            }

            return View(model);
        }

        public ActionResult GetRoles(bool excludeCustomer = false)
        {
            var roles = _maintenanceService.UserProfiles.GetAllRoles(excludeCustomer);

            LookupModel model = new LookupModel
            {
                Page = 0,
                PageCount = 0,
                ColumnHeaders = new string[] { "Role" },
                Rows = roles.OrderBy(o => o.Name).Select(r => new LookupRow
                {
                    Key = r.Name,
                    Value = r.RoleID,
                    TableData = new string[]
                    {
                        r.Name
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Checklists

        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Checklists", 20)]
        public ActionResult Checklists()
        {
            var model = new ChecklistViewModel();
            model.Active = _maintenanceService.Checklists.GetActiveChecklists();
            model.Inactive = _maintenanceService.Checklists.GetInactiveChecklists();

            Func<ChecklistModel, string> GetChecklistType = a =>
            {
                return
                    a.IsContractChecklist ? "Contract" :
                    a.IsJobTypeChecklist ? "Job Type" :
                    a.IsMaintenanceChecklist ? "Maintenance" :
                    a.IsVehicleTypeChecklist ? "Vehicle Type" :
                    a.IsSignatureChecklist ? "Signature" :
                    a.IsHighSpeedChecklist ? "12AB Works" :
                    a.IsLowSpeedChecklist ? "12D Works" :
                    a.IsPreHireChecklist ? "Pre Hire" :
                    a.IsVmsChecklist ? "Vms" :
                    a.Is12cChecklist ? "12C Works" :
                    a.IsBatteryChecklist ? "Battery" :
                    a.IsPreHireBatteryChecklist ? "PreHireBattery" :
                    a.IsPostHireBatteryChecklist ? "PostHireBattery" :
                    a.IsJettingPermitToWorkChecklist ? "Jetting Permit To Work" :
                    a.IsJettingRiskAssessmentChecklist ? "Jetting Risk Assessment" :
                    a.IsTankeringPermitToWorkChecklist ? "Tankering Permit To Work" :
                    a.IsTankeringRiskAssessmentChecklist ? "Tankering Risk Assessment" :
                    a.IsCCTVRiskAssessmentChecklist ? "CCTV Risk Assessment" :
                    a.IsCyclicalWorksRiskAssessmentChecklist ? "Cyclical Works Risk Assessment" :
                    null;
            };

            foreach (var item in model.Active)
            {
                item.ChecklistType = GetChecklistType(item);
            }

            foreach (var item in model.Inactive)
            {
                item.ChecklistType = GetChecklistType(item);
            }

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewChecklist()
        {
            var model = new ChecklistModel();
            return View(model);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewChecklist(ChecklistModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = _maintenanceService.Checklists.Create(model);
            if (result.Errors.Any())
            {
                result.Errors.ForEach(a => ModelState.AddModelError(a.Key, a.Error));
                return View(model);
            }

            return RedirectToAction("UpdateChecklist", new { id = result.Result });
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateChecklist(Guid id)
        {
            var model = new UpdateChecklistViewModel();
            model.Checklist = _maintenanceService.Checklists.Single(id);

            model.Checklist.ChecklistType =
                model.Checklist.IsContractChecklist ? "Contract" :
                model.Checklist.IsJobTypeChecklist ? "JobType" :
                model.Checklist.IsVehicleTypeChecklist ? "VehicleType" :
                model.Checklist.IsMaintenanceChecklist ? "Maintenance" :
                model.Checklist.IsSignatureChecklist ? "Signature" :
                model.Checklist.IsHighSpeedChecklist ? "HighSpeed" :
                model.Checklist.IsLowSpeedChecklist ? "LowSpeed" :
                model.Checklist.IsPreHireChecklist ? "PreHire" :
                model.Checklist.IsVmsChecklist ? "Vms" :
                model.Checklist.Is12cChecklist ? "12C" :
                model.Checklist.IsBatteryChecklist ? "Battery" :
                model.Checklist.IsPreHireBatteryChecklist ? "PreHireBattery" :
                model.Checklist.IsPostHireBatteryChecklist ? "PostHireBattery" :
                model.Checklist.IsJettingPermitToWorkChecklist ? "JettingPermitToWork" :
                model.Checklist.IsJettingRiskAssessmentChecklist ? "JettingRiskAssessment" :
                model.Checklist.IsTankeringPermitToWorkChecklist ? "TankeringPermitToWork" :
                model.Checklist.IsTankeringRiskAssessmentChecklist ? "TankeringRiskAssessment" :
                model.Checklist.IsCCTVRiskAssessmentChecklist ? "CCTVRiskAssessment" :
                model.Checklist.IsCyclicalWorksRiskAssessmentChecklist ? "CyclicalWorksRiskAssessment" :
                null;

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateChecklist(UpdateChecklistViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var results = _maintenanceService.Checklists.Update(model.Checklist);
            if (results.Errors.Any())
            {
                results.Errors.ForEach(a => ModelState.AddModelError(a.Key, a.Error));
                return View(model);
            }

            return RedirectToAction("Checklists");
        }

        #region TableActions
        Func<ChecklistQuestionModel, TableAction> updateAction = cc =>
        {
            return new TableAction()
            {
                Url = "/Contract/Maintenance/UpdateChecklistQuestion/" + cc.ID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };
        #endregion

        public ActionResult ActiveQuestions(int rowCount, int page, string query, Guid checklistId)
        {
            PagedResults<ChecklistQuestionModel> results = _maintenanceService.Checklists.GetActiveChecklistQuestions(rowCount, page, query, checklistId);

            PagedTable model = QuestionOverviewTable(results, new List<Func<ChecklistQuestionModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable QuestionOverviewTable(PagedResults<ChecklistQuestionModel> results, List<Func<ChecklistQuestionModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Question Number", "Question" },
                Rows = results.Data.Select(s => new TableRow
                {
                    Data = new string[] { !s.IsActive ? "inactive" : "" },
                    TableData = new string[] { s.QuestionOrder.ToString(), s.ChecklistQuestion },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        public ActionResult NewChecklistQuestion(Guid id)
        {
            var model = new ChecklistQuestionModel();
            model.ChecklistID = id;
            model.QuestionOrder = _maintenanceService.Checklists.GetNextQuestionOrderNumber(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult NewChecklistQuestion(ChecklistQuestionModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Guid checklistQuestion = _maintenanceService.Checklists.CreateChecklistQuestion(model);
            }

            return RedirectToAction("UpdateChecklist", "Maintenance", new { id = model.ChecklistID });
        }

        public ActionResult UpdateChecklistQuestion(Guid id)
        {
            ChecklistQuestionViewModel model = new ChecklistQuestionViewModel()
            {
                 ChecklistQuestion = _maintenanceService.Checklists.SingleQuestion(id),
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateChecklistQuestion(ChecklistQuestionViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _maintenanceService.Checklists.UpdateQuestion(model.ChecklistQuestion);
            }

            return RedirectToAction("UpdateChecklist", "Maintenance", new { id = model.ChecklistQuestion.ChecklistID });
        }

        public ActionResult GetTenderChecklistLookup(int rowCount, int page, string query)
        {
            PagedResults<ChecklistModel> lookup = _maintenanceService.Checklists.GetActiveTenderChecklists(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Checklists" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.ChecklistName, Value = s.ChecklistID, Data = new string[] { s.ChecklistName }, TableData = new string[] { s.ChecklistName } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJobTypeChecklistLookup(int rowCount, int page, string query)
        {
            PagedResults<ChecklistModel> lookup = _maintenanceService.Checklists.GetActiveJobTypeChecklists(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Checklists" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.ChecklistName, Value = s.ChecklistID, Data = new string[] { s.ChecklistName }, TableData = new string[] { s.ChecklistName } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleTypeChecklistLookup(int rowCount, int page, string query)
        {
            PagedResults<ChecklistModel> lookup = _maintenanceService.Checklists.GetActiveVehicleTypeChecklists(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Checklists" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.ChecklistName, Value = s.ChecklistID, Data = new string[] { s.ChecklistName }, TableData = new string[] { s.ChecklistName } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChecklistsForJobType(List<string> jobTypeID)
        {
            List<ChecklistModel> checklists = new List<ChecklistModel>();
            foreach (var id in jobTypeID)
            {
                checklists.AddRange(_maintenanceService.Checklists.GetJobTypeChecklists(new Guid(id)).OrderBy(s => s.Checklist.ChecklistName).Select(x => new ChecklistModel()
                {
                    ChecklistID = x.ChecklistID,
                    ChecklistName = x.Checklist.ChecklistName
                }).ToList());
            }
            return Json(checklists, JsonRequestBehavior.AllowGet);
        }        

        #endregion

        #region Job Types

        //[NavigationMenuItem("Job Types", 30)]
        public ActionResult JobTypes()
        {
            JobTypeViewModel model = new JobTypeViewModel()
            {
                Active = _maintenanceService.JobTypes.GetActiveJobTypes(),
                Inactive = _maintenanceService.JobTypes.GetInactiveJobTypes()
            };


            return View(model);
        }

        public ActionResult NewJobType()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewJobType(UpdateJobTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.JobTypes.Create(model.JobType, model.Checklists);

            return RedirectToAction("JobTypes");
        }

        public ActionResult UpdateJobType(Guid id)
        {
            Dictionary<Guid, string> checklists = _maintenanceService.Checklists.GetJobTypeChecklists(id).OrderBy(s => s.Checklist.ChecklistName).ToDictionary(k => k.ChecklistID, v => v.Checklist.ChecklistName);
            TempData["Checklists"] = checklists;

            JobTypeModel model = _maintenanceService.JobTypes.Single(id);

            var uvr = new UpdateJobTypeViewModel()
            {
                JobType = model,
                Checklists = checklists
            };

            return View(uvr);
        }

        [HttpPost]
        public ActionResult UpdateJobType(UpdateJobTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            Dictionary<Guid, string> checklists = TempData["Checklists"] as Dictionary<Guid, string>;

            _maintenanceService.JobTypes.Update(model.JobType, model.Checklists, checklists);

            return RedirectToAction("JobTypes");
        }

        public ActionResult GetJobTypesLookup(int rowCount, int page, string query)
        {
            PagedResults<JobTypeLookup> lookup = _maintenanceService.JobTypes.GetPagedActiveJobTypesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "JobType" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Vehicle Types

        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Vehicle Types", 40)]
        public ActionResult VehicleTypes()
        {
            VehicleTypeViewModel model = new VehicleTypeViewModel()
            {
                Active = _maintenanceService.VehicleTypes.GetActiveVehicleTypes(),
                Inactive = _maintenanceService.VehicleTypes.GetInactiveVehicleTypes()
            };


            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewVehicleType()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewVehicleType(UpdateVehicleTypeViewModel model)
        {

            if (string.IsNullOrEmpty(model.VehicleType.Name))
            {
                ModelState.AddModelError("VehicleType.Name", "Name is required");
            }

            //if (model.Checklists == null || model.Checklists.Count != 1)
            //{
            //    ModelState.AddModelError("Checklists", "Each vehicle type must have exactly one associated checklist");
            //}

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _maintenanceService.VehicleTypes.Create(model.VehicleType, model.Checklists);

            return RedirectToAction("VehicleTypes");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateVehicleType(Guid id)
        {
            Dictionary<Guid, string> checklists = _maintenanceService.Checklists.GetVehicleTypeChecklists(id).OrderBy(s => s.Checklist.ChecklistName).ToDictionary(k => k.ChecklistID, v => v.Checklist.ChecklistName);
            TempData["Checklists"] = checklists;

            VehicleTypeModel model = _maintenanceService.VehicleTypes.Single(id);

            var uvr = new UpdateVehicleTypeViewModel()
            {
                VehicleType = model,
                Checklists = checklists
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateVehicleType(UpdateVehicleTypeViewModel model)
        {
            //if (model.Checklists == null || model.Checklists.Count != 1)
            //{
            //    ModelState.AddModelError("Checklists", "Each vehicle type must have exactly one associated checklist");
            //}

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Dictionary<Guid, string> checklists = TempData["Checklists"] as Dictionary<Guid, string>;

            _maintenanceService.VehicleTypes.Update(model.VehicleType, model.Checklists, checklists);

            return RedirectToAction("VehicleTypes");
        }

        public ActionResult GetVehicleTypesLookup(int rowCount, int page, string query)
        {
            PagedResults<VehicleTypeLookup> lookup = _maintenanceService.VehicleTypes.GetPagedActiveVehicleTypesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Vehicle Type" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Equipment Types

        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Equipment Types", 50)]
        public ActionResult EquipmentTypes()
        {
            EquipmentTypeViewModel model = new EquipmentTypeViewModel()
            {
                Active = _maintenanceService.EquipmentTypes.GetActiveEquipmentTypes(),
                Inactive = _maintenanceService.EquipmentTypes.GetInactiveEquipmentTypes()
            };


            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewEquipmentType()
        {
            var model = new UpdateEquipmentTypeViewModel();
            model.VmsOrAssets = _maintenanceService.EquipmentTypes.GetVmsOrAssets().ToSelectList();

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewEquipmentType(UpdateEquipmentTypeViewModel model)
        {

            if (string.IsNullOrEmpty(model.EquipmentType.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }

            if (!ModelState.IsValid)
            {
                model.VmsOrAssets = _maintenanceService.EquipmentTypes.GetVmsOrAssets().ToSelectList();

                return View(model);
            }

            _maintenanceService.EquipmentTypes.Create(model.EquipmentType);

            return RedirectToAction("EquipmentTypes");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateEquipmentType(Guid id)
        {
            var model = new UpdateEquipmentTypeViewModel();
            model.EquipmentType = _maintenanceService.EquipmentTypes.Single(id);
            model.VmsOrAssets = _maintenanceService.EquipmentTypes.GetVmsOrAssets().ToSelectList(optionLabel: "");

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateEquipmentType(UpdateEquipmentTypeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.VmsOrAssets = _maintenanceService.EquipmentTypes.GetVmsOrAssets().ToSelectList();

                return View(model);
            }

            _maintenanceService.EquipmentTypes.Update(model.EquipmentType);

            return RedirectToAction("EquipmentTypes");
        }

        public ActionResult GetEquipmentTypesLookup(int rowCount, int page, string query)
        {
            PagedResults<EquipmentTypeLookup> lookup = _maintenanceService.EquipmentTypes.GetPagedActiveEquipmentTypesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Equipment Type" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Medical Examination Types

        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Medical Examination Types", 50)]
        public ActionResult MedicalExaminationTypes()
        {
            MedicalExaminationTypeViewModel model = new MedicalExaminationTypeViewModel()
            {
                Active = _maintenanceService.MedicalExaminationTypes.GetActiveMedicalExaminationTypes(),
                Inactive = _maintenanceService.MedicalExaminationTypes.GetInactiveMedicalExaminationTypes()
            };


            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewMedicalExaminationType()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewMedicalExaminationType(UpdateMedicalExaminationTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.MedicalExaminationTypes.Create(model.MedicalExaminationType);

            return RedirectToAction("MedicalExaminationTypes");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateMedicalExaminationType(Guid id)
        {
            MedicalExaminationTypeModel model = _maintenanceService.MedicalExaminationTypes.Single(id);

            var uvr = new UpdateMedicalExaminationTypeViewModel()
            {
                MedicalExaminationType = model
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateMedicalExaminationType(UpdateMedicalExaminationTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.MedicalExaminationTypes.Update(model.MedicalExaminationType);

            return RedirectToAction("MedicalExaminationTypes");
        }

        public ActionResult GetMedicalExaminationTypesLookup(int rowCount, int page, string query)
        {
            PagedResults<MedicalExaminationTypeLookup> lookup = _maintenanceService.MedicalExaminationTypes.GetPagedActiveMedicalExaminationTypesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Medical Examination Type" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Certificate Types

        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Certificate Types", 50)]
        public ActionResult CertificateTypes()
        {
            CertificateTypeViewModel model = new CertificateTypeViewModel()
            {
                Active = _maintenanceService.CertificateTypes.GetActiveCertificateTypes(),
                Inactive = _maintenanceService.CertificateTypes.GetInactiveCertificateTypes()
            };


            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewCertificateType()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewCertificateType(UpdateCertificateTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.CertificateTypes.Create(model.CertificateType);

            return RedirectToAction("CertificateTypes");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateCertificateType(Guid id)
        {
            CertificateTypeModel model = _maintenanceService.CertificateTypes.Single(id);

            var uvr = new UpdateCertificateTypeViewModel()
            {
                CertificateType = model
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateCertificateType(UpdateCertificateTypeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.CertificateTypes.Update(model.CertificateType);

            return RedirectToAction("CertificateTypes");
        }

        public ActionResult GetCertificateTypesLookup(int rowCount, int page, string query)
        {
            PagedResults<CertificateTypeLookup> lookup = _maintenanceService.CertificateTypes.GetPagedActiveCertificateTypesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Certificate Type" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Loading Sheet Tempaltes

        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Loading Sheet Templates", 60)]
        public ActionResult LoadingSheetTemplates()
        {
            LoadingSheetTemplateViewModel model = new LoadingSheetTemplateViewModel()
            {
                Active = _maintenanceService.LoadingSheetTemplates.GetActiveLoadingSheetTemplates(),
                Inactive = _maintenanceService.LoadingSheetTemplates.GetInactiveLoadingSheetTemplates()
            };


            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewLoadingSheetTemplate()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewLoadingSheetTemplate(UpdateLoadingSheetTemplateViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var templateID = _maintenanceService.LoadingSheetTemplates.Create(model.LoadingSheetTemplate);

            return RedirectToAction("UpdateLoadingSheetTemplate", new { id = templateID });
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateLoadingSheetTemplate(Guid id)
        {
            LoadingSheetTemplateModel model = _maintenanceService.LoadingSheetTemplates.Single(id);

            var uvr = new UpdateLoadingSheetTemplateViewModel()
            {
                LoadingSheetTemplate = model
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateLoadingSheetTemplate(UpdateLoadingSheetTemplateViewModel model, string Equipment, string NewEquipmentName, decimal? quantity, int? sheetOrder)
        {
            if (Equipment != null)
            {
                if (!string.IsNullOrEmpty(NewEquipmentName) && quantity.HasValue && sheetOrder.HasValue)
                    _loadingSheetService.AddNonStandardTemplateEquipment(model.LoadingSheetTemplate.LoadingSheetTemplateID, NewEquipmentName, quantity.Value, sheetOrder.Value);

                return View(model);
            }

            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.LoadingSheetTemplates.Update(model.LoadingSheetTemplate);

            return RedirectToAction("LoadingSheetTemplates");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult AddLoadingSheetTemplateEquipment(List<string> selectedEquipment, decimal quantity, string loadingSheetTemplateID, int sheetOrder)
        {
            if (selectedEquipment != null)
            {
                _loadingSheetService.AddTemplateEquipment(selectedEquipment, quantity, new Guid(loadingSheetTemplateID), sheetOrder);
            }

            return Json(Url.Action("UpdateLoadingSheetTemplate", "Maintenance", new { id = loadingSheetTemplateID }));
        }

        public ActionResult GetLoadingSheetTemplatesLookup(int rowCount, int page, string query)
        {
            PagedResults<LoadingSheetTemplateLookup> lookup = _maintenanceService.LoadingSheetTemplates.GetPagedActiveLoadingSheetTemplatesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Loading Sheet Template" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Action Titles

        //[NavigationMenuItem("Action Titles", 50)]
        //public ActionResult ActionTitles()
        //{
        //    ActionTitleViewModel model = new ActionTitleViewModel()
        //    {
        //        Active = _maintenanceService.ActionTitles.GetActiveActionTitles(),
        //        Inactive = _maintenanceService.ActionTitles.GetInactiveActionTitles()
        //    };


        //    return View(model);
        //}

        //public ActionResult NewActionTitle()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult NewActionTitle(UpdateActionTitleViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //        return View(model);

        //    _maintenanceService.ActionTitles.Create(model.ActionTitle);

        //    return RedirectToAction("ActionTitles");
        //}

        //public ActionResult UpdateActionTitle(Guid id)
        //{
        //    ActionTitleModel model = _maintenanceService.ActionTitles.Single(id);

        //    var uvr = new UpdateActionTitleViewModel()
        //    {
        //        ActionTitle = model
        //    };

        //    return View(uvr);
        //}

        //[HttpPost]
        //public ActionResult UpdateActionTitle(UpdateActionTitleViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //        return View(model);

        //    _maintenanceService.ActionTitles.Update(model.ActionTitle);

        //    return RedirectToAction("ActionTitles");
        //}

        //public ActionResult GetActionTitlesLookup(int rowCount, int page, string query)
        //{
        //    PagedResults<ActionTitleLookup> lookup = _maintenanceService.ActionTitles.GetPagedActiveActionTitlesLookup(rowCount, page, query);
        //    LookupModel model = new LookupModel()
        //    {
        //        Page = lookup.Page,
        //        PageCount = lookup.PageCount,
        //        ColumnHeaders = new string[] { "Title" },
        //        Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
        //    };
        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        #region Price Lines
        [Authorize(Roles = "Manager")]
        //[NavigationMenuItem("Price Lines", 70)]
        public ActionResult PriceLines()
        {
            var model = new PriceLineViewModel
            {
                Active = _maintenanceService.PriceLines.GetActivePriceLines(),
                Inactive = _maintenanceService.PriceLines.GetInactivePriceLines()
            };

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewPriceLine()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewPriceLine(UpdatePriceLineViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.PriceLines.Create(model.PriceLine);

            return RedirectToAction("PriceLines");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdatePriceLine(Guid id)
        {
            var model = _maintenanceService.PriceLines.Single(id);

            var uvr = new UpdatePriceLineViewModel
            {
                PriceLine = model
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdatePriceLine(UpdatePriceLineViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.PriceLines.Update(model.PriceLine);

            return RedirectToAction("PriceLines");
        }

        public ActionResult GetPriceLinesLookup(int rowCount, int page, string query)
        {
            var lookup = _maintenanceService.PriceLines.GetPagedActivePriceLinesLookup(rowCount, page, query);
            LookupModel model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Price Line", "Description" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key, s.Description } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPriceLine(Guid id)
        {
            var model = _maintenanceService.PriceLines.Single(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Reason Codes
        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Job / Task Reason Codes", 70)]
        public ActionResult ReasonCodes()
        {
            var model = new ReasonCodeViewModel
            {
                Active = _maintenanceService.ReasonCodes.GetActiveReasonCodes(),
                Inactive = _maintenanceService.ReasonCodes.GetInactiveReasonCodes()
            };

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewReasonCode()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewReasonCode(UpdateReasonCodeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.ReasonCodes.Create(model.ReasonCode);

            return RedirectToAction("ReasonCodes");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateReasonCode(Guid id)
        {
            var model = _maintenanceService.ReasonCodes.Single(id);

            var uvr = new UpdateReasonCodeViewModel
            {
                ReasonCode = model
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateReasonCode(UpdateReasonCodeViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.ReasonCodes.Update(model.ReasonCode);

            return RedirectToAction("ReasonCodes");
        }

        public ActionResult GetReasonCodesLookup(int rowCount, int page, string query)
        {
            var lookup = _maintenanceService.ReasonCodes.GetPagedActiveReasonCodesLookup(rowCount, page, query);
            LookupModel model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Reason Code" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReasonCode(Guid id)
        {
            var model = _maintenanceService.ReasonCodes.Single(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Employee Absence Reasons
        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Employee Absence Reason", 71)]
        public ActionResult EmployeeAbsenceReasons()
        {
            var model = new EmployeeAbsenceReasonViewModel
            {
                Active = _maintenanceService.EmployeeAbsenceReasons.GetActiveEmployeeAbsenceReasons(),
                Inactive = _maintenanceService.EmployeeAbsenceReasons.GetInactiveEmployeeAbsenceReasons()
            };

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewEmployeeAbsenceReason()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewEmployeeAbsenceReason(UpdateEmployeeAbsenceReasonViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.EmployeeAbsenceReasons.Create(model.EmployeeAbsenceReason);

            return RedirectToAction("EmployeeAbsenceReasons");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateEmployeeAbsenceReason(Guid id)
        {
            var model = _maintenanceService.EmployeeAbsenceReasons.Single(id);

            var uvr = new UpdateEmployeeAbsenceReasonViewModel
            {
                EmployeeAbsenceReason = model
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateEmployeeAbsenceReason(UpdateEmployeeAbsenceReasonViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.EmployeeAbsenceReasons.Update(model.EmployeeAbsenceReason);

            return RedirectToAction("EmployeeAbsenceReasons");
        }

        public ActionResult GetEmployeeAbsenceReasonsLookup(int rowCount, int page, string query)
        {
            var lookup = _maintenanceService.EmployeeAbsenceReasons.GetPagedActiveEmployeeAbsenceReasonsLookup(rowCount, page, query);
            LookupModel model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Employee Absence Reason" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeAbsenceReason(Guid id)
        {
            var model = _maintenanceService.EmployeeAbsenceReasons.Single(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Vehicle Absence Reasons
        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Vehicle Absence Reason", 71)]
        public ActionResult VehicleAbsenceReasons()
        {
            var model = new VehicleAbsenceReasonViewModel
            {
                Active = _maintenanceService.VehicleAbsenceReasons.GetActiveVehicleAbsenceReasons(),
                Inactive = _maintenanceService.VehicleAbsenceReasons.GetInactiveVehicleAbsenceReasons()
            };

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewVehicleAbsenceReason()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewVehicleAbsenceReason(UpdateVehicleAbsenceReasonViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.VehicleAbsenceReasons.Create(model.VehicleAbsenceReason);

            return RedirectToAction("VehicleAbsenceReasons");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateVehicleAbsenceReason(Guid id)
        {
            var model = _maintenanceService.VehicleAbsenceReasons.Single(id);

            var uvr = new UpdateVehicleAbsenceReasonViewModel
            {
                VehicleAbsenceReason = model
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateVehicleAbsenceReason(UpdateVehicleAbsenceReasonViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.VehicleAbsenceReasons.Update(model.VehicleAbsenceReason);

            return RedirectToAction("VehicleAbsenceReasons");
        }

        public ActionResult GetVehicleAbsenceReasonsLookup(int rowCount, int page, string query)
        {
            var lookup = _maintenanceService.VehicleAbsenceReasons.GetPagedActiveVehicleAbsenceReasonsLookup(rowCount, page, query);
            LookupModel model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Vehicle Absence Reason" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleAbsenceReason(Guid id)
        {
            var model = _maintenanceService.VehicleAbsenceReasons.Single(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Weather Conditions
        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Weather Conditions", 100)]
        public ActionResult WeatherConditions()
        {
            var model = new WeatherConditionViewModel
            {
                Active = _maintenanceService.WeatherConditions.GetActiveWeatherConditions(),
                Inactive = _maintenanceService.WeatherConditions.GetInactiveWeatherConditions()
            };

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewWeatherCondition()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewWeatherCondition(UpdateWeatherConditionViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.WeatherConditions.Create(model.WeatherCondition);

            return RedirectToAction("WeatherConditions");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateWeatherCondition(Guid id)
        {
            var model = _maintenanceService.WeatherConditions.Single(id);

            var uvr = new UpdateWeatherConditionViewModel
            {
                WeatherCondition = model
            };

            return View(uvr);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateWeatherCondition(UpdateWeatherConditionViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _maintenanceService.WeatherConditions.Update(model.WeatherCondition);

            return RedirectToAction("WeatherConditions");
        }

        public ActionResult GetWeatherConditionsLookup(int rowCount, int page, string query)
        {
            var lookup = _maintenanceService.WeatherConditions.GetPagedActiveWeatherConditionsLookup(rowCount, page, query);
            LookupModel model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Weather Condition" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetWeatherCondition(Guid id)
        {
            var model = _maintenanceService.WeatherConditions.Single(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Disposal Sites
        [Authorize(Roles = "Manager")]
        [NavigationMenuItem("Disposal Sites", 30)]
        public ActionResult DisposalSites()
        {
            var model = new DisposalSitesViewModel();
            model.Active = _maintenanceService.DisposalSites.GetActiveDisposalSites();
            model.Inactive = _maintenanceService.DisposalSites.GetInactiveDisposalSites();

            return View(model);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult NewDisposalSite()
        {
            return View();
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult NewDisposalSite(DisposalSiteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _maintenanceService.DisposalSites.Create(viewModel.ToModel());
                return RedirectToAction("DisposalSites");
            }

            return View(viewModel);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult UpdateDisposalSite(Guid id)
        {
            var viewModel = _maintenanceService.DisposalSites.Single(id).ToViewModel();

            return View(viewModel);
        }

        [Authorize(Roles = "Manager")]
        [HttpPost]
        public ActionResult UpdateDisposalSite(DisposalSiteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _maintenanceService.DisposalSites.Update(viewModel.ToModel());
                return RedirectToAction("DisposalSites");
            }

            return View(viewModel);
        }

        public ActionResult GetDisposalSitesLookup(int rowCount, int page, string query)
        {
            var lookup = _maintenanceService.DisposalSites.GetPagedActiveDisposalSitesLookup(rowCount, page, query);

            var model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Disposal Site" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDisposalSite(Guid id)
        {
            var model = _maintenanceService.DisposalSites.Single(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }

}
