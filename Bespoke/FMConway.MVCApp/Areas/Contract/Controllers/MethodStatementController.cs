﻿using FMConway.MVCApp.Areas.Contract.ViewModels.MethodStatements;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Files.Interface;
using FMConway.Services.Files.Model;
using FMConway.Services.MethodStatements.Interfaces;
using FMConway.Services.MethodStatements.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Method Statements", 115, "fa fa-book-open", "Administrator, Supervisor, Manager, MethodStatements")]
    public class MethodStatementController : BaseController
    {
        IMethodStatementService _methodStatementService;
        IFileService _fileService;

        public MethodStatementController(IMethodStatementService methodStatementService, IFileService fileService)
        {
            _methodStatementService = methodStatementService;
            _fileService = fileService;
        }

        #region MethodStatement table actions
        Func<MethodStatementModel, TableAction> updateAction = methodStatement =>
        {
            return new TableAction()
            {
                Url = "/Contract/MethodStatement/Update/" + methodStatement.MethodStatementID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<MethodStatementModel, TableAction> viewAction = methodStatement =>
        {
            return new TableAction()
            {
                Url = "/Contract/MethodStatement/Summary/" + methodStatement.MethodStatementID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        public ActionResult ActiveMethodStatements(int rowCount, int page, string query)
        {
            PagedResults<MethodStatementModel> results = _methodStatementService.GetActiveMethodStatementsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = MethodStatementOverviewTable(results, new List<Func<MethodStatementModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        public ActionResult InactiveMethodStatements(int rowCount, int page, string query)
        {
            PagedResults<MethodStatementModel> results = _methodStatementService.GetInactiveMethodStatementsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = MethodStatementOverviewTable(results, new List<Func<MethodStatementModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable MethodStatementOverviewTable(PagedResults<MethodStatementModel> results, List<Func<MethodStatementModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Method Statement Title" /*, "Revision" */},
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Title/*, s.Revision */},
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            MethodStatementViewModel model = new MethodStatementViewModel() { };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        [HttpPost]
        public ActionResult New(MethodStatementViewModel model, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (file != null)
            {
                byte[] fileData = new byte[file.ContentLength];
                file.InputStream.Read(fileData, 0, fileData.Length);

                FileModel fileModel = new FileModel()
                {
                    FileDownloadName = file.FileName,
                    Data = fileData,
                    ContentType = file.ContentType,
                };

                var addedFile = _fileService.Add(fileModel);

                model.MethodStatement.FileID = addedFile.ID;
            }

            Guid methodStatementID = _methodStatementService.Create(model.MethodStatement);

            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        public ActionResult Update(Guid id)
        {
            var currentStatement = _methodStatementService.Single(id);
            MethodStatementViewModel model = new MethodStatementViewModel()
            {
                MethodStatement = currentStatement,
                FileName = currentStatement.FileID.HasValue ? _fileService.Single(currentStatement.FileID.Value).FileDownloadName : ""
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        [HttpPost]
        public ActionResult Update(MethodStatementViewModel model, string Cancel, HttpPostedFileBase file)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                if (file != null && !model.MethodStatement.FileID.HasValue)
                {
                    byte[] fileData = new byte[file.ContentLength];
                    file.InputStream.Read(fileData, 0, fileData.Length);

                    FileModel fileModel = new FileModel()
                    {
                        FileDownloadName = file.FileName,
                        Data = fileData,
                        ContentType = file.ContentType,
                    };

                    var addedFile = _fileService.Add(fileModel);

                    model.MethodStatement.FileID = addedFile.ID;
                }

                _methodStatementService.Update(model.MethodStatement);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            var currentStatement = _methodStatementService.Single(id);
            MethodStatementViewModel model = new MethodStatementViewModel()
            {
                MethodStatement = currentStatement,
                FileName = currentStatement.FileID.HasValue ? _fileService.Single(currentStatement.FileID.Value).FileDownloadName : ""
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MethodStatements")]
        [HttpPost]
        public ActionResult Summary()
        {
            return RedirectToAction("Inactive");
        }

        public ActionResult GetMethodStatementLookup(int rowCount, int page, string query)
        {
            PagedResults<MethodStatementLookup> lookup = _methodStatementService.GetPagedActiveMethodStatementsLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Method Statement"/* , "Revision" */},
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key, /*s.Revision*/ } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMethodStatementsForJobLookup(int rowCount, int page, string query, Guid jobID)
        {
            PagedResults<MethodStatementLookup> lookup = _methodStatementService.GetPagedActiveMethodStatementsForJobLookup(rowCount, page, query, jobID);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Method Statement"/*, "Revision" */},
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key, /*s.Revision*/ } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
