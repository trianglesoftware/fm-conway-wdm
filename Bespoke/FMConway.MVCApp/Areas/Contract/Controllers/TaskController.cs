﻿using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Areas.Contract.ViewModels.Tasks;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Tasks.Models;
using FMConway.Services.Tasks.Interfaces;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.WorkInstructionDetails.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMConway.Services.Pdf.Services;
using FMConway.Services.Helpers;
using FMConway.Services.Tasks.Enums;
using FMConway.MVCApp.Areas.Contract.ViewModels.MethodStatements;
using FMConway.Services.Files.Model;
using FMConway.Services.Files.Interface;
using FMConway.Services.Checklists.Interfaces;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.Services.Depots.Models;
using FMConway.Services.Lanes.Interfaces;
using FMConway.Services.Lanes.Models;
using FMConway.Services.Directions.Interfaces;
using FMConway.Services.ActivityPriorities.Interfaces;
using FMConway.Services.ActivityPriorities.Models;
using FMConway.Services;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    public class TaskController : BaseController
    {
        ITaskService _taskService;
        IFileService _fileService;
        IChecklistService _checklistService;
        IErrorLoggingService _errorLoggingService;
        ILanesService _lanesService;
        IDirectionService _directionService;
        IActivityPriorityService _activityPriorityService;

        public TaskController(ITaskService taskService, IFileService fileService, IErrorLoggingService errorLoggingService, IChecklistService checklistService, ILanesService lanesService, IDirectionService directionService, IActivityPriorityService activityPriorityService)
        {
            _taskService = taskService;
            _fileService = fileService;
            _lanesService = lanesService;
            _errorLoggingService = errorLoggingService;
            _checklistService = checklistService;
            _directionService = directionService;
            _activityPriorityService = activityPriorityService;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Tasks, Customer")]
        public ActionResult ActiveTasks(int rowCount, int page, string query, Guid workInstructionID)
        {
            PagedResults<TaskModel> results = _taskService.GetActiveTasksPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

            PagedTable model = TaskOverviewTable(results, new List<Func<TaskModel, TableAction>>() { viewTaskAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewTask(Guid id)
        {
            var model = _taskService.Single(id);

            TaskViewModel tvm = new TaskViewModel()
            {
                TaskID = model.TaskID,
                TaskType = model.TaskType,
                StartTime = model.StartTime,
                EndTime = model.EndTime,
                Name = model.Name,
                WINumber = model.WINumber.ToString(),
                WorkInstructionID = model.WorkInstructionID,
                ChecklistID = model.ChecklistID,
                AreaCallAreaID = model.AreaCallAreaID,
                PhotosRequired = model.PhotosRequired,
                Longitude = model.Longitude,
                Latitude = model.Latitude,
                Weather = model.Weather,
                TaskChecklistAnswerPhoto = new TaskChecklistAnswerPhotoModel() { TaskChecklistAnswerID = Guid.NewGuid(), Latitude = 0, Longitude = 0 }

            };

            switch (tvm.TaskType)
            {
                case "Traffic Count":
                    // Get traffic count details
                    tvm.Notes = model.Notes;
                    break;

                case "Free Text and Details":
                    // Get free text details e.g. person's name
                    tvm.Notes = model.Notes;
                    break;

                case "Area Call":
                    // Get person's name, reference, extra details
                    tvm.AreaCall = _taskService.GetAreaCallDetails(tvm.AreaCallAreaID.Value);
                    break;

                case "Checklist":
                    // Get checklist and answers
                    tvm.Checklist = true;
                    tvm.IsRiskAssessment = _taskService.IsTaskRiskAssessmentChecklist(id);
                    break;

                case "Signature":
                    // Get signatures
                    tvm.Signatures = true;
                    break;

                case "Equipment Install":
                    tvm.Equipment = true;
                    tvm.Notes = model.Tag == "ShowNotes" ? model.Notes : null;
                    break;

                case "Equipment Collection":
                    tvm.Equipment = true;
                    tvm.Notes = model.Tag == "ShowNotes" ? model.Notes : null;
                    break;

                case "Photos":
                    tvm.PhotosRequired = true;
                    tvm.Notes = model.Tag != null && model.Tag.Contains("ShowNotes") ? model.Notes : null;
                    break;

                case "Weather":
                    tvm.IsWeather = true;
                    break;

                case "Activity Details":
                    tvm.ActivityDetails = _taskService.GetActivityDetails(tvm.WorkInstructionID);
                    break;

                case "Activity Risk Assessment":
                    tvm.HasActivityRiskAssessments = _taskService.HasActivityRiskAssessments(tvm.TaskID);
                    break;

                case "Cleansing Log":
                    tvm.CleansingLog = true;
                    break;

                case "Unloading Process":
                    tvm.UnloadingProcess = true;
                    break;

                default:
                    break;
            }

            return View(tvm);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [HttpPost]
        public ActionResult ViewTask(TaskViewModel vm)
        {
            var model = _taskService.Single(vm.TaskID);

            TaskViewModel tvm = new TaskViewModel()
            {
                TaskID = model.TaskID,
                TaskType = model.TaskType,
                StartTime = model.StartTime,
                EndTime = model.EndTime,
                Name = model.Name,
                WINumber = model.WINumber.ToString(),
                WorkInstructionID = model.WorkInstructionID,
                ChecklistID = model.ChecklistID,
                AreaCallAreaID = model.AreaCallAreaID,
                PhotosRequired = model.PhotosRequired,
                Longitude = model.Longitude,
                Latitude = model.Latitude,
                Weather = model.Weather
            };

            model.Latitude = vm.Latitude;
            model.Longitude = vm.Longitude;
            model.StartTime = vm.StartTime;
            model.EndTime = vm.EndTime;
            model.Notes = vm.Notes;

            if (ModelState.IsValid)
            {
                _taskService.Update(model);
                if (vm.ActivityDetails != null)
                {
                    var activityModel = new TaskActivityDetailsModel();
                    activityModel.ClientReferenceNumber = vm.ActivityDetails.ClientReferenceNumber;
                    activityModel.WorkInstructionID = vm.WorkInstructionID;
                    activityModel.SiteName = vm.ActivityDetails.SiteName;
                    activityModel.Operator = vm.ActivityDetails.Operator;
                    activityModel.ActivityPriorityID = vm.ActivityDetails.ActivityPriorityID;
                    _taskService.UpdateTaskActivity(activityModel);
                }
            }
            else
            {
                return View(tvm);
            }

            switch (tvm.TaskType)
            {
                case "Traffic Count":
                    // Get traffic count details
                    tvm.Notes = model.Notes;
                    break;

                case "Free Text and Details":
                    // Get free text details e.g. person's name
                    tvm.Notes = model.Notes;
                    break;

                case "Area Call":
                    // Get person's name, reference, extra details
                    tvm.AreaCall = _taskService.GetAreaCallDetails(tvm.AreaCallAreaID.Value);
                    break;

                case "Checklist":
                    // Get checklist and answers
                    tvm.Checklist = true;
                    tvm.IsRiskAssessment = _taskService.IsTaskRiskAssessmentChecklist(vm.TaskID);
                    break;

                case "Signature":
                    // Get signatures
                    tvm.Signatures = true;
                    break;

                case "Equipment Install":
                    tvm.Equipment = true;
                    tvm.Notes = model.Tag == "ShowNotes" ? model.Notes : null;
                    break;

                case "Equipment Collection":
                    tvm.Equipment = true;
                    tvm.Notes = model.Tag == "ShowNotes" ? model.Notes : null;
                    break;

                case "Photos":
                    tvm.PhotosRequired = true;
                    tvm.Notes = model.Tag != null && model.Tag.Contains("ShowNotes") ? model.Notes : null;
                    break;

                case "Weather":
                    tvm.IsWeather = true;
                    break;

                case "Activity Details":
                    tvm.ActivityDetails = _taskService.GetActivityDetails(tvm.WorkInstructionID);
                    break;

                case "Activity Risk Assessment":
                    tvm.HasActivityRiskAssessments = _taskService.HasActivityRiskAssessments(tvm.TaskID);
                    break;

                case "Cleansing Log":
                    tvm.CleansingLog = true;
                    break;

                case "Unloading Process":
                    tvm.UnloadingProcess = true;
                    break;

                default:
                    break;
            }

            if (vm.AssetsInstalled != null)
            {
                if (!_taskService.RemoveAssets(vm.AssetsInstalled.Select(a => a.EquipmentID).FirstOrDefault(), vm.AssetsInstalled.Select(a => a.AssetID).ToList()))
                {
                    ModelState.AddModelError("", "Error deleting Installed Assets");
                    return View(tvm);
                }

                foreach (var asset in vm.AssetsInstalled)
                {
                    if (asset.AssetNumber == null)
                    {
                        ModelState.AddModelError("", "Asset Number is required.");
                        return View(tvm);
                    }

                    if (!_taskService.UpdateAssets(vm.TaskID, asset.AssetID, asset.EquipmentID, asset.ItemNumber, asset.AssetNumber))
                    {
                        ModelState.AddModelError("", "Error updating Installed Assets");
                        return View(tvm);
                    }
                }

            }

            if (vm.AssetsCollected != null)
            {
                foreach (var asset in vm.AssetsCollected)
                {
                    if (asset.AssetNumber == null)
                    {
                        ModelState.AddModelError("", "Asset Number is required.");
                        return View(tvm);
                    }

                    if (!_taskService.UpdateAssets(vm.TaskID, asset.AssetID, asset.EquipmentID, asset.ItemNumber, asset.AssetNumber))
                    {
                        ModelState.AddModelError("", "Error updating Asset Collected");
                        return View(vm);
                    }
                }
            }

            if (vm.TaskEquipments != null)
            {
                foreach (var equipment in vm.TaskEquipments)
                {
                    if (!_taskService.UpdateEquipmentQuantity(equipment.EquipmentID, equipment.Quantity))
                    {
                        ModelState.AddModelError("", "Error updating Equipment Quantity.");
                        return View(tvm);
                    }
                }
            }

            return RedirectToAction("ViewTask", new { id = vm.TaskID });
        }

        public ActionResult GetTaskChecklistAnswers(int rowCount, int page, string query, Guid taskID)
        {
            try
            {
                var results = _taskService.GetTaskChecklistAnswersPaged(rowCount, page, HttpUtility.HtmlEncode(query), taskID);

                var isRiskAssessment = _taskService.IsTaskRiskAssessmentChecklist(taskID);
                var isBatteryCheck = _taskService.IsTaskBatteryChecklist(taskID);
                var isYesNoNa = _taskService.IsYesNoNaChecklist(taskID);

                Func<int?, string> yesNoNaAnswer = a =>
                {
                    return a == null ? "" :
                        a == 0 ? "No" :
                        a == 1 ? "Yes" :
                        a == 2 ? "Na" : "";
                };

                Func<int?, string> standardAnswer = a =>
                {
                    return a == null ? "" :
                        a == 0 ? "No" :
                        a == 1 ? "Yes" : "";
                };

                Func<int?, string> riskAssessmentAnswer = a =>
                {
                    return a == null ? "" :
                        a == 0 ? "High" :
                        a == 1 ? "Med" :
                        a == 2 ? "Low" : "";
                };

                PagedTable model = null;
                if (isRiskAssessment && !isBatteryCheck)
                {
                    model = new PagedTable
                    {
                        Page = results.Page,
                        PageCount = results.PageCount,
                        ColumnHeaders = new string[]
                        {
                            "Question",
                            "Answer",
                            "Reason",
                            "Time"
                        },
                        Rows = results.Data.Select(s => new TableRow
                        {
                            TableData = new string[]
                            {
                                s.QuestionOrder + ". " + s.ChecklistQuestion,
                                riskAssessmentAnswer(s.ChecklistAnswer),
                                s.Reason,
                                s.ChecklistTime.ToLongTimeString()
                            },
                            Data = new string[]
                            {
                                s.ChecklistAnswerID.ToString(), //TaskChecklistAnswerPK
                                s.ChecklistAnswer.ToString(), //Answer
                            },
                        }).ToList()
                    };
                }
                else if (isBatteryCheck)
                {
                    model = new PagedTable
                    {
                        Page = results.Page,
                        PageCount = results.PageCount,
                        ColumnHeaders = new string[]
                        {
                            "Question",
                            "Answer",
                            "Reason",
                            "Time"
                        },
                        Rows = results.Data.Select(s => new TableRow
                        {
                            TableData = new string[]
                            {
                                s.QuestionOrder + ". " + s.ChecklistQuestion,
                                standardAnswer(s.ChecklistAnswer),
                                s.Reason,
                                s.ChecklistTime.ToLongTimeString()
                            },
                            Data = new string[]
                            {
                                s.ChecklistAnswerID.ToString(), //TaskChecklistAnswerPK
                                s.ChecklistAnswer.ToString(), //Answer
                            },
                        }).ToList()
                    };
                }
                else if (isYesNoNa)
                {
                    model = new PagedTable
                    {
                        Page = results.Page,
                        PageCount = results.PageCount,
                        ColumnHeaders = new string[]
                        {
                            "Question",
                            "Answer",
                            "Reason",
                            "Time"
                        },
                        Rows = results.Data.Select(s => new TableRow
                        {
                            TableData = new string[]
                            {
                                s.QuestionOrder + ". " + s.ChecklistQuestion,
                                yesNoNaAnswer(s.ChecklistAnswer),
                                s.Reason,
                                s.ChecklistTime.ToLongTimeString()
                            },
                            Data = new string[]
                            {
                                s.ChecklistAnswerID.ToString(), //TaskChecklistAnswerPK
                                s.ChecklistAnswer.ToString(), //Answer
                            },
                        }).ToList()
                    };
                }
                else
                {
                    model = new PagedTable
                    {
                        Page = results.Page,
                        PageCount = results.PageCount,
                        ColumnHeaders = new string[]
                        {
                            "Question",
                            "Answer",
                        },
                        Rows = results.Data.Select(s => new TableRow
                        {
                            TableData = new string[]
                            {
                                s.QuestionOrder + ". " + s.ChecklistQuestion,
                                standardAnswer(s.ChecklistAnswer),
                            }
                        }).ToList()
                    };
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetTaskChecklistAnswers", e, taskID.ToString());
                return View();
            }
        }

        public ActionResult GetTaskSignatures(int rowCount, int page, string query, Guid taskID)
        {
            try
            {
                PagedResults<TaskSignatureModel> results = _taskService.GetTaskSignaturesPaged(rowCount, page, HttpUtility.HtmlEncode(query), taskID);

                PagedTable model = TaskSignatureOverviewTable(results, new List<Func<TaskSignatureModel, TableAction>>() { viewTaskSignature });

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetTaskSignatures", e, taskID.ToString());
                return View();
            }
        }

        public ActionResult GetTaskPhotos(int rowCount, int page, string query, Guid taskID)
        {
            try
            {
                PagedResults<TaskPhotoModel> results = _taskService.GetTaskPhotosPaged(rowCount, page, HttpUtility.HtmlEncode(query), taskID);

                PagedTable model = TaskPhotoOverviewTable(results, new List<Func<TaskPhotoModel, TableAction>>() { viewTaskPhoto });

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetTaskPhotos", e, taskID.ToString());
                return View();
            }
        }

        [HttpPost]
        public ActionResult UploadTaskPhoto(TaskViewModel vm, HttpPostedFileBase file)
        {
            var model = new TaskPhotoModel();
            if (file != null)
            {
                byte[] fileData = new byte[file.ContentLength];
                file.InputStream.Read(fileData, 0, fileData.Length);

                FileModel fileModel = new FileModel()
                {
                    FileDownloadName = file.FileName,
                    Data = fileData,
                    ContentType = file.ContentType
                };

                var addedFile = _fileService.Add(fileModel);

                model.FileID = addedFile.ID;
                model.TaskID = vm.TaskID;
                model.Name = vm.Name;
                model.Latitude = vm.TaskPhotoLatitude;
                model.Longitude = vm.TaskPhotoLongitude;
            }

            model = _taskService.UploadTaskPhoto(model);


            return null;
        }

        [HttpPost]
        public ActionResult UploadCleansingLogPhoto(CleansingLogViewModel vm, HttpPostedFileBase file)
        {
            var model = new CleansingLogPhotoModel();
            if (file != null)
            {
                byte[] fileData = new byte[file.ContentLength];
                file.InputStream.Read(fileData, 0, fileData.Length);

                FileModel fileModel = new FileModel()
                {
                    FileDownloadName = file.FileName,
                    Data = fileData,
                    ContentType = file.ContentType
                };

                var addedFile = _fileService.Add(fileModel);

                model.FileID = addedFile.ID;
                model.CleansingLogID = vm.CleansingLogID;
                model.Name = vm.CleansingLogPhoto.Name;
                model.Latitude = vm.CleansingLogPhoto.Latitude;
                model.Longitude = vm.CleansingLogPhoto.Longitude;
                model.Comments = vm.CleansingLogPhoto.Comments;
            }

            _taskService.UploadCleansingLogPhoto(model);


            return null;
        }


        [HttpPost]
        public ActionResult UploadTaskChecklistPhoto(TaskViewModel vm, HttpPostedFileBase file)
        {
            var model = new TaskChecklistAnswerPhotoModel();
            if (file != null)
            {
                byte[] fileData = new byte[file.ContentLength];
                file.InputStream.Read(fileData, 0, fileData.Length);

                FileModel fileModel = new FileModel()
                {
                    FileDownloadName = file.FileName,
                    Data = fileData,
                    ContentType = file.ContentType
                };

                var addedFile = _fileService.Add(fileModel);

                model.FileID = addedFile.ID;
                model.Latitude = vm.TaskChecklistAnswerPhoto.Latitude;
                model.Longitude = vm.TaskChecklistAnswerPhoto.Longitude;
                model.TaskChecklistAnswerID = vm.TaskChecklistAnswerPhoto.TaskChecklistAnswerID;
            }

            _taskService.UploadTaskChecklistPhoto(model);
            return null;
        }

        [HttpPost]
        public ActionResult UploadUploadingProcessPhoto(UnloadingProcessViewModel vm, HttpPostedFileBase file)
        {
            var model = new UnloadingProcessPhotoModel();
            if (file != null)
            {
                byte[] fileData = new byte[file.ContentLength];
                file.InputStream.Read(fileData, 0, fileData.Length);

                FileModel fileModel = new FileModel()
                {
                    FileDownloadName = file.FileName,
                    Data = fileData,
                    ContentType = file.ContentType
                };

                var addedFile = _fileService.Add(fileModel);

                model.FileID = addedFile.ID;
                model.UnloadingProcessID = vm.UnloadingProcessID;
                model.Name = vm.UnloadingProcessPhoto.Name;
                model.Latitude = vm.UnloadingProcessPhoto.Latitude;
                model.Longitude = vm.UnloadingProcessPhoto.Longitude;
                model.Comments = vm.UnloadingProcessPhoto.Comments;
            }

            _taskService.UploadUnloadingProcessPhoto(model);
            return null;
        }

        [HttpPost]
        public ActionResult UnloadingProcess(UnloadingProcessViewModel vm)
        {
            var model = new UnloadingProcessModel();
            model.UnloadingProcessID = vm.UnloadingProcessID;
            model.WasteTicketNumber = vm.WasteTicketNumber;
            model.VolumeOfWasteDisposed = vm.VolumeOfWasteDisposed;
            model.ArriveAtDisposalSiteDate = vm.ArriveAtDisposalSiteDate;
            model.LeaveDisposalSiteDate = vm.LeaveDisposalSiteDate;
            model.DepartToDisposalSiteDate = vm.DepartToDisposalSiteDate;
            model.Comments = vm.Comments;
            _taskService.UpdateUnloadingProcess(model);
            return View(vm);
        }

        [HttpPost]
        public ActionResult UpdateCheckListAnswer(TaskViewModel vm)
        {
            ChecklistAnswerModel model = new ChecklistAnswerModel();
            model.Reason = vm.CheckListAnswer.Reason;
            model.ChecklistAnswer = vm.CheckListAnswer.ChecklistAnswer;
            model.ChecklistAnswerID = vm.CheckListAnswer.ChecklistAnswerID;
            _checklistService.UpdateAnswer(model);
            return null;
        }

        public ActionResult GetTaskChecklistAnswerPhotos(int rowCount, int page, string query, Guid taskChecklistAnswerID)
        {
            try
            {
                var results = _taskService.GetTaskChecklistAnswerPhotosPaged(rowCount, page, HttpUtility.HtmlEncode(query), taskChecklistAnswerID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[]
                    {
                        "Name",
                        "Longitude",
                        "Latitude"
                    },
                    Rows = results.Data.Select(s => new TableRow
                    {
                        TableData = new string[]
                        {
                            s.Name,
                            s.Longitude.ToString(),
                            s.Latitude.ToString()
                        },
                        Actions = new List<TableAction>
                        {
                            new TableAction
                            {
                                Url = "/File/CreateDocument/" + s.FileID,
                                Title = "View",
                                IconClass = "icon-pencil"
                            }
                        }
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetTaskChecklistAnswerPhotos", e, taskChecklistAnswerID.ToString());
                return View();
            }
        }

        public ActionResult GetTaskEquipment(int rowCount, int page, string query, Guid workInstructionID)
        {
            try
            {
                var results = _taskService.GetTaskEquipmentPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[]
                    {
                        "Equipment",
                        "Type",
                        "Qty Installed",
                        "Qty Collected",
                        "Asset"
                    },
                    Rows = results.Data.Select(s => new TableRow
                    {
                        Data = new string[]
                        {
                            s.EquipmentID.ToString(),
                            s.hasEquipmentInstallTask.ToString(),
                            s.hasEquipmentCollectionTask.ToString(),
                            (!String.IsNullOrWhiteSpace(s.VmsOrAsset)).ToString(),
                            s.TaskEquipmentID.ToString(),
                        },
                        TableData = new string[] {
                            s.Equipment,
                            s.Type,
                            //<input id='TaskEquipments_{results.Data.IndexOf(s)}__EquipmentID' name='TaskEquipments[{results.Data.IndexOf(s)}].EquipmentID' type='text' value='{s.TaskEquipmentID}>
                            !String.IsNullOrWhiteSpace(s.VmsOrAsset) ? s.QuantityInstalled.ToString() :
                                $"<input class='equipmentQuantity' id='TaskEquipments_{results.Data.IndexOf(s)}__Quantity' name='TaskEquipments[{results.Data.IndexOf(s)}].Quantity' type='number' min='0' value='{s.QuantityInstalled}'>" +
                                $"<input id='TaskEquipments_{results.Data.IndexOf(s)}__EquipmentID' name='TaskEquipments[{results.Data.IndexOf(s)}].EquipmentID' type='hidden' value='{s.TaskEquipmentID}'>",
                            s.QuantityCollected.ToString(),
                            s.VmsOrAsset
                        }
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetTaskEquipment", e, workInstructionID.ToString());
                return View();
            }
        }

        public ActionResult GetTaskEquipmentAssets(int rowCount, int page, string query, Guid workInstructionID, Guid equipmentID, string taskType)
        {
            if (taskType != TaskTypesEnum.EquipmentInstall && taskType != TaskTypesEnum.EquipmentCollection)
            {
                throw new Exception($"Task type '{taskType}' invalid");
            }

            var results = _taskService.GetTaskEquipmentAssetsPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID, equipmentID, taskType);

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "No.",
                    "Asset Number",
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] {
                        s.TaskEquipmentAssetID.ToString(),
                        s.EquipmentID.ToString(),
                        s.ItemNumber.ToString(),
                        s.AssetNumber
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCleansingLogs(int rowCount, int page, string query, Guid taskID)
        {
            var results = _taskService.GetCleansingLogsPaged(rowCount, page, HttpUtility.HtmlEncode(query), taskID);

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Road",
                    "Section",
                    "Created Date",
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.Road,
                        s.Section,
                        s.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    },
                    Actions = new List<TableAction>
                    {
                        new TableAction("/Contract/Task/CleansingLog/" + s.CleansingLogID, "View", "icon-eye-open")
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetActivityRiskAssessments(int rowCount, int page, string query, Guid taskID)
        {
            var results = _taskService.GetActivityRiskAssessmentsPaged(rowCount, page, HttpUtility.HtmlEncode(query), taskID);

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Road",
                    "Speed",
                    "TM Requirement",
                    "Created Date",
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.RoadName,
                        s.RoadSpeed,
                        s.TMRequirement,
                        s.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    },
                    Actions = new List<TableAction>
                    {
                        new TableAction("/Contract/Task/ActivityRiskAssessment/" + s.ActivityRiskAssessmentID, "View", "icon-eye-open")
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUnloadingProcesses(int rowCount, int page, string query, Guid taskID)
        {
            var results = _taskService.GetUnloadingProcessesPaged(rowCount, page, HttpUtility.HtmlEncode(query), taskID);

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Waste Ticket No",
                    "Volume",
                    "Completed",
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.WasteTicketNumber,
                        s.VolumeOfWasteDisposed?.ToString("0.#"),
                        s.Completed?.ToString("dd/MM/yyyy HH:mm:ss"),
                    },
                    Actions = new List<TableAction>
                    {
                        new TableAction("/Contract/Task/UnloadingProcess/" + s.UnloadingProcessID, "View", "icon-eye-open")
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #region Task table actions
        Func<TaskModel, TableAction> viewTaskAction = task =>
        {
            return new TableAction()
            {
                Url = "/Contract/Task/ViewTask/" + task.TaskID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<TaskSignatureModel, TableAction> viewTaskSignature = signature =>
            {
                return new TableAction()
                {
                    Url = "/File/CreateDocument/" + signature.FileID,
                    Title = "View",
                    IconClass = "icon-pencil"
                };
            };

        Func<TaskPhotoModel, TableAction> viewTaskPhoto = photo =>
        {
            return new TableAction()
            {
                Url = "/File/CreateDocument/" + photo.FileID,
                Title = "View",
                IconClass = "icon-pencil"
            };
        };
        #endregion
        private PagedTable TaskOverviewTable(PagedResults<TaskModel> results, List<Func<TaskModel, TableAction>> actions)
        {
            var isCustomer = User.IsInRole("Customer");

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name", "Task Item Type", "Start Time", "End Time" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name.ToString(), s.TaskType, s.StartTime.ToString(), s.EndTime.ToString() },
                    Actions =
                        isCustomer ? null :
                        actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable TaskSignatureOverviewTable(PagedResults<TaskSignatureModel> results, List<Func<TaskSignatureModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.EmployeeName },

                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable TaskPhotoOverviewTable(PagedResults<TaskPhotoModel> results, List<Func<TaskPhotoModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name", "Longitude", "Latitude", "Comments" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name, s.Longitude.ToString(), s.Latitude.ToString(), s.Comments },

                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        public ActionResult CleansingLog(Guid id)
        {
            var viewModel = _taskService.GetCleansingLog(id).ToViewModel();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CleansingLog(CleansingLogViewModel vm)
        {
            var model = new CleansingLogModel();
            model.LaneID = vm.LaneID;
            model.DirectionID = vm.DirectionID;
            model.CleansingLogID = vm.CleansingLogID;
            model.Section = vm.Section;
            model.Comments = vm.Comments;
            model.Road = vm.Road;
            model.StartMarker = vm.StartMarker;
            model.EndMarker = vm.EndMarker;
            model.CatchpitsCleaned = vm.CatchpitsCleaned;
            model.Channels = vm.Channels;
            model.CatchpitsMissed = vm.CatchpitsMissed;
            model.EndMarker = vm.EndMarker;
            model.GulliesCleaned = vm.GulliesCleaned;
            model.GulliesMissed = vm.GulliesMissed;
            model.SlotDrains = vm.SlotDrains;
            model.Defects = vm.Defects;

            _taskService.UpdateCleansingLog(model);

            return RedirectToAction("ViewTask", "Task", new { id = vm.TaskID });
        }

        public ActionResult ActivityRiskAssessment(Guid id)
        {
            var viewModel = _taskService.GetActivityRiskAssessment(id).ToViewModel();

            return View(viewModel);
        }

        public ActionResult GetLanesLookup(int rowCount, int page, string query)
        {
            PagedResults<LaneLookup> lookup = _lanesService.GetPagedActiveLanesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Lane" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDirectionsLookup(int rowCount, int page, string query)
        {
            PagedResults<DirectionLookup> lookup = _directionService.GetPagedActiveDirectionsLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Lane" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetActivityPrioritiesLookup(int rowCount, int page, string query)
        {
            PagedResults<ActivityPriorityLookup> lookup = _activityPriorityService.GetPagedActiveActivityPriorityLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Activty Priority" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCleansingLogPhotos(int rowCount, int page, string query, Guid cleansingLogID)
        {
            try
            {
                var results = _taskService.GetCleansingLogPhotosPaged(rowCount, page, HttpUtility.HtmlEncode(query), cleansingLogID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[] { "Name", "Longitude", "Latitude", "Comments" },
                    Rows = results.Data.Select(a => new TableRow
                    {
                        TableData = new string[] { a.Name, a.Longitude.ToString(), a.Latitude.ToString(), a.Comments },
                        Actions = new List<TableAction> { new TableAction("/File/CreateDocument/" + a.FileID, "View", "icon-pencil") }
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog(nameof(GetCleansingLogPhotos), e, cleansingLogID.ToString());
                return View();
            }
        }

        public ActionResult UnloadingProcess(Guid id)
        {
            var viewModel = _taskService.GetUnloadingProcess(id).ToViewModel();

            return View(viewModel);
        }

        public ActionResult GetUnloadingProcessPhotos(int rowCount, int page, string query, Guid unloadingProcessID)
        {
            try
            {
                var results = _taskService.GetUnloadingProcessPhotosPaged(rowCount, page, HttpUtility.HtmlEncode(query), unloadingProcessID);

                var model = new PagedTable
                {
                    Page = results.Page,
                    PageCount = results.PageCount,
                    ColumnHeaders = new string[] { "Name", "Longitude", "Latitude", "Comments" },
                    Rows = results.Data.Select(a => new TableRow
                    {
                        TableData = new string[] { a.Name, a.Longitude.ToString(), a.Latitude.ToString(), a.Comments },
                        Actions = new List<TableAction> { new TableAction("/File/CreateDocument/" + a.FileID, "View", "icon-pencil") }
                    }).ToList()
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog(nameof(GetUnloadingProcessPhotos), e, unloadingProcessID.ToString());
                return View();
            }
        }

    }
}