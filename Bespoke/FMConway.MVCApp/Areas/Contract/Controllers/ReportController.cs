﻿using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.ErrorLogging.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMConway.Services.Reports.Models;
using FMConway.Services.Reports.Interfaces;
using FMConway.Services.Employees.Services;
using FMConway.Services.Extensions;
using FMConway.MVCApp.Areas.Contract.ViewModels.Report;
using FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions;
using FMConway.Services.WorkInstructions.Models;
using FMConway.Services.Depots.Services;
using FMConway.Services.Jobs.Services;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
	[Authorize(Roles = "Administrator, Supervisor, Manager, Customer")]
	[Users(Users.UserTypes.Contractor)]
	[NavigationMenu("Reports", 150, "fa fa-chart-line", "Supervisor, Manager")]
	public class ReportController : BaseController
	{
		IReportService _reportService;
		IErrorLoggingService _errorLoggingService;
		EmployeeService _employeeService;
		DepotService _depotService;
		JobService _jobService;

		public ReportController(IReportService reportService, IErrorLoggingService errorLoggingService, EmployeeService employeeService, DepotService depoService, JobService jobService)
		{
			_reportService = reportService;
			_errorLoggingService = errorLoggingService;
			_employeeService = employeeService;
			_depotService = depoService;
			_jobService = jobService;
		}

		#region Price Lines

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem("Job and Task Prices", 10)]
		public ActionResult PriceLines()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveJobPriceLines(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveJobPriceLines(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Job Number",
                    "Job Title",
					"Has Price Lines",
					"Task Number",
                    "Customer",
                    "Contract Number",
                    "Customer Project Code",
					"Code",
					"Description",
					"Rate",
					"Note"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.JobNumber.ToString(),
                        a.JobTitle,
						a.HasPriceLines,
						a.WorkInstructionNumber.ToString(),
                        a.Customer,
                        a.ContractNumber,
                        a.CustomerProjectCode,
						a.Code,
						a.Description,
						a.Rate?.ToString("0.00##"),
						a.Note
					},
					Actions = a.HasPriceLines == "Yes" ? new List<TableAction>
					{
						new TableAction("/Contract/Job/UpdatePriceLine/" + a.JobPriceLineID, "View", "icon-pencil")
					} : null
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveWorkInstructionPriceLines(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveWorkInstructionPriceLines(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Job Number",
                    "Job Title",
					"Has Price Lines",
					"Task Number",
                    "Customer",
                    "Contract Number",
                    "Customer Code",
					"Code",
					"Description",
					"Rate",
					"Note"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.JobNumber.ToString(),
                        a.JobTitle,
						a.HasPriceLines,
						a.WorkInstructionNumber.ToString(),
                        a.Customer,
                        a.ContractNumber,
                        a.CustomerCode,
						a.Code,
						a.Description,
						a.Rate?.ToString("0.00##"),
						a.Note
					},
					Actions = a.HasPriceLines == "Yes" ? new List<TableAction>
					{
						new TableAction("/Contract/WorkInstruction/UpdatePriceLine/" + a.WorkInstructionPriceLineID, "View", "icon-pencil")
					} : null
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Employee Absences

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(5)]
		public ActionResult EmployeeAbsences()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveEmployeeAbsences(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveEmployeeAbsences(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Name",
					"Reason",
					"From",
					"To",
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Name,
						a.Reason,
						a.From.ToShortDateString(),
						a.To.ToShortDateString(),
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/Employee/UpdateEmployeeAbsence/" + a.EmployeeAbsenceID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Vehicle Absences

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(22)]
		public ActionResult VehicleAbsences()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveVehicleAbsences(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveVehicleAbsences(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Make",
					"Model",
					"Registration",
					"Reason",
					"From",
					"To",
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Make,
						a.Model,
						a.Registration,
						a.Reason,
						a.From.ToShortDateString(),
						a.To.ToShortDateString(),
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/Vehicle/UpdateVehicleAbsence/" + a.VehicleAbsenceID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Qualification Expiry

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(17)]
		public ActionResult QualificationExpiry()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveQualificationExpiry(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveQualificationExpiry(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Employee",
					"Certificate Name",
					"Certificate Type",
					"Area Protocol",
					"Description",
					"Expiry Date",
					"Document Uploaded"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Employee,
						a.CertificateName,
						a.CertificateType,
						a.Area,
						a.Description,
						a.ExpiryDate.ToShortDateString(),
						a.DocumentUploaded,
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/AreaCertificate/Update/" + a.EmployeeCertificatePK, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Work Instruction Report

		public ActionResult WorkInstructionReport(Guid id)
		{
            var bytes = _reportService.GenerateJobSheetReport(id, out string filename);

            return File(bytes, "application/pdf", filename);
        }

        public ActionResult WorkInstructionReportPreview(Guid id)
        {
            var bytes = _reportService.GenerateJobSheetReport(id, out string filename);

            return File(bytes, "application/pdf");
        }

        #endregion

        #region Daily Diary

        [Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(4)]
		public ActionResult DailyDiary()
		{
			var model = new DailyDiaryViewModel();
			model.StartDate = DateTime.Today;

			return View(model);
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveDailyDiary(int rowCount, int page, string query, DateTime? startDate)
		{
			var results = _reportService.GetPagedActiveDailyDiary(rowCount, page, HttpUtility.HtmlEncode(query), startDate);

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Task",
					"Job",
					"Customer",
					"Contract Manager",
					"Contract Number",
					"Job Title",
					"Start Date",
					"SRW/Permit Number",
					"Issued By",
					"Scheduled",
					"Status",
					"Successful",
					"Job Type",
					"Address"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Task.ToString(),
						a.Job.ToString(),
						a.Customer,
						a.ContractManager,
						a.ContractNumber,
						a.JobTitle,
						a.StartDate.ToShortDateString() + " " + a.StartDate.ToShortTimeString(),
						a.SRW,
						a.IssuedBy,
						a.Scheduled ? "Yes" : "No",
						a.Status,
						a.Successful != null ? (a.Successful.Value ? "Yes" : "No") : "",
						a.JobType,
						a.Address
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/WorkInstruction/Update/" + a.WorkInstructionID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Timesheet Information
		[Authorize(Roles = "Supervisor, Manager, Employees")]
		[NavigationMenuItem(20)]
		public ActionResult TimesheetInformation()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager, Employees")]
		public ActionResult GetTimesheetInformation(int rowCount, int page, string query)
		{
			var results = _reportService.GetTimesheetInformationPaged(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Employee Name",
					"Employee Type",
					"Phone Number",
					"Tasks today",
					"Tasks tomorrow",
					"Tasks this week",
					"Tasks next week"
				},
				Rows = results.Data.Select(s => new TableRow
				{
					TableData = new string[]
					{
						s.EmployeeName,
						s.EmployeeType,
						s.EmployeePhoneNumber,
						s.TasksToday.ToString(),
						s.TasksTomorrow.ToString(),
						s.TasksThisWeek.ToString(),
						s.TasksNextWeek.ToString()
					},
					Actions = new List<TableAction>
					{
						new TableAction
						{
							Url = "/Contract/Report/TimesheetInformationDetail/" + s.EmployeeID,
							Title = "Edit",
							IconClass = "icon-pencil"
						}
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		[Authorize(Roles = "Supervisor, Manager, Employees")]
		public ActionResult TimesheetInformationDetail(Guid id)
		{
			var model = new TimesheetInformationDetailViewModel();
			model.EmployeeID = id;
			model.EmployeeName = _employeeService.GetEmployeeName(id);

			return View(model);
		}

		[Authorize(Roles = "Supervisor, Manager, Employees")]
		public ActionResult GetTimesheetInformationDetail(int rowCount, int page, string query, Guid employeeID)
		{
			var results = _reportService.GetTimesheetInformationDetailPaged(rowCount, page, HttpUtility.HtmlEncode(query), employeeID);

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Day",
					"Scheduled Date",
					"Task",
					"Customer",
					"Contract Number",
					"Contract Manager",
					"Job Title",
					"Description",
					"Start Date",
					"Job Type",
					"Speed",
					"On Site",
					"Off Site",
					"Travel Time",
					"Address"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Day.DayOfWeek.ToString(),
						a.Day.ToShortDateString(),
						a.Schedule?.Task.ToString() ?? "",
						a.Schedule?.Customer ?? "",
						a.Schedule?.ContractNumber ?? "",
						a.Schedule?.ContractManager ?? "",
						a.Schedule?.JobTitle ?? "",
						a.Schedule?.Description ?? "",
						a.Schedule?.StartDate.ToShortDateString() ?? "",
						a.Schedule?.JobType ?? "",
						a.Schedule?.Speed ?? "",
						a.Schedule?.OnSite?.ToShortTimeString() ?? "",
						a.Schedule?.OffSite?.ToShortTimeString() ?? "",
						a.Schedule != null && a.Schedule.TravelTime > 0 ? TimeSpan.FromMinutes(a.Schedule.TravelTime).ToString(@"hh\:mm") : "",
						a.Schedule?.Address ?? ""
					},
					Data = new string[]
					{
						a.Schedule == null && a.Day.DayOfWeek != DayOfWeek.Saturday && a.Day.DayOfWeek != DayOfWeek.Sunday ? "noschedule" : "",
						a.Day == DateTime.Today ? "today" : ""
					},
					Actions = new List<TableAction>
					{
						new TableAction
						{
							Url = "/Operation/Schedule/ViewScheduler?startDate=" + HttpUtility.UrlEncode(a.Day.StartOfWeek(DayOfWeek.Monday).ToString("MM/dd/yyyy")) + "#Anchor-" + a.Day.DayOfWeek,
							Title = "Edit",
							IconClass = "icon-pencil",
							Class = "FindMe"
						}
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Weekly Timesheet

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(23)]
		public ActionResult WeeklyTimesheet()
		{
			var model = new WeeklyTimesheetViewModel();
			model.StartDate = DateTime.Today.StartOfWeek(DayOfWeek.Monday);

			return View(model);
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveWeeklyTimesheet(int rowCount, int page, string query, DateTime? startDate, Guid? employeeID)
		{
			startDate = startDate ?? DateTime.Today;
			startDate = startDate.Value.StartOfWeek(DayOfWeek.Monday);
			employeeID = employeeID ?? Guid.Empty;

			var results = _reportService.GetPagedActiveWeeklyTimesheet(rowCount, page, HttpUtility.HtmlEncode(query), startDate.Value, (Guid)employeeID);

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Name",
					"Job Title",
					"Clock No",
					"Mon",
					"Tues",
					"Wed",
					"Thur",
					"Fri",
					"Sat",
					"Sun",
					"Sleep Over Hours",
					"Appears (Hours)",
					"Hours",
					"Travel Time",
					"Stand By",
					"Bonus",
					"Holiday Pay Required",
					"No of Nights Subsistence"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Name,
						a.JobTitle,
						a.ClockNo,
						a.Mon,
						a.Tues,
						a.Wed,
						a.Thur,
						a.Fri,
						a.Sat,
						a.Sun,
						a.SleepOverHours,
						a.Appears,
						a.Hours,
						a.TravelTime,
						a.StandBy,
						a.Bonus,
						a.HolidayPayRequired,
						a.NoOfNightsSubsistence
					},
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region On Hire / Installed

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem("On Hire / Installed Jobs", 15)]
		public ActionResult OnHireInstalled()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveOnHireInstalled(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveOnHireInstalled(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Task",
					"Job",
					"Job Title",
					"Install Date",
					"Issued By",
					"Equipment",
					"Type",
					"Quantity",
					"Asset Numbers",
					"Collection Task",
					"Collection Status",
					"Collection Date",
					"Address"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Task.ToString(),
						a.Job.ToString(),
						a.JobTitle,
						a.InstallDate.ToShortDateString() + " " + a.InstallDate.ToShortTimeString(),
						a.IssuedBy,
						a.Equipment,
						a.Type,
						a.Quantity.ToString(),
						a.AssetNumbers,
						a.CollectionTask?.ToString(),
						a.CollectionStatus,
						a.CollectionDate != null ? a.CollectionDate.Value.ToShortDateString() + " " + a.CollectionDate.Value.ToShortTimeString() : "",
						a.Address
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/WorkInstruction/Update/" + a.WorkInstructionID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Tracker

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(20)]
		public ActionResult Tracker()
		{
			var model = new TrackerViewModel();
			model.Date = DateTime.Today;

			return View(model);
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveTracker(int rowCount, int page, string query, Guid? contractID, DateTime? date)
		{
			var results = _reportService.GetPagedActiveTracker(rowCount, page, HttpUtility.HtmlEncode(query), contractID, date);

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Contract",
					"Job",
					"Task",
					"TM requirement",
					"Address",
					"Depot",
					"Status",
					"Date Booking",
					"Booked by",
					"Date Installed",
					"Requested Install Time",
					"Time Arrived",
					"Success Time / Type",
					"Cancelled / Abort Details",
					"Notes"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.ContractNumber,
						a.JobNumber.ToString(),
						a.TaskNumber.ToString(),
						a.Activities,
						a.Address,
						a.Depot,
						a.Status,
						a.WorkInstructionCreatedDate.ToShortDateString(),
						a.IssuedBy,
						a.StartDate.ToShortDateString(),
						a.StartDate.ToShortTimeString(),
						a.TimeArrived != null ? a.TimeArrived.Value.ToShortTimeString() : "",
						a.IsSuccessful != null ? (a.IsSuccessful.Value ? "Yes" : "No") : "",
						a.AbortReason,
						a.TaskDetails
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/WorkInstruction/Update/" + a.WorkInstructionID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Performance

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(16)]
		public ActionResult Performance()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActivePerformance(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActivePerformance(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Task",
					"Job",
					"Customer",
					"Contract Manager",
					"Contract Number",
					"Job Title",
					"Start Date",
					"Job Type",
					"Activity",
					"Foreman",
					"Scheme",
					"Successful",
					"Reason Code",
					"Reason Notes"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Task.ToString(),
						a.Job.ToString(),
						a.Customer,
						a.ContractManager,
						a.ContractNumber,
						a.JobTitle,
						a.StartDate.ToShortDateString(),// + " " + a.StartDate.ToShortTimeString(),
						a.JobType,
						a.Activity,
						a.Foreman,
						a.SchemeTitle,
						a.IsSuccessful != null ? (a.IsSuccessful.Value ? "Yes" : "No") : "",
						a.ReasonCode,
						a.ReasonNotes
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/WorkInstruction/Update/" + a.WorkInstructionID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Planned Works

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(16)]
		public ActionResult PlannedWorks()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActivePlannedWorks(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActivePlannedWorks(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Task",
					"Job",
					"Customer",
					"Contract Manager",
					"Contract Number",
					"Job Title",
					"Start Date",
					"SRW/Permit Number",
					"Issued By",
					"Scheduled",
					"Status",
					"Successful",
					"Job Type",
					"Address"
				},
				Rows = results.Data.Select(a => new TableRow
				{
					TableData = new string[]
					{
						a.Task.ToString(),
						a.Job.ToString(),
						a.Customer,
						a.ContractManager,
						a.ContractNumber,
						a.JobTitle,
						a.StartDate.ToShortDateString() + " " + a.StartDate.ToShortTimeString(),
						a.SRW,
						a.IssuedBy,
						a.Scheduled ? "Yes" : "No",
						a.Status,
						a.Successful != null ? (a.Successful.Value ? "Yes" : "No") : "",
						a.JobType,
						a.Address
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/WorkInstruction/Update/" + a.WorkInstructionID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Tasks

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(24)]
		public ActionResult Tasks()
		{
			var vm = new ActiveTasksViewModel();
			vm.Tasks = new List<TaskViewModel>();
			var workInstructions = _reportService.GetActiveTasks();

			foreach (var workInstruction in workInstructions.OrderBy(o => o.WorkInstructionNumber))
			{
				var job = _jobService.Single((Guid)workInstruction.JobID);
				var task = new TaskViewModel()
				{
					TaskID = workInstruction.WorkInstructionID,
					TaskNumber = workInstruction.WorkInstructionNumber,
					JobNumber = (int)workInstruction.JobNumber,
					CustomerName = job.Customer,
					ContractManager = job.ContractsManager,
					ContractNumber = job.ContractNumber,
					JobTitle = job.JobTitle,
					Depot = _depotService.Single(workInstruction.DepotID).DepotName,
					IssuedBy = workInstruction.Employee.EmployeeName,
					Scheduled = workInstruction.IsScheduled ? "Yes" : "No",
					TaskStatus = workInstruction.Status,
					TaskType = workInstruction.JobType,
					TaskDate = workInstruction.StartDate,
					Invoiced = workInstruction.Status == "Invoiced",
					InvoiceDate = workInstruction.InvoiceDate					
				};

				vm.Tasks.Add(task);
				vm.InvoiceDate = DateTime.Today;
			}
			return View(vm);
		}

		[Authorize(Roles = "Supervisor, Manager")]
		[HttpPost]
		public ActionResult Tasks(ActiveTasksViewModel vm)
		{
			if (vm.Tasks.Any())
			{
				List<Guid> selectedTaskIDs = new List<Guid>();
				foreach(var task in vm.Tasks.Where(x => x.Invoiced))
                {
					selectedTaskIDs.Add(task.TaskID);
                }

				if (selectedTaskIDs.Count() > 0)
				{
					var res = _reportService.SetTasksToInvoiced(selectedTaskIDs, vm.InvoiceDate);
					if (res)
                    {
						return RedirectToAction("Tasks");
					}
					ModelState.AddModelError("","Unable to update Tasks");
					return View(vm);
				}
			}			

			return RedirectToAction("Tasks");
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveTasks(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveTasks(rowCount, page, HttpUtility.HtmlEncode(query));

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Task Number",
					"Job Number",
					"Customer",
					"Contract Manager",
					"Contract Number",
					"Job Title",
					"Start Date",
					"Issued By",
					"Scheduled",
					"Depot",
					"Task Type",
					"Status",
					"Invoiced",
					"Invoice Date"
				},
				Rows = results.Data.Select(a => new TableRow
				{

					TableData = new string[]
					{
						a.WorkInstructionNumber.ToString(),
						a.JobNumber.ToString(),
						a.CustomerName,
						a.ContractManager,
						a.ContractNumber,
						a.JobTitle,
						a.StartDate.ToShortDateString(),
						a.IssuedBy,
						a.Scheduled,
						a.Depot.DepotName,
						a.TaskType.JobType1,
						a.Status.Status1,
						a.Status.Status1 == "Invoiced" ? "Yes" : "No",
						a.InvoiceDate.HasValue ? a.InvoiceDate.ToString().Substring(0,10) : ""
					},
					Actions = new List<TableAction>
					{
						// new TableAction("/Contract/WorkInstruction/Update/" + a.WorkInstructionID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Commercial Report

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(25)]
		public ActionResult Commercial()
        {
			return View();
        }

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveCommercial(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveCommercial(rowCount, page, HttpUtility.HtmlEncode(query));


            results.Data.ForEach(delegate(CommercialModel m) 
			{
				m.ShiftType = m.StartDate.TimeOfDay < new TimeSpan(7, 0, 0) || m.FinishDate.TimeOfDay > new TimeSpan(17, 0, 0) ? "N" : "D";
				//m.TMResource = m.PriceLines.Any() ? String.Join(", ", m.PriceLines.Where(p => p.IsActive).Select(l => l.PriceLine.Description.Trim())) : "";
				m.VehicleReg = m.Shift == null ? "" : m.Shift.ShiftVehicles.Any() ? String.Join(", ", m.Shift.ShiftVehicles.Select(v => v.Vehicle.Registration)) : "";
				m.QuantityShift = m.PriceLines.Any() ? String.Join(", ", m.PriceLines.Where(p => p.IsActive).Select(l => l.PriceLine.Description.Trim())) : "";
				m.Cost = m.PriceLines.Where(p => p.IsActive).Sum(s => s.Rate.GetValueOrDefault());
				m.Concatenate = m.Date.ToString("dd-MM-yyyy") + "/" + m.Job.Contract.ContractNumber + "/" + m.Job.CustomerProjectCode + "/" + m.PriceLines.Where(p => p.IsActive).Sum(s => s.Rate.GetValueOrDefault()).ToString("F");

			});

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Depot",
					"Date",
					"Shift",
					"Chargeable",
					"TM Project Code",
					"Customer",
					"Authoriser",
					"Cust Project Code",
					"Address",
					"TM Resource",
					"Quantity",
					"Rate",
					"Total",
					"TM Activities",
					"Task Details",
					"FMC VRN",
					//"Qty. Shift",
					//"Cost",
					"Concatenate"
				},
				Rows = results.Data.Select(a => new TableRow
				{

					TableData = new string[]
					{
						a.Depot,
						a.Date.ToShortDateString(),
						a.ShiftType,
						a.Chargeable,
						a.TMProjectCode,
						a.Customer,
						a.Authoriser,
						a.CustProjectCode,
						a.Address,
						a.TMResource,
						a.Quantity.ToString("F"),
						a.Rate.ToString("F"),
						a.Total.ToString("F"),
						a.TMActivities.Trim(),
						a.TaskDetails.Trim(),
						a.VehicleReg,
						//a.QuantityShift.ToString(),
						//a.Cost.ToString("F"),
						a.Concatenate
					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/WorkInstruction/Update/" + a.WorkInstructionID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region On Hire report

		[Authorize(Roles = "Supervisor, Manager")]
		[NavigationMenuItem(26)]
		public ActionResult OnHire()
		{
			return View();
		}

		[Authorize(Roles = "Supervisor, Manager")]
		public ActionResult ActiveOnHire(int rowCount, int page, string query)
		{
			var results = _reportService.GetPagedActiveOnHire(rowCount, page, HttpUtility.HtmlEncode(query));


            results.Data.ForEach(delegate (OnHireReportModel m)
            {
				m.ShiftType = m.PriceLines.Any()
					? m.PriceLines.Where(p => p.PriceLine.Code.EndsWith("HIR")).Any()
						? "WH"
						: m.StartDate.TimeOfDay < new TimeSpan(7, 0, 0) || m.FinishDate.TimeOfDay > new TimeSpan(17, 0, 0)
							? "N"
							: "D"
					: m.StartDate.TimeOfDay < new TimeSpan(7, 0, 0) || m.FinishDate.TimeOfDay > new TimeSpan(17, 0, 0) ? "N" : "D";
				m.SORCode = m.TaskEquipment.Task.WorkInstruction.WorkInstructionPriceLines.Any() ? String.Join(", ", m.TaskEquipment.Task.WorkInstruction.WorkInstructionPriceLines.Select(s => s.PriceLine.Code)) : "";
				m.EquipOnHire = m.PriceLines.Any() ? m.PriceLines.Where(p => p.IsActive && p.PriceLine.Code.EndsWith("ON HIRE")).Select(s => s.PriceLine.Description).FirstOrDefault() : "";
				m.TMResource = m.PriceLines.Any() ? String.Join(", ", m.PriceLines.Where(p => p.IsActive).Select(l => l.PriceLine.Description.Trim())) : "";
				m.SORCode = m.PriceLines.Any() ? String.Join(", ", m.PriceLines.Select(s => s.PriceLine.Code)) : "";
				m.SORDescription = m.PriceLines.Any() ? String.Join(", ", m.PriceLines.Where(p => p.IsActive).Select(l => l.PriceLine.Description.Trim())) : "";
				m.Vehicles = m.Shift == null ? "" : m.Shift.ShiftVehicles.Any() ? String.Join(", ", (m.Shift.ShiftVehicles.Select(v => v.Vehicle.Make + " (" + v.Vehicle.Registration + ")"))) : "";
				m.Names = m.Shift == null ? "" : m.Shift.ShiftStaffs.Any() ? String.Join(", ", m.Shift.ShiftStaffs.Select(s => s.Employee.EmployeeName)) : "";
			});

			var model = new PagedTable
			{
				Page = results.Page,
				PageCount = results.PageCount,
				ColumnHeaders = new string[]
				{
					"Day",
					"Date",
					"Shift",
					"Equipment on Hire",
					"Qty On Hire",
					"On Hire",
					"TM Project Code",
					"Customer",
					"Authoriser",
					"Cust Project Code",
					"Chargeable",
					"Address",
					"Borough",
					"TM Resource",
					"Quantity",
					"Works Description",
					"Site Contact",
					"SOR Code",
					"SOR Description",
					"Job Notes",
					"Names",
					"Vehicles",
					"Asset Number",
					"Asset Description"
				},
				Rows = results.Data.Select(a => new TableRow
				{

					TableData = new string[]
					{
						a.Day.DayOfWeek.ToString(),
						a.Date.ToShortDateString(),
						a.ShiftType,
						a.EquipOnHire,
						a.QuantOnHire.ToString(),
						a.OnHire.ToString(),
						a.TMProjectCode,
						a.Customer,
						a.Authoriser,
						a.CustProjectCode,
						a.Chargeable,
						a.Address,
						a.Borough,
						a.TMResource,
						a.Quantity.ToString(),
						a.WorksDescription,
						a.SiteContact,
						a.SORCode,
						a.SORDescription,
						a.JobNotes,
						a.Names,
						a.Vehicles,
						a.AssetNumber,
						a.AssetDescription

					},
					Actions = new List<TableAction>
					{
						new TableAction("/Contract/WorkInstruction/Update/" + a.WorkInstructionID, "View", "icon-pencil")
					}
				}).ToList()
			};

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		#endregion
	}
}
