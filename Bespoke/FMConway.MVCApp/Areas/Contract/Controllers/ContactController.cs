﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Contacts;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Contacts.Interfaces;
using FMConway.Services.Contacts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    public class ContactController : Controller
    {
        IContactService _contactService;

        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        public ActionResult New(Guid id)
        {
            ContactViewModel model = new ContactViewModel()
            {
                Contact = new ContactModel()
                {
                    CustomerID = id
                }
            };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        [HttpPost]
        public ActionResult New(ContactViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Guid contactID = _contactService.Create(model.Contact);
            }

            return RedirectToAction("Update", "Customer", new { id = model.Contact.CustomerID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        public ActionResult Update(Guid id)
        {
            ContactViewModel model = new ContactViewModel()
            {
                Contact = _contactService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        [HttpPost]
        public ActionResult Update(ContactViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _contactService.Update(model.Contact);
            }

            return RedirectToAction("Update", "Customer", new { id = model.Contact.CustomerID });
        }

        #region TableActions
        Func<ContactModel, TableAction> updateAction = cc =>
        {
            return new TableAction()
            {
                Url = "/Contract/Contact/Update/" + cc.ContactID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Customers")]
        public ActionResult ActiveContacts(int rowCount, int page, string query, Guid customerId)
        {
            PagedResults<ContactModel> results = _contactService.GetActiveCustomerContactsOverview(rowCount, page, query, customerId);

            PagedTable model = ContactOverviewTable(results, new List<Func<ContactModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable ContactOverviewTable(PagedResults<ContactModel> results, List<Func<ContactModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name", "Position", "Phone Number" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.ContactName, s.JobTitle, s.PhoneNumber },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }
    }
}
