﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Depots;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Depots.Interfaces;
using FMConway.Services.Depots.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Depots", 110, "fa fa-truck-ramp-box", "Administrator, Supervisor, Manager, Depots")]
    public class DepotController : BaseController
    {
        IDepotService _depotService;

        public DepotController(IDepotService depotService)
        {
            _depotService = depotService;
        }

        #region Depot table actions
        Func<DepotModel, TableAction> updateAction = depot =>
        {
            return new TableAction()
            {
                Url = "/Contract/Depot/Update/" + depot.DepotID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<DepotModel, TableAction> viewAction = depot =>
        {
            return new TableAction()
            {
                Url = "/Contract/Depot/Summary/" + depot.DepotID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        public ActionResult ActiveDepots(int rowCount, int page, string query)
        {
            PagedResults<DepotModel> results = _depotService.GetActiveDepotsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = DepotOverviewTable(results, new List<Func<DepotModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        public ActionResult InactiveDepots(int rowCount, int page, string query)
        {
            PagedResults<DepotModel> results = _depotService.GetInactiveDepotsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = DepotOverviewTable(results, new List<Func<DepotModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable DepotOverviewTable(PagedResults<DepotModel> results, List<Func<DepotModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Depot", "Address" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.DepotName, s.AddressLine1 + ", " + s.AddressLine2 + ", " + s.AddressLine3 + ", " + s.City + ", " + s.County + ", " + s.Postcode },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            DepotViewModel model = new DepotViewModel() { };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [HttpPost]
        public ActionResult New(DepotViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Guid depotID = _depotService.Create(model.Depot);

            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        public ActionResult Update(Guid id)
        {
            DepotViewModel model = new DepotViewModel()
            {
                Depot = _depotService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [HttpPost]
        public ActionResult Update(DepotViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _depotService.Update(model.Depot);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            DepotViewModel model = new DepotViewModel()
            {
                Depot = _depotService.Single(id)
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Depots")]
        [HttpPost]
        public ActionResult Summary()
        {
            return RedirectToAction("Inactive");
        }

        public ActionResult GetDepotLookup(int rowCount, int page, string query)
        {
            PagedResults<DepotLookup> lookup = _depotService.GetPagedActiveDepotsLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Depot" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
