﻿using FMConway.MVCApp.Areas.Contract.ViewModels.MedicalExaminations;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.Services.Collections.Paging;
using FMConway.Services.MedicalExaminations.Interfaces;
using FMConway.Services.MedicalExaminations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    public class MedicalExaminationController : BaseController
    {
        IMedicalExaminationService _medicalExaminationService;

        public MedicalExaminationController(IMedicalExaminationService medicalExaminationService)
        {
            _medicalExaminationService = medicalExaminationService;
        }

        #region Contract table actions
        Func<MedicalExaminationModel, TableAction> updateAction = medicalExamination =>
        {
            return new TableAction()
            {
                Url = "/Contract/MedicalExamination/Update/" + medicalExamination.MedicalExaminationID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, MedicalExaminations")]
        public ActionResult ActiveMedicalExaminations(int rowCount, int page, string query, Guid employeeID)
        {
            PagedResults<MedicalExaminationModel> results = _medicalExaminationService.GetActiveMedicalExaminationsPaged(rowCount, page, HttpUtility.HtmlEncode(query), employeeID);

            PagedTable model = MedicalExaminationOverviewTable(results, new List<Func<MedicalExaminationModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable MedicalExaminationOverviewTable(PagedResults<MedicalExaminationModel> results, List<Func<MedicalExaminationModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name", "Examination Type", "Date" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.MedicalExaminationName, s.MedicalExaminationType.Name, s.ExaminationDate.ToShortDateString() },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MedicalExaminations")]
        public ActionResult Create(Guid employeeID)
        {
            MedicalExaminationViewModel model = new MedicalExaminationViewModel()
            {
                MedicalExamination = new MedicalExaminationModel()
                {
                    EmployeeID = employeeID,
                    ExaminationDate = DateTime.Today
                }
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MedicalExaminations")]
        [HttpPost]
        public ActionResult Create(MedicalExaminationViewModel model, string cancel)
        {
            if (cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                _medicalExaminationService.Create(model.MedicalExamination);
            }

            return RedirectToAction("Update", "Employee", new { id = model.MedicalExamination.EmployeeID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MedicalExaminations")]
        public ActionResult Update(Guid id)
        {
            MedicalExaminationViewModel model = new MedicalExaminationViewModel()
            {
                MedicalExamination = _medicalExaminationService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, MedicalExaminations")]
        [HttpPost]
        public ActionResult Update(MedicalExaminationViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _medicalExaminationService.Update(model.MedicalExamination);
            }
            return RedirectToAction("Update", "Employee", new { id = model.MedicalExamination.EmployeeID });
        }
    }
}
