﻿using FMConway.MVCApp.Areas.Contract.ViewModels.FirstAidCertificates;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.Services.Collections.Paging;
using FMConway.Services.FirstAidCertificates.Interfaces;
using FMConway.Services.FirstAidCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    public class FirstAidCertificateController : BaseController
    {
        IFirstAidCertificateService _firstAidCertificateService;

        public FirstAidCertificateController(IFirstAidCertificateService firstAidCertificateService)
        {
            _firstAidCertificateService = firstAidCertificateService;
        }

        #region First Aid Certificates table actions
        Func<FirstAidCertificateModel, TableAction> updateAction = firstAidCertificate =>
        {
            return new TableAction()
            {
                Url = "/Contract/FirstAidCertificate/Update/" + firstAidCertificate.FirstAidCertificateID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, FirstAidCertificates")]
        public ActionResult ActiveFirstAidCertificates(int rowCount, int page, string query, Guid employeeID)
        {
            PagedResults<FirstAidCertificateModel> results = _firstAidCertificateService.GetActiveFirstAidCertificatesPaged(rowCount, page, HttpUtility.HtmlEncode(query), employeeID);

            PagedTable model = FirstAidCertificateOverviewTable(results, new List<Func<FirstAidCertificateModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable FirstAidCertificateOverviewTable(PagedResults<FirstAidCertificateModel> results, List<Func<FirstAidCertificateModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Name", "Certificate Type", "Expiry Date" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.CertificateName, s.CertificateType.Name, s.ExpiryDate.ToShortDateString() },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, FirstAidCertificates")]
        public ActionResult Create(Guid employeeID)
        {
            FirstAidCertificateViewModel model = new FirstAidCertificateViewModel()
            {
                FirstAidCertificate = new FirstAidCertificateModel()
                {
                    EmployeeID = employeeID,
                    ExpiryDate = DateTime.Today
                }
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, FirstAidCertificates")]
        [HttpPost]
        public ActionResult Create(FirstAidCertificateViewModel model, string cancel)
        {
            if (cancel == null)
            {
                List<FirstAidCertificateDocument> documents = TempData["SubmittedDocuments"] as List<FirstAidCertificateDocument>;

                if (documents == null)
                    documents = new List<FirstAidCertificateDocument>();

                if (model.UploadedDocuments != null)
                {
                    //Remove any documents that have been deleted
                    foreach (var document in documents.Reverse<FirstAidCertificateDocument>())
                    {
                        if (!model.UploadedDocuments.Any(ud => ud.Number.Equals(document.Number)))
                        {
                            documents.Remove(document);
                        }
                    }
                    //Delete the documents where the user has uploaded new files / or remove files.
                    foreach (var document in documents.Reverse<FirstAidCertificateDocument>())
                    {
                        if (model.UploadedDocuments.Where(ud => !ud.Loaded).Any(ud => ud.Number.Equals(document.Number)))
                            documents.Remove(document);
                    }
                    //Lets add the new documents
                    documents.AddRange(model.UploadedDocuments.Where(ud => !ud.Loaded));
                }
                documents.ForEach(doc => doc.Loaded = true);

                if (!ModelState.IsValid)
                {
                    TempData["SubmittedDocuments"] = documents;
                    model.UploadedDocuments = documents;

                    return View(model);
                }

                var firstAidCertificateID = _firstAidCertificateService.Create(model.FirstAidCertificate);

                if (documents != null)
                {
                    foreach (var document in documents)
                    {
                        byte[] documentData = new byte[document.Document.ContentLength];
                        document.Document.InputStream.Read(documentData, 0, documentData.Length);
                        _firstAidCertificateService.Documents.Create(firstAidCertificateID, document.Number, document.Document.FileName, document.Document.ContentType, documentData);
                    }
                }
            }

            return RedirectToAction("Update", "Employee", new { id = model.FirstAidCertificate.EmployeeID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, FirstAidCertificates")]
        public ActionResult Update(Guid id)
        {
            List<FirstAidCertificateDocument> documents = _firstAidCertificateService.Documents.GetActiveDocumentsForFirstAidCertificate(id).Select(s => new FirstAidCertificateDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Number = s.Title, Loaded = true }).ToList();
           
            FirstAidCertificateViewModel model = new FirstAidCertificateViewModel()
            {
                FirstAidCertificate = _firstAidCertificateService.Single(id),
                Documents = documents
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, FirstAidCertificates")]
        [HttpPost]
        public ActionResult Update(FirstAidCertificateViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                List<FirstAidCertificateDocument> currentDocuments = _firstAidCertificateService.Documents.GetActiveDocumentsForFirstAidCertificate(model.FirstAidCertificate.FirstAidCertificateID).Select(s => new FirstAidCertificateDocument() { DocumentID = s.DocumentID, FileID = s.FileID, Number = s.Title, Loaded = true }).ToList();
                List<FirstAidCertificateDocument> documents = currentDocuments.ToList();

                #region documents
                
                if (model.Documents != null)
                {
                    //Remove any documents that have been deleted
                    foreach (var document in documents.Reverse<FirstAidCertificateDocument>())
                    {
                        if (!model.Documents.Any(ud => ud.Number.Equals(document.Number)))
                        {
                            documents.Remove(document);
                        }
                    }
                    //Delete the documents where the user has uploaded new files / or remove files.
                    foreach (var document in documents.Reverse<FirstAidCertificateDocument>())
                    {
                        if (model.Documents.Where(ud => !ud.Loaded).Any(ud => ud.Number.Equals(document.Number)))
                            documents.Remove(document);
                    }
                    //Lets add the new documents
                    documents.AddRange(model.Documents.Where(ud => !ud.Loaded));
                }
                else
                {
                    documents.Clear();
                }

                documents.ForEach(doc => doc.Loaded = true);
                #endregion

                if (!ModelState.IsValid)
                {
                    int index = 0;
                    documents.ForEach(d => { ModelState.Remove(string.Format("Documents[{0}].Loaded", index)); index++; });

                    model.Documents = documents;

                    return View(model);
                }
                _firstAidCertificateService.Update(model.FirstAidCertificate);

                #region Documents
                if (documents.Any())
                {
                    //Add new Documents
                    foreach (var document in documents.Where(d => !d.DocumentID.HasValue))
                    {
                        byte[] documentData = new byte[document.Document.ContentLength];
                        document.Document.InputStream.Read(documentData, 0, documentData.Length);
                        _firstAidCertificateService.Documents.Create(model.FirstAidCertificate.FirstAidCertificateID, document.Number, document.Document.FileName, document.Document.ContentType, documentData);
                    }

                    //Delete removed docs
                    var docsToDelete = currentDocuments.Where(cd => !documents.Where(d => d.DocumentID.HasValue).Any(d => d.DocumentID.Value.Equals(cd.DocumentID.Value))).ToList();
                    docsToDelete.ForEach(d => _firstAidCertificateService.Documents.Inactivate(d.DocumentID.Value));
                }
                else if (currentDocuments != null & currentDocuments.Any())
                {
                    currentDocuments.ForEach(d => _firstAidCertificateService.Documents.Inactivate(d.DocumentID.Value));
                }
                #endregion
            }
            return RedirectToAction("Update", "Employee", new { id = model.FirstAidCertificate.EmployeeID });
        }
    }
}
