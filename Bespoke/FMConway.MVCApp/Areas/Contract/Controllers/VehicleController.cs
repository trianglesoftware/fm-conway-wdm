﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Vehicles;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Vehicles.Interfaces;
using FMConway.Services.Vehicles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Vehicles", 130, "fa fa-truck-pickup", "Administrator, Supervisor, Manager, Vehicles")]
    public class VehicleController : BaseController
    {
        IVehicleService _vehicleService;

        public VehicleController(IVehicleService VehicleService)
        {
            _vehicleService = VehicleService;
        }

        #region Vehicle table actions
        Func<VehicleModel, TableAction> updateAction = Vehicle =>
        {
            return new TableAction()
            {
                Url = "/Contract/Vehicle/Update/" + Vehicle.VehicleID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<VehicleModel, TableAction> viewAction = Vehicle =>
        {
            return new TableAction()
            {
                Url = "/Contract/Vehicle/Summary/" + Vehicle.VehicleID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        public ActionResult ActiveVehicles(int rowCount, int page, string query)
        {
            PagedResults<VehicleModel> results = _vehicleService.GetActiveVehiclesPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = VehicleOverviewTable(results, new List<Func<VehicleModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        public ActionResult InactiveVehicles(int rowCount, int page, string query)
        {
            PagedResults<VehicleModel> results = _vehicleService.GetInactiveVehiclesPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = VehicleOverviewTable(results, new List<Func<VehicleModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable VehicleOverviewTable(PagedResults<VehicleModel> results, List<Func<VehicleModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Vehicle Type","Make", "Model", "Registration" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.VehicleType.Name,s.Make, s.Model, s.Registration },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            VehicleViewModel model = new VehicleViewModel() { };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        [HttpPost]
        public ActionResult New(VehicleViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Guid VehicleID = _vehicleService.Create(model.Vehicle);

            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        public ActionResult Update(Guid id)
        {
            VehicleViewModel model = new VehicleViewModel()
            {
                Vehicle = _vehicleService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        [HttpPost]
        public ActionResult Update(VehicleViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _vehicleService.Update(model.Vehicle);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            VehicleViewModel model = new VehicleViewModel()
            {
                Vehicle = _vehicleService.Single(id)
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        [HttpPost]
        public ActionResult Summary(VehicleViewModel model)
        {
            _vehicleService.Update(model.Vehicle);
            return RedirectToAction("Inactive");
        }

        public ActionResult GetVehicleLookup(int rowCount, int page, string query)
        {
            PagedResults<VehicleLookup> lookup = _vehicleService.GetPagedActiveVehiclesLookup(rowCount, page, query);
            LookupModel model = new LookupModel
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Make", "Model", "Registration" },
                Rows = lookup.Data.Select(s => new LookupRow
                {
                    Key = s.Key,
                    Value = s.Value,
                    TableData = new string[]
                    {
                        s.Make,
                        s.Model,
                        s.Key
                    }
                }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #region Vehicle Absences
        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        public ActionResult NewVehicleAbsence(Guid vehicleID)
        {
            var model = new VehicleAbsenceViewModel();
            model.VehicleAbsence.VehicleID = vehicleID;
            model.VehicleAbsence.Vehicle = _vehicleService.GetVehicleRegistration(vehicleID);

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        [HttpPost]
        public ActionResult NewVehicleAbsence(VehicleAbsenceViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _vehicleService.CreateVehicleAbsence(model.VehicleAbsence);

            return RedirectToAction("Update", new { id = model.VehicleAbsence.VehicleID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        public ActionResult UpdateVehicleAbsence(Guid id)
        {
            var model = new VehicleAbsenceViewModel();
            model.VehicleAbsence = _vehicleService.GetVehicleAbsence(id);

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        [HttpPost]
        public ActionResult UpdateVehicleAbsence(VehicleAbsenceViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _vehicleService.UpdateVehicleAbsence(model.VehicleAbsence);

            return RedirectToAction("Update", new { id = model.VehicleAbsence.VehicleID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Vehicles")]
        public ActionResult ActiveAbsences(int rowCount, int page, string query, Guid vehicleID)
        {
            var results = _vehicleService.GetActiveVehicleAbsencesPaged(rowCount, page, HttpUtility.HtmlEncode(query), vehicleID);

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "From", "To", "Reason" },
                Rows = results.Data.Select(a => new TableRow
                {
                    TableData = new string[] { a.FromDate?.ToShortDateString(), a.ToDate?.ToShortDateString(), a.VehicleAbsenceReason },
                    Actions = new List<TableAction>
                    {
                        new TableAction
                        {
                            Url = "/Contract/Vehicle/UpdateVehicleAbsence/" + a.VehicleAbsenceID,
                            Title = "Edit",
                            IconClass = "icon-pencil"
                        }
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult GetVehiclesEmployee(Guid vehicleID)
        {
            var model = _vehicleService.GetVehiclesEmployee(vehicleID);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
