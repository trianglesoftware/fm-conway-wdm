﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Timesheets;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Timesheets.Interfaces;
using FMConway.Services.Timesheets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Timesheets", 40, "fa fa-clock", "Administrator, Supervisor, Manager, Timesheets")]
    public class TimesheetController : BaseController
    {
        ITimesheetService _timesheetService;

        public TimesheetController(ITimesheetService timesheetService)
        {
            _timesheetService = timesheetService;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Timesheets")]
        [NavigationMenuItem(10, "fa fa-calendar-day")]
        public ActionResult Daily()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Timesheets")]
        [NavigationMenuItem(10, "fa fa-calendar-week")]
        public ActionResult Weekly()
        {
            return View();
        }

        #region Lists

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult ActiveDailyTimesheets(int rowCount, int page, string query)
        {
            var results = _timesheetService.GetActiveDailyTimesheetsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Date",
                    "Employee",
                    "Number Of Shifts",
                    "Worked",
                    "Break",
                    "Travel"
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.WeekStart.ToShortDateString(),
                        s.EmployeeName,
                        s.NumberOfShifts.ToString(),
                        TimeSpan.FromMinutes((s.WorkedHours * 60) + s.WorkedMins).ToString(@"hh\:mm"),
                        TimeSpan.FromMinutes((s.BreakHours * 60) + s.BreakMins).ToString(@"hh\:mm"),
                        TimeSpan.FromMinutes((s.TravelHours * 60) + s.TravelMins).ToString(@"hh\:mm"),
                    },
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult ActiveTimesheetsAll(int rowCount, int page, string query)
        {
            PagedResults<TimesheetModel> results = null;

            results = _timesheetService.GetAllActiveTimesheetsPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = TimesheetListOverviewTable(results, new List<Func<TimesheetModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Timesheet")]
        public ActionResult ActiveTimesheets(int rowCount, int page, string query, Guid employeeID)
        {
            PagedResults<TimesheetModel> results = _timesheetService.GetActiveTimesheetsPaged(rowCount, page, HttpUtility.HtmlEncode(query), employeeID);

            PagedTable model = TimesheetOverviewTable(results, new List<Func<TimesheetModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Timesheet")]
        public ActionResult ActiveTimesheetRecords(int rowCount, int page, string query, Guid timesheetID)
        {
            PagedResults<TimesheetRecordModel> results = _timesheetService.GetActiveTimesheetRecordsPaged(rowCount, page, HttpUtility.HtmlEncode(query), timesheetID);

            PagedTable model = TimesheetRecordOverviewTable(results, new List<Func<TimesheetRecordModel, TableAction>>() { viewTimeSheetAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Table Overviews

        private PagedTable TimesheetRecordOverviewTable(PagedResults<TimesheetRecordModel> results, List<Func<TimesheetRecordModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Start Time",
                    "End Time",
                    "Worked",
                    "Break",
                    "Travel",
                    "Task(s)"
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.StartTime.ToString(),
                        s.EndTime.ToString(),
                        TimeSpan.FromMinutes((s.WorkedHours * 60) + s.WorkedMins).ToString(@"hh\:mm"),
                        TimeSpan.FromMinutes((s.BreakHours * 60) + s.BreakMins).ToString(@"hh\:mm"),
                        TimeSpan.FromMinutes((s.TravelHours * 60) + s.TravelMins).ToString(@"hh\:mm"),
                        s.WorkInstruction
                    },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable TimesheetOverviewTable(PagedResults<TimesheetModel> results, List<Func<TimesheetModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Week Start", "Number Of Shifts", "Worked Hours", "Worked Mins"},
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.WeekStart.ToShortDateString(), s.NumberOfShifts.ToString(), s.WorkedHours.ToString(), s.WorkedMins.ToString() },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable TimesheetListOverviewTable(PagedResults<TimesheetModel> results, List<Func<TimesheetModel, TableAction>> actions)
        {
            //TimeSpan.FromMinutes(minutes);
            //return timespan.ToString(@"hh\:mm");

            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Week Start",
                    "Employee",
                    "Number Of Shifts",
                    "Worked",
                    "Break",
                    "Travel"
                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.WeekStart.ToShortDateString(),
                        s.EmployeeName,
                        s.NumberOfShifts.ToString(),
                        TimeSpan.FromMinutes((s.WorkedHours * 60) + s.WorkedMins).ToString(@"hh\:mm"),
                        TimeSpan.FromMinutes((s.BreakHours * 60) + s.BreakMins).ToString(@"hh\:mm"),
                        TimeSpan.FromMinutes((s.TravelHours * 60) + s.TravelMins).ToString(@"hh\:mm"),
                    },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        #endregion

        #region Views

        public ActionResult CreateTimesheet(Guid employeeID)
        {
            TimesheetRecordViewModel model = new TimesheetRecordViewModel() 
            {
                EmployeeID = employeeID,
                StartHour = 9,
                FinishHour = 17,
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateTimesheet(TimesheetRecordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model.ShiftID.Equals(Guid.Empty))
            {
                ModelState.AddModelError("TimesheetRecord", "No shifts exist.");
                return View(model);
            }

            DateTime date;
            if (DateTime.TryParse(model.ShiftDate, out date))
            {
                model.StartTime = date;
            }
            else
            {
                ModelState.AddModelError("ShiftDate", "Please try again.");
                return View(model); 
            }

            if (model.BreakHours == 0 && model.BreakMins == 0 && model.WorkedHours == 0 && model.WorkedMins == 0)
            {
                ModelState.AddModelError("Empty", "Please enter some hours.");
                return View(model);
            }

            Guid timesheetID = _timesheetService.CreateTimesheet(model.EmployeeID, model.StartTime);

            if (timesheetID != Guid.Empty)
            {
                TimesheetRecordModel record = new TimesheetRecordModel() 
                {                    
                    TimesheetID = timesheetID,
                    StartTime = model.StartTime.Date.AddHours(model.StartHour).AddMinutes(model.StartMin),
                    EndTime = model.StartTime.Date.AddHours(model.FinishHour).AddMinutes(model.FinishMin),
                    PaidBreakAllowance = model.PaidBreakAllowance,
                    IPVAllowance = model.IPVAllowance,
                    OnCallAllowance = model.OnCallAllowance,
                    WorkedHours = model.WorkedHours,
                    WorkedMins = model.WorkedMins,
                    BreakHours = model.BreakHours,
                    BreakMins = model.BreakMins,
                    TravelHours = model.TravelHours,
                    TravelMins = model.TravelMins,
                    ShiftID = model.ShiftID
                };

                Guid id = _timesheetService.CreateTimesheetRecord(record);

                if (id != Guid.Empty)
                {
                    return RedirectToAction("UpdateTimesheet", new { id = timesheetID });
                }                
            }

            ModelState.AddModelError("Timesheet", "Failed to create timesheet.");
            return View(model);
        }

        public ActionResult UpdateTimesheet(Guid id)
        {
            TimesheetViewModel model = new TimesheetViewModel();

            var t = _timesheetService.Single(id);

            if (t != null)
            {
                model.EmployeeID = t.EmployeeID;
            }

            model.TimesheetID = id;
           
            return View(model);
        }

        public ActionResult CreateTimesheetRecord(Guid timesheetID)
        {
            var timesheet = _timesheetService.Single(timesheetID);

            TimesheetRecordViewModel model = new TimesheetRecordViewModel()
            {
                EmployeeID = timesheet.EmployeeID,
                TimesheetID = timesheet.TimesheetID,
                StartHour = 9,
                FinishHour = 17,
                WeekStart = timesheet.WeekStart
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateTimesheetRecord(TimesheetRecordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model.ShiftID.Equals(Guid.Empty))
            {
                ModelState.AddModelError("TimesheetRecord", "No shifts exist.");
                return View(model); 
            }

            TimesheetRecordModel record = new TimesheetRecordModel()
            {
                TimesheetID = model.TimesheetID,
                StartTime = model.StartTime.Date.AddHours(model.StartHour).AddMinutes(model.StartMin),
                EndTime = model.StartTime.Date.AddHours(model.FinishHour).AddMinutes(model.FinishMin),
                PaidBreakAllowance = model.PaidBreakAllowance,
                IPVAllowance = model.IPVAllowance,
                OnCallAllowance = model.OnCallAllowance,
                WorkedHours = model.WorkedHours,
                WorkedMins = model.WorkedMins,
                BreakHours = model.BreakHours,
                BreakMins = model.BreakMins,
                TravelHours = model.TravelHours,
                TravelMins = model.TravelMins,
                ShiftID = model.ShiftID
            };

            Guid result = _timesheetService.CreateTimesheetRecord(record);

            if (result != Guid.Empty)
            {
                return RedirectToAction("UpdateTimesheetRecord", new { id = result });
            }
            else
            {
                ModelState.AddModelError("TimesheetRecord", "Failed to create timesheet record.");
                return View(model); 
            }
        }

        public ActionResult UpdateTimesheetRecord(Guid id)
        {
            TimesheetRecordModel record = _timesheetService.SingleRecord(id);

            TimesheetRecordViewModel model = new TimesheetRecordViewModel() 
            {
                TimesheetRecordID = record.TimesheetRecordID,
                StartTime = record.StartTime,
                EndTime = record.EndTime,
                PaidBreakAllowance = record.PaidBreakAllowance,
                IPVAllowance = record.IPVAllowance,
                OnCallAllowance = record.OnCallAllowance,
                WorkedHours = record.WorkedHours,
                WorkedMins = record.WorkedMins,
                BreakHours = record.BreakHours,
                BreakMins = record.BreakMins,
                TravelHours = record.TravelHours,
                TravelMins = record.TravelMins,
                WorkInstruction = record.WorkInstruction,
                TimesheetID = record.TimesheetID
            };                                   

            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateTimesheetRecord(TimesheetRecordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            TimesheetRecordModel record = new TimesheetRecordModel()
            {
                TimesheetID = model.TimesheetID,
                TimesheetRecordID = model.TimesheetRecordID,
                PaidBreakAllowance = model.PaidBreakAllowance,
                IPVAllowance = model.IPVAllowance,
                OnCallAllowance = model.OnCallAllowance,
                WorkedHours = model.WorkedHours,
                WorkedMins = model.WorkedMins,
                BreakHours = model.BreakHours,
                BreakMins = model.BreakMins,
                TravelHours = model.TravelHours,
                TravelMins = model.TravelMins
            };

            if (_timesheetService.UpdateTimesheetRecord(record))
            {
                return RedirectToAction("UpdateTimesheet", new { id = model.TimesheetID });
            }
            else
            {
                ModelState.AddModelError("TimesheetRecord", "Failed to update timesheet record.");
                return View(model);
            }
        }

        #endregion
        
        #region Timesheet table actions
        Func<TimesheetModel, TableAction> updateAction = timesheet =>
        {
            return new TableAction()
            {
                Url = "/Contract/Timesheet/UpdateTimesheet/" + timesheet.TimesheetID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<TimesheetRecordModel, TableAction> viewTimeSheetAction = tr =>
        {
            return new TableAction()
            {
                Url = "/Contract/Timesheet/UpdateTimesheetRecord/" + tr.TimesheetRecordID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        #endregion
        
        public ActionResult PrintTimesheet(Guid id)
        {
            byte[] report;
            string name;

            _timesheetService.PrintTimesheet(id, out name, out report);

            var file = File(report, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", name);

            return file;
        }

        public ActionResult GetShiftsForEmployeeLookup(int rowCount, int page, string query, Guid employeeID)
        {
            PagedResults<ShiftLookup> lookup = _timesheetService.GetPagedActiveShiftsLookup(rowCount, page, query, employeeID);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Shift"},
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };

            if (model.Rows.Count.Equals(0))
            {
                model.Rows.Add(new LookupRow() { Key = "No shifts", Value = Guid.Empty, TableData = new string[] { "No Shifts" } });
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetShiftsForTimesheetLookup(int rowCount, int page, string query, Guid timesheetID)
        {
            PagedResults<ShiftLookup> lookup = _timesheetService.GetPagedActiveShiftsForTimesheetLookup(rowCount, page, query, timesheetID);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Shift" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };

            if (model.Rows.Count.Equals(0))
            {
                model.Rows.Add(new LookupRow() { Key = "No shifts", Value = Guid.Empty, TableData = new string[] { "No Shifts" } });
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}
