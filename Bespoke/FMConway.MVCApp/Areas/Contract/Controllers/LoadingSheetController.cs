﻿using FMConway.MVCApp.Areas.Contract.ViewModels.LoadingSheets;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Controllers;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Employees.Interfaces;
using FMConway.Services.Employees.Models;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.LoadingSheets.Models;
using FMConway.Services.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Triangle.Membership.Services.Sql;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    public class LoadingSheetController : BaseController
    {
        ILoadingSheetService _loadingSheetService;
        IUserProfileService _userProfileService;
        IEmployeeService _employeeService;

        public LoadingSheetController(ILoadingSheetService loadingSheetService, IUserProfileService userProfileService, IEmployeeService employeeService)
        {
            _loadingSheetService = loadingSheetService;
            _userProfileService = userProfileService;
            _employeeService = employeeService;
        }

        #region Loading Sheet table actions
        Func<LoadingSheetModel, TableAction> updateAction = loadingSheet =>
        {
            return new TableAction()
            {
                Url = "/Contract/LoadingSheet/Update/" + loadingSheet.LoadingSheetID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets, Customer")]
        public ActionResult ActiveLoadingSheets(int rowCount, int page, string query, Guid workInstructionID)
        {
            PagedResults<LoadingSheetModel> results = _loadingSheetService.GetActiveLoadingSheetsPaged(rowCount, page, HttpUtility.HtmlEncode(query), workInstructionID);

            PagedTable model = LoadingSheetOverviewTable(results, new List<Func<LoadingSheetModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable LoadingSheetOverviewTable(PagedResults<LoadingSheetModel> results, List<Func<LoadingSheetModel, TableAction>> actions)
        {
            var isCustomer = User.IsInRole("Customer");

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Loading Sheet","Date" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.Name, s.LoadingSheetDate.ToShortDateString() },
                    Actions =
                        isCustomer ? null :
                        actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets")]
        public ActionResult New(Guid workInstructionID)
        {
            var model = new LoadingSheetViewModel();
            model.LoadingSheet = new LoadingSheetModel();
            model.LoadingSheet.LoadingSheetDate = DateTime.Today;
            model.LoadingSheet.WorkInstructionID = workInstructionID;

            var sqlMembership = new SqlMembershipService();
            var currentUser = sqlMembership.GetUserByUsername(User.Identity.Name);
            var currentUserProfile = _userProfileService.GetUserProfile(currentUser.ID);
            var currentEmployee = _employeeService.Single((Guid)currentUserProfile.EmployeeID);

            model.LoadingSheet.EmployeeID = currentEmployee.EmployeeID;
            model.LoadingSheet.Employee = new EmployeeModel
            {
                EmployeeName = currentEmployee.EmployeeName
            };

            var loadingSheetStatuses = _loadingSheetService.Status.GetLoadingSheetStatus();
            var currentStatusID = loadingSheetStatuses.Where(a => a.Status == "Current").Select(a => a.StatusID).Single();
            model.LoadingSheet.StatusID = currentStatusID;

            List<SelectListItem> loadingSheetStatus = new List<SelectListItem>();
            loadingSheetStatus.Add(new SelectListItem() { Text = "" });
            model.Status = loadingSheetStatus.Union(loadingSheetStatuses.Select(x => new SelectListItem
            {
                Text = x.Status,
                Value = x.StatusID.ToString(),
                Selected = x.StatusID == model.LoadingSheet.StatusID
            }));

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets")]
        [HttpPost]
        public ActionResult New(LoadingSheetViewModel model)
        {
            if (!ModelState.IsValid)
            {
                List<SelectListItem> loadingSheetStatus = new List<SelectListItem>();
                loadingSheetStatus.Add(new SelectListItem() { Text = "" });

                model.Status = loadingSheetStatus.Union(_loadingSheetService.Status.GetLoadingSheetStatus().Select(x => new SelectListItem
                {
                    Text = x.Status,
                    Value = x.StatusID.ToString(),
                    Selected = model.LoadingSheet.StatusID != null && model.LoadingSheet.StatusID == x.StatusID ? true : false
                }));

                return View(model);
            }

            Guid loadingSheetID = _loadingSheetService.Create(model.LoadingSheet);

            return RedirectToAction("Update", "LoadingSheet", new { id = model.LoadingSheet.LoadingSheetID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets")]
        public ActionResult Update(Guid id)
        {
            List<SelectListItem> loadingSheetStatus = new List<SelectListItem>();
            loadingSheetStatus.Add(new SelectListItem() { Text = "" });

            var loadingSheet = _loadingSheetService.Single(id);

            LoadingSheetViewModel model = new LoadingSheetViewModel()
            {
                LoadingSheet = loadingSheet,
                Status = loadingSheetStatus.Union(_loadingSheetService.Status.GetLoadingSheetStatus().Select(x => new SelectListItem
                {
                    Text = x.Status,
                    Value = x.StatusID.ToString(),
                    Selected = loadingSheet.StatusID != null && loadingSheet.StatusID == x.StatusID ? true : false
                }))
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets")]
        [HttpPost]
        public ActionResult Update(LoadingSheetViewModel model, string Cancel, string Equipment, string NewEquipmentName, string print, decimal? quantity, int? sheetOrder)
        {
            if (print != null)
            {
                var report = _loadingSheetService.Print(model.LoadingSheet.LoadingSheetID);
                var file = File(report, "application/pdf", "LoadingSheet.pdf");
                return file;
            }
            if (Equipment != null)
            {
                _loadingSheetService.AddNonStandardEquipment(model.LoadingSheet.LoadingSheetID, NewEquipmentName, quantity.Value, sheetOrder.Value);

                model.Status = _loadingSheetService.Status.GetLoadingSheetStatus().Select(x => new SelectListItem
                {
                    Text = x.Status,
                    Value = x.StatusID.ToString(),
                    Selected = x.StatusID == model.LoadingSheet.StatusID
                });

                return View(model);
            }
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    model.Status = _loadingSheetService.Status.GetLoadingSheetStatus().Select(x => new SelectListItem
                    {
                        Text = x.Status,
                        Value = x.StatusID.ToString()
                    });

                    return View(model);
                }
                _loadingSheetService.Update(model.LoadingSheet);
            }
            return RedirectToAction("Update", "LoadingSheet", new { id = model.LoadingSheet.LoadingSheetID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets")]
        public ActionResult AddEquipment(List<string> selectedEquipment, decimal quantity, string loadingSheetID, int sheetOrder)
        {
            if (selectedEquipment != null)
            {
                _loadingSheetService.AddEquipment(selectedEquipment, quantity, new Guid(loadingSheetID), sheetOrder);
            }

            return Json(Url.Action("Update", "LoadingSheet", new { id = loadingSheetID })); 
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets")]
        public ActionResult RemoveEquipment(Guid id)
        {
            var loadingSheetID = _loadingSheetService.RemoveEquipment(id);
            return RedirectToAction("Update", "LoadingSheet", new { id = loadingSheetID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, LoadingSheets")]
        public ActionResult RemoveTemplateEquipment(Guid id)
        {
            var loadingSheetID = _loadingSheetService.RemoveTemplateEquipment(id);
            return RedirectToAction("UpdateLoadingSheetTemplate", "Maintenance", new { id = loadingSheetID });
        }
    }
}
