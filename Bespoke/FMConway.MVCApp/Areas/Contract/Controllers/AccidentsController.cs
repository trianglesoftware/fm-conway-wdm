﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Accidents;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Jobs.Interfaces;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.Maintenance;
using FMConway.Services.Accidents.Interfaces;
using FMConway.Services.Accidents.Models;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.WorkInstructions.Interfaces;
using FMConway.Services.WorkInstructionDetails.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Accidents", 140, "fa fa-car-crash", "Administrator, Supervisor, Manager")]
    public class AccidentsController : BaseController
    {
        IAccidentsService _accidentsService;
        IWorkInstructionService _workInstructionService;
        IMaintenanceService _maintenanceService;
        ILoadingSheetService _loadingSheetService;
        IWorkInstructionDetailsService _workInstructionDetailsService;
        IErrorLoggingService _errorLoggingService;

        public AccidentsController(IWorkInstructionService workInstructionService, IMaintenanceService maintenanceService, ILoadingSheetService loadingSheetService, IWorkInstructionDetailsService workInstructionDetailsService, IErrorLoggingService errorLoggingService, IAccidentsService accidentsService)
        {
            _accidentsService = accidentsService;
            _workInstructionService = workInstructionService;
            _maintenanceService = maintenanceService;
            _loadingSheetService = loadingSheetService;
            _workInstructionDetailsService = workInstructionDetailsService;
            _errorLoggingService = errorLoggingService;
        }

        #region Lists

        [Authorize(Roles = "Administrator, Supervisor, Manager")]
        [NavigationMenuItem(10, "fa fa-folder-open")]
        public ActionResult WithShifts()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager")]
        [NavigationMenuItem(20, "fa fa-folder-minus")]
        public ActionResult WithoutShifts()
        {
            return View();
        }

        #endregion

        #region Tables

        [Authorize(Roles = "Administrator, Supervisor, Manager")]
        public ActionResult AccidentsWithShifts(int rowCount, int page, string query)
        {
            PagedResults<AccidentModel> results = _accidentsService.GetAccidentsWithShifts(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = AccidentOverviewTable(results, new List<Func<AccidentModel, TableAction>>() { viewAccidentAction });

            foreach (var item in model.Rows)
            {
                foreach (var action in item.Actions)
                {
                    action.Url += "?fromID=null&from=menu";
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager")]
        public ActionResult AccidentsWithoutShifts(int rowCount, int page, string query)
        {
            PagedResults<AccidentModel> results = _accidentsService.GetAccidentsWithoutShifts(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = AccidentOverviewTable(results, new List<Func<AccidentModel, TableAction>>() { viewAccidentAction });

            foreach (var item in model.Rows)
            {
                foreach (var action in item.Actions)
                {
                    action.Url += "?fromID=null&from=menu";
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAccidents(int rowCount, int page, string query, Guid shiftID, Guid workInstructionID)
        {
            try
            {
                PagedResults<AccidentModel> results = _accidentsService.GetAccidentsPaged(rowCount, page, HttpUtility.HtmlEncode(query), shiftID);

                PagedTable model = AccidentOverviewTable(results, new List<Func<AccidentModel, TableAction>>() { viewAccidentAction });

                foreach (var item in model.Rows)
                {
                    foreach (var action in item.Actions)
                    {
                        action.Url += "?fromID=" + workInstructionID.ToString() + "&from=wi";
                    }
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetAccidents", e, shiftID.ToString());
                return View();
            }
        }

        public ActionResult GetAccidentPhotos(int rowCount, int page, string query, Guid accidentID)
        {
            try
            {
                PagedResults<AccidentPhotoModel> results = _accidentsService.GetAccidentPhotosPaged(rowCount, page, HttpUtility.HtmlEncode(query), accidentID);

                PagedTable model = AccidentPhotoOverviewTable(results, new List<Func<AccidentPhotoModel, TableAction>>() { viewAccidentPhoto });

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetAccidentPhotos", e, accidentID.ToString());
                return View();
            }
        }

        public ActionResult GetAccidentsForJob(int rowCount, int page, string query, Guid jobID)
        {
            try
            {
                PagedResults<AccidentModel> results = _accidentsService.GetAccidentsForJobPaged(rowCount, page, HttpUtility.HtmlEncode(query), jobID);

                PagedTable model = AccidentOverviewTable(results, new List<Func<AccidentModel, TableAction>>() { viewAccidentAction });

                foreach (var item in model.Rows)
                {
                    foreach (var action in item.Actions)
                    {
                        action.Url += "?fromID=" + jobID.ToString() + "&from=job";
                    }
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _errorLoggingService.CreateErrorLog("GetAccidentsForJob", e, jobID.ToString());
                return View();
            }
        }

        #endregion

        #region Table actions

        Func<AccidentModel, TableAction> viewAccidentAction = accident =>
        {
            return new TableAction()
            {
                Url = "/Contract/Accidents/ViewAccident/" + accident.AccidentID,
                Title = "View",
                IconClass = "icon-pencil"
            };
        };

        Func<AccidentPhotoModel, TableAction> viewAccidentPhoto = ap =>
        {
            return new TableAction()
            {
                Url = "/File/CreateDocument/" + ap.FileID,
                Title = "View",
                IconClass = "icon-pencil"
            };
        };

        #endregion

        #region Table overviews

        private PagedTable AccidentOverviewTable(PagedResults<AccidentModel> results, List<Func<AccidentModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Description", "Location","Date" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.AccidentDescription.ToString(), s.Location, s.AccidentDate.ToString() },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        private PagedTable AccidentPhotoOverviewTable(PagedResults<AccidentPhotoModel> results, List<Func<AccidentPhotoModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Date", "Longitude", "Latitude" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.FileName, s.Longitude.ToString(), s.Latitude.ToString() },
                    Actions = actions.Select(a => a(s)).ToList()
                }).ToList()
            };

            return model;
        }

        #endregion

        #region Views

        public ActionResult ViewAccident(Guid id, Guid? fromID, string from)
        {
            var accident = _accidentsService.SingleAccident(id);

            AccidentViewModel avm = new AccidentViewModel()
            {                
                AccidentID = id,
                AccidentDate = accident.AccidentDate,
                AccidentDescription = accident.AccidentDescription,
                PeopleInvolved = accident.PeopleInvolved,
                PersonsReporting = accident.PersonsReporting,
                Witnesses = accident.Witnesses,
                Registrations = accident.Registrations,
                Location = accident.Location,
                ActionTaken = accident.ActionTaken,
                IsActive = accident.IsActive
            };

            if (from == "wi")
            {
                avm.WorkInstructionID = fromID.Value;
            }
            else if (from == "job")
            {
                avm.JobID = fromID.Value;
            }

            return View(avm);
        }

        #endregion

    }
}
