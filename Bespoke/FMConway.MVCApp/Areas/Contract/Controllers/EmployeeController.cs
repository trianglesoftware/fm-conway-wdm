﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Employees;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.MVCApp.Utility;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Employees.Interfaces;
using FMConway.Services.Employees.Models;
using FMConway.Services.Extensions;
using FMConway.Services.Maintenance;
using FMConway.Services.UserProfiles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Triangle.Membership.Interface;
using Triangle.Membership.Services.Sql;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Employees", 30, "fa fa-user", "Administrator, Supervisor, Manager, Employees")]
    public class EmployeeController : BaseController
    {
        IEmployeeService _employeeService;
        IMaintenanceService _maintenanceService;
        IUserService _userService;

        public EmployeeController(IEmployeeService employeeService, IMaintenanceService maintenanceService, IUserService userService)
        {
            _employeeService = employeeService;
            _maintenanceService = maintenanceService;
            _userService = userService;
        }

        #region Employee table actions
        Func<EmployeeModel, TableAction> updateAction = Employee =>
        {
            return new TableAction()
            {
                Url = "/Contract/Employee/Update/" + Employee.EmployeeID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<EmployeeModel, TableAction> viewAction = Employee =>
        {
            return new TableAction()
            {
                Url = "/Contract/Employee/Summary/" + Employee.EmployeeID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        public ActionResult ActiveEmployees(int rowCount, int page, string query)
        {
            PagedResults<EmployeeModel> results = _employeeService.GetActiveEmployeesPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = EmployeeOverviewTable(results, new List<Func<EmployeeModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        public ActionResult InactiveEmployees(int rowCount, int page, string query)
        {
            PagedResults<EmployeeModel> results = _employeeService.GetInactiveEmployeesPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = EmployeeOverviewTable(results, new List<Func<EmployeeModel, TableAction>>() { viewAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable EmployeeOverviewTable(PagedResults<EmployeeModel> results, List<Func<EmployeeModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Employee Name", "Employee Type", "Phone Number", "Email Address", "Username" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.EmployeeName, s.EmployeeTypeID != null ? s.EmployeeType.Name : "", s.PhoneNumber, s.EmailAddress, s.Username },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        [NavigationMenuItem(30, "fa-regular fa-pen-to-square")]
        public ActionResult New()
        {
            List<SelectListItem> employeeTypes = new List<SelectListItem>();
            employeeTypes.Add(new SelectListItem() { Text = "" });

            EmployeeViewModel model = new EmployeeViewModel() { 
                EmployeeTypes = employeeTypes.Union(_employeeService.Types.GetEmployeeTypes().Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.ID.ToString()
                }))
            };
            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        [HttpPost]
        public ActionResult New(EmployeeViewModel model)
        {
            if (!string.IsNullOrEmpty(model.Employee.EmailAddress))
                if (!EmailUtility.ValidateEmail(model.Employee.EmailAddress))
                    ModelState.AddModelError("User.Email", "Please provide a valid email.");

            if (model.AddNewUser)
            {
                if (String.IsNullOrWhiteSpace(model.Username) || String.IsNullOrWhiteSpace(model.Password) || String.IsNullOrWhiteSpace(model.ConfirmPassword))
                {
                    ModelState.AddModelError("User", "Please complete the username and passwords fields.");
                }
                else if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("Password", "Passwords do not match.");
                }
                else if (model.RoleID == null)
                {
                    ModelState.AddModelError("Roles", "The Roles field is required.");
                }
                else if (_userService.UserNameExists(model.Username))
                {
                    ModelState.AddModelError("User.UserName", "The user name '" + model.Username + "' already exists.");
                }
            }

            if (string.IsNullOrEmpty(model.Employee.EmployeeName))
            {
                ModelState.AddModelError("Employee.EmployeeName", "Employee Name is required");
            }

            if (!ModelState.IsValid)
            {
                List<SelectListItem> employeeTypes = new List<SelectListItem>();
                employeeTypes.Add(new SelectListItem() { Text = "" });

                model.EmployeeTypes = employeeTypes.Union(_employeeService.Types.GetEmployeeTypes().Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.ID.ToString(),
                    Selected = model.Employee.EmployeeTypeID != null && model.Employee.EmployeeTypeID == x.ID ? true : false
                }));

                return View(model);
            }

            Guid employeeID = _employeeService.Create(model.Employee);

            if (model.AddNewUser)
            {
                SqlUser user = new SqlUser()
                {
                    UserName = model.Username,
                    Email = model.Employee.EmailAddress,
                    IsApproved = true,
                    IsLockedOut = false
                };

                IMembershipUser newUser = _userService.Add(user, model.Password, model.ConfirmPassword);

                _maintenanceService.UserProfiles.CreateUserProfile(new UserProfileModel()
                {
                    UserID = newUser.ID,
                    EmployeeID = employeeID
                });

                Roles.AddUserToRole(model.Username, model.RoleLookup);
            }

            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        public ActionResult Update(Guid id)
        {
            var currentEmployee = _employeeService.Single(id);
            EmployeeViewModel model = new EmployeeViewModel()
            {
                Employee = currentEmployee,
                EmployeeTypes = _employeeService.Types.GetEmployeeTypes().ToList().Select(x => new SelectListItem{
                    Text = x.Name,
                    Value = x.ID.ToString(),
                    Selected = x.ID.Equals(currentEmployee.EmployeeTypeID)
                })
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        [HttpPost]
        public ActionResult Update(EmployeeViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (string.IsNullOrEmpty(model.Employee.EmployeeName))
                {
                    ModelState.AddModelError("Employee.EmployeeName", "Employee Name is required");
                }

                // Employee end date cannot be before the start date
                if (model.Employee.StartDate.HasValue && model.Employee.EndDate.HasValue)
                {
                    if (model.Employee.StartDate.Value > model.Employee.EndDate.Value)
                    {
                        ModelState.AddModelError("Employee.EndDate", "Date Left cannot be before Date Started");
                    }
                }

                if (!ModelState.IsValid)
                {
                    model.EmployeeTypes = _employeeService.Types.GetEmployeeTypes().Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.ID.ToString()
                    });

                    return View(model);
                }
                _employeeService.Update(model.Employee);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            EmployeeViewModel model = new EmployeeViewModel()
            {
                Employee = _employeeService.Single(id)
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        [HttpPost]
        public ActionResult Summary(EmployeeViewModel model)
        {
             _employeeService.Update(model.Employee);
            return RedirectToAction("Inactive");
        }

        public ActionResult GetEmployeeLookup(int rowCount, int page, string query)
        {
            PagedResults<EmployeeLookup> lookup = _employeeService.GetPagedActiveEmployeesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Employee" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeUserLookup(int rowCount, int page, string query)
        {
            PagedResults<EmployeeLookup> lookup = _employeeService.GetPagedActiveEmployeeUsersLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Employee" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeScheduleLookup(int rowCount, int page, string query, Guid? areaID, Guid? jobID, Guid? certificateTypeID, bool isAgencyStaff)
        {
            PagedResults<EmployeeLookup> lookup = _employeeService.GetPagedActiveFilteredEmployeesLookup(rowCount, page, query, areaID, jobID, certificateTypeID, isAgencyStaff);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Employee" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetContractManagerLookup(int rowCount, int page, string query)
        {
            PagedResults<EmployeeLookup> lookup = _employeeService.GetPagedActiveContractManagersLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Employee" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #region Employee Absences
        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        public ActionResult NewEmployeeAbsence(Guid employeeID)
        {
            var model = new EmployeeAbsenceViewModel();
            model.EmployeeAbsence.EmployeeID = employeeID;
            model.EmployeeAbsence.Employee = _employeeService.GetEmployeeName(employeeID);

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        [HttpPost]
        public ActionResult NewEmployeeAbsence(EmployeeAbsenceViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _employeeService.CreateEmployeeAbsence(model.EmployeeAbsence);

            return RedirectToAction("Update", new { id = model.EmployeeAbsence.EmployeeID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        public ActionResult UpdateEmployeeAbsence(Guid id)
        {
            var model = new EmployeeAbsenceViewModel();
            model.EmployeeAbsence = _employeeService.GetEmployeeAbsence(id);

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        [HttpPost]
        public ActionResult UpdateEmployeeAbsence(EmployeeAbsenceViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _employeeService.UpdateEmployeeAbsence(model.EmployeeAbsence);

            return RedirectToAction("Update", new { id = model.EmployeeAbsence.EmployeeID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Employees")]
        public ActionResult ActiveAbsences(int rowCount, int page, string query, Guid employeeID)
        {
            var results = _employeeService.GetActiveEmployeeAbsencesPaged(rowCount, page, HttpUtility.HtmlEncode(query), employeeID);

            var model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "From", "To", "Reason" },
                Rows = results.Data.Select(a => new TableRow
                {
                    TableData = new string[] { a.FromDate?.ToShortDateString(), a.ToDate?.ToShortDateString(), a.EmployeeAbsenceReason },
                    Actions = new List<TableAction>
                    {
                        new TableAction
                        {
                            Url = "/Contract/Employee/UpdateEmployeeAbsence/" + a.EmployeeAbsenceID,
                            Title = "Edit",
                            IconClass = "icon-pencil"
                        }
                    }
                }).ToList()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
