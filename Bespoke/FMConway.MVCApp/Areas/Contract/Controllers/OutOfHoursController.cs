﻿using FMConway.MVCApp.Areas.Contract.ViewModels.OutOfHours;
using FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Extensions;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.ErrorLogging.Interfaces;
using FMConway.Services.Jobs.Interfaces;
using FMConway.Services.OutOfHours.Interfaces;
using FMConway.Services.OutOfHours.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    [Users(Users.UserTypes.Contractor)]
    [NavigationMenu("Out Of Hours", 50, "fa fa-calendar-week", "Administrator, Supervisor, Manager, Tasks")]
    public class OutOfHoursController : Controller
    {
        IOutOfHoursService _outOfHoursService;
        IErrorLoggingService _errorLoggingService;

        public OutOfHoursController(IOutOfHoursService outOfHoursService, IErrorLoggingService errorLoggingService)
        {
            _outOfHoursService = outOfHoursService;
            _errorLoggingService = errorLoggingService;
        }

        Func<OutOfHoursList, TableAction> updateAction = outOfHours =>
        {
            return new TableAction()
            {
                Url = "/Contract/OutOfHours/ViewOutOfHours/" + outOfHours.WorkInstructionID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        private PagedTable OutOfHoursTable(PagedResults<OutOfHoursList> results, List<Func<OutOfHoursList, TableAction>> actions, bool shortAddress = false)
        {
            PagedTable model = new PagedTable
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[]
                {
                    "Task",
                    "Start Date",
                    "Address",
                    "Job Number",
                    "Customer",
                    "Issued By",
                    "Job Type"

                },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[]
                    {
                        s.WorkInstructionNumber,
                        s.StartDate.ToShortDateString(),
                        s.Address,
                        s.JobNumber?.ToString(),
                        s.Customer,
                        s.IssuedBy,
                        s.JobType
                    },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        [NavigationMenuItem(30, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        [NavigationMenuItem(40, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult ViewOutOfHours(Guid id)
        {
            var viewModel = _outOfHoursService.GetOutOfHoursShift(id).ToViewModel();

            return View(viewModel);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        [HttpPost]
        public ActionResult ViewOutOfHours(OutOfHoursViewModel viewModel)
        {
            if (viewModel.IsActive)
            {
                if (viewModel.NewJob)
                {
                    if (viewModel.ContractID == null)
                    {
                        ModelState.AddModelError(nameof(viewModel.ContractID), "Contract / Phase is a required field.");
                    }
                    if (viewModel.DepotID == null)
                    {
                        ModelState.AddModelError(nameof(viewModel.DepotID), "Sub Department is a required field.");
                    }
                    if (viewModel.StartDate == null)
                    {
                        ModelState.AddModelError(nameof(viewModel.StartDate), "Start Date is a required field.");
                    }
                }
                else
                {
                    if (viewModel.JobID == null)
                    {
                        ModelState.AddModelError(nameof(viewModel.JobID), "Job is a required field.");
                    }
                }
            }

            if (ModelState.IsValid)
            {
                var results = _outOfHoursService.Save(viewModel.ToModel());
                ModelState.AddModelErrors(results.Errors);

                if (!results.Errors.Any())
                {
                    return RedirectToAction(nameof(ViewOutOfHours), new { id = viewModel.WorkInstructionID });
                }
            }

            return View(viewModel);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult OutOfHoursShiftsToBeAssigned(int rowCount, int page, string query)
        {
            PagedResults<OutOfHoursList> results = _outOfHoursService.GetOutOfHoursMainPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = OutOfHoursTable(results, new List<Func<OutOfHoursList, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult AssignedOutOfHoursShifts(int rowCount, int page, string query)
        {
            PagedResults<OutOfHoursList> results = _outOfHoursService.GetAssignedOutOfHoursPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = OutOfHoursTable(results, new List<Func<OutOfHoursList, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "Administrator, Supervisor, Manager, WorkInstructions")]
        public ActionResult InactiveOutOfHoursShifts(int rowCount, int page, string query)
        {
            PagedResults<OutOfHoursList> results = _outOfHoursService.GetInactiveOutOfHoursPaged(rowCount, page, HttpUtility.HtmlEncode(query));

            PagedTable model = OutOfHoursTable(results, new List<Func<OutOfHoursList, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}
