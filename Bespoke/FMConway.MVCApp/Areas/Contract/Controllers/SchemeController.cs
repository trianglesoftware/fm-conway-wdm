﻿using FMConway.MVCApp.Areas.Contract.ViewModels.Schemes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.Services.Collections.Paging;
using FMConway.Services.Schemes.Interfaces;
using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.Controllers
{
    public class SchemeController : BaseController
    {
        ISchemeService _schemeService;

        public SchemeController(ISchemeService schemeService)
        {
            _schemeService = schemeService;
        }

        #region Contract table actions
        Func<SchemeModel, TableAction> updateAction = scheme =>
        {
            return new TableAction()
            {
                Url = "/Contract/Scheme/Update/" + scheme.SchemeID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };
        #endregion

        [Authorize(Roles = "Administrator, Supervisor, Manager, Schemes")]
        public ActionResult ActiveSchemes(int rowCount, int page, string query, Guid contractID)
        {
            PagedResults<SchemeModel> results = _schemeService.GetActiveSchemesPaged(rowCount, page, HttpUtility.HtmlEncode(query), contractID);

            PagedTable model = SchemeOverviewTable(results, new List<Func<SchemeModel, TableAction>>() { updateAction });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private PagedTable SchemeOverviewTable(PagedResults<SchemeModel> results, List<Func<SchemeModel, TableAction>> actions)
        {
            PagedTable model = new PagedTable()
            {
                Page = results.Page,
                PageCount = results.PageCount,
                ColumnHeaders = new string[] { "Title", "Description" },
                Rows = results.Data.Select(s => new TableRow
                {
                    TableData = new string[] { s.SchemeTitle, s.SchemeDescription },
                    Actions = actions.Select(action => action(s)).ToList()
                }).ToList()
            };

            return model;
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Schemes")]
        public ActionResult Create(Guid contractID)
        {
            SchemeViewModel model = new SchemeViewModel()
            {
                Scheme = new SchemeModel()
                {
                    ContractID = contractID
                }
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Schemes")]
        [HttpPost]
        public ActionResult Create(SchemeViewModel model, string cancel)
        {
            if (cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Guid schemeID = _schemeService.Create(model.Scheme);

                return RedirectToAction("Update", new { id = schemeID });
            }

            return RedirectToAction("Update", "Contract", new { id = model.Scheme.ContractID });
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Schemes")]
        public ActionResult Update(Guid id)
        {
            SchemeViewModel model = new SchemeViewModel()
            {
                Scheme = _schemeService.Single(id),
            };

            return View(model);
        }

        [Authorize(Roles = "Administrator, Supervisor, Manager, Schemes")]
        [HttpPost]
        public ActionResult Update(SchemeViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                _schemeService.Update(model.Scheme);

                return RedirectToAction("Update", new { id = model.Scheme.SchemeID });
            }
            return RedirectToAction("Update", "Contract", new { id = model.Scheme.ContractID });
        }

        public ActionResult GetSchemeLookup(int rowCount, int page, string query)
        {
            PagedResults<SchemeLookup> lookup = _schemeService.GetPagedActiveSchemesLookup(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Contract", "Scheme" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.Key, Value = s.Value, TableData = new string[] { s.Contract, s.Key } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSingle(Guid schemeID)
        {
            var model = _schemeService.Single(schemeID);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
