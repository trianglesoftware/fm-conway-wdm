﻿using FMConway.Services.MedicalExaminations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.MedicalExaminations
{
    public class MedicalExaminationViewModel
    {
        public MedicalExaminationModel MedicalExamination { get; set; }
    }
}