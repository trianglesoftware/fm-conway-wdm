﻿using FMConway.Services.Jobs.Models;
using FMConway.Services.OutOfHours.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.OutOfHours
{
    public class OutOfHoursViewModel
    {
        public Guid WorkInstructionID { get; set; }
        [Required, Display(Name = "Customer")]
        public Guid? CustomerID { get; set; }
        public string CustomerLookup { get; set; }
        [Display(Name = "Job")]
        public Guid? JobID { get; set; }
        public string JobLookup { get; set; }
        public string WorkInstructionNumber { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }

        public bool NewJob { get; set; }
        [Display(Name = "Contract / Phase")]
        public Guid? ContractID { get; set; }
        public string ContractLookup { get; set; }
        [Display(Name = "Sub Department")]
        public Guid? DepotID { get; set; }
        public string DepotLookup { get; set; }
        public string CustomerProjectCode { get; set; }
        public string JobTitle { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
    }

    public static partial class ViewModelExtensions
    {
        public static OutOfHoursModel ToModel(this OutOfHoursViewModel vm)
        {
            var m = new OutOfHoursModel();
            m.WorkInstructionID = vm.WorkInstructionID;
            m.CustomerID = vm.CustomerID;
            m.CustomerLookup = vm.CustomerLookup;
            m.JobID = vm.JobID;
            m.JobLookup = vm.JobLookup;
            m.WorkInstructionNumber = vm.WorkInstructionNumber;
            m.IsActive = vm.IsActive;
            m.IsLocked = vm.IsLocked;

            m.NewJob = vm.NewJob;
            m.ContractID = vm.ContractID;
            m.ContractLookup = vm.ContractLookup;
            m.DepotID = vm.DepotID;
            m.DepotLookup = vm.DepotLookup;
            m.CustomerProjectCode = vm.CustomerProjectCode;
            m.WorkInstructionNumber = vm.WorkInstructionNumber;
            m.JobTitle = vm.JobTitle;
            m.StartDate = vm.StartDate;
            m.FinishDate = vm.FinishDate;

            return m;
        }

        public static OutOfHoursViewModel ToViewModel(this OutOfHoursModel m)
        {
            var vm = new OutOfHoursViewModel();
            vm.WorkInstructionID = m.WorkInstructionID;
            vm.CustomerID = m.CustomerID;
            vm.CustomerLookup = m.CustomerLookup;
            vm.JobID = m.JobID;
            vm.JobLookup = m.JobLookup;
            vm.IsActive = m.IsActive;
            vm.IsLocked = m.IsLocked;

            vm.NewJob = m.NewJob;
            vm.ContractID = m.ContractID;
            vm.ContractLookup = m.ContractLookup;
            vm.DepotID = m.DepotID;
            vm.DepotLookup = m.DepotLookup;
            vm.CustomerProjectCode = m.CustomerProjectCode;
            vm.WorkInstructionNumber = m.WorkInstructionNumber;
            vm.JobTitle = m.JobTitle;
            vm.StartDate = m.StartDate;
            vm.FinishDate = m.FinishDate;

            return vm;
        }
    }
}