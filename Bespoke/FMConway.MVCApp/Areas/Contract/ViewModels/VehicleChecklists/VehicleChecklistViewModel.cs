﻿using FMConway.Services.Checklists.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.VehicleChecklists
{
    public class VehicleChecklistViewModel
    {
        public Guid ActivityID { get; set; }
        public Guid VehicleID { get; set; }

        public List<ChecklistModel> VehicleChecklists { get; set; }
    }
}