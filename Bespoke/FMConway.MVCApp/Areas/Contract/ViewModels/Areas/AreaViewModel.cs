﻿using FMConway.Services.Areas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Areas
{
    public class AreaViewModel
    {
        public AreaModel Area { get; set; }
    }
}