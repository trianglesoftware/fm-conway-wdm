﻿using FMConway.Services.MethodStatements.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.MethodStatements
{
    public class DefaultMethodStatement
    {
        public MethodStatementModel MethodStatementModel { get; set; }
        public bool Status { get; set; }
    }
}