﻿using FMConway.Services.MethodStatements.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.MethodStatements
{
    public class MethodStatementViewModel
    {
        public MethodStatementModel MethodStatement { get; set; }
        public string FileName { get; set; }
    }
}