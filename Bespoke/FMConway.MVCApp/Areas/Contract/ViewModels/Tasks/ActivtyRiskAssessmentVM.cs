﻿using FMConway.Services.Tasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Tasks
{
    public class ActivtyRiskAssessmentVM
    {
        public Guid ActivityRiskAssessmentID { get; set; }
        public Guid TaskID { get; set; }
        public Guid CleansingLogID { get; set; }
        public string RoadName { get; set; }
        public Guid RoadSpeedID { get; set; }
        public string RoadSpeed { get; set; }
        public Guid TMRequirementID { get; set; }
        public string TMRequirement { get; set; }
        public bool Schools { get; set; }
        public bool PedestrianCrossings { get; set; }
        public bool TreeCanopies { get; set; }
        public bool WaterCourses { get; set; }
        public bool OverheadCables { get; set; }
        public bool AdverseWeather { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public static partial class ViewModelExtensions
    {
        public static ActivtyRiskAssessmentVM ToViewModel(this TaskActivityRiskAssessmentModel m)
        {
            var vm = new ActivtyRiskAssessmentVM();
            vm.ActivityRiskAssessmentID = m.ActivityRiskAssessmentID;
            vm.TaskID = m.TaskID;
            vm.CleansingLogID = m.CleansingLogID;
            vm.RoadName = m.RoadName;
            vm.RoadSpeedID = m.RoadSpeedID;
            vm.RoadSpeed = m.RoadSpeed;
            vm.TMRequirementID = m.TMRequirementID;
            vm.TMRequirement = m.TMRequirement;
            vm.Schools = m.Schools;
            vm.PedestrianCrossings = m.PedestrianCrossings;
            vm.TreeCanopies = m.TreeCanopies;
            vm.WaterCourses = m.WaterCourses;
            vm.OverheadCables = m.OverheadCables;
            vm.AdverseWeather = m.AdverseWeather;
            vm.Notes = m.Notes;
            vm.CreatedDate = m.CreatedDate;

            return vm;
        }
    }
}