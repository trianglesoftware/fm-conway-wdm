﻿using FMConway.Services.Tasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Tasks
{
    public class CleansingLogViewModel
    {
        public Guid CleansingLogID { get; set; }
        public Guid TaskID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public string Road { get; set; }
        public string Section { get; set; }
        public Guid? LaneID { get; set; }
        public string LaneLookup { get; set; }
        public Guid? DirectionID { get; set; }
        public string DirectionLookup { get; set; }
        public string StartMarker { get; set; }
        public string EndMarker { get; set; }
        public decimal? GulliesCleaned { get; set; }
        public decimal? GulliesMissed { get; set; }
        public decimal? CatchpitsCleaned { get; set; }
        public decimal? CatchpitsMissed { get; set; }
        public decimal? Channels { get; set; }
        public decimal? SlotDrains { get; set; }
        public decimal? Defects { get; set; }
        public string Comments { get; set; }
        public DateTime? CreatedDate { get; set; }
        public CleansingLogPhotoModel CleansingLogPhoto { get; set; }

    }

    public static partial class ViewModelExtensions
    {
        public static CleansingLogModel ToModel(this CleansingLogViewModel vm)
        {
            var m = new CleansingLogModel();
            m.CleansingLogID = vm.CleansingLogID;
            m.TaskID = vm.TaskID;
            m.WorkInstructionID = vm.WorkInstructionID;
            m.Road = vm.Road;
            m.Section = vm.Section;
            m.LaneID = vm.LaneID;
            m.LaneLookup = vm.LaneLookup;
            m.DirectionLookup = vm.DirectionLookup;
            m.StartMarker = vm.StartMarker;
            m.EndMarker = vm.EndMarker;
            m.GulliesCleaned = vm.GulliesCleaned;
            m.GulliesMissed = vm.GulliesMissed;
            m.CatchpitsCleaned = vm.CatchpitsCleaned;
            m.CatchpitsMissed = vm.CatchpitsMissed;
            m.Channels = vm.Channels;
            m.SlotDrains = vm.SlotDrains;
            m.Comments = vm.Comments;
            m.CreatedDate = (DateTime)vm.CreatedDate;

            return m;
        }

        public static CleansingLogViewModel ToViewModel(this CleansingLogModel m)
        {
            var vm = new CleansingLogViewModel();
            vm.CleansingLogID = m.CleansingLogID;
            vm.TaskID = m.TaskID;
            vm.WorkInstructionID = m.WorkInstructionID;
            vm.Road = m.Road;
            vm.Section = m.Section;
            vm.LaneID = m.LaneID;
            vm.LaneLookup = m.LaneLookup;
            vm.DirectionLookup = m.DirectionLookup;
            vm.StartMarker = m.StartMarker;
            vm.EndMarker = m.EndMarker;
            vm.GulliesCleaned = m.GulliesCleaned;
            vm.GulliesMissed = m.GulliesMissed;
            vm.CatchpitsCleaned = m.CatchpitsCleaned;
            vm.CatchpitsMissed = m.CatchpitsMissed;
            vm.Channels = m.Channels;
            vm.SlotDrains = m.SlotDrains;
            vm.Comments = m.Comments;
            vm.CreatedDate = m.CreatedDate;

            return vm;
        }
    }
}