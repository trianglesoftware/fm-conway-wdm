﻿using FMConway.Services.Tasks.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Tasks
{
    public class UnloadingProcessViewModel
    {
        public Guid UnloadingProcessID { get; set; }
        public Guid TaskID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public DateTime? DepartToDisposalSiteDate { get; set; }
        public DateTime? ArriveAtDisposalSiteDate { get; set; }
        public Guid? DisposalSiteFK { get; set; }
        [Display(Name = "Disposal Site")]
        public string DisposalSiteLookup { get; set; }
        public string WasteTicketNumber { get; set; }
        public decimal? VolumeOfWasteDisposed { get; set; }
        public string Comments { get; set; }
        public DateTime? LeaveDisposalSiteDate { get; set; }
        public UnloadingProcessPhotoModel UnloadingProcessPhoto { get; set; }
    }

    public static partial class ViewModelExtensions
    {
        public static UnloadingProcessModel ToModel(this UnloadingProcessViewModel vm)
        {
            var m = new UnloadingProcessModel();
            m.UnloadingProcessID = vm.UnloadingProcessID;
            m.TaskID = vm.TaskID;
            m.WorkInstructionID = vm.WorkInstructionID;
            m.DepartToDisposalSiteDate = vm.DepartToDisposalSiteDate;
            m.ArriveAtDisposalSiteDate = vm.ArriveAtDisposalSiteDate;
            m.DisposalSiteFK = vm.DisposalSiteFK;
            m.DisposalSiteLookup = vm.DisposalSiteLookup;
            m.WasteTicketNumber = vm.WasteTicketNumber;
            m.VolumeOfWasteDisposed = vm.VolumeOfWasteDisposed;
            m.Comments = vm.Comments;
            m.LeaveDisposalSiteDate = vm.LeaveDisposalSiteDate;

            return m;
        }

        public static UnloadingProcessViewModel ToViewModel(this UnloadingProcessModel m)
        {
            var vm = new UnloadingProcessViewModel();
            vm.UnloadingProcessID = m.UnloadingProcessID;
            vm.TaskID = m.TaskID;
            vm.WorkInstructionID = m.WorkInstructionID;
            vm.DepartToDisposalSiteDate = m.DepartToDisposalSiteDate;
            vm.ArriveAtDisposalSiteDate = m.ArriveAtDisposalSiteDate;
            vm.DisposalSiteFK = m.DisposalSiteFK;
            vm.DisposalSiteLookup = m.DisposalSiteLookup;
            vm.WasteTicketNumber = m.WasteTicketNumber;
            vm.VolumeOfWasteDisposed = m.VolumeOfWasteDisposed;
            vm.Comments = m.Comments;
            vm.LeaveDisposalSiteDate = m.LeaveDisposalSiteDate;

            return vm;
        }
    }
}