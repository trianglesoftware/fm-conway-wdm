﻿using FMConway.Services.Tasks.Models;
using FMConway.Services.WorkInstructionDetails.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Tasks
{
    public class TaskViewModel
    {
        public Guid TaskID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public Guid? AreaCallAreaID { get; set; }
        public Guid? ChecklistID { get; set; }
        public string Name { get; set; }
        public string WINumber { get; set; }
        public string TaskType { get; set; }
        [Required(ErrorMessage = "Start date and time cannot be empty")]
        //validate:Must be greater than current date
        [DataType(DataType.DateTime)]
        public DateTime? StartTime { get; set; }
        [Required(ErrorMessage = "End date and time cannot be empty")]
        //validate:Must be greater than current date
        [DataType(DataType.DateTime)]
        public DateTime? EndTime { get; set; }
        
        public string Notes { get; set; }
        public string TrafficCount { get; set; }        
        public bool Signatures { get; set; }
        public bool Checklist { get; set; }
        public bool PhotosRequired { get; set; }
        public bool IsWeather { get; set; }
        public bool Equipment { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public HttpPostedFile TaskPhoto { get; set; }
        public decimal TaskPhotoLatitude { get; set; }
        public decimal TaskPhotoLongitude { get; set; }

        public string Weather { get; set; }
        public ChecklistAnswerModel CheckListAnswer { get; set; }
        public AreaCallModel AreaCall { get; set; }
        public TaskActivityDetailsModel ActivityDetails { get; set; }
        public FMConway.Services.Tasks.Models.TaskChecklistAnswerPhotoModel TaskChecklistAnswerPhoto { get; set; }
        public bool CleansingLog { get; set; }
        public bool UnloadingProcess { get; set; }

        public bool IsRiskAssessment { get; set; }
        public bool HasActivityRiskAssessments { get; set; }

        public List<UpdateAssetModel> TaskEquipments { get; set; }
        public List<UpdateAssetModel> AssetsInstalled { get; set; }
        public List<UpdateAssetModel> AssetsCollected { get; set; }

        public class UpdateAssetModel
        {
            public Guid AssetID { get; set; }
            public Guid EquipmentID { get; set; }
            public string ItemNumber { get; set; }
            public string AssetNumber { get; set; }
            public int Quantity { get; set; }
        }

    }
}