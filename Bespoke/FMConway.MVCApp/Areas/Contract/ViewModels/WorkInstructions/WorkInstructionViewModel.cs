﻿using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions
{
    public class WorkInstructionViewModel
    {
        public WorkInstructionModel WorkInstruction { get; set; }

        public int? FirstConeHour { get; set; }
        public int? FirstConeMin { get; set; }
        public int? LastConeHour { get; set; }
        public int? LastConeMin { get; set; }

        public int StartHour { get; set; }
        public int StartMin { get; set; }
        public int FinishHour { get; set; }
        public int FinishMin { get; set; }

        public Guid? ShiftID { get; set; }

        public bool ShowTasks { get; set; }

        public IEnumerable<SelectListItem> WorkInstructionStatus { get; set; }

        public Guid LoadingSheetTemplateID { get; set; }
        public IEnumerable<SelectListItem> LoadingSheetTemplates { get; set; }

        public Dictionary<Guid, string> Drawings { get; set; }
        public Dictionary<Guid, string> MethodStatements { get; set; }
        public Dictionary<Guid, string> ToolboxTalks { get; set; }

        public IEnumerable<SelectListItem> IsSuccessfulStates { get; set; }

        public IEnumerable<WorkInstructionDocument> UploadedDocuments { get; set; }
        public List<WorkInstructionDocument> Documents { get; set; }
        public List<Guid> DocumentsFromJob { get; set; }

        public List<WorkInstructionPriceLineModel> PriceLines { get; set; }
        public bool Invoiced { get; set; }

        public WorkInstructionViewModel()
        {
            DocumentsFromJob = new List<Guid>();
        }
    }

    public class NewWorkInstructionViewModel : WorkInstructionViewModel
    {
        //[Required(ErrorMessage = "Price Line is required")]
        public Guid? PriceLineFK { get; set; }
        public string PriceLineCode { get; set; }
        public string PriceLineDescription { get; set; }
        public decimal? PriceLineRate { get; set; }
        public string PriceLineNote { get; set; }
    }
}