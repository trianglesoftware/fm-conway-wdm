﻿using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions
{
    public class ShiftProgressVM
    {
        public Guid WorkInstructionID { get; set; }
        public Guid ShiftID { get; set; }
        public int ShiftProgressID { get; set; }
        public DateTime StartedDate { get; set; }
        public int StartedHour { get; set; }
        public int StartedMin { get; set; }

        public IEnumerable<SelectListItem> ShiftProgresses { get; set; }
    }
}