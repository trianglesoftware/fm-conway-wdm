﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.Services.WorkInstructions.Models;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions
{
    public class WorkInstructionPriceLineViewModel
    {
        public WorkInstructionPriceLineModel WorkInstructionPriceLine { get; set; }

        public WorkInstructionPriceLineViewModel()
        {
            WorkInstructionPriceLine = new WorkInstructionPriceLineModel();
        }
    }
}