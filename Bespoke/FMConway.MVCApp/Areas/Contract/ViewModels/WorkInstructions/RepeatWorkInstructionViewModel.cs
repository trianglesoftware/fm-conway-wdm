﻿using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions
{
    public class RepeatWorkInstructionViewModel
    {
        public Guid WorkInstructionID { get; set; }
        public Guid JobID { get; set; }
        public int WorkInstructionNumber { get; set; }
        public DateTime StartDate { get; set; }
        [Display(Name = "Task Details")]
        public string Comments { get; set; }
        public string EmployeeName { get; set; }
        public string TrafficManagementActivities { get; set; }
        public bool IsActive { get; set; }

        [Required]
        public string JobTypeID { get; set; }
        public IEnumerable<SelectListItem> JobType { get; set; }
        public Guid? InstallWIID { get; set; }
        public string InstallWI { get; set; }

        public Guid? ChecklistID { get; set; }
        public string Checklist { get; set; }

        public string Client { get; set; }
        public string ContractNumber { get; set; }
        public string SchemeTitle { get; set; }
        public string ContractTitle { get; set; }
        public DateTime sequenceStartDate { get; set; }
        public DateTime sequenceEndDate { get; set; }
        public string RepeatID { get; set; }
        public IEnumerable<SelectListItem> RepeatType { get; set; }
        public string WeekendInstructionID { get; set; }
        public IEnumerable<SelectListItem> WeekendInstruction { get; set; }
        public string DayFrequencyID { get; set; }
        public IEnumerable<SelectListItem> DayFrequency { get; set; }

        public IEnumerable<SelectListItem> JobTypes { get; set; }

        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }

        // the parameters to WorkInstructionServices.DuplicateWorkInstruction should really just be changed to a RepeatWorkInstructionModel.
        // for the time just going to pass the address in as its own model, but all these parameters should be refactored into a model class in future.
        public RepeatWorkInstructionAddressModel GetAddressModel()
        {
            var addressModel = new RepeatWorkInstructionAddressModel();
            addressModel.AddressLine1 = AddressLine1;
            addressModel.AddressLine2 = AddressLine2;
            addressModel.AddressLine3 = AddressLine3;
            addressModel.City = City;
            addressModel.County = County;
            addressModel.Postcode = Postcode;

            return addressModel;
        }
    }
}