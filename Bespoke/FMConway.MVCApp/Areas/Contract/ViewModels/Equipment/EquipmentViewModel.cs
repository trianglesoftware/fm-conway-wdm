﻿using FMConway.Services.Equipments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Equipment
{
    public class EquipmentViewModel
    {
        public Guid LoadingSheetID { get; set; }

        public EquipmentModel Equipment { get; set; }

        public HttpPostedFileBase Image { get; set; }

        public decimal Quantity { get; set; }

        public int SheetOrder { get; set; }

        public string ImageData { get; set; }
    }
}