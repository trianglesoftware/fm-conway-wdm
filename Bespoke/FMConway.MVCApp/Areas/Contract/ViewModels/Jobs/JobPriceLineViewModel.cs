﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.Services.Jobs.Models;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Jobs
{
    public class JobPriceLineViewModel
    {
        public JobPriceLineModel JobPriceLine { get; set; }

        public JobPriceLineViewModel()
        {
            JobPriceLine = new JobPriceLineModel();
        }
    }
}