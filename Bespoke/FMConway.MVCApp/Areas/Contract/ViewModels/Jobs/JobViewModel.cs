﻿using FMConway.Services.Jobs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Jobs
{
    public class JobViewModel
    {
        public JobModel Job { get; set; }

        public Dictionary<Guid, string> JobTypes { get; set; }
        public Dictionary<Guid, string> Areas { get; set; }
        public Dictionary<Guid, string> MethodStatements { get; set; }
        public Dictionary<Guid, string> ToolboxTalks { get; set; }
        public Dictionary<Guid, string> JobCertificates { get; set; }

        public IEnumerable<JobDocument> UploadedDocuments { get; set; }
        public List<JobDocument> Documents { get; set; }
        public IEnumerable<SelectListItem> IsSuccessfulStates { get; set; }
    }

    public class JobDocument
    {
        public Guid? FileID { get; set; }
        public Guid? DocumentID { get; set; }

        public HttpPostedFileBase Document { get; set; }
        public string Title { get; set; }
        public string Revision { get; set; }
        public bool Loaded { get; set; }
    }
}