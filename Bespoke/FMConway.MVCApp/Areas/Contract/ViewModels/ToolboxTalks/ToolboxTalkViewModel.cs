﻿using FMConway.Services.MethodStatements.Models;
using FMConway.Services.ToolboxTalks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.ToolboxTalks
{
    public class ToolboxTalkViewModel
    {
        public ToolboxTalkModel ToolboxTalk { get; set; }
    }
}