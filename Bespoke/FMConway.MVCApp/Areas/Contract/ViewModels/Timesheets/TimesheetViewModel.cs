﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Timesheets
{
    public class TimesheetViewModel
    {
        public Guid TimesheetID { get; set; }
        public Guid EmployeeID { get; set; }        
    }
}