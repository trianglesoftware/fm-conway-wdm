﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Timesheets
{
    public class TimesheetRecordViewModel
    {
        public Guid TimesheetRecordID { get; set; }
        public Guid TimesheetID { get; set; }
        public Guid EmployeeID { get; set; }
        public Guid ShiftID { get; set; }
        public string ShiftDate { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public int StartHour { get; set; }
        public int StartMin { get; set; }

        public int FinishHour { get; set; }
        public int FinishMin { get; set; }

        public DateTime WeekStart { get; set; }

        public string WorkInstruction { get; set; }

        public int WorkedHours { get; set; }
        [Range(0, 59, ErrorMessage = "Worked Mins must be between 0 and 59")]
        public int WorkedMins { get; set; }
        public int BreakHours { get; set; }
        [Range(0, 59, ErrorMessage = "Break Mins must be between 0 and 59")]
        public int BreakMins { get; set; }
        public int TravelHours { get; set; }
        [Range(0, 59, ErrorMessage = "Travel Mins must be between 0 and 59")]
        public int TravelMins { get; set; }

        public bool IPVAllowance { get; set; }
        public bool OnCallAllowance { get; set; }
        public bool PaidBreakAllowance { get; set; }
    }
}