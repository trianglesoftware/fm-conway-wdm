﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.ReasonCodes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.ReasonCodes
{
    public class ReasonCodeViewModel
    {
        [Table("ActiveReasonCodes")]
        [ColumnIgnore("ReasonCodeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<ReasonCodeModel> Active { get; set; }

        [Table("InactiveReasonCodes")]
        [ColumnIgnore("ReasonCodeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<ReasonCodeModel> Inactive { get; set; }
    }
}