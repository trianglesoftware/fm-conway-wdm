﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.Services.ReasonCodes.Models;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.ReasonCodes
{
    public class UpdateReasonCodeViewModel
    {
        public ReasonCodeModel ReasonCode { get; set; }
    }
}