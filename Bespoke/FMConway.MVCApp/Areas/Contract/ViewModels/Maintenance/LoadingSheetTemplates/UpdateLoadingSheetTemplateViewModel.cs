﻿using FMConway.Services.JobTypes.Models;
using FMConway.Services.LoadingSheetTemplates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.LoadingSheetTemplates
{
    public class UpdateLoadingSheetTemplateViewModel
    {
        public LoadingSheetTemplateModel LoadingSheetTemplate { get; set; }
    }
}