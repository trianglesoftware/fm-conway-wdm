﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.LoadingSheetTemplates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.LoadingSheetTemplates
{
    public class LoadingSheetTemplateViewModel
    {
        [Table("ActiveLoadingSheetTemplates")]
        [ColumnIgnore("LoadingSheetTemplateID", "VehicleTypeID", "VehicleType", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<LoadingSheetTemplateModel> Active { get; set; }

        [Table("InactiveLoadingSheetTemplates")]
        [ColumnIgnore("LoadingSheetTemplateID", "VehicleTypeID", "VehicleType", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<LoadingSheetTemplateModel> Inactive { get; set; }
    }
}