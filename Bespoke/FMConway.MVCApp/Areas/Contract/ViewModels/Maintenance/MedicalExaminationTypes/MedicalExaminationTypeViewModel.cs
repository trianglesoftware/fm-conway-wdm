﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.MedicalExaminationTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.MedicalExaminationTypes
{
    public class MedicalExaminationTypeViewModel
    {
        [Table("ActiveMedicalExaminationTypes")]
        [ColumnIgnore("MedicalExaminationTypeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<MedicalExaminationTypeModel> Active { get; set; }

        [Table("InactiveMedicalExaminationTypes")]
        [ColumnIgnore("MedicalExaminationTypeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<MedicalExaminationTypeModel> Inactive { get; set; }
    }
}