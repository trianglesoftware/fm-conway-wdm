﻿using FMConway.Services.JobTypes.Models;
using FMConway.Services.MedicalExaminationTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.MedicalExaminationTypes
{
    public class UpdateMedicalExaminationTypeViewModel
    {
        public MedicalExaminationTypeModel MedicalExaminationType { get; set; }
    }
}