﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.JobTypes
{
    public class JobTypeViewModel
    {
        [Table("ActiveJobTypes")]
        [ColumnIgnore("JobTypeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<JobTypeModel> Active { get; set; }

        [Table("InactiveJobTypes")]
        [ColumnIgnore("JobTypeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<JobTypeModel> Inactive { get; set; }
    }
}