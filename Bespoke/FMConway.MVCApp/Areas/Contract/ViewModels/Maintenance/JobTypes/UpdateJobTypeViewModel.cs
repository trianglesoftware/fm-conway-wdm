﻿using FMConway.Services.JobTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.JobTypes
{
    public class UpdateJobTypeViewModel
    {
        public JobTypeModel JobType { get; set; }

        public Dictionary<Guid, string> Checklists { get; set; }
    }
}