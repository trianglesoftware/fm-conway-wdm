﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Triangle.Membership.Services.Sql;
using FMConway.Services.UserProfiles.Models;
using System.ComponentModel.DataAnnotations;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.Users
{
    public class UpdateSystemUserModel
    {
        public SqlUser User { get; set; }

        [DisplayName("Role")]
        public Guid RoleID { get; set; }
        public string RoleLookup { get; set; }

        public Guid OriginalRoleID { get; set; }
        public string OriginalRoleLookup { get; set; }

        [DisplayName("Employee")]
        public Guid? EmployeeID { get; set; }
        public string EmployeeName { get; set; }

        [DisplayName("Customer")]
        public Guid? CustomerID { get; set; }
        public string CustomerName { get; set; }

        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}