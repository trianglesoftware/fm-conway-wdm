﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Triangle.Membership.Services.Sql;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.Users
{
    public class NewSystemUserModel
    {
        public SqlUser User { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmationPassword { get; set; }

        [Required]
        [DisplayName("Role")]
        public Guid RoleID { get; set; }
        public string RoleLookup { get; set; }

        [DisplayName("Employee")]
        public Guid? EmployeeID { get; set; }
        public string EmployeeName { get; set; }

        [DisplayName("Customer")]
        public Guid? CustomerID { get; set; }
        public string CustomerName { get; set; }
    }
}