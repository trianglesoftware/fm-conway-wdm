﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Triangle.Membership.Membership;
using Triangle.Membership.Services.Sql;
using Triangle.Membership.Users;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.Users
{
    public class SystemUserModel
    {
        public List<SqlUser> Users { get; set; }
    }
}