﻿using FMConway.Services.DisposalSites.Models;
using FMConway.Services.WeatherConditions.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.DisposalSites
{
    public class DisposalSiteViewModel
    {
        public Guid DisposalSiteID { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
    }

    public static partial class ViewModelExtensions
    {
        public static DisposalSiteModel ToModel(this DisposalSiteViewModel vm)
        {
            var m = new DisposalSiteModel();
            m.DisposalSiteID = vm.DisposalSiteID;
            m.Name = vm.Name;
            m.IsActive = vm.IsActive;

            return m;
        }

        public static DisposalSiteViewModel ToViewModel(this DisposalSiteModel m)
        {
            var vm = new DisposalSiteViewModel();
            vm.DisposalSiteID = m.DisposalSiteID;
            vm.Name = m.Name;
            vm.IsActive = m.IsActive;

            return vm;
        }
    }
}