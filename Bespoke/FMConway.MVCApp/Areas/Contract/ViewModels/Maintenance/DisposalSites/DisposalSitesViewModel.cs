﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.DisposalSites.Models;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.WeatherConditions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.DisposalSites
{
    public class DisposalSitesViewModel
    {
        [Table("ActiveDisposalSites")]
        [ColumnIgnore("DisposalSiteID")]
        public List<DisposalSiteList> Active { get; set; }

        [Table("InactiveDisposalSites")]
        [ColumnIgnore("DisposalSiteID")]
        public List<DisposalSiteList> Inactive { get; set; }
    }
}