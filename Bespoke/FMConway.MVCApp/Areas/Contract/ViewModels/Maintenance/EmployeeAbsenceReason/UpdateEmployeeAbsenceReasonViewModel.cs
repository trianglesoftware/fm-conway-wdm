﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.Services.EmployeeAbsenceReasons.Models;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.EmployeeAbsenceReasons
{
    public class UpdateEmployeeAbsenceReasonViewModel
    {
        public EmployeeAbsenceReasonModel EmployeeAbsenceReason { get; set; }
    }
}