﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.EmployeeAbsenceReasons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.EmployeeAbsenceReasons
{
    public class EmployeeAbsenceReasonViewModel
    {
        [Table("ActiveEmployeeAbsenceReasons")]
        [ColumnIgnore("EmployeeAbsenceReasonID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<EmployeeAbsenceReasonModel> Active { get; set; }

        [Table("InactiveEmployeeAbsenceReasons")]
        [ColumnIgnore("EmployeeAbsenceReasonID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<EmployeeAbsenceReasonModel> Inactive { get; set; }
    }
}