﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.WeatherConditions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.WeatherConditions
{
    public class WeatherConditionViewModel
    {
        [Table("ActiveWeatherConditions")]
        [ColumnIgnore("WeatherConditionID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<WeatherConditionModel> Active { get; set; }

        [Table("InactiveWeatherConditions")]
        [ColumnIgnore("WeatherConditionID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<WeatherConditionModel> Inactive { get; set; }
    }
}