﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.Services.WeatherConditions.Models;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.WeatherConditions
{
    public class UpdateWeatherConditionViewModel
    {
        public WeatherConditionModel WeatherCondition { get; set; }
    }
}