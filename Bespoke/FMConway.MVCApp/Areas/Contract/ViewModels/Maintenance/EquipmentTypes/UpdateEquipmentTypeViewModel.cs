﻿using FMConway.Services.EquipmentTypes.Models;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.EquipmentTypes
{
    public class UpdateEquipmentTypeViewModel
    {
        public EquipmentTypeModel EquipmentType { get; set; }
        public IEnumerable<SelectListItem> VmsOrAssets { get; set; }
    }
}