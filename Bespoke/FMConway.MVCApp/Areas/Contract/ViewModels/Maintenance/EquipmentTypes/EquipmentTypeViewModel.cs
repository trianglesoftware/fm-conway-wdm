﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.EquipmentTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.EquipmentTypes
{
    public class EquipmentTypeViewModel
    {
        [Table("ActiveEquipmentTypes")]
        [ColumnIgnore("EquipmentTypeID", "EquipmentTypeVmsOrAssetID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<EquipmentTypeModel> Active { get; set; }

        [Table("InactiveEquipmentTypes")]
        [ColumnIgnore("EquipmentTypeID", "EquipmentTypeVmsOrAssetID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<EquipmentTypeModel> Inactive { get; set; }
    }
}