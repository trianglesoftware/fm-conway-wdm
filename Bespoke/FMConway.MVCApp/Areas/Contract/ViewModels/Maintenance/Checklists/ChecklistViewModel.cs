﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.Checklists.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.Checklists
{
    public class ChecklistViewModel
    {
        [Table("ActiveChecklists")]
        [ColumnIgnore("ChecklistID", "IsJobTypeChecklist", "IsSignatureChecklist", "IsContractChecklist", "IsMaintenanceChecklist","IsVehicleTypeChecklist", "IsHighSpeedChecklist", "Is12cChecklist", "IsLowSpeedChecklist", "IsPreHireChecklist", "IsVmsChecklist", "IsBatteryChecklist", "IsPreHireBatteryChecklist", "IsPostHireBatteryChecklist", "IsJettingPermitToWorkChecklist", "IsJettingRiskAssessmentChecklist", "IsTankeringPermitToWorkChecklist", "IsTankeringRiskAssessmentChecklist", "IsCCTVRiskAssessmentChecklist", "IsCyclicalWorksRiskAssessmentChecklist", "CompleteAtDepot", "StartOfWork", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate", "ChecklistQuestions")]
        public List<ChecklistModel> Active { get; set; }

        [Table("InactiveChecklists")]
        [ColumnIgnore("ChecklistID", "IsJobTypeChecklist", "IsSignatureChecklist", "IsContractChecklist", "IsMaintenanceChecklist", "IsVehicleTypeChecklist", "IsHighSpeedChecklist", "Is12cChecklist", "IsLowSpeedChecklist", "IsPreHireChecklist", "IsVmsChecklist", "IsBatteryChecklist", "IsPreHireBatteryChecklist", "IsPostHireBatteryChecklist", "IsJettingPermitToWorkChecklist", "IsJettingRiskAssessmentChecklist", "IsTankeringPermitToWorkChecklist", "IsTankeringRiskAssessmentChecklist", "IsCCTVRiskAssessmentChecklist", "IsCyclicalWorksRiskAssessmentChecklist", "CompleteAtDepot", "StartOfWork", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate", "ChecklistQuestions")]
        public List<ChecklistModel> Inactive { get; set; }
    }
}