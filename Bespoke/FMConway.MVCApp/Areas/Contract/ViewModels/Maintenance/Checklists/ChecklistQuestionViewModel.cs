﻿using FMConway.Services.Checklists.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.Checklists
{
    public class ChecklistQuestionViewModel
    {
        public ChecklistQuestionModel ChecklistQuestion { get; set; }
    }
}