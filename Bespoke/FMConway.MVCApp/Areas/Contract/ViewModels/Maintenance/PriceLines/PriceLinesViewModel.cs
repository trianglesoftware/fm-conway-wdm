﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.PriceLines.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.PriceLines
{
    public class PriceLineViewModel
    {
        [Table("ActivePriceLines")]
        [ColumnIgnore("PriceLineID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<PriceLineModel> Active { get; set; }

        [Table("InactivePriceLines")]
        [ColumnIgnore("PriceLineID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<PriceLineModel> Inactive { get; set; }
    }
}