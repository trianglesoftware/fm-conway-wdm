﻿using FMConway.Services.PriceLines.Models;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.PriceLines
{
    public class UpdatePriceLineViewModel
    {
        public PriceLineModel PriceLine { get; set; }
    }
}