﻿using FMConway.Services.JobTypes.Models;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.VehicleTypes
{
    public class UpdateVehicleTypeViewModel
    {
        public VehicleTypeModel VehicleType { get; set; }

        public Dictionary<Guid, string> Checklists { get; set; }
    }
}