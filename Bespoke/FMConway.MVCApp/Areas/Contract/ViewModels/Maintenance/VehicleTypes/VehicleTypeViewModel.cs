﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.VehicleTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.VehicleTypes
{
    public class VehicleTypeViewModel
    {
        [Table("ActiveVehicleTypes")]
        [ColumnIgnore("VehicleTypeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<VehicleTypeModel> Active { get; set; }

        [Table("InactiveVehicleTypes")]
        [ColumnIgnore("VehicleTypeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<VehicleTypeModel> Inactive { get; set; }
    }
}