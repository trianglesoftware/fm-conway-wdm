﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.Services.VehicleAbsenceReasons.Models;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.VehicleAbsenceReasons
{
    public class UpdateVehicleAbsenceReasonViewModel
    {
        public VehicleAbsenceReasonModel VehicleAbsenceReason { get; set; }
    }
}