﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.VehicleAbsenceReasons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.VehicleAbsenceReasons
{
    public class VehicleAbsenceReasonViewModel
    {
        [Table("ActiveVehicleAbsenceReasons")]
        [ColumnIgnore("VehicleAbsenceReasonID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<VehicleAbsenceReasonModel> Active { get; set; }

        [Table("InactiveVehicleAbsenceReasons")]
        [ColumnIgnore("VehicleAbsenceReasonID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<VehicleAbsenceReasonModel> Inactive { get; set; }
    }
}