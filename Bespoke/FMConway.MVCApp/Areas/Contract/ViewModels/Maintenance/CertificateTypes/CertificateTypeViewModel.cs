﻿using FMConway.MVCApp.Extensions.TableGenerator.Attributes;
using FMConway.Services.JobTypes.Models;
using FMConway.Services.CertificateTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.CertificateTypes
{
    public class CertificateTypeViewModel
    {
        [Table("ActiveCertificateTypes")]
        [ColumnIgnore("CertificateTypeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<CertificateTypeModel> Active { get; set; }

        [Table("InactiveCertificateTypes")]
        [ColumnIgnore("CertificateTypeID", "IsActive", "CreatedByID", "CreatedDate", "UpdatedByID", "UpdatedDate")]
        public List<CertificateTypeModel> Inactive { get; set; }
    }
}