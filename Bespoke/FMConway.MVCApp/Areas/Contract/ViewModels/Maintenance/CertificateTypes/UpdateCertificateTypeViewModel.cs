﻿using FMConway.Services.JobTypes.Models;
using FMConway.Services.CertificateTypes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Maintenance.CertificateTypes
{
    public class UpdateCertificateTypeViewModel
    {
        public CertificateTypeModel CertificateType { get; set; }
    }
}