﻿using FMConway.Services.Schemes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Schemes
{
    public class SchemeViewModel
    {
        public SchemeModel Scheme { get; set; }
    }
}