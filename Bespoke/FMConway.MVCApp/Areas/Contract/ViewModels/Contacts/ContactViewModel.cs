﻿using FMConway.Services.Contacts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Contacts
{
    public class ContactViewModel
    {
        public ContactModel Contact { get; set; }
    }
}