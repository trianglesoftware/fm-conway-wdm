﻿using FMConway.Services.Depots.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Depots
{
    public class DepotViewModel
    {
        public DepotModel Depot { get; set; }
    }
}