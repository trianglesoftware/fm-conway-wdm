﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructionDetails
{
    public class VehicleDefectViewModel
    {
        public Guid VehicleChecklistDefectID { get; set; }
        public Guid VehicleChecklistAnswerID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public string Question { get; set; }
        public string Description { get; set; }
    }
}