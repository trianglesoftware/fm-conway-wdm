﻿using FMConway.Services.WorkInstructionDetails.Models;
using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructionDetails
{
    public class EquipmentItemChecklistViewModel
    {     
        public EquipmentItemChecklistModel EquipmentItemChecklist { get; set; }
    }
}