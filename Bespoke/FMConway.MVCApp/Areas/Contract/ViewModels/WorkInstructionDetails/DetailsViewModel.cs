﻿using FMConway.Services.WorkInstructions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructionDetails
{
    public class DetailsViewModel
    {     
        public Guid? JobID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public Guid ShiftID { get; set; }
    }
}