﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructionDetails
{
    public class MaintenanceDetailViewModel
    {
        public Guid MaintenanceCheckDetailID { get; set; }
        public Guid MaintenanceCheckAnswerID { get; set; }
        public Guid WorkInstructionID { get; set; }
        public string Description { get; set; }
        public string Question { get; set; }
    }
}