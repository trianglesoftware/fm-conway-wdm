﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructionDetails
{
    public class ShiftVehicleViewModel
    {
        public Guid ShiftVehicleID { get; set; }
        public Guid ShiftID { get; set; }
        public Guid VehicleID { get; set; }
        public Guid WorkInstructionID { get; set; }

        public string Vehicle { get; set; }
        public string Registration { get; set; }
        public string Driver { get; set; }
        [Display(Name = "Mileage")]
        public int StartMileage { get; set; }
        public int EndMileage { get; set; }
        public string ReasonNotOn { get; set; }
    }
}