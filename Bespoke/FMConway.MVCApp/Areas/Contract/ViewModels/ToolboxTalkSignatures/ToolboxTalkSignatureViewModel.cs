﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.ToolboxTalkSignatures
{
    public class ToolboxTalkSignatureViewModel
    {
        public Guid ActivityID { get; set; }
        public Guid ToolboxTalkID { get; set; }
        public List<ToolboxTalkSignature> Documents { get; set; }
    }

    public class ToolboxTalkSignature
    {
        public Guid? FileID { get; set; }
        public Guid? DocumentID { get; set; }

        public HttpPostedFileBase Document { get; set; }
        public bool Loaded { get; set; }
        public string Title { get; set; }
    }
}