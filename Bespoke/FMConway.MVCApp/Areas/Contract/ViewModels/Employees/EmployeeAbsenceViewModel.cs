﻿using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Employees
{
    public class EmployeeAbsenceViewModel
    {
        public EmployeeAbsenceViewModel()
        {
            EmployeeAbsence = new EmployeeAbsenceModel();
        }

        public EmployeeAbsenceModel EmployeeAbsence { get; set; }
    }
}