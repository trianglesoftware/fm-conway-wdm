﻿using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Employees
{
    public class EmployeeViewModel
    {
        public EmployeeModel Employee { get; set; }

        public IEnumerable<SelectListItem> EmployeeTypes { get; set; }

        public bool AddNewUser { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        [DisplayName("Role")]
        public Guid? RoleID { get; set; }
        public string RoleLookup { get; set; }
    }
}