﻿using FMConway.Services.Vehicles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Vehicles
{
    public class VehicleAbsenceViewModel
    {
        public VehicleAbsenceViewModel()
        {
            VehicleAbsence = new VehicleAbsenceModel();
        }

        public VehicleAbsenceModel VehicleAbsence { get; set; }
    }
}