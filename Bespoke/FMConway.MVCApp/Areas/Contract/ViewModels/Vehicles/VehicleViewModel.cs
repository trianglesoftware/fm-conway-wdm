﻿using FMConway.Services.MethodStatements.Models;
using FMConway.Services.ToolboxTalks.Models;
using FMConway.Services.Vehicles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Vehicles
{
    public class VehicleViewModel
    {
        public VehicleModel Vehicle { get; set; }
    }
}