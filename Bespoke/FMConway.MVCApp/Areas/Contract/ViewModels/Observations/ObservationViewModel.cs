﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Observations
{
    public class ObservationViewModel
    {
        public Guid? WorkInstructionID { get; set; }
        public Guid? JobID { get; set; }
        public Guid ObservationID { get; set; }
        public string ObservationDescription { get; set; }
        public DateTime ObservationDate { get; set; }
        public string Location { get; set; }
        public string PeopleInvolved { get; set; }
        public string CauseOfProblem { get; set; }
    }
}