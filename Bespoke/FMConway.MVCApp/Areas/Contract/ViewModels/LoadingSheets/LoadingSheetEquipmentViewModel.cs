﻿using FMConway.Services.LoadingSheets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.LoadingSheets
{
    public class LoadingSheetEquipmentViewModel
    {
        public LoadingSheetEquipmentModel Equipment { get; set; }
    }
}