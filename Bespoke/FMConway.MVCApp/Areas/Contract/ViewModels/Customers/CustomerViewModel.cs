﻿using FMConway.Services.Customers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Customers
{
    public class CustomerViewModel
    {
        public CustomerModel Customer { get; set; }
    }
}