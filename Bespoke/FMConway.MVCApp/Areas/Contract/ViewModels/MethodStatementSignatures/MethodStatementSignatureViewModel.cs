﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.MethodStatementSignatures
{
    public class MethodStatementSignatureViewModel
    {
        public Guid ActivityID { get; set; }
        public Guid MethodStatementID { get; set; }
        public List<MethodStatementSignature> Documents { get; set; }
    }

    public class MethodStatementSignature
    {
        public Guid? FileID { get; set; }
        public Guid? DocumentID { get; set; }

        public HttpPostedFileBase Document { get; set; }
        public bool Loaded { get; set; }
        public string Title { get; set; }
    }
}