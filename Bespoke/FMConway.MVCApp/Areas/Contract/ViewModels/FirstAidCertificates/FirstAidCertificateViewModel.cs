﻿using FMConway.Services.FirstAidCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.FirstAidCertificates
{
    public class FirstAidCertificateViewModel
    {
        public FirstAidCertificateModel FirstAidCertificate { get; set; }

        public IEnumerable<FirstAidCertificateDocument> UploadedDocuments { get; set; }
        public List<FirstAidCertificateDocument> Documents { get; set; }
    }

    public class FirstAidCertificateDocument
    {
        public Guid? FileID { get; set; }
        public Guid? DocumentID { get; set; }

        public HttpPostedFileBase Document { get; set; }
        public string Number { get; set; }
        public bool Loaded { get; set; }
    }
}