﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.Services.WorkInstructions.Models;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Accidents
{
    public class AccidentViewModel
    {
        public Guid AccidentID { get; set; }
        public string AccidentDescription { get; set; }
        public DateTime AccidentDate { get; set; }
        public Guid? WorkInstructionID { get; set; }
        public Guid? JobID { get; set; }
        public string Registrations { get; set; }
        public string PersonsReporting { get; set; }
        public string Location { get; set; }
        public string PeopleInvolved { get; set; }
        public string Witnesses { get; set; }
        public string ActionTaken { get;set;}
        public Guid? ShiftID { get; set; }
        public bool IsActive { get; set; }
    }
}