﻿using FMConway.Services.AreaCertificates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.AreaCertificates
{
    public class AreaCertificateViewModel
    {
        public AreaCertificateModel AreaCertificate { get; set; }

        public IEnumerable<AreaCertificateDocument> UploadedDocuments { get; set; }
        public List<AreaCertificateDocument> Documents { get; set; }
    }

    public class AreaCertificateDocument
    {
        public Guid? FileID { get; set; }
        public Guid? DocumentID { get; set; }

        public HttpPostedFileBase Document { get; set; }
        public string Number { get; set; }
        public bool Loaded { get; set; }
    }
}