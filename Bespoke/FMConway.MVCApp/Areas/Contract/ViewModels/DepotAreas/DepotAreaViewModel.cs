﻿using FMConway.Services.DepotAreas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.DepotAreas
{
    public class DepotAreaViewModel
    {
        public DepotAreaModel DepotArea { get; set; }
    }
}