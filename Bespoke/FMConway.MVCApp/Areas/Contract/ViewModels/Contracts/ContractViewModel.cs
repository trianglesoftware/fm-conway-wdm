﻿using FMConway.Services.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Contracts
{
    public class ContractViewModel
    {
        public ContractModel Contract { get; set; }
        public IEnumerable<SelectListItem> ContractType { get; set; }
        public IEnumerable<SelectListItem> ContractStatus { get; set; }
        public IEnumerable<SelectListItem> BillingRequirements { get; set; }

        public IEnumerable<ContractDocument> UploadedDocuments { get; set; }
        public List<ContractDocument> Documents { get; set; }

        //public Dictionary<Guid, string> TenderChecklists { get; set; }
    }

    public class ContractDocument
    {
        public Guid? FileID { get; set; }
        public Guid? DocumentID { get; set; }

        public HttpPostedFileBase Document { get; set; }
        public string Number { get; set; }
        public bool Loaded { get; set; }
    }
}