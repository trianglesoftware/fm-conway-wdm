﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Report
{
    public class TrackerViewModel
    {
        public Guid? ContractID { get; set; }
        public string ContractName { get; set; }

        public DateTime Date { get; set; }
    }
}