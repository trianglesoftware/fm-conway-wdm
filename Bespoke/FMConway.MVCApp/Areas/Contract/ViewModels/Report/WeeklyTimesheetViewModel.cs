﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Report
{
    public class WeeklyTimesheetViewModel
    {
        public Guid EmployeeID { get; set; }
        public string EmployeeName { get; set; }

        public DateTime StartDate { get; set; }
    }
}