﻿using FMConway.MVCApp.Areas.Contract.ViewModels.WorkInstructions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Report
{
    public class ActiveTasksViewModel
    {
        public DateTime InvoiceDate { get; set; }
        public List<TaskViewModel> Tasks { get; set; }
    }
}