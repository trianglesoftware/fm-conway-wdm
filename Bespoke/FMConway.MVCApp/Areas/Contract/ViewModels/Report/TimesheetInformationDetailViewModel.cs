﻿using FMConway.Services.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Report
{
    public class TimesheetInformationDetailViewModel
    {
        public Guid EmployeeID { get; set; }
        public string EmployeeName { get; set; }
    }
}