﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Areas.Contract.ViewModels.Report
{
    public class TaskViewModel
    {
        public Guid TaskID { get; set; }
        public int TaskNumber { get; set; }
        public int JobNumber { get; set; }
        public string CustomerName { get; set; }
        public string ContractManager { get; set; }
        public string ContractNumber { get; set; }
        public string JobTitle { get; set; }
        public DateTime TaskDate { get; set; }
        public string IssuedBy { get; set; }
        public string Scheduled { get; set; }
        public string Depot { get; set; }
        public string TaskType { get; set; }
        public string TaskStatus { get; set; }
        public bool Invoiced { get; set; }
        public DateTime? InvoiceDate { get; set; }
    }
}