﻿using FMConway.MVCApp.Areas.Operation.ViewModels.LoadingSheets;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Bootstrap.Collections.Lookup;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Collections.Paging;
using FMConway.Services.LoadingSheets.Interfaces;
using FMConway.Services.LoadingSheets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Operation.Controllers
{
    [Users(Users.UserTypes.Operation)]
    [NavigationMenu("Loading Sheets", 10, "icon-file", "LoadingSheets")]
    public class LoadingSheetController : BaseController
    {
        ILoadingSheetService _loadingSheetService;

        public LoadingSheetController(ILoadingSheetService loadingSheetService)
        {
            _loadingSheetService = loadingSheetService;
        }

        #region LoadingSheet table actions
        Func<LoadingSheetModel, TableAction> updateAction = loadingSheet =>
        {
            return new TableAction()
            {
                Url = "/Operation/LoadingSheet/Update/" + loadingSheet.LoadingSheetID,
                Title = "Edit",
                IconClass = "icon-pencil"
            };
        };

        Func<LoadingSheetModel, TableAction> viewAction = LoadingSheet =>
        {
            return new TableAction()
            {
                Url = "/Operation/LoadingSheet/Summary/" + LoadingSheet.LoadingSheetID,
                Title = "View",
                IconClass = "icon-eye-open"
            };
        };
        #endregion

        [Authorize(Roles = "LoadingSheets")]
        [NavigationMenuItem(10, "fa fa-check")]
        public ActionResult Active()
        {
            return View();
        }

        [Authorize(Roles = "LoadingSheets")]
        [NavigationMenuItem(20, "fa fa-ban")]
        public ActionResult Inactive()
        {
            return View();
        }

        public ActionResult GetLoadingSheetLookup(int rowCount, int page, string query)
        {
            PagedResults<LoadingSheetModel> lookup = _loadingSheetService.GetActiveLoadingSheetsPaged(rowCount, page, query);
            LookupModel model = new LookupModel()
            {
                Page = lookup.Page,
                PageCount = lookup.PageCount,
                ColumnHeaders = new string[] { "Loading Sheet Number" },
                Rows = lookup.Data.Select(s => new LookupRow() { Key = s.LoadingSheetNumber.ToString(), Value = s.LoadingSheetID, TableData = new string[] { s.LoadingSheetNumber.ToString() } }).ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Operation, LoadingSheets")]
        public ActionResult Update(Guid id)
        {
            List<SelectListItem> loadingSheetStatus = new List<SelectListItem>();
            loadingSheetStatus.Add(new SelectListItem() { Text = "" });

            var loadingSheet = _loadingSheetService.Single(id);

            LoadingSheetViewModel model = new LoadingSheetViewModel()
            {
                LoadingSheet = loadingSheet,
                Status = loadingSheetStatus.Union(_loadingSheetService.Status.GetLoadingSheetStatus().Select(x => new SelectListItem
                {
                    Text = x.Status,
                    Value = x.StatusID.ToString(),
                    Selected = loadingSheet.StatusID != null && loadingSheet.StatusID == x.StatusID ? true : false
                }))
            };

            return View(model);
        }

        [Authorize(Roles = "Operation, LoadingSheets")]
        [HttpPost]
        public ActionResult Update(LoadingSheetViewModel model, string Cancel)
        {
            if (Cancel == null)
            {
                if (!ModelState.IsValid)
                {
                    model.Status = _loadingSheetService.Status.GetLoadingSheetStatus().Select(x => new SelectListItem
                    {
                        Text = x.Status,
                        Value = x.StatusID.ToString()
                    });

                    return View(model);
                }
                _loadingSheetService.UpdateOperations(model.LoadingSheet);
            }
            return RedirectToAction("Active");
        }

        [Authorize(Roles = "Operation, LoadingSheets")]
        public ActionResult Summary(Guid id)
        {
            TempData["ReturnUri"] = HttpContext.Request.UrlReferrer.AbsolutePath;
            LoadingSheetViewModel model = new LoadingSheetViewModel()
            {
                LoadingSheet = _loadingSheetService.Single(id)
            };

            return View(model);
        }

        [Authorize(Roles = "Operation, LoadingSheets")]
        [HttpPost]
        public ActionResult Summary()
        {
            return RedirectToAction("Inactive");
        }
    }
}
