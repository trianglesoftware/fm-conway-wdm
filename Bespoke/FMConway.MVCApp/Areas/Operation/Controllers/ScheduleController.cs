﻿using FMConway.MVCApp.Areas.Operation.ViewModels.Schedule;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Controllers;
using FMConway.MVCApp.Menu;
using FMConway.Services.Depots.Interfaces;
using FMConway.Services.Contracts.Interfaces;
using FMConway.Services.Schedules.Interfaces;
using FMConway.Services.Schedules.Models;
using FMConway.Services.Vehicles.Interfaces;
using FMConway.Services.Vehicles.Models;
using FMConway.Services.Employees.Interfaces;
using FMConway.Services.Employees.Models;
using FMConway.Services.WorkInstructions.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FMConway.MVCApp.Extensions;
using System.Web.Routing;
using FMConway.Services.Helpers;
using System.Runtime.InteropServices;
using FMConway.Services.Customers.Interfaces;
using System.Globalization;
using System.Net;
using FMConway.Services.Extensions;

namespace FMConway.MVCApp.Areas.Operation.Controllers
{
    [NavigationMenu("Schedule", 10, "fa fa-calendar-days", "Schedule, Administrator, Supervisor, Manager")]
    public class ScheduleController : BaseController
    {
        IScheduleService _scheduleService;
        IVehicleService _vehicleService;
        IDepotService _depotService;
        IEmployeeService _employeeService;
        IWorkInstructionService _workInstructionService;
        IContractService _contractService;
        ICustomerService _customerService;

        public ScheduleController(IScheduleService scheduleService,ICustomerService customerService,  IVehicleService vehicleService, IDepotService depotService, IWorkInstructionService workInstructionService, IEmployeeService employeeService, IContractService contractService)
        {
            _scheduleService = scheduleService;
            _vehicleService = vehicleService;
            _depotService = depotService;
            _workInstructionService = workInstructionService;
            _employeeService = employeeService;
            _contractService = contractService;
            _customerService = customerService;
        }

        [Authorize(Roles = "Schedule, Administrator, Supervisor, Manager")]
        [NavigationMenuItem(20, "fa fa-calendar-day")]
        public ActionResult ViewScheduler(DateTime? startDate, Guid? contractID, Guid? depotID, List<string> selectedContracts, List<string> selectedCustomers)
        {
            // disable caching, some users are clicking browser back button and seeing a stale scheduler (edge / chrome)
            // setting "cache-control" in header didn't have any effect but setting it in Response.Cache does
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(TimeSpan.Zero);
            Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            Response.Cache.SetNoStore();

            // weekStart
            DateTime weekStart = DateTime.Now;

            var mondayDate = DateTime.Today;
            while (mondayDate.DayOfWeek != DayOfWeek.Monday)
            {
                mondayDate = mondayDate.AddDays(-1);
            }

            weekStart = mondayDate;
            
            if (startDate.HasValue)
            {
                weekStart = startDate.Value;
            }

            var viewModel = new SchedulerViewModel();
            var schedules = _scheduleService.GetActiveSchedulesForWeek(weekStart, contractID, depotID, selectedContracts, selectedCustomers);

            foreach ( var s in schedules)
            {
                var topReg = s.Vehicles.OrderBy(o => o.Registration).Take(1).SingleOrDefault();
                s.TopVehicleRegistration = topReg == null ? null : topReg.Registration;
            }

            viewModel.Schedules = schedules;
            viewModel.Vehicles = _vehicleService.GetAllActiveVehicles(weekStart);
            viewModel.Employees = _employeeService.GetAllActiveScheduleEmployees(weekStart);
            viewModel.Contracts = _contractService.GetAllActiveContracts();
            viewModel.Depots = _depotService.GetAllActiveDepots().ToSelectList(depotID, "All Depots");
            viewModel.Customers = _customerService.GetAllActiveCustomers();
            viewModel.WeekStart = weekStart;
            viewModel.ContractID = contractID;
            viewModel.DepotID = depotID;
            viewModel.SelectedContracts = selectedContracts;
            viewModel.SelectedCustomers = selectedCustomers;
            viewModel.UpdatedTime = DateTime.Now;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult ViewScheduler(SchedulerViewModel model, DateTime? startDate)
        {
            Guid? contractID = model.ContractID;

            var routeValueDictionary = new RouteValueDictionary();
            if (startDate != null)
            {
                routeValueDictionary.Add("startDate", Convert.ToDateTime(startDate));
            }

            if (model.ContractID != null)
            {
                routeValueDictionary.Add("contractID", model.ContractID);
            }

            if (model.DepotID != null)
            {
                routeValueDictionary.Add("depotID", model.DepotID);
            }

            if (model.SelectedContracts != null)
            {
                for (int i = 0; i < model.SelectedContracts.Count; i++)
                {
                    routeValueDictionary.Add("SelectedContracts[" + i + "]", model.SelectedContracts[i]);
                }  
            }

            if (model.SelectedCustomers != null)
            {
                for (int i = 0; i < model.SelectedCustomers.Count; i++)
                {
                    routeValueDictionary.Add("SelectedCustomers[" + i + "]", model.SelectedCustomers[i]);
                }
            }

            return RedirectToAction("ViewScheduler", routeValueDictionary);
        }

        [Authorize(Roles = "Schedule, Administrator, Supervisor, Manager")]
        [NavigationMenuItem(40, "fa fa-calendar-day")]
        public ActionResult ViewInlineScheduler(DateTime? day, Guid? contractID, Guid? depotID, List<string> selectedContracts, List<string> selectedCustomers)
        {
            var model = new SchedulerViewModel();

            if (!day.HasValue)
            {
                day = DateTime.Now;
            }

            DateTime weekStart = new GregorianCalendar().AddDays(day.Value, -((int)day.Value.DayOfWeek) + 1);
            var weekEnd = weekStart.AddDays(7);

            var schedule = _scheduleService.GetActiveSchedulesForDay(day.Value, contractID, depotID, selectedContracts, selectedCustomers);
            var dayTotals = _scheduleService.GetActiveSchedulesTotalsForWeek(weekStart, contractID, depotID, selectedContracts, selectedCustomers);
            model.DayTotals = dayTotals.Select(s => new ViewModels.Schedule.DayTotal { Day = s.Day,ScheduleCount = s.ScheduleCount}).ToList();

            foreach (var s in schedule)
            {
                var topReg = s.Vehicles.OrderBy(o => o.Registration).Take(1).SingleOrDefault();
                s.TopVehicleRegistration = topReg == null ? null : topReg.Registration;
            }

            model.Schedules = schedule;
            model.Vehicles = _vehicleService.GetAllActiveVehicles(day.Value);
            model.Employees = _employeeService.GetAllActiveScheduleEmployees(day.Value);
            model.Contracts = _contractService.GetAllActiveContracts();
            model.Depots = _depotService.GetAllActiveDepots().ToSelectList(depotID, "All Depots");
            model.Customers = _customerService.GetAllActiveCustomers();
            model.Day = day;
            model.WeekStart = weekStart;
            model.ContractID = contractID;
            model.DepotID = depotID;
            model.SelectedContracts = selectedContracts;
            model.SelectedCustomers = selectedCustomers;
            model.UpdatedTime = DateTime.Now;

            return View(model);
        }





        [HttpPost]
        public ActionResult ConfirmSchedule(ConfirmModel model)
        {
            var viewModel = new ScheduleResultModel();

            // scheduleID
            if (model.scheduleID == Guid.Empty)
            {
                viewModel.Response = "Failed";
                return Json(viewModel);
            }

            // check schedule last updated date
            var scheduleUpdatedDate = _scheduleService.GetScheduleUpdatedDate(model.scheduleID);
            if (scheduleUpdatedDate.TruncateMilliseconds() > model.UpdatedDate.TruncateMilliseconds())
            {
                viewModel.Response = "Outdated";
                return Json(viewModel);
            }

            // check work instruction is active
            var workInstructionIsActive = _scheduleService.IsWorkInstructionActiveForSchedule(model.scheduleID);
            if (!workInstructionIsActive)
            {
                viewModel.Response = "WorkInstructionInactive";
                return Json(viewModel);
            }

            // operatives
            foreach (Guid operativeID in model.operatives)
            {
                if (operativeID == Guid.Empty)
                {
                    viewModel.Response = "Failed";
                    return Json(viewModel);
                }
            }

            // veicles
            if (model.vehicles.Where(a => a == Guid.Empty).Any())
            {
                viewModel.Response = "Failed";
                return Json(viewModel);
            }

            // update scheduler
            var schedulerResults = _scheduleService.UpdateFromScheduler(model.scheduleID, model.vehicles, model.operatives, model.Confirmed);
            if (!schedulerResults.Errors.Any())
            {
                viewModel.Response = "Success";
                viewModel.UpdatedDate = schedulerResults.Result.UpdatedDate;

                return Json(viewModel);
            }

            viewModel.Response = "Failed";
            return Json(viewModel);
        }

        public class ConfirmModel
        {
            public ConfirmModel()
            {
                operatives = new List<Guid>();
                vehicles = new List<Guid>();
            }

            public Guid scheduleID { get; set; }
            public List<Guid> operatives { get; set; }
            public List<Guid> vehicles { get; set; }
            public bool Confirmed { get; set; }
            public DateTime UpdatedDate { get; set; }
        }

        public class ScheduleResultModel
        {
            public string Response { get; set; }
            public DateTime? UpdatedDate { get; set; }
        }

        [HttpPost]
        public ActionResult UnconfirmSchedule(Guid scheduleID, DateTime updatedDate)
        {
            var viewModel = new ScheduleResultModel();

            // check schedule last updated date
            var scheduleUpdatedDate = _scheduleService.GetScheduleUpdatedDate(scheduleID);
            if (scheduleUpdatedDate.TruncateMilliseconds() > updatedDate.TruncateMilliseconds())
            {
                viewModel.Response = "Outdated";
                return Json(viewModel);
            }

            // update scheduler
            var unconfirmResults = _scheduleService.UnconfirmSchedule(scheduleID);
            if (!unconfirmResults.Errors.Any())
            {
                viewModel.Response = "Success";
                viewModel.UpdatedDate = unconfirmResults.Result;

                return Json(viewModel);
            }

            viewModel.Response = "Failed";
            return Json(viewModel);
        }

        public ActionResult ShiftProgressUpdate(DateTime lastUpdate, Guid? contractID)
        {
            List<ProgressModel> progress = _scheduleService.GetShiftProgress(lastUpdate, contractID);
            return Json(progress);
        }

        [HttpPost]
        public JsonResult PreDropChecks(Guid id, bool isOperative, DateTime date)
        {
            var model = _scheduleService.PreDropChecks(id, isOperative, date);
            return Json(model);
        }

        [HttpPost]
        public JsonResult SaveOrderNumber(Guid workInstructionID, int orderNumber)
        {
            var results = _scheduleService.SaveOrderNumber(workInstructionID, orderNumber);
            Response.StatusCode = results.Errors.Any() ? (int)HttpStatusCode.InternalServerError : Response.StatusCode;

            return Json(results);
        }
    }
}
