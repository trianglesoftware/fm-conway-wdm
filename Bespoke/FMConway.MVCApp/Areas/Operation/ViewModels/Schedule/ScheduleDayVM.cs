﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.Services.Schedules.Models;

namespace FMConway.MVCApp.Areas.Operation.ViewModels.Schedule
{
    public class ScheduleDayVM
    {
        public DateTime Date { get; set; }
        public List<SchedulerModel> Schedules { get; set; }
    }
}