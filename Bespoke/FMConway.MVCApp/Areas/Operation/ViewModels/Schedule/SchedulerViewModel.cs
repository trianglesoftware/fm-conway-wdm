﻿using FMConway.Services.Contracts.Models;
using FMConway.Services.Customers.Models;
using FMConway.Services.Employees.Models;
using FMConway.Services.Schedules.Models;
using FMConway.Services.Vehicles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Operation.ViewModels.Schedule
{
    public class SchedulerViewModel
    {
        public DateTime WeekStart { get; set; }
        public DateTime? Day { get; set; }
        public DateTime UpdatedTime { get; set; }
        public Guid? ContractID { get; set; }
        public Guid? DepotID { get; set; }    
        public List<VehicleModel> Vehicles { get; set; }
        public List<EmployeeModel> Employees { get; set; }
        public List<SchedulerModel> Schedules { get; set; }
        public List<ContractModel> Contracts { get; set; }
        public List<CustomerModel> Customers { get; set; }
        public List<DayTotal> DayTotals { get; set; }
        public IEnumerable<SelectListItem> Depots { get; set; }
        public List<string> SelectedContracts { get; set; }
        public List<string> SelectedCustomers { get; set; }
    }
}