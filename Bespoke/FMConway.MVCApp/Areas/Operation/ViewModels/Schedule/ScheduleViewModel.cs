﻿using FMConway.Services.Employees.Models;
using FMConway.Services.Schedules.Models;
using FMConway.Services.Vehicles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Operation.ViewModels.Schedule
{
    public class ScheduleViewModel
    {
        public ScheduleModel Schedule { get; set; }
        public bool FurtherScheduling { get; set; }

        public int StartHour { get; set; }
        public int StartMin { get; set; }
        public int FinishHour { get; set; }
        public int FinishMin { get; set; }

        public Dictionary<Guid, string> Staff { get; set; }

        public Guid? DepotID { get; set; }
        public IEnumerable<SelectListItem> Depots { get; set; }

        public DateTime WeekStart { get; set; }
        public List<ScheduleVehicleModel> Vehicles { get; set; }
        public List<ScheduleModel> Schedules { get; set; }

        public Guid AreaID { get; set; }
        public string AreaName { get; set; }
        public Guid JobID { get; set; }
        public string JobNumber { get; set; }
        public Guid CertificateTypeID { get; set; }
        public string CertificateType { get; set; }
        public bool IsAgencyStaff { get; set; }
    }
}