﻿using FMConway.Services.LoadingSheets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Areas.Operation.ViewModels.LoadingSheets
{
    public class LoadingSheetViewModel
    {
        public LoadingSheetModel LoadingSheet { get; set; }

        public IEnumerable<SelectListItem> Status { get; set; }
    }
}