﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Triangle.Membership.Services.Sql;
using Triangle.Membership.Users;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Menu;
using FMConway.Services.UserProfiles.Models;
using FMConway.Services.UserProfiles;

namespace FMConway.MVCApp.Controllers
{
    [NavigationMenu("Home", 1, "icon-home icon-white")]
    public class HomeController : BaseController
    {
        [NavigationMenuItem("Home", 10, isDefault: true)]
        public ActionResult Index()
        {
            UserProfileModel userProfile;

            if (HttpContext.Session["UserProfile"] == null)
            {
                AspUserContext context = new AspUserContext(new SqlMembershipService());
                UserProfileService profileService = new UserProfileService(context);
                userProfile = profileService.GetUserProfile(context.UserDetails.ID);
                HttpContext.Session["UserProfile"] = userProfile;
            }
            else
            {
                userProfile = HttpContext.Session["UserProfile"] as UserProfileModel;
            }

            if (userProfile.CustomerID != null)
            {
                // customer
                return RedirectToAction("Active", "WorkInstruction", new { area = "Contract" });
                //return View("CustomerDashboard");
            }
            else if (userProfile.UserType.IsContractManager)
            {
                // employee
                return View("ContractManagerDashboard");
            }
            else
            {
                // employee
                return View("OperationManagerDashboard");
            }
        }
    }
}
