﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using FMConway.MVCApp.Attributes;
using FMConway.MVCApp.Menu;
using FMConway.MVCApp.Models.Menu;
using System.Web.Security;

namespace FMConway.MVCApp.Controllers
{
    public class MenuController : BaseController
    {
        [ChildActionOnly]
        public PartialViewResult NavigationMenu()
        {

            MenuItemCollection mic = Menu.DynamicMenu.Generate(HttpContext, Roles.GetRolesForUser(User.Identity.Name));

            var model = new MenuViewModel();
            model.MenuItems = mic.Collection;

            return PartialView(model);
        }

    }
}
