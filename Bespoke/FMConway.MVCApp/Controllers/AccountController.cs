﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using FMConway.MVCApp.Filters;
using FMConway.MVCApp.Models;

using Triangle.Membership.Interface;
using FMConway.MVCApp.ActionFilters;
using FMConway.MVCApp.Utility;
using Triangle.Membership.Models;
using System.Diagnostics;
using System.Text;
using System.Web.Configuration;

namespace FMConway.MVCApp.Controllers
{
    public class AccountController : Controller
    {
        IMembershipProvider _membershipProvider;

        public AccountController(IMembershipProvider provider)
        {
            _membershipProvider = provider;

        }

        [AllowAnonymous, ImportModelStateFromTempData]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl, string forgot)
        {
            if (!string.IsNullOrEmpty(forgot))
            {
                if (string.IsNullOrEmpty(model.UserName))
                {
                    ModelState.Clear();
                    ModelState.AddModelError("UserName", "Your username must be supplied to request a password reset.");
                    return View(model);
                }
                else
                {
                    ModelState.Clear();
                    ViewBag.Info = "Thank you, you will receive an email shortly containing your password reset link.";
                    ViewBag.ReturnUrl = returnUrl;

                    try
                    {
                        var account = _membershipProvider.GetAccount(model.UserName);
                        if (account == null)
                        {
                            // return with "Thank you" response but do nothing
                            return View();
                        }

                        PasswordResetToken t = _membershipProvider.GeneratePasswordResetToken(model.UserName);

                        string resetLink = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Authority + Url.Action("reset", new { token = t.Token });

                        string email = string.Format(@"<div style='font-family:Calibri;'>
                                                           <p  style='color:#262626;'>Hi <span style='color:#467551'><b>{0}</b></span>,
                                                           <br/>
                                                           <br/>
                                                           You have requested to reset your password. Please click the link below to continue with your request.</p>
                                                           <a href='{1}' >Password Reset </a>
                                                       </div>", t.User.UserName, resetLink);

                        EmailUtility.SendEmail(t.User.Email, "CTM - Password Reset", email);

                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine("Failed to generate password reset token for user '" + model.UserName + "\n\r" + e.ToString());

                        ViewBag.Info = null;
                        ViewBag.Error = "An error occurred while generating forgotten password email";
                    }

                    return View();
                }
            }

            if (ModelState.IsValid)
            {
                if (!_membershipProvider.Login(model.UserName, model.Password, model.RememberMe))
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    return View(model);
                }

                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form

            return View(model);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            };


        }

        public ActionResult LogOff()
        {
            _membershipProvider.Logout();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Reset(string token)
        {
            if (!_membershipProvider.IsPasswordResetTokenValid(token))
                return RedirectToAction("index", "home");

            TempData["Token"] = token;

            return View();
        }

        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public ActionResult Reset(string password, string confirmationPassword)
        {
            if (!password.Equals(confirmationPassword))
            {
                ModelState.AddModelError("password", "The new password and confirmation password do not match.");
                return View();
            }

            string token = TempData["Token"] as string;

            if (!_membershipProvider.ResetPassword(token, password))
            {
                ModelState.AddModelError("", "Sorry, the password reset session has expired.");
            }

            return RedirectToAction("index", "home");
        }

        [ImportModelStateFromTempData]
        public ActionResult UserProfile()
        {
            IMembershipUser account = _membershipProvider.GetAccount(User.Identity.Name);
            ViewBag.CurrentEmail = account.Email;

            return View();
        }

        [HttpPost, ValidateAntiForgeryToken, ExportModelStateToTempData]
        public ActionResult UpdateEmail(string email)
        {
            IMembershipUser account = _membershipProvider.GetAccount(User.Identity.Name);

            if (!EmailUtility.ValidateEmail(email))
                ModelState.AddModelError("email", "Please provide a valid e-mail address.");

            if (ModelState.IsValid)
            {
                account.Email = email;
                _membershipProvider.UpdateAccount(account);
            }

            return RedirectToAction("UserProfile");
        }

        [HttpPost, ValidateAntiForgeryToken, ExportModelStateToTempData]
        public ActionResult UpdatePassword(LocalPasswordModel passwordChange)
        {
            IMembershipUser account = _membershipProvider.GetAccount(User.Identity.Name);

            if (ModelState.IsValid)
            {
                if (!_membershipProvider.ChangePassword(User.Identity.Name, passwordChange.OldPassword, passwordChange.NewPassword))
                {
                    ModelState.AddModelError("OldPassword", "Sorry, we could not updated your password at this time. Please try again later.");
                }

            }

            return RedirectToAction("UserProfile");
        }

    }
}
