﻿using FMConway.Services.Files.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace FMConway.MVCApp.Controllers
{
    public class FileController : Controller
    {
        FileService _fileService;

        public FileController(FileService fileService)
        {
            _fileService = fileService;
        }

        public FileResult GetFile(Guid id)
        {
            var file = _fileService.Single(id);

            MemoryStream memStream = new MemoryStream(file.Data);

            FileStreamResult result = new FileStreamResult(memStream, file.ContentType);

            switch (file.ContentType)
            {
                case string ext when ext.Contains(".document"):
                    result.FileDownloadName = file.FileDownloadName;
                    break;
                case string ext when ext.Contains("/msword"):
                    result.FileDownloadName = file.FileDownloadName;
                    break;         
                default:
                    break;
            }

            return result;
        }

        public ActionResult CreateDocument(Guid id)
        {
            var file = _fileService.Single(id);

            MemoryStream memStream = new MemoryStream(file.Data);
            return new FileStreamResult(memStream, file.ContentType);
        }

    }
}
