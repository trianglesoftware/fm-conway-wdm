﻿using FMConway.MVCApp.Bootstrap.Collections;
using FMConway.MVCApp.Extensions;
using FMConway.Services.Excel.Extensions;
using FMConway.Services.Excel.Services;
using FMConway.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {

        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            var excelExport = Request.QueryString["excelexport"];
            if (excelExport != null && excelExport == "1")
            {
                var result = filterContext.Result as JsonResult;
                if (result != null)
                {
                    var pagedTable = result.Data as PagedTable;
                    if (pagedTable != null)
                    {
                        var model = pagedTable.ToDataTable().ToXlsx(true);
                        filterContext.Result = File(model, ContentType.Xlsx, string.Format("Excel ({0}).xlsx", DateTime.Now.ToString("dd-MM-yyyy HHmmss")));
                    }
                }
            }
        }
    }
}