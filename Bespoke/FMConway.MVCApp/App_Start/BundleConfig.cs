﻿using System.Web;
using System.Web.Optimization;

namespace FMConway.MVCApp
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                                         "~/Scripts/jquery-{version}.js",
                                         "~/Scripts/jquery-mousewheel-3.1.4/jquery.mousewheel.js",
                                         "~/Scripts/custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js",
                                         "~/Content/masonry.js",
                                         "~/Scripts/cookie.js",
                                         "~/Content/classywiggle/js/jquery.classywiggle.js",
                                         "~/Content/bxSlider/jquery.bxslider.js",
                                         "~/Content/fancybox/jquery.fancybox-{version}.js",
                                         "~/Scripts/css3-mediaqueries.js",
                                         "~/Scripts/moment.min.js",
                                         "~/Scripts/moment-with-locales.min.js",
                                         "~/Scripts/moment-duration-format.js",
                                         "~/Scripts/fm-conway.js",
                                         "~/Content/jquery-datetimepicker/jquery.datetimepicker.full.js"
                                         ));



            //bundles.Add(new StyleBundle("~/Content/jquery-datetimepicker/bundle").Include( 
            //                  , new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                                         "~/Scripts/jquery.unobtrusive-ajax.js",
                                         "~/Scripts/jquery.jquery.validate.js",
                                          "~/Scripts/jquery.validate.unobtrusive.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                                        "~/Content/table.css",
                                        "~/Content/site.css",
                                        "~/Content/theme.css"));

            bundles.Add(new ScriptBundle("~/Content/Core").Include(
                                        "~/Content/triangle/core.js",
                                        "~/Scripts/table.js"));

   

            bundles.Add(new StyleBundle("~/Content/Bootstrap").Include(
                                        "~/Content/bootstrap-responsive.css",
                                        "~/Content/bootstrap.css"));

            bundles.Add(new ScriptBundle("~/Content/Bootstrap/JS").Include(
                                         "~/Content/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/Content/Scheduler_js").Include(
                                         "~/Content/bootstrap.js",
                                         "~/Scripts/moment.min.js",
                                         "~/Scripts/moment-with-locales.min.js",
                                         "~/Scripts/moment-duration-format.js",
                                         "~/Scripts/jquery-{version}.js",
                                         "~/Content/jquery-ui/js/jquery-ui-{version}.custom.js",
                                         "~/Content/Scheduler.js"));

            bundles.Add(new ScriptBundle("~/Content/SchedulerInline_js").Include(
                                         "~/Content/bootstrap.js",
                                         "~/Scripts/moment.min.js",
                                         "~/Scripts/moment-with-locales.min.js",
                                         "~/Scripts/moment-duration-format.js",
                                         "~/Scripts/jquery-{version}.js",
                                         "~/Content/jquery-ui/js/jquery-ui-{version}.custom.js",
                                         "~/Content/SchedulerInline.js"));

            bundles.Add(new StyleBundle("~/Content/Scheduler_css").Include(
                                        "~/Content/Scheduler.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                               "~/Content/themes/base/jquery.ui.core.css",
                               "~/Content/themes/base/jquery.ui.resizable.css",
                               "~/Content/themes/base/jquery.ui.selectable.css",
                               "~/Content/themes/base/jquery.ui.accordion.css",
                               "~/Content/themes/base/jquery.ui.autocomplete.css",
                               "~/Content/themes/base/jquery.ui.button.css",
                               "~/Content/themes/base/jquery.ui.dialog.css",
                               "~/Content/themes/base/jquery.ui.slider.css",
                               "~/Content/themes/base/jquery.ui.tabs.css",
                               "~/Content/themes/base/jquery.ui.datepicker.css",
                               "~/Content/themes/base/jquery.ui.progressbar.css",
                               "~/Content/themes/base/jquery.ui.theme.css",
                               "~/Content/bxSlider/jquery.bxslider.css",
                               "~/Content/fancybox/jquery.fancybox-{version}.css"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                             "~/Content/jquery-ui/js/jquery-ui.min.js"));



            bundles.Add(new StyleBundle("~/Content/jquery-ui/css/ui-darkness/bundle").Include(
                                         "~/Content/jquery-ui/css/ui-darkness/jquery-ui-1.10.3.custom.css",
                                         "~/Content/jquery-datetimepicker/jquery.datetimepicker.min.css"));

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}