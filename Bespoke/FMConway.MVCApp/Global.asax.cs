﻿using log4net;
using log4net.Config;
using Spring.Context;
using Spring.Context.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FMConway.MVCApp.Extensions;

namespace FMConway.MVCApp
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start()
        {
            XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ControllerBuilder.Current.SetControllerFactory(new ControllerFactory());

            log.Info("Application started.");

        }

        protected void Application_Error(object sender, EventArgs e)
        {
#if !DEBUG
            try
            {
                var httpContext = ((MvcApplication)sender).Context;
                var currentController = " ";
                var currentAction = " ";
                var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

                if (currentRouteData != null)
                {
                    if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                    {
                        currentController = currentRouteData.Values["controller"].ToString();
                    }

                    if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                    {
                        currentAction = currentRouteData.Values["action"].ToString();
                    }
                }

                var ex = Server.GetLastError();

                string errorMessage = ex.GetVeboseException(includeStackTrace: true);

                if (currentRouteData != null)
                    log.ErrorFormat("{0} Controller: {1}; Action: {2}; \r\n {3}", currentRouteData.DataTokens.Keys.Any(k => k.Equals("area")) ? "Area: " + currentRouteData.DataTokens["area"] + ";" : "", currentRouteData.Values.Keys.Any(k => k.Equals("controller")) ? currentRouteData.Values["controller"] : "", currentRouteData.Values.Keys.Any(k => k.Equals("action")) ? currentRouteData.Values["action"] : "", errorMessage);
                else
                    log.Error(errorMessage);

                var controller = new FMConway.MVCApp.Controllers.ErrorController();
                var routeData = new RouteData();
                var action = "Index";

                if (ex is HttpException)
                {
                    var httpEx = ex as HttpException;

                    switch (httpEx.GetHttpCode())
                    {
                        case 404:
                            action = "NotFound";
                            break;
                    }
                }

                httpContext.ClearError();
                httpContext.Response.Clear();
                httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
                httpContext.Response.TrySkipIisCustomErrors = true;

                routeData.Values["controller"] = "Error";
                routeData.Values["action"] = action;

                controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
                ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));

            }
            catch (Exception ex)
            {
                log.Fatal(ex.GetVeboseException());
            }
#endif
        }

    }

    public class ControllerFactory : IControllerFactory
    {
        ControllerServiceProvider _controllerProvider;

        public ControllerFactory()
        {
            _controllerProvider = new ControllerServiceProvider();
        }

        public IController CreateController(RequestContext requestContext, string controllerName)
        {
            if (requestContext.RouteData.DataTokens != null && requestContext.RouteData.DataTokens.Keys.Contains("area"))
                controllerName = requestContext.RouteData.DataTokens["area"] + "_" + controllerName;

            return _controllerProvider.GetController(controllerName);
        }

        public System.Web.SessionState.SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, string controllerName)
        {
            return System.Web.SessionState.SessionStateBehavior.Default;
        }

        public void ReleaseController(IController controller)
        {
            var disposable = controller as IDisposable;

            if (disposable != null)
                disposable.Dispose();
        }
    }

    public class ControllerServiceProvider : ServiceProvider
    {
        public IController GetController(string controllerName)
        {
            return (IController)GetObject(controllerName);
        }
    }

    public abstract class ServiceProvider
    {
        protected virtual object GetObject(string objectName)
        {
            IApplicationContext context = ContextRegistry.GetContext();

            object contextObject = null;

            try
            {
                contextObject = context.GetObject(objectName.ToLower());
            }
            catch (Spring.Objects.Factory.NoSuchObjectDefinitionException e)
            {
                throw new HttpException(404, objectName + " does not exist!");
            }


            return contextObject;
        }
    }

}