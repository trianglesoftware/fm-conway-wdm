﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FMConway.MVCApp.Bootstrap
{
    public class BootstrapValidation<TModel>
    {
        ViewContext _context;

        public BootstrapValidation(ViewContext context)
        {
            this._context = context;
        }

        #region Validation Summary

        public HtmlString ValidationSummary()
        {
            return ValidationSummary(string.Empty, null);
        }

        public HtmlString ValidationSummary(object htmlAttributes)
        {
            return ValidationSummary(string.Empty, htmlAttributes);
        }

        public HtmlString ValidationSummary(string message)
        {
            return ValidationSummary(message, null);
        }

        public HtmlString ValidationSummary(string message, object htmlAttributes)
        {
            ICollection<ModelState> modelStates = _context.ViewData.ModelState.Values;

            if (!HasModelStateErrors(modelStates))
                return new HtmlString(string.Empty);

            var container = new HtmlGenericControl("div");

            IDictionary<string, object> attrs = new RouteValueDictionary(htmlAttributes);

            foreach (var att in attrs)
            {
                container.Attributes.Add(att.Key, att.Value.ToString());
            }

            container.Attributes["class"] += " alert alert-error";
            container.Attributes["class"] = container.Attributes["class"].Trim();

            if (!string.IsNullOrEmpty(message))
                container.Controls.Add(new HtmlGenericControl("label") { InnerHtml = message });

            container.Controls.Add(ValidationErrors(modelStates));

            return RenderControl(container);
        }

        #endregion

        /// <summary>
        /// Returns an un-ordered list of validation errors.
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        private HtmlGenericControl ValidationErrors(ICollection<ModelState> modelStates)
        {
            var ul = new HtmlGenericControl("ul");

            foreach (ModelState modelState in modelStates)
            {

                foreach (var error in modelState.Errors)
                {
                    ul.Controls.Add(new HtmlGenericControl("li")
                    {
                        InnerText = error.ErrorMessage
                    });
                }
            }

            return ul;
        }

        private bool HasModelStateErrors(ICollection<ModelState> modelStates)
        {
            if (modelStates.Any())
            {
                foreach (var modelState in modelStates)
                {
                    if (modelState.Errors.Any())
                        return true;
                }
            }

            return false;
        }

        private HtmlString RenderControl(Control contr)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    contr.RenderControl(htw);
                    return new HtmlString(sw.ToString());
                }
            }
        }
    }
}