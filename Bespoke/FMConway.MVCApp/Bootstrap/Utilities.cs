﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Bootstrap
{
    public class Utilities
    {

        public static string PascalCaseToFriendly(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, "[a-z][A-Z]", m =>
            {
                return m.Value[0] + " " + m.Value[1];
            });
        }

        public static string PascalCaseToCss(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, "[a-z][A-Z]", m =>
            {
                return m.Value[0] + "-" + m.Value[1];
            }).ToLower();
        }

    }
}