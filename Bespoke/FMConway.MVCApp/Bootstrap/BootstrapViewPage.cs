﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FMConway.MVCApp.Bootstrap
{
    //public abstract class BootstrapViewPage : WebViewPage
    //{
    //    public BootstrapHelper<object> Bootstrap { get; private set; }

    //    public override void InitHelpers()
    //    {
    //        base.InitHelpers();

    //        this.Bootstrap = new BootstrapHelper<object>(this.ViewContext, this);
    //    }

    //}

    public abstract class BootstrapViewPage<TModel> : WebViewPage<TModel>
    {
        public BootstrapHelper<TModel> Bootstrap { get; private set; }

        public override void InitHelpers()
        {       
            base.InitHelpers();

            this.Bootstrap = new BootstrapHelper<TModel>(this.ViewContext, this, base.Html);
        }
    }
}