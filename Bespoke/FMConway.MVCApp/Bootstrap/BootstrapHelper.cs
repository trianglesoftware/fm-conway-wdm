﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using FMConway.MVCApp.Extensions;
using System.Web.UI.WebControls;
using System.Globalization;

namespace FMConway.MVCApp.Bootstrap
{
    public class BootstrapHelper<TModel>
    {
        protected ViewContext _context;
        protected IViewDataContainer _viewDataContainer;
        protected HtmlHelper<TModel> _htmlHelper;

        public BootstrapValidation<TModel> Validation { get; set; }

        public BootstrapHelper(ViewContext context, IViewDataContainer viewDataContainer, HtmlHelper<TModel> htmlHelper)
        {
            this._context = context;
            this._viewDataContainer = viewDataContainer;
            this._htmlHelper = htmlHelper;

            InitHelpers();
        }

        protected virtual void InitHelpers()
        {
            this.Validation = new BootstrapValidation<TModel>(_context);
        }

        #region Controls

        #region Lookups

        public HtmlString MultiListLookupFor<TKey, TValue>(Expression<Func<TModel, Dictionary<TKey, TValue>>> expression, string displayName = null, bool disabled = false, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            #region expression parsing

            ExpressionDataModel expressionData = GetExpressionData(expression);

            Dictionary<TKey, TValue> selectedValues = GetModelStateValue<Dictionary<TKey, TValue>>(expressionData.FullName);

            #endregion

            var container = new HtmlGenericControl("div");
            container.ID = expressionData.FullName.Replace('.', '-') + "-lookup";
            container.Attributes["class"] = "lookup";

            if (disabled)
                container.Attributes["class"] += " disabled";

            var attributes = new RouteValueDictionary(htmlAttributes);

            if (!attributes.Keys.Any(k => k.ToLower().Equals("disabled")))
                attributes.Add("disabled", "disabled");

            displayName = displayName ?? expressionData.Metadata.DisplayName ?? Utilities.PascalCaseToFriendly(expressionData.Metadata.PropertyName);

            HtmlGenericControl input = MultiListLookupControlGroup<TKey, TValue>(expressionData.FullName, displayName, selectedValues, InputType.Text, attributes, false, disabled, expressionData.Metadata.IsRequired);

            ModelError errorInfo;
            if (ModelStateError(expressionData.FullName, out errorInfo))
            {
                input.Attributes["class"] += " error";
                if (displayValidationMessage)
                {
                    var helpText = new HtmlGenericControl("span");
                    helpText.Attributes["class"] = "help-inline";
                    helpText.InnerText = errorInfo.ErrorMessage;
                    input.Controls[1].Controls.Add(helpText);
                }
            }

            container.Controls.Add(input);

            string modalID = expressionData.FullName.Replace('.', '-') + "-modal";
            string modalHeader = displayName + " Lookup";

            #region lookup button

            HtmlAnchor lookupLink = LookupLink(modalID, modalHeader, disabled);

            input.Controls[1/*div class="controls"*/].Controls.AddAt(1, lookupLink);

            #endregion

            //#region header controls

            //var search = new HtmlInputText();
            //search.Attributes["placeholder"] = "search";
            //search.Attributes["class"] = "search";
            //search.Attributes["disabled"] = "disabled";
            //#endregion

            //var headerControls = new List<Control>() { search };

            //#region footer controls

            //var actionSelect = new HtmlGenericControl("button");
            //actionSelect.Attributes["class"] = "btn btn-primary action-select";
            //actionSelect.InnerText = "Select";
            //actionSelect.Attributes["data-dismiss"] = "modal";

            //var actionClose = new HtmlGenericControl("button");
            //actionClose.Attributes["class"] = "btn";
            //actionClose.Attributes["data-dismiss"] = "modal";
            //actionClose.InnerText = "Close";

            //#endregion

            //var footerControls = new List<Control>() { actionClose, actionSelect };

            //container.Controls.Add(Modal(modalID, modalHeader, headerControls, null, footerControls));


            return container.RenderControl();
        }

        public HtmlString LookupFor<TProperty>(Expression<Func<TModel, TProperty>> expression, Expression<Func<TModel, string>> selectedDeffinition, string displayName = null, bool disabled = false, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            #region expression parsing

            ExpressionDataModel expressionData = GetExpressionData(expression);
            ExpressionDataModel selectedDeffinitionExpressionData = GetExpressionData(selectedDeffinition);

            var selectedValue = GetModelStateValue<object>(selectedDeffinitionExpressionData.FullName);
            string selectedDeffinitionValue = selectedValue != null ? selectedValue.ToString() : string.Empty;

            #endregion

            var container = new HtmlGenericControl("div");
            container.ID = expressionData.FullName.Replace('.', '-') + "-lookup";
            container.Attributes["class"] = "lookup";
            container.Attributes["data-title"] = displayName;

            if (disabled)
                container.Attributes["class"] += " disabled";

            var attributes = new RouteValueDictionary(htmlAttributes);

            if (!attributes.Keys.Any(k => k.ToLower().Equals("readonly")))
                attributes.Add("readonly", "readonly");

            attributes["class"] = attributes.Keys.Any(k => k.ToLower().Equals("class")) ? "selected " + attributes["class"] : "selected";

            HtmlGenericControl hiddenValue = Input(expressionData.FullName, null, InputType.Hidden, null);
            container.Controls.Add(hiddenValue);

            HtmlGenericControl input = null;

            displayName = displayName ?? expressionData.Metadata.DisplayName ?? Utilities.PascalCaseToFriendly(expressionData.Metadata.PropertyName);

            input = InputControlGroup(selectedDeffinitionExpressionData.FullName, displayName, selectedDeffinitionValue, InputType.Lookup, attributes, false, expressionData.Metadata.IsRequired);

            ModelError errorInfo;
            if (ModelStateError(expressionData.FullName, out errorInfo))
            {
                input.Attributes["class"] += " error";
                if (displayValidationMessage)
                {
                    var helpText = new HtmlGenericControl("span");
                    helpText.Attributes["class"] = "help-inline";
                    helpText.InnerText = errorInfo.ErrorMessage;
                    input.Controls[1].Controls.Add(helpText);
                }
            }

            container.Controls.Add(input);

            string modalID = expressionData.FullName.Replace('.', '-') + "-modal";
            string modalHeader = displayName;

            #region lookup button

            HtmlAnchor lookupLink = LookupLink(modalID, modalHeader, disabled);

            input.Controls[1/*div class="controls"*/].Controls.AddAt(1, lookupLink);

            #endregion

            //#region header controls

            //var search = new HtmlInputText();
            //search.Attributes["placeholder"] = "search";
            //search.Attributes["class"] = "search";
            //search.Attributes["disabled"] = "disabled";
            //#endregion

            //var headerControls = new List<Control>() { search };

            //container.Controls.Add(Modal(modalID, modalHeader, headerControls, null, null));


            return container.RenderControl();
        }

        public HtmlString TextBoxLookupFor<TProperty>(Expression<Func<TModel, TProperty>> expression, string displayName = null, bool disabled = false, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            #region expression parsing

            ExpressionDataModel expressionData = GetExpressionData(expression);

            #endregion

            var container = new HtmlGenericControl("div");
            container.ID = expressionData.FullName.Replace('.', '-') + "-lookup";
            container.Attributes["class"] = "lookup";
            container.Attributes["data-title"] = displayName;

            if (disabled)
                container.Attributes["class"] += " disabled";

            var attributes = new RouteValueDictionary(htmlAttributes);

            attributes["class"] = attributes.Keys.Any(k => k.ToLower().Equals("class")) ? "selected " + attributes["class"] : "selected";

            HtmlGenericControl input = null;

            displayName = displayName ?? expressionData.Metadata.DisplayName ?? Utilities.PascalCaseToFriendly(expressionData.Metadata.PropertyName);

            input = InputControlGroup(expressionData.FullName, displayName, null, InputType.Text, attributes, false, expressionData.Metadata.IsRequired);

            ModelError errorInfo;
            if (ModelStateError(expressionData.FullName, out errorInfo))
            {
                input.Attributes["class"] += " error";
                if (displayValidationMessage)
                {
                    var helpText = new HtmlGenericControl("span");
                    helpText.Attributes["class"] = "help-inline";
                    helpText.InnerText = errorInfo.ErrorMessage;
                    input.Controls[1].Controls.Add(helpText);
                }
            }

            container.Controls.Add(input);

            string modalID = expressionData.FullName.Replace('.', '-') + "-modal";
            string modalHeader = displayName;

            #region lookup button

            HtmlAnchor lookupLink = LookupLink(modalID, modalHeader, disabled);

            input.Controls[1/*div class="controls"*/].Controls.AddAt(1, lookupLink);

            #endregion

            //#region header controls

            //var search = new HtmlInputText();
            //search.Attributes["placeholder"] = "search";
            //search.Attributes["class"] = "search";
            //search.Attributes["disabled"] = "disabled";
            //#endregion

            //var headerControls = new List<Control>() { search };

            //container.Controls.Add(Modal(modalID, modalHeader, headerControls, null, null));


            return container.RenderControl();
        }

        public HtmlString TextAreaLookupFor<TProperty>(Expression<Func<TModel, TProperty>> expression, string displayName = null, bool disabled = false, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            #region expression parsing

            ExpressionDataModel expressionData = GetExpressionData(expression);

            #endregion

            var container = new HtmlGenericControl("div");
            container.ID = expressionData.FullName.Replace('.', '-') + "-lookup";
            container.Attributes["class"] = "lookup";

            container.Attributes["data-title"] = displayName;

            if (disabled)
                container.Attributes["class"] += " disabled";

            var attributes = new RouteValueDictionary(htmlAttributes);

            attributes["class"] = attributes.Keys.Any(k => k.ToLower().Equals("class")) ? "selected " + attributes["class"] : "selected";

            displayName = displayName ?? expressionData.Metadata.DisplayName ?? Utilities.PascalCaseToFriendly(expressionData.Metadata.PropertyName);

            HtmlGenericControl textArea = new HtmlGenericControl("textarea");
            textArea.ID = expressionData.FullName.Replace('.', '-');
            textArea.Attributes["name"] = expressionData.FullName;

            textArea.AddAttributes(attributes);

            string inputValue = GetModelInputValue(expressionData.FullName, typeof(string)) as string;
            if (inputValue == null)
            {
                var msValue = GetModelStateValue<object>(expressionData.FullName);
                if (msValue != null)
                    textArea.InnerHtml = msValue.ToString();
                else
                    textArea.InnerHtml = string.Empty;
            }
            else
            {
                textArea.InnerHtml = inputValue;
            }

            HtmlGenericControl input = ControlGroup(expressionData.Metadata.DisplayName ?? Utilities.PascalCaseToFriendly(expressionData.Metadata.PropertyName), textArea, InputType.Text, attributes, displayValidationMessage, expressionData.Metadata.IsRequired);

            container.Controls.Add(input);

            string modalID = expressionData.FullName.Replace('.', '-') + "-modal";
            string modalHeader = displayName + " Lookup";

            #region lookup button

            HtmlAnchor lookupLink = LookupLink(modalID, modalHeader, disabled);

            input.Controls[1/*div class="controls"*/].Controls.AddAt(1, lookupLink);

            #endregion

            //#region header controls

            //var search = new HtmlInputText();
            //search.Attributes["placeholder"] = "search";
            //search.Attributes["class"] = "search";
            //search.Attributes["disabled"] = "disabled";
            //#endregion

            //var headerControls = new List<Control>() { search };

            //container.Controls.Add(Modal(modalID, modalHeader, headerControls, null, null));

            return container.RenderControl();
        }

        private HtmlAnchor LookupLink(string id, string title, bool disabled)
        {
            HtmlAnchor lookupLink = new HtmlAnchor();
            lookupLink.Attributes["class"] = "lookup-button";
            //lookupLink.Attributes["data-placement"] = "right";
            //lookupLink.Attributes["data-delay"] = " { \"show\": \"900\", \"hide\": \"100\" }";
            lookupLink.HRef = "#" + id;

            lookupLink.Attributes["role"] = "button";
            lookupLink.Attributes["data-backdrop"] = "false";

            lookupLink.Title = title;

            if (disabled)
            {
                lookupLink.Attributes["class"] += " disabled";
                lookupLink.Attributes["disabled"] = "disabled";
            }
            //else
            //{
            //    lookupLink.Attributes["data-toggle"] = "modal";
            //}

            var icon = new HtmlGenericControl("span");
            icon.Attributes["class"] = "fa fa-chevron-up";

            lookupLink.Controls.Add(icon);

            return lookupLink;
        }

        #endregion

        #region CheckBoxFor

        public HtmlString CheckBoxFor(Expression<Func<TModel, bool>> expression, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            return InputFor(expression, InputType.CheckBox, displayValidationMessage, null, htmlAttributes);
        }
        #endregion

        #region RadioButtonFor

        public HtmlString RadioButtonFor<TProperty>(Expression<Func<TModel, TProperty>> expression, string value, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            return InputFor(expression, InputType.Radio, displayValidationMessage, value, htmlAttributes);
        }
        #endregion

        #region DatePickerFor

        public HtmlString DatePickerFor<TProperty>(Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            return InputFor(expression, InputType.DateTime, displayValidationMessage, null, htmlAttributes);
        }
        #endregion

        #region TextBoxFor

        public HtmlString TextBoxFor<TProperty>(Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            return InputFor(expression, InputType.Text, displayValidationMessage, null, htmlAttributes);
        }
        #endregion

        #region PrependedFor

        public HtmlString PrependedFor(Expression<Func<TModel, Decimal>> expression, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            return InputFor(expression, InputType.Prepend, displayValidationMessage, null, htmlAttributes);
        }
        #endregion

        #region PasswordFor

        public HtmlString PasswordFor<TProperty>(Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            return InputFor(expression, InputType.Password, displayValidationMessage, null, htmlAttributes);
        }

        #endregion

        #region TextAreaFor

        public HtmlString TextAreaFor<TProperty>(Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            RouteValueDictionary attributeDictionary = new RouteValueDictionary(htmlAttributes);

            ExpressionDataModel expressionData = GetExpressionData(expression);

            HtmlGenericControl textArea = new HtmlGenericControl("textarea");
            textArea.ID = expressionData.FullName.Replace('.', '-');
            textArea.Attributes["name"] = expressionData.FullName;

            textArea.AddAttributes(attributeDictionary);

            string inputValue = GetModelInputValue(expressionData.FullName, typeof(string)) as string;
            if (inputValue == null)
            {
                var msValue = GetModelStateValue<object>(expressionData.FullName);
                if (msValue != null)
                    textArea.InnerHtml = msValue.ToString();
                else
                    textArea.InnerHtml = string.Empty;
            }
            else
            {
                textArea.InnerHtml = inputValue;
            }

            HtmlGenericControl controlGroup = ControlGroup(expressionData.Metadata.DisplayName ?? Utilities.PascalCaseToFriendly(expressionData.Metadata.PropertyName), textArea, InputType.Text, attributeDictionary, displayValidationMessage, expressionData.Metadata.IsRequired);

            return controlGroup.RenderControl();
        }

        #endregion

        #region DropDownListFor

        public HtmlString DropDownListFor<TProperty>(Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, object htmlAttributes = null, bool displayValidationMessage = false)
        {
            var attributeDictionary = new RouteValueDictionary(htmlAttributes);
            ExpressionDataModel expressionData = GetExpressionData(expression);

            HtmlGenericControl selector = new HtmlGenericControl("select");
            selector.AddAttributes(attributeDictionary);
            selector.ID = expressionData.FullName.Replace('.', '-');
            selector.Attributes["name"] = expressionData.FullName;

            string inputValue = GetModelInputValue(expressionData.FullName, typeof(string)) as string;
            if (inputValue == null)
            {
                var msValue = GetModelStateValue<object>(expressionData.FullName);
                if (msValue != null)
                {
                    SelectListItem selectedItem = listItems.Where(i => i.Value != null && i.Value.Equals(msValue.ToString())).FirstOrDefault();
                    if (selectedItem != null)
                    {
                        listItems.ToList().ForEach(i => i.Selected = false);

                        selectedItem.Selected = true;
                    }
                }
            }

            var anySelected = listItems.Any(a => a.Selected);

            listItems.ToList().ForEach(i =>
            {
                var option = new HtmlGenericControl("option");
                option.Attributes["value"] = i.Value ?? "";
                option.InnerText = i.Text;

                if (i.Selected || (!anySelected && inputValue == i.Value))
                {
                    option.Attributes["selected"] = "selected";
                }

                selector.Controls.Add(option);
            });

            HtmlControl container = ControlGroup(expressionData.Metadata.DisplayName ?? Utilities.PascalCaseToFriendly(expressionData.Metadata.PropertyName), selector, InputType.Dropdown, attributeDictionary, displayValidationMessage, expressionData.Metadata.IsRequired);

            return container.RenderControl();
        }

        #endregion

        private HtmlString InputFor<TProperty>(Expression<Func<TModel, TProperty>> expression, InputType inputType, bool displayValidationMessage, string value = null, object htmlAttributes = null)
        {
            ExpressionDataModel expressionData = GetExpressionData(expression);

            var controlGroup = InputControlGroup(expressionData.FullName, expressionData.Metadata.DisplayName ?? Utilities.PascalCaseToFriendly(expressionData.Metadata.PropertyName), value, inputType, new RouteValueDictionary(htmlAttributes), displayValidationMessage, inputType == InputType.CheckBox ? false : expressionData.Metadata.IsRequired);

            return controlGroup.RenderControl();
        }

        private HtmlGenericControl MultiListLookupControlGroup<TKey, TValue>(string controlName, string displayName, Dictionary<TKey, TValue> values, InputType inputType, RouteValueDictionary htmlAttributes, bool displayValidationMessage, bool disabled, bool required)
        {
            var list = new HtmlGenericControl("ul");
            list.Attributes["class"] = "lookup-multi-list";
            list.ID = controlName.Replace('.', '-');

            list.AddAttributes(htmlAttributes);

            if (values != null)
            {

                int index = 0;
                foreach (var value in values.OrderBy(o => o.Value))
                {
                    var listItem = new HtmlGenericControl("li");
                    listItem.Attributes["class"] = "selected";
                    listItem.InnerText = value.Value.ToString();
                    list.Controls.Add(listItem);


                    var hiddenKey = new HtmlInputHidden() { ID = controlName + "[" + index + "].Key", Name = controlName + "[" + index + "].Key", Value = value.Key.ToString() };
                    hiddenKey.Attributes["class"] = "hidden-key";
                    listItem.Controls.Add(hiddenKey);

                    var hiddenValue = new HtmlInputHidden() { ID = controlName + "[" + index + "].Value", Name = controlName + "[" + index + "].Value", Value = value.Value.ToString() };
                    hiddenValue.Attributes["class"] = "hidden-value";
                    listItem.Controls.Add(hiddenValue);


                    //var actionRemove = new HtmlAnchor();
                    //actionRemove.Attributes["class"] = "mli-remove";
                    //actionRemove.HRef = "#";

                    //var removeIcon = new HtmlGenericControl("i");
                    //removeIcon.Attributes["class"] = "icon-remove icon-inactive";

                    //actionRemove.Controls.Add(removeIcon);
                    //listItem.Controls.Add(actionRemove);

                    //if (!disabled)
                    //{
                    //    var actionRemove = new HtmlAnchor() { HRef = "#", Title = "Remove '" + value.Key + "'" };
                    //    actionRemove.Attributes["class"] = "mli-remove";


                    //    var icon = new HtmlGenericControl("i");
                    //    icon.Attributes["class"] = "icon-remove icon-inactive";
                    //    actionRemove.Controls.Add(icon);

                    //    listItem.Controls.Add(actionRemove);
                    //}

                    index++;
                }
            }

            HtmlGenericControl controlGroup = ControlGroup(displayName, list, InputType.Lookup, htmlAttributes, displayValidationMessage, required);
            return controlGroup;
        }

        private HtmlGenericControl InputControlGroup(string controlName, string displayName, string value, InputType inputType, RouteValueDictionary htmlAttributes, bool displayValidationMessage, bool required)
        {
            HtmlGenericControl input = Input(controlName, value, inputType, htmlAttributes);
            HtmlGenericControl controlGroup = ControlGroup(displayName, input, inputType, htmlAttributes, displayValidationMessage, required);

            return controlGroup;
        }

        private HtmlGenericControl ControlGroup(string label, HtmlControl control, InputType inputType, RouteValueDictionary htmlAttributes, bool displayValidationMessage, bool required)
        {
            var container = new HtmlGenericControl("div");
            container.Attributes["class"] = "control-group";
            container.Attributes["data-required"] = required.ToString().ToLower();

            if (htmlAttributes.Keys.Any(k => k.Equals("label", StringComparison.CurrentCultureIgnoreCase)))
                label = (String)htmlAttributes["label"];

            container.Controls.Add(Label(control.ID, $"{label}{(required ? "<span>*</span>" : "")}"));

            var controls = new HtmlGenericControl("div");
            controls.Attributes["class"] = "controls";

            #region Prepends
            if (inputType == InputType.Prepend)
            {
                controls.Attributes["class"] += " input-prepend";

                HtmlGenericControl currencyAddon = new HtmlGenericControl("span");
                currencyAddon.Attributes["class"] = "add-on";

                if (htmlAttributes.Keys.Any(k => k.Equals("prepend")))
                {
                    currencyAddon.InnerText = htmlAttributes["prepend"].ToString();
                    htmlAttributes.Remove("prepend");
                }

                controls.Controls.Add(currencyAddon);
            }
            #endregion

            controls.Controls.Add(control);

            container.Controls.Add(controls);

            ModelError errorInfo;
            if (ModelStateError(control.Attributes["name"], out errorInfo))
            {
                container.Attributes["class"] += " error";

                if (displayValidationMessage)
                {
                    var helpText = new HtmlGenericControl("span");
                    helpText.Attributes["class"] = "help-inline";
                    helpText.InnerText = errorInfo.ErrorMessage;
                    controls.Controls.Add(helpText);
                }
            }

            return container;
        }

        private HtmlGenericControl Label(string @for, string text)
        {
            var label = new HtmlGenericControl("label");
            label.Attributes["class"] = "control-label";
            label.Attributes["for"] = @for;
            label.InnerHtml = text;
            return label;
        }

        private HtmlGenericControl Input(string name, string value, InputType inputType, RouteValueDictionary htmlAttributes)
        {
            var input = new HtmlGenericControl("input");
            input.Attributes["autocomplete"] = "off";

            // replace attribute key '_' with '-'
            if (htmlAttributes != null)
            {
                var routeValueDictionary = new RouteValueDictionary();
                foreach (var attribute in htmlAttributes)
                {
                    routeValueDictionary.Add(attribute.Key.Replace('_', '-'), attribute.Value);
                }
                htmlAttributes = routeValueDictionary;
            }

            input.AddAttributes(htmlAttributes);
            input.Attributes["name"] = name;

            if ((int)inputType >= 100)
            {
                input.Attributes["type"] = "text";
            }
            else
            {
                input.Attributes["type"] = Enum.GetName(typeof(InputType), inputType).ToLower();
            }

            input.ID = name.Replace('.', '-');

            if (htmlAttributes != null && htmlAttributes.Keys.Any(k => k.Equals("value", StringComparison.CurrentCultureIgnoreCase)))
                return input;

            switch (inputType)
            {
                case InputType.CheckBox:
                    bool isChecked;

                    bool? modelStateCheck = GetModelInputValue(name, typeof(bool)) as bool?;

                    isChecked = modelStateCheck.HasValue ? modelStateCheck.Value : GetModelStateValue<bool>(name);//GetModelStateValue<bool>(name);

                    if (isChecked)
                        input.Attributes["checked"] = "checked";

                    input.Attributes["value"] = "true";//isChecked.ToString();

                    break;
                case InputType.Radio:
                    string modelValue = GetModelInputValue(name, typeof(string)) as string;
                    if (modelValue == null)
                    {
                        var msValue = GetModelStateValue<object>(name);
                        if (msValue != null)
                            modelValue = msValue.ToString();//GetModelStateValue<string>(name);
                    }

                    if (!string.IsNullOrEmpty(modelValue) && modelValue.Equals(value, StringComparison.CurrentCultureIgnoreCase))
                        input.Attributes["checked"] = "checked";

                    input.Attributes["value"] = value;
                    break;
                case InputType.DateTime:
                    input.Attributes["class"] += string.IsNullOrEmpty(input.Attributes["class"]) ? "datepicker" : " datepicker";
                    string inputValue = "";
                    DateTime? modelInputValue = GetModelInputValue(name, typeof(DateTime?)) as DateTime?;

                    if (modelInputValue == null)
                    {
                        DateTime? msValue = GetModelStateValue<object>(name) as DateTime?;
                        if (msValue != null)
                            inputValue = msValue.Value.ToShortDateString();
                        else
                            inputValue = value;
                    }
                    else
                        inputValue = modelInputValue.Value.ToShortDateString();

                    if (!string.IsNullOrEmpty(inputValue))
                        input.Attributes["value"] = inputValue;

                    break;
                default:
                    string defaultValue = GetModelInputValue(name, typeof(string)) as string;
                    if (defaultValue == null)
                    {
                        var msValue = GetModelStateValue<object>(name);
                        if (msValue != null)
                        {
                            if (msValue.GetType() == typeof(decimal) && htmlAttributes.Keys.Any(k => k.Equals("format")))
                            {
                                decimal d = (Decimal)msValue;
                                string format = htmlAttributes["format"].ToString();
                                defaultValue = d.ToString(format);
                            }
                            else
                            {
                                defaultValue = msValue.ToString();
                            }
                        }
                        else
                            defaultValue = value;
                    }
                    if (!string.IsNullOrEmpty(defaultValue))
                        input.Attributes["value"] = defaultValue;

                    break;
            }

            input.AddAttributes(htmlAttributes);

            return input;
        }

        public HtmlGenericControl Modal(string modalId, string headerText, IEnumerable<Control> headerControls, IEnumerable<Control> bodyControls, IEnumerable<Control> footerControls)
        {
            var modal = new HtmlGenericControl("div");
            modal.ID = modalId;
            modal.Attributes["class"] = "modal hide fade";
            modal.Attributes["role"] = "dialog";

            #region header

            var headerContainer = new HtmlGenericControl("div");
            headerContainer.Attributes["class"] = "modal-header";
            modal.Controls.Add(headerContainer);

            var header = new HtmlGenericControl("h3");
            header.InnerText = headerText;
            headerContainer.Controls.Add(header);

            if (headerControls != null)
                headerControls.ToList().ForEach(cont => headerContainer.Controls.Add(cont));

            var headerButton = new HtmlGenericControl("i");
            headerButton.Attributes["class"] = "close icon-remove icon-themed";
            headerButton.Attributes["data-dismiss"] = "modal";

            headerContainer.Controls.Add(headerButton);

            var clearFix = new HtmlGenericControl("span");
            clearFix.Attributes["class"] = "clearfix";
            headerContainer.Controls.Add(clearFix);

            #endregion

            #region body

            var modalBody = new HtmlGenericControl("div");
            modalBody.Attributes["class"] = "modal-body";
            modal.Controls.Add(modalBody);

            if (bodyControls != null)
                bodyControls.ToList().ForEach(cont => modalBody.Controls.Add(cont));

            #endregion

            #region footer

            if (footerControls != null)
            {

                var footer = new HtmlGenericControl("div");
                footer.Attributes["class"] = "modal-footer";
                modal.Controls.Add(footer);

                footerControls.ToList().ForEach(cont => footer.Controls.Add(cont));
            }

            #endregion

            return modal;
        }

        #region Forms

        public MvcForm BeginHorizontalForm()
        {
            return BeginHorizontalForm(null);
        }

        public MvcForm BeginHorizontalForm(object htmlAttribtes)
        {
            var attributes = new RouteValueDictionary(htmlAttribtes);

            if (attributes.Keys.Any(k => k.Equals("class", StringComparison.CurrentCultureIgnoreCase)))
                attributes["class"] += " form-horizontal";
            else
                attributes["class"] += "form-horizontal";

            return BeginForm(attributes);
        }

        public MvcForm BeginInlineForm()
        {
            return BeginInlineForm(null);
        }

        public MvcForm BeginInlineForm(object htmlAttribtes)
        {
            var attributes = new RouteValueDictionary(htmlAttribtes);

            if (attributes.Keys.Any(k => k.Equals("class", StringComparison.CurrentCultureIgnoreCase)))
                attributes.Add("class", "form-inline");
            else
                attributes["class"] += "form-inline";

            return BeginForm(attributes);
        }

        public MvcForm BeginForm()
        {
            return BeginForm(null);
        }

        public MvcForm BeginForm(object htmlAttributes)
        {
            return BeginForm(new RouteValueDictionary(htmlAttributes));
        }

        private MvcForm BeginForm(RouteValueDictionary attributes)
        {

            TagBuilder formTag = new TagBuilder("form");
            formTag.MergeAttributes(attributes);

            if (!attributes.Keys.Any(k => k.Equals("action", StringComparison.CurrentCultureIgnoreCase)))
                formTag.MergeAttribute("action", _context.HttpContext.Request.RawUrl);

            if (!attributes.Keys.Any(k => k.Equals("method", StringComparison.CurrentCultureIgnoreCase)))
                formTag.MergeAttribute("method", "POST");

            _context.Writer.Write(formTag.ToString(TagRenderMode.StartTag));

            MvcForm form = new MvcForm(_context);

            return form;
        }

        #endregion

        #endregion

        private bool ModelStateError(string key, out ModelError error)
        {
            ModelState modelState;
            error = null;

            if (!string.IsNullOrEmpty(key))
            {
                if (_viewDataContainer.ViewData.ModelState.TryGetValue(key, out modelState))
                {
                    if (modelState.Errors.Any())
                    {
                        //Lets return the first error because we dont want to list all the errors for one property
                        error = modelState.Errors[0];
                        return true;
                    }
                }
            }

            return false;
        }

        private T GetModelStateValue<T>(string key)
        {
            T obj = default(T);

            object res = _viewDataContainer.ViewData.Eval(key);

            if (res != null)
                obj = (T)res;

            return obj;
        }

        private object GetModelInputValue(string key, Type destinationType)
        {
            ModelState modelState;

            if (_viewDataContainer.ViewData.ModelState.TryGetValue(key, out modelState))
            {
                if (modelState.Value != null)
                {
                    return modelState.Value.ConvertTo(destinationType, culture: CultureInfo.CurrentCulture);
                }
            }

            return null;
        }

        private ExpressionDataModel GetExpressionData<TProperty>(Expression<Func<TModel, TProperty>> expression)
        {
            string expressionText = ExpressionHelper.GetExpressionText(expression);

            string fullName = _context.ViewData.TemplateInfo.GetFullHtmlFieldName(expressionText);

            ModelMetadata meta = ModelMetadata.FromLambdaExpression(expression, (ViewDataDictionary<TModel>)_viewDataContainer.ViewData);

            return new ExpressionDataModel()
            {
                ExpressionText = expressionText,
                FullName = fullName,
                Metadata = meta
            };
        }
    }

    public class ExpressionDataModel
    {
        public string FullName { get; set; }
        public string ExpressionText { get; set; }
        public ModelMetadata Metadata { get; set; }
    }

    public enum InputType
    {
        CheckBox = 1,
        Hidden = 2,
        Password = 3,
        Radio = 4,
        Text = 5,
        DateTime = 100,
        Prepend = 101,
        Dropdown = 102,
        Lookup = 103
    }
}