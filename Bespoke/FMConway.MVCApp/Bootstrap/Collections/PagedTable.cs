﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMConway.MVCApp.Bootstrap.Collections
{
    public class PagedTable
    {
        public int Page { get; set; }
        public int PageCount { get; set; }
        public int RowCount { get; set; }
        public string[] ColumnHeaders { get; set; }
        public List<TableRow> Rows { get; set; }
        
    }

    public class TableRow
    {
        public string[] TableData { get; set; }
        public string[] Data { get; set; }
        public List<TableAction> Actions { get; set; }
    }

    public class TableAction
    {
        public string IconClass { get; set; }
        public string Url { get; set; }
        public string Class { get; set; }
        public string Title { get; set; }

        public TableAction(string url = null, string title = null, string icon = null, string @class = null)
        {
            Url = url;
            Title = title;
            IconClass = icon;
            Class = @class;
        }
    }
}