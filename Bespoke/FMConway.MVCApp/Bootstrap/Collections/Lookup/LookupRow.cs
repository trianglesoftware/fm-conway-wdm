﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.MVCApp.Bootstrap.Collections.Lookup
{
    public class LookupRow
    {
        public string Key { get; set; }
        public object Value { get; set; }
        public string[] TableData { get; set; }
        public string[] Data { get; set; }
    }
}
