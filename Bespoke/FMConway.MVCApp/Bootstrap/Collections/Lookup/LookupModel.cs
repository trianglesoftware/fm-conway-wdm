﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FMConway.MVCApp.Bootstrap.Collections.Lookup
{
    public class LookupModel
    {
        public int Page { get; set; }
        public int PageCount { get; set; }
        public string[] ColumnHeaders { get; set; }
        public List<LookupRow> Rows { get; set; }
    }
}
