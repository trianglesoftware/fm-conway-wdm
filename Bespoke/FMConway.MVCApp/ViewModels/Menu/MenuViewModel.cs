﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FMConway.MVCApp.Menu;

namespace FMConway.MVCApp.Models.Menu
{
    public class MenuViewModel
    {
        public List<MenuItem> MenuItems { get; set; }
        public bool HideItems { get; set; }
    }
}