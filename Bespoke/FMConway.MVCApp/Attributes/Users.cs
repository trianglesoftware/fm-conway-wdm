﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Triangle.Membership.Services.Sql;
using Triangle.Membership.Users;
using FMConway.MVCApp.Menu;

namespace FMConway.MVCApp.Attributes
{
    public class Users : DMAuthorizeAttribute
    {
        UserTypes _user;

        public Users(UserTypes user)
        {
            _user = user;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return IsAuthorised(httpContext);
        }

        public override bool IsAuthorised(HttpContextBase httpContext)
        {
            AspUserContext context = new AspUserContext(new SqlMembershipService());

            if (!context.IsAuthenticated)
                return false;

            return true;
        }

        [Flags]
        public enum UserTypes
        {
            Contractor = 1,
            Operation = 2,
        }
    }
}