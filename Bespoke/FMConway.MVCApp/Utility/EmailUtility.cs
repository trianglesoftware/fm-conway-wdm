﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;

namespace FMConway.MVCApp.Utility
{
    public class EmailUtility
    {
        public static bool ValidateEmail(string email)
        {
            string emailValidationRegex = WebConfigurationManager.AppSettings["EmailValidation"];

            return Regex.IsMatch(email, emailValidationRegex);
        }

        public static void SendEmail(string emailList, string subject, string body)
        {
            MailMessage mail = new MailMessage(WebConfigurationManager.AppSettings["NoReplyEmail"], emailList);
            
            SmtpClient client = new SmtpClient();
            
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            client.Send(mail);
        }

    }
}