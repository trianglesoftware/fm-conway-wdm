﻿CREATE TABLE [security].[UsersInRoles] (
    [Users_UserPK] UNIQUEIDENTIFIER NOT NULL,
    [Roles_RolePK] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_UsersInRoles] PRIMARY KEY NONCLUSTERED ([Users_UserPK] ASC, [Roles_RolePK] ASC),
    CONSTRAINT [FK_UsersInRoles_Role] FOREIGN KEY ([Roles_RolePK]) REFERENCES [security].[Roles] ([RolePK]),
    CONSTRAINT [FK_UsersInRoles_User] FOREIGN KEY ([Users_UserPK]) REFERENCES [security].[Users] ([UserPK])
);

