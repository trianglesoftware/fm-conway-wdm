﻿CREATE TABLE [security].[Roles] (
    [RolePK]        UNIQUEIDENTIFIER CONSTRAINT [DF_Roles_RolePK] DEFAULT (newid()) NOT NULL,
    [Name]          NVARCHAR (100)   NOT NULL,
    [SystemDefault] BIT              CONSTRAINT [DF_Roles_SystemDefault] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RolePK] ASC)
);

