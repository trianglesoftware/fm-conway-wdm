﻿CREATE TABLE [security].[Users] (
    [UserPK]                           UNIQUEIDENTIFIER NOT NULL,
    [UserName]                         NVARCHAR (100)   NOT NULL,
    [Password]                         NVARCHAR (100)   NOT NULL,
    [Email]                            NVARCHAR (300)   NOT NULL,
    [IsApproved]                       BIT              NOT NULL,
    [IsLockedOut]                      BIT              NOT NULL,
    [IsAnonymous]                      BIT              NOT NULL,
    [LastPasswordChangedDate]          DATETIME2 (7)    NULL,
    [LastLockoutDate]                  DATETIME2 (7)    NULL,
    [FailedPasswordAttemptCount]       INT              NOT NULL,
    [FailedPasswordAnswerAttemptCount] INT              NOT NULL,
    [LastLoginDate]                    DATETIME2 (7)    NULL,
    [IsActive]                         BIT              CONSTRAINT [DF_Users_IsActive] DEFAULT ((0)) NOT NULL,
    [PasswordResetToken]               NVARCHAR (100)   NULL,
    [PasswordResetExpiry]              DATETIME         NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserPK] ASC)
);

