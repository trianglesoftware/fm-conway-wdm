﻿
CREATE VIEW [dbo].[vv_TaskChecklists]
AS
SELECT 
	t.TaskPK,
	tc.CreatedDate,
	cq.ChecklistQuestion,
	cq.QuestionOrder,
	tca.Answer,
	tca.Reason,
	tca.TaskChecklistAnswerPK,
	tc.TaskChecklistPK,
	tca.UpdatedDate
FROM Tasks t
INNER JOIN Checklists c on c.ChecklistPK = t.ChecklistFK
INNER JOIN ChecklistQuestions cq on cq.ChecklistFK = c.ChecklistPK
LEFT JOIN TaskChecklists tc on tc.TaskFK = t.TaskPK and tc.ChecklistFK = c.ChecklistPK
LEFT JOIN TaskChecklistAnswers tca on tca.TaskChecklistFK = tc.TaskChecklistPK and tca.ChecklistQuestionFK = cq.ChecklistQuestionPK




