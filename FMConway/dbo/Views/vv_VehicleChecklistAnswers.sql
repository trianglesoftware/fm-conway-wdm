﻿CREATE VIEW dbo.vv_VehicleChecklistAnswers
AS
SELECT        TOP (100) PERCENT vca.VehicleChecklistAnswerPK, vc.VehicleChecklistPK, s.ShiftPK, v.Make + ' ' + v.Model AS Make, v.Registration, vc.StartOfShift, cq.ChecklistQuestion, vca.Answer, cq.QuestionOrder, 
                         vcd.VehicleChecklistDefectPK
FROM            dbo.Shifts AS s INNER JOIN
                         dbo.ShiftVehicles AS sv ON sv.ShiftFK = s.ShiftPK INNER JOIN
                         dbo.Vehicles AS v ON v.VehiclePK = sv.VehicleFK INNER JOIN
                         dbo.VehicleTypeChecklists AS vtc ON vtc.VehicleTypeFK = v.VehicleTypeFK INNER JOIN
                         dbo.Checklists AS c ON c.ChecklistPK = vtc.ChecklistFK INNER JOIN
                         dbo.ChecklistQuestions AS cq ON cq.ChecklistFK = c.ChecklistPK LEFT OUTER JOIN
                         dbo.VehicleChecklists AS vc ON sv.ShiftFK = vc.ShiftFK AND sv.VehicleFK = vc.VehicleFK LEFT OUTER JOIN
                         dbo.VehicleChecklistAnswers AS vca ON vca.VehicleChecklistFK = vc.VehicleChecklistPK AND vca.ChecklistQuestionFK = cq.ChecklistQuestionPK LEFT OUTER JOIN
                         dbo.VehicleChecklistDefects AS vcd ON vcd.VehicleChecklistAnswerFK = vca.VehicleChecklistAnswerPK
WHERE        (c.IsActive = 1) AND (s.IsActive = 1) AND (vtc.IsActive = 1)
ORDER BY vc.StartOfShift, Make, cq.QuestionOrder


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "s"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sv"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 135
               Right = 435
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vtc"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 252
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 473
               Bottom = 135
               Right = 682
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cq"
            Begin Extent = 
               Top = 6
               Left = 720
               Bottom = 135
               Right = 919
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vc"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
  ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vv_VehicleChecklistAnswers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       Begin Table = "vca"
            Begin Extent = 
               Top = 138
               Left = 284
               Bottom = 267
               Right = 512
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vcd"
            Begin Extent = 
               Top = 138
               Left = 550
               Bottom = 267
               Right = 777
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vv_VehicleChecklistAnswers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vv_VehicleChecklistAnswers';

