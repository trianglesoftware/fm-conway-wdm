﻿CREATE VIEW dbo.vv_PerformanceReport
AS
SELECT        TOP (100) PERCENT wi.WorkInstructionPK, wi.WorkInstructionNumber, j.JobNumber, cu.CustomerName, e1.EmployeeName, ct.ContractNumber, j.JobTitle, wi.WorkInstructionStartDate, jt.JobType, wi.TrafficManagementActivities, 
                         s.SchemeTitle, CASE WHEN wi.IsSuccessful = 1 THEN 'Yes' WHEN wi.IsSuccessful = 0 THEN 'No' ELSE '' END AS IsSuccessfulStr, wi.IsSuccessful, wi.ReasonNotes,
                             (SELECT        TOP (1) e2.EmployeeName
                               FROM            dbo.Schedules AS sc INNER JOIN
                                                         dbo.ScheduleEmployees AS se ON sc.SchedulePK = se.ScheduleFK INNER JOIN
                                                         dbo.Employees AS e2 ON e2.EmployeePK = se.EmployeeFK
                               WHERE        (sc.WorkInstructionFK = wi.WorkInstructionPK) AND (e2.EmployeeTypeFK = 'A08226AC-F91C-4645-8CA5-B7579FD6ADD3')) AS Foreman, wi.IsActive, rc.Code
FROM            dbo.WorkInstructions AS wi INNER JOIN
                         dbo.Jobs AS j ON wi.JobFK = j.JobPK INNER JOIN
                         dbo.Schemes AS s ON j.SchemeFK = s.SchemePK INNER JOIN
                         dbo.Contracts AS ct ON ct.ContractPK = s.ContractFK INNER JOIN
                         dbo.Customers AS cu ON cu.CustomerPK = ct.CustomerFK INNER JOIN
                         dbo.Employees AS e1 ON ct.EmployeeFK = e1.EmployeePK INNER JOIN
                         dbo.JobTypes AS jt ON wi.JobTypeFK = jt.JobTypePK LEFT OUTER JOIN
                         dbo.ReasonCodes AS rc ON wi.ReasonCodeFK = rc.ReasonCodePK
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vv_PerformanceReport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'  End
         Begin Table = "rc"
            Begin Extent = 
               Top = 221
               Left = 155
               Bottom = 332
               Right = 377
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1680
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vv_PerformanceReport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[18] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "wi"
            Begin Extent = 
               Top = 13
               Left = 21
               Bottom = 143
               Right = 331
            End
            DisplayFlags = 280
            TopColumn = 30
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 6
               Left = 386
               Bottom = 136
               Right = 611
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 649
               Bottom = 136
               Right = 819
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ct"
            Begin Extent = 
               Top = 6
               Left = 857
               Bottom = 136
               Right = 1060
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cu"
            Begin Extent = 
               Top = 255
               Left = 767
               Bottom = 385
               Right = 945
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e1"
            Begin Extent = 
               Top = 199
               Left = 983
               Bottom = 329
               Right = 1188
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "jt"
            Begin Extent = 
               Top = 270
               Left = 571
               Bottom = 400
               Right = 741
            End
            DisplayFlags = 280
            TopColumn = 0
       ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vv_PerformanceReport';

