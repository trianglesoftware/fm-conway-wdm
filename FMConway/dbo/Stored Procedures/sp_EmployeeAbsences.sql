﻿CREATE procedure [dbo].[sp_EmployeeAbsences] 
	@EmployeePK uniqueidentifier
as
begin

with cte
as   
(
	select
		1 as n, -- anchor member
		sub.FromDate as Date,
		sub.ToDate
	from
		(
		select
			ea.FromDate,
			ea.ToDate
		from
			dbo.Employees as e
			inner join
			dbo.EmployeeAbsences as ea on ea.EmployeeFK = e.EmployeePK
			where
			ea.IsActive = 1 and
			e.EmployeePK = @EmployeePK
		) as sub
	union all
	select
		n + 1, -- recursive member
		dateadd(d, 1, cte.Date),
		cte.ToDate
	FROM cte
	WHERE dateadd(d,1, cte.Date) <= cte.ToDate -- terminator
)
select cte.Date
from cte;

end
GO