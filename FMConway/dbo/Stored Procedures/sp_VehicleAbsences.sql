﻿CREATE procedure [dbo].[sp_VehicleAbsences] 
	@VehiclePK uniqueidentifier
as
begin

with cte
as   
(
	select
		1 as n, -- anchor member
		sub.FromDate as Date,
		sub.ToDate
	from
		(
		select
			va.FromDate,
			va.ToDate
		from
			dbo.Vehicles as v
			inner join
			dbo.VehicleAbsences as va on va.VehicleFK = v.VehiclePK
			where
			va.IsActive = 1 and
			v.VehiclePK = @VehiclePK
		) as sub
	union all
	select
		n + 1, -- recursive member
		dateadd(d, 1, cte.Date),
		cte.ToDate
	FROM cte
	WHERE dateadd(d,1, cte.Date) <= cte.ToDate -- terminator
)
select cte.Date
from cte;

end
GO