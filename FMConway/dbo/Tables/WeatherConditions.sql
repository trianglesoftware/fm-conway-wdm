﻿CREATE TABLE [dbo].[WeatherConditions] (
    [WeatherConditionPK] UNIQUEIDENTIFIER NOT NULL,
    [Name]               VARCHAR (200)    NOT NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_WeatherConditions] PRIMARY KEY CLUSTERED ([WeatherConditionPK] ASC)
);

