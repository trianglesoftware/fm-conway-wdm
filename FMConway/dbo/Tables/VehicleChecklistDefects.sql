﻿CREATE TABLE [dbo].[VehicleChecklistDefects] (
    [VehicleChecklistDefectPK] UNIQUEIDENTIFIER NOT NULL,
    [Description]              NVARCHAR (MAX)   NOT NULL,
    [VehicleChecklistAnswerFK] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                 BIT              NOT NULL,
    [CreatedByID]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]              DATETIME2 (7)    NOT NULL,
    [UpdatedByID]              UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]              DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_VehicleChecklistDefects] PRIMARY KEY CLUSTERED ([VehicleChecklistDefectPK] ASC),
    CONSTRAINT [FK_VehicleChecklistDefects_VehicleChecklistAnswers] FOREIGN KEY ([VehicleChecklistAnswerFK]) REFERENCES [dbo].[VehicleChecklistAnswers] ([VehicleChecklistAnswerPK])
);

