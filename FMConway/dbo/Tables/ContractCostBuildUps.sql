﻿CREATE TABLE [dbo].[ContractCostBuildUps] (
    [ContractCostBuildUpPK] UNIQUEIDENTIFIER NOT NULL,
    [ContractFK]            UNIQUEIDENTIFIER NOT NULL,
    [DocumentNumber]        NVARCHAR (300)   NOT NULL,
    [FileFK]                UNIQUEIDENTIFIER NOT NULL,
    [IsActive]              BIT              NOT NULL,
    [CreatedByFK]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]           DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]           DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ContractCostBuildUps] PRIMARY KEY CLUSTERED ([ContractCostBuildUpPK] ASC),
    CONSTRAINT [FK_ContractCostBuildUps_Contracts] FOREIGN KEY ([ContractFK]) REFERENCES [dbo].[Contracts] ([ContractPK]),
    CONSTRAINT [FK_ContractCostBuildUps_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK])
);

