﻿CREATE TABLE [dbo].[JobSpeeds] (
    [JobSpeedPK]  UNIQUEIDENTIFIER NOT NULL,
    [Name]        NVARCHAR (300)   NOT NULL,
    [IsDefault]   BIT              CONSTRAINT [DF_JobSpeeds_IsDefault] DEFAULT ((0)) NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    [UpdatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobSpeeds] PRIMARY KEY CLUSTERED ([JobSpeedPK] ASC)
);

