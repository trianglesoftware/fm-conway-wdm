﻿CREATE TABLE [dbo].[BriefingChecklists] (
    [BriefingChecklistPK] UNIQUEIDENTIFIER NOT NULL,
    [BriefingFK]          UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]         UNIQUEIDENTIFIER NOT NULL,
    [IsActive]            BIT              NOT NULL,
    [CreatedByID]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]         DATETIME2 (7)    NOT NULL,
    [UpdatedByID]         UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]         DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_BriefingChecklists] PRIMARY KEY CLUSTERED ([BriefingChecklistPK] ASC),
    CONSTRAINT [FK_BriefingChecklists_Briefings] FOREIGN KEY ([BriefingFK]) REFERENCES [dbo].[Briefings] ([BriefingPK]),
    CONSTRAINT [FK_BriefingChecklists_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK])
);




GO
CREATE NONCLUSTERED INDEX [NCI-BC]
    ON [dbo].[BriefingChecklists]([BriefingFK] ASC, [IsActive] ASC)
    INCLUDE([ChecklistFK]);

