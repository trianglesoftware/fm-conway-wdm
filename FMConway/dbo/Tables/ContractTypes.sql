﻿CREATE TABLE [dbo].[ContractTypes] (
    [ContractTypePK]          UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [ContractTypeDescription] NVARCHAR (200)   DEFAULT ('') NOT NULL,
    [IsActive]                BIT              NOT NULL,
    CONSTRAINT [PK_ContractTypes] PRIMARY KEY CLUSTERED ([ContractTypePK] ASC)
);



