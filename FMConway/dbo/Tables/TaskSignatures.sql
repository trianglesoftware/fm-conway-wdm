﻿CREATE TABLE [dbo].[TaskSignatures] (
    [TaskSignaturePK]  UNIQUEIDENTIFIER NOT NULL,
    [TaskFK]           UNIQUEIDENTIFIER NOT NULL,
    [FileFK]           UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]       UNIQUEIDENTIFIER NULL,
    [PrintedName]      VARCHAR (200)    NULL,
    [IsClient]         BIT              CONSTRAINT [DF_TaskSignatures_IsClient] DEFAULT ((0)) NOT NULL,
    [ClientVehicleReg] VARCHAR (100)    NULL,
    [IsActive]         BIT              NOT NULL,
    [CreatedByID]      UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]      DATETIME2 (7)    NOT NULL,
    [UpdatedByID]      UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]      DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_TaskSignatures] PRIMARY KEY CLUSTERED ([TaskSignaturePK] ASC),
    CONSTRAINT [FK_TaskSignatures_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_TaskSignatures_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_TaskSignatures_Tasks] FOREIGN KEY ([TaskFK]) REFERENCES [dbo].[Tasks] ([TaskPK])
);





