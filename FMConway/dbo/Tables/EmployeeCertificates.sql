﻿CREATE TABLE [dbo].[EmployeeCertificates] (
    [EmployeeCertificatePK] UNIQUEIDENTIFIER NOT NULL,
    [Name]                  NVARCHAR (500)   NOT NULL,
    [Description]           NVARCHAR (MAX)   NOT NULL,
    [CertificateTypeFK]     UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]            UNIQUEIDENTIFIER NOT NULL,
    [AreaFK]                UNIQUEIDENTIFIER NULL,
    [ExpiryDate]            DATETIME2 (7)    NOT NULL,
    [IsFirstAid]            BIT              NOT NULL,
    [IsActive]              BIT              NOT NULL,
    [CreatedByFK]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]           DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]           DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_EmployeeCertificates] PRIMARY KEY CLUSTERED ([EmployeeCertificatePK] ASC),
    CONSTRAINT [FK_EmployeeCertificates_Areas] FOREIGN KEY ([AreaFK]) REFERENCES [dbo].[Areas] ([AreaPK]),
    CONSTRAINT [FK_EmployeeCertificates_CertificateTypes] FOREIGN KEY ([CertificateTypeFK]) REFERENCES [dbo].[CertificateTypes] ([CertificateTypePK]),
    CONSTRAINT [FK_EmployeeCertificates_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK])
);

