﻿CREATE TABLE [dbo].[CertificateDocuments] (
    [CertificateDocumentPK] UNIQUEIDENTIFIER NOT NULL,
    [EmployeeCertificateFK] UNIQUEIDENTIFIER NOT NULL,
    [Title]                 NVARCHAR (300)   NOT NULL,
    [FileFK]                UNIQUEIDENTIFIER NOT NULL,
    [IsActive]              BIT              NOT NULL,
    [CreatedByFK]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]           DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]           DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_CertificateDocuments] PRIMARY KEY CLUSTERED ([CertificateDocumentPK] ASC),
    CONSTRAINT [FK_CertificateDocuments_EmployeeCertificates] FOREIGN KEY ([EmployeeCertificateFK]) REFERENCES [dbo].[EmployeeCertificates] ([EmployeeCertificatePK]),
    CONSTRAINT [FK_CertificateDocuments_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK])
);

