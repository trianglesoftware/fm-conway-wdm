﻿CREATE TABLE [dbo].[UserProfiles] (
    [UserFK]     UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_UserProfiles] PRIMARY KEY CLUSTERED ([UserFK] ASC),
    CONSTRAINT [FK_UserProfiles_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_UserProfiles_Users] FOREIGN KEY ([UserFK]) REFERENCES [security].[Users] ([UserPK])
);




GO
CREATE NONCLUSTERED INDEX [NCI-Emp]
    ON [dbo].[UserProfiles]([EmployeeFK] ASC);

