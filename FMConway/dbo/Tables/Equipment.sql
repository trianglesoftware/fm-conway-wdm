﻿CREATE TABLE [dbo].[Equipment] (
    [EquipmentPK]     UNIQUEIDENTIFIER NOT NULL,
    [EquipmentName]   NVARCHAR (500)   NOT NULL,
    [EquipmentTypeFK] UNIQUEIDENTIFIER NOT NULL,
    [FileFK]          UNIQUEIDENTIFIER NULL,
    [IsActive]        BIT              NOT NULL,
    [CreatedByFK]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]     DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]     UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]     DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Equipment] PRIMARY KEY CLUSTERED ([EquipmentPK] ASC),
    CONSTRAINT [FK_Equipment_EquipmentTypes] FOREIGN KEY ([EquipmentTypeFK]) REFERENCES [dbo].[EquipmentTypes] ([EquipmentTypePK]),
    CONSTRAINT [FK_Equipment_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK])
);

