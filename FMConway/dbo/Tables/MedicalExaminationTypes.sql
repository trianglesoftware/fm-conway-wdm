﻿CREATE TABLE [dbo].[MedicalExaminationTypes] (
    [MedicalExaminationTypePK] UNIQUEIDENTIFIER NOT NULL,
    [Name]                     NVARCHAR (500)   NOT NULL,
    [IsActive]                 BIT              NOT NULL,
    [CreatedByFK]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]              DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]              UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]              DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_MedicalExaminationTypes] PRIMARY KEY CLUSTERED ([MedicalExaminationTypePK] ASC)
);

