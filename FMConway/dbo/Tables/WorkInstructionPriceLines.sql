﻿CREATE TABLE [dbo].[WorkInstructionPriceLines] (
    [WorkInstructionPriceLinePK] UNIQUEIDENTIFIER NOT NULL,
    [WorkInstructionFK]          UNIQUEIDENTIFIER NOT NULL,
    [PriceLineFK]                UNIQUEIDENTIFIER NOT NULL,
    [Rate]                       DECIMAL (19, 4)  NULL,
    [Note]                       NVARCHAR (2000)  NULL,
    [IsActive]                   BIT              NOT NULL,
    [CreatedByFK]                UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_TaskPriceLines] PRIMARY KEY CLUSTERED ([WorkInstructionPriceLinePK] ASC),
    CONSTRAINT [FK_TaskPriceLines_PriceLines] FOREIGN KEY ([PriceLineFK]) REFERENCES [dbo].[PriceLines] ([PriceLinePK]),
    CONSTRAINT [FK_TaskPriceLines_WorkInstructions] FOREIGN KEY ([WorkInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);

