﻿CREATE TABLE [dbo].[JobDrawings] (
    [JobDrawingPK] UNIQUEIDENTIFIER NOT NULL,
    [JobFK]        UNIQUEIDENTIFIER NOT NULL,
    [DrawingFK]    UNIQUEIDENTIFIER NOT NULL,
    [IsActive]     BIT              NOT NULL,
    [CreatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]  DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobDrawings] PRIMARY KEY CLUSTERED ([JobDrawingPK] ASC),
    CONSTRAINT [FK_JobDrawings_Drawings] FOREIGN KEY ([DrawingFK]) REFERENCES [dbo].[Drawings] ([DrawingPK]),
    CONSTRAINT [FK_JobDrawings_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK])
);

