﻿CREATE TABLE [dbo].[JobPacks] (
    [JobPackPK]   UNIQUEIDENTIFIER NOT NULL,
    [ShiftFK]     UNIQUEIDENTIFIER NULL,
    [JobPackName] NVARCHAR (MAX)   NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedByID] UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    [UpdatedByID] UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobPacks] PRIMARY KEY CLUSTERED ([JobPackPK] ASC),
    CONSTRAINT [FK_JobPacks_JobPacks] FOREIGN KEY ([JobPackPK]) REFERENCES [dbo].[JobPacks] ([JobPackPK]),
    CONSTRAINT [FK_JobPacks_Shifts1] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK])
);

