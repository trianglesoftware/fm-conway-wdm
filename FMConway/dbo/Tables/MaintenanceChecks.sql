﻿CREATE TABLE [dbo].[MaintenanceChecks] (
    [MaintenanceCheckPK] UNIQUEIDENTIFIER NOT NULL,
    [Time]               DATETIME2 (7)    NOT NULL,
    [WorksInstructionFK] UNIQUEIDENTIFIER NOT NULL,
    [Description]        NVARCHAR (MAX)   NOT NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedByID]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByID]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_MaintenanceChecks] PRIMARY KEY CLUSTERED ([MaintenanceCheckPK] ASC),
    CONSTRAINT [FK_MaintenanceChecks_WorkInstructions] FOREIGN KEY ([WorksInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);




GO
CREATE NONCLUSTERED INDEX [NCI-WI-Active]
    ON [dbo].[MaintenanceChecks]([WorksInstructionFK] ASC, [IsActive] ASC);

