﻿CREATE TABLE [dbo].[AreaCallAreas] (
    [AreaCallAreaPK] UNIQUEIDENTIFIER NOT NULL,
    [Area]           NVARCHAR (500)   NOT NULL,
    [CallProtocol]   NVARCHAR (MAX)   NOT NULL,
    [NCCNumber]      NVARCHAR (200)   NOT NULL,
    [RCCNumber]      NVARCHAR (200)   NOT NULL,
    [PersonsName]    NVARCHAR (MAX)   NULL,
    [Reference]      NVARCHAR (MAX)   NULL,
    [ExtraDetails]   NVARCHAR (MAX)   NULL,
    [IsActive]       BIT              DEFAULT ((0)) NOT NULL,
    [CreatedByFK]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]    DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]    UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]    DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_AreaCallAreas] PRIMARY KEY CLUSTERED ([AreaCallAreaPK] ASC)
);

