﻿CREATE TABLE [dbo].[Status] (
    [StatusPK]                UNIQUEIDENTIFIER NOT NULL,
    [Status]                  NVARCHAR (200)   NOT NULL,
    [IsWorkInstructionStatus] BIT              CONSTRAINT [DF_Status_IsWorkInstructionStatus] DEFAULT ((0)) NOT NULL,
    [IsContractStatus]        BIT              NOT NULL,
    [IsJobStatus]             BIT              CONSTRAINT [DF_Status_IsJobStatus] DEFAULT ((0)) NOT NULL,
    [IsSignRequestStatus]     BIT              CONSTRAINT [DF_Status_IsSignRequestStatus] DEFAULT ((0)) NOT NULL,
    [IsLoadingSheetStatus]    BIT              CONSTRAINT [DF_Status_IsLoadingSheetStatus] DEFAULT ((0)) NOT NULL,
    [IsActive]                BIT              NOT NULL,
    [CreatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]             DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]             DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED ([StatusPK] ASC)
);

