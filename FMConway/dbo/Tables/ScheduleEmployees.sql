﻿CREATE TABLE [dbo].[ScheduleEmployees] (
    [ScheduleEmployeePK] UNIQUEIDENTIFIER NOT NULL,
    [ScheduleFK]         UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]         UNIQUEIDENTIFIER NOT NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ScheduleEmployees] PRIMARY KEY CLUSTERED ([ScheduleEmployeePK] ASC),
    CONSTRAINT [FK_ScheduleEmployees_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_ScheduleEmployees_Schedules] FOREIGN KEY ([ScheduleFK]) REFERENCES [dbo].[Schedules] ([SchedulePK])
);

