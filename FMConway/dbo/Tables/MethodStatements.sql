﻿CREATE TABLE [dbo].[MethodStatements] (
    [MethodStatementPK]    UNIQUEIDENTIFIER NOT NULL,
    [MethodStatementTitle] NVARCHAR (300)   NOT NULL,
    [Revision]             NVARCHAR (100)   NOT NULL,
    [FileFK]               UNIQUEIDENTIFIER NULL,
    [IsActive]             BIT              NOT NULL,
    [CreatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]          DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]          DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_MethodStatements] PRIMARY KEY CLUSTERED ([MethodStatementPK] ASC),
    CONSTRAINT [FK_MethodStatements_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK])
);

