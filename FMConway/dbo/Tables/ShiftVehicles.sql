﻿CREATE TABLE [dbo].[ShiftVehicles] (
    [ShiftVehiclePK]        UNIQUEIDENTIFIER NOT NULL,
    [ShiftFK]               UNIQUEIDENTIFIER NOT NULL,
    [VehicleFK]             UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]            UNIQUEIDENTIFIER NULL,
    [StartMileage]          INT              NOT NULL,
    [EndMileage]            INT              NOT NULL,
    [LoadingSheetCompleted] BIT              NOT NULL,
    [NotOnShift]            BIT              CONSTRAINT [DF_ShiftVehicles_NotOnShift] DEFAULT ((0)) NOT NULL,
    [ReasonNotOnShift]      NVARCHAR (MAX)   CONSTRAINT [DF_ShiftVehicles_ReasonNotOnShift] DEFAULT ('') NOT NULL,
    [IsActive]              BIT              NOT NULL,
    [CreatedByID]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]           DATETIME2 (7)    NOT NULL,
    [UpdatedByID]           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]           DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ShiftVehicles] PRIMARY KEY CLUSTERED ([ShiftVehiclePK] ASC),
    CONSTRAINT [FK_ShiftVehicles_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_ShiftVehicles_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK]),
    CONSTRAINT [FK_ShiftVehicles_Vehicles] FOREIGN KEY ([VehicleFK]) REFERENCES [dbo].[Vehicles] ([VehiclePK])
);



