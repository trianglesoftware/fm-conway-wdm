﻿CREATE TABLE [dbo].[LoadingSheets] (
    [LoadingSheetPK]     UNIQUEIDENTIFIER NOT NULL,
    [LoadingSheetNumber] INT              NOT NULL,
    [Name]               NVARCHAR (500)   CONSTRAINT [DF_LoadingSheets_Name] DEFAULT ('') NOT NULL,
    [WorkInstructionFK]  UNIQUEIDENTIFIER NOT NULL,
    [LoadingSheetDate]   DATETIME2 (7)    NOT NULL,
    [Comments]           NVARCHAR (MAX)   NOT NULL,
    [EmployeeFK]         UNIQUEIDENTIFIER NOT NULL,
    [StatusFK]           UNIQUEIDENTIFIER NOT NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_LoadingSheets] PRIMARY KEY CLUSTERED ([LoadingSheetPK] ASC),
    CONSTRAINT [FK_LoadingSheets_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_LoadingSheets_Status] FOREIGN KEY ([StatusFK]) REFERENCES [dbo].[Status] ([StatusPK]),
    CONSTRAINT [FK_LoadingSheets_WorkInstructions] FOREIGN KEY ([WorkInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);



