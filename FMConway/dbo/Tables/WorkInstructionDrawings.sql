﻿CREATE TABLE [dbo].[WorkInstructionDrawings] (
    [WorkInstructionDrawingPK] UNIQUEIDENTIFIER NOT NULL,
    [WorkInstructionFK]        UNIQUEIDENTIFIER NOT NULL,
    [DrawingFK]                UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                 BIT              NOT NULL,
    [CreatedByFK]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]              DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]              UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]              DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_WorkInstructionDrawings] PRIMARY KEY CLUSTERED ([WorkInstructionDrawingPK] ASC),
    CONSTRAINT [FK_WorkInstructionDrawings_Drawings] FOREIGN KEY ([DrawingFK]) REFERENCES [dbo].[Drawings] ([DrawingPK]),
    CONSTRAINT [FK_WorkInstructionDrawings_WorkInstructions] FOREIGN KEY ([WorkInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);

