﻿CREATE TABLE [dbo].[PriceLines] (
    [PriceLinePK] UNIQUEIDENTIFIER NOT NULL,
    [Code]        NVARCHAR (300)   NOT NULL,
    [Description] NVARCHAR (1000)  NULL,
    [DefaultRate] DECIMAL (19, 4)  NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    [UpdatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_PriceLines] PRIMARY KEY CLUSTERED ([PriceLinePK] ASC)
);

