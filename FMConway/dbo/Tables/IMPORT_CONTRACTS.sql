﻿CREATE TABLE [dbo].[IMPORT_CONTRACTS] (
    [Customer]             VARCHAR (50) NULL,
    [Sub Contract Code]    VARCHAR (50) NULL,
    [Contract Title]       VARCHAR (50) NULL,
    [Contract Type]        VARCHAR (50) NULL,
    [Depot]                VARCHAR (50) NULL,
    [Contract Status]      VARCHAR (50) NULL,
    [Contract Manager]     VARCHAR (50) NULL,
    [Billing Requirements] VARCHAR (50) NULL,
    [Date To Submit]       VARCHAR (50) NULL,
    [Quantity Surveyor]    VARCHAR (50) NULL
);

