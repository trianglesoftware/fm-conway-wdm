﻿CREATE TABLE [dbo].[OutOfHoursChecklistAnswerPhotos] (
    [OOHChecklistAnswerPhotoPK] UNIQUEIDENTIFIER NOT NULL,
    [OOHChecklistAnswerFK]      UNIQUEIDENTIFIER NOT NULL,
    [FileFK]                    UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                  BIT              NOT NULL,
    [CreatedByID]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]               DATETIME2 (7)    NOT NULL,
    [UpdatedByID]               UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]               DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_OutOfHoursChecklistAnswerPhotos] PRIMARY KEY CLUSTERED ([OOHChecklistAnswerPhotoPK] ASC),
    CONSTRAINT [FK_OutOfHoursChecklistAnswerPhotos_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_OutOfHoursChecklistAnswerPhotos_OutOfHoursChecklistAnswers] FOREIGN KEY ([OOHChecklistAnswerFK]) REFERENCES [dbo].[OutOfHoursChecklistAnswers] ([OOHChecklistAnswerPK])
);

