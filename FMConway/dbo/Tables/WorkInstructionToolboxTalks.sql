﻿CREATE TABLE [dbo].[WorkInstructionToolboxTalks] (
    [WorkInstructionToolboxTalkPK] UNIQUEIDENTIFIER NOT NULL,
    [WorkInstructionFK]            UNIQUEIDENTIFIER NOT NULL,
    [ToolboxTalkFK]                UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                     BIT              NOT NULL,
    [CreatedByFK]                  UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                  DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                  DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_WorkInstructionToolboxTalks] PRIMARY KEY CLUSTERED ([WorkInstructionToolboxTalkPK] ASC),
    CONSTRAINT [FK_WorkInstructionToolboxTalks_ToolboxTalks] FOREIGN KEY ([ToolboxTalkFK]) REFERENCES [dbo].[ToolboxTalks] ([ToolboxTalkPK]),
    CONSTRAINT [FK_WorkInstructionToolboxTalks_WorkInstructions] FOREIGN KEY ([WorkInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);

