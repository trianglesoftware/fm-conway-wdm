﻿CREATE TABLE [dbo].[EquipmentItemChecklists] (
    [EquipmentItemChecklistPK]    UNIQUEIDENTIFIER NOT NULL,
    [LoadingSheetEquipmentItemFK] UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]                 UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                    BIT              NOT NULL,
    [CreatedByFK]                 UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                 DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                 UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                 DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_EquipmentItemChecklists] PRIMARY KEY CLUSTERED ([EquipmentItemChecklistPK] ASC),
    CONSTRAINT [FK_EquipmentItemChecklists_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_EquipmentItemChecklists_LoadingSheetEquipmentItems] FOREIGN KEY ([LoadingSheetEquipmentItemFK]) REFERENCES [dbo].[LoadingSheetEquipmentItems] ([LoadingSheetEquipmentItemPK])
);

