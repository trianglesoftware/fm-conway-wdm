﻿CREATE TABLE [dbo].[Contracts] (
    [ContractPK]           UNIQUEIDENTIFIER NOT NULL,
    [ContractNumber]       NVARCHAR (200)   NOT NULL,
    [ContractTitle]        NVARCHAR (300)   NOT NULL,
    [CustomerFK]           UNIQUEIDENTIFIER NOT NULL,
    [DepotFK]              UNIQUEIDENTIFIER NOT NULL,
    [DateAwarded]          DATETIME2 (7)    NOT NULL,
    [DateExpires]          DATETIME2 (7)    NOT NULL,
    [StatusFK]             UNIQUEIDENTIFIER NOT NULL,
    [TenderNumber]         NVARCHAR (200)   NOT NULL,
    [EmployeeFK]           UNIQUEIDENTIFIER NOT NULL,
    [BillingRequirementFK] UNIQUEIDENTIFIER NOT NULL,
    [DateToBeSubmitted]    NVARCHAR (300)   NOT NULL,
    [QuantitySurveyor]     NVARCHAR (300)   NOT NULL,
    [IsHidden]             BIT              CONSTRAINT [DF_Contracts_IsHidden] DEFAULT ((0)) NOT NULL,
    [IsActive]             BIT              NOT NULL,
    [CreatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]          DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]          DATETIME2 (7)    NOT NULL,
    [ContractTypeFK]       UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Contracts] PRIMARY KEY CLUSTERED ([ContractPK] ASC),
    CONSTRAINT [FK_Contracts_BillingRequirements] FOREIGN KEY ([BillingRequirementFK]) REFERENCES [dbo].[BillingRequirements] ([BillingRequirementPK]),
    CONSTRAINT [FK_Contracts_ContractTypes] FOREIGN KEY ([ContractTypeFK]) REFERENCES [dbo].[ContractTypes] ([ContractTypePK]),
    CONSTRAINT [FK_Contracts_Customers] FOREIGN KEY ([CustomerFK]) REFERENCES [dbo].[Customers] ([CustomerPK]),
    CONSTRAINT [FK_Contracts_Depots] FOREIGN KEY ([DepotFK]) REFERENCES [dbo].[Depots] ([DepotPK]),
    CONSTRAINT [FK_Contracts_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_Contracts_Status] FOREIGN KEY ([StatusFK]) REFERENCES [dbo].[Status] ([StatusPK])
);






GO
CREATE NONCLUSTERED INDEX [NCI-Cust]
    ON [dbo].[Contracts]([CustomerFK] ASC);

