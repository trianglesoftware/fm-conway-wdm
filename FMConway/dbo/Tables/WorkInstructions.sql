﻿CREATE TABLE [dbo].[WorkInstructions] (
    [WorkInstructionPK]                     UNIQUEIDENTIFIER NOT NULL,
    [WorkInstructionNumber]                 INT              NOT NULL,
    [JobTypeFK]                             UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]                           UNIQUEIDENTIFIER NULL,
    [AreaFK]                                UNIQUEIDENTIFIER NULL,
    [JobFK]                                 UNIQUEIDENTIFIER NOT NULL,
    [JobPackFK]                             UNIQUEIDENTIFIER NOT NULL,
    [DepotFK]                               UNIQUEIDENTIFIER NOT NULL,
    [InstallWIPK]                           UNIQUEIDENTIFIER NULL,
    [ParentWIFK]                            UNIQUEIDENTIFIER NULL,
    [WorkInstructionStartDate]              DATETIME2 (7)    NOT NULL,
    [WorkInstructionFinishDate]             DATETIME2 (7)    NOT NULL,
    [SRWNumber]                             NVARCHAR (200)   NOT NULL,
    [Comments]                              NVARCHAR (MAX)   NOT NULL,
    [EmployeeFK]                            UNIQUEIDENTIFIER NOT NULL,
    [FirstCone]                             DATETIME2 (7)    NULL,
    [LastCone]                              DATETIME2 (7)    NULL,
    [TrafficManagementActivities]           NVARCHAR (500)   NOT NULL,
    [ActivityDescription]                   NVARCHAR (500)   NOT NULL,
    [SiteInductionRequired]                 BIT              NOT NULL,
    [SiteInductionNotes]                    NVARCHAR (MAX)   NOT NULL,
    [SiteSpecificConditions]                NVARCHAR (500)   NOT NULL,
    [SharedRoadSpace]                       NVARCHAR (MAX)   NOT NULL,
    [StatusFK]                              UNIQUEIDENTIFIER NOT NULL,
    [IsSuccessful]                          BIT              NULL,
    [ReasonCodeFK]                          UNIQUEIDENTIFIER NULL,
    [ReasonNotes]                           NVARCHAR (2000)  NULL,
    [IsScheduled]                           BIT              CONSTRAINT [DF_WorkInstructions_IsScheduled] DEFAULT ((0)) NOT NULL,
    [MaintenanceChecklistReminderInMinutes] INT              NULL,
    [IsOutOfHours]                          BIT              CONSTRAINT [DF_WorkInstructions_IsOutOfHours] DEFAULT ((0)) NOT NULL,
    [IsActive]                              BIT              NOT NULL,
    [CreatedByFK]                           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                           DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                           DATETIME2 (7)    NOT NULL,
    [IsChargeable]                          BIT              NOT NULL,
    [InvoiceDate]                           DATETIME         NULL,
    CONSTRAINT [PK_WorkInstructions] PRIMARY KEY CLUSTERED ([WorkInstructionPK] ASC),
    CONSTRAINT [FK_WorkInstructions_Areas] FOREIGN KEY ([AreaFK]) REFERENCES [dbo].[Areas] ([AreaPK]),
    CONSTRAINT [FK_WorkInstructions_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_WorkInstructions_Depots] FOREIGN KEY ([DepotFK]) REFERENCES [dbo].[Depots] ([DepotPK]),
    CONSTRAINT [FK_WorkInstructions_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_WorkInstructions_JobPacks] FOREIGN KEY ([JobPackFK]) REFERENCES [dbo].[JobPacks] ([JobPackPK]),
    CONSTRAINT [FK_WorkInstructions_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK]),
    CONSTRAINT [FK_WorkInstructions_JobTypes] FOREIGN KEY ([JobTypeFK]) REFERENCES [dbo].[JobTypes] ([JobTypePK]),
    CONSTRAINT [FK_WorkInstructions_ReasonCodes] FOREIGN KEY ([ReasonCodeFK]) REFERENCES [dbo].[ReasonCodes] ([ReasonCodePK]),
    CONSTRAINT [FK_WorkInstructions_Status] FOREIGN KEY ([StatusFK]) REFERENCES [dbo].[Status] ([StatusPK])
);






GO
CREATE NONCLUSTERED INDEX [NCI-JobPack]
    ON [dbo].[WorkInstructions]([JobPackFK] ASC);


GO
CREATE NONCLUSTERED INDEX [NCI-Area]
    ON [dbo].[WorkInstructions]([AreaFK] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_JobFK]
    ON [dbo].[WorkInstructions]([JobFK] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IsActive]
    ON [dbo].[WorkInstructions]([IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InstallWIPK]
    ON [dbo].[WorkInstructions]([InstallWIPK] ASC);

