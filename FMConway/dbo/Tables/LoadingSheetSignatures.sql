﻿CREATE TABLE [dbo].[LoadingSheetSignatures] (
    [LoadingSheetSignaturePK] UNIQUEIDENTIFIER NOT NULL,
    [LoadingSheetFK]          UNIQUEIDENTIFIER NOT NULL,
    [FileFK]                  UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                BIT              NOT NULL,
    [CreatedByID]             UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]             DATETIME2 (7)    NOT NULL,
    [UpdatedByID]             UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]             DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_LoadingSheetSignatures] PRIMARY KEY CLUSTERED ([LoadingSheetSignaturePK] ASC),
    CONSTRAINT [FK_LoadingSheetSignatures_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_LoadingSheetSignatures_LoadingSheets] FOREIGN KEY ([LoadingSheetFK]) REFERENCES [dbo].[LoadingSheets] ([LoadingSheetPK])
);

