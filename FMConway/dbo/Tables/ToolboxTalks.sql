﻿CREATE TABLE [dbo].[ToolboxTalks] (
    [ToolboxTalkPK] UNIQUEIDENTIFIER NOT NULL,
    [ToolboxTalk]   NVARCHAR (300)   NOT NULL,
    [Revision]      NVARCHAR (100)   NOT NULL,
    [Body]          NVARCHAR (MAX)   NOT NULL,
    [IsActive]      BIT              NOT NULL,
    [CreatedByFK]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]   DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]   UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]   DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ToolboxTalks] PRIMARY KEY CLUSTERED ([ToolboxTalkPK] ASC)
);

