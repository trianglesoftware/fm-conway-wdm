﻿CREATE TABLE [dbo].[JobCertificates] (
    [JobCertificatePK]  UNIQUEIDENTIFIER NOT NULL,
    [JobFK]             UNIQUEIDENTIFIER NOT NULL,
    [CertificateTypeFK] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]          BIT              NOT NULL,
    [CreatedByFK]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]       DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]       UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]       DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobCertificates] PRIMARY KEY CLUSTERED ([JobCertificatePK] ASC),
    CONSTRAINT [FK_JobCertificates_CertificateTypes] FOREIGN KEY ([CertificateTypeFK]) REFERENCES [dbo].[CertificateTypes] ([CertificateTypePK]),
    CONSTRAINT [FK_JobCertificates_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK])
);

