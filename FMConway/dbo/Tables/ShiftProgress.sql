﻿CREATE TABLE [dbo].[ShiftProgress] (
    [ShiftProgressID] INT            IDENTITY (1, 1) NOT NULL,
    [Progress]        NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ShiftProgress] PRIMARY KEY CLUSTERED ([ShiftProgressID] ASC)
);

