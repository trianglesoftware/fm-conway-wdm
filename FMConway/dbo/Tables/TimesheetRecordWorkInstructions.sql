﻿CREATE TABLE [dbo].[TimesheetRecordWorkInstructions] (
    [TimesheetRecordWorkInstructionPK] UNIQUEIDENTIFIER NOT NULL,
    [TimesheetRecordFK]                UNIQUEIDENTIFIER NOT NULL,
    [ShiftFK]                          UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                         BIT              NOT NULL,
    [CreatedDate]                      DATETIME2 (7)    NOT NULL,
    [CreatedByFK]                      UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                      DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                      UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_TimesheetRecordWorkInstructions] PRIMARY KEY CLUSTERED ([TimesheetRecordWorkInstructionPK] ASC),
    CONSTRAINT [FK_TimesheetRecordWorkInstructions_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK]),
    CONSTRAINT [FK_TimesheetRecordWorkInstructions_TimesheetRecords] FOREIGN KEY ([TimesheetRecordFK]) REFERENCES [dbo].[TimesheetRecords] ([TimesheetRecordPK])
);



