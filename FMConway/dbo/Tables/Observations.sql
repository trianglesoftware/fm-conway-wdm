﻿CREATE TABLE [dbo].[Observations] (
    [ObservationPK]          UNIQUEIDENTIFIER NOT NULL,
    [ShiftFK]                UNIQUEIDENTIFIER NULL,
    [PeopleInvolved]         NVARCHAR (MAX)   NOT NULL,
    [Location]               NVARCHAR (MAX)   NOT NULL,
    [ObservationDescription] NVARCHAR (MAX)   NOT NULL,
    [CauseOfProblem]         NVARCHAR (MAX)   NOT NULL,
    [ObservationDate]        DATETIME2 (7)    NOT NULL,
    [IsActive]               BIT              NOT NULL,
    [CreatedByID]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]            DATETIME2 (7)    NOT NULL,
    [UpdatedByID]            UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]            DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Observations] PRIMARY KEY CLUSTERED ([ObservationPK] ASC),
    CONSTRAINT [FK_Observations_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK])
);

