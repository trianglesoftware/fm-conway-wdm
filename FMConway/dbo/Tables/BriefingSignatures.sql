﻿CREATE TABLE [dbo].[BriefingSignatures] (
    [BriefingSignaturePK] UNIQUEIDENTIFIER NOT NULL,
    [BriefingFK]          UNIQUEIDENTIFIER NOT NULL,
    [FileFK]              UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]          UNIQUEIDENTIFIER NOT NULL,
    [BriefingStart]       DATETIME2 (7)    NOT NULL,
    [BriefingEnd]         DATETIME2 (7)    NOT NULL,
    [IsActive]            BIT              NOT NULL,
    [CreatedByID]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]         DATETIME2 (7)    NOT NULL,
    [UpdatedByID]         UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]         DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_BriefingSignatures] PRIMARY KEY CLUSTERED ([BriefingSignaturePK] ASC),
    CONSTRAINT [FK_BriefingSignatures_Briefings] FOREIGN KEY ([BriefingFK]) REFERENCES [dbo].[Briefings] ([BriefingPK]),
    CONSTRAINT [FK_BriefingSignatures_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_BriefingSignatures_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK])
);

