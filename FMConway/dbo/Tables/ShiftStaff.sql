﻿CREATE TABLE [dbo].[ShiftStaff] (
    [ShiftStaffPK]       UNIQUEIDENTIFIER NOT NULL,
    [ShiftFK]            UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]         UNIQUEIDENTIFIER NOT NULL,
    [AgencyEmployeeName] NVARCHAR (500)   NULL,
    [StartTime]          DATETIME2 (7)    NULL,
    [EndTime]            DATETIME2 (7)    NULL,
    [NotOnShift]         BIT              CONSTRAINT [DF_ShiftStaff_NotOnShift] DEFAULT ((0)) NOT NULL,
    [ReasonNotOn]        NVARCHAR (MAX)   CONSTRAINT [DF_ShiftStaff_ReasonNotOn] DEFAULT ('') NOT NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedByID]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByID]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ShiftStaff] PRIMARY KEY CLUSTERED ([ShiftStaffPK] ASC),
    CONSTRAINT [FK_ShiftStaff_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_ShiftStaff_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK])
);





