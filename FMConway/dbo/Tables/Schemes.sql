﻿CREATE TABLE [dbo].[Schemes] (
    [SchemePK]     UNIQUEIDENTIFIER NOT NULL,
    [SchemeTitle]  NVARCHAR (300)   NOT NULL,
    [Description]  NVARCHAR (MAX)   NOT NULL,
    [AddressLine1] NVARCHAR (500)   NOT NULL,
    [AddressLine2] NVARCHAR (500)   NOT NULL,
    [AddressLine3] NVARCHAR (500)   NOT NULL,
    [City]         NVARCHAR (500)   NOT NULL,
    [County]       NVARCHAR (500)   NOT NULL,
    [Postcode]     NVARCHAR (500)   NOT NULL,
    [Comments]     NVARCHAR (MAX)   NOT NULL,
    [ContractFK]   UNIQUEIDENTIFIER NOT NULL,
    [IsActive]     BIT              NOT NULL,
    [CreatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]  DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Schemes] PRIMARY KEY CLUSTERED ([SchemePK] ASC),
    CONSTRAINT [FK_Schemes_Contracts] FOREIGN KEY ([ContractFK]) REFERENCES [dbo].[Contracts] ([ContractPK])
);




GO
CREATE NONCLUSTERED INDEX [NCI-Contract]
    ON [dbo].[Schemes]([ContractFK] ASC);

