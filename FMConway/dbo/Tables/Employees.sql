﻿CREATE TABLE [dbo].[Employees] (
    [EmployeePK]           UNIQUEIDENTIFIER NOT NULL,
    [EmployeeTypeFK]       UNIQUEIDENTIFIER NOT NULL,
    [EmployeeName]         NVARCHAR (500)   NOT NULL,
    [PhoneNumber]          NVARCHAR (100)   NOT NULL,
    [EmailAddress]         NVARCHAR (300)   NOT NULL,
    [AgencyStaff]          BIT              CONSTRAINT [DF_Employees_AgencyStaff] DEFAULT ((0)) NOT NULL,
    [AgencyName]           NVARCHAR (500)   CONSTRAINT [DF_Employees_AgencyName] DEFAULT ('') NOT NULL,
    [IsCurrentlyEmployeed] BIT              CONSTRAINT [DF_Employees_IsCurrentlyEmployeed] DEFAULT ((0)) NOT NULL,
    [DepotFK]              UNIQUEIDENTIFIER NOT NULL,
    [VehicleFK]            UNIQUEIDENTIFIER NULL,
    [StartDate]            DATETIME2 (7)    NULL,
    [EndDate]              DATETIME2 (7)    NULL,
    [IsActive]             BIT              NOT NULL,
    [IsHidden]             BIT              CONSTRAINT [DF_Employees_IsHidden] DEFAULT ((0)) NOT NULL,
    [CreatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]          DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]          DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED ([EmployeePK] ASC),
    CONSTRAINT [FK_Employees_Depots] FOREIGN KEY ([DepotFK]) REFERENCES [dbo].[Depots] ([DepotPK]),
    CONSTRAINT [FK_Employees_EmployeeTypes] FOREIGN KEY ([EmployeeTypeFK]) REFERENCES [dbo].[EmployeeTypes] ([EmployeeTypePK]),
    CONSTRAINT [FK_Employees_Vehicles] FOREIGN KEY ([VehicleFK]) REFERENCES [dbo].[Vehicles] ([VehiclePK])
);





