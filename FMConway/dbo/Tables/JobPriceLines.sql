﻿CREATE TABLE [dbo].[JobPriceLines] (
    [JobPriceLinePK]             UNIQUEIDENTIFIER NOT NULL,
    [JobFK]                      UNIQUEIDENTIFIER NOT NULL,
    [PriceLineFK]                UNIQUEIDENTIFIER NOT NULL,
    [WorkInstructionPriceLineFK] UNIQUEIDENTIFIER NULL,
    [Rate]                       DECIMAL (19, 4)  NULL,
    [Note]                       NVARCHAR (2000)  NULL,
    [IsActive]                   BIT              NOT NULL,
    [CreatedByFK]                UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobPriceLines] PRIMARY KEY CLUSTERED ([JobPriceLinePK] ASC),
    CONSTRAINT [FK_JobPriceLines_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK]),
    CONSTRAINT [FK_JobPriceLines_PriceLines] FOREIGN KEY ([PriceLineFK]) REFERENCES [dbo].[PriceLines] ([PriceLinePK]),
    CONSTRAINT [FK_JobPriceLines_WorkInstructionPriceLines] FOREIGN KEY ([WorkInstructionPriceLineFK]) REFERENCES [dbo].[WorkInstructionPriceLines] ([WorkInstructionPriceLinePK])
);

