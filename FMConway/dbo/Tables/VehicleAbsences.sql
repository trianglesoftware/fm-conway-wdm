﻿CREATE TABLE [dbo].[VehicleAbsences] (
    [VehicleAbsencePK]       UNIQUEIDENTIFIER NOT NULL,
    [VehicleFK]              UNIQUEIDENTIFIER NOT NULL,
    [VehicleAbsenceReasonFK] UNIQUEIDENTIFIER NOT NULL,
    [FromDate]               DATETIME2 (7)    NOT NULL,
    [ToDate]                 DATETIME2 (7)    NOT NULL,
    [IsActive]               BIT              NOT NULL,
    [CreatedByFK]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]            DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]            UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]            DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_VehicleAbsences] PRIMARY KEY CLUSTERED ([VehicleAbsencePK] ASC),
    CONSTRAINT [FK_VehicleAbsences_VehicleAbsenceReasons] FOREIGN KEY ([VehicleAbsenceReasonFK]) REFERENCES [dbo].[VehicleAbsenceReasons] ([VehicleAbsenceReasonPK]),
    CONSTRAINT [FK_VehicleAbsences_Vehicles] FOREIGN KEY ([VehicleFK]) REFERENCES [dbo].[Vehicles] ([VehiclePK])
);

