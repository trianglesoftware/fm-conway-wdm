﻿CREATE TABLE [dbo].[VehicleChecklistAnswers] (
    [VehicleChecklistAnswerPK] UNIQUEIDENTIFIER NOT NULL,
    [VehicleChecklistFK]       UNIQUEIDENTIFIER NOT NULL,
    [ChecklistQuestionFK]      UNIQUEIDENTIFIER NOT NULL,
    [Answer]                   INT              NOT NULL,
    [CreatedByID]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]              DATETIME2 (7)    NOT NULL,
    [UpdatedByID]              UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]              DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_VehicleChecklistAnswers] PRIMARY KEY CLUSTERED ([VehicleChecklistAnswerPK] ASC),
    CONSTRAINT [FK_VehicleChecklistAnswers_ChecklistQuestions] FOREIGN KEY ([ChecklistQuestionFK]) REFERENCES [dbo].[ChecklistQuestions] ([ChecklistQuestionPK]),
    CONSTRAINT [FK_VehicleChecklistAnswers_VehicleChecklists] FOREIGN KEY ([VehicleChecklistFK]) REFERENCES [dbo].[VehicleChecklists] ([VehicleChecklistPK])
);




GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20171103-091035]
    ON [dbo].[VehicleChecklistAnswers]([VehicleChecklistFK] ASC);

