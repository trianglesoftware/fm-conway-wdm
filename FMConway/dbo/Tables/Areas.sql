﻿CREATE TABLE [dbo].[Areas] (
    [AreaPK]       UNIQUEIDENTIFIER NOT NULL,
    [Area]         NVARCHAR (500)   NOT NULL,
    [CallProtocol] NVARCHAR (MAX)   NOT NULL,
    [NCCNumber]    NVARCHAR (200)   CONSTRAINT [DF_Areas_NCCNumber] DEFAULT ('') NOT NULL,
    [RCCNumber]    NVARCHAR (200)   CONSTRAINT [DF_Areas_RCCNumber] DEFAULT ('') NOT NULL,
    [PersonsName]  NVARCHAR (MAX)   NULL,
    [Reference]    NVARCHAR (MAX)   NULL,
    [ExtraDetails] NVARCHAR (MAX)   NULL,
    [IsActive]     BIT              CONSTRAINT [DF_Areas_IsActive] DEFAULT ((0)) NOT NULL,
    [CreatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]  DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Areas] PRIMARY KEY CLUSTERED ([AreaPK] ASC)
);

