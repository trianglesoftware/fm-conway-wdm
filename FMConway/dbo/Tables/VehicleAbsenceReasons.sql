﻿CREATE TABLE [dbo].[VehicleAbsenceReasons] (
    [VehicleAbsenceReasonPK] UNIQUEIDENTIFIER NOT NULL,
    [Code]                   NVARCHAR (200)   NOT NULL,
    [Description]            NVARCHAR (1000)  NULL,
    [IsActive]               BIT              NOT NULL,
    [CreatedByFK]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]            DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]            UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]            DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_VehicleAbsenceReasons] PRIMARY KEY CLUSTERED ([VehicleAbsenceReasonPK] ASC)
);

