﻿CREATE TABLE [dbo].[JobTypes] (
    [JobTypePK]   UNIQUEIDENTIFIER NOT NULL,
    [JobType]     NVARCHAR (300)   NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    [UpdatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobTypes] PRIMARY KEY CLUSTERED ([JobTypePK] ASC)
);

