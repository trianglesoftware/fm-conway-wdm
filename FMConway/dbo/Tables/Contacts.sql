﻿CREATE TABLE [dbo].[Contacts] (
    [ContactPK]    UNIQUEIDENTIFIER NOT NULL,
    [ContactName]  NVARCHAR (500)   NOT NULL,
    [JobTitle]     NVARCHAR (500)   NOT NULL,
    [PhoneNumber]  NVARCHAR (100)   NOT NULL,
    [MobileNumber] NVARCHAR (100)   NOT NULL,
    [EmailAddress] NVARCHAR (500)   NOT NULL,
    [CustomerFK]   UNIQUEIDENTIFIER NOT NULL,
    [IsActive]     BIT              NOT NULL,
    [CreatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]  DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([ContactPK] ASC),
    CONSTRAINT [FK_Contacts_Customers] FOREIGN KEY ([CustomerFK]) REFERENCES [dbo].[Customers] ([CustomerPK])
);

