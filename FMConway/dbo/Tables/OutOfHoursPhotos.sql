﻿CREATE TABLE [dbo].[OutOfHoursPhotos] (
    [OutOfHoursPhotoPK] UNIQUEIDENTIFIER NOT NULL,
    [OutOfHoursFK]      UNIQUEIDENTIFIER NOT NULL,
    [FileFK]            UNIQUEIDENTIFIER NOT NULL,
    [IsActive]          BIT              NOT NULL,
    [CreatedByID]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]       DATETIME2 (7)    NOT NULL,
    [UpdatedByID]       UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]       DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_OutOfHoursPhotos] PRIMARY KEY CLUSTERED ([OutOfHoursPhotoPK] ASC),
    CONSTRAINT [FK_OutOfHoursPhotos_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_OutOfHoursPhotos_OutOfHoursPhotos] FOREIGN KEY ([OutOfHoursFK]) REFERENCES [dbo].[OutOfHours] ([OutOfHoursPK])
);

