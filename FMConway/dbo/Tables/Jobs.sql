﻿CREATE TABLE [dbo].[Jobs] (
    [JobPK]                     UNIQUEIDENTIFIER NOT NULL,
    [JobNumber]                 INT              IDENTITY (1, 1) NOT NULL,
    [JobTitle]                  NVARCHAR (300)   NOT NULL,
    [StartDate]                 DATETIME2 (7)    NOT NULL,
    [EndDate]                   DATETIME2 (7)    NULL,
    [Description]               NVARCHAR (500)   NOT NULL,
    [AddressLine1]              NVARCHAR (500)   NOT NULL,
    [AddressLine2]              NVARCHAR (500)   NOT NULL,
    [AddressLine3]              NVARCHAR (500)   NOT NULL,
    [City]                      NVARCHAR (500)   NOT NULL,
    [County]                    NVARCHAR (500)   NOT NULL,
    [Postcode]                  NVARCHAR (500)   NOT NULL,
    [Comments]                  NVARCHAR (MAX)   NOT NULL,
    [SchemeFK]                  UNIQUEIDENTIFIER NULL,
    [ContractFK]                UNIQUEIDENTIFIER NOT NULL,
    [DepotFK]                   UNIQUEIDENTIFIER NOT NULL,
    [IsClientSignatureRequired] BIT              CONSTRAINT [DF_Jobs_IsClientSignatureRequired] DEFAULT ((0)) NOT NULL,
    [JobSpeedFK]                UNIQUEIDENTIFIER NOT NULL,
    [StatusFK]                  UNIQUEIDENTIFIER NOT NULL,
    [IsSuccessful]              BIT              NULL,
    [ReasonCodeFK]              UNIQUEIDENTIFIER NULL,
    [ReasonNotes]               NVARCHAR (2000)  NULL,
    [Rural]                     BIT              NOT NULL,
    [IsActive]                  BIT              NOT NULL,
    [IsHidden]                  BIT              CONSTRAINT [DF_Jobs_IsHidden] DEFAULT ((0)) NOT NULL,
    [CreatedByFK]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]               DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]               UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]               DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Jobs] PRIMARY KEY CLUSTERED ([JobPK] ASC),
    CONSTRAINT [FK_Jobs_Contracts] FOREIGN KEY ([ContractFK]) REFERENCES [dbo].[Contracts] ([ContractPK]),
    CONSTRAINT [FK_Jobs_Depots] FOREIGN KEY ([DepotFK]) REFERENCES [dbo].[Depots] ([DepotPK]),
    CONSTRAINT [FK_Jobs_JobSpeeds] FOREIGN KEY ([JobSpeedFK]) REFERENCES [dbo].[JobSpeeds] ([JobSpeedPK]),
    CONSTRAINT [FK_Jobs_ReasonCodes] FOREIGN KEY ([ReasonCodeFK]) REFERENCES [dbo].[ReasonCodes] ([ReasonCodePK]),
    CONSTRAINT [FK_Jobs_Schemes] FOREIGN KEY ([SchemeFK]) REFERENCES [dbo].[Schemes] ([SchemePK]),
    CONSTRAINT [FK_Jobs_Status] FOREIGN KEY ([StatusFK]) REFERENCES [dbo].[Status] ([StatusPK])
);






GO
CREATE NONCLUSTERED INDEX [NCI-Scheme]
    ON [dbo].[Jobs]([SchemeFK] ASC);

