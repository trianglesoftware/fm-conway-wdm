﻿CREATE TABLE [dbo].[TaskChecklists] (
    [TaskChecklistPK] UNIQUEIDENTIFIER NOT NULL,
    [TaskFK]          UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]     UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              NOT NULL,
    [CreatedByID]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]     DATETIME2 (7)    NOT NULL,
    [UpdatedByID]     UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]     DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_TaskChecklists] PRIMARY KEY CLUSTERED ([TaskChecklistPK] ASC),
    CONSTRAINT [FK_TaskChecklists_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_TaskChecklists_Tasks] FOREIGN KEY ([TaskFK]) REFERENCES [dbo].[Tasks] ([TaskPK])
);

