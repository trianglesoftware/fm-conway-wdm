﻿CREATE TABLE [dbo].[TimesheetSignatures] (
    [TimesheetSignaturePK] UNIQUEIDENTIFIER NOT NULL,
    [TimesheetFK]          UNIQUEIDENTIFIER NOT NULL,
    [FileFK]               UNIQUEIDENTIFIER NOT NULL,
    [IsActive]             BIT              NOT NULL,
    [CreatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]          DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]          DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_TimesheetSignatures] PRIMARY KEY CLUSTERED ([TimesheetSignaturePK] ASC),
    CONSTRAINT [FK_TimesheetSignatures_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_TimesheetSignatures_Timesheets] FOREIGN KEY ([TimesheetFK]) REFERENCES [dbo].[Timesheets] ([TimesheetPK])
);

