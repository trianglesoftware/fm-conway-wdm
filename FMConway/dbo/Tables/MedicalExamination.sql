﻿CREATE TABLE [dbo].[MedicalExamination] (
    [MedicalExaminationPK]     UNIQUEIDENTIFIER NOT NULL,
    [Name]                     NVARCHAR (500)   NOT NULL,
    [Description]              NVARCHAR (MAX)   NOT NULL,
    [MedicalExaminationTypeFK] UNIQUEIDENTIFIER NOT NULL,
    [ExaminationDate]          DATETIME2 (7)    NOT NULL,
    [EmployeeFK]               UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                 BIT              NOT NULL,
    [CreatedByFK]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]              DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]              UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]              DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_MedicalExamination] PRIMARY KEY CLUSTERED ([MedicalExaminationPK] ASC),
    CONSTRAINT [FK_MedicalExamination_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_MedicalExamination_MedicalExaminationTypes] FOREIGN KEY ([MedicalExaminationTypeFK]) REFERENCES [dbo].[MedicalExaminationTypes] ([MedicalExaminationTypePK])
);

