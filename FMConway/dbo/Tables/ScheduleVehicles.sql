﻿CREATE TABLE [dbo].[ScheduleVehicles] (
    [ScheduleVehiclePK] UNIQUEIDENTIFIER NOT NULL,
    [ScheduleFK]        UNIQUEIDENTIFIER NOT NULL,
    [VehicleFK]         UNIQUEIDENTIFIER NOT NULL,
    [IsActive]          BIT              NOT NULL,
    [CreatedByID]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]       DATETIME2 (7)    NOT NULL,
    [UpdatedByID]       UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]       DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ScheduleVehicles] PRIMARY KEY CLUSTERED ([ScheduleVehiclePK] ASC),
    CONSTRAINT [FK_ScheduleVehicles_Schedules] FOREIGN KEY ([ScheduleFK]) REFERENCES [dbo].[Schedules] ([SchedulePK]),
    CONSTRAINT [FK_ScheduleVehicles_Vehicles] FOREIGN KEY ([VehicleFK]) REFERENCES [dbo].[Vehicles] ([VehiclePK])
);

