﻿CREATE TABLE [dbo].[TaskEquipments] (
    [TaskEquipmentPK] UNIQUEIDENTIFIER NOT NULL,
    [TaskFK]          UNIQUEIDENTIFIER NOT NULL,
    [EquipmentFK]     UNIQUEIDENTIFIER NOT NULL,
    [Quantity]        INT              NOT NULL,
    [IsActive]        BIT              NOT NULL,
    [CreatedDate]     DATETIME2 (7)    NOT NULL,
    [CreatedByID]     UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]     DATETIME2 (7)    NOT NULL,
    [UpdatedByID]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_TaskEquipments] PRIMARY KEY CLUSTERED ([TaskEquipmentPK] ASC),
    CONSTRAINT [FK_TaskEquipments_Equipment] FOREIGN KEY ([EquipmentFK]) REFERENCES [dbo].[Equipment] ([EquipmentPK]),
    CONSTRAINT [FK_TaskEquipments_Tasks] FOREIGN KEY ([TaskFK]) REFERENCES [dbo].[Tasks] ([TaskPK])
);

