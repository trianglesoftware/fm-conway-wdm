﻿CREATE TABLE [dbo].[VehicleChecklistDefectPhotos] (
    [VehicleChecklistDefectPhotoPK] UNIQUEIDENTIFIER NOT NULL,
    [VehicleChecklistDefectFK]      UNIQUEIDENTIFIER NOT NULL,
    [FileFK]                        UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                      BIT              NOT NULL,
    [CreatedByID]                   UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                   DATETIME2 (7)    NOT NULL,
    [UpdatedByID]                   UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                   DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_VehicleChecklistDefectPhotos] PRIMARY KEY CLUSTERED ([VehicleChecklistDefectPhotoPK] ASC),
    CONSTRAINT [FK_VehicleChecklistDefectPhotos_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_VehicleChecklistDefectPhotos_VehicleChecklistDefects] FOREIGN KEY ([VehicleChecklistDefectFK]) REFERENCES [dbo].[VehicleChecklistDefects] ([VehicleChecklistDefectPK])
);

