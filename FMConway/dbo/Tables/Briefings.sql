﻿CREATE TABLE [dbo].[Briefings] (
    [BriefingPK]     UNIQUEIDENTIFIER NOT NULL,
    [BriefingTypeFK] INT              NOT NULL,
    [JobPackFK]      UNIQUEIDENTIFIER NOT NULL,
    [Name]           NVARCHAR (MAX)   NOT NULL,
    [Details]        NVARCHAR (MAX)   NOT NULL,
    [FileFK]         UNIQUEIDENTIFIER NULL,
    [IsActive]       BIT              NOT NULL,
    [CreatedByID]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]    DATETIME2 (7)    NOT NULL,
    [UpdatedByID]    UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]    DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Briefings] PRIMARY KEY CLUSTERED ([BriefingPK] ASC),
    CONSTRAINT [FK_Briefings_BriefingTypes] FOREIGN KEY ([BriefingTypeFK]) REFERENCES [dbo].[BriefingTypes] ([BriefingTypePK]),
    CONSTRAINT [FK_Briefings_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_Briefings_JobPacks] FOREIGN KEY ([JobPackFK]) REFERENCES [dbo].[JobPacks] ([JobPackPK])
);




GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20171103-091113]
    ON [dbo].[Briefings]([JobPackFK] ASC, [IsActive] ASC);

