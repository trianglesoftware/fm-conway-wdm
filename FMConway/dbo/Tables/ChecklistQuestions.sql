﻿CREATE TABLE [dbo].[ChecklistQuestions] (
    [ChecklistQuestionPK] UNIQUEIDENTIFIER NOT NULL,
    [ChecklistQuestion]   NVARCHAR (MAX)   NOT NULL,
    [QuestionOrder]       INT              NOT NULL,
    [ChecklistFK]         UNIQUEIDENTIFIER NOT NULL,
    [IsActive]            BIT              NOT NULL,
    [CreatedByFK]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]         DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]         UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]         DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ChecklistQuestions] PRIMARY KEY CLUSTERED ([ChecklistQuestionPK] ASC),
    CONSTRAINT [FK_ChecklistQuestions_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK])
);

