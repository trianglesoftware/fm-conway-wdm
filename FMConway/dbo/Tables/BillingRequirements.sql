﻿CREATE TABLE [dbo].[BillingRequirements] (
    [BillingRequirementPK] UNIQUEIDENTIFIER NOT NULL,
    [BillingRequirement]   NVARCHAR (300)   NOT NULL,
    [IsActive]             BIT              NOT NULL,
    [CreatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]          DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]          DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_BillingRequirements] PRIMARY KEY CLUSTERED ([BillingRequirementPK] ASC)
);

