﻿CREATE TABLE [dbo].[TaskChecklistAnswers] (
    [TaskChecklistAnswerPK] UNIQUEIDENTIFIER NOT NULL,
    [TaskChecklistFK]       UNIQUEIDENTIFIER NOT NULL,
    [ChecklistQuestionFK]   UNIQUEIDENTIFIER NOT NULL,
    [Answer]                INT              NOT NULL,
    [Reason]                VARCHAR (1000)   NULL,
    [IsActive]              BIT              NOT NULL,
    [CreatedByID]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]           DATETIME2 (7)    NOT NULL,
    [UpdatedByID]           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]           DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_TaskChecklistAnsweres] PRIMARY KEY CLUSTERED ([TaskChecklistAnswerPK] ASC),
    CONSTRAINT [FK_TaskChecklistAnswers_ChecklistQuestions] FOREIGN KEY ([ChecklistQuestionFK]) REFERENCES [dbo].[ChecklistQuestions] ([ChecklistQuestionPK]),
    CONSTRAINT [FK_TaskChecklistAnswers_TaskChecklists] FOREIGN KEY ([TaskChecklistFK]) REFERENCES [dbo].[TaskChecklists] ([TaskChecklistPK])
);



