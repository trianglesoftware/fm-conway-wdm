﻿CREATE TABLE [dbo].[ErrorLogs] (
    [LogPK]       UNIQUEIDENTIFIER NOT NULL,
    [Description] NVARCHAR (MAX)   NOT NULL,
    [Location]    NVARCHAR (MAX)   NOT NULL,
    [Exception]   NVARCHAR (MAX)   NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ErrorLogs] PRIMARY KEY CLUSTERED ([LogPK] ASC)
);

