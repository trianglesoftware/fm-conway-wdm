﻿CREATE TABLE [dbo].[BriefingChecklistAnswers] (
    [BriefingChecklistAnswerPK] UNIQUEIDENTIFIER NOT NULL,
    [BriefingChecklistFK]       UNIQUEIDENTIFIER NOT NULL,
    [ChecklistQuestionFK]       UNIQUEIDENTIFIER NOT NULL,
    [Answer]                    BIT              NOT NULL,
    [IsActive]                  BIT              NOT NULL,
    [CreatedByID]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]               DATETIME2 (7)    NOT NULL,
    [UpdatedByID]               UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]               DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_BriefingChecklistAnswers] PRIMARY KEY CLUSTERED ([BriefingChecklistAnswerPK] ASC),
    CONSTRAINT [FK_BriefingChecklistAnswers_BriefingChecklists] FOREIGN KEY ([BriefingChecklistFK]) REFERENCES [dbo].[BriefingChecklists] ([BriefingChecklistPK]),
    CONSTRAINT [FK_BriefingChecklistAnswers_ChecklistQuestions] FOREIGN KEY ([ChecklistQuestionFK]) REFERENCES [dbo].[ChecklistQuestions] ([ChecklistQuestionPK])
);

