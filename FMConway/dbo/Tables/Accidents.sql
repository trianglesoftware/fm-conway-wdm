﻿CREATE TABLE [dbo].[Accidents] (
    [AccidentPK]          UNIQUEIDENTIFIER NOT NULL,
    [AccidentDescription] NVARCHAR (MAX)   NOT NULL,
    [AccidentDate]        DATETIME2 (7)    NOT NULL,
    [Location]            NVARCHAR (MAX)   NOT NULL,
    [Registrations]       NVARCHAR (MAX)   NOT NULL,
    [PersonReporting]     NVARCHAR (MAX)   NOT NULL,
    [PeopleInvolved]      NVARCHAR (MAX)   NOT NULL,
    [Witnesses]           NVARCHAR (MAX)   NOT NULL,
    [ActionTaken]         NVARCHAR (MAX)   NOT NULL,
    [ShiftFK]             UNIQUEIDENTIFIER NULL,
    [IsActive]            BIT              NOT NULL,
    [CreatedByID]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]         DATETIME2 (7)    NOT NULL,
    [UpdatedByID]         UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]         DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Accidents] PRIMARY KEY CLUSTERED ([AccidentPK] ASC),
    CONSTRAINT [FK_Accidents_Accidents] FOREIGN KEY ([AccidentPK]) REFERENCES [dbo].[Accidents] ([AccidentPK]),
    CONSTRAINT [FK_Accidents_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK])
);

