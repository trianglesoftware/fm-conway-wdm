﻿CREATE TABLE [dbo].[VehicleChecklists] (
    [VehicleChecklistPK] UNIQUEIDENTIFIER NOT NULL,
    [VehicleFK]          UNIQUEIDENTIFIER NOT NULL,
    [ShiftFK]            UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]        UNIQUEIDENTIFIER NOT NULL,
    [StartOfShift]       BIT              NOT NULL,
    [PhotoFileFK]        UNIQUEIDENTIFIER NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedByID]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByID]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_VehicleChecklists] PRIMARY KEY CLUSTERED ([VehicleChecklistPK] ASC),
    CONSTRAINT [FK_VehicleChecklists_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_VehicleChecklists_Files] FOREIGN KEY ([PhotoFileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_VehicleChecklists_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK]),
    CONSTRAINT [FK_VehicleChecklists_Vehicles] FOREIGN KEY ([VehicleFK]) REFERENCES [dbo].[Vehicles] ([VehiclePK])
);



