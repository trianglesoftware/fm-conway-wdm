﻿CREATE TABLE [dbo].[AccidentPhotos] (
    [AccidentPhotoPK] UNIQUEIDENTIFIER NOT NULL,
    [AccidentFK]      UNIQUEIDENTIFIER NOT NULL,
    [FileFK]          UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              NOT NULL,
    [CreatedByID]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]     DATETIME2 (7)    NOT NULL,
    [UpdatedByID]     UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]     DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_AccidentPhotos] PRIMARY KEY CLUSTERED ([AccidentPhotoPK] ASC),
    CONSTRAINT [FK_AccidentPhotos_Accidents] FOREIGN KEY ([AccidentFK]) REFERENCES [dbo].[Accidents] ([AccidentPK]),
    CONSTRAINT [FK_AccidentPhotos_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK])
);



