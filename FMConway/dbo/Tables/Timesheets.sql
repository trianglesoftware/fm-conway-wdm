﻿CREATE TABLE [dbo].[Timesheets] (
    [TimesheetPK] UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]  UNIQUEIDENTIFIER NOT NULL,
    [WeekStart]   DATETIME2 (7)    NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    [CreatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate] DATETIME2 (7)    NOT NULL,
    [UpdatedByFK] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Timesheets] PRIMARY KEY CLUSTERED ([TimesheetPK] ASC),
    CONSTRAINT [FK_Timesheets_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK])
);

