﻿CREATE TABLE [dbo].[ShiftProgressInfo] (
    [ShiftProgressInfoPK] INT              IDENTITY (1, 1) NOT NULL,
    [ShiftFK]             UNIQUEIDENTIFIER NOT NULL,
    [ShiftProgressID]     INT              NOT NULL,
    [Time]                DATETIME2 (7)    NOT NULL,
    [CreatedDate]         DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ShiftProgressInfo] PRIMARY KEY CLUSTERED ([ShiftProgressInfoPK] ASC),
    CONSTRAINT [FK_ShiftProgressInfo_ShiftProgress] FOREIGN KEY ([ShiftProgressID]) REFERENCES [dbo].[ShiftProgress] ([ShiftProgressID]),
    CONSTRAINT [FK_ShiftProgressInfo_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK])
);

