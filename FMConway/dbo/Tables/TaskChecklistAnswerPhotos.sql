﻿CREATE TABLE [dbo].[TaskChecklistAnswerPhotos] (
    [TaskChecklistAnswerPhotoPK] UNIQUEIDENTIFIER NOT NULL,
    [TaskChecklistAnswerFK]      UNIQUEIDENTIFIER NOT NULL,
    [FileFK]                     UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                   BIT              NOT NULL,
    [CreatedByID]                UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                DATETIME2 (7)    NOT NULL,
    [UpdatedByID]                UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_TaskChecklistAnswerPhotos] PRIMARY KEY CLUSTERED ([TaskChecklistAnswerPhotoPK] ASC),
    CONSTRAINT [FK_TaskChecklistAnswerPhotos_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_TaskChecklistAnswerPhotos_TaskChecklistAnswers] FOREIGN KEY ([TaskChecklistAnswerFK]) REFERENCES [dbo].[TaskChecklistAnswers] ([TaskChecklistAnswerPK])
);

