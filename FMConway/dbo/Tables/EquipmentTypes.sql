﻿CREATE TABLE [dbo].[EquipmentTypes] (
    [EquipmentTypePK]           UNIQUEIDENTIFIER NOT NULL,
    [Name]                      NVARCHAR (300)   NOT NULL,
    [EquipmentTypeVmsOrAssetFK] UNIQUEIDENTIFIER NULL,
    [IsActive]                  BIT              NOT NULL,
    [CreatedByFK]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]               DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]               UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]               DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_EquipmentTypes] PRIMARY KEY CLUSTERED ([EquipmentTypePK] ASC),
    CONSTRAINT [FK_EquipmentTypes_EquipmentTypeVmsOrAsset] FOREIGN KEY ([EquipmentTypeVmsOrAssetFK]) REFERENCES [dbo].[EquipmentTypeVmsOrAsset] ([EquipmentTypeVmsOrAssetPK])
);



