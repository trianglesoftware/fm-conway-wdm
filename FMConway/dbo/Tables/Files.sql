﻿CREATE TABLE [dbo].[Files] (
    [FilePK]           UNIQUEIDENTIFIER CONSTRAINT [DF_Files_FilePK] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [FileDownloadName] VARCHAR (200)    NOT NULL,
    [Data]             VARBINARY (MAX)  NOT NULL,
    [ContentType]      NVARCHAR (200)   NOT NULL,
    [Longitude]        DECIMAL (18, 6)  NULL,
    [Latitude]         DECIMAL (18, 6)  NULL,
    [IsActive]         BIT              NOT NULL,
    [CreatedByFK]      UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]      DATETIME         NOT NULL,
    [UpdatedByFK]      UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]      DATETIME         NOT NULL,
    CONSTRAINT [PK_Files] PRIMARY KEY CLUSTERED ([FilePK] ASC)
);



