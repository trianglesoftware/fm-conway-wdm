﻿CREATE TABLE [dbo].[ReasonCodes] (
    [ReasonCodePK]            UNIQUEIDENTIFIER NOT NULL,
    [Code]                    NVARCHAR (200)   NOT NULL,
    [Description]             NVARCHAR (1000)  NULL,
    [IsEmergencyCancellation] BIT              CONSTRAINT [DF_ReasonCodes_IsEmergencyCancellation] DEFAULT ((0)) NOT NULL,
    [IsActive]                BIT              NOT NULL,
    [CreatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]             DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]             DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobReasonCode] PRIMARY KEY CLUSTERED ([ReasonCodePK] ASC)
);

