﻿CREATE TABLE [dbo].[ContractTenderChecklists] (
    [ContractTenderChecklistPK] UNIQUEIDENTIFIER NOT NULL,
    [ContractFK]                UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]               UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                  BIT              NOT NULL,
    [CreatedByFK]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]               DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]               UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]               DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ContractTenderChecklistAnswers] PRIMARY KEY CLUSTERED ([ContractTenderChecklistPK] ASC),
    CONSTRAINT [FK_ContractTenderChecklists_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_ContractTenderChecklists_Contracts] FOREIGN KEY ([ContractFK]) REFERENCES [dbo].[Contracts] ([ContractPK])
);

