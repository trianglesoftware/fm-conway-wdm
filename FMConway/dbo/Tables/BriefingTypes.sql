﻿CREATE TABLE [dbo].[BriefingTypes] (
    [BriefingTypePK] INT            IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (MAX) NOT NULL,
    [IsActive]       BIT            NOT NULL,
    CONSTRAINT [PK_BriefingTypes] PRIMARY KEY CLUSTERED ([BriefingTypePK] ASC),
    CONSTRAINT [FK_BriefingTypes_BriefingTypes] FOREIGN KEY ([BriefingTypePK]) REFERENCES [dbo].[BriefingTypes] ([BriefingTypePK])
);

