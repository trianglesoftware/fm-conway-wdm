﻿CREATE TABLE [dbo].[EmployeeAbsenceReasons] (
    [EmployeeAbsenceReasonPK] UNIQUEIDENTIFIER NOT NULL,
    [Code]                    NVARCHAR (200)   NOT NULL,
    [Description]             NVARCHAR (1000)  NULL,
    [IsActive]                BIT              NOT NULL,
    [CreatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]             DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]             DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_StaffAbsenceReasons] PRIMARY KEY CLUSTERED ([EmployeeAbsenceReasonPK] ASC)
);

