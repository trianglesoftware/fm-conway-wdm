﻿CREATE TABLE [dbo].[JobTypeChecklists] (
    [JobTypeChecklistPK] UNIQUEIDENTIFIER NOT NULL,
    [JobTypeFK]          UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]        UNIQUEIDENTIFIER NOT NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobTypeChecklists] PRIMARY KEY CLUSTERED ([JobTypeChecklistPK] ASC),
    CONSTRAINT [FK_JobTypeChecklists_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_JobTypeChecklists_JobTypes] FOREIGN KEY ([JobTypeFK]) REFERENCES [dbo].[JobTypes] ([JobTypePK])
);

