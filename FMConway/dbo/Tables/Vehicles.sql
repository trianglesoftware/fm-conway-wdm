﻿CREATE TABLE [dbo].[Vehicles] (
    [VehiclePK]     UNIQUEIDENTIFIER NOT NULL,
    [Make]          NVARCHAR (200)   NOT NULL,
    [Model]         NVARCHAR (200)   NOT NULL,
    [Registration]  NVARCHAR (100)   NOT NULL,
    [VehicleTypeFK] UNIQUEIDENTIFIER NOT NULL,
    [DepotFK]       UNIQUEIDENTIFIER NOT NULL,
    [IsActive]      BIT              NOT NULL,
    [CreatedByFK]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]   DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]   UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]   DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Vehicles] PRIMARY KEY CLUSTERED ([VehiclePK] ASC),
    CONSTRAINT [FK_Vehicles_Depots] FOREIGN KEY ([DepotFK]) REFERENCES [dbo].[Depots] ([DepotPK]),
    CONSTRAINT [FK_Vehicles_VehicleTypes] FOREIGN KEY ([VehicleTypeFK]) REFERENCES [dbo].[VehicleTypes] ([VehicleTypePK])
);

