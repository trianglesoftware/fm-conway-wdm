﻿CREATE TABLE [dbo].[Depots] (
    [DepotPK]      UNIQUEIDENTIFIER NOT NULL,
    [DepotAreaFK]  UNIQUEIDENTIFIER NOT NULL,
    [DepotName]    NVARCHAR (400)   NOT NULL,
    [AddressLine1] NVARCHAR (400)   NOT NULL,
    [AddressLine2] NVARCHAR (400)   NOT NULL,
    [AddressLine3] NVARCHAR (400)   NOT NULL,
    [City]         NVARCHAR (400)   NOT NULL,
    [County]       NVARCHAR (400)   NOT NULL,
    [Postcode]     NVARCHAR (400)   NOT NULL,
    [IsActive]     BIT              NOT NULL,
    [IsHidden]     BIT              CONSTRAINT [DF_Depots_IsHidden] DEFAULT ((0)) NOT NULL,
    [CreatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]  DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Depots] PRIMARY KEY CLUSTERED ([DepotPK] ASC),
    CONSTRAINT [FK_Depots_DepotAreas] FOREIGN KEY ([DepotAreaFK]) REFERENCES [dbo].[DepotAreas] ([DepotAreaPK])
);



