﻿CREATE TABLE [dbo].[TimesheetRecords] (
    [TimesheetRecordPK]  UNIQUEIDENTIFIER NOT NULL,
    [StartDate]          DATETIME2 (7)    NOT NULL,
    [EndDate]            DATETIME2 (7)    NOT NULL,
    [WorkedHours]        INT              NOT NULL,
    [WorkedMins]         INT              NOT NULL,
    [BreakHours]         INT              NOT NULL,
    [BreakMins]          INT              NOT NULL,
    [TravelHours]        INT              NOT NULL,
    [TravelMins]         INT              NOT NULL,
    [IPVAllowance]       BIT              CONSTRAINT [DF_TimesheetRecords_IPVAllowance] DEFAULT ((0)) NOT NULL,
    [PaidBreakAllowance] BIT              CONSTRAINT [DF_TimesheetRecords_PaidBreakAllowance] DEFAULT ((0)) NOT NULL,
    [OnCallAllowance]    BIT              CONSTRAINT [DF_TimesheetRecords_OnCallAllowance] DEFAULT ((0)) NOT NULL,
    [TimesheetFK]        UNIQUEIDENTIFIER NOT NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [CreatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]        UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_TimesheetRecords] PRIMARY KEY CLUSTERED ([TimesheetRecordPK] ASC),
    CONSTRAINT [FK_TimesheetRecords_Timesheets] FOREIGN KEY ([TimesheetFK]) REFERENCES [dbo].[Timesheets] ([TimesheetPK])
);





