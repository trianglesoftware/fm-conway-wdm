﻿CREATE TABLE [dbo].[OutOfHoursChecklistAnswers] (
    [OOHChecklistAnswerPK] UNIQUEIDENTIFIER NOT NULL,
    [OOHChecklistFK]       UNIQUEIDENTIFIER NOT NULL,
    [ChecklistQuestionFK]  UNIQUEIDENTIFIER NOT NULL,
    [Answer]               INT              NOT NULL,
    [Reason]               NVARCHAR (100)   NOT NULL,
    [Time]                 DATETIME2 (7)    NOT NULL,
    [IsActive]             BIT              NOT NULL,
    [CreatedByID]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]          DATETIME2 (7)    NOT NULL,
    [UpdatedByID]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]          DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_OutOfHoursChecklistAnswers] PRIMARY KEY CLUSTERED ([OOHChecklistAnswerPK] ASC),
    CONSTRAINT [FK_OutOfHoursChecklistAnswers_ChecklistQuestions] FOREIGN KEY ([ChecklistQuestionFK]) REFERENCES [dbo].[ChecklistQuestions] ([ChecklistQuestionPK]),
    CONSTRAINT [FK_OutOfHoursChecklistAnswers_OutOfHoursChecklists] FOREIGN KEY ([OOHChecklistFK]) REFERENCES [dbo].[OutOfHoursChecklists] ([OOHChecklistPK])
);

