﻿CREATE TABLE [dbo].[WorkInstructionSignatures] (
    [WorkInstructionSignaturePK] UNIQUEIDENTIFIER NOT NULL,
    [WorkInstructionFK]          UNIQUEIDENTIFIER NOT NULL,
    [FileFK]                     UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]                 UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                   BIT              NOT NULL,
    [CreatedByID]                UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                DATETIME2 (7)    NOT NULL,
    [UpdatedByID]                UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_WorkInstructionSignatures] PRIMARY KEY CLUSTERED ([WorkInstructionSignaturePK] ASC),
    CONSTRAINT [FK_WorkInstructionSignatures_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK]),
    CONSTRAINT [FK_WorkInstructionSignatures_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_WorkInstructionSignatures_WorkInstructions] FOREIGN KEY ([WorkInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);

