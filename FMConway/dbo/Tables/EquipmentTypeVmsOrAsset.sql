﻿CREATE TABLE [dbo].[EquipmentTypeVmsOrAsset] (
    [EquipmentTypeVmsOrAssetPK] UNIQUEIDENTIFIER NOT NULL,
    [Name]                      NVARCHAR (100)   NOT NULL,
    [IsActive]                  BIT              NOT NULL,
    [CreatedByFK]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]               DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]               UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]               DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_EquipmentTypeCategories] PRIMARY KEY CLUSTERED ([EquipmentTypeVmsOrAssetPK] ASC)
);

