﻿CREATE TABLE [dbo].[VehicleTypeChecklists] (
    [VehicleTypeChecklistPK] UNIQUEIDENTIFIER NOT NULL,
    [VehicleTypeFK]          UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]            UNIQUEIDENTIFIER NOT NULL,
    [IsActive]               BIT              NOT NULL,
    [CreatedByFK]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]            DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]            UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]            DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_VehicleTypeChecklists] PRIMARY KEY CLUSTERED ([VehicleTypeChecklistPK] ASC),
    CONSTRAINT [FK_VehicleTypeChecklists_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_VehicleTypeChecklists_VehicleTypes] FOREIGN KEY ([VehicleTypeFK]) REFERENCES [dbo].[VehicleTypes] ([VehicleTypePK])
);

