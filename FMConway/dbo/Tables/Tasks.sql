﻿CREATE TABLE [dbo].[Tasks] (
    [TaskPK]                  UNIQUEIDENTIFIER NOT NULL,
    [Name]                    NVARCHAR (MAX)   NOT NULL,
    [WorksInstructionFK]      UNIQUEIDENTIFIER NOT NULL,
    [TaskTypeFK]              INT              NOT NULL,
    [TaskOrder]               INT              NOT NULL,
    [AreaCallAreaFK]          UNIQUEIDENTIFIER NULL,
    [ChecklistFK]             UNIQUEIDENTIFIER NULL,
    [StartTime]               DATETIME2 (7)    NULL,
    [EndTime]                 DATETIME2 (7)    NULL,
    [Longitude]               DECIMAL (18, 6)  NULL,
    [Latitude]                DECIMAL (18, 6)  NULL,
    [Notes]                   NVARCHAR (MAX)   NULL,
    [PhotosRequired]          BIT              CONSTRAINT [DF_Tasks_PhotosRequired] DEFAULT ((0)) NOT NULL,
    [ShowDetails]             BIT              CONSTRAINT [DF_Tasks_ShowDetails] DEFAULT ((0)) NOT NULL,
    [ShowClientSiganture]     BIT              CONSTRAINT [DF_Tasks_ShowClientSiganture] DEFAULT ((0)) NOT NULL,
    [ClientSignatureRequired] BIT              CONSTRAINT [DF_Tasks_ClientSignatureRequired] DEFAULT ((0)) NOT NULL,
    [ShowStaffSignature]      BIT              CONSTRAINT [DF_Tasks_ShowStaffSignature] DEFAULT ((0)) NOT NULL,
    [StaffSignatureRequired]  BIT              CONSTRAINT [DF_Tasks_StaffSignatureRequired] DEFAULT ((0)) NOT NULL,
    [Tag]                     VARCHAR (1000)   NULL,
    [WeatherConditionFK]      UNIQUEIDENTIFIER NULL,
    [IsCancelled]             BIT              NOT NULL,
    [IsActive]                BIT              NOT NULL,
    [CreatedByID]             UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]             DATETIME2 (7)    NOT NULL,
    [UpdatedByID]             UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]             DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED ([TaskPK] ASC),
    CONSTRAINT [FK_Tasks_AreaCallAreas] FOREIGN KEY ([AreaCallAreaFK]) REFERENCES [dbo].[AreaCallAreas] ([AreaCallAreaPK]),
    CONSTRAINT [FK_Tasks_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_Tasks_TaskTypes] FOREIGN KEY ([TaskTypeFK]) REFERENCES [dbo].[TaskTypes] ([TaskTypePK]),
    CONSTRAINT [FK_Tasks_WeatherConditions] FOREIGN KEY ([WeatherConditionFK]) REFERENCES [dbo].[WeatherConditions] ([WeatherConditionPK]),
    CONSTRAINT [FK_Tasks_WorkInstructions] FOREIGN KEY ([WorksInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);






GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20171103-091018]
    ON [dbo].[Tasks]([WorksInstructionFK] ASC);


GO
CREATE NONCLUSTERED INDEX [NCI-ACA]
    ON [dbo].[Tasks]([AreaCallAreaFK] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WorkInstructionFK]
    ON [dbo].[Tasks]([WorksInstructionFK] ASC, [IsCancelled] ASC, [IsActive] ASC);

