﻿CREATE TABLE [dbo].[TaskTypes] (
    [TaskTypePK] INT            IDENTITY (1, 1) NOT NULL,
    [TaskType]   NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_TaskTypes] PRIMARY KEY CLUSTERED ([TaskTypePK] ASC)
);

