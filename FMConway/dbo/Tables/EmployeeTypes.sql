﻿CREATE TABLE [dbo].[EmployeeTypes] (
    [EmployeeTypePK]    UNIQUEIDENTIFIER NOT NULL,
    [EmployeeType]      NVARCHAR (300)   NOT NULL,
    [IsContractManager] BIT              CONSTRAINT [DF_EmployeeTypes_IsContractManager] DEFAULT ((0)) NOT NULL,
    [IsActive]          BIT              NOT NULL,
    [CreatedByFK]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]       DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]       UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]       DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_EmployeeTypes] PRIMARY KEY CLUSTERED ([EmployeeTypePK] ASC)
);

