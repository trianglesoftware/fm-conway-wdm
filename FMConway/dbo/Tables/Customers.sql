﻿CREATE TABLE [dbo].[Customers] (
    [CustomerPK]    UNIQUEIDENTIFIER NOT NULL,
    [CustomerName]  NVARCHAR (200)   NOT NULL,
    [AccountNumber] NVARCHAR (200)   NOT NULL,
    [CustomerCode]  NVARCHAR (200)   NOT NULL,
    [AddressLine1]  NVARCHAR (500)   NOT NULL,
    [AddressLine2]  NVARCHAR (500)   NOT NULL,
    [AddressLine3]  NVARCHAR (500)   NOT NULL,
    [City]          NVARCHAR (500)   NOT NULL,
    [County]        NVARCHAR (500)   NOT NULL,
    [Postcode]      NVARCHAR (500)   NOT NULL,
    [Comments]      NVARCHAR (MAX)   NOT NULL,
    [PhoneNumber]   NVARCHAR (100)   NOT NULL,
    [FaxNumber]     NVARCHAR (100)   NOT NULL,
    [EmailAddress]  NVARCHAR (300)   NOT NULL,
    [IsActive]      BIT              NOT NULL,
    [IsHidden]      BIT              CONSTRAINT [DF_Customers_IsHidden] DEFAULT ((0)) NOT NULL,
    [CreatedByFK]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]   DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]   UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]   DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED ([CustomerPK] ASC)
);



