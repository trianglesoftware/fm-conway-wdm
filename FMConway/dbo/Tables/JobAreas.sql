﻿CREATE TABLE [dbo].[JobAreas] (
    [JobAreaPK]   UNIQUEIDENTIFIER NOT NULL,
    [JobFK]       UNIQUEIDENTIFIER NOT NULL,
    [AreaFK]      UNIQUEIDENTIFIER NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    [UpdatedByFK] UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobAreas] PRIMARY KEY CLUSTERED ([JobAreaPK] ASC),
    CONSTRAINT [FK_JobAreas_Areas] FOREIGN KEY ([AreaFK]) REFERENCES [dbo].[Areas] ([AreaPK]),
    CONSTRAINT [FK_JobAreas_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK])
);

