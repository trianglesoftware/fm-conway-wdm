﻿CREATE TABLE [dbo].[TaskEquipmentAssets] (
    [TaskEquipmentAssetPK] UNIQUEIDENTIFIER NOT NULL,
    [TaskEquipmentFK]      UNIQUEIDENTIFIER NOT NULL,
    [ItemNumber]           INT              NOT NULL,
    [AssetNumber]          VARCHAR (200)    NOT NULL,
    CONSTRAINT [PK_TaskEquipmentAssets] PRIMARY KEY CLUSTERED ([TaskEquipmentAssetPK] ASC),
    CONSTRAINT [FK_TaskEquipmentAssets_TaskEquipments] FOREIGN KEY ([TaskEquipmentFK]) REFERENCES [dbo].[TaskEquipments] ([TaskEquipmentPK])
);

