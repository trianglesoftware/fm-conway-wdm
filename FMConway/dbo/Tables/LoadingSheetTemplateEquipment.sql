﻿CREATE TABLE [dbo].[LoadingSheetTemplateEquipment] (
    [LoadingSheetTemplateEquipmentPK] UNIQUEIDENTIFIER NOT NULL,
    [LoadingSheetTemplateFK]          UNIQUEIDENTIFIER NOT NULL,
    [NonStandardEquipment]            NVARCHAR (1000)  NOT NULL,
    [IsNonStandard]                   BIT              NOT NULL,
    [EquipmentFK]                     UNIQUEIDENTIFIER NULL,
    [Quantity]                        DECIMAL (18, 2)  CONSTRAINT [DF_LoadingSheetTemplateEquipment_Quantity] DEFAULT ((0)) NOT NULL,
    [SheetOrder]                      INT              CONSTRAINT [DF_LoadingSheetTemplateEquipment_SheetOrder] DEFAULT ((0)) NOT NULL,
    [IsActive]                        BIT              NOT NULL,
    [CreatedByFK]                     UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                     DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                     UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                     DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_LoadingSheetTemplateEquipment] PRIMARY KEY CLUSTERED ([LoadingSheetTemplateEquipmentPK] ASC),
    CONSTRAINT [FK_LoadingSheetTemplateEquipment_Equipment] FOREIGN KEY ([EquipmentFK]) REFERENCES [dbo].[Equipment] ([EquipmentPK]),
    CONSTRAINT [FK_LoadingSheetTemplateEquipment_LoadingSheetTemplates] FOREIGN KEY ([LoadingSheetTemplateFK]) REFERENCES [dbo].[LoadingSheetTemplates] ([LoadingSheetTemplatePK])
);

