﻿CREATE TABLE [dbo].[ObservationPhotos] (
    [ObservationPhotoPK] UNIQUEIDENTIFIER NOT NULL,
    [ObservationFK]      UNIQUEIDENTIFIER NOT NULL,
    [FileFK]             UNIQUEIDENTIFIER NOT NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedByID]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]        DATETIME2 (7)    NOT NULL,
    [UpdatedByID]        UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]        DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_ObservationPhotos] PRIMARY KEY CLUSTERED ([ObservationPhotoPK] ASC),
    CONSTRAINT [FK_ObservationPhotos_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_ObservationPhotos_Observations] FOREIGN KEY ([ObservationFK]) REFERENCES [dbo].[Observations] ([ObservationPK])
);



