﻿CREATE TABLE [dbo].[JobPackDocuments] (
    [JobPackDocumentPK] UNIQUEIDENTIFIER NOT NULL,
    [JobPackFK]         UNIQUEIDENTIFIER NOT NULL,
    [FileFK]            UNIQUEIDENTIFIER NOT NULL,
    [IsActive]          BIT              NOT NULL,
    [CreatedByID]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]       DATETIME2 (7)    NOT NULL,
    [UpdatedByID]       UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]       DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobPackDocuments] PRIMARY KEY CLUSTERED ([JobPackDocumentPK] ASC),
    CONSTRAINT [FK_JobPackDocuments_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_JobPackDocuments_JobPacks] FOREIGN KEY ([JobPackFK]) REFERENCES [dbo].[JobPacks] ([JobPackPK])
);




GO
CREATE NONCLUSTERED INDEX [NIC-JP-File]
    ON [dbo].[JobPackDocuments]([JobPackFK] ASC, [IsActive] ASC)
    INCLUDE([FileFK]);

