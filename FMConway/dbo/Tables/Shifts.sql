﻿CREATE TABLE [dbo].[Shifts] (
    [ShiftPK]               UNIQUEIDENTIFIER NOT NULL,
    [ShiftDate]             DATETIME2 (7)    NOT NULL,
    [ShiftComplete]         BIT              NOT NULL,
    [ShiftCompletionStatus] INT              NOT NULL,
    [IsActive]              BIT              NOT NULL,
    [CreatedByID]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]           DATETIME2 (7)    NOT NULL,
    [UpdatedByID]           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]           DATETIME2 (7)    NOT NULL,
    [IsCancelled]           BIT              NOT NULL,
    [IsConfirmed]           BIT              CONSTRAINT [DF_Shifts_IsConfirmed] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Shifts] PRIMARY KEY CLUSTERED ([ShiftPK] ASC)
);




GO
CREATE NONCLUSTERED INDEX [NCI-Created]
    ON [dbo].[Shifts]([CreatedDate] ASC)
    INCLUDE([ShiftDate], [ShiftComplete], [IsActive], [IsCancelled]);

