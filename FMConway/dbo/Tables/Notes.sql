﻿CREATE TABLE [dbo].[Notes] (
    [NotePK]      UNIQUEIDENTIFIER NOT NULL,
    [ShiftFK]     UNIQUEIDENTIFIER NOT NULL,
    [Detail]      NVARCHAR (MAX)   NOT NULL,
    [NoteDate]    DATETIME2 (7)    NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedByID] UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Notes] PRIMARY KEY CLUSTERED ([NotePK] ASC),
    CONSTRAINT [FK_Notes_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK])
);



