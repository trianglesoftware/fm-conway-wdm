﻿CREATE TABLE [dbo].[JobToolboxTalks] (
    [JobToolboxTalkPK] UNIQUEIDENTIFIER NOT NULL,
    [JobFK]            UNIQUEIDENTIFIER NOT NULL,
    [ToolboxTalkFK]    UNIQUEIDENTIFIER NOT NULL,
    [IsActive]         BIT              NOT NULL,
    [CreatedByFK]      UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]      DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]      UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]      DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobToolboxTalks] PRIMARY KEY CLUSTERED ([JobToolboxTalkPK] ASC),
    CONSTRAINT [FK_JobToolboxTalks_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK]),
    CONSTRAINT [FK_JobToolboxTalks_ToolboxTalks] FOREIGN KEY ([ToolboxTalkFK]) REFERENCES [dbo].[ToolboxTalks] ([ToolboxTalkPK])
);

