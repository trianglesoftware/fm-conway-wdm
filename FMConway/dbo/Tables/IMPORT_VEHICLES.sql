﻿CREATE TABLE [dbo].[IMPORT_VEHICLES] (
    [Vehicle Make]  VARCHAR (50) NULL,
    [Vehicle Model] VARCHAR (50) NULL,
    [Registration]  VARCHAR (50) NULL,
    [Vehicle Type]  VARCHAR (50) NULL,
    [Depot]         VARCHAR (50) NULL
);

