﻿CREATE TABLE [dbo].[MaintenanceCheckDetailPhotos] (
    [MaintenanceCheckDetailPhotoPK] UNIQUEIDENTIFIER NOT NULL,
    [MaintenanceCheckDetailFK]      UNIQUEIDENTIFIER NOT NULL,
    [FileFK]                        UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                      BIT              NOT NULL,
    [CreatedByID]                   UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                   DATETIME2 (7)    NOT NULL,
    [UpdatedByID]                   UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                   DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_MaintenanceCheckDetailPhotos] PRIMARY KEY CLUSTERED ([MaintenanceCheckDetailPhotoPK] ASC),
    CONSTRAINT [FK_MaintenanceCheckDetailPhotos_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_MaintenanceCheckDetailPhotos_MaintenanceCheckDetails] FOREIGN KEY ([MaintenanceCheckDetailFK]) REFERENCES [dbo].[MaintenanceCheckDetails] ([MaintenanceCheckDetailPK])
);

