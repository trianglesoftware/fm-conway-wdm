﻿CREATE TABLE [dbo].[LoadingSheetEquipment] (
    [LoadingSheetEquipmentPK] UNIQUEIDENTIFIER NOT NULL,
    [LoadingSheetFK]          UNIQUEIDENTIFIER NOT NULL,
    [NonStandardEquipment]    NVARCHAR (1000)  CONSTRAINT [DF_LoadingSheetEquipment_NonStandardEquipment] DEFAULT ('') NOT NULL,
    [IsNonStandard]           BIT              CONSTRAINT [DF_LoadingSheetEquipment_IsNonStandard] DEFAULT ((0)) NOT NULL,
    [EquipmentFK]             UNIQUEIDENTIFIER NULL,
    [Quantity]                DECIMAL (18, 2)  CONSTRAINT [DF_LoadingSheetEquipment_Quantity] DEFAULT ((0)) NOT NULL,
    [SheetOrder]              INT              CONSTRAINT [DF_LoadingSheetEquipment_SheetOrder] DEFAULT ((0)) NOT NULL,
    [IsActive]                BIT              NOT NULL,
    [CreatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]             DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]             DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_LoadingSheetEquipment] PRIMARY KEY CLUSTERED ([LoadingSheetEquipmentPK] ASC),
    CONSTRAINT [FK_LoadingSheetEquipment_Equipment] FOREIGN KEY ([EquipmentFK]) REFERENCES [dbo].[Equipment] ([EquipmentPK]),
    CONSTRAINT [FK_LoadingSheetEquipment_LoadingSheets] FOREIGN KEY ([LoadingSheetFK]) REFERENCES [dbo].[LoadingSheets] ([LoadingSheetPK])
);




GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20171103-091059]
    ON [dbo].[LoadingSheetEquipment]([LoadingSheetFK] ASC);

