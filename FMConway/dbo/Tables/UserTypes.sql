﻿CREATE TABLE [dbo].[UserTypes] (
    [UserTypePK]         UNIQUEIDENTIFIER NOT NULL,
    [UserType]           NVARCHAR (100)   NOT NULL,
    [IsContractManager]  BIT              NOT NULL,
    [IsOperationManager] BIT              NOT NULL,
    CONSTRAINT [PK_UserTypes] PRIMARY KEY CLUSTERED ([UserTypePK] ASC)
);

