﻿CREATE TABLE [dbo].[Drawings] (
    [DrawingPK]    UNIQUEIDENTIFIER NOT NULL,
    [DrawingTitle] NVARCHAR (300)   NOT NULL,
    [Revision]     NVARCHAR (100)   NOT NULL,
    [FileFK]       UNIQUEIDENTIFIER NOT NULL,
    [IsActive]     BIT              NOT NULL,
    [CreatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]  DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Drawings] PRIMARY KEY CLUSTERED ([DrawingPK] ASC),
    CONSTRAINT [FK_Drawings_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK])
);

