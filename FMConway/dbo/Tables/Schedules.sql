﻿CREATE TABLE [dbo].[Schedules] (
    [SchedulePK]        UNIQUEIDENTIFIER NOT NULL,
    [WorkInstructionFK] UNIQUEIDENTIFIER NOT NULL,
    [StartDate]         DATETIME2 (7)    NOT NULL,
    [EndDate]           DATETIME2 (7)    NOT NULL,
    [IsComplete]        BIT              CONSTRAINT [DF_Schedules_IsComplete] DEFAULT ((0)) NOT NULL,
    [IsActive]          BIT              NOT NULL,
    [ShiftFK]           UNIQUEIDENTIFIER NULL,
    [CreatedByFK]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]       DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]       UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]       DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Schedules] PRIMARY KEY CLUSTERED ([SchedulePK] ASC),
    CONSTRAINT [FK_Schedules_Shifts] FOREIGN KEY ([ShiftFK]) REFERENCES [dbo].[Shifts] ([ShiftPK]),
    CONSTRAINT [FK_Schedules_WorkInstructions] FOREIGN KEY ([WorkInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);





