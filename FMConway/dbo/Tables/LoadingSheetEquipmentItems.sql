﻿CREATE TABLE [dbo].[LoadingSheetEquipmentItems] (
    [LoadingSheetEquipmentItemPK] UNIQUEIDENTIFIER NOT NULL,
    [LoadingSheetEquipmentFK]     UNIQUEIDENTIFIER NOT NULL,
    [ItemNumber]                  INT              NOT NULL,
    [ChecklistFK]                 UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                    BIT              NOT NULL,
    [CreatedByFK]                 UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                 DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                 UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                 DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_LoadingSheetEquipmentItems] PRIMARY KEY CLUSTERED ([LoadingSheetEquipmentItemPK] ASC),
    CONSTRAINT [FK_LoadingSheetEquipmentItems_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_LoadingSheetEquipmentItems_LoadingSheetEquipment] FOREIGN KEY ([LoadingSheetEquipmentFK]) REFERENCES [dbo].[LoadingSheetEquipment] ([LoadingSheetEquipmentPK])
);

