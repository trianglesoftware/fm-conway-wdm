﻿CREATE TABLE [dbo].[EquipmentItemChecklistAnswers] (
    [EquipmentItemChecklistAnswerPK] UNIQUEIDENTIFIER NOT NULL,
    [EquipmentItemChecklistFK]       UNIQUEIDENTIFIER NOT NULL,
    [ChecklistQuestionFK]            UNIQUEIDENTIFIER NOT NULL,
    [Answer]                         BIT              NOT NULL,
    [Reason]                         NVARCHAR (1000)  NULL,
    [IsActive]                       BIT              NOT NULL,
    [CreatedByFK]                    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                    DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                    UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                    DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_EquipmentItemChecklistAnswers] PRIMARY KEY CLUSTERED ([EquipmentItemChecklistAnswerPK] ASC),
    CONSTRAINT [FK_EquipmentItemChecklistAnswers_ChecklistQuestions] FOREIGN KEY ([ChecklistQuestionFK]) REFERENCES [dbo].[ChecklistQuestions] ([ChecklistQuestionPK]),
    CONSTRAINT [FK_EquipmentItemChecklistAnswers_EquipmentItemChecklists] FOREIGN KEY ([EquipmentItemChecklistFK]) REFERENCES [dbo].[EquipmentItemChecklists] ([EquipmentItemChecklistPK])
);

