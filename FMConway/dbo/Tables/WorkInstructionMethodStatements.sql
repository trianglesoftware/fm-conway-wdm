﻿CREATE TABLE [dbo].[WorkInstructionMethodStatements] (
    [WorkInstructionMethodStatementPK] UNIQUEIDENTIFIER NOT NULL,
    [WorkInstructionFK]                UNIQUEIDENTIFIER NOT NULL,
    [MethodStatementFK]                UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                         BIT              NOT NULL,
    [CreatedByFK]                      UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]                      DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]                      UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]                      DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_WorkInstructionMethodStatements] PRIMARY KEY CLUSTERED ([WorkInstructionMethodStatementPK] ASC),
    CONSTRAINT [FK_WorkInstructionMethodStatements_MethodStatements] FOREIGN KEY ([MethodStatementFK]) REFERENCES [dbo].[MethodStatements] ([MethodStatementPK]),
    CONSTRAINT [FK_WorkInstructionMethodStatements_WorkInstructions] FOREIGN KEY ([WorkInstructionFK]) REFERENCES [dbo].[WorkInstructions] ([WorkInstructionPK])
);

