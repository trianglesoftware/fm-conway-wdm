﻿CREATE TABLE [dbo].[MaintenanceCheckDetails] (
    [MaintenanceCheckDetailPK] UNIQUEIDENTIFIER NOT NULL,
    [MaintenanceCheckAnswerFK] UNIQUEIDENTIFIER NOT NULL,
    [Description]              NVARCHAR (MAX)   NOT NULL,
    [IsActive]                 BIT              NOT NULL,
    [CreatedByID]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]              DATETIME2 (7)    NOT NULL,
    [UpdatedByID]              UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]              DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_MaintenanceCheckDetails] PRIMARY KEY CLUSTERED ([MaintenanceCheckDetailPK] ASC),
    CONSTRAINT [FK_MaintenanceCheckDetails_MaintenanceCheckAnswers] FOREIGN KEY ([MaintenanceCheckAnswerFK]) REFERENCES [dbo].[MaintenanceCheckAnswers] ([MaintenanceCheckAnswerPK])
);

