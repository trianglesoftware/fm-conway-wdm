﻿CREATE TABLE [dbo].[Checklists] (
    [ChecklistPK]            UNIQUEIDENTIFIER NOT NULL,
    [ChecklistName]          NVARCHAR (300)   NOT NULL,
    [IsContractChecklist]    BIT              NOT NULL,
    [IsJobTypeChecklist]     BIT              CONSTRAINT [DF_Checklists_IsJobTypeChecklist] DEFAULT ((0)) NOT NULL,
    [IsVehicleTypeChecklist] BIT              CONSTRAINT [DF_Checklists_IsVehicleTypeChecklist] DEFAULT ((0)) NOT NULL,
    [IsMaintenanceChecklist] BIT              CONSTRAINT [DF_Checklists_IsMaintenanceChecklist] DEFAULT ((0)) NOT NULL,
    [IsSignatureChecklist]   BIT              CONSTRAINT [DF_Checklists_IsSignatureChecklist] DEFAULT ((0)) NOT NULL,
    [IsHighSpeedChecklist]   BIT              CONSTRAINT [DF_Checklists_IsHighSpeedChecklist] DEFAULT ((0)) NOT NULL,
    [IsLowSpeedChecklist]    BIT              CONSTRAINT [DF_Checklists_IsLowSpeedChecklist] DEFAULT ((0)) NOT NULL,
    [IsPreHireChecklist]     BIT              CONSTRAINT [DF_Checklists_IsPreHireChecklist] DEFAULT ((0)) NOT NULL,
    [IsVmsChecklist]         BIT              CONSTRAINT [DF_Checklists_IsVmsChecklist] DEFAULT ((0)) NOT NULL,
    [Is12cChecklist]         BIT              CONSTRAINT [DF_Checklists_Is12cChecklist] DEFAULT ((0)) NOT NULL,
    [CompleteAtDepot]        BIT              CONSTRAINT [DF_Checklists_CompleteAtDepot] DEFAULT ((0)) NOT NULL,
    [StartOfWork]            BIT              CONSTRAINT [DF_Checklists_StartOfWork] DEFAULT ((0)) NOT NULL,
    [IsActive]               BIT              NOT NULL,
    [CreatedByFK]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]            DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]            UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]            DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Checklists] PRIMARY KEY CLUSTERED ([ChecklistPK] ASC),
    CONSTRAINT [FK_Checklists_Checklists] FOREIGN KEY ([ChecklistPK]) REFERENCES [dbo].[Checklists] ([ChecklistPK])
);







