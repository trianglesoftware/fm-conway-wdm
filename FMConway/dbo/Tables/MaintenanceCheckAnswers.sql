﻿CREATE TABLE [dbo].[MaintenanceCheckAnswers] (
    [MaintenanceCheckAnswerPK] UNIQUEIDENTIFIER NOT NULL,
    [MaintenanceCheckFK]       UNIQUEIDENTIFIER NOT NULL,
    [ChecklistQuestionFK]      UNIQUEIDENTIFIER NOT NULL,
    [Answer]                   BIT              NOT NULL,
    [IsActive]                 BIT              NOT NULL,
    [CreatedByID]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]              DATETIME2 (7)    NOT NULL,
    [UpdatedByID]              UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]              DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_MaintenanceCheckAnswers] PRIMARY KEY CLUSTERED ([MaintenanceCheckAnswerPK] ASC),
    CONSTRAINT [FK_MaintenanceCheckAnswers_ChecklistQuestions] FOREIGN KEY ([ChecklistQuestionFK]) REFERENCES [dbo].[ChecklistQuestions] ([ChecklistQuestionPK]),
    CONSTRAINT [FK_MaintenanceCheckAnswers_MaintenanceChecks] FOREIGN KEY ([MaintenanceCheckFK]) REFERENCES [dbo].[MaintenanceChecks] ([MaintenanceCheckPK])
);

