﻿CREATE TABLE [dbo].[OutOfHoursChecklists] (
    [OOHChecklistPK] UNIQUEIDENTIFIER NOT NULL,
    [OutOfHoursFK]   UNIQUEIDENTIFIER NOT NULL,
    [ChecklistFK]    UNIQUEIDENTIFIER NOT NULL,
    [IsActive]       BIT              NOT NULL,
    [CreatedByID]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]    DATETIME2 (7)    NOT NULL,
    [UpdatedByID]    UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]    DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_OutOfHoursChecklists] PRIMARY KEY CLUSTERED ([OOHChecklistPK] ASC),
    CONSTRAINT [FK_OutOfHoursChecklists_Checklists] FOREIGN KEY ([ChecklistFK]) REFERENCES [dbo].[Checklists] ([ChecklistPK]),
    CONSTRAINT [FK_OutOfHoursChecklists_OutOfHours] FOREIGN KEY ([OutOfHoursFK]) REFERENCES [dbo].[OutOfHours] ([OutOfHoursPK])
);

