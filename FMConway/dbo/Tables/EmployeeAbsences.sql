﻿CREATE TABLE [dbo].[EmployeeAbsences] (
    [EmployeeAbsencePK]       UNIQUEIDENTIFIER NOT NULL,
    [EmployeeFK]              UNIQUEIDENTIFIER NOT NULL,
    [EmployeeAbsenceReasonFK] UNIQUEIDENTIFIER NOT NULL,
    [FromDate]                DATETIME2 (7)    NOT NULL,
    [ToDate]                  DATETIME2 (7)    NOT NULL,
    [IsActive]                BIT              NOT NULL,
    [CreatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]             DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]             UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]             DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_EmployeeAbsences] PRIMARY KEY CLUSTERED ([EmployeeAbsencePK] ASC),
    CONSTRAINT [FK_EmployeeAbsences_EmployeeAbsenceReasons] FOREIGN KEY ([EmployeeAbsenceReasonFK]) REFERENCES [dbo].[EmployeeAbsenceReasons] ([EmployeeAbsenceReasonPK]),
    CONSTRAINT [FK_EmployeeAbsences_Employees] FOREIGN KEY ([EmployeeFK]) REFERENCES [dbo].[Employees] ([EmployeePK])
);

