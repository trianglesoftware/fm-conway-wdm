﻿CREATE TABLE [dbo].[TaskPhotos] (
    [TaskPhotoPK] UNIQUEIDENTIFIER NOT NULL,
    [TaskFK]      UNIQUEIDENTIFIER NOT NULL,
    [FileFK]      UNIQUEIDENTIFIER NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedByID] UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate] DATETIME2 (7)    NOT NULL,
    [UpdatedByID] UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_TaskPhotos] PRIMARY KEY CLUSTERED ([TaskPhotoPK] ASC),
    CONSTRAINT [FK_TaskPhotos_Files] FOREIGN KEY ([FileFK]) REFERENCES [dbo].[Files] ([FilePK]),
    CONSTRAINT [FK_TaskPhotos_Tasks] FOREIGN KEY ([TaskFK]) REFERENCES [dbo].[Tasks] ([TaskPK])
);

