﻿CREATE TABLE [dbo].[JobJobTypes] (
    [JobJobTypePK] UNIQUEIDENTIFIER NOT NULL,
    [JobFK]        UNIQUEIDENTIFIER NOT NULL,
    [JobTypeFK]    UNIQUEIDENTIFIER NOT NULL,
    [IsActive]     BIT              NOT NULL,
    [CreatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]  DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobJobTypes] PRIMARY KEY CLUSTERED ([JobJobTypePK] ASC),
    CONSTRAINT [FK_JobJobTypes_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK]),
    CONSTRAINT [FK_JobJobTypes_JobTypes] FOREIGN KEY ([JobTypeFK]) REFERENCES [dbo].[JobTypes] ([JobTypePK])
);

