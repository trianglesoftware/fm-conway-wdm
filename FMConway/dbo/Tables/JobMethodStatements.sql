﻿CREATE TABLE [dbo].[JobMethodStatements] (
    [JobMethodStatementPK] UNIQUEIDENTIFIER NOT NULL,
    [JobFK]                UNIQUEIDENTIFIER NOT NULL,
    [MethodStatementFK]    UNIQUEIDENTIFIER NOT NULL,
    [IsActive]             BIT              NOT NULL,
    [CreatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]          DATETIME2 (7)    NOT NULL,
    [UpdatedByFK]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]          DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JobMethodStatements] PRIMARY KEY CLUSTERED ([JobMethodStatementPK] ASC),
    CONSTRAINT [FK_JobMethodStatements_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK]),
    CONSTRAINT [FK_JobMethodStatements_MethodStatements] FOREIGN KEY ([MethodStatementFK]) REFERENCES [dbo].[MethodStatements] ([MethodStatementPK])
);

