﻿CREATE TABLE [dbo].[OutOfHours] (
    [OutOfHoursPK] UNIQUEIDENTIFIER NOT NULL,
    [Customer]     NVARCHAR (MAX)   NOT NULL,
    [TMRequested]  NVARCHAR (MAX)   NOT NULL,
    [Location]     NVARCHAR (MAX)   NOT NULL,
    [ArriveOnSite] DATETIME2 (7)    NOT NULL,
    [Equipment]    NVARCHAR (MAX)   NOT NULL,
    [JobInstalled] DATETIME2 (7)    NOT NULL,
    [LeaveSite]    DATETIME2 (7)    NOT NULL,
    [JobFK]        UNIQUEIDENTIFIER NULL,
    [IsActive]     BIT              NOT NULL,
    [CreatedDate]  DATETIME2 (7)    NOT NULL,
    [CreatedByID]  UNIQUEIDENTIFIER NOT NULL,
    [UpdatedDate]  DATETIME2 (7)    NOT NULL,
    [UpdatedByID]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_OutOfHours] PRIMARY KEY CLUSTERED ([OutOfHoursPK] ASC),
    CONSTRAINT [FK_OutOfHours_Jobs] FOREIGN KEY ([JobFK]) REFERENCES [dbo].[Jobs] ([JobPK])
);

