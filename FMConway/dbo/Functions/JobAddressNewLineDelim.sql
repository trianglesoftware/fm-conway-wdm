﻿

CREATE function [dbo].[JobAddressNewLineDelim] (@JobPK uniqueidentifier)
returns varchar(500)
as
begin

declare @Address varchar(500)
	
select
@Address = Coalesce(reverse(stuff(reverse(
	COALESCE(NULLIF(j.AddressLine1, '') + CHAR(13) + CHAR(10), '') +
	COALESCE(NULLIF(j.AddressLine2, '') + CHAR(13) + CHAR(10), '') +
	COALESCE(NULLIF(j.AddressLine3, '') + CHAR(13) + CHAR(10), '') +
	COALESCE(NULLIF(j.City, '') + CHAR(13) + CHAR(10), '') +
	COALESCE(NULLIF(j.County, '') + CHAR(13) + CHAR(10), '') +
	COALESCE(NULLIF(j.PostCode, '') + CHAR(13) + CHAR(10), '')
	), 1, 2, '')),'')
from
dbo.Jobs as j
where
j.JobPK = @JobPK

return @Address

end
GO



