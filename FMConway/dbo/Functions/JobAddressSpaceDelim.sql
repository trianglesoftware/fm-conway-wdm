﻿

CREATE function [dbo].[JobAddressSpaceDelim] (@JobPK uniqueidentifier)
returns varchar(500)
as
begin

declare @Address varchar(500)
	
select
@Address = Coalesce(reverse(stuff(reverse(
	COALESCE(NULLIF(j.AddressLine1, '') + ' ', '') +
	COALESCE(NULLIF(j.AddressLine2, '') + ' ', '') +
	COALESCE(NULLIF(j.AddressLine3, '') + ' ', '') +
	COALESCE(NULLIF(j.City, '') + ' ', '') +
	COALESCE(NULLIF(j.County, '') + ' ', '') +
	COALESCE(NULLIF(j.PostCode, '') + ' ', '')
	), 1, 1, '')),'')
from
dbo.Jobs as j
where
j.JobPK = @JobPK

return @Address

end
GO