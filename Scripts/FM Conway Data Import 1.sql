
--------------- SET UP CONSTANTS ---------------
DECLARE @AdminGuid UNIQUEIDENTIFIER
DECLARE @ContractsManager VARCHAR(20)
DECLARE @FakeDepotArea UNIQUEIDENTIFIER
DECLARE @ExpiryDate DATETIME2(7)
DECLARE @DefaultEmployeeType UNIQUEIDENTIFIER
DECLARE @DefaultDepot UNIQUEIDENTIFIER

SET @AdminGuid = '005AEBC4-A174-4541-8951-60CF83A8DA80'
SET @ContractsManager = 'Contracts Manager'
SET @FakeDepotArea = 'B08B7DB1-E5A3-4716-91DC-89432C9F8FB0'
SET @ExpiryDate = DATEADD(YEAR, 1, GETDATE())
SET @DefaultEmployeeType = 'B6C5A45C-9FC4-439D-9DFE-DE397564B101'
SET @DefaultDepot = '45732491-CBAD-4AE1-8C2B-7922EA710E7D';

--------------- EMPLOYEE TYPES ---------------
WITH cte_types AS
(
	SELECT DISTINCT [Employee Type]
	FROM IMPORT_EMPLOYEES
	WHERE [Employee Type] NOT IN (SELECT EmployeeType FROM EmployeeTypes) AND LEN([Employee Type])>0
)

INSERT INTO EmployeeTypes(EmployeeTypePK, EmployeeType, IsContractManager, IsActive, CreatedByFK, CreatedDate, UpdatedByFK, UpdatedDate)
SELECT NEWID(), [Employee Type], CASE WHEN [Employee Type]='Contracts Manager' THEN 1 ELSE 0 END, 1, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM cte_types;	

--------------- DEPOTS ---------------
WITH cte_depots AS
(
	SELECT DISTINCT Depot 
	FROM IMPORT_EMPLOYEES 
	WHERE Depot NOT IN (SELECT DepotName FROM Depots) AND LEN(Depot)>0
)

INSERT INTO [dbo].[Depots] ([DepotPK],[DepotAreaFK],[DepotName],[AddressLine1],[AddressLine2],[AddressLine3],[City],[County],[Postcode],
				 [IsActive],[IsHidden],[CreatedByFK],[CreatedDate],[UpdatedByFK],[UpdatedDate])
SELECT NEWID(), @FakeDepotArea, Depot, '', '', '', '', '', '', 1, 0, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM cte_depots

--------------- EMPLOYEES ---------------

INSERT INTO [dbo].[Employees]
           ([EmployeePK]
           ,[EmployeeTypeFK]
           ,[EmployeeName]
           ,[PhoneNumber]
           ,[EmailAddress]
           ,[AgencyStaff]
           ,[AgencyName]
           ,[IsCurrentlyEmployeed]
           ,[DepotFK]
           ,[VehicleFK]
           ,[IsActive]
           ,[IsHidden]
           ,[CreatedByFK]
           ,[CreatedDate]
           ,[UpdatedByFK]
           ,[UpdatedDate])
SELECT NEWID() AS EmployeePK, 
		ISNULL(et.EmployeeTypePK,@DefaultEmployeeType) AS EmployeeTypeFK,
		[Employee Name] AS EmployeeName,
		'' AS PhoneNumber,	
		'' AS EmailAddress,	
		CAST(CASE WHEN [Agency Staff]='YES' THEN 1 ELSE 0 END AS BIT) AS AgencyStaff,
		'' AS AgencyName,
		CAST(CASE WHEN [Is Currently Employed]='YES' THEN 1 ELSE 0 END AS BIT) AS IsCurrentlyEmployed,
		ISNULL(d.DepotPK, @DefaultDepot),
		NULL as VehicleFK,
		1, 0, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM IMPORT_EMPLOYEES ie
LEFT JOIN EmployeeTypes et ON ie.[Employee Type] = et.EmployeeType
LEFT JOIN Depots d ON ie.[Depot] = d.DepotName

--------------- CERTIFICATE TYPES ---------------

INSERT INTO [dbo].[CertificateTypes] ([CertificateTypePK], [Name], [IsActive], [CreatedByFK], [CreatedDate], [UpdatedByFK], [UpdatedDate])
SELECT NEWID() AS CertificateTypePK, COLUMN_NAME AS Name, 1, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'IMPORT_EMPLOYEES'
AND COLUMN_NAME NOT IN ('Employee Name','Employee Type','Is Currently Employed','Agency Staff','Depot')

--------------- EMPLOYEE CERTIFICATES ---------------
DECLARE @sql VARCHAR(1000)

DECLARE db_cursor CURSOR FOR 
SELECT COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'IMPORT_EMPLOYEES'
AND COLUMN_NAME NOT IN ('Employee Name','Employee Type','Is Currently Employed','Agency Staff','Depot')

DECLARE @name VARCHAR(1000)
OPEN db_cursor  
FETCH NEXT FROM db_cursor INTO @name  

WHILE @@FETCH_STATUS = 0  
BEGIN  
	SET @sql = 'INSERT INTO [dbo].[EmployeeCertificates]
				   ([EmployeeCertificatePK], [Name], [Description], [EmployeeFK], [CertificateTypeFK],[ExpiryDate]
				   ,[IsFirstAid], [IsActive], [CreatedByFK], [CreatedDate], [UpdatedByFK], [UpdatedDate])
		SELECT	NEWID(), '''' AS Name,'''' AS Description,
				e.EmployeePK AS EmployeeFK,
				ct.CertificateTypePK,
				'''+CONVERT(VARCHAR,@ExpiryDate,121)+''', 0, 1,
				'''+CAST(@AdminGuid AS NVARCHAR(36))+''', 
				'''+CONVERT(VARCHAR,GETDATE(),121)+''', 
				'''+CAST(@AdminGuid AS NVARCHAR(36))+''',
				'''+CONVERT(VARCHAR,GETDATE(),121)+'''
		FROM IMPORT_EMPLOYEES ie
		JOIN Employees e ON e.EmployeeName = ie.[Employee Name]
		JOIN CertificateTypes ct ON ct.Name = '''+@name+'''
		WHERE ['+@name+'] = ''Yes'''
	  EXEC(@sql)
	  FETCH NEXT FROM db_cursor INTO @name  
END 
CLOSE db_cursor  
DEALLOCATE db_cursor 

