
use ClancyPlant

--run 'DataClean - keep some test data' then this

delete
from
Employees
where EmployeePK not in (
'09E51FE2-CBDC-4C0B-8F88-6BC4DBF63CE2',
'51910D85-63F4-4741-9103-AA9E529BC33E'
)

delete
from dbo.Contacts

delete
from dbo.Customers

delete
from dbo.Contracts

delete
from dbo.Jobs

delete
from dbo.ToolboxTalks

delete 
from dbo.Vehicles

delete 
from dbo.Timesheets
