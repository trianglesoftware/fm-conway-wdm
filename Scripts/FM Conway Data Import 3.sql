--------------- SET UP CONSTANTS ---------------
DECLARE @AdminGuid UNIQUEIDENTIFIER
DECLARE @FakeDepotArea UNIQUEIDENTIFIER
DECLARE @DefaultEmployeeType UNIQUEIDENTIFIER
DECLARE @DateContractExpires DATETIME
DECLARE @DefaultContractManager UNIQUEIDENTIFIER
DECLARE @DefaultBillingRequirement UNIQUEIDENTIFIER

SET @AdminGuid = '005AEBC4-A174-4541-8951-60CF83A8DA80'
SET @FakeDepotArea = 'B08B7DB1-E5A3-4716-91DC-89432C9F8FB0'
SET @DefaultEmployeeType = 'B6C5A45C-9FC4-439D-9DFE-DE397564B101'
SET @DateContractExpires = DATEADD(YEAR, 1, GETDATE());

--------------- CUSTOMERS ---------------
WITH cte_cust AS
(
	SELECT DISTINCT Customer FROM IMPORT_CONTRACTS
	WHERE Customer NOT IN (SELECT CustomerName FROM Customers)
)

INSERT INTO [dbo].[Customers]
           ([CustomerPK], [CustomerName], [AccountNumber], [CustomerCode], 
		   [AddressLine1], [AddressLine2], [AddressLine3], [City], [County], [Postcode],
		   [Comments], [PhoneNumber], [FaxNumber],[EmailAddress],
		   [IsActive], [IsHidden], [CreatedByFK], [CreatedDate], [UpdatedByFK], [UpdatedDate])
SELECT	NEWID(), Customer, '' AS AccountNumber, '' AS CustomerCode, 
		'' AS AddressLine1, '' AS AddressLine2, '' AS AddressLine3, '' AS City, '' AS County, '' AS Postcode,
		'' AS Comments, '' AS PhoneNumber, '' AS FaxNumber, '' AS EmailAddress,
		1, 0, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM cte_cust;

------------ CONTRACT TYPES ------------

WITH cte_ctypes AS
(
	SELECT DISTINCT [Contract Type] AS ContractType FROM IMPORT_CONTRACTS
	WHERE [Contract Type] NOT IN (SELECT ContractTypeDescription FROM ContractTypes)
)

INSERT INTO [dbo].[ContractTypes]  ([ContractTypePK], [ContractTypeDescription], [IsActive])
SELECT NEWID(), ContractType, 1
FROM cte_ctypes;	

--------------- DEPOTS ---------------
WITH cte_cdepots AS
(
	SELECT DISTINCT Depot 
	FROM IMPORT_CONTRACTS
	WHERE Depot NOT IN (SELECT DepotName FROM Depots) 
)

INSERT INTO [dbo].[Depots] ([DepotPK],[DepotAreaFK],[DepotName],[AddressLine1],[AddressLine2],[AddressLine3],[City],[County],[Postcode],
				 [IsActive],[IsHidden],[CreatedByFK],[CreatedDate],[UpdatedByFK],[UpdatedDate])
SELECT NEWID(), @FakeDepotArea, Depot, '', '', '', '', '', '', 1, 0, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM cte_cdepots;	

WITH cte_cemployees AS
(
	SELECT DISTINCT [Contract Manager] AS ContractManager, Depot FROM IMPORT_CONTRACTS
	WHERE [Contract Manager] NOT IN (SELECT EmployeeName FROM Employees)
	AND LEN([Contract Manager])>0
)

INSERT INTO [dbo].[Employees]
           ([EmployeePK], [EmployeeTypeFK], [EmployeeName], [PhoneNumber], [EmailAddress], [AgencyStaff], [AgencyName], [IsCurrentlyEmployeed], [DepotFK]
           ,[VehicleFK], [IsActive], [IsHidden], [CreatedByFK], [CreatedDate], [UpdatedByFK], [UpdatedDate])
SELECT NEWID() AS EmployeePK, 
		@DefaultEmployeeType AS EmployeeTypeFK,
		ContractManager AS EmployeeName,
		'' AS PhoneNumber,	
		'' AS EmailAddress,	
		0 AS AgencyStaff,
		'' AS AgencyName,
		1 AS IsCurrentlyEmployed,
		d.DepotPK,	
		NULL as VehicleFK,
		1, 0, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM cte_cemployees
LEFT JOIN Depots d ON cte_cemployees.[Depot] = d.DepotName;	

--------------- BILLING REQUIREMENTS ---------------
WITH cte_breq AS
(
	SELECT DISTINCT [Billing Requirements] AS BillingRequirements
	FROM IMPORT_CONTRACTS
	WHERE [Billing Requirements]  NOT IN (SELECT BillingRequirement FROM BillingRequirements)
	AND LEN([Billing Requirements] )>0
)

INSERT INTO [dbo].[BillingRequirements] ([BillingRequirementPK], [BillingRequirement], [IsActive], [CreatedByFK], [CreatedDate], [UpdatedByFK], [UpdatedDate])
SELECT NEWID(), BillingRequirements, 1, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM cte_breq

--------------- CONTRACTS ---------------
SET @DefaultContractManager = (SELECT EmployeePK FROM Employees WHERE EmployeeName = 'Ian O''Donnell')
SET @DefaultBillingRequirement = (SELECT BillingRequirementPK FROM BillingRequirements WHERE BillingRequirement = 'Weekly Data Loader')

INSERT INTO [dbo].[Contracts]
           ([ContractPK], [ContractNumber], [ContractTitle], [CustomerFK], [DepotFK], [DateAwarded]
           ,[DateExpires]           ,[StatusFK]           ,[TenderNumber]           ,[EmployeeFK]           ,[BillingRequirementFK]
           ,[DateToBeSubmitted]           ,[QuantitySurveyor]           ,[IsHidden]           ,[IsActive]           ,[CreatedByFK]
           ,[CreatedDate]           ,[UpdatedByFK]           ,[UpdatedDate]           ,[ContractTypeFK])
SELECT NEWID() AS ContractPK, ic.[Sub Contract Code] AS ContractNumber, [Contract Title], c.CustomerPK AS CustomerFK, d.DepotPK AS DepotFK, GETDATE() AS DateAwarded,
	@DateContractExpires, s.StatusPK AS StatusFK, '' AS TenderNumber, ISNULL(e.EmployeePK, @DefaultContractManager) AS EmployeeFK, ISNULL(br.BillingRequirementPK, @DefaultBillingRequirement) AS BillingRequirementFK,
	ic.[Date To Submit] AS DateToBeSubmitted, ic.[Quantity Surveyor], 0, 1, @AdminGuid, GETDATE(), @AdminGuid, GETDATE(), 
	ct.ContractTypePK AS ContractTypeFK
FROM IMPORT_CONTRACTS ic
LEFT JOIN Customers c ON ic.Customer = c.CustomerName
LEFT JOIN Depots d ON ic.Depot = d.DepotName
LEFT JOIN Status s ON ic.[Contract Status] = s.Status
LEFT JOIN Employees e ON ic.[Contract Manager] = e.EmployeeName
LEFT JOIN BillingRequirements br ON br.BillingRequirement = ic.[Billing Requirements]
LEFT JOIN ContractTypes ct ON ic.[Contract Type] = ct.ContractTypeDescription

