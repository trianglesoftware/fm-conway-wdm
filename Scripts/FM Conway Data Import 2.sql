---------- SET UP CONSTANTS ----------

DECLARE @AdminGuid UNIQUEIDENTIFIER
DECLARE @FakeDepotArea UNIQUEIDENTIFIER

SET @AdminGuid = '005AEBC4-A174-4541-8951-60CF83A8DA80'
SET @FakeDepotArea = 'B08B7DB1-E5A3-4716-91DC-89432C9F8FB0';

---------- VEHICLE TYPES ----------
WITH cte_vtypes AS
(
	SELECT DISTINCT [Vehicle Type] AS VehicleType
	FROM IMPORT_VEHICLES
	WHERE [Vehicle Type] NOT IN (SELECT [Name] FROM VehicleTypes)
	AND LEN([Vehicle Type])>0
)

INSERT INTO [dbo].[VehicleTypes] ([VehicleTypePK],[Name] ,[IsActive] ,[CreatedByFK],[CreatedDate] ,[UpdatedByFK] ,[UpdatedDate])
SELECT NEWID(), VehicleType, 1, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM cte_vtypes;

---------- VEHICLE DEPOTS ----------

WITH cte_vdepots AS
(
	SELECT DISTINCT Depot 
	FROM IMPORT_VEHICLES
	WHERE [Depot] NOT IN (SELECT DepotName FROM Depots)
	AND LEN(Depot)>0
)

INSERT INTO [dbo].[Depots] ([DepotPK],[DepotAreaFK],[DepotName],[AddressLine1],[AddressLine2],[AddressLine3],[City],[County],[Postcode],
				 [IsActive],[IsHidden],[CreatedByFK],[CreatedDate],[UpdatedByFK],[UpdatedDate])
SELECT NEWID(), @FakeDepotArea, Depot, '', '', '', '', '', '', 1, 0, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM cte_vdepots

INSERT INTO [dbo].[Vehicles] ([VehiclePK], [Make], [Model], [Registration], [VehicleTypeFK] ,[DepotFK], [IsActive]           
				,[CreatedByFK], [CreatedDate], [UpdatedByFK], [UpdatedDate])
SELECT NEWID(), iv.[Vehicle Make], iv.[Vehicle Model], iv.Registration, vt.VehicleTypePK, d.DepotPK, 
		1, @AdminGuid, GETDATE(), @AdminGuid, GETDATE()
FROM IMPORT_VEHICLES iv
LEFT JOIN VehicleTypes vt ON vt.Name = iv.[Vehicle Type]
LEFT JOIN Depots d ON iv.[Depot] = d.DepotName