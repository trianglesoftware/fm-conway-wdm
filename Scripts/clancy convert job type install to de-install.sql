
use ClancyPlant

-- convert tasks for job type "install" to "de-install"

-- check if any de-install work instructions has no 'Job De-installed' task but does have 'Job Installed' task
--select
--wi.*
--from
--dbo.WorkInstructions as wi
--inner join dbo.JobTypes as jt on jt.JobTypePK = wi.JobTypeFK
--left join (select t.WorksInstructionFK, t.TaskPK from dbo.Tasks as t where t.Name = 'Job De-installed') as t on t.WorksInstructionFK = wi.WorkInstructionPK
--inner join (select t.WorksInstructionFK, t.TaskPK from dbo.Tasks as t where t.Name = 'Job Installed') as t1 on t1.WorksInstructionFK = wi.WorkInstructionPK
--where
--jt.JobType = 'De-install' and
--t.WorksInstructionFK is null
--order by
--wi.WorkInstructionNumber
--

declare @WorkInstructionPK uniqueidentifier = '92212C4C-38B4-4EC4-8D54-98FA552C38AE'

begin transaction

-- print tasks before update
select
*
from
dbo.Tasks
where
WorksInstructionFK = @WorkInstructionPK
order by
TaskOrder

--select *
delete t
from
dbo.Tasks as t
where
t.WorksInstructionFK = @WorkInstructionPK and
t.Name = 'Job Start'

--select *
update
t
set
t.Name = 'Job De-installed',
t.TaskOrder = 7
from
dbo.Tasks as t
where
t.WorksInstructionFK = @WorkInstructionPK and
t.Name = 'Job Installed'

--select *
update
t
set
t.TaskOrder = 8
from
dbo.Tasks as t
where
t.WorksInstructionFK = @WorkInstructionPK and
t.Name = 'Signatures Required'

--select *
update
t
set
t.Name = 'Equipment Collection',
t.TaskTypeFK = 9,
t.TaskOrder = 9
from
dbo.Tasks as t
where
t.WorksInstructionFK = @WorkInstructionPK and
t.Name = 'Equipment Installation'

--select *
update
t
set
t.TaskOrder = 10
from
dbo.Tasks as t
where
t.WorksInstructionFK = @WorkInstructionPK and
t.Name = 'Leave Site'

-- print tasks after update
select
*
from
dbo.Tasks
where
WorksInstructionFK = @WorkInstructionPK
order by
TaskOrder

rollback transaction
--commit transaction

