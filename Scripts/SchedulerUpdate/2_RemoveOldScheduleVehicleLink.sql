/*
   16 April 201810:08:10
   User: 
   Server: ATLAS\INST2014
   Database: ClancyPlant
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Schedules
	DROP CONSTRAINT FK_Schedules_Vehicles
GO
ALTER TABLE dbo.Vehicles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Schedules
	DROP COLUMN VehicleFK
GO
ALTER TABLE dbo.Schedules SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
