INSERT INTO
	[ClancyPlant].[dbo].[ScheduleVehicles]
SELECT 
	  NEWID() as [ScheduleVehiclePK]
	  ,[SchedulePK] as [ScheduleFK]
      ,[VehicleFK]
      ,[IsActive]
      ,[CreatedByFK]
      ,[CreatedDate]
      ,[UpdatedByFK]
      ,[UpdatedDate]
FROM 
	[ClancyPlant].[dbo].[Schedules] as s
WHERE 
	s.VehicleFK IS NOT NULL