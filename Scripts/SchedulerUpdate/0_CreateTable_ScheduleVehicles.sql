/*
   16 April 201810:02:12
   User: 
   Server: ATLAS\INST2014
   Database: ClancyPlant
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Vehicles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Schedules SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.ScheduleVehicles
	(
	ScheduleVehiclePK uniqueidentifier NOT NULL,
	ScheduleFK uniqueidentifier NOT NULL,
	VehicleFK uniqueidentifier NOT NULL,
	IsActive bit NOT NULL,
	CreatedByID uniqueidentifier NOT NULL,
	CreatedDate datetime2(7) NOT NULL,
	UpdatedByID uniqueidentifier NOT NULL,
	UpdatedDate datetime2(7) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.ScheduleVehicles ADD CONSTRAINT
	PK_ScheduleVehicles PRIMARY KEY CLUSTERED 
	(
	ScheduleVehiclePK
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ScheduleVehicles ADD CONSTRAINT
	FK_ScheduleVehicles_Schedules FOREIGN KEY
	(
	ScheduleFK
	) REFERENCES dbo.Schedules
	(
	SchedulePK
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ScheduleVehicles ADD CONSTRAINT
	FK_ScheduleVehicles_Vehicles FOREIGN KEY
	(
	VehicleFK
	) REFERENCES dbo.Vehicles
	(
	VehiclePK
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ScheduleVehicles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
