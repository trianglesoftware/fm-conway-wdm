using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using DotNetOpenAuth.AspNet;

using Triangle.Membership.Environments;
using Triangle.Membership.Interface;
using Microsoft.Web.WebPages.OAuth;
using Triangle.Membership.Users;
using Triangle.Membership.Models;

namespace Triangle.Membership.Membership
{
    public class MembershipProvider : IMembershipProvider, IOAuthProvider, IOpenAuthDataProvider
    {
        private const int _tokenSizeInBytes = 16;

        private static readonly Dictionary<string, AuthenticationClientData> _authenticationClients = new Dictionary<string, AuthenticationClientData>(StringComparer.OrdinalIgnoreCase);

        private readonly IApplicationEnvironment _applicationEnvironment;
        private readonly IUserService _userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CocentricMembershipProvider" /> class.
        /// </summary>
        /// <param name="userService">The user store.</param>
        /// <param name="applicationEnvironment">The application environment.</param>
        public MembershipProvider(IUserService userService, IApplicationEnvironment applicationEnvironment)
        {
            this._userService = userService;
            this._applicationEnvironment = applicationEnvironment;
        }

        #region IMembershipProvider Members

        /// <summary>
        /// Determines whether the provided <paramref name="username"/> and
        /// <paramref name="password"/> combination is valid
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="rememberMe">
        /// if set to <c>true</c> [remember me].
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        public bool Login(string username, string password, bool rememberMe = false)
        {

            if (_userService.IsValidLogin(username, password))
            {
                _applicationEnvironment.IssueAuthTicket(username, rememberMe);
                return true;
            }

            return false;
        }

        /// <summary>
        ///   Logout the current user
        /// </summary>
        public void Logout()
        {
            _applicationEnvironment.RevokeAuthTicket();
        }

        /// <summary>
        ///   Creates an account.
        /// </summary>
        /// <param name="user"> The user. </param>
        public IMembershipUser CreateAccount(IRegistration user)
        {
            return _userService.Add(user);
        }

        /// <summary>
        ///   Updates the account.
        /// </summary>
        /// <param name="user"> The user. </param>
        public void UpdateAccount(IMembershipUser user)
        {
            _userService.Save(user);
        }
        
        /// <summary>
        ///   Updates the account.
        /// </summary>
        /// <param name="user"> The user. </param>
        public IMembershipUser GetAccount(string userName)
        {
            return _userService.GetUserByUsername(userName);
        }

        /// <summary>
        ///   Determines whether the specific <paramref name="username" /> has a
        ///   local account
        /// </summary>
        /// <param name="username"> The username. </param>
        /// <returns> <c>true</c> if the specified username has a local account; otherwise, <c>false</c> . </returns>
        public bool HasLocalAccount(string userName)
        {
            return _userService.HasLocalAuthAccount(userName);
        }

        /// <summary>
        ///   Changes the password for a user
        /// </summary>
        /// <param name="username"> The username. </param>
        /// <param name="oldPassword"> The old password. </param>
        /// <param name="newPassword"> The new password. </param>
        /// <returns> </returns>
        public bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            return _userService.ChangePassword(username, oldPassword, newPassword);
        }

        /// <summary>
        ///   Sets the local password for a user
        /// </summary>
        /// <param name="username"> The username. </param>
        /// <param name="newPassword"> The new password. </param>
        public void SetLocalPassword(string username, string newPassword)
        {
            
            if (!_userService.HasLocalAuthAccount(username))
            {
                throw new MembershipException("SetLocalPassword can only be used on accounts that currently don't have a local password.");
            }

            _userService.SetLocalAuthAccountPassword(username, newPassword);
        }

        /// <summary>
        ///   Generates the password reset token for a user
        /// </summary>
        /// <param name="username"> The username. </param>
        /// <param name="tokenExpirationInMinutesFromNow"> The token expiration in minutes from now. </param>
        /// <returns> </returns>
        public PasswordResetToken GeneratePasswordResetToken(string username)
        {
            return _userService.GetNewPasswordResetToken(username);
        }

        /// <summary>
        ///   Resets the password for the supplied
        ///   <paramref name="passwordResetToken" />
        /// </summary>
        /// <param name="passwordResetToken"> The password reset token to perform the lookup on. </param>
        /// <param name="newPassword"> The new password for the user. </param>
        /// <returns> </returns>
        public bool ResetPassword(string passwordResetToken, string newPassword)
        {
            return _userService.ResetPassword(passwordResetToken, newPassword);
        }


        public bool IsPasswordResetTokenValid(string token)
        {
            return _userService.IsPasswordResetTokenValid(token);
        }

        #endregion

        #region IOAuthProvider Members

        /// <summary>
        ///   Creates the OAuth account.
        /// </summary>
        /// <param name="provider"> The provider. </param>
        /// <param name="providerUserId"> The provider user id. </param>
        /// <param name="user"> The user. </param>
        public void CreateOAuthAccount(string provider, string providerUserId, IMembershipUser user)
        {
            _userService.CreateOAuthAccount(provider, providerUserId, user);
        }

        /// <summary>
        ///   Dissassociates the OAuth account for a userid.
        /// </summary>
        /// <param name="provider"> The provider. </param>
        /// <param name="providerUserId"> The provider user id. </param>
        /// <returns> </returns>
        public bool DisassociateOAuthAccount(string provider, string providerUserId)
        {
            IMembershipUser user = _userService.GetUserByOAuthProvider(provider, providerUserId);
            if (user == null)
            {
                return false;
            }
            IEnumerable<OAuthAccount> accounts = _userService.GetOAuthAccountsForUser(user.UserName);

            if (HasLocalAccount(user.UserName))
                return _userService.DeleteOAuthAccount(provider, providerUserId);

            if (accounts.Count() > 1)
                return _userService.DeleteOAuthAccount(provider, providerUserId);

            return false;
        }

        /// <summary>
        ///   Gets the OAuth client data for a provider
        /// </summary>
        /// <param name="provider"> The provider. </param>
        /// <returns> </returns>
        public AuthenticationClientData GetOAuthClientData(string providerName)
        {
            return _authenticationClients[providerName];
        }

        /// <summary>
        /// Gets the registered client data.
        /// </summary>
        /// <value>
        /// The registered client data.
        /// </value>
        public ICollection<AuthenticationClientData> RegisteredClientData
        {
            get { return _authenticationClients.Values; }
        }

        /// <summary>
        ///   Gets the name of the OAuth accounts for a user.
        /// </summary>
        /// <param name="username"> The username. </param>
        /// <returns> </returns>
        public IEnumerable<OAuthAccount> GetOAuthAccountsFromUserName(string username)
        {
            return _userService.GetOAuthAccountsForUser(username);
        }

        /// <summary>
        /// Requests the OAuth authentication.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="returnUrl">The return URL.</param>
        public void RequestOAuthAuthentication(string provider, string returnUrl)
        {
            AuthenticationClientData client = _authenticationClients[provider];
            _applicationEnvironment.RequestAuthentication(client.AuthenticationClient, this, returnUrl);
        }

        /// <summary>
        ///   Verifies the OAuth authentication.
        /// </summary>
        /// <param name="action"> The action. </param>
        /// <returns> </returns>
        public AuthenticationResult VerifyOAuthAuthentication(string returnUrl)
        {
            string providerName = _applicationEnvironment.GetOAuthPoviderName();
            if (String.IsNullOrEmpty(providerName))
            {
                return AuthenticationResult.Failed;
            }

            AuthenticationClientData client = _authenticationClients[providerName];
            return _applicationEnvironment.VerifyAuthentication(client.AuthenticationClient, this, returnUrl);
        }

        /// <summary>
        /// Attempts to perform an OAuth Login.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="providerUserId">The provider user id.</param>
        /// <param name="persistCookie">if set to <c>true</c> [persist cookie].</param>
        /// <returns></returns>
        public bool OAuthLogin(string provider, string providerUserId, bool persistCookie)
        {
            AuthenticationClientData oauthProvider = _authenticationClients[provider];
            HttpContextBase context = _applicationEnvironment.AcquireContext();
            var securityManager = new OpenAuthSecurityManager(context, oauthProvider.AuthenticationClient, this);
            return securityManager.Login(providerUserId, persistCookie);
        }

        #endregion

        #region IOpenAuthDataProvider Members

        /// <summary>
        /// Gets the user name from open auth.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="providerUserId">The provider user id.</param>
        /// <returns></returns>
        public string GetUserNameFromOpenAuth(string provider, string providerUserId)
        {
            IMembershipUser user = _userService.GetUserByOAuthProvider(provider, providerUserId);
            if (user != null)
            {
                return user.UserName;
            }
            return String.Empty;
        }

        #endregion

        /// <summary>
        /// Generates the token.
        /// </summary>
        /// <returns></returns>
        private static string GenerateToken()
        {
            using (var prng = new RNGCryptoServiceProvider())
            {
                return GenerateToken(prng);
            }
        }

        /// <summary>
        /// Generates the token.
        /// </summary>
        /// <param name="generator">The generator.</param>
        /// <returns></returns>
        internal static string GenerateToken(RandomNumberGenerator generator)
        {
            var tokenBytes = new byte[_tokenSizeInBytes];
            generator.GetBytes(tokenBytes);
            return HttpServerUtility.UrlTokenEncode(tokenBytes);
        }

        /// <summary>
        /// Registers the client.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="extraData">The extra data.</param>
        public static void RegisterClient(IAuthenticationClient client,
                                          string displayName, IDictionary<string, object> extraData)
        {
            var clientData = new AuthenticationClientData(client, displayName, extraData);
            _authenticationClients.Add(client.ProviderName, clientData);
        }
    }
}