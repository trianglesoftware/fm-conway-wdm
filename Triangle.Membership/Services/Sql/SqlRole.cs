﻿using System.Collections.Generic;
using Triangle.Membership.Interface;

namespace Triangle.Membership.Services.Sql
{
    public class SqlRole : IRole<SqlUser>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<SqlUser> Users { get; set; }
    }
}