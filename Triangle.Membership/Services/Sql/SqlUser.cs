﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using Triangle.Membership.Data;
using Triangle.Membership.Interface;

namespace Triangle.Membership.Services.Sql
{
    public class SqlUser : IMembershipUser
    {
        public Guid ID { get; set; }

        [Required]
        [StringLength(maximumLength: 100)]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        public string Email { get; set; }

        [Display(Name = "Approved")]
        public bool IsApproved { get; set; }

        [Display(Name = "Anonymous")]
        public bool IsAnonymous { get; set; }

        [Display(Name = "Password changed")]
        public DateTime? LastPasswordChangedDate { get; set; }

        [Display(Name = "Last locked out")]
        public DateTime? LastLockoutDate { get; set; }

        [Display(Name = "Locked out")]
        public bool IsLockedOut { get; set; }

        [Display(Name = "Failed password attempts")]
        public int FailedPasswordAttemptCount { get; set; }

        [Display(Name = "Failed password answer attempts")]
        public int FailedPasswordAnswerAttemptCount { get; set; }

        [Display(Name = "Last logged in")]
        public DateTime? LastLoginDate { get; set; }

        public string PasswordResetToken { get; set; }

        [Display(Name = "Password reset expiration")]
        public DateTime? PasswordResetTokenExpiration { get; set; }

        public Guid RoleID { get; set; }
        public string RoleLookup { get; set; }

        internal static Expression<Func<User, SqlUser>> EntityToModel = s => new SqlUser
        {
            ID = s.UserPK,
            UserName = s.UserName,
            Email = s.Email,
            FailedPasswordAnswerAttemptCount = s.FailedPasswordAnswerAttemptCount,
            FailedPasswordAttemptCount = s.FailedPasswordAttemptCount,
            IsAnonymous = s.IsAnonymous,
            IsApproved = s.IsApproved,
            IsLockedOut = s.IsLockedOut,
            LastLockoutDate = s.LastLockoutDate,
            LastLoginDate = s.LastLoginDate,
            LastPasswordChangedDate = s.LastPasswordChangedDate,
            PasswordResetToken = s.PasswordResetToken,
            PasswordResetTokenExpiration = s.PasswordResetExpiry,
            RoleID = s.Roles.Select(b => b.RolePK).FirstOrDefault(),
            RoleLookup = s.Roles.Select(b => b.Name).FirstOrDefault() ?? ""
        };

    }
}