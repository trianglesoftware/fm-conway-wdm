﻿
using Microsoft.Web.WebPages.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using Triangle.Membership.Data;
using Triangle.Membership.Interface;
using Triangle.Membership.Membership;
using Triangle.Membership.Models;
using Triangle.Membership.Utilities;

namespace Triangle.Membership.Services.Sql
{
    /// <summary>
    /// Handles all requests relating to users and membership.
    /// </summary>
    public class SqlMembershipService : IUserService
    {
        private MembershipEntitiesContainer _membershipContext;

        /// <summary>
        /// Constructor.
        /// </summary>
        public SqlMembershipService()
        {
            _membershipContext = new MembershipEntitiesContainer();
        }

        public LoginSummaryModel GetLoginSummary(string userName)
        {
            throw new NotImplementedException();
        }

        #region IUserService

        public IMembershipUser Add(IRegistration model)
        {
            if (!model.Password.Equals(model.ConfirmPassword))
                throw new InvalidOperationException("Passwords must match.");

            bool userNameExists = _membershipContext.Users.Where(u => u.UserName.Equals(model.UserName, StringComparison.InvariantCultureIgnoreCase)).Any();

            if (userNameExists)
                throw new InvalidOperationException("Username already exists.");

            string password = PasswordHash.CreateHash(model.Password);

            User newUser = new User()
            {
                UserPK = Guid.NewGuid(),
                UserName = model.UserName,
                Password = password,
                Email = model.Email,
                LastLoginDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value,
                LastLockoutDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value,
                LastPasswordChangedDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value,
                IsActive = true
            };

            _membershipContext.Users.Add(newUser);
            _membershipContext.SaveChanges();

            var returnUser = new SqlUser()
            {
                ID = newUser.UserPK,
                UserName = newUser.UserName,
                Email = newUser.Email
            };

            return returnUser;

        }

        public IMembershipUser Add(IMembershipUser model, string password, string confirmationPassword)
        {
            if (!password.Equals(confirmationPassword))
                throw new InvalidOperationException("Passwords must match.");

            bool userNameExists = _membershipContext.Users.Where(u => u.UserName.Equals(model.UserName, StringComparison.InvariantCultureIgnoreCase)).Any();

            if (userNameExists)
                throw new InvalidOperationException("Username already exists.");

            string hashPassword = PasswordHash.CreateHash(password);

            if (model.ID == Guid.Empty)
                model.ID = Guid.NewGuid();

            User newUser = new User()
            {
                UserPK = model.ID,
                UserName = model.UserName,
                Password = hashPassword,
                Email = model.Email,
                IsAnonymous = model.IsAnonymous,
                IsApproved = model.IsApproved,
                IsLockedOut = model.IsLockedOut,
                FailedPasswordAnswerAttemptCount = model.FailedPasswordAnswerAttemptCount,
                FailedPasswordAttemptCount = model.FailedPasswordAttemptCount,
                LastLockoutDate = model.LastLockoutDate,
                LastLoginDate = model.LastLoginDate,
                LastPasswordChangedDate = model.LastPasswordChangedDate,
                IsActive = true
            };

            _membershipContext.Users.Add(newUser);
            _membershipContext.SaveChanges();

            return model;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when more than one user is found with the same 'UserName'.</exception>
        /// <exception cref="System.NullReferenceException">Thrown when no users with the given 'UserName' can be found.</exception>
        /// <param name="user"></param>
        public void Save(IMembershipUser user)
        {
            User current = _membershipContext.Users.Where(u => u.UserPK.Equals(user.ID)).SingleOrDefault();
            var isUserLockedOut = current.IsLockedOut;

            if (current == null)
            {
                throw new NullReferenceException("Unable to locate user '" + user.UserName + "' (" + user.ID + ")");
            }

            if (isUserLockedOut && !user.IsLockedOut)
            {
                user.FailedPasswordAttemptCount = 0;
            }

            current.UserName = user.UserName;
            current.Email = user.Email;
            current.FailedPasswordAnswerAttemptCount = user.FailedPasswordAnswerAttemptCount;
            current.FailedPasswordAttemptCount = user.FailedPasswordAttemptCount;
            current.IsAnonymous = user.IsAnonymous;
            current.IsApproved = user.IsApproved;
            current.IsLockedOut = user.IsLockedOut;
            current.LastLockoutDate = user.LastLockoutDate;
            current.LastLoginDate = user.LastLoginDate;
            current.LastPasswordChangedDate = user.LastPasswordChangedDate;
            current.PasswordResetExpiry = user.PasswordResetTokenExpiration;
            current.PasswordResetToken = user.PasswordResetToken;
            
            _membershipContext.SaveChanges();
        }

        public bool IsValidLogin(string userName, string password)
        {
            var user = _membershipContext.Users.Where(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

            if (user == null)
            {
                return false;
            }

            if (user.IsLockedOut || user.IsAnonymous || !user.IsApproved)
                return false;

            if (user.FailedPasswordAttemptCount > 5)
                return false;

            if (!PasswordHash.ValidatePassword(password, user.Password))
            {
                user.FailedPasswordAttemptCount++;
                if (user.FailedPasswordAttemptCount > 5)
                {
                    user.IsLockedOut = true;
                    user.LastLockoutDate = DateTime.Now;
                }

                _membershipContext.SaveChanges();

                return false;
            }

            user.LastLoginDate = DateTime.Now;

            if (user.FailedPasswordAttemptCount > 0)
            {
                user.FailedPasswordAttemptCount = 0;
            }

            _membershipContext.SaveChanges();

            return true;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            var user = _membershipContext.Users.Where(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

            if (user == null)
            {
                return false;
            }

            string oldPasswordHash = PasswordHash.CreateHash(oldPassword);

            if (!PasswordHash.ValidatePassword(oldPassword, user.Password))
                return false;

            string hashPassword = PasswordHash.CreateHash(newPassword);

            user.Password = hashPassword;
            user.LastPasswordChangedDate = DateTime.Now;

            _membershipContext.SaveChanges();

            return true;
        }

        public bool ChangePassword(string userName, string newPassword)
        {
            var user = _membershipContext.Users.Where(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

            if (user == null)
            {
                return false;
            }

            string hashPassword = PasswordHash.CreateHash(newPassword);

            user.Password = hashPassword;
            user.LastPasswordChangedDate = DateTime.Now;

            _membershipContext.SaveChanges();

            return true;
        }

        public bool ResetPassword(string passwordResetToken, string newPassword)
        {
            var user = _membershipContext.Users.Where(u => u.PasswordResetToken.Equals(passwordResetToken) && u.PasswordResetExpiry.HasValue && u.PasswordResetExpiry > DateTime.Now).SingleOrDefault();

            if (user == null)
                return false;

            user.Password = PasswordHash.CreateHash(newPassword);
            user.PasswordResetExpiry = null;
            user.LastPasswordChangedDate = DateTime.Now;
            
            _membershipContext.SaveChanges();

            return true;
        }

        public bool IsPasswordResetTokenValid(string token)
        {
            return _membershipContext.Users.Any(u => u.PasswordResetToken.Equals(token) && u.PasswordResetExpiry.HasValue && u.PasswordResetExpiry > DateTime.Now);
        }

        public PasswordResetToken GetNewPasswordResetToken(string userName)
        {

            string token = PasswordHash.GeneratePasswordResetToken();
            var user = _membershipContext.Users.Where(u => u.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase)).Select(SqlUser.EntityToModel).Single();

            user.PasswordResetToken = token;
            user.PasswordResetTokenExpiration = DateTime.Now.AddMinutes(15);

            Save(user);

            return new PasswordResetToken { Token = token, User = (IMembershipUser)user };
        }

        public bool UserNameExists(string userName)
        {
            return _membershipContext.Users.Any(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase));
        }

        public IEnumerable<IMembershipUser> GetAllUsers()
        {
            var users = _membershipContext.Users.Select(SqlUser.EntityToModel).ToList();
            return users.Cast<IMembershipUser>();
        }

        public IEnumerable<IMembershipUser> GetAllUsers(int resultCount, int page, out int pageCount)
        {
            int recordCount = _membershipContext.Users.Count();
            pageCount = (int)Math.Ceiling((decimal)recordCount / resultCount);

            var users = _membershipContext.Users.OrderBy(u => u.UserName).Skip(resultCount * page).Take(resultCount).Select(SqlUser.EntityToModel).ToList();
            return users.Cast<IMembershipUser>();
        }

        public IQueryable<User> QueryFilter(IQueryable<User> source, string query)
        {
            DateTime dateQuery;
            var isDateQuery = DateTime.TryParse(query, out dateQuery);

            if (!string.IsNullOrWhiteSpace(query))
            {
                source = source.Where(a =>
                    a.UserName.Contains(query) ||
                    a.Email.Contains(query) ||
                    (isDateQuery ? a.LastLoginDate == dateQuery : false) ||
                    (isDateQuery ? a.LastPasswordChangedDate == dateQuery : false) ||
                    a.Roles.Any(b => b.Name.Contains(query)));
            }

            return source;
        }

        public IEnumerable<IMembershipUser> GetActiveUsers()
        {
            var users = _membershipContext.Users.Where(u => u.IsApproved && !u.IsLockedOut && u.IsActive).Select(SqlUser.EntityToModel).ToList();
            return users.Cast<IMembershipUser>();
        }

        public IEnumerable<IMembershipUser> GetActiveUsers(int resultCount, int page, string query, out int pageCount)
        {
            var source = _membershipContext.Users.Where(u => u.IsApproved && u.IsActive);
            source = QueryFilter(source, query);

            var sourceFiltered = source.OrderBy(u => u.UserName).Skip(resultCount * page).Take(resultCount);

            var recordCount = sourceFiltered.Count();
            pageCount = (int)Math.Ceiling((decimal)recordCount / resultCount);

            var results = sourceFiltered.Select(SqlUser.EntityToModel).ToList().Cast<IMembershipUser>();

            return results;
        }

        public IEnumerable<IMembershipUser> GetLockedOutUsers()
        {
            var users = _membershipContext.Users.Where(u => u.IsLockedOut && u.IsActive).Select(SqlUser.EntityToModel).ToList();
            return users.Cast<IMembershipUser>();
        }

        public IEnumerable<IMembershipUser> GetLockedOutUsers(int resultCount, int page, string query, out int pageCount)
        {
            var source = _membershipContext.Users.Where(u => u.IsLockedOut && u.IsActive);
            source = QueryFilter(source, query);

            var sourceFiltered = source.OrderBy(u => u.UserName).Skip(resultCount * page).Take(resultCount);

            var recordCount = sourceFiltered.Count();
            pageCount = (int)Math.Ceiling((decimal)recordCount / resultCount);

            var results = sourceFiltered.Select(SqlUser.EntityToModel).ToList().Cast<IMembershipUser>();

            return results;
        }

        public IEnumerable<IMembershipUser> GetUsersRequiringApproval()
        {
            var users = _membershipContext.Users.Where(u => !u.IsApproved && !u.IsLockedOut && u.IsActive).Select(SqlUser.EntityToModel).ToList();
            return users.Cast<IMembershipUser>();
        }

        public IEnumerable<IMembershipUser> GetUsersRequiringApproval(int resultCount, int page, string query, out int pageCount)
        {
            var source = _membershipContext.Users.Where(u => !u.IsApproved && !u.IsLockedOut && u.IsActive);
            source = QueryFilter(source, query);

            var sourceFiltered = source.OrderBy(u => u.UserName).Skip(resultCount * page).Take(resultCount);

            var recordCount = sourceFiltered.Count();
            pageCount = (int)Math.Ceiling((decimal)recordCount / resultCount);

            var results = sourceFiltered.Select(SqlUser.EntityToModel).ToList().Cast<IMembershipUser>();

            return results;
        }

        public IEnumerable<IMembershipUser> GetObsoleteUsers()
        {
            var users = _membershipContext.Users.Where(u => !u.IsActive).Select(SqlUser.EntityToModel).ToList();
            return users.Cast<IMembershipUser>();
        }

        public IEnumerable<IMembershipUser> GetObsoleteUsers(int resultCount, int page, out int pageCount)
        {
            int recordCount = _membershipContext.Users.Where(u => !u.IsActive).Count();
            pageCount = (int)Math.Ceiling((decimal)recordCount / resultCount);

            var users = _membershipContext.Users.Where(u => !u.IsActive).OrderBy(u => u.UserName).Skip(resultCount * page).Take(resultCount).Select(SqlUser.EntityToModel).ToList();
            return users.Cast<IMembershipUser>();
        }

        public IMembershipUser GetUserByUsername(string userName)
        {
            var matchingUsers = _membershipContext.Users
                        .Where(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase))
                        .Select(SqlUser.EntityToModel)
                        .Take(2)
                        .ToList();

            if (matchingUsers.Count == 0 || matchingUsers.Count > 1)
            {
                return null;
            }

            return matchingUsers.Single();
        }

        public IMembershipUser GetUserByID(Guid id)
        {
            SqlUser matchingUser = _membershipContext.Users
                        .Where(s => s.UserPK.Equals(id))
                        .Select(SqlUser.EntityToModel)
                        .Single();

            return matchingUser;
        }

        public IMembershipUser GetUserByPasswordResetToken(string passwordResetToken)
        {
            throw new NotImplementedException();
        }

        #region OAuth

        public IMembershipUser CreateOAuthAccount(string provider, string providerUserId, IMembershipUser user)
        {
            throw new NotImplementedException();
        }

        public IMembershipUser GetUserByOAuthProvider(string provider, string providerUserId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<OAuthAccount> GetOAuthAccountsForUser(string username)
        {
            throw new NotImplementedException();
        }

        public bool DeleteOAuthAccount(string provider, string providerUserId)
        {
            throw new NotImplementedException();
        }

        public bool HasLocalAuthAccount(string userName)
        {
            throw new NotImplementedException();
        }

        public bool SetLocalAuthAccountPassword(string userName, string password)
        {
            throw new NotImplementedException();
        }

        #endregion

        #endregion

    }
}
