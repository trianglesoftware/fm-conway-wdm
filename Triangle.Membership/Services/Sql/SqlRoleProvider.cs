﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Triangle.Membership.Data;

namespace Triangle.Membership.Services.Sql
{
    public class SqlRoleProvider : RoleProvider
    {
        public SqlRoleProvider()
            : base()
        {
        }

        public override string ApplicationName { get; set; }

        public override void CreateRole(string roleName)
        {
            using (var membership = new MembershipEntitiesContainer())
            {
                bool roleExists = membership.Roles.Where(r => r.Name.Equals(roleName, StringComparison.InvariantCultureIgnoreCase)).Any();

                if (roleExists)
                    throw new InvalidOperationException("Role '" + roleName + "' already exists.");

                Role newRole = new Role()
                {
                    RolePK = Guid.NewGuid(),
                    Name = roleName
                };

                membership.Roles.Add(newRole);
                membership.SaveChanges();
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            using (var membership = new MembershipEntitiesContainer())
            {
                Role role = membership.Roles.Where(r => r.Name.Equals(roleName, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

                if (role == null)
                    return false;

                membership.Roles.Remove(role);
                membership.SaveChanges();
            }

            return true;
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            string[] users = null;

            using (var membership = new MembershipEntitiesContainer())
            {
               users = membership.Users.Where(u => u.Roles.Any(r => r.Name.Equals(roleName, StringComparison.InvariantCultureIgnoreCase))).Select(u => u.UserName).ToArray();
            }

            return users;
        }

        public override string[] GetAllRoles()
        {
            string[] roles = null;

            using (var membership = new MembershipEntitiesContainer())
            {
                roles = membership.Roles.Select(r => r.Name).ToArray();
            }

            return roles;
        }

        public override string[] GetRolesForUser(string username)
        {
            string[] rolesForUser = null;

            using (var membership = new MembershipEntitiesContainer())
            {
                rolesForUser = membership.Roles.Where(r => r.Users.Any(u => u.UserName.Equals(username, StringComparison.InvariantCultureIgnoreCase))).Select(r => r.Name).ToArray();
            }

            return rolesForUser;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return FindUsersInRole(roleName, null);
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            using (var membership = new MembershipEntitiesContainer())
            {
                return membership.Users.Any(u => u.UserName.Equals(username, StringComparison.InvariantCultureIgnoreCase) && u.Roles.Any(r => r.Name.Equals(roleName, StringComparison.InvariantCultureIgnoreCase)));
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            using (var membership = new MembershipEntitiesContainer())
            {
                List<User> users = membership.Users.Where(u => usernames.Any(un => un.Equals(u.UserName, StringComparison.InvariantCultureIgnoreCase))).ToList();
                List<Role> roles = membership.Roles.Where(r => roleNames.Any(rn => rn.Equals(r.Name, StringComparison.InvariantCultureIgnoreCase))).ToList();

                users.ForEach(u =>
                {
                    roles.ForEach(r =>
                    {
                        if (!u.Roles.Contains(r))
                            u.Roles.Add(r);
                    });
                });

                membership.SaveChanges();
            }

        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            using (var membership = new MembershipEntitiesContainer())
            {
                List<User> users = membership.Users.Where(u => usernames.Any(un => un.Equals(u.UserName, StringComparison.InvariantCultureIgnoreCase))).ToList();
                List<Role> roles = membership.Roles.Where(r => roleNames.Any(rn => rn.Equals(r.Name, StringComparison.InvariantCultureIgnoreCase))).ToList();

                users.ForEach(u =>
                {
                    roles.ForEach(r =>
                    {
                        if (u.Roles.Contains(r))
                            u.Roles.Remove(r);
                    });
                });

                membership.SaveChanges();
            }
        }

        public override bool RoleExists(string roleName)
        {
            using (var membership = new MembershipEntitiesContainer())
            {
                return membership.Roles.Any(r => r.Name.Equals(roleName, StringComparison.InvariantCultureIgnoreCase));
            }

        }
    }

}
