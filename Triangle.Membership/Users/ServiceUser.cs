﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Interface;
using Triangle.Membership.Services.Sql;

namespace Triangle.Membership.Users
{
    public class ServiceUser : UserContext
    {
        private SqlRoleProvider _roleProvider;
        private readonly string _username;

        public ServiceUser(IUserService userService, string username)
            : base(userService)
        {
            _username = username;
        }

        public override string Name
        {
            get { return _username; }
        }

        public override bool IsAuthenticated
        {
            get { return true; }
        }

        public override bool IsInRole(string roleName)
        {
            return _roleProvider.IsUserInRole(_username, roleName);
        }
    }
}
