﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Interface;

namespace Triangle.Membership.Users
{
    public abstract class UserContext
    {
        public abstract string Name { get; }
        public abstract bool IsAuthenticated { get; }
        public abstract bool IsInRole(string roleName);

        protected IUserService _userService;

        public UserContext(IUserService userService)
        {
            _userService = userService;
        }

        public virtual IMembershipUser UserDetails
        {
            get { return GetUserDetails(); }
        }

        private IMembershipUser GetUserDetails()
        {
            return _userService.GetUserByUsername(Name);
        }
    }

}
