﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Triangle.Membership.Interface;

namespace Triangle.Membership.Users
{
    public class AspUserContext : UserContext
    {
        public AspUserContext(IUserService userService)
            : base(userService) { }

        public override string Name
        {
            get { return HttpContext.Current.User.Identity.Name; }
        }

        public override bool IsAuthenticated
        {
            get { return HttpContext.Current.User.Identity.IsAuthenticated; }
        }

        public override bool IsInRole(string roleName)
        {
            return HttpContext.Current.User.IsInRole(roleName);
        }
    }
}
