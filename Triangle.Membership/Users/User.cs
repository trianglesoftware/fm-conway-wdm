﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle.Membership.Users
{

    public class User
    {
        public Guid ID { get; set; }
        public string Name { get; set; }

    }
}
