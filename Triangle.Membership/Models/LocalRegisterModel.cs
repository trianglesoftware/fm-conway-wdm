﻿
using System.ComponentModel.DataAnnotations;
using Triangle.Membership.Interface;
using Triangle.Membership.Utilities;

namespace Triangle.Membership.Models
{
    /// <summary>
    /// Model for accepting user registration requests.
    /// </summary>    
    public class LocalRegisterModel : IRegistration
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(maximumLength: 320)]
        [RegularExpression(Emails.EmailValidationRegularExpression)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [StringLength(maximumLength: 100, MinimumLength = 2)]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(maximumLength: 128, MinimumLength = 6, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(maximumLength: 128)]
        [Compare(otherProperty: "Password", ErrorMessage = "The two passwords are not the same.")]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }
    }
}
