﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triangle.Membership.Interface;

namespace Triangle.Membership.Models
{
    public class PasswordResetToken
    {
        public string Token { get; set; }
        public IMembershipUser User { get; set; }
        
    }
}
