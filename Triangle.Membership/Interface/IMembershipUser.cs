using System;
using System.Collections.Generic;

namespace Triangle.Membership.Interface
{
    public interface IMembershipUser
    {
        Guid ID { get; set; }
        string UserName { get; set; }
        string Email { get; set; }
        bool IsApproved { get; set; }
        bool IsLockedOut { get; set; }
        bool IsAnonymous { get; set; }
        System.DateTime? LastPasswordChangedDate { get; set; }
        System.DateTime? LastLockoutDate { get; set; }
        int FailedPasswordAttemptCount { get; set; }
        int FailedPasswordAnswerAttemptCount { get; set; }
        System.DateTime? LastLoginDate { get; set; }
        string PasswordResetToken { get; set; }
        DateTime? PasswordResetTokenExpiration { get; set; }
    }

    public interface IRegistration
    {
        string UserName { get; set; }
        string Password { get; set; }
        string ConfirmPassword { get; set; }
        string Email { get; set; }
    }

}