using Microsoft.Web.WebPages.OAuth;
using System;
using System.Collections.Generic;
using Triangle.Membership.Interface;
using Triangle.Membership.Models;

namespace Triangle.Membership.Interface
{
    public interface IUserService
    {
        PasswordResetToken GetNewPasswordResetToken(string userName);

        bool IsValidLogin(string userName, string password);
        bool ChangePassword(string userName, string oldPassword, string newPassword);
        bool ChangePassword(string userName, string newPassword);
        bool ResetPassword(string passwordResetToken, string newPassword);
        bool IsPasswordResetTokenValid(string token);
        bool DeleteOAuthAccount(string provider, string providerUserId);
        bool HasLocalAuthAccount(string userName);
        bool SetLocalAuthAccountPassword(string userName, string password);
        bool UserNameExists(string userName);

        IEnumerable<OAuthAccount> GetOAuthAccountsForUser(string username);

        IMembershipUser GetUserByPasswordResetToken(string passwordResetToken);
        IMembershipUser CreateOAuthAccount(string provider, string providerUserId, IMembershipUser user);
        IMembershipUser GetUserByUsername(string username);
        IMembershipUser GetUserByID(Guid id);
        IMembershipUser GetUserByOAuthProvider(string provider, string providerUserId);

        IEnumerable<IMembershipUser> GetAllUsers();
        IEnumerable<IMembershipUser> GetAllUsers(int resultCount, int page, out int pageCount);
        IEnumerable<IMembershipUser> GetActiveUsers();
        IEnumerable<IMembershipUser> GetActiveUsers(int resultCount, int page, string query, out int pageCount);
        IEnumerable<IMembershipUser> GetLockedOutUsers();
        IEnumerable<IMembershipUser> GetLockedOutUsers(int resultCount, int page, string query, out int pageCount);
        IEnumerable<IMembershipUser> GetUsersRequiringApproval();
        IEnumerable<IMembershipUser> GetUsersRequiringApproval(int resultCount, int page, string query, out int pageCount);
        IEnumerable<IMembershipUser> GetObsoleteUsers();
        IEnumerable<IMembershipUser> GetObsoleteUsers(int resultCount, int page, out int pageCount);

        IMembershipUser Add(IRegistration user);
        IMembershipUser Add(IMembershipUser user, string password, string confirmationPassword);
        void Save(IMembershipUser user);
    }
}