using System.Collections.Generic;
using Triangle.Membership.Models;
using Triangle.Membership.Users;
namespace Triangle.Membership.Interface
{
    public interface IMembershipProvider
    {
        /// <summary>
        /// Determines whether the provided <paramref name="username"/> and
        /// <paramref name="password"/> combination is valid
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="rememberMe">
        /// if set to <c>true</c> [remember me].
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        bool Login(string username, string password, bool rememberMe = false);

        /// <summary>
        ///   Logout the current user
        /// </summary>
        void Logout();

        /// <summary>
        ///   Creates an account.
        /// </summary>
        /// <param name="user"> The user. </param>
        IMembershipUser CreateAccount(IRegistration user);

        /// <summary>
        ///   Updates the account.
        /// </summary>
        /// <param name="user"> The user. </param>
        void UpdateAccount(IMembershipUser user);

        /// <summary>
        ///   Get the account by Username.
        /// </summary>
        /// <param name="user"> The user. </param>
        IMembershipUser GetAccount(string userName);

        /// <summary>
        ///   Changes the password for a user
        /// </summary>
        /// <param name="username"> The username. </param>
        /// <param name="oldPassword"> The old password. </param>
        /// <param name="newPassword"> The new password. </param>
        /// <returns> </returns>
        bool ChangePassword(string username, string oldPassword, string newPassword);

        /// <summary>
        ///   Generates the password reset token for a user
        /// </summary>
        /// <param name="username"> The username. </param>
        /// <param name="tokenExpirationInMinutesFromNow"> The token expiration in minutes from now. </param>
        /// <returns> </returns>
        PasswordResetToken GeneratePasswordResetToken(string username);

        /// <summary>
        ///   Resets the password for the supplied
        ///   <paramref name="passwordResetToken" />
        /// </summary>
        /// <param name="passwordResetToken"> The password reset token to perform the lookup on. </param>
        /// <param name="newPassword"> The new password for the user. </param>
        /// <returns> </returns>
        bool ResetPassword(string passwordResetToken, string newPassword);


        bool IsPasswordResetTokenValid(string token);

    }
}