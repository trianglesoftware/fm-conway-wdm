﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle.Membership.Utilities
{
    public class Emails
    {
        /// <summary>
        /// Regex string for validating email addresses.
        /// </summary>
        /// <remarks>Only checking for an @ symbol with anything either side at the moment.</remarks>
        public const string EmailValidationRegularExpression = ".+[@]{1}.+";
    }
}
