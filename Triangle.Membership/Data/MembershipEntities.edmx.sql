
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 07/17/2013 12:47:59
-- Generated from EDMX file: C:\Projects\UK Landscapes\Dev\Dan\Triangle.Membership\MembershipEntities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HWMartin];
GO
IF SCHEMA_ID(N'security') IS NULL EXECUTE(N'CREATE SCHEMA [security]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[security].[FK_UsersInRoles_User]', 'F') IS NOT NULL
    ALTER TABLE [security].[UsersInRoles] DROP CONSTRAINT [FK_UsersInRoles_User];
GO
IF OBJECT_ID(N'[security].[FK_UsersInRoles_Role]', 'F') IS NOT NULL
    ALTER TABLE [security].[UsersInRoles] DROP CONSTRAINT [FK_UsersInRoles_Role];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[security].[Users]', 'U') IS NOT NULL
    DROP TABLE [security].[Users];
GO
IF OBJECT_ID(N'[security].[Roles]', 'U') IS NOT NULL
    DROP TABLE [security].[Roles];
GO
IF OBJECT_ID(N'[security].[UsersInRoles]', 'U') IS NOT NULL
    DROP TABLE [security].[UsersInRoles];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [security].[Users] (
    [UserPK] uniqueidentifier  NOT NULL,
    [UserName] nvarchar(100)  NOT NULL,
    [Password] nvarchar(100)  NOT NULL,
    [Salt] nvarchar(128)  NOT NULL,
    [Email] nvarchar(300)  NOT NULL,
    [IsApproved] bit  NOT NULL,
    [IsLockedOut] bit  NOT NULL,
    [IsAnonymous] bit  NOT NULL,
    [LastPasswordChangedDate] datetime  NOT NULL,
    [LastLockoutDate] datetime  NOT NULL,
    [FailedPasswordAttemptCount] int  NOT NULL,
    [FailedPasswordAnswerAttemptCount] int  NOT NULL,
    [LastLoginDate] datetime  NOT NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [security].[Roles] (
    [RolePK] uniqueidentifier  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [Description] nvarchar(200)  NOT NULL
);
GO

-- Creating table 'UsersInRoles'
CREATE TABLE [security].[UsersInRoles] (
    [Users_UserPK] uniqueidentifier  NOT NULL,
    [Roles_RolePK] uniqueidentifier  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [UserPK] in table 'Users'
ALTER TABLE [security].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserPK] ASC);
GO

-- Creating primary key on [RolePK] in table 'Roles'
ALTER TABLE [security].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([RolePK] ASC);
GO

-- Creating primary key on [Users_UserPK], [Roles_RolePK] in table 'UsersInRoles'
ALTER TABLE [security].[UsersInRoles]
ADD CONSTRAINT [PK_UsersInRoles]
    PRIMARY KEY NONCLUSTERED ([Users_UserPK], [Roles_RolePK] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Users_UserPK] in table 'UsersInRoles'
ALTER TABLE [security].[UsersInRoles]
ADD CONSTRAINT [FK_UsersInRoles_User]
    FOREIGN KEY ([Users_UserPK])
    REFERENCES [security].[Users]
        ([UserPK])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Roles_RolePK] in table 'UsersInRoles'
ALTER TABLE [security].[UsersInRoles]
ADD CONSTRAINT [FK_UsersInRoles_Role]
    FOREIGN KEY ([Roles_RolePK])
    REFERENCES [security].[Roles]
        ([RolePK])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersInRoles_Role'
CREATE INDEX [IX_FK_UsersInRoles_Role]
ON [security].[UsersInRoles]
    ([Roles_RolePK]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------