﻿using Mobile.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using Triangle.Membership.Interface;
using Triangle.Membership.Users;

namespace Mobile
{
    public class Service : IService
    {
        DataService _dataService;

        public Service()
        {
            _dataService = new DataService();
        }

        [WebInvoke(Method = "GET", UriTemplate = "/SignOn", ResponseFormat = WebMessageFormat.Json)]
        public UserProfileModel SignOn()
        {
            ServiceUser userContext = GetUserContext();
            IMembershipUser userDetails = userContext.UserDetails;

            var context = new FMConwayEntities();

            Guid? employeeFK = null;

            try
            {
                employeeFK = context.UserProfiles.Where(a => a.UserFK == userDetails.ID).Select(a => a.EmployeeFK).SingleOrDefault();
            }
            catch (Exception ex)
            {
                // if get "schema specified is not valid" error here, update / refresh the mobile service edmx as it's missing a column
                // the FMConway.Services edmx has. Unable to log this error here as _dataService.LogError() will throw the same error
                throw;
            }

            UserProfileModel currentUser = new UserProfileModel
            {
                UserID = userDetails.ID,
                StaffID = employeeFK,
                AppVersion = WebConfigurationManager.AppSettings["appVersion"].ToString(),
            };

            return currentUser;
        }

        #region Data For App

        [WebGet(UriTemplate = "/Users", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<UserModel> GetUsers()
        {
            return _dataService.GetUsers();
        }

        [WebGet(UriTemplate = "/Staff", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<StaffModel> GetStaff()
        {
            return _dataService.GetStaff();
        }

        [WebGet(UriTemplate = "/Vehicles", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<VehicleModel> GetVehicles()
        {
            return _dataService.GetVehicles();
        }

        [WebGet(UriTemplate = "/EquipmentTypes", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<EquipmentTypeModel> GetEquipmentTypes()
        {
            return _dataService.GetEquipmentTypes();
        }

        [WebGet(UriTemplate = "/Equipment", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<EquipmentModel> GetEquipment()
        {
            return _dataService.GetEquipment();
        }

        [WebGet(UriTemplate = "/Checklists", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<ChecklistModel> GetChecklists()
        {
            return _dataService.GetChecklists();
        }

        [WebGet(UriTemplate = "/VehicleTypes", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<VehicleTypeModel> GetVehicleTypes()
        {
            return _dataService.GetVehicleTypes();
        }

        [WebGet(UriTemplate = "/BriefingTypes", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<BriefingTypeModel> GetBriefingTypes()
        {
            return _dataService.GetBriefingTypes();
        }

        [WebGet(UriTemplate = "/ActivityTypes", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<ActivityTypeModel> GetActivityTypes()
        {
            return _dataService.GetActivityTypes();
        }

        //[WebGet(UriTemplate = "/ObservationTypes", ResponseFormat = WebMessageFormat.Json)]
        //public IEnumerable<ObservationTypeModel> GetObservationTypes()
        //{
        //    return _dataService.GetObservationTypes();
        //}

        [WebGet(UriTemplate = "/TaskTypes", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<TaskTypeModel> GetTaskTypes()
        {
            return _dataService.GetTaskTypes();
        }

        [WebGet(UriTemplate = "/WeatherConditions", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<WeatherConditionModel> GetWeatherConditions()
        {
            return _dataService.GetWeatherConditions();
        }

        [WebGet(UriTemplate = "/Shifts?userID={userID}&syncDate={syncDate}", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<ShiftModel> GetShifts(Guid userID, string syncDate)
        {
            return _dataService.GetShifts(userID, syncDate);
        }

        [WebGet(UriTemplate = "/ShiftUpdate?shiftID={shiftID}", ResponseFormat = WebMessageFormat.Json)]
        public ShiftUpdateModel GetShiftUpdate(Guid shiftID)
        {
            return _dataService.GetShiftUpdate(shiftID);
        }

        [WebGet(UriTemplate = "/JobPacks?userID={userID}&syncDate={syncDate}", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<JobPackModel> GetJobPacks(Guid userID, string syncDate)
        {
            return _dataService.GetJobPacks(userID, syncDate);
        }

        [WebGet(UriTemplate = "/Areas?userID={userID}&syncDate={syncDate}", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<AreaModel> GetAreas(Guid userID, string syncDate)
        {
            return _dataService.GetAreas(userID, syncDate);
        }

        [WebGet(UriTemplate = "/StaffRecord?recordID={staffRecordID}", ResponseFormat = WebMessageFormat.Json)]
        public string GetStaffRecord(Guid staffRecordID)
        {
            return _dataService.GetStaffRecord(staffRecordID);
        }

        [WebGet(UriTemplate = "/Document?documentID={documentID}&jobPackID={jobPackID}&briefing={briefing}", ResponseFormat = WebMessageFormat.Json)]
        public string GetDocument(Guid documentID, Guid jobPackID, bool briefing)
        {
            return _dataService.GetDocument(documentID, jobPackID, briefing);
        }

        [WebGet(UriTemplate = "/VehiclesOnShifts", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<ShiftVehicleModel> GetVehiclesOnShifts()
        {
            return _dataService.GetVehiclesOnShifts();
        }

        [WebGet(UriTemplate = "/VehicleChecklists?userID={userID}&syncDate={syncDate}", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<VehicleChecklistModel> GetVehicleChecklists(Guid userID, string syncDate)
        {
            return _dataService.GetVehicleChecklists(userID, syncDate);
        }

        [WebGet(UriTemplate = "/ErrorLogs?quantity={quantity}", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<ErrorLogModel> GetErrorLogs(int quantity)
        {
            return _dataService.GetErrorLogs(quantity);
        }

        [WebGet(UriTemplate= "/CheckForNewShifts?userID={userID}&syncDate={syncDate}", ResponseFormat = WebMessageFormat.Json)]
        public bool CheckForNewShifts(Guid userID, string syncDate)
        {
            return true;
        }

        [WebGet(UriTemplate = "/ActivityPriorities", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<ActivityPriorityModel> GetActivityPriorities()
        {
            return _dataService.GetActivityPriorities();
        }

        [WebGet(UriTemplate = "/Lanes", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<LaneModel> GetLanes()
        {
            return _dataService.GetLanes();
        }

        [WebGet(UriTemplate = "/TMRequirements", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<TMRequirementModel> GetTMRequirements()
        {
            return _dataService.GetTMRequirements();
        }

        [WebGet(UriTemplate = "/RoadSpeeds", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<RoadSpeedModel> GetRoadSpeeds()
        {
            return _dataService.GetRoadSpeeds();
        }

        [WebGet(UriTemplate = "/Directions", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<DirectionModel> GetDirections()
        {
            return _dataService.GetDirections();
        }

        [WebGet(UriTemplate = "/DisposalSites", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<DisposalSiteModel> GetDisposalSites()
        {
            return _dataService.GetDisposalSites();
        }

        [WebGet(UriTemplate = "/Customers", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<CustomerModel> GetCustomers()
        {
            return _dataService.GetCustomers();
        }

        [WebGet(UriTemplate = "/Contacts", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<ContactModel> GetContacts()
        {
            return _dataService.GetContacts();
        }

        [WebGet(UriTemplate = "/MethodStatements?syncDate={syncDate}", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<MethodStatementModel> GetMethodStatements(DateTime syncDate)
        {
            return _dataService.GetMethodStatements(syncDate);
        }

        [WebGet(UriTemplate = "/MethodStatementDocument?methodStatementID={methodStatementID}", ResponseFormat = WebMessageFormat.Json)]
        public string GetMethodStatementDocument(Guid methodStatementID)
        {
            return _dataService.GetMethodStatementDocument(methodStatementID);
        }

        [WebGet(UriTemplate = "/Depots", ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<DepotModel> GetDepots()
        {
            return _dataService.GetDepots();
        }

        #endregion

        #region Data From App

        [WebGet(UriTemplate = "/UpdateShiftStatus?shiftID={shiftID}&completionStatus={completionStatus}", ResponseFormat = WebMessageFormat.Json)]
        public void UpdateShiftStatus(Guid shiftID, int completionStatus)
        {
            try
            {
                _dataService.UpdateShiftStatus(shiftID, completionStatus);
            }
            catch (Exception e)
            {
                _dataService.LogError("UpdateShiftStatus", e, false);
                throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [WebGet(UriTemplate = "/CompleteShift?shiftID={shiftID}", ResponseFormat = WebMessageFormat.Json)]
        public void CompleteShift(Guid shiftID)
        {
            try
            {
                _dataService.CompleteShift(shiftID);
            }
            catch (Exception e)
            {
                _dataService.LogError("CompleteShift", e, false);
                throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
            }
        }

        //[WebGet(UriTemplate = "/CancelShift?shiftID={shiftID}", ResponseFormat = WebMessageFormat.Json)]
        //public void CancelShift(Guid shiftID)

        [WebInvoke(Method = "POST", UriTemplate = "/PushCancelShift", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushCancelShift(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var progresses = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    CancelShiftModel cancelShift = jss.Deserialize<CancelShiftModel>(progresses);

                    _dataService.CancelShift(cancelShift);
                }
                catch (Exception e)
                {
                    _dataService.LogError("CancelShift", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/UpdateShiftProgress", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public List<ShiftProgressResult> UpdateShiftProgress(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var progresses = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<ShiftProgressModel> shiftProgresses = jss.Deserialize<List<ShiftProgressModel>>(progresses);

                    var results = new List<ShiftProgressResult>();
                    foreach (ShiftProgressModel sp in shiftProgresses)
                    {
                        var result = _dataService.UpdateShiftProgress(sp);
                        if (result != null)
                        {
                            results.Add(result);
                        }
                    }

                    return results;
                }
                catch (Exception e)
                {
                    _dataService.LogError("UpdateShiftProgress", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/UpdateShiftProgressStarted", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public ServiceResult<string> UpdateShiftProgressStarted(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var json = sr.ReadToEnd();
                    var jss = new JavaScriptSerializer();
                    var shiftProgressStarted = jss.Deserialize<ShiftProgressStartedModel>(json);

                    var result = _dataService.UpdateShiftProgressStarted(shiftProgressStarted);

                    return new ServiceResult<string>(result);
                }
                catch (Exception e)
                {
                    _dataService.LogError("UpdateShiftProgressStarted", e, false);
                    throw new WebFaultException(HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushShiftStaff", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushShiftStaff(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var shiftStaff = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<ShiftStaffModel> sStaff = jss.Deserialize<List<ShiftStaffModel>>(shiftStaff);

                    foreach (var ss in sStaff)
                    {
                        _dataService.PushShiftStaff(ss);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushShiftStaff", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushShiftVehicles", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushShiftVehicles(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var shiftVehicles = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    ShiftVehicleContainerModel shiftVehicle = jss.Deserialize<ShiftVehicleContainerModel>(shiftVehicles);

                    foreach (var sv in shiftVehicle.ShiftVehicles)
                    {
                        _dataService.PushShiftVehicle(sv, shiftVehicle.RestrictedUpdate);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushShiftVehicles", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushVehicleChecklists", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushVehicleChecklists(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var vehicleChecklists = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<VehicleChecklistModel> vChecklists = jss.Deserialize<List<VehicleChecklistModel>>(vehicleChecklists);

                    if (vChecklists != null)
                    {
                        _dataService.PushVehicleChecklist(vChecklists);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushVehicleChecklists", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushTasks", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushTasks(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var tasks = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<TaskModel> taskList = jss.Deserialize<List<TaskModel>>(tasks);

                    if (taskList != null)
                    {
                        foreach (var t in taskList)
                        {
                            _dataService.PushTask(t);
                        }
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushTasks", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushTaskChecklists", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushTaskChecklists(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var taskChecklists = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<TaskChecklistModel> tChecklists = jss.Deserialize<List<TaskChecklistModel>>(taskChecklists);

                    if (tChecklists != null)
                    {
                        foreach (var tc in tChecklists)
                        {
                            _dataService.PushTaskChecklist(tc);

                        }
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushTaskChecklists", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushTaskEquipment", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushTaskEquipment(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var tasks = jss.Deserialize<List<TaskEquipmentBaseModel>>(sr.ReadToEnd());

                    if (tasks != null)
                    {
                        _dataService.PushTaskEquipment(tasks);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushTaskEquipment", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushAreas", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushAreas(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var areas = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<AreaModel> areaList = jss.Deserialize<List<AreaModel>>(areas);

                    if (areaList != null)
                    {
                        foreach (var a in areaList)
                        {
                            _dataService.PushArea(a);
                        }
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushAreas", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushAccidents", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushAccidents(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var accidents = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    AccidentModel accident = jss.Deserialize<AccidentModel>(accidents);

                    if (accident != null)
                    {
                        Guid id = _dataService.PushAccident(accident);

                        bool errors = false;

                        if (accident.Photos != null)
                        {
                            foreach (AccidentPhotoModel photo in accident.Photos)
                            {
                                photo.AccidentID = id;

                                if (!_dataService.PushAccidentPhoto(photo))
                                {
                                    errors = true;
                                }
                            }
                        }

                        if (errors)
                        {
                            throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                        }

                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushAccidents", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushObservations", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushObservations(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var observations = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    ObservationModel observation = jss.Deserialize<ObservationModel>(observations);

                    if (observation != null)
                    {
                        Guid id = _dataService.PushObservation(observation);

                        bool errors = false;

                        if (observation.Photos != null)
                        {
                            foreach (ObservationPhotoModel photo in observation.Photos)
                            {
                                photo.ObservationID = id;
                                if (!_dataService.PushObservationPhoto(photo))
                                {
                                    errors = true;
                                }
                            }
                        }

                        if (errors)
                        {
                            throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                        }

                        //return id;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushObservations", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }

            //return 0;
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushOutOfHours", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushOutOfHours(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var outOfHours = jss.Deserialize<OutOfHoursModel>(sr.ReadToEnd());

                    if (outOfHours != null)
                    {
                        _dataService.PushOutOfHours(outOfHours);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushOutOfHours", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushNotes", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushNotes(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var notes = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<NoteModel> noteList = jss.Deserialize<List<NoteModel>>(notes);

                    if (noteList != null)
                    {
                        foreach (var n in noteList)
                        {
                            _dataService.PushNote(n);
                        }
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushNotes", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushMaintenanceChecks", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushMaintenanceChecks(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var maintenanceChecks = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<MaintenanceCheckModel> maintenanceChecksList = jss.Deserialize<List<MaintenanceCheckModel>>(maintenanceChecks);

                    if (maintenanceChecksList != null)
                    {
                        _dataService.PushMaintenanceChecklist(maintenanceChecksList);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushMaintenanceChecks", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushMaintenanceCheckDetailPhoto", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushMaintenanceCheckDetailPhoto(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                string JSON = "";
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JSON = dataStream.ToString();

                    if (JSON == "[]")
                    {
                        return false;
                    }

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    MaintenanceCheckDetailPhotoModel mcdp = jss.Deserialize<MaintenanceCheckDetailPhotoModel>(dataStream);

                    if (mcdp.DataString != null)
                    {
                        _dataService.PushMaintenanceCheckDetailPhoto(mcdp);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushMaintenanceCheckDetailPhoto", e.ToString() + " JSON: " + JSON, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushBriefingSignature", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushBriefingSignature(Stream data)
        {

            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    BriefingSignatureModel d = jss.Deserialize<BriefingSignatureModel>(dataStream);

                    if (d.DataString != null)
                    {
                        return _dataService.PushBriefingSignature(d);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushBriefingSignature", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushActivitySignature", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushActivitySignature(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    ActivitySignatureModel d = jss.Deserialize<ActivitySignatureModel>(dataStream);

                    if (d.DataString != null)
                    {
                        return _dataService.PushActivitySignature(d);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushActivitySignature", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushDefectPhoto", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushDefectPhoto(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    VehicleChecklistAnswerDefectPhotoModel d = jss.Deserialize<VehicleChecklistAnswerDefectPhotoModel>(dataStream);

                    if (d.DataString != null)
                    {
                        return _dataService.PushVehicleDefectPhoto(d);
                    }
                    else { return false; }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushDefectPhoto", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushTaskPhoto", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushTaskPhoto(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    TaskPhotoModel d = jss.Deserialize<TaskPhotoModel>(dataStream);

                    if (d.DataString != null)
                    {
                        return _dataService.PushTaskPhoto(d);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushTaskPhoto", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushTaskChecklistAnswerPhoto", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushTaskChecklistAnswerPhoto(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var d = jss.Deserialize<TaskChecklistAnswerPhotoModel>(dataStream);

                    if (d.DataString != null)
                    {
                        return _dataService.PushTaskChecklistAnswerPhoto(d);
                    }
                    else { return false; }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushTaskChecklistAnswerPhoto", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushTaskSignature", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushTaskSignature(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    TaskSignatureModel d = jss.Deserialize<TaskSignatureModel>(dataStream);

                    if (d.DataString != null)
                    {
                        return _dataService.PushTaskSignature(d);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushTaskSignature", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushLoadingSheetSignature", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushLoadingSheetSignature(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    LoadingSheetSignatureModel d = jss.Deserialize<LoadingSheetSignatureModel>(dataStream);

                    if (d.DataString != null)
                    {
                        return _dataService.PushLoadingSheetSignature(d);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushLoadingSheetSignature", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushEquipmentItems", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushEquipmentItems(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var pushEquipmentItems = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var pushEquipmentItemModel = jss.Deserialize<PushEquipmentItemModel>(pushEquipmentItems);
                    if (pushEquipmentItemModel != null)
                    {
                        _dataService.PushEquipmentItems(pushEquipmentItemModel);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushEquipmentItems", e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushError", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushError(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                string JSON = "";
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JSON = dataStream;

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    List<ErrorLog> logs = jss.Deserialize<List<ErrorLog>>(dataStream);

                    foreach (ErrorLog e in logs)
                    {
                        _dataService.LogError(e.Description, e.Exception, true);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushError", e.ToString() + " JSON: " + JSON, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushDatabase", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushDatabase(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    string fileJSON = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    FileModel file = jss.Deserialize<FileModel>(fileJSON);

                    if (file != null)
                    {
                        _dataService.PushDatabase(file.DataString);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError("PushDatabase", e, false);
                    throw new WebFaultException(HttpStatusCode.InternalServerError);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushCleansingLogs", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushCleansingLogs(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var json = sr.ReadToEnd();

                    var jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var models = jss.Deserialize<List<CleansingLogModel>>(json);

                    if (models != null)
                    {
                        _dataService.PushCleansingLogs(models);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError(nameof(PushCleansingLogs), e, false);
                    throw new WebFaultException(HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushCleansingLogPhoto", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushCleansingLogPhoto(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    var jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var model = jss.Deserialize<CleansingLogPhotoModel>(dataStream);

                    if (model.DataString != null)
                    {
                        return _dataService.PushCleansingLogPhoto(model);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError(nameof(PushCleansingLogPhoto), e, false);
                    throw new WebFaultException(HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushActivityDetails", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushActivityDetails(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var json = sr.ReadToEnd();

                    var jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var model = jss.Deserialize<ActivityDetailModel>(json);

                    if (model != null)
                    {
                        _dataService.PushActivityDetails(model);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError(nameof(PushActivityDetails), e, false);
                    throw new WebFaultException(HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushActivityRiskAssessments", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushActivityRiskAssessments(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var json = sr.ReadToEnd();

                    var jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var model = jss.Deserialize<List<ActivityRiskAssessmentModel>>(json);

                    if (model != null)
                    {
                        _dataService.PushActivityRiskAssessments(model);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError(nameof(PushActivityRiskAssessments), e, false);
                    throw new WebFaultException(HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushUnloadingProcesses", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public void PushUnloadingProcesses(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var json = sr.ReadToEnd();

                    var jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var models = jss.Deserialize<List<UnloadingProcessModel>>(json);

                    if (models != null)
                    {
                        _dataService.PushUnloadingProcesses(models);
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError(nameof(PushUnloadingProcesses), e, false);
                    throw new WebFaultException(HttpStatusCode.BadRequest);
                }
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "/PushUnloadingProcessPhoto", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool PushUnloadingProcessPhoto(Stream data)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                try
                {
                    var dataStream = sr.ReadToEnd();

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;

                    var model = jss.Deserialize<UnloadingProcessPhotoModel>(dataStream);

                    if (model.DataString != null)
                    {
                        return _dataService.PushUnloadingProcessPhoto(model);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _dataService.LogError(nameof(PushUnloadingProcessPhoto), e, false);
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                }
            }
        }

        #endregion

        private ServiceUser GetUserContext()
        {
            string currentUsername = OperationContext.Current.ServiceSecurityContext.AuthorizationContext.Properties["Identity"] as String;

            var sms = new Triangle.Membership.Services.Sql.SqlMembershipService();
            return new ServiceUser(sms, currentUsername);
        }

        //private UserProfileModel GetUserProfile(UserContext userContext)
        //{
        //    var ups = new FMConway.Services.UserProfiles.UserProfileService(userContext);

        //    var currentUser = ups.GetUserProfile(userContext.UserDetails.ID);

        //    return new UserProfileModel()
        //    {
        //        Username = userContext.Name,
        //        Profile = currentUser
        //    };
        //}

        private DateTime StringToDate(string date)
        {
            var components = date.Split('-');
            int[] converted = Array.ConvertAll<string, int>(components, int.Parse);
            return new DateTime(converted[2], converted[1], converted[0], converted[3], converted[4], 0);
        }
    }
}
