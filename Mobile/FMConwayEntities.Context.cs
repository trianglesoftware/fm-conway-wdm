﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mobile
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class FMConwayEntities : DbContext
    {
        public FMConwayEntities()
            : base("name=FMConwayEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AccidentPhoto> AccidentPhotos { get; set; }
        public virtual DbSet<Accident> Accidents { get; set; }
        public virtual DbSet<AreaCallArea> AreaCallAreas { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<BillingRequirement> BillingRequirements { get; set; }
        public virtual DbSet<BriefingChecklistAnswer> BriefingChecklistAnswers { get; set; }
        public virtual DbSet<BriefingChecklist> BriefingChecklists { get; set; }
        public virtual DbSet<Briefing> Briefings { get; set; }
        public virtual DbSet<BriefingSignature> BriefingSignatures { get; set; }
        public virtual DbSet<BriefingType> BriefingTypes { get; set; }
        public virtual DbSet<CertificateDocument> CertificateDocuments { get; set; }
        public virtual DbSet<CertificateType> CertificateTypes { get; set; }
        public virtual DbSet<ChecklistQuestion> ChecklistQuestions { get; set; }
        public virtual DbSet<Checklist> Checklists { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ContractCostBuildUp> ContractCostBuildUps { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<ContractTenderChecklist> ContractTenderChecklists { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Depot> Depots { get; set; }
        public virtual DbSet<Drawing> Drawings { get; set; }
        public virtual DbSet<EmployeeCertificate> EmployeeCertificates { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeType> EmployeeTypes { get; set; }
        public virtual DbSet<Equipment> Equipments { get; set; }
        public virtual DbSet<EquipmentType> EquipmentTypes { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<JobArea> JobAreas { get; set; }
        public virtual DbSet<JobDrawing> JobDrawings { get; set; }
        public virtual DbSet<JobJobType> JobJobTypes { get; set; }
        public virtual DbSet<JobMethodStatement> JobMethodStatements { get; set; }
        public virtual DbSet<JobPackDocument> JobPackDocuments { get; set; }
        public virtual DbSet<JobPack> JobPacks { get; set; }
        public virtual DbSet<JobTypeChecklist> JobTypeChecklists { get; set; }
        public virtual DbSet<JobType> JobTypes { get; set; }
        public virtual DbSet<LoadingSheetEquipment> LoadingSheetEquipments { get; set; }
        public virtual DbSet<LoadingSheet> LoadingSheets { get; set; }
        public virtual DbSet<LoadingSheetSignature> LoadingSheetSignatures { get; set; }
        public virtual DbSet<LoadingSheetTemplateEquipment> LoadingSheetTemplateEquipments { get; set; }
        public virtual DbSet<LoadingSheetTemplate> LoadingSheetTemplates { get; set; }
        public virtual DbSet<MaintenanceCheckAnswer> MaintenanceCheckAnswers { get; set; }
        public virtual DbSet<MaintenanceCheckDetailPhoto> MaintenanceCheckDetailPhotos { get; set; }
        public virtual DbSet<MaintenanceCheckDetail> MaintenanceCheckDetails { get; set; }
        public virtual DbSet<MaintenanceCheck> MaintenanceChecks { get; set; }
        public virtual DbSet<MedicalExamination> MedicalExaminations { get; set; }
        public virtual DbSet<MedicalExaminationType> MedicalExaminationTypes { get; set; }
        public virtual DbSet<MethodStatement> MethodStatements { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<ObservationPhoto> ObservationPhotos { get; set; }
        public virtual DbSet<Observation> Observations { get; set; }
        public virtual DbSet<ScheduleEmployee> ScheduleEmployees { get; set; }
        public virtual DbSet<Schedule> Schedules { get; set; }
        public virtual DbSet<Scheme> Schemes { get; set; }
        public virtual DbSet<ShiftProgress> ShiftProgresses { get; set; }
        public virtual DbSet<ShiftProgressInfo> ShiftProgressInfoes { get; set; }
        public virtual DbSet<Shift> Shifts { get; set; }
        public virtual DbSet<ShiftStaff> ShiftStaffs { get; set; }
        public virtual DbSet<ShiftVehicle> ShiftVehicles { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<TaskChecklistAnswer> TaskChecklistAnswers { get; set; }
        public virtual DbSet<TaskChecklist> TaskChecklists { get; set; }
        public virtual DbSet<TaskPhoto> TaskPhotos { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskSignature> TaskSignatures { get; set; }
        public virtual DbSet<TaskType> TaskTypes { get; set; }
        public virtual DbSet<TimesheetRecord> TimesheetRecords { get; set; }
        public virtual DbSet<TimesheetRecordWorkInstruction> TimesheetRecordWorkInstructions { get; set; }
        public virtual DbSet<Timesheet> Timesheets { get; set; }
        public virtual DbSet<TimesheetSignature> TimesheetSignatures { get; set; }
        public virtual DbSet<ToolboxTalk> ToolboxTalks { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<VehicleChecklistDefectPhoto> VehicleChecklistDefectPhotos { get; set; }
        public virtual DbSet<VehicleChecklistDefect> VehicleChecklistDefects { get; set; }
        public virtual DbSet<VehicleChecklist> VehicleChecklists { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        public virtual DbSet<VehicleTypeChecklist> VehicleTypeChecklists { get; set; }
        public virtual DbSet<VehicleType> VehicleTypes { get; set; }
        public virtual DbSet<WorkInstructionDrawing> WorkInstructionDrawings { get; set; }
        public virtual DbSet<WorkInstructionMethodStatement> WorkInstructionMethodStatements { get; set; }
        public virtual DbSet<WorkInstructionSignature> WorkInstructionSignatures { get; set; }
        public virtual DbSet<WorkInstructionToolboxTalk> WorkInstructionToolboxTalks { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<EquipmentTypeVmsOrAsset> EquipmentTypeVmsOrAssets { get; set; }
        public virtual DbSet<LoadingSheetEquipmentItem> LoadingSheetEquipmentItems { get; set; }
        public virtual DbSet<EquipmentItemChecklist> EquipmentItemChecklists { get; set; }
        public virtual DbSet<EquipmentItemChecklistAnswer> EquipmentItemChecklistAnswers { get; set; }
        public virtual DbSet<TaskEquipment> TaskEquipments { get; set; }
        public virtual DbSet<WorkInstruction> WorkInstructions { get; set; }
        public virtual DbSet<TaskEquipmentAsset> TaskEquipmentAssets { get; set; }
        public virtual DbSet<TaskChecklistAnswerPhoto> TaskChecklistAnswerPhotos { get; set; }
        public virtual DbSet<WeatherCondition> WeatherConditions { get; set; }
        public virtual DbSet<ReasonCode> ReasonCodes { get; set; }
        public virtual DbSet<VehicleChecklistAnswer> VehicleChecklistAnswers { get; set; }
        public virtual DbSet<DepotArea> DepotAreas { get; set; }
        public virtual DbSet<EmployeeAbsenceReason> EmployeeAbsenceReasons { get; set; }
        public virtual DbSet<EmployeeAbsence> EmployeeAbsences { get; set; }
        public virtual DbSet<JobCertificate> JobCertificates { get; set; }
        public virtual DbSet<JobPriceLine> JobPriceLines { get; set; }
        public virtual DbSet<JobSpeed> JobSpeeds { get; set; }
        public virtual DbSet<JobToolboxTalk> JobToolboxTalks { get; set; }
        public virtual DbSet<PriceLine> PriceLines { get; set; }
        public virtual DbSet<ScheduleVehicle> ScheduleVehicles { get; set; }
        public virtual DbSet<VehicleAbsenceReason> VehicleAbsenceReasons { get; set; }
        public virtual DbSet<VehicleAbsence> VehicleAbsences { get; set; }
        public virtual DbSet<WorkInstructionPriceLine> WorkInstructionPriceLines { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<ContractType> ContractTypes { get; set; }
        public virtual DbSet<ActivityPriority> ActivityPriorities { get; set; }
        public virtual DbSet<Direction> Directions { get; set; }
        public virtual DbSet<Lane> Lanes { get; set; }
        public virtual DbSet<DisposalSite> DisposalSites { get; set; }
        public virtual DbSet<UnloadingProcess> UnloadingProcesses { get; set; }
        public virtual DbSet<UnloadingProcessPhoto> UnloadingProcessPhotos { get; set; }
        public virtual DbSet<CleansingLog> CleansingLogs { get; set; }
        public virtual DbSet<CleansingLogPhoto> CleansingLogPhotos { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<OutOfHour> OutOfHours { get; set; }
        public virtual DbSet<WorkInstructionsView> WorkInstructionsViews { get; set; }
        public virtual DbSet<RoadSpeed> RoadSpeeds { get; set; }
        public virtual DbSet<TMRequirement> TMRequirements { get; set; }
        public virtual DbSet<ActivityRiskAssessment> ActivityRiskAssessments { get; set; }
    }
}
