//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mobile
{
    using System;
    using System.Collections.Generic;
    
    public partial class ToolboxTalk
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ToolboxTalk()
        {
            this.WorkInstructionToolboxTalks = new HashSet<WorkInstructionToolboxTalk>();
            this.JobToolboxTalks = new HashSet<JobToolboxTalk>();
        }
    
        public System.Guid ToolboxTalkPK { get; set; }
        public string ToolboxTalk1 { get; set; }
        public string Revision { get; set; }
        public string Body { get; set; }
        public bool IsActive { get; set; }
        public System.Guid CreatedByFK { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.Guid UpdatedByFK { get; set; }
        public System.DateTime UpdatedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkInstructionToolboxTalk> WorkInstructionToolboxTalks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobToolboxTalk> JobToolboxTalks { get; set; }
    }
}
