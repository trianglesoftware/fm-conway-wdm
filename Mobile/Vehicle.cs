//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mobile
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vehicle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vehicle()
        {
            this.ShiftVehicles = new HashSet<ShiftVehicle>();
            this.VehicleChecklists = new HashSet<VehicleChecklist>();
            this.Employees = new HashSet<Employee>();
            this.ScheduleVehicles = new HashSet<ScheduleVehicle>();
            this.VehicleAbsences = new HashSet<VehicleAbsence>();
        }
    
        public System.Guid VehiclePK { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Registration { get; set; }
        public System.Guid VehicleTypeFK { get; set; }
        public System.Guid DepotFK { get; set; }
        public bool IsActive { get; set; }
        public System.Guid CreatedByFK { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.Guid UpdatedByFK { get; set; }
        public System.DateTime UpdatedDate { get; set; }
    
        public virtual Depot Depot { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShiftVehicle> ShiftVehicles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleChecklist> VehicleChecklists { get; set; }
        public virtual VehicleType VehicleType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employee> Employees { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScheduleVehicle> ScheduleVehicles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleAbsence> VehicleAbsences { get; set; }
    }
}
