﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Extensions
{
    public static class StringExtensions
    {
        // allows for case insensitive contains (the standard contains is case sensitive)
        public static bool Contains(this string source, string text, StringComparison stringComparison)
        {
            return source?.IndexOf(text, stringComparison) >= 0;
        }
    }
}