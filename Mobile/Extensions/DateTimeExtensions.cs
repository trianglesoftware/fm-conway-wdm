﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Extensions
{
    public static class DateTimeExtensions
    {
        // closest day (startOfWeek) to date (dt)
        // eg dt = DateTime.Today, startofWeek = Monday (returns last mondays date)
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static DateTime TruncateMilliseconds(this DateTime d)
        {
            return d.Date.AddHours(d.Hour).AddMinutes(d.Minute).AddSeconds(d.Second);
        }
    }
}