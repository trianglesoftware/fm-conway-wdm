﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Extensions
{
    public static class DictionaryExtensions
    {
        public static void AddSafely(this System.Collections.IDictionary dictionary, object key, object value)
        {
            try
            {
                dictionary.Add(key, value);
            }
            catch (Exception ex)
            {
                try
                {
                    if (!typeof(System.Collections.IEnumerable).IsAssignableFrom(value.GetType()))
                    {
                        // try wrapping the object inside a dictionary if not already an enumerable, this can help serialization
                        dictionary.Add(key, new Dictionary<string, object>
                        {
                            { nameof(value), value },
                        });
                    }
                    else
                    {
                        // if object can't be serialized, add the reason why (message).
                        dictionary.Add(key, "Error adding parameter - " + ex.Message);
                    }
                }
                catch (Exception)
                {
                    try
                    {
                        // don't propagate exception as it's important that the original exception is saved over serializing any parameters
                        dictionary.Add(key, "Error adding parameter (p2) - " + ex.Message);
                    }
                    catch (Exception)
                    {
                        // ignore
                    }
                }
            }
        }
    }
}