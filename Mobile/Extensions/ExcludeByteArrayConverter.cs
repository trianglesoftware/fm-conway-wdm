﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;

namespace Mobile.Extensions
{
    public class ExcludeByteArrayConverter : JavaScriptConverter
    {
        public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            var properties = new Dictionary<string, object>();
            foreach (var prop in obj.GetType().GetProperties())
            {
                // ignore byte[] and any property which contains the name "data"
                // some byte arrays are stored as base64 text in string properties with various names usually containing the keyword "data".
                if (prop.PropertyType == typeof(byte[]) || prop.Name.Contains("data", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                var value = prop.GetValue(obj, BindingFlags.Public, null, null, null);
                properties.Add(prop.Name, value);
            }

            return properties;
        }

        public override IEnumerable<Type> SupportedTypes
        {
            get
            {
                return GetType().Assembly.GetTypes();
            }
        }
    }
}