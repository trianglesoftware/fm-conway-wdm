﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class RoadSpeedModel
    {
        [DataMember]
        public Guid? RoadSpeedID { get; set; }

        [DataMember]
        public string Speed { get; set; }

        [DataMember]
        public bool IsActive { get; set; }
    }
}