﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class AccidentModel
    {
        [DataMember]
        public Guid AccidentID { get; set; }

        [DataMember]
        public Guid? ShiftID { get; set; }

        [DataMember]
        public string AccidentDetails { get; set; }

        [DataMember]
        public DateTime AccidentDate { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string Registrations { get; set; }

        [DataMember]
        public string PersonReporting { get; set; }

        [DataMember]
        public string PeopleInvolved { get; set; }

        [DataMember]
        public string Witnesses { get; set; }

        [DataMember]
        public string ActionTaken { get; set; }

        [DataMember]
        public bool IsNew { get; set; }

        [DataMember]
        public IEnumerable<AccidentPhotoModel> Photos { get; set; }
    }
}