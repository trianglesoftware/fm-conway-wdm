﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class BriefingTypeModel
    {
        [DataMember]
        public int BriefingTypeID { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}