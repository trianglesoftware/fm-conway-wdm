﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    public class ShiftProgressStartedModel : ShiftProgressModel
    {
        [DataMember]
        public DateTime? ShiftUpdatedDate { get; set; }
    }
}