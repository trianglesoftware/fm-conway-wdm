﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class EquipmentItemChecklistAnswerModel
    {
        [DataMember]
        public Guid EquipmentItemChecklistAnswerID { get; set; }

        [DataMember]
        public Guid EquipmentItemChecklistID { get; set; }

        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public bool Answer { get; set; }

        [DataMember]
        public string Reason { get; set; }
    }
}