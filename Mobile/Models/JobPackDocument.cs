﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class JobPackDocumentModel
    {
        [DataMember]
        public Guid JobPackDocumentID { get; set; }

        [DataMember]
        public string Data { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public string DocumentName { get; set; }

        [DataMember]
        public Guid JobPackID { get; set; }

        [DataMember]
        public Guid BriefingID { get; set; }
    }
}