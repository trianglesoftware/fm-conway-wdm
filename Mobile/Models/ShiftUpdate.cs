﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ShiftUpdateModel
    {
        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public bool ShiftComplete { get; set; }

        [DataMember]
        public bool IsCancelled { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public DateTime ShiftDate { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }

        [DataMember]
        public int OrderNumber { get; set; }

        [DataMember]
        public IEnumerable<ShiftUpdateStaff> ShiftStaffs { get; set; }

        [DataMember]
        public IEnumerable<ShiftVehicleModel> ShiftVehicles { get; set; }

        [DataMember]
        public IEnumerable<VehicleChecklistModel> VehicleChecklists { get; set; }
    }
}