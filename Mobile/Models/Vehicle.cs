﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class VehicleModel
    {
        [DataMember]
        public Guid VehicleID { get; set; }

        [DataMember]
        public Guid StaffID { get; set; }

        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public string Registration { get; set; }

        [DataMember]
        public Guid DepotID { get; set; }

        [DataMember]
        public Guid VehicleTypeID { get; set; }
    }
}