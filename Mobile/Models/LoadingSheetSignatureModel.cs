﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class LoadingSheetSignatureModel
    {
        [DataMember]
        public Guid LoadingSheetSignatureID { get; set; }

        [DataMember]
        public Guid LoadingSheetID { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public string DataString { get; set; }

        [DataMember]
        public DateTime Time { get; set; }
    }
}