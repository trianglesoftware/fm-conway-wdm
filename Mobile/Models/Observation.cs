﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ObservationModel
    {
        [DataMember]
        public Guid ObservationID { get; set; }

        [DataMember]
        public Guid? ShiftID { get; set; }

        [DataMember]
        public string ObservationDescription { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string PeopleInvolved { get; set; }

        [DataMember]
        public string CauseOfProblem { get; set; }

        [DataMember]
        public Guid ObservationTypeID { get; set; }

        [DataMember]
        public DateTime ObservationDate { get; set; }
        
        [DataMember]
        public bool IsNew { get; set; }

        [DataMember]
        public IEnumerable<ObservationPhotoModel> Photos { get; set; }
    }
}