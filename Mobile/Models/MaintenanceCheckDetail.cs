﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class MaintenanceCheckDetailModel
    {
        [DataMember]
        public Guid MaintenanceCheckID { get; set; }

        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public Guid MaintenanceCheckDetailID { get; set; }

        [DataMember]
        public string Description { get; set; }

    }
}