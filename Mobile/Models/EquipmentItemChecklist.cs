﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class EquipmentItemChecklistModel
    {
        [DataMember]
        public Guid EquipmentItemChecklistID { get; set; }

        [DataMember]
        public Guid LoadingSheetEquipmentItemID { get; set; }

        [DataMember]
        public Guid ChecklistID { get; set; }
    }
}