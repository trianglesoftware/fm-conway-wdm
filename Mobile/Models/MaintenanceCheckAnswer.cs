﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]    
    public class MaintenanceCheckAnswerModel
    {
        [DataMember]
        public Guid MaintenanceCheckID { get; set; }

        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public bool Answer { get; set; }
    }
}