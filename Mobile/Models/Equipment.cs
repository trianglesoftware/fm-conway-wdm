﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class EquipmentModel
    {
        [DataMember]
        public Guid? EquipmentID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Guid? EquipmentTypeID { get; set; }
    }
}