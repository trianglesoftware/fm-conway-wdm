﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class ActivityDetailModel
    {
        [DataMember]
        public Guid ActivityID { get; set; }

        [DataMember]
        public string SiteName { get; set; }

        [DataMember]
        public string ClientReferenceNumber { get; set; }

        [DataMember]
        public Guid? ActivityPriorityID { get; set; }

        [DataMember]
        public string Operator { get; set; }
    }
}