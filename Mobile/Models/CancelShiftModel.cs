﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    public class CancelShiftModel
    {
        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public string CancellationReason { get; set; }
    }
}