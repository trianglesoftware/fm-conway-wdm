﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class VehicleChecklistAnswerModel
    {
        [DataMember]
        public Guid VehicleChecklistID { get; set; }

        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public int Answer { get; set; }

    }
}