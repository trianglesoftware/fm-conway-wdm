﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    public class BriefingSignatureModel
    {
        [DataMember]
        public Guid BriefingID { get; set; }

        [DataMember]
        public Guid StaffID { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public string DataString { get; set; }

        [DataMember]
        public DateTime BriefingStart { get; set; }

        [DataMember]
        public DateTime BriefingEnd { get; set; }
    }
}