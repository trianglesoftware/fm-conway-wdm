﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class StaffRecordModel
    {
        [DataMember]
        public Guid StaffRecordID { get; set; }

        [DataMember]
        public string Data { get; set; }

        [DataMember]
        public byte[] DataArray { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public string RecordName { get; set; }

        [DataMember]
        public Guid StaffID { get; set; }
    }
}