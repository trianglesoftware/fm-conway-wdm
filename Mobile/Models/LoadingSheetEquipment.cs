﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class LoadingSheetEquipmentModel
    {
        [DataMember]
        public Guid LoadingSheetEquipmentID { get; set; }

        [DataMember]
        public Guid LoadingSheetID { get; set; }

        [DataMember]
        public Guid? EquipmentID { get; set; }

        [DataMember]
        public Guid? EquipmentTypeID { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public int SheetOrder { get; set; }

        [DataMember]
        public string VmsOrAsset { get; set; }
    }
}