﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class VehicleChecklistAnswerDefectPhotoModel
    {
        [DataMember]
        public Guid VehicleChecklistDefectPhotoID { get; set; }

        [DataMember]
        public Guid VehicleID { get; set; }

        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public Guid ChecklistID { get; set; }

        [DataMember]
        public Guid VehicleChecklistDefectID { get; set; }

        [DataMember]
        public Guid VehicleChecklistID { get; set; }

        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public string DataString { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public string Filename { get; set; }
    }
}