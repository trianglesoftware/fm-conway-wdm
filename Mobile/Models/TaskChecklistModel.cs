﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace Mobile.Models
{
    [DataContract]
    public class TaskChecklistModel
    {
        [DataMember]
        public Guid TaskID { get; set; }

        [DataMember]
        public Guid ChecklistID { get; set; }

        [DataMember]
        public IEnumerable<TaskChecklistAnswerModel> Answers { get; set; }
    }
}