﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class PushEquipmentItemModel
    {
        [DataMember]
        public List<LoadingSheetModel> LoadingSheets { get; set; }

        [DataMember]
        public List<LoadingSheetEquipmentModel> LoadingSheetEquipments { get; set; }

        [DataMember]
        public List<LoadingSheetEquipmentItemModel> LoadingSheetEquipmentItems { get; set; }

        [DataMember]
        public List<EquipmentItemChecklistModel> EquipmentItemChecklists { get; set; }

        [DataMember]
        public List<EquipmentItemChecklistAnswerModel> EquipmentItemChecklistAnswers { get; set; }
    }
}