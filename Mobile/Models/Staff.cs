﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class StaffModel
    {
        [DataMember]
        public Guid StaffID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Guid DepotID { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public bool IsAgency { get; set; }
    }
}