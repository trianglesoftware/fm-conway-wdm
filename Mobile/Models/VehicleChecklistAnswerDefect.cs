﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class VehicleChecklistAnswerDefectModel
    {
        [DataMember]
        public Guid VehicleChecklistID { get; set; }

        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public Guid VehicleChecklistDefectID { get; set; }

        [DataMember]
        public string Defect { get; set; }

        [DataMember]
        public List<VehicleChecklistAnswerDefectPhotoModel> Photos { get; set; }
    }
}