﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class MethodStatementModel
    {
        [DataMember]
        public Guid MethodStatementID { get; set; }

        [DataMember]
        public Guid? DocumentID { get; set; }

        [DataMember]
        public string MethodStatementTitle { get; set; }

        [DataMember]
        public string DocumentName { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public bool IsActive { get; set; }
    }
}