﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    public class MaintenanceCheckModel
    {
        [DataMember]
        public Guid MaintenanceCheckID { get; set; }

        [DataMember]
        public Guid ActivityID { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public IEnumerable<MaintenanceCheckAnswerModel> Answers { get; set; }

        [DataMember]
        public IEnumerable<MaintenanceCheckDetailModel> Details { get; set; }
    }
}