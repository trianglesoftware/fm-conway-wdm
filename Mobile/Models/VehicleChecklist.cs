﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class VehicleChecklistModel
    {
        [DataMember]
        public Guid VehicleChecklistID { get; set; }

        [DataMember]
        public Guid VehicleID { get; set; }

        [DataMember]
        public Guid ChecklistID { get; set; }

        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool StartOfShift { get; set; }

        [DataMember]
        public string ImageData { get; set; }

        [DataMember]
        public string ImageFilename { get; set; }

        [DataMember]
        public IEnumerable<VehicleChecklistAnswerModel> Answers { get; set; }

        [DataMember]
        public IEnumerable<VehicleChecklistAnswerDefectModel> Defects { get; set; }
    }
}