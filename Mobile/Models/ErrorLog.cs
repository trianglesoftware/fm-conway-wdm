﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class ErrorLogModel
    {
        [DataMember]
        public Guid? LogPK { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string Exception { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}