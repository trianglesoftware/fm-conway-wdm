﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Models
{
    public class UserProfileModel
    {
        public Guid UserID { get; set; }
        public Guid? StaffID { get; set; }
        public string AppVersion { get; set; }
    }
}