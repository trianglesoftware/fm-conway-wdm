﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    public class TaskEquipmentBaseModel
    {
        [DataMember]
        public Guid TaskID { get; set; }

        [DataMember]
        public IEnumerable<TaskEquipmentModel> TaskEquipments { get; set; }
    }
}