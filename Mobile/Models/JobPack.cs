﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class JobPackModel
    {
        [DataMember]
        public Guid JobPackID { get; set; }

        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public string JobPackName { get; set; }

        [DataMember]
        public IEnumerable<BriefingModel> Briefings { get; set; }

        [DataMember]
        public IEnumerable<JobPackDocumentModel> Documents { get; set; }

        [DataMember]
        public IEnumerable<ActivityModel> Activities { get; set; }
    }
}