﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class AreaModel
    {
        [DataMember]
        public Guid AreaID { get; set; }

        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Details { get; set; }

        [DataMember]
        public string CallProtocol { get; set; }

        [DataMember]
        public string PersonsName { get; set; }

        [DataMember]
        public string Reference { get; set; }
    }
}