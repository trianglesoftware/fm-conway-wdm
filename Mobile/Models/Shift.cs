﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ShiftModel
    {
        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public bool DayShift { get; set; }

        [DataMember]
        public Guid UserID { get; set; }

        [DataMember]
        public DateTime ShiftDate { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }

        [DataMember]
        public int ShiftCompletionStatus { get; set; }

        [DataMember]
        public bool IsCancelled { get; set; }

        [DataMember]
        public string ShiftDetails { get; set; }

        [DataMember]
        public string JobTypes { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string TrafficManagementActivities { get; set; }

        [DataMember]
        public string SRWNumber { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string Borough { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string County { get; set; }

        [DataMember]
        public string Postcode { get; set; }

        [DataMember]
        public string CustomerProjCode { get; set; }

        [DataMember]
        public IEnumerable<ShiftStaffModel> Staff { get; set; }

        [DataMember]
        public IEnumerable<AccidentModel> Accidents { get; set; }

        [DataMember]
        public IEnumerable<ObservationModel> Observations { get; set; }

        [DataMember]
        public IEnumerable<NoteModel> Notes { get; set; }

        [DataMember]
        public IEnumerable<VehicleModel> Vehicles { get; set; }

        [DataMember]
        public IEnumerable<ShiftVehicleModel> ShiftVehicles { get; set; }

        [DataMember]
        public IEnumerable<ShiftProgressModel> ShiftProgress { get; set; }
    }
}