﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class FileModel
    {
        [DataMember]
        public Guid FileID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string MediaType { get; set; }

        [DataMember]
        [Obsolete]
        public byte[] Data { get; set; }

        [DataMember]
        public string DataString { get; set; }
    }
}