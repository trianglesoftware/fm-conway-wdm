﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class LoadingSheetEquipmentItemModel
    {
        [DataMember]
        public Guid LoadingSheetEquipmentItemID { get; set; }

        [DataMember]
        public Guid LoadingSheetEquipmentID { get; set; }

        [DataMember]
        public int ItemNumber { get; set; }

        [DataMember]
        public Guid ChecklistID { get; set; }
    }
}