﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class ChecklistQuestionModel
    {
        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public string Question { get; set; }

        [DataMember]
        public int QuestionOrder { get; set; }

        [DataMember]
        public Guid ChecklistID { get; set; }
    }
}