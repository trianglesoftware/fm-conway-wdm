﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ShiftProgressResult
    {
        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public string Progress { get; set; }

        [DataMember]
        public bool Saved { get; set; }
    }
}