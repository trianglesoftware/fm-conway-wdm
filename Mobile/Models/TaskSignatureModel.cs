﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class TaskSignatureModel
    {
        [DataMember]
        public Guid TaskSignatureID { get; set; }

        [DataMember]
        public Guid TaskID { get; set; }

        [DataMember]
        public Guid StaffID { get; set; }

        [DataMember]
        public bool IsClient { get; set; }

        [DataMember]
        public string ClientVehicleReg { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public string DataString { get; set; }

        [DataMember]
        public string PrintedName { get; set; }
    }
}