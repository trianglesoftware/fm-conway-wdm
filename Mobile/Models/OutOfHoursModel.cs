﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class OutOfHoursModel
    {
        public OutOfHoursModel()
        {
            Briefings = new List<OutOfHoursBriefingModel>();
            MethodStatements = new List<OutOfHoursMethodStatementModel>();
            ShiftStaffs = new List<OutOfHoursShiftStaffModel>();
            ShiftVehicles = new List<OutOfHoursShiftVehicleModel>();
            Tasks = new List<OutOfHoursTaskModel>();
        }

        [DataMember]
        public Guid ActivityID { get; set; }

        [DataMember]
        public Guid JobTypeID { get; set; }

        [DataMember]
        public Guid JobPackID { get; set; }

        [DataMember]
        public Guid DepotID { get; set; }

        [DataMember]
        public Guid CustomerID { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime FinishDate { get; set; }

        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string Borough { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string County { get; set; }

        [DataMember]
        public string Postcode { get; set; }

        [DataMember]
        public List<OutOfHoursBriefingModel> Briefings { get; set; }

        [DataMember]
        public List<OutOfHoursMethodStatementModel> MethodStatements { get; set; }

        [DataMember]
        public List<OutOfHoursShiftStaffModel> ShiftStaffs { get; set; }

        [DataMember]
        public List<OutOfHoursShiftVehicleModel> ShiftVehicles { get; set; }

        [DataMember]
        public List<OutOfHoursTaskModel> Tasks { get; set; }

    }

    public class OutOfHoursBriefingModel
    {
        [DataMember]
        public Guid BriefingID { get; set; }

        [DataMember]
        public int BriefingTypeID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Guid FileID { get; set; }
    }

    public class OutOfHoursMethodStatementModel
    {
        [DataMember]
        public Guid ActivityMethodStatementID { get; set; }

        [DataMember]
        public Guid MethodStatementID { get; set; }
    }

    public class OutOfHoursShiftStaffModel
    {
        [DataMember]
        public Guid EmployeeID { get; set; }
    }

    public class OutOfHoursShiftVehicleModel
    {
        [DataMember]
        public Guid VehicleID { get; set; }
    }

    public class OutOfHoursTaskModel
    {
        [DataMember]
        public Guid TaskID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int TaskTypeID { get; set; }

        [DataMember]
        public int TaskOrder { get; set; }

        [DataMember]
        public Guid? ChecklistID { get; set; }

        [DataMember]
        public bool PhotosRequired { get; set; }

        [DataMember]
        public bool ShowDetails { get; set; }

        [DataMember]
        public bool ShowClientSignature { get; set; }

        [DataMember]
        public bool ClientSignatureRequired { get; set; }

        [DataMember]
        public bool ShowStaffSignature { get; set; }

        [DataMember]
        public bool StaffSignatureRequired { get; set; }

        [DataMember]
        public string Tag { get; set; }

        [DataMember]
        public bool IsMandatory { get; set; }
    }

    [DataContract]
    public class OutOfHoursPhotoModel
    {
        [DataMember]
        public Guid OutOfHours { get; set; }
        [DataMember]
        public Guid OutOfHoursID { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public string DataString { get; set; }

        [DataMember]
        public DateTime Time { get; set; }
    }

    [DataContract]
    public class OutOfHoursChecklistModel
    {
        [DataMember]
        public Guid OOHChecklistID { get; set; }
        [DataMember]
        public Guid OutOfHoursID { get; set; }
        [DataMember]
        public Guid ChecklistID { get; set; }
        [DataMember]
        public IEnumerable<OutOfHoursChecklistAnswerModel> Answers { get; set; }
    }

    [DataContract]
    public class OutOfHoursChecklistAnswerModel
    {
        [DataMember]
        public Guid OOHChecklistAnswerID { get; set; }
        [DataMember]
        public Guid OOHChecklistID { get; set; }
        [DataMember]
        public Guid ChecklistQuestionID { get; set; }
        [DataMember]
        public int Answer { get; set; }
        [DataMember]
        public string Reason { get; set; }
        [DataMember]
        public DateTime Time { get; set; }
        [DataMember]
        public IEnumerable<OutOfHoursChecklistAnswerPhotoModel> Photos { get; set; }
    }

    [DataContract]
    public class OutOfHoursChecklistAnswerPhotoModel
    {
        [DataMember]
        public Guid OOHChecklistAnswerID { get; set; }
        [DataMember]
        public Guid OOHChecklistAnswerPhotoID { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public string DataString { get; set; }

        [DataMember]
        public DateTime Time { get; set; }
    }
}