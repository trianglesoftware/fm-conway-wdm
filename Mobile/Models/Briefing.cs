﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract] 
    public class BriefingModel
    {
        [DataMember]
        public Guid BriefingID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Details { get; set; }

        [DataMember]
        public int BriefingTypeID { get; set; }

        [DataMember]
        public Guid JobPackID { get; set; }

        [DataMember]
        public Guid? FileID { get; set; }

        [DataMember]
        public string DocumentName { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public IEnumerable<ChecklistModel> Checklists { get; set; }
    }
}