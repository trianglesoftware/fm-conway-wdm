﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class ServiceResult<T>
    {
        [DataMember]
        public T Result { get; set; }

        public ServiceResult(T result)
        {
            Result = result;
        }
    }
}