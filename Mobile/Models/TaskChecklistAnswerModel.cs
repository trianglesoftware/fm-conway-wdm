﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    public class TaskChecklistAnswerModel
    {
        [DataMember]
        public Guid TaskChecklistAnswerID { get; set; }

        [DataMember]
        public Guid TaskChecklistID { get; set; }

        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public int Answer { get; set; }

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public DateTime Time { get; set; }
    }
}