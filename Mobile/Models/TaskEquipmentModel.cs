﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    public class TaskEquipmentModel
    {
        [DataMember]
        public Guid TaskEquipmentID { get; set; }

        [DataMember]
        public Guid TaskID { get; set; }

        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public Guid EquipmentID { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public IEnumerable<TaskEquipmentAssetModel> TaskEquipmentAssets { get; set; }
    }
}