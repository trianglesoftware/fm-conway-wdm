﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class UnloadingProcessModel
    {
        [DataMember]
        public Guid UnloadingProcessID { get; set; }

        [DataMember]
        public Guid TaskID { get; set; }

        [DataMember]
        public Guid ActivityID { get; set; }

        [DataMember]
        public DateTime? DepartToDisposalSiteDate { get; set; }

        [DataMember]
        public DateTime? ArriveAtDisposalSiteDate { get; set; }

        [DataMember]
        public Guid? DisposalSiteID { get; set; }

        [DataMember]
        public string WasteTicketNumber { get; set; }

        [DataMember]
        public decimal? VolumeOfWasteDisposed { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public DateTime? LeaveDisposalSiteDate { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}