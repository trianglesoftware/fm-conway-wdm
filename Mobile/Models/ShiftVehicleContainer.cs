﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ShiftVehicleContainerModel
    {
        [DataMember]
        public bool RestrictedUpdate { get; set; }

        [DataMember]
        public IEnumerable<ShiftVehicleModel> ShiftVehicles { get; set; }
    }
}