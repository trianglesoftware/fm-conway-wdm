﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ActivityMethodStatementModel
    {
        [DataMember]
        public Guid ActivityMethodStatementID { get; set; }

        [DataMember]
        public Guid ActivityID { get; set; }

        [DataMember]
        public Guid MethodStatementID { get; set; }

        [DataMember]
        public bool IsActive { get; set; }
    }
}