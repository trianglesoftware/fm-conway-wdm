﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class LoadingSheetModel
    {
        [DataMember]
        public Guid LoadingSheetID { get; set; }

        [DataMember]
        public Guid VehicleID { get; set; }

        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public Guid ActivityID { get; set; }

        [DataMember]
        public IEnumerable<LoadingSheetEquipmentModel> LoadingSheetEquipment { get; set; }
    }
}