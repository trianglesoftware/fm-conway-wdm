﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class VehicleTypeModel
    {
        [DataMember]
        public Guid VehicleTypeID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public IEnumerable<ChecklistModel> Checklists { get; set; }
    }
}