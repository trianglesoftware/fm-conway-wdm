﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class ObservationTypeModel
    {
        [DataMember]
        public Guid ObservationTypeID { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}