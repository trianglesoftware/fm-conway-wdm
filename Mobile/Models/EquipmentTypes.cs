﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class EquipmentTypeModel
    {
        [DataMember]
        public Guid EquipmentTypeID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string VmsOrAsset { get; set; }
    }
}