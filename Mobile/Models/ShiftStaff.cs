﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ShiftStaffModel
    {
        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public Guid StaffID { get; set; }

        [DataMember]
        public string ReasonNotOn { get; set; }

        [DataMember]
        public bool NotOnShift { get; set; }

        [DataMember]
        public string AgencyEmployeeName { get; set; }

        [DataMember]
        public DateTime? StartTime { get; set; }

        [DataMember]
        public DateTime? EndTime { get; set; }

        [DataMember]
        public bool FirstAider { get; set; }

        [DataMember]
        public int WorkedHours { get; set; }

        [DataMember]
        public int WorkedMins { get; set; }

        [DataMember]
        public int BreakHours { get; set; }

        [DataMember]
        public int BreakMins { get; set; }

        [DataMember]
        public int TravelHours { get; set; }

        [DataMember]
        public int TravelMins { get; set; }

        [DataMember]
        public bool IPV { get; set; }

        [DataMember]
        public bool OnCall { get; set; }

        [DataMember]
        public bool PaidBreak { get; set; }

        [DataMember]
        public List<StaffRecordModel> StaffRecords { get; set; }
    }
}