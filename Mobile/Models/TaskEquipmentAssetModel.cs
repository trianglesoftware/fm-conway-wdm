﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    public class TaskEquipmentAssetModel
    {
        [DataMember]
        public Guid TaskEquipmentAssetID { get; set; }

        [DataMember]
        public Guid TaskEquipmentID { get; set; }

        [DataMember]
        public int ItemNumber { get; set; }

        [DataMember]
        public string AssetNumber { get; set; }
    }
}