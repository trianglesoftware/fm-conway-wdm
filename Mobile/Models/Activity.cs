﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ActivityModel
    {
        public ActivityModel()
        {
            Contacts = new List<ContactModel>();
        }

        [DataMember]
        public Guid ActivityID { get; set; } // aka WorksInstructionPK

        [DataMember]
        public Guid JobPackID { get; set; }

        [DataMember]
        public Guid ActivityTypeID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Customer { get; set; }

        [DataMember]
        public string CustomerPhoneNumber { get; set; }

        [DataMember]
        public string Contract { get; set; }

        [DataMember]
        public string ContractNumber { get; set; }

        [DataMember]
        public string Scheme { get; set; }

        [DataMember]
        public string ScopeOfWork { get; set; }

        [DataMember]
        public string FirstCone { get; set; }

        [DataMember]
        public DateTime? FirstConeDate { get; set; }

        [DataMember]
        public string LastCone { get; set; }

        [DataMember]
        public DateTime? LastConeDate { get; set; }

        [DataMember]
        public int WorksInstructionNumber { get; set; }

        [DataMember]
        public string WorksInstructionComments { get; set; }

        [DataMember]
        public decimal Distance { get; set; }

        [DataMember]
        public string Road { get; set; }

        [DataMember]
        public string CWayDirection { get; set; }

        [DataMember]
        public DateTime InstallationTime { get; set; }

        [DataMember]
        public string HealthAndSafety { get; set; }

        [DataMember]
        public DateTime RemovalTime { get; set; }

        [DataMember]
        public string AreaCallNumber { get; set; }

        [DataMember]
        public string AreaCallProtocol { get; set; }

        [DataMember]
        public int MaintenanceChecklistReminderInMinutes { get; set; }

        [DataMember]
        public string ClientReferenceNumber { get; set; }

        [DataMember]
        public Guid CustomerID { get; set; }

        [DataMember]
        public bool IsOutOfHours { get; set; }

        [DataMember]
        public DateTime WorkInstructionStartDate { get; set; }

        [DataMember]
        public DateTime WorkInstructionFinishDate { get; set; }

        [DataMember]
        public Guid DepotID { get; set; }

        [DataMember]
        public int OrderNumber { get; set; }

        [DataMember]
        public IEnumerable<MaintenanceCheckModel> MaintenanceChecks { get; set; }

        [DataMember]
        public IEnumerable<TaskModel> Tasks { get; set; }

        [DataMember]
        public IEnumerable<ActivityMethodStatementModel> MethodStatements { get; set; }

        // this Contacts property is now obsolete but had to be left in, in order for previous version of the app to communicate with mobile service
        // once app has been released to all devices, remove this property in the next release
        [DataMember]
        public IEnumerable<ContactModel> Contacts { get; set; }
    }

}