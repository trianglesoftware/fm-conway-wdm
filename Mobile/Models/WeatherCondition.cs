﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class WeatherConditionModel
    {
        [DataMember]
        public Guid WeatherConditionID { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}