﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Models
{
    public class ActivityRiskAssessmentModel
    {
        public Guid ActivityRiskAssessmentID { get; set; }
        public Guid TaskID { get; set; }
        public Guid CleansingLogID { get; set; }
        public string RoadName { get; set; }
        public Guid RoadSpeedID { get; set; }
        public Guid TMRequirementID { get; set; }
        public bool Schools { get; set; }
        public bool PedestrianCrossings { get; set; }
        public bool TreeCanopies { get; set; }
        public bool WaterCourses { get; set; }
        public bool OverheadCables { get; set; }
        public bool AdverseWeather { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}