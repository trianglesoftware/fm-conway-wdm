﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class MaintenanceCheckDetailPhotoModel
    {
        [DataMember]
        public Guid MaintenanceCheckDetailID { get; set; }

        [DataMember]
        public Guid MaintenanceCheckID { get; set; }

        [DataMember]
        public Guid ChecklistQuestionID { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public string DataString { get; set; }
    }
}