﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class UserModel
    {
        [DataMember]
        public Guid UserID { get; set; }

        [DataMember]
        public Guid UserTypeID { get; set; }

        [DataMember]
        public Guid DepotID { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string EmployeeType { get; set; }

    }
}