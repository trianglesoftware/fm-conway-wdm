﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class DepotModel
    {
        [DataMember]
        public Guid DepotID { get; set; }

        [DataMember]
        public string DepotName { get; set; }

        [DataMember]
        public bool IsActive { get; set; }
    }
}