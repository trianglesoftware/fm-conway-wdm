﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class NoteModel
    {
        [DataMember]
        public Guid NoteID { get; set; }

        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public string NoteDetails { get; set; }

        [DataMember]
        public DateTime NoteDate { get; set; }

        [DataMember]
        public bool IsNew { get; set; }
    }
}