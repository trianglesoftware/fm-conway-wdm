﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ShiftVehicleModel
    {
        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public Guid VehicleID { get; set; }

        [DataMember]
        public Guid? StaffID { get; set; }

        [DataMember]
        public int StartMileage { get; set; }

        [DataMember]
        public int EndMileage { get; set; }

        [DataMember]
        public bool LoadingSheetCompleted { get; set; }

        [DataMember]
        public bool NotOnShift { get; set; }

        [DataMember]
        public string ReasonNotOn { get; set; }

        [DataMember]
        public IEnumerable<LoadingSheetModel> LoadingSheets { get; set; }

        public override string ToString()
        {
            return "ShiftID: " + ShiftID + "VehicleID: " + VehicleID + "StaffID: " + StaffID;
        }
    }
}