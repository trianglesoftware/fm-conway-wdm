﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class ContactModel
    {
        [DataMember]
        public Guid ContactID { get; set; }

        [DataMember]
        public Guid CustomerID { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string MobileNumber { get; set; }

        [DataMember]
        public bool IsActive { get; set; }
    }
}