﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class ChecklistModel
    {
        [DataMember]
        public Guid ChecklistID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsMaintenance { get; set; }

        [DataMember]
        public bool IsPreHire { get; set; }

        [DataMember]
        public bool IsVms { get; set; }

        [DataMember]
        public bool IsBattery { get; set; }

        [DataMember]
        public bool IsPreHireBattery { get; set; }

        [DataMember]
        public bool IsPostHireBattery { get; set; }

        [DataMember]
        public bool IsHighSpeed { get; set; }

        [DataMember]
        public bool IsLowSpeed { get; set; }

        [DataMember]
        public bool IsJettingPermitToWork { get; set; }

        [DataMember]
        public bool IsJettingRiskAssessment { get; set; }

        [DataMember]
        public bool IsTankeringPermitToWork { get; set; }

        [DataMember]
        public bool IsTankeringRiskAssessment { get; set; }

        [DataMember]
        public bool IsCCTVRiskAssessment { get; set; }

        [DataMember]
        public bool IsJettingNewRiskAssessment { get; set; }

        [DataMember]
        public IEnumerable<ChecklistQuestionModel> ChecklistQuestions { get; set; }
    }
}