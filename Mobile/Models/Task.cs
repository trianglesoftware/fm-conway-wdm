﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class TaskModel
    {
        [DataMember]
        public Guid TaskID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Guid ActivityID { get; set; }

        [DataMember]
        public int TaskOrder { get; set; }

        [DataMember]
        public int TaskTypeID { get; set; }

        [DataMember]
        public Guid? AreaID { get; set; }

        [DataMember]
        public Guid? ChecklistID { get; set; }

        [DataMember]
        public DateTime? StartTime { get; set; }

        [DataMember]
        public DateTime? EndTime { get; set; }

        [DataMember]
        public string Notes { get; set; }

        [DataMember]
        public bool IsCancelled { get; set; }

        [DataMember]
        public bool PhotosRequired { get; set; }

        [DataMember]
        public bool ShowDetails { get; set; }

        [DataMember]
        public bool ShowClientSignature { get; set; }

        [DataMember]
        public bool ClientSignatureRequired { get; set; }

        [DataMember]
        public bool ShowStaffSignature { get; set; }

        [DataMember]
        public bool StaffSignatureRequired { get; set; }

        [DataMember]
        public String Tag { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public Guid? WeatherConditionID { get; set; }

        [DataMember]
        public bool IsMandatory { get; set; }

        [DataMember]
        public IEnumerable<TaskEquipmentModel> TaskEquipment { get; set; }
    }
}