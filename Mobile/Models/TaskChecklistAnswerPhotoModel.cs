﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    [DataContract]
    public class TaskChecklistAnswerPhotoModel
    {
        [DataMember]
        public Guid TaskChecklistAnswerPhotoID { get; set; }

        [DataMember]
        public Guid TaskChecklistAnswerID { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public string DataString { get; set; }

        [DataMember]
        public DateTime Time { get; set; }
    }
}