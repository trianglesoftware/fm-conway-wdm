﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Mobile.Models
{
    public class ShiftProgressModel
    {
        [DataMember]
        public Guid ShiftID { get; set; }

        [DataMember]
        public string Progress { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}