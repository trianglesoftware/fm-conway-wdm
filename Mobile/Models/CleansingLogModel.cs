﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mobile.Models
{
    [DataContract]
    public class CleansingLogModel
    {
        [DataMember]
        public Guid CleansingLogID { get; set; }

        [DataMember]
        public Guid TaskID { get; set; }

        [DataMember]
        public Guid ActivityID { get; set; }

        [DataMember]
        public string Road { get; set; }

        [DataMember]
        public string Section { get; set; }

        [DataMember]
        public Guid? LaneID { get; set; }

        [DataMember]
        public Guid? DirectionID { get; set; }

        [DataMember]
        public string StartMarker { get; set; }

        [DataMember]
        public string EndMarker { get; set; }

        [DataMember]
        public decimal? GulliesCleaned { get; set; }

        [DataMember]
        public decimal? GulliesMissed { get; set; }

        [DataMember]
        public decimal? CatchpitsCleaned { get; set; }

        [DataMember]
        public decimal? CatchpitsMissed { get; set; }

        [DataMember]
        public decimal? Channels { get; set; }

        [DataMember]
        public decimal? SlotDrains { get; set; }

        [DataMember]
        public decimal? Defects { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}