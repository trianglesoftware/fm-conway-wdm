﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Mobile
{
    public class FMConwayContext
    {
        FMConwayEntities _context;
        bool _managedExtenally = false;

        public FMConwayEntities Context
        {
            get
            {               
                return _context;
            }
        }

        public void SetContext(FMConwayEntities context, bool managedExternally = true)
        {
            _managedExtenally = managedExternally;
            _context = context;
            _context.Database.CommandTimeout = 360;
        }

        public void ResetContext()
        {
            _context = new FMConwayEntities();
            _context.Database.CommandTimeout = 360;
        }

        public FMConwayContext()
        {
            _context = new FMConwayEntities();
            _context.Database.CommandTimeout = 360;
        }

        public virtual void SaveChanges()
        {
            if (!_managedExtenally)
            {
                _context.SaveChanges();
            }
        }
    }
}