﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;
using Triangle.Membership;

namespace Mobile.Authentication
{
    public class CustomBasicBinding : ServiceAuthorizationManager
    {
        private const string _authHeader = "Authorization";


        protected override bool CheckAccessCore(OperationContext operationContext)
        {

            IncomingWebRequestContext wrc = WebOperationContext.Current.IncomingRequest;

            if (wrc.Headers.AllKeys.Any(k => k.Equals(_authHeader)))
            {
                string authHeader = wrc.Headers[_authHeader];

                if (!string.IsNullOrEmpty(authHeader))
                {
                    string[] credentials = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(authHeader.Substring(6))).Split(':');

                    if (credentials.Length == 2)
                    {
                        string userName = credentials[0];
                        string password = credentials[1];

                        //Validate user against database
                        Triangle.Membership.Services.Sql.SqlMembershipService sms = new Triangle.Membership.Services.Sql.SqlMembershipService();
                        if (sms.IsValidLogin(userName, password))
                        {
                            operationContext.ServiceSecurityContext.AuthorizationContext.Properties["Identity"] = userName;
                            return true;
                        }

                    }
                }
            }

            SetUnauthorizedResponse();
            return false;
        }


        private void SetUnauthorizedResponse()
        {
            WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
            WebOperationContext.Current.OutgoingResponse.Headers.Add("WWW-Authenticate: Basic realm=\"HWMartin\"");
        }

    }

}