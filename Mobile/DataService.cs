﻿using Mobile.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Triangle.Membership.Users;
using System.ServiceModel;
using System.ServiceModel.Web;
using FMConway.Services;
using System.Data.Entity;
using FMConway.Services.WorkInstructions.Enums;
using FMConway.Services.Tasks.Enums;
using System.Transactions;
using System.Configuration;
using System.IO;
using System.Data.Entity.Validation;
using System.Net;
using System.Web.Script.Serialization;
using Mobile.Extensions;
using System.Runtime.ExceptionServices;

namespace Mobile
{
    public class DataService : FMConwayContext
    {
        #region Data For App

        public List<UserModel> GetUsers()
        {
            List<UserModel> users = new List<UserModel>();

            users = Context.Users
                .Where(a => a.UserProfile.EmployeeFK != null)
                .Select(s => new UserModel
                {
                    UserID = s.UserPK,
                    Username = s.UserName,
                    EmployeeType = s.UserProfile.Employee.EmployeeType.EmployeeType1 ?? ""
                }).ToList();

            return users;
        }

        public List<ShiftModel> GetShifts(Guid userID, string syncDate)
        {
            List<ShiftModel> shifts = new List<ShiftModel>();
            DateTime sync = DateTime.Parse(syncDate);

            try
            {
                //PropagateVehicleChecklistsIfPossible();

                shifts = Context.Shifts.Where(s =>
                    s.CreatedDate > sync &&
                    s.ShiftStaffs.Any(ss => ss.Employee.UserProfiles.Any(up => up.UserFK == userID && ss.Employee.IsActive) && ss.IsActive) &&
                    DbFunctions.TruncateTime(s.ShiftDate) == DateTime.Today &&
                    s.IsActive &&
                    !s.IsCancelled &&
                    s.IsConfirmed &&
                    !s.ShiftComplete &&
                    s.Schedules.Any(sc => sc.IsActive) &&
                    s.JobPacks.Any(jp => jp.IsActive)
                    )
                    .Select(s => new ShiftModel
                    {
                        ShiftID = s.ShiftPK,
                        UserID = userID,
                        ShiftDate = s.ShiftDate,
                        UpdatedDate = s.UpdatedDate,
                        ShiftCompletionStatus = s.ShiftCompletionStatus,
                        ShiftDetails = s.JobPacks.SelectMany(jp => jp.WorkInstructions.Where(wi => wi.IsActive)).Take(1).Select(wi => wi.Job.Contract.ContractTitle).FirstOrDefault(),
                        TrafficManagementActivities = s.JobPacks.SelectMany(a => a.WorkInstructions).Select(a => a.TrafficManagementActivities).FirstOrDefault() ?? "",
                        SRWNumber = s.JobPacks.SelectMany(a => a.WorkInstructions).Select(a => a.SRWNumber).FirstOrDefault() ?? "",
                        Staff = s.ShiftStaffs.Where(ss => ss.IsActive && !ss.NotOnShift).Select(ss => new ShiftStaffModel
                        {
                            ShiftID = s.ShiftPK,
                            StaffID = ss.EmployeeFK,
                            FirstAider = false,
                            StartTime = ss.StartTime,
                            EndTime = ss.EndTime,
                            StaffRecords = ss.Employee.EmployeeCertificates.Where(c => c.IsActive).Select(c => new StaffRecordModel
                            {
                                StaffRecordID = c.EmployeeCertificatePK, // Multiples?
                                StaffID = c.EmployeeFK,
                                RecordName = c.CertificateDocuments.Where(d => d.IsActive).OrderByDescending(d => d.CreatedDate).Select(d => d.File.FileDownloadName).FirstOrDefault(),
                                ContentType = c.CertificateDocuments.Where(d => d.IsActive).OrderByDescending(d => d.CreatedDate).Select(d => d.File.ContentType).FirstOrDefault(),
                            }).ToList(),
                        }).ToList(),

                        Accidents = s.Accidents.Where(a => a.IsActive).Select(a => new AccidentModel
                        {
                            AccidentID = a.AccidentPK,
                            ShiftID = a.ShiftFK.Value,
                            AccidentDate = a.AccidentDate,
                            AccidentDetails = a.AccidentDescription,
                            Location = a.Location,
                            Registrations = a.Registrations,
                            PersonReporting = a.PersonReporting,
                            PeopleInvolved = a.PeopleInvolved,
                            Witnesses = a.Witnesses,
                            ActionTaken = a.ActionTaken
                        }).ToList(),

                        Observations = s.Observations.Where(o => o.IsActive).Select(o => new ObservationModel
                        {
                            ObservationID = o.ObservationPK,
                            ShiftID = o.ShiftFK.Value,
                            ObservationDate = o.ObservationDate,
                            ObservationDescription = o.ObservationDescription,
                            CauseOfProblem = o.CauseOfProblem,
                            Location = o.Location,
                            PeopleInvolved = o.PeopleInvolved
                        }).ToList(),

                        Notes = s.Notes.Where(p => p.IsActive).Select(p => new NoteModel
                        {
                            NoteID = p.NotePK,
                            ShiftID = p.ShiftFK,
                            NoteDate = p.NoteDate,
                            NoteDetails = p.Detail
                        }).ToList(),

                        ShiftVehicles = s.ShiftVehicles.Where(sv => sv.IsActive).Select(sv => new ShiftVehicleModel
                        {
                            VehicleID = sv.VehicleFK,
                            StaffID = sv.EmployeeFK,
                            ShiftID = sv.ShiftFK,
                            LoadingSheetCompleted = sv.LoadingSheetCompleted,
                            StartMileage = sv.StartMileage,
                            EndMileage = sv.EndMileage,
                            LoadingSheets = s.JobPacks.SelectMany(jp => jp.WorkInstructions.SelectMany(wi => wi.LoadingSheets).Select(ls => new LoadingSheetModel
                            {
                                LoadingSheetID = ls.LoadingSheetPK,
                                VehicleID = sv.VehicleFK,
                                ShiftID = s.ShiftPK,
                                LoadingSheetEquipment = ls.LoadingSheetEquipments.Select(lse => new LoadingSheetEquipmentModel
                                {
                                    LoadingSheetEquipmentID = lse.LoadingSheetEquipmentPK,
                                    EquipmentID = lse.EquipmentFK,
                                    EquipmentTypeID = lse.Equipment.EquipmentTypeFK,
                                    Quantity = (int)lse.Quantity,
                                    SheetOrder = lse.SheetOrder,
                                    VmsOrAsset = lse.Equipment.EquipmentType.EquipmentTypeVmsOrAsset.Name ?? ""
                                }).ToList()
                            }))
                        }).ToList(),
                    }).ToList();

                foreach (ShiftModel s in shifts)
                {
                    // shift progress
                    var progress = Context.ShiftProgresses.Select(p => new ShiftProgressModel
                    {
                        ShiftID = s.ShiftID,
                        Progress = p.Progress
                    }).ToList();

                    var currentProgress = Context.ShiftProgressInfoes.Where(p => p.ShiftFK.Equals(s.ShiftID)).ToList();

                    foreach (var p in currentProgress)
                    {
                        var prog = progress.Where(pr => pr.Progress.Equals(p.ShiftProgress.Progress)).SingleOrDefault();
                        prog.CreatedDate = p.Time;
                    }

                    s.ShiftProgress = progress;

                    // work instruction address
                    var addressModel = Context.Shifts.Where(a => a.ShiftPK == s.ShiftID)
                        .SelectMany(a => a.JobPacks)
                        .SelectMany(a => a.WorkInstructions)
                        .Select(a => new
                        {
                            a.AddressLine1,
                            a.AddressLine2,
                            a.AddressLine3,
                            a.City,
                            a.County,
                            a.Postcode
                        }).Single();

                    s.AddressLine1 = addressModel.AddressLine1;
                    s.AddressLine2 = addressModel.AddressLine2;
                    s.Borough = addressModel.AddressLine3;
                    s.City = addressModel.City;
                    s.County = addressModel.County;
                    s.Postcode = addressModel.Postcode;

                    s.Address = string.Join(" ", new List<string>
                    {
                        addressModel.AddressLine1,
                        addressModel.AddressLine2,
                        addressModel.AddressLine3,
                        addressModel.City,
                        addressModel.County,
                        addressModel.Postcode
                    }.Where(a => !string.IsNullOrWhiteSpace(a)).Select(a => a.Trim())) ?? "";

                    // job types
                    s.JobTypes = string.Join(", ",
                        Context.Shifts.Where(a => a.ShiftPK == s.ShiftID)
                        .SelectMany(a => a.JobPacks)
                        .SelectMany(a => a.WorkInstructions)
                        .SelectMany(a => a.Job.JobJobTypes)
                        .Where(a => a.IsActive)
                        .Select(a => a.JobType.DisplayName)
                        .OrderBy(a => a)) ?? "";

                    // job Proj Code
                    s.CustomerProjCode = string.Join(", ",
                        Context.Shifts.Where(a => a.ShiftPK == s.ShiftID)
                        .SelectMany(a => a.JobPacks)
                        .SelectMany(a => a.WorkInstructions)
                        .Select(a => a.Job.CustomerProjectCode)
                        .OrderBy(a => a)) ?? "";

                }
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, new Dictionary<string, object>
                {
                    { nameof(userID), userID },
                    { nameof(syncDate), syncDate }
                });

                LogError("GetShifts", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return shifts;
        }

        public ShiftUpdateModel GetShiftUpdate(Guid shiftID)
        {
            try
            {
                //PropagateVehicleChecklistsIfPossible();

                var shiftUpdate = Context.Shifts.Where(a => a.ShiftPK == shiftID).Select(a => new ShiftUpdateModel
                {
                    ShiftID = a.ShiftPK,
                    ShiftComplete = a.ShiftComplete,
                    IsCancelled = a.IsCancelled,
                    IsActive = a.IsActive,
                    ShiftDate = a.ShiftDate,
                    UpdatedDate = a.UpdatedDate,
                    OrderNumber = a.JobPacks.SelectMany(b => b.WorkInstructions).Select(b => b.OrderNumber).FirstOrDefault(),
                    ShiftStaffs = a.ShiftStaffs.Where(b => b.IsActive).Select(b => new ShiftUpdateStaff
                    {
                        StaffID = b.EmployeeFK,
                        UserID = b.Employee.UserProfiles.Select(c => c.UserFK).FirstOrDefault(),
                    }).ToList(),
                    ShiftVehicles = a.ShiftVehicles.Where(b => b.IsActive).Select(b => new ShiftVehicleModel
                    {
                        ShiftID = b.ShiftFK,
                        VehicleID = b.VehicleFK,
                        StaffID = b.EmployeeFK,
                        StartMileage = b.StartMileage
                    }).ToList(),
                    VehicleChecklists = a.VehicleChecklists.Where(b => b.IsActive).Select(b => new VehicleChecklistModel
                    {
                        VehicleChecklistID = b.VehicleChecklistPK,
                        VehicleID = b.VehicleFK,
                        ChecklistID = b.ChecklistFK,
                        ShiftID = b.ShiftFK,
                        StartOfShift = b.StartOfShift,
                        Answers = b.VehicleChecklistAnswers.Select(c => new VehicleChecklistAnswerModel
                        {
                            ChecklistQuestionID = c.ChecklistQuestionFK,
                            Answer = c.Answer
                        }).ToList(),
                        Defects = b.VehicleChecklistAnswers.SelectMany(c => c.VehicleChecklistDefects).Select(c => new VehicleChecklistAnswerDefectModel
                        {
                            VehicleChecklistID = c.VehicleChecklistAnswer.VehicleChecklistFK,
                            ChecklistQuestionID = c.VehicleChecklistAnswer.ChecklistQuestionFK,
                            Defect = c.Description,
                            Photos = c.VehicleChecklistDefectPhotos.Where(d => d.IsActive).Select(d => new VehicleChecklistAnswerDefectPhotoModel
                            {
                                VehicleChecklistDefectPhotoID = d.VehicleChecklistDefectPhotoPK,
                                VehicleID = b.VehicleFK,
                                ShiftID = b.ShiftFK,
                                ChecklistID = b.ChecklistFK,
                                VehicleChecklistDefectID = d.VehicleChecklistDefectFK,
                                VehicleChecklistID = b.VehicleChecklistPK,
                                ChecklistQuestionID = c.VehicleChecklistAnswer.ChecklistQuestionFK,
                                Longitude = d.File.Longitude ?? 0,
                                Latitude = d.File.Latitude ?? 0,
                                Time = d.CreatedDate,
                                Filename = d.File.FileDownloadName
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).Single();

                foreach (var vehicleChecklist in shiftUpdate.VehicleChecklists)
                {
                    var file = Context.VehicleChecklists.Where(a => a.VehicleChecklistPK == vehicleChecklist.VehicleChecklistID).Select(a => a.File).SingleOrDefault();
                    if (file != null)
                    {
                        vehicleChecklist.ImageData = Convert.ToBase64String(file.Data);
                        vehicleChecklist.ImageFilename = file.FileDownloadName;
                    }

                    foreach (var defect in vehicleChecklist.Defects)
                    {
                        foreach (var photo in defect.Photos)
                        {
                            var data = Context.VehicleChecklistDefectPhotos.Where(a => a.VehicleChecklistDefectPhotoPK == photo.VehicleChecklistDefectPhotoID).Select(a => a.File.Data).Single();
                            photo.DataString = Convert.ToBase64String(data);
                        }
                    }
                }

                return shiftUpdate;
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, shiftID);
                LogError("GetShiftUpdate", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public List<StaffModel> GetStaff()
        {
            List<StaffModel> staff = new List<StaffModel>();

            try
            {
                staff = Context.Employees.Where(e => e.IsActive).Select(e => new StaffModel
                {
                    StaffID = e.EmployeePK,
                    Name = e.EmployeeName,
                    DepotID = e.DepotFK,
                    Type = e.EmployeeType.EmployeeType1,
                    IsAgency = e.AgencyStaff
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetStaff", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return staff;
        }

        public List<VehicleModel> GetVehicles()
        {
            List<VehicleModel> vehicles = new List<VehicleModel>();

            try
            {
                vehicles = Context.Vehicles.Where(v => v.IsActive).Select(v => new VehicleModel
                {
                    VehicleID = v.VehiclePK,
                    Make = v.Make,
                    Model = v.Model,
                    Registration = v.Registration,
                    DepotID = v.DepotFK,
                    VehicleTypeID = v.VehicleTypeFK
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetVehicles", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return vehicles;

        }

        public List<EquipmentTypeModel> GetEquipmentTypes()
        {
            List<EquipmentTypeModel> equipmentTypes = new List<EquipmentTypeModel>();

            try
            {
                equipmentTypes = Context.EquipmentTypes.Where(e => e.IsActive).Select(e => new EquipmentTypeModel
                {
                    EquipmentTypeID = e.EquipmentTypePK,
                    Name = e.Name,
                    VmsOrAsset = e.EquipmentTypeVmsOrAsset.Name ?? ""
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetEquipmentTypes", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return equipmentTypes;
        }

        public List<EquipmentModel> GetEquipment()
        {
            List<EquipmentModel> equipments = new List<EquipmentModel>();

            try
            {
                equipments = Context.Equipments.Where(e => e.IsActive).Select(e => new EquipmentModel
                {
                    EquipmentID = e.EquipmentPK,
                    Name = e.EquipmentName,
                    EquipmentTypeID = e.EquipmentTypeFK
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetEquipment", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return equipments;
        }

        public List<ChecklistModel> GetChecklists()
        {
            List<ChecklistModel> checklists = new List<ChecklistModel>();

            try
            {
                checklists = Context.Checklists.Where(c => c.IsActive && (c.IsJobTypeChecklist || c.IsMaintenanceChecklist || c.IsSignatureChecklist || c.IsVehicleTypeChecklist || c.IsHighSpeedChecklist || c.IsLowSpeedChecklist || c.IsPreHireChecklist || c.IsVmsChecklist) || c.IsBatteryChecklist || c.IsPreHireBatteryChecklist || c.IsPostHireBatteryChecklist || c.IsJettingPermitToWorkChecklist || c.IsJettingRiskAssessmentChecklist || c.IsTankeringPermitToWorkChecklist || c.IsTankeringRiskAssessmentChecklist || c.IsCCTVRiskAssessmentChecklist || c.IsJettingNewRiskAssessmentChecklist).Select(c => new ChecklistModel
                {
                    ChecklistID = c.ChecklistPK,
                    Name = c.ChecklistName,
                    IsMaintenance = c.IsMaintenanceChecklist,
                    IsPreHire = c.IsPreHireChecklist,
                    IsVms = c.IsVmsChecklist,
                    IsHighSpeed = c.IsHighSpeedChecklist,
                    IsLowSpeed = c.IsLowSpeedChecklist,
                    IsBattery = c.IsBatteryChecklist,
                    IsPreHireBattery = c.IsPreHireBatteryChecklist,
                    IsPostHireBattery = c.IsPostHireBatteryChecklist,
                    IsJettingPermitToWork = c.IsJettingPermitToWorkChecklist,
                    IsJettingRiskAssessment = c.IsJettingRiskAssessmentChecklist,
                    IsTankeringPermitToWork = c.IsTankeringPermitToWorkChecklist,
                    IsTankeringRiskAssessment = c.IsTankeringRiskAssessmentChecklist,
                    IsCCTVRiskAssessment = c.IsCCTVRiskAssessmentChecklist,
                    IsJettingNewRiskAssessment = c.IsJettingNewRiskAssessmentChecklist,
                    ChecklistQuestions = c.ChecklistQuestions.Where(cq => cq.IsActive).Select(cq => new ChecklistQuestionModel
                    {
                        ChecklistID = cq.ChecklistFK,
                        ChecklistQuestionID = cq.ChecklistQuestionPK,
                        Question = cq.ChecklistQuestion1,
                        QuestionOrder = cq.QuestionOrder
                    }).ToList()
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetChecklists", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return checklists;
        }

        public List<VehicleTypeModel> GetVehicleTypes()
        {
            List<VehicleTypeModel> vehicleTypes = new List<VehicleTypeModel>();

            try
            {
                vehicleTypes = Context.VehicleTypes.Where(vt => vt.IsActive).Select(vt => new VehicleTypeModel
                {
                    VehicleTypeID = vt.VehicleTypePK,
                    Name = vt.Name,
                    Checklists = vt.VehicleTypeChecklists.Where(vtc => vtc.IsActive).Select(vtc => new ChecklistModel
                    {
                        ChecklistID = vtc.ChecklistFK
                    }).ToList()
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetVehicleTypes", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return vehicleTypes;
        }

        public List<BriefingTypeModel> GetBriefingTypes()
        {
            List<BriefingTypeModel> briefingTypes = new List<BriefingTypeModel>();

            try
            {
                briefingTypes = Context.BriefingTypes.Where(b => b.IsActive).Select(b => new BriefingTypeModel
                {
                    BriefingTypeID = b.BriefingTypePK,
                    Name = b.Name
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetBriefingTypes", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return briefingTypes;
        }

        public List<ActivityTypeModel> GetActivityTypes()
        {
            List<ActivityTypeModel> activityTypes = new List<ActivityTypeModel>();

            try
            {
                activityTypes = Context.JobTypes.Where(jt => jt.IsActive).Select(jt => new ActivityTypeModel
                {
                    ActivityTypeID = jt.JobTypePK,
                    Name = jt.JobType1,
                    DisplayName = jt.DisplayName,
                    TrafficCountLimit = 0
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetActivityTypes", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return activityTypes;
        }

        public List<TaskTypeModel> GetTaskTypes()
        {
            List<TaskTypeModel> taskTypes = new List<TaskTypeModel>();

            try
            {
                taskTypes = Context.TaskTypes.Select(tt => new TaskTypeModel
                {
                    TaskTypeID = tt.TaskTypePK,
                    Name = tt.TaskType1
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetTaskTypes", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return taskTypes;
        }

        public List<WeatherConditionModel> GetWeatherConditions()
        {
            var weatherConditions = new List<WeatherConditionModel>();

            try
            {
                weatherConditions = Context.WeatherConditions.Where(a => a.IsActive).Select(a => new WeatherConditionModel
                {
                    WeatherConditionID = a.WeatherConditionPK,
                    Name = a.Name
                }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetWeatherConditions", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return weatherConditions;
        }

        public List<JobPackModel> GetJobPacks(Guid userID, string syncDate)
        {
            List<JobPackModel> jobPacks = new List<JobPackModel>();

            DateTime sync = DateTime.Parse(syncDate);

            try
            {
                jobPacks = Context.JobPacks.AsNoTracking()
                    .Where(jp =>
                        jp.Shift.CreatedDate > sync &&
                        jp.IsActive &&
                        jp.Shift.ShiftStaffs.Any(ss => ss.Employee.UserProfiles.Any(up => up.UserFK == userID && up.Employee.IsActive)) &&
                        DbFunctions.TruncateTime(jp.Shift.ShiftDate) == DateTime.Today &&
                        jp.Shift.IsActive &&
                        !jp.Shift.IsCancelled &&
                        !jp.Shift.ShiftComplete)
                    .Select(jp => new JobPackModel
                    {
                        JobPackID = jp.JobPackPK,
                        ShiftID = jp.ShiftFK.Value,
                        JobPackName = jp.JobPackName,
                        Documents = jp.JobPackDocuments.Where(jpd => jpd.IsActive).Select(jpd => new JobPackDocumentModel
                        {
                            JobPackDocumentID = jpd.JobPackDocumentPK,
                            DocumentName = jpd.File.FileDownloadName,
                            JobPackID = jpd.JobPackFK,
                            ContentType = jpd.File.ContentType
                        }).ToList(),
                        Briefings = jp.Briefings.Where(b => b.IsActive).Select(b => new BriefingModel
                        {
                            BriefingID = b.BriefingPK,
                            BriefingTypeID = b.BriefingTypeFK,
                            JobPackID = b.JobPackFK,
                            Name = b.Name,
                            Details = b.Details,
                            FileID = b.FileFK,
                            DocumentName = b.FileFK.HasValue ? b.File.FileDownloadName : "",
                            ContentType = b.FileFK.HasValue ? b.File.ContentType : "",
                            Checklists = b.BriefingChecklists.Where(bc => bc.IsActive).Select(bc => new ChecklistModel
                            {
                                ChecklistID = bc.ChecklistFK
                            }).ToList()
                        }).ToList(),
                        Activities = jp.WorkInstructions.Where(wi => wi.IsActive).Select(wi => new ActivityModel
                        {
                            ActivityID = wi.WorkInstructionPK,
                            ActivityTypeID = wi.JobTypeFK,
                            JobPackID = wi.JobPackFK,
                            Name = wi.ActivityDescription,
                            Customer = wi.Job.Contract.Customer.CustomerName,
                            CustomerPhoneNumber = wi.Job.Contract.Customer.PhoneNumber,
                            Contract = wi.Job.Contract.ContractTitle,
                            ContractNumber = wi.Job.Contract.ContractNumber,
                            Scheme = wi.Job.Scheme.Description,
                            ScopeOfWork = wi.ActivityDescription,
                            Distance = 0.00M,
                            Road = "",
                            CWayDirection = "",
                            InstallationTime = DateTime.Now,
                            RemovalTime = DateTime.Now,
                            HealthAndSafety = "",
                            FirstConeDate = wi.FirstCone,
                            LastConeDate = wi.LastCone,
                            WorksInstructionNumber = wi.WorkInstructionNumber,
                            WorksInstructionComments = wi.Comments ?? "",
                            AreaCallNumber = wi.Area.NCCNumber,
                            AreaCallProtocol = wi.Area.CallProtocol,
                            MaintenanceChecklistReminderInMinutes = wi.MaintenanceChecklistReminderInMinutes ?? 0,
                            ClientReferenceNumber = wi.ClientReferenceNumber ?? "",
                            CustomerID = wi.Job.Contract.CustomerFK,
                            IsOutOfHours = wi.IsOutOfHours,
                            WorkInstructionStartDate = wi.WorkInstructionStartDate,
                            WorkInstructionFinishDate = wi.WorkInstructionFinishDate,
                            DepotID = wi.DepotFK,
                            OrderNumber = wi.OrderNumber,
                            Tasks = wi.Tasks.Where(t => t.IsActive && !t.IsCancelled).Select(t => new TaskModel
                            {
                                TaskID = t.TaskPK,
                                Name = t.Name,
                                TaskOrder = t.TaskOrder,
                                TaskTypeID = t.TaskTypeFK,
                                AreaID = t.AreaCallAreaFK,
                                ChecklistID = t.ChecklistFK,
                                Notes = t.Notes ?? "",
                                StartTime = t.StartTime,
                                EndTime = t.EndTime,
                                ActivityID = wi.WorkInstructionPK,
                                PhotosRequired = t.PhotosRequired,
                                ShowDetails = t.ShowDetails,
                                ShowClientSignature = t.ShowClientSiganture,
                                ClientSignatureRequired = t.ClientSignatureRequired,
                                ShowStaffSignature = t.ShowStaffSignature,
                                StaffSignatureRequired = t.StaffSignatureRequired,
                                Tag = t.Tag ?? "",
                                WeatherConditionID = t.WeatherConditionFK,
                                IsMandatory = t.IsMandatory,
                                TaskEquipment = t.TaskEquipments.Where(te => te.IsActive).Select(te => new TaskEquipmentModel
                                {
                                    TaskEquipmentID = te.TaskEquipmentPK,
                                    TaskID = te.TaskFK,
                                    Quantity = te.Quantity,
                                    EquipmentID = te.EquipmentFK,
                                    ShiftID = jp.ShiftFK.Value,
                                    TaskEquipmentAssets = te.TaskEquipmentAssets.Select(tea => new TaskEquipmentAssetModel
                                    {
                                        TaskEquipmentAssetID = tea.TaskEquipmentAssetPK,
                                        TaskEquipmentID = tea.TaskEquipmentFK,
                                        ItemNumber = tea.ItemNumber,
                                        AssetNumber = tea.AssetNumber
                                    })
                                }).ToList()

                            }).ToList(),
                            MaintenanceChecks = wi.MaintenanceChecks.Where(m => m.IsActive).Select(m => new MaintenanceCheckModel
                            {
                                MaintenanceCheckID = m.MaintenanceCheckPK,
                                Description = m.Description,
                                Time = m.Time,
                                ActivityID = wi.WorkInstructionPK,
                                Answers = m.MaintenanceCheckAnswers.Where(ma => ma.IsActive).Select(ma => new MaintenanceCheckAnswerModel
                                {
                                    MaintenanceCheckID = ma.MaintenanceCheckFK,
                                    Answer = ma.Answer,
                                    ChecklistQuestionID = ma.ChecklistQuestionFK
                                }).ToList()
                            }).ToList(),
                            MethodStatements = wi.WorkInstructionMethodStatements.Select(ms => new ActivityMethodStatementModel
                            {
                                ActivityMethodStatementID = ms.WorkInstructionMethodStatementPK,
                                ActivityID = ms.WorkInstructionFK,
                                MethodStatementID = ms.MethodStatementFK,
                                IsActive = ms.IsActive
                            }).ToList(),
                        }).ToList()
                    }).ToList();

                foreach (var jp in jobPacks)
                {
                    foreach (var activity in jp.Activities)
                    {
                        if (activity.FirstConeDate != null)
                        {
                            var firstConeDate = (DateTime)activity.FirstConeDate;
                            activity.FirstCone = firstConeDate.Minute < 10 ? firstConeDate.Hour + ":0" + firstConeDate.Minute : firstConeDate.Hour + ":" + firstConeDate.Minute;
                        }
                        else
                        {
                            activity.FirstCone = "";
                        }

                        if (activity.LastConeDate != null)
                        {
                            var lastConeDate = (DateTime)activity.LastConeDate;
                            activity.LastCone = lastConeDate.Minute < 10 ? lastConeDate.Hour + ":0" + lastConeDate.Minute : lastConeDate.Hour + ":" + lastConeDate.Minute;
                        }
                        else
                        {
                            activity.LastCone = "";
                        }
                    }
                }

                return jobPacks;
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, new Dictionary<string, object>
                {
                    { nameof(userID), userID },
                    { nameof(syncDate), syncDate }
                });

                LogError("GetJobPacks", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public List<AreaModel> GetAreas(Guid userID, string syncDate)
        {
            List<AreaModel> areas = new List<AreaModel>();

            DateTime sync = DateTime.Parse(syncDate);

            try
            {
                areas = Context.AreaCallAreas.AsNoTracking().Where(a => a.IsActive && a.Tasks.Any(t => t.WorkInstruction.JobPack.Shift.IsActive
                    && !t.WorkInstruction.JobPack.Shift.IsCancelled && !t.WorkInstruction.JobPack.Shift.IsCancelled
                    && DbFunctions.TruncateTime(t.WorkInstruction.JobPack.Shift.ShiftDate) == DateTime.Today
                    && t.WorkInstruction.JobPack.Shift.CreatedDate > sync
                    && t.WorkInstruction.JobPack.Shift.ShiftStaffs.Any(ss => ss.Employee.UserProfiles.Any(up => up.UserFK == userID && up.Employee.IsActive)))).Select(a => new AreaModel
                    {
                        AreaID = a.AreaCallAreaPK,
                        Name = a.Area,
                        Details = a.ExtraDetails,
                        CallProtocol = a.CallProtocol,
                        PersonsName = a.PersonsName,
                        Reference = a.Reference

                    }).ToList();
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, new Dictionary<string, object>
                {
                    { nameof(userID), userID },
                    { nameof(syncDate), syncDate }
                });

                LogError("GetAreas", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return areas;
        }

        public string GetStaffRecord(Guid staffRecordID)
        {
            try
            {
                byte[] byteArray = null; //Remove

                var cert = Context.EmployeeCertificates.Where(c => c.EmployeeCertificatePK.Equals(staffRecordID) && c.IsActive && c.CertificateDocuments.Any(d => d.IsActive)).SingleOrDefault();

                if (cert != null)
                {
                    byteArray = cert.CertificateDocuments.Where(d => d.IsActive).OrderByDescending(d => d.CreatedDate).Take(1).Select(d => d.File.Data).SingleOrDefault();
                }

                if (byteArray != null)
                {
                    return Convert.ToBase64String(byteArray);
                }
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, staffRecordID);
                LogError("GetStaffRecord", e, false);
            }

            return "";
        }

        public string GetDocument(Guid documentID, Guid jobPackID, bool isBriefing)
        {
            try
            {
                byte[] byteArray;

                if (isBriefing)
                {
                    byteArray = Context.Briefings.Where(b => b.IsActive && b.JobPackFK.Equals(jobPackID) && b.FileFK.Value.Equals(documentID)).Select(b => b.File.Data).SingleOrDefault();
                }
                else
                {
                    byteArray = Context.JobPackDocuments.Where(jpd => jpd.IsActive && jpd.JobPackFK.Equals(jobPackID) && jpd.JobPackDocumentPK.Equals(documentID)).Select(jpd => jpd.File.Data).SingleOrDefault();
                }

                if (byteArray != null)
                {
                    return Convert.ToBase64String(byteArray);
                }
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, new Dictionary<string, object>
                {
                    { nameof(documentID), documentID },
                    { nameof(jobPackID), jobPackID },
                    { nameof(isBriefing), isBriefing },
                });

                LogError("GetDocument", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return "";
        }

        public List<ShiftVehicleModel> GetVehiclesOnShifts()
        {
            List<ShiftVehicleModel> vehicles = new List<ShiftVehicleModel>();

            try
            {
                vehicles = Context.ShiftVehicles.Where(sv =>
                    sv.Shift.ShiftDate.Day.Equals(DateTime.Now.Day) &&
                    sv.Shift.ShiftDate.Month.Equals(DateTime.Now.Month) &&
                    sv.Shift.ShiftDate.Year.Equals(DateTime.Now.Year) &&
                    sv.IsActive &&
                    sv.Shift.IsActive &&
                    !sv.Shift.IsCancelled &&
                    sv.Shift.VehicleChecklists.Count.Equals(0)).
                    Select(sv => new ShiftVehicleModel
                    {
                        VehicleID = sv.VehicleFK,
                        StaffID = sv.EmployeeFK,
                        LoadingSheetCompleted = sv.LoadingSheetCompleted,
                        StartMileage = sv.StartMileage,
                        EndMileage = sv.EndMileage,
                        ShiftID = sv.ShiftFK
                    }).ToList();
            }
            catch (Exception e)
            {
                LogError("GetVehiclesOnShifts", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return vehicles;
        }

        // if VehicleChecklist doesn't exist for a shift, copy VehicleChecklist for the same vehicle from a different shift on the same day if it exists
        // Serializable transaction to stop concurrent queries from performing multiple inserts
        public void PropagateVehicleChecklistsIfPossible()
        {
            var saveChanges = false;

            var options = new TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, options))
            {
                var shiftVehicles = Context.ShiftVehicles.Where(a =>
                    a.IsActive &&
                    a.Shift.IsActive &&
                    !a.Shift.IsCancelled &&
                    !a.Shift.ShiftComplete &&
                    DbFunctions.TruncateTime(a.Shift.ShiftDate) == DateTime.Today &&
                    !a.Shift.VehicleChecklists.Any(b => b.VehicleFK == a.VehicleFK && b.StartOfShift == true && b.IsActive))
                    .ToList();

                foreach (var shiftVehicle in shiftVehicles)
                {
                    var vehicleChecklist = Context.VehicleChecklists.Where(a =>
                        a.VehicleFK == shiftVehicle.VehicleFK &&
                        a.IsActive &&
                        a.StartOfShift == true &&
                        a.Shift.ShiftVehicles.Any(b =>
                            b.VehicleFK == shiftVehicle.VehicleFK &&
                            b.IsActive &&
                            b.Shift.IsActive &&
                            DbFunctions.TruncateTime(a.Shift.ShiftDate) == DateTime.Today))
                        .OrderByDescending(a => a.UpdatedDate)
                        .FirstOrDefault();

                    if (vehicleChecklist != null)
                    {
                        var shiftVehicleTarget = Context.ShiftVehicles.Where(a => a.ShiftFK == vehicleChecklist.ShiftFK && a.VehicleFK == vehicleChecklist.VehicleFK && a.IsActive).FirstOrDefault();
                        if (shiftVehicleTarget == null)
                        {
                            continue;
                        }

                        shiftVehicle.StartMileage = shiftVehicleTarget.StartMileage;
                        shiftVehicle.EmployeeFK = shiftVehicleTarget.EmployeeFK;

                        var vc = new VehicleChecklist();
                        vc.VehicleChecklistPK = Guid.NewGuid();
                        vc.VehicleFK = vehicleChecklist.VehicleFK;
                        vc.ShiftFK = shiftVehicle.ShiftFK;
                        vc.ChecklistFK = vehicleChecklist.ChecklistFK;
                        vc.StartOfShift = vehicleChecklist.StartOfShift;
                        vc.PhotoFileFK = vehicleChecklist.PhotoFileFK != null ? Guid.NewGuid() : (Guid?)null;
                        vc.IsActive = vehicleChecklist.IsActive;
                        vc.CreatedByID = vehicleChecklist.CreatedByID;
                        vc.CreatedDate = vehicleChecklist.CreatedDate;
                        vc.UpdatedByID = vehicleChecklist.UpdatedByID;
                        vc.UpdatedDate = vehicleChecklist.UpdatedDate;
                        Context.VehicleChecklists.Add(vc);

                        if (vehicleChecklist.PhotoFileFK != null)
                        {
                            var file = vehicleChecklist.File;

                            var f = new File();
                            f.FilePK = (Guid)vc.PhotoFileFK;
                            f.FileDownloadName = file.FileDownloadName;
                            f.Data = file.Data;
                            f.ContentType = file.ContentType;
                            f.Longitude = file.Longitude;
                            f.Latitude = file.Latitude;
                            f.IsActive = file.IsActive;
                            f.CreatedByFK = file.CreatedByFK;
                            f.CreatedDate = file.CreatedDate;
                            f.UpdatedByFK = file.UpdatedByFK;
                            f.UpdatedDate = file.UpdatedDate;
                            Context.Files.Add(f);
                        }

                        foreach (var vehicleChecklistAnswer in vehicleChecklist.VehicleChecklistAnswers)
                        {
                            var vca = new VehicleChecklistAnswer();
                            vca.VehicleChecklistAnswerPK = Guid.NewGuid();
                            vca.VehicleChecklistFK = vc.VehicleChecklistPK;
                            vca.ChecklistQuestionFK = vehicleChecklistAnswer.ChecklistQuestionFK;
                            vca.Answer = vehicleChecklistAnswer.Answer;
                            vca.CreatedByID = vehicleChecklistAnswer.CreatedByID;
                            vca.CreatedDate = vehicleChecklistAnswer.CreatedDate;
                            vca.UpdatedByID = vehicleChecklistAnswer.UpdatedByID;
                            vca.UpdatedDate = vehicleChecklistAnswer.UpdatedDate;
                            Context.VehicleChecklistAnswers.Add(vca);

                            foreach (var vehicleChecklistDefect in vehicleChecklistAnswer.VehicleChecklistDefects)
                            {
                                var vcd = new VehicleChecklistDefect();
                                vcd.VehicleChecklistDefectPK = Guid.NewGuid();
                                vcd.Description = vehicleChecklistDefect.Description;
                                vcd.VehicleChecklistAnswerFK = vca.VehicleChecklistAnswerPK;
                                vcd.IsActive = vehicleChecklistDefect.IsActive;
                                vcd.CreatedByID = vehicleChecklistDefect.CreatedByID;
                                vcd.CreatedDate = vehicleChecklistDefect.CreatedDate;
                                vcd.UpdatedByID = vehicleChecklistDefect.UpdatedByID;
                                vcd.UpdatedDate = vehicleChecklistDefect.UpdatedDate;
                                Context.VehicleChecklistDefects.Add(vcd);

                                foreach (var vehicleChecklistDefectPhoto in vehicleChecklistDefect.VehicleChecklistDefectPhotos)
                                {
                                    var vcdp = new VehicleChecklistDefectPhoto();
                                    vcdp.VehicleChecklistDefectPhotoPK = Guid.NewGuid();
                                    vcdp.VehicleChecklistDefectFK = vcd.VehicleChecklistDefectPK;
                                    vcdp.FileFK = Guid.NewGuid();
                                    vcdp.IsActive = vehicleChecklistDefectPhoto.IsActive;
                                    vcdp.CreatedByID = vehicleChecklistDefectPhoto.CreatedByID;
                                    vcdp.CreatedDate = vehicleChecklistDefectPhoto.CreatedDate;
                                    vcdp.UpdatedByID = vehicleChecklistDefectPhoto.UpdatedByID;
                                    vcdp.UpdatedDate = vehicleChecklistDefectPhoto.UpdatedDate;
                                    Context.VehicleChecklistDefectPhotos.Add(vcdp);

                                    var file = vehicleChecklistDefectPhoto.File;

                                    var f = new File();
                                    f.FilePK = vcdp.FileFK;
                                    f.FileDownloadName = file.FileDownloadName;
                                    f.Data = file.Data;
                                    f.ContentType = file.ContentType;
                                    f.Longitude = file.Longitude;
                                    f.Latitude = file.Latitude;
                                    f.IsActive = file.IsActive;
                                    f.CreatedByFK = file.CreatedByFK;
                                    f.CreatedDate = file.CreatedDate;
                                    f.UpdatedByFK = file.UpdatedByFK;
                                    f.UpdatedDate = file.UpdatedDate;
                                    Context.Files.Add(f);
                                }
                            }
                        }

                        saveChanges = true;
                        Context.SaveChanges();
                    }
                }

                if (saveChanges)
                {
                    transactionScope.Complete();
                }
            }
        }

        public List<VehicleChecklistModel> GetVehicleChecklists(Guid userID, string syncDate)
        {
            List<VehicleChecklistModel> vehicleChecklists = new List<VehicleChecklistModel>();
            DateTime sync = DateTime.Parse(syncDate);

            try
            {
                vehicleChecklists = Context.VehicleChecklists.Where(vc =>
                    vc.IsActive &&
                    vc.Shift.IsActive &&
                    !vc.Shift.IsCancelled &&
                    !vc.Shift.ShiftComplete &&
                    vc.Shift.CreatedDate > sync &&
                    DbFunctions.TruncateTime(vc.Shift.ShiftDate) == DateTime.Today &&
                    vc.Shift.ShiftStaffs.Any(ss => ss.Employee.UserProfiles.Any(up => up.UserFK == userID && up.Employee.IsActive)))
                    .Select(vc => new VehicleChecklistModel
                    {
                        VehicleChecklistID = vc.VehicleChecklistPK,
                        VehicleID = vc.VehicleFK,
                        ChecklistID = vc.ChecklistFK,
                        ShiftID = vc.ShiftFK,
                        StartOfShift = vc.StartOfShift,
                        Answers = vc.VehicleChecklistAnswers.Select(vca => new VehicleChecklistAnswerModel
                        {
                            ChecklistQuestionID = vca.ChecklistQuestionFK,
                            Answer = vca.Answer
                        }).ToList(),
                        Defects = vc.VehicleChecklistAnswers.SelectMany(vca => vca.VehicleChecklistDefects).Select(vcd => new VehicleChecklistAnswerDefectModel
                        {
                            ChecklistQuestionID = vcd.VehicleChecklistAnswer.ChecklistQuestionFK,
                            Defect = vcd.Description
                        }).ToList()
                    }).ToList();

                // get ImageData and ImageFilename
                foreach (var vehicleChecklist in vehicleChecklists)
                {
                    var file = Context.VehicleChecklists.Where(a => a.VehicleChecklistPK == vehicleChecklist.VehicleChecklistID).Select(a => a.File).SingleOrDefault();
                    if (file != null)
                    {
                        vehicleChecklist.ImageData = Convert.ToBase64String(file.Data);
                        vehicleChecklist.ImageFilename = file.FileDownloadName;
                    }
                }
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, new Dictionary<string, object>
                {
                    { nameof(userID), userID },
                    { nameof(syncDate), syncDate }
                });

                LogError("GetVehicleChecklists", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return vehicleChecklists;
        }

        public List<ErrorLogModel> GetErrorLogs(int quantity)
        {
            List<ErrorLogModel> logs = new List<ErrorLogModel>();

            try
            {
                logs = Context.ErrorLogs.OrderByDescending(e => e.CreatedDate).Take(quantity).Select(e => new ErrorLogModel
                {
                    LogPK = e.LogPK,
                    Exception = e.Exception,
                    Location = e.Location,
                    Description = e.Description,
                    CreatedDate = e.CreatedDate
                }).ToList();
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, quantity);
                LogError("GetErrorLogs", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return logs;
        }

        public List<ActivityPriorityModel> GetActivityPriorities()
        {
            var activityPriorities = new List<ActivityPriorityModel>();

            try
            {
                activityPriorities = Context.ActivityPriorities.Select(a => new ActivityPriorityModel
                {
                    ActivityPriorityID = a.ActivityPriorityPK,
                    Name = a.Name,
                    IsActive = a.IsActive
                }).ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetActivityPriorities), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return activityPriorities;
        }

        public List<LaneModel> GetLanes()
        {
            var lanes = new List<LaneModel>();

            try
            {
                lanes = Context.Lanes.Select(a => new LaneModel
                {
                    LaneID = a.LanePK,
                    Name = a.Name,
                    IsActive = a.IsActive
                }).ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetLanes), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return lanes;
        }

        public List<TMRequirementModel> GetTMRequirements()
        {
            var tmReqs = new List<TMRequirementModel>();

            try
            {
                tmReqs = Context.TMRequirements.Select(a => new TMRequirementModel
                {
                    TMRequirementID = a.TMRequirementPK,
                    Name = a.Name,
                    IsActive = a.IsActive
                }).ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetTMRequirements), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return tmReqs;
        }

        public List<RoadSpeedModel> GetRoadSpeeds()
        {
            var speeds = new List<RoadSpeedModel>();

            try
            {
                speeds = Context.RoadSpeeds.Select(a => new RoadSpeedModel
                {
                    RoadSpeedID = a.RoadSpeedPK,
                    Speed = a.Speed,
                    IsActive = a.IsActive
                }).ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetRoadSpeeds), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return speeds;
        }

        public List<DirectionModel> GetDirections()
        {
            var directions = new List<DirectionModel>();

            try
            {
                directions = Context.Directions.Select(a => new DirectionModel
                {
                    DirectionID = a.DirectionPK,
                    Name = a.Name,
                    IsActive = a.IsActive
                }).ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetDirections), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return directions;
        }

        public List<DisposalSiteModel> GetDisposalSites()
        {
            var disposalSites = new List<DisposalSiteModel>();

            try
            {
                disposalSites = Context.DisposalSites.Select(a => new DisposalSiteModel
                {
                    DisposalSiteID = a.DisposalSitePK,
                    Name = a.Name,
                    IsActive = a.IsActive
                }).ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetDisposalSites), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return disposalSites;
        }

        public List<CustomerModel> GetCustomers()
        {
            var customers = new List<CustomerModel>();

            try
            {
                customers = Context.Customers.Select(a => new CustomerModel
                {
                    CustomerID = a.CustomerPK,
                    CustomerName = a.CustomerName,
                    AccountNumber = a.AccountNumber,
                    CustomerCode = a.CustomerCode,
                    PhoneNumber = a.PhoneNumber,
                    IsActive = a.IsActive
                }).ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetCustomers), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return customers;
        }

        public List<ContactModel> GetContacts()
        {
            var contacts = new List<ContactModel>();

            try
            {
                contacts = Context.Contacts.Select(a => new ContactModel
                {
                    ContactID = a.ContactPK,
                    CustomerID = a.CustomerFK,
                    ContactName = a.ContactName,
                    PhoneNumber = a.PhoneNumber,
                    MobileNumber = a.MobileNumber,
                    IsActive = a.IsActive
                }).ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetContacts), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return contacts;
        }

        public List<MethodStatementModel> GetMethodStatements(DateTime syncDate)
        {
            var methodStatements = new List<MethodStatementModel>();

            try
            {
                methodStatements = Context.MethodStatements
                    .Where(a => a.UpdatedDate >= syncDate)
                    .Select(a => new MethodStatementModel
                    {
                        MethodStatementID = a.MethodStatementPK,
                        DocumentID = a.FileFK,
                        MethodStatementTitle = a.MethodStatementTitle ?? "",
                        DocumentName = a.File.FileDownloadName ?? "",
                        ContentType = a.File.ContentType ?? "",
                        IsActive = a.IsActive
                    })
                    .ToList();
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, syncDate);
                LogError(nameof(GetMethodStatements), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return methodStatements;
        }

        public string GetMethodStatementDocument(Guid methodStatementID)
        {
            try
            {
                var byteArray = Context.MethodStatements.Where(a => a.MethodStatementPK == methodStatementID).Select(b => b.File.Data).SingleOrDefault();

                if (byteArray != null)
                {
                    return Convert.ToBase64String(byteArray);
                }
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, methodStatementID);
                LogError("GetMethodStatementDocument", e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return null;
        }

        public List<DepotModel> GetDepots()
        {
            var depots = new List<DepotModel>();

            try
            {
                depots = Context.Depots.Select(a => new DepotModel
                {
                    DepotID = a.DepotPK,
                    DepotName = a.DepotName,
                    IsActive = a.IsActive
                })
                .ToList();
            }
            catch (Exception e)
            {
                LogError(nameof(GetDepots), e, false);
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return depots;
        }

        #endregion 

        #region Data From App

        public void UpdateShiftStatus(Guid shiftID, int completionStatus)
        {
            var shift = Context.Shifts.Where(s => s.ShiftPK.Equals(shiftID)).SingleOrDefault();

            if (shift != null)
            {
                using (var transaction = Context.Database.BeginTransaction())
                {
                    try
                    {
                        shift.ShiftCompletionStatus = completionStatus;
                        shift.UpdatedDate = DateTime.Now;
                        shift.UpdatedByID = GetUserContext().UserDetails.ID;

                        Context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        if (transaction != null)
                        {
                            transaction.Rollback();
                        }

                        e.Data.AddSafely(Global.ExceptionDataKey, new Dictionary<string, object>
                        {
                            { nameof(shiftID), shiftID },
                            { nameof(completionStatus), completionStatus }
                        });

                        ExceptionDispatchInfo.Capture(e).Throw();
                    }
                }
            }
        }

        public void CompleteShift(Guid shiftID)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                try
                {
                    var shift = Context.Shifts.Where(s => s.ShiftPK.Equals(shiftID)).SingleOrDefault();
                    if (shift != null)
                    {
                        shift.ShiftComplete = true;
                        shift.UpdatedDate = DateTime.Now;
                        shift.UpdatedByID = GetUserContext().UserDetails.ID;

                        var workInstruction = shift.JobPacks.SelectMany(a => a.WorkInstructions).Single();
                        workInstruction.Status = Context.Status.Where(a => a.IsActive && a.IsWorkInstructionStatus && a.Status1 == "Complete").Single();

                        // late task equipment assignment to deinstall job type
                        if (workInstruction.JobType.JobType1 == JobTypesEnum.Install)
                        {
                            var deinstallWorkInstruction = Context.WorkInstructions.Where(a => a.InstallWIPK == workInstruction.WorkInstructionPK && a.IsActive).SingleOrDefault();
                            if (deinstallWorkInstruction != null)
                            {
                                var equipmentCollectionTask = deinstallWorkInstruction.Tasks.Where(a => a.TaskType.TaskType1 == TaskTypesEnum.EquipmentCollection).Single();
                                if (!equipmentCollectionTask.TaskEquipments.Any())
                                {
                                    var equipmentInstallTask = workInstruction.Tasks.Where(a => a.TaskType.TaskType1 == TaskTypesEnum.EquipmentInstall).Single();
                                    foreach (var taskEquipment in equipmentInstallTask.TaskEquipments)
                                    {
                                        var te = new TaskEquipment();
                                        te.TaskEquipmentPK = Guid.NewGuid();
                                        te.Task = equipmentCollectionTask;
                                        te.EquipmentFK = taskEquipment.EquipmentFK;
                                        te.Quantity = taskEquipment.Quantity;
                                        te.IsActive = true;
                                        te.CreatedDate = DateTime.Now;
                                        te.CreatedByID = GetUserContext().UserDetails.ID;
                                        te.UpdatedDate = DateTime.Now;
                                        te.UpdatedByID = GetUserContext().UserDetails.ID;
                                        Context.TaskEquipments.Add(te);

                                        foreach (var taskEquipmentAsset in taskEquipment.TaskEquipmentAssets)
                                        {
                                            var tea = new TaskEquipmentAsset();
                                            tea.TaskEquipmentAssetPK = Guid.NewGuid();
                                            tea.TaskEquipment = te;
                                            tea.ItemNumber = taskEquipmentAsset.ItemNumber;
                                            tea.AssetNumber = taskEquipmentAsset.AssetNumber;
                                            Context.TaskEquipmentAssets.Add(tea);
                                        }
                                    }
                                }
                            }
                        }

                        Context.SaveChanges();

                        tran.Commit();
                    }
                }
                catch (Exception e)
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, shiftID);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public void CancelShift(CancelShiftModel cancelShift)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var shift = Context.Shifts.Where(s => s.ShiftPK.Equals(cancelShift.ShiftID)).Single();

                    var completeStatusPK = Context.Status.Where(a => a.IsWorkInstructionStatus && a.Status1 == "Complete").Select(a => a.StatusPK).Single();

                    // check database has designated IsEmergencyCancellation
                    var reasonCodePK = Context.ReasonCodes.Where(a => a.IsEmergencyCancellation).Select(a => (Guid?)a.ReasonCodePK).FirstOrDefault();

                    var workInstruction = shift.JobPacks.SelectMany(a => a.WorkInstructions).Single();
                    workInstruction.StatusFK = completeStatusPK;
                    workInstruction.IsSuccessful = false;
                    workInstruction.ReasonNotes = cancelShift.CancellationReason;
                    workInstruction.ReasonCodeFK = reasonCodePK;

                    shift.IsCancelled = true;
                    shift.UpdatedDate = DateTime.Now;
                    shift.UpdatedByID = GetUserContext().UserDetails.ID;

                    Context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, cancelShift);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public ShiftProgressResult UpdateShiftProgress(ShiftProgressModel model)
        {
            var result = new ShiftProgressResult();

            try
            {
                var shift = Context.Shifts.Where(s => s.ShiftPK == model.ShiftID).SingleOrDefault();

                // ignore out of hours shift progresses until shift has been created
                if (shift == null)
                {
                    return null;
                }

                shift.UpdatedDate = DateTime.Now;
                shift.UpdatedByID = GetUserContext().UserDetails.ID;

                int shiftProgressID = Context.ShiftProgresses.Where(sp => sp.Progress == model.Progress).Select(sp => sp.ShiftProgressID).SingleOrDefault();
                var info = Context.ShiftProgressInfoes.Where(a => a.ShiftFK == shift.ShiftPK && a.ShiftProgressID == shiftProgressID).FirstOrDefault();

                if (info == null)
                {
                    info = new ShiftProgressInfo();
                    info.ShiftFK = shift.ShiftPK;
                    info.ShiftProgressID = shiftProgressID;

                    Context.ShiftProgressInfoes.Add(info);
                }

                info.Time = model.CreatedDate;
                info.CreatedDate = DateTime.Now;

                Context.SaveChanges();

                result.ShiftID = model.ShiftID;
                result.Progress = model.Progress;
                result.Saved = true;
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, model);
                ExceptionDispatchInfo.Capture(e).Throw();
            }

            return result;
        }

        public string UpdateShiftProgressStarted(ShiftProgressStartedModel model)
        {
            var shift = Context.Shifts.Where(s => s.ShiftPK == model.ShiftID).SingleOrDefault();

            // ignore out of hours shift progresses until shift has been created
            if (shift == null)
            {
                return null;
            }

            if (!shift.IsConfirmed)
            {
                return "Unconfirmed";
            }

            if (model.ShiftUpdatedDate != null && shift.UpdatedDate.TruncateMilliseconds() != model.ShiftUpdatedDate.Value.TruncateMilliseconds())
            {
                return "Outdated";
            }

            var shiftProgressResult = UpdateShiftProgress(model);
            if (shiftProgressResult?.Saved == false)
            {
                return "Error";
            }

            return null;
        }

        public void PushShiftStaff(ShiftStaffModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var ss = Context.ShiftStaffs.Where(s => s.ShiftFK.Equals(model.ShiftID) && s.EmployeeFK.Equals(model.StaffID)).SingleOrDefault();

                    if (ss != null)
                    {
                        ss.AgencyEmployeeName = model.AgencyEmployeeName != null ? model.AgencyEmployeeName : null;
                        ss.StartTime = model.StartTime;
                        ss.EndTime = model.EndTime;
                        ss.NotOnShift = model.NotOnShift;
                        ss.ReasonNotOn = model.ReasonNotOn;
                        ss.IsActive = !model.NotOnShift;
                        ss.UpdatedDate = DateTime.Now;
                        ss.UpdatedByID = GetUserContext().UserDetails.ID;

                        Context.SaveChanges();

                        // Find if a timesheet exists for employee
                        if (model.EndTime.HasValue)
                        {
                            DateTime weekStart = GetWeekStart();

                            var existingTimesheet = Context.Timesheets.Where(t => t.EmployeeFK.Equals(model.StaffID) && t.WeekStart.Equals(weekStart)).SingleOrDefault();

                            if (existingTimesheet != null)
                            {
                                var existingRecord = existingTimesheet.TimesheetRecords.Where(r => r.TimesheetRecordWorkInstructions.Any(tr => tr.ShiftFK.Equals(model.ShiftID))).SingleOrDefault();

                                if (existingRecord == null)
                                {
                                    TimesheetRecord record = new TimesheetRecord()
                                    {
                                        TimesheetRecordPK = Guid.NewGuid(),
                                        StartDate = model.StartTime.HasValue ? model.StartTime.Value : DateTime.MinValue,
                                        EndDate = model.EndTime.HasValue ? model.EndTime.Value : DateTime.MinValue,
                                        WorkedHours = model.WorkedHours,
                                        WorkedMins = model.WorkedMins,
                                        BreakHours = model.BreakHours,
                                        BreakMins = model.BreakMins,
                                        TravelHours = model.TravelHours,
                                        TravelMins = model.TravelMins,
                                        IPVAllowance = model.IPV,
                                        PaidBreakAllowance = model.PaidBreak,
                                        OnCallAllowance = model.OnCall,
                                        TimesheetFK = existingTimesheet.TimesheetPK,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedByFK = GetUserContext().UserDetails.ID,
                                        UpdatedDate = DateTime.Now,
                                        UpdatedByFK = GetUserContext().UserDetails.ID
                                    };

                                    Context.TimesheetRecords.Add(record);

                                    SaveChanges();

                                    TimesheetRecordWorkInstruction trwi = new TimesheetRecordWorkInstruction()
                                    {
                                        TimesheetRecordWorkInstructionPK = Guid.NewGuid(),
                                        TimesheetRecordFK = record.TimesheetRecordPK,
                                        ShiftFK = model.ShiftID,
                                        CreatedDate = DateTime.Now,
                                        CreatedByFK = GetUserContext().UserDetails.ID,
                                        UpdatedDate = DateTime.Now,
                                        UpdatedByFK = GetUserContext().UserDetails.ID
                                    };

                                    Context.TimesheetRecordWorkInstructions.Add(trwi);

                                    SaveChanges();
                                }
                                else
                                {
                                    existingRecord.WorkedHours = model.WorkedHours;
                                    existingRecord.WorkedMins = model.WorkedMins;
                                    existingRecord.BreakHours = model.BreakHours;
                                    existingRecord.BreakMins = model.BreakMins;
                                    existingRecord.TravelHours = model.TravelHours;
                                    existingRecord.TravelMins = model.TravelMins;
                                    existingRecord.IPVAllowance = model.IPV;
                                    existingRecord.OnCallAllowance = model.OnCall;
                                    existingRecord.PaidBreakAllowance = model.PaidBreak;
                                    existingRecord.EndDate = model.EndTime.Value;
                                    existingRecord.UpdatedByFK = GetUserContext().UserDetails.ID;
                                    existingRecord.UpdatedDate = DateTime.Now;

                                    SaveChanges();
                                }

                                existingTimesheet.UpdatedByFK = GetUserContext().UserDetails.ID;
                                existingTimesheet.UpdatedDate = DateTime.Now;

                            }
                            else
                            {
                                Timesheet timesheet = new Timesheet()
                                {
                                    TimesheetPK = Guid.NewGuid(),
                                    EmployeeFK = model.StaffID,
                                    WeekStart = weekStart,
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedByFK = GetUserContext().UserDetails.ID,
                                    UpdatedDate = DateTime.Now,
                                    UpdatedByFK = GetUserContext().UserDetails.ID
                                };

                                Context.Timesheets.Add(timesheet);

                                SaveChanges();

                                TimesheetRecord record = new TimesheetRecord()
                                {
                                    TimesheetRecordPK = Guid.NewGuid(),
                                    StartDate = model.StartTime.HasValue ? model.StartTime.Value : DateTime.MinValue,
                                    EndDate = model.EndTime.HasValue ? model.EndTime.Value : DateTime.MinValue,
                                    WorkedHours = model.WorkedHours,
                                    WorkedMins = model.WorkedMins,
                                    BreakHours = model.BreakHours,
                                    BreakMins = model.BreakMins,
                                    TravelHours = model.TravelHours,
                                    TravelMins = model.TravelMins,
                                    IPVAllowance = model.IPV,
                                    PaidBreakAllowance = model.PaidBreak,
                                    OnCallAllowance = model.OnCall,
                                    TimesheetFK = timesheet.TimesheetPK,
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedByFK = GetUserContext().UserDetails.ID,
                                    UpdatedDate = DateTime.Now,
                                    UpdatedByFK = GetUserContext().UserDetails.ID
                                };

                                Context.TimesheetRecords.Add(record);

                                SaveChanges();

                                TimesheetRecordWorkInstruction trwi = new TimesheetRecordWorkInstruction()
                                {
                                    TimesheetRecordWorkInstructionPK = Guid.NewGuid(),
                                    TimesheetRecordFK = record.TimesheetRecordPK,
                                    ShiftFK = model.ShiftID,
                                    CreatedDate = DateTime.Now,
                                    CreatedByFK = GetUserContext().UserDetails.ID,
                                    UpdatedDate = DateTime.Now,
                                    UpdatedByFK = GetUserContext().UserDetails.ID
                                };

                                Context.TimesheetRecordWorkInstructions.Add(trwi);

                                SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        ShiftStaff shiftStaff = new ShiftStaff()
                        {
                            ShiftStaffPK = Guid.NewGuid(),
                            ShiftFK = model.ShiftID,
                            EmployeeFK = model.StaffID,
                            AgencyEmployeeName = model.AgencyEmployeeName != null ? model.AgencyEmployeeName : null,
                            StartTime = model.StartTime,
                            EndTime = model.EndTime,
                            NotOnShift = model.NotOnShift,
                            ReasonNotOn = model.ReasonNotOn,
                            IsActive = !model.NotOnShift,
                            CreatedDate = DateTime.Now,
                            CreatedByID = GetUserContext().UserDetails.ID,
                            UpdatedDate = DateTime.Now,
                            UpdatedByID = GetUserContext().UserDetails.ID,
                        };

                        Context.ShiftStaffs.Add(shiftStaff);
                        SaveChanges();

                        if (model.EndTime.HasValue)
                        {
                            DateTime weekStart = GetWeekStart();

                            var existingTimesheet = Context.Timesheets.Where(t => t.EmployeeFK.Equals(model.StaffID) && t.WeekStart.Equals(weekStart)).SingleOrDefault();

                            if (existingTimesheet != null)
                            {
                                var existingRecord = existingTimesheet.TimesheetRecords.Where(r => r.TimesheetRecordWorkInstructions.Any(tr => tr.ShiftFK.Equals(model.ShiftID))).SingleOrDefault();

                                if (existingRecord == null)
                                {
                                    TimesheetRecord record = new TimesheetRecord()
                                    {
                                        TimesheetRecordPK = Guid.NewGuid(),
                                        StartDate = model.StartTime.HasValue ? model.StartTime.Value : DateTime.MinValue,
                                        EndDate = model.EndTime.HasValue ? model.EndTime.Value : DateTime.MinValue,
                                        WorkedHours = model.WorkedHours,
                                        WorkedMins = model.WorkedMins,
                                        BreakHours = model.BreakHours,
                                        BreakMins = model.BreakMins,
                                        TravelHours = model.TravelHours,
                                        TravelMins = model.TravelMins,
                                        IPVAllowance = model.IPV,
                                        PaidBreakAllowance = model.PaidBreak,
                                        OnCallAllowance = model.OnCall,
                                        TimesheetFK = existingTimesheet.TimesheetPK,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        CreatedByFK = GetUserContext().UserDetails.ID,
                                        UpdatedDate = DateTime.Now,
                                        UpdatedByFK = GetUserContext().UserDetails.ID
                                    };

                                    Context.TimesheetRecords.Add(record);

                                    SaveChanges();

                                    TimesheetRecordWorkInstruction trwi = new TimesheetRecordWorkInstruction()
                                    {
                                        TimesheetRecordWorkInstructionPK = Guid.NewGuid(),
                                        TimesheetRecordFK = record.TimesheetRecordPK,
                                        ShiftFK = model.ShiftID,
                                        CreatedDate = DateTime.Now,
                                        CreatedByFK = GetUserContext().UserDetails.ID,
                                        UpdatedDate = DateTime.Now,
                                        UpdatedByFK = GetUserContext().UserDetails.ID
                                    };

                                    Context.TimesheetRecordWorkInstructions.Add(trwi);

                                    SaveChanges();
                                }
                                else
                                {
                                    existingRecord.WorkedHours = model.WorkedHours;
                                    existingRecord.WorkedMins = model.WorkedMins;
                                    existingRecord.BreakHours = model.BreakHours;
                                    existingRecord.BreakMins = model.BreakMins;
                                    existingRecord.TravelHours = model.TravelHours;
                                    existingRecord.TravelMins = model.TravelMins;
                                    existingRecord.IPVAllowance = model.IPV;
                                    existingRecord.OnCallAllowance = model.OnCall;
                                    existingRecord.PaidBreakAllowance = model.PaidBreak;
                                    existingRecord.EndDate = model.EndTime.Value;
                                    existingRecord.UpdatedByFK = GetUserContext().UserDetails.ID;
                                    existingRecord.UpdatedDate = DateTime.Now;

                                    SaveChanges();
                                }

                                existingTimesheet.UpdatedByFK = GetUserContext().UserDetails.ID;
                                existingTimesheet.UpdatedDate = DateTime.Now;

                            }
                            else
                            {
                                Timesheet timesheet = new Timesheet()
                                {
                                    TimesheetPK = Guid.NewGuid(),
                                    EmployeeFK = model.StaffID,
                                    WeekStart = weekStart,
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedByFK = GetUserContext().UserDetails.ID,
                                    UpdatedDate = DateTime.Now,
                                    UpdatedByFK = GetUserContext().UserDetails.ID
                                };

                                Context.Timesheets.Add(timesheet);

                                SaveChanges();

                                TimesheetRecord record = new TimesheetRecord()
                                {
                                    TimesheetRecordPK = Guid.NewGuid(),
                                    StartDate = model.StartTime.HasValue ? model.StartTime.Value : DateTime.MinValue,
                                    EndDate = model.EndTime.HasValue ? model.EndTime.Value : DateTime.MinValue,
                                    WorkedHours = model.WorkedHours,
                                    WorkedMins = model.WorkedMins,
                                    BreakHours = model.BreakHours,
                                    BreakMins = model.BreakMins,
                                    TravelHours = model.TravelHours,
                                    TravelMins = model.TravelMins,
                                    IPVAllowance = model.IPV,
                                    PaidBreakAllowance = model.PaidBreak,
                                    OnCallAllowance = model.OnCall,
                                    TimesheetFK = timesheet.TimesheetPK,
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedByFK = GetUserContext().UserDetails.ID,
                                    UpdatedDate = DateTime.Now,
                                    UpdatedByFK = GetUserContext().UserDetails.ID
                                };

                                Context.TimesheetRecords.Add(record);

                                SaveChanges();

                                TimesheetRecordWorkInstruction trwi = new TimesheetRecordWorkInstruction()
                                {
                                    TimesheetRecordWorkInstructionPK = Guid.NewGuid(),
                                    TimesheetRecordFK = record.TimesheetRecordPK,
                                    ShiftFK = model.ShiftID,
                                    CreatedDate = DateTime.Now,
                                    CreatedByFK = GetUserContext().UserDetails.ID,
                                    UpdatedDate = DateTime.Now,
                                    UpdatedByFK = GetUserContext().UserDetails.ID
                                };

                                Context.TimesheetRecordWorkInstructions.Add(trwi);

                                SaveChanges();
                            }
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, new Dictionary<string, object>
                    {
                        { nameof(model), model },
                    });

                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public void PushShiftVehicle(ShiftVehicleModel model, bool restrictedUpdate)
        {
            try
            {
                var shift = Context.Shifts.Where(a => a.ShiftPK == model.ShiftID).Single();

                // if shift complete or cancelled don't update
                if (shift.ShiftComplete || shift.IsCancelled)
                {
                    return;
                }

                var shiftVehicle = Context.ShiftVehicles.Where(s => s.VehicleFK == model.VehicleID && s.ShiftFK == model.ShiftID).SingleOrDefault();

                if (shiftVehicle == null)
                {
                    shiftVehicle = new ShiftVehicle();
                    shiftVehicle.ShiftVehiclePK = Guid.NewGuid();
                    shiftVehicle.ShiftFK = model.ShiftID;
                    shiftVehicle.VehicleFK = model.VehicleID;
                    shiftVehicle.EmployeeFK = model.StaffID;
                    shiftVehicle.CreatedByID = GetUserContext().UserDetails.ID;
                    shiftVehicle.CreatedDate = DateTime.Now;

                    Context.ShiftVehicles.Add(shiftVehicle);
                }

                shiftVehicle.EmployeeFK = model.StaffID;
                shiftVehicle.StartMileage = model.StartMileage;
                shiftVehicle.IsActive = !model.NotOnShift;

                if (!restrictedUpdate)
                {
                    shiftVehicle.EndMileage = model.EndMileage;
                    shiftVehicle.LoadingSheetCompleted = model.LoadingSheetCompleted;
                    shiftVehicle.NotOnShift = model.NotOnShift;
                    shiftVehicle.ReasonNotOnShift = model.ReasonNotOn ?? "";
                    shiftVehicle.UpdatedDate = DateTime.Now;
                    shiftVehicle.UpdatedByID = GetUserContext().UserDetails.ID;
                }

                Context.SaveChanges();
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, new Dictionary<string, object>
                {
                    { nameof(model), model },
                    { nameof(restrictedUpdate), restrictedUpdate }
                });

                ExceptionDispatchInfo.Capture(e).Throw();
            }
        }

        public void PushVehicleChecklist(List<VehicleChecklistModel> checklists)
        {
            foreach (VehicleChecklistModel model in checklists)
            {
                // ResetContext within each loop to avoid Context.VehicleChecklists returning a local VehicleChecklist entity
                // lazy loading doesn't work on newly added entities held in the local context (this was causing a null reference exception)
                ResetContext();

                var existingChecklist = Context.VehicleChecklists
                    .Where(a => a.ShiftFK == model.ShiftID && a.VehicleFK == model.VehicleID && a.StartOfShift == model.StartOfShift && a.IsActive)
                    .OrderByDescending(a => a.CreatedDate)
                    .ThenByDescending(a => a.VehicleChecklistPK) // for cases when created date is identical
                    .FirstOrDefault();

                using (var transaction = Context.Database.BeginTransaction())
                {
                    try
                    {
                        if (existingChecklist != null) // Already exists so update
                        {
                            // if shift complete or cancelled continue
                            if (existingChecklist.Shift.ShiftComplete || existingChecklist.Shift.IsCancelled)
                            {
                                continue;
                            }

                            // update checklist
                            existingChecklist.UpdatedDate = DateTime.Now;
                            existingChecklist.UpdatedByID = GetUserContext().UserDetails.ID;

                            if (!string.IsNullOrWhiteSpace(model.ImageData) && !string.IsNullOrWhiteSpace(model.ImageFilename))
                            {
                                if (existingChecklist.PhotoFileFK != null)
                                {
                                    var deleteFile = Context.Files.Where(a => a.FilePK == existingChecklist.PhotoFileFK).FirstOrDefault();
                                    if (deleteFile != null)
                                    {
                                        Context.Files.Remove(deleteFile);
                                    }
                                }

                                var file = new File();
                                file.FilePK = Guid.NewGuid();
                                file.FileDownloadName = model.ImageFilename;
                                file.Data = Convert.FromBase64String(model.ImageData);
                                file.ContentType = "image/jpeg";
                                file.Longitude = 0;
                                file.Latitude = 0;
                                file.IsActive = true;
                                file.CreatedByFK = GetUserContext().UserDetails.ID;
                                file.CreatedDate = DateTime.Now;
                                file.UpdatedByFK = GetUserContext().UserDetails.ID;
                                file.UpdatedDate = DateTime.Now;
                                Context.Files.Add(file);

                                existingChecklist.File = file;
                            }

                            Context.SaveChanges();

                            // update answers and defects
                            if (existingChecklist.VehicleChecklistAnswers != null && model.Answers != null) // Update Answers, do not need to create
                            {
                                foreach (var answer in model.Answers)
                                {
                                    var existingAnswer = existingChecklist.VehicleChecklistAnswers.Where(s => s.ChecklistQuestionFK == answer.ChecklistQuestionID).FirstOrDefault();
                                    if (existingAnswer != null)
                                    {
                                        existingAnswer.Answer = answer.Answer;
                                        existingAnswer.UpdatedDate = DateTime.Now;
                                        existingAnswer.UpdatedByID = GetUserContext().UserDetails.ID;
                                    }
                                    Context.SaveChanges();
                                }

                                if (model.Defects != null) // There may be new defects
                                {
                                    foreach (var defect in model.Defects)
                                    {
                                        var defectAnswer = existingChecklist.VehicleChecklistAnswers.Where(a => a.ChecklistQuestionFK.Equals(defect.ChecklistQuestionID)).FirstOrDefault();

                                        if (defectAnswer != null)
                                        {
                                            var existingDefect = Context.VehicleChecklistDefects.Where(d => d.VehicleChecklistAnswerFK.Equals(defectAnswer.VehicleChecklistAnswerPK)).FirstOrDefault();

                                            if (existingDefect != null) // Exists so update
                                            {
                                                existingDefect.Description = defect.Defect;
                                                existingDefect.UpdatedDate = DateTime.Now;
                                                existingDefect.UpdatedByID = GetUserContext().UserDetails.ID;

                                                Context.SaveChanges();
                                            }
                                            else // Does not exist so create
                                            {
                                                VehicleChecklistDefect newDefect = new VehicleChecklistDefect()
                                                {
                                                    VehicleChecklistDefectPK = Guid.NewGuid(),
                                                    Description = defect.Defect,
                                                    VehicleChecklistAnswerFK = defectAnswer.VehicleChecklistAnswerPK,
                                                    IsActive = true,
                                                    CreatedDate = DateTime.Now,
                                                    UpdatedDate = DateTime.Now,
                                                    CreatedByID = GetUserContext().UserDetails.ID,
                                                    UpdatedByID = GetUserContext().UserDetails.ID
                                                };

                                                defectAnswer.VehicleChecklistDefects.Add(newDefect);

                                                Context.SaveChanges();
                                            }
                                        }
                                    }

                                }
                            }

                        }
                        else // New checklist
                        {
                            VehicleChecklist newChecklist = new VehicleChecklist()
                            {
                                VehicleChecklistPK = model.VehicleChecklistID,
                                VehicleFK = model.VehicleID,
                                ShiftFK = model.ShiftID,
                                ChecklistFK = model.ChecklistID,
                                StartOfShift = model.StartOfShift,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                CreatedByID = GetUserContext().UserDetails.ID,
                                UpdatedByID = GetUserContext().UserDetails.ID
                            };

                            if (!string.IsNullOrWhiteSpace(model.ImageData) && !string.IsNullOrWhiteSpace(model.ImageFilename))
                            {
                                var file = new File();
                                file.FilePK = Guid.NewGuid();
                                file.FileDownloadName = model.ImageFilename;
                                file.Data = Convert.FromBase64String(model.ImageData);
                                file.ContentType = "image/jpeg";
                                file.Longitude = 0;
                                file.Latitude = 0;
                                file.IsActive = true;
                                file.CreatedByFK = GetUserContext().UserDetails.ID;
                                file.CreatedDate = DateTime.Now;
                                file.UpdatedByFK = GetUserContext().UserDetails.ID;
                                file.UpdatedDate = DateTime.Now;
                                Context.Files.Add(file);

                                newChecklist.File = file;
                            }

                            foreach (VehicleChecklistAnswerModel answer in model.Answers)
                            {
                                VehicleChecklistAnswer newAnswer = new VehicleChecklistAnswer()
                                {
                                    VehicleChecklistAnswerPK = Guid.NewGuid(),
                                    VehicleChecklistFK = newChecklist.VehicleChecklistPK,
                                    ChecklistQuestionFK = answer.ChecklistQuestionID,
                                    Answer = answer.Answer,
                                    CreatedDate = DateTime.Now,
                                    UpdatedDate = DateTime.Now,
                                    CreatedByID = GetUserContext().UserDetails.ID,
                                    UpdatedByID = GetUserContext().UserDetails.ID
                                };

                                newChecklist.VehicleChecklistAnswers.Add(newAnswer);
                            }

                            Context.VehicleChecklists.Add(newChecklist);

                            Context.SaveChanges();

                            if (model.Defects != null)
                            {
                                foreach (VehicleChecklistAnswerDefectModel defect in model.Defects)
                                {
                                    var answer = newChecklist.VehicleChecklistAnswers.Where(a => a.VehicleChecklistFK.Equals(newChecklist.VehicleChecklistPK) &&
                                        a.ChecklistQuestionFK.Equals(defect.ChecklistQuestionID)).FirstOrDefault();

                                    if (answer != null) // Answer exists
                                    {
                                        VehicleChecklistDefect newDefect = new VehicleChecklistDefect()
                                        {
                                            VehicleChecklistDefectPK = Guid.NewGuid(),
                                            VehicleChecklistAnswerFK = answer.VehicleChecklistAnswerPK,
                                            Description = defect.Defect,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            UpdatedDate = DateTime.Now,
                                            CreatedByID = GetUserContext().UserDetails.ID,
                                            UpdatedByID = GetUserContext().UserDetails.ID
                                        };

                                        answer.VehicleChecklistDefects.Add(newDefect);

                                        Context.SaveChanges();
                                    }
                                }
                            }
                        }

                        Context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        if (transaction != null)
                        {
                            transaction.Rollback();
                        }

                        e.Data.AddSafely(Global.ExceptionDataKey, checklists);
                        ExceptionDispatchInfo.Capture(e).Throw();
                    }
                }
            }
        }

        public void PushTask(TaskModel model)
        {
            try
            {
                var task = Context.Tasks.Where(t => t.TaskPK.Equals(model.TaskID)).SingleOrDefault();

                if (task != null)
                {
                    task.StartTime = model.StartTime;
                    task.EndTime = model.EndTime;
                    task.Notes = model.Notes;
                    task.Longitude = model.Longitude;
                    task.Latitude = model.Latitude;
                    task.WeatherConditionFK = model.WeatherConditionID;
                    task.UpdatedByID = GetUserContext().UserDetails.ID;
                    task.UpdatedDate = DateTime.Now;

                    Context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, model);
                ExceptionDispatchInfo.Capture(e).Throw();
            }
        }

        public void PushTaskChecklist(TaskChecklistModel model)
        {
            var taskChecklist = Context.TaskChecklists.Where(a => a.ChecklistFK == model.ChecklistID && a.TaskFK == model.TaskID && a.IsActive).SingleOrDefault();

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (taskChecklist != null) // Exists so update answers
                    {
                        foreach (var answer in model.Answers)
                        {
                            var existingAnswer = taskChecklist.TaskChecklistAnswers.Where(a => a.TaskChecklistAnswerPK == answer.TaskChecklistAnswerID && a.IsActive).SingleOrDefault();

                            if (existingAnswer != null)
                            {
                                existingAnswer.Answer = answer.Answer;
                                existingAnswer.Reason = answer.Reason;
                                existingAnswer.UpdatedByID = GetUserContext().UserDetails.ID;
                                existingAnswer.UpdatedDate = answer.Time;

                                Context.SaveChanges();
                            }
                        }
                    }
                    else // Doesn't exist so create
                    {
                        if (model.Answers != null)
                        {
                            var newChecklist = new TaskChecklist
                            {
                                TaskChecklistPK = Guid.NewGuid(),
                                TaskFK = model.TaskID,
                                ChecklistFK = model.ChecklistID,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                CreatedByID = GetUserContext().UserDetails.ID,
                                UpdatedByID = GetUserContext().UserDetails.ID
                            };

                            Context.SaveChanges();

                            foreach (var answer in model.Answers)
                            {
                                var newAnswer = new TaskChecklistAnswer()
                                {
                                    TaskChecklistAnswerPK = answer.TaskChecklistAnswerID,
                                    TaskChecklistFK = newChecklist.TaskChecklistPK,
                                    ChecklistQuestionFK = answer.ChecklistQuestionID,
                                    Answer = answer.Answer,
                                    Reason = answer.Reason,
                                    IsActive = true,
                                    CreatedDate = answer.Time,
                                    UpdatedDate = answer.Time,
                                    CreatedByID = GetUserContext().UserDetails.ID,
                                    UpdatedByID = GetUserContext().UserDetails.ID
                                };

                                newChecklist.TaskChecklistAnswers.Add(newAnswer);

                                Context.SaveChanges();
                            }

                            Context.TaskChecklists.Add(newChecklist);

                        }
                    }

                    Context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public void PushTaskEquipment(List<TaskEquipmentBaseModel> tasksModel)
        {
            try
            {
                if (tasksModel == null || !tasksModel.Any())
                {
                    return;
                }

                foreach (var taskModel in tasksModel)
                {
                    taskModel.TaskEquipments = taskModel.TaskEquipments ?? new List<TaskEquipmentModel>();

                    var keepTaskEquipmentIDs = taskModel.TaskEquipments.Select(a => a.TaskEquipmentID).ToList();

                    // delete task equipment
                    var deleteTaskEquipment = Context.TaskEquipments.Where(a => a.TaskFK == taskModel.TaskID && !keepTaskEquipmentIDs.Contains(a.TaskEquipmentPK));
                    foreach (var taskEquipment in deleteTaskEquipment)
                    {
                        taskEquipment.IsActive = false;
                    }

                    // insert, update and delete
                    foreach (var taskEquipmentModel in taskModel.TaskEquipments)
                    {
                        taskEquipmentModel.TaskEquipmentAssets = taskEquipmentModel.TaskEquipmentAssets ?? new List<TaskEquipmentAssetModel>();

                        var taskEquipment = Context.TaskEquipments.Where(a => a.TaskEquipmentPK == taskEquipmentModel.TaskEquipmentID).SingleOrDefault();
                        if (taskEquipment == null)
                        {
                            // new task equipment and assets
                            taskEquipment = new TaskEquipment();
                            taskEquipment.TaskEquipmentPK = taskEquipmentModel.TaskEquipmentID;
                            taskEquipment.TaskFK = taskEquipmentModel.TaskID;
                            taskEquipment.EquipmentFK = taskEquipmentModel.EquipmentID;
                            taskEquipment.Quantity = taskEquipmentModel.Quantity;
                            taskEquipment.IsActive = true;
                            taskEquipment.CreatedByID = GetUserContext().UserDetails.ID;
                            taskEquipment.CreatedDate = DateTime.Now;
                            taskEquipment.UpdatedByID = GetUserContext().UserDetails.ID;
                            taskEquipment.UpdatedDate = DateTime.Now;
                            Context.TaskEquipments.Add(taskEquipment);

                            foreach (var taskEquipmentAssetModel in taskEquipmentModel.TaskEquipmentAssets)
                            {
                                var taskEquipmentAsset = new TaskEquipmentAsset();
                                taskEquipmentAsset.TaskEquipmentAssetPK = taskEquipmentAssetModel.TaskEquipmentAssetID;
                                taskEquipmentAsset.TaskEquipmentFK = taskEquipmentAssetModel.TaskEquipmentID;
                                taskEquipmentAsset.ItemNumber = taskEquipmentAssetModel.ItemNumber;
                                taskEquipmentAsset.AssetNumber = taskEquipmentAssetModel.AssetNumber;
                                Context.TaskEquipmentAssets.Add(taskEquipmentAsset);
                            }
                        }
                        else
                        {
                            // update task equipment
                            taskEquipment.Quantity = taskEquipmentModel.Quantity;
                            taskEquipment.UpdatedByID = GetUserContext().UserDetails.ID;
                            taskEquipment.UpdatedDate = DateTime.Now;

                            // delete task equipment assets
                            var keepTaskEquipmentAssetPKs = taskEquipmentModel.TaskEquipmentAssets.Select(a => a.TaskEquipmentAssetID).ToList();
                            var deleteTaskEquipmentAssets = taskEquipment.TaskEquipmentAssets.Where(a => !keepTaskEquipmentAssetPKs.Contains(a.TaskEquipmentAssetPK));
                            Context.TaskEquipmentAssets.RemoveRange(deleteTaskEquipmentAssets);

                            foreach (var taskEquipmentAssetModel in taskEquipmentModel.TaskEquipmentAssets)
                            {
                                var taskEquipmentAsset = Context.TaskEquipmentAssets.Where(a => a.TaskEquipmentAssetPK == taskEquipmentAssetModel.TaskEquipmentAssetID).SingleOrDefault();
                                if (taskEquipmentAsset == null)
                                {
                                    // new equipment asset
                                    taskEquipmentAsset = new TaskEquipmentAsset();
                                    taskEquipmentAsset.TaskEquipmentAssetPK = taskEquipmentAssetModel.TaskEquipmentAssetID;
                                    taskEquipmentAsset.TaskEquipmentFK = taskEquipmentAssetModel.TaskEquipmentID;
                                    taskEquipmentAsset.ItemNumber = taskEquipmentAssetModel.ItemNumber;
                                    taskEquipmentAsset.AssetNumber = taskEquipmentAssetModel.AssetNumber;
                                    Context.TaskEquipmentAssets.Add(taskEquipmentAsset);
                                }
                                else
                                {
                                    // update equipment asset
                                    taskEquipmentAsset.ItemNumber = taskEquipmentAssetModel.ItemNumber;
                                    taskEquipmentAsset.AssetNumber = taskEquipmentAssetModel.AssetNumber;
                                }
                            }
                        }
                    }
                }

                Context.SaveChanges();
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, tasksModel);
                ExceptionDispatchInfo.Capture(e).Throw();
            }
        }

        public void PushArea(AreaModel model)
        {
            var area = Context.AreaCallAreas.Where(a => a.AreaCallAreaPK.Equals(model.AreaID) && a.IsActive).SingleOrDefault();

            if (area != null)
            {
                using (var transaction = Context.Database.BeginTransaction())
                {
                    try
                    {
                        area.Reference = model.Reference;
                        area.PersonsName = model.PersonsName;
                        area.ExtraDetails = model.Details;
                        area.UpdatedByFK = GetUserContext().UserDetails.ID;
                        area.UpdatedDate = DateTime.Now;

                        Context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        if (transaction != null)
                        {
                            transaction.Rollback();
                        }

                        e.Data.AddSafely(Global.ExceptionDataKey, model);
                        ExceptionDispatchInfo.Capture(e).Throw();
                    }
                }
            }
        }

        public Guid PushAccident(AccidentModel model)
        {
            var existingAccident = Context.Accidents.Where(a => a.AccidentPK.Equals(model.AccidentID)).SingleOrDefault();

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (existingAccident != null) // Exists so update
                    {
                        existingAccident.AccidentDescription = model.AccidentDetails;
                        existingAccident.AccidentDate = model.AccidentDate;
                        existingAccident.Location = model.Location;
                        existingAccident.Registrations = model.Registrations;
                        existingAccident.PersonReporting = model.PersonReporting;
                        existingAccident.PeopleInvolved = model.PeopleInvolved;
                        existingAccident.Witnesses = model.Witnesses;
                        existingAccident.ActionTaken = model.ActionTaken;
                        existingAccident.UpdatedByID = GetUserContext().UserDetails.ID;
                        existingAccident.UpdatedDate = DateTime.Now;

                        Context.SaveChanges();
                        transaction.Commit();

                        return existingAccident.AccidentPK;
                    }
                    else
                    {
                        Accident newAccident = new Accident();

                        newAccident.AccidentPK = Guid.NewGuid();
                        newAccident.AccidentDescription = model.AccidentDetails;
                        newAccident.AccidentDate = model.AccidentDate;
                        newAccident.Location = model.Location;
                        newAccident.Registrations = model.Registrations;
                        newAccident.PersonReporting = model.PersonReporting;
                        newAccident.PeopleInvolved = model.PeopleInvolved;
                        newAccident.Witnesses = model.Witnesses;
                        newAccident.ActionTaken = model.ActionTaken;
                        newAccident.UpdatedByID = GetUserContext().UserDetails.ID;
                        newAccident.UpdatedDate = DateTime.Now;
                        newAccident.CreatedByID = GetUserContext().UserDetails.ID;
                        newAccident.CreatedDate = DateTime.Now;
                        newAccident.ShiftFK = model.ShiftID;
                        newAccident.IsActive = true;

                        Context.Accidents.Add(newAccident);

                        Context.SaveChanges();

                        transaction.Commit();

                        return newAccident.AccidentPK;
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                    throw;
                }
            }
        }

        public bool PushAccidentPhoto(AccidentPhotoModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    File newFile = new File()
                    {
                        FilePK = Guid.NewGuid(),
                        FileDownloadName = "Accident Photo " + model.Time.ToString(),
                        ContentType = ".jpg",
                        Data = System.Convert.FromBase64String(model.DataString),
                        CreatedDate = model.Time,
                        UpdatedDate = DateTime.Now,
                        CreatedByFK = GetUserContext().UserDetails.ID,
                        UpdatedByFK = GetUserContext().UserDetails.ID,
                        IsActive = true,
                        Longitude = model.Longitude,
                        Latitude = model.Latitude
                    };

                    Context.Files.Add(newFile);

                    Context.SaveChanges();

                    AccidentPhoto photo = new AccidentPhoto()
                    {
                        AccidentPhotoPK = Guid.NewGuid(),
                        AccidentFK = model.AccidentID,
                        CreatedDate = model.Time,
                        UpdatedDate = DateTime.Now,
                        CreatedByID = GetUserContext().UserDetails.ID,
                        UpdatedByID = GetUserContext().UserDetails.ID,
                        IsActive = true,
                        FileFK = newFile.FilePK
                    };

                    Context.AccidentPhotos.Add(photo);

                    Context.SaveChanges();

                    transaction.Commit();

                    return true;
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                    throw;
                }
            }
        }

        public Guid PushObservation(ObservationModel model)
        {
            var existingObservation = Context.Observations.Where(a => a.ObservationPK.Equals(model.ObservationID)).SingleOrDefault();

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (existingObservation != null) // Exists so update
                    {
                        existingObservation.PeopleInvolved = model.PeopleInvolved;
                        existingObservation.ObservationDescription = model.ObservationDescription;
                        existingObservation.Location = model.Location;
                        existingObservation.CauseOfProblem = model.CauseOfProblem;
                        existingObservation.ObservationDate = model.ObservationDate;
                        existingObservation.UpdatedByID = GetUserContext().UserDetails.ID;
                        existingObservation.UpdatedDate = DateTime.Now;

                        Context.SaveChanges();
                        transaction.Commit();

                        return existingObservation.ObservationPK;
                    }
                    else
                    {
                        Observation newObservation = new Observation();

                        newObservation.ObservationPK = Guid.NewGuid();
                        newObservation.ShiftFK = model.ShiftID;
                        newObservation.PeopleInvolved = model.PeopleInvolved;
                        newObservation.ObservationDescription = model.ObservationDescription;
                        newObservation.Location = model.Location;
                        newObservation.CauseOfProblem = model.CauseOfProblem;
                        newObservation.ObservationDate = model.ObservationDate;
                        newObservation.UpdatedByID = GetUserContext().UserDetails.ID;
                        newObservation.UpdatedDate = DateTime.Now;
                        newObservation.CreatedByID = GetUserContext().UserDetails.ID;
                        newObservation.CreatedDate = DateTime.Now;
                        newObservation.IsActive = true;

                        Context.Observations.Add(newObservation);

                        Context.SaveChanges();

                        transaction.Commit();

                        return newObservation.ObservationPK;
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                    throw;
                }
            }
        }

        public bool PushObservationPhoto(ObservationPhotoModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    File newFile = new File()
                    {
                        FilePK = Guid.NewGuid(),
                        FileDownloadName = "Observation Photo " + model.Time.ToString(),
                        ContentType = ".jpg",
                        Data = System.Convert.FromBase64String(model.DataString),
                        CreatedDate = model.Time,
                        UpdatedDate = DateTime.Now,
                        CreatedByFK = GetUserContext().UserDetails.ID,
                        UpdatedByFK = GetUserContext().UserDetails.ID,
                        IsActive = true,
                        Longitude = model.Longitude,
                        Latitude = model.Latitude
                    };

                    Context.Files.Add(newFile);

                    Context.SaveChanges();

                    ObservationPhoto photo = new ObservationPhoto()
                    {
                        ObservationPhotoPK = Guid.NewGuid(),
                        ObservationFK = model.ObservationID,
                        CreatedDate = model.Time,
                        UpdatedDate = DateTime.Now,
                        CreatedByID = GetUserContext().UserDetails.ID,
                        UpdatedByID = GetUserContext().UserDetails.ID,
                        IsActive = true,
                        FileFK = newFile.FilePK
                    };

                    Context.ObservationPhotos.Add(photo);

                    Context.SaveChanges();

                    transaction.Commit();

                    return true;
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                    throw;
                }
            }
        }

        public void PushNote(NoteModel model)
        {
            var existingNote = Context.Notes.Where(n => n.NotePK.Equals(model.NoteID)).SingleOrDefault();

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (existingNote != null) // Exists so update
                    {
                        existingNote.Detail = model.NoteDetails;

                        Context.SaveChanges();
                        transaction.Commit();
                    }
                    else
                    {
                        Note newNote = new Note()
                        {
                            NotePK = model.NoteID,
                            ShiftFK = model.ShiftID,
                            Detail = model.NoteDetails,
                            NoteDate = model.NoteDate,
                            IsActive = true,
                            CreatedByID = GetUserContext().UserDetails.ID,
                            CreatedDate = DateTime.Now
                        };

                        Context.Notes.Add(newNote);

                        Context.SaveChanges();

                        transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public void PushMaintenanceChecklist(List<MaintenanceCheckModel> checklists)
        {
            foreach (MaintenanceCheckModel model in checklists)
            {
                var existingChecklist = Context.MaintenanceChecks.Where(vc => vc.WorksInstructionFK == model.ActivityID && vc.Time.Equals(model.Time) && vc.IsActive).FirstOrDefault();

                using (var transaction = Context.Database.BeginTransaction())
                {
                    try
                    {
                        if (existingChecklist != null) // Already exists so update
                        {
                            existingChecklist.UpdatedDate = DateTime.Now;
                            existingChecklist.UpdatedByID = GetUserContext().UserDetails.ID;

                            Context.SaveChanges();

                            if (existingChecklist.MaintenanceCheckAnswers != null) // Update Answers, do not need to create
                            {
                                foreach (var answer in model.Answers)
                                {
                                    var existingAnswer = existingChecklist.MaintenanceCheckAnswers.Where(s => s.ChecklistQuestionFK == answer.ChecklistQuestionID).FirstOrDefault();

                                    existingAnswer.Answer = answer.Answer;
                                    existingAnswer.UpdatedDate = DateTime.Now;
                                    existingAnswer.UpdatedByID = GetUserContext().UserDetails.ID;

                                    Context.SaveChanges();
                                }
                            }

                            if (model.Details != null) // There may be new details
                            {
                                foreach (var defect in model.Details)
                                {
                                    //var existingDefect = Context.MaintenanceCheckDetails.Where(d => d.MaintenanceCheckAnswer.ChecklistQuestionFK.Equals(defect.ChecklistQuestionID)
                                    //    && d.MaintenanceCheckAnswer.MaintenanceCheckFK.Equals(defect.MaintenanceCheckID)).SingleOrDefault();

                                    //if (existingDefect != null) // Exists so update
                                    //{
                                    //    existingDefect.Description = defect.Description;
                                    //    existingDefect.UpdatedDate = DateTime.Now;
                                    //    existingDefect.UpdatedByID = GetUserProfile(GetUserContext()).Profile.UserID;

                                    //    Context.SaveChanges();
                                    //}
                                    //else // Does not exist so create
                                    //{
                                    //    var answer = existingChecklist.MaintenanceCheckAnswers.Where(a => a.MaintenanceCheckFK.Equals(defect.MaintenanceCheckID) &&
                                    //    a.ChecklistQuestionFK.Equals(defect.ChecklistQuestionID)).SingleOrDefault();

                                    //    if (answer != null)
                                    //    {
                                    //        MaintenanceCheckDetail newDefect = new MaintenanceCheckDetail()
                                    //        {
                                    //            MaintenanceCheckDetailPK = Guid.NewGuid(),
                                    //            Description = defect.Description,
                                    //            MaintenanceCheckAnswerFK = answer.MaintenanceCheckAnswerPK,
                                    //            IsActive = true,
                                    //            CreatedDate = DateTime.Now,
                                    //            UpdatedDate = DateTime.Now,
                                    //            CreatedByID = GetUserProfile(GetUserContext()).Profile.UserID,
                                    //            UpdatedByID = GetUserProfile(GetUserContext()).Profile.UserID
                                    //        };

                                    //        answer.MaintenanceCheckDetails.Add(newDefect);

                                    //        Context.SaveChanges();
                                    //    }
                                    //}


                                    var defectAnswer = existingChecklist.MaintenanceCheckAnswers.Where(a => a.ChecklistQuestionFK == defect.ChecklistQuestionID).FirstOrDefault();

                                    if (defectAnswer != null)
                                    {
                                        var existingDefect = Context.MaintenanceCheckDetails.Where(d => d.MaintenanceCheckAnswerFK == defectAnswer.MaintenanceCheckAnswerPK).FirstOrDefault();

                                        if (existingDefect != null) // Exists so update
                                        {
                                            existingDefect.Description = defect.Description;
                                            existingDefect.UpdatedDate = DateTime.Now;
                                            existingDefect.UpdatedByID = GetUserContext().UserDetails.ID;

                                            Context.SaveChanges();
                                        }
                                        else // Does not exist so create
                                        {
                                            MaintenanceCheckDetail newDefect = new MaintenanceCheckDetail()
                                            {
                                                MaintenanceCheckDetailPK = Guid.NewGuid(),
                                                Description = defect.Description,
                                                MaintenanceCheckAnswerFK = defectAnswer.MaintenanceCheckAnswerPK,
                                                IsActive = true,
                                                CreatedDate = DateTime.Now,
                                                UpdatedDate = DateTime.Now,
                                                CreatedByID = GetUserContext().UserDetails.ID,
                                                UpdatedByID = GetUserContext().UserDetails.ID
                                            };

                                            defectAnswer.MaintenanceCheckDetails.Add(newDefect);

                                            Context.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                        else // New checklist
                        {
                            MaintenanceCheck newChecklist = new MaintenanceCheck()
                            {
                                MaintenanceCheckPK = model.MaintenanceCheckID,
                                WorksInstructionFK = model.ActivityID,
                                Time = model.Time,
                                Description = model.Description ?? "",
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                CreatedByID = GetUserContext().UserDetails.ID,
                                UpdatedByID = GetUserContext().UserDetails.ID
                            };

                            if (model.Answers != null)
                            {
                                foreach (MaintenanceCheckAnswerModel answer in model.Answers)
                                {
                                    MaintenanceCheckAnswer newAnswer = new MaintenanceCheckAnswer()
                                    {
                                        //YOU SHOULD NEVER CREATE THE ID IN THE WEBSERVICES. IF THIS REQUEST SUCCESS, BUT ANOTHER FAILS, THIS IS GOING TO
                                        //CREATE ANOTHER ANSWER. WE ALREADY GOT A COMPLAINS ABOUT THIS ISSUE.
                                        MaintenanceCheckAnswerPK = Guid.NewGuid(),
                                        MaintenanceCheckFK = newChecklist.MaintenanceCheckPK,
                                        ChecklistQuestionFK = answer.ChecklistQuestionID,
                                        Answer = answer.Answer,
                                        IsActive = true,
                                        CreatedDate = DateTime.Now,
                                        UpdatedDate = DateTime.Now,
                                        CreatedByID = GetUserContext().UserDetails.ID,
                                        UpdatedByID = GetUserContext().UserDetails.ID
                                    };

                                    newChecklist.MaintenanceCheckAnswers.Add(newAnswer);
                                }
                            }

                            Context.MaintenanceChecks.Add(newChecklist);

                            Context.SaveChanges();

                            if (model.Details != null)
                            {
                                foreach (MaintenanceCheckDetailModel defect in model.Details)
                                {
                                    var answer = newChecklist.MaintenanceCheckAnswers.Where(a =>
                                        a.MaintenanceCheckFK == newChecklist.MaintenanceCheckPK &&
                                        a.ChecklistQuestionFK == defect.ChecklistQuestionID)
                                        .FirstOrDefault();

                                    if (answer != null) // Answer exists
                                    {
                                        MaintenanceCheckDetail newDefect = new MaintenanceCheckDetail()
                                        {
                                            MaintenanceCheckDetailPK = Guid.NewGuid(),
                                            MaintenanceCheckAnswerFK = answer.MaintenanceCheckAnswerPK,
                                            Description = defect.Description,
                                            IsActive = true,
                                            CreatedDate = DateTime.Now,
                                            UpdatedDate = DateTime.Now,
                                            CreatedByID = GetUserContext().UserDetails.ID,
                                            UpdatedByID = GetUserContext().UserDetails.ID
                                        };

                                        answer.MaintenanceCheckDetails.Add(newDefect);

                                        Context.SaveChanges();
                                    }
                                }
                            }
                        }

                        Context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        if (transaction != null)
                        {
                            transaction.Rollback();
                        }

                        e.Data.AddSafely(Global.ExceptionDataKey, checklists);
                        ExceptionDispatchInfo.Capture(e).Throw();
                    }
                }
            }
        }

        public void PushMaintenanceCheckDetailPhoto(MaintenanceCheckDetailPhotoModel model)
        {
            var detail = Context.MaintenanceCheckDetails.Where(d => d.MaintenanceCheckAnswer.MaintenanceCheckFK.Equals(model.MaintenanceCheckID) && d.MaintenanceCheckAnswer.ChecklistQuestionFK.Equals(model.ChecklistQuestionID)).SingleOrDefault();

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (detail != null)
                    {
                        File newFile = new File()
                        {
                            FilePK = Guid.NewGuid(),
                            FileDownloadName = "Maintenance Photo " + detail.MaintenanceCheckAnswer.MaintenanceCheck.Time.ToString(),
                            ContentType = ".jpg",
                            Data = System.Convert.FromBase64String(model.DataString),
                            CreatedDate = detail.MaintenanceCheckAnswer.MaintenanceCheck.Time,
                            UpdatedDate = DateTime.Now,
                            CreatedByFK = GetUserContext().UserDetails.ID,
                            UpdatedByFK = GetUserContext().UserDetails.ID,
                            IsActive = true,
                            Longitude = model.Longitude,
                            Latitude = model.Latitude
                        };

                        Context.Files.Add(newFile);

                        Context.SaveChanges();

                        MaintenanceCheckDetailPhoto photo = new MaintenanceCheckDetailPhoto()
                        {
                            MaintenanceCheckDetailPhotoPK = Guid.NewGuid(),
                            MaintenanceCheckDetailFK = detail.MaintenanceCheckDetailPK,
                            FileFK = newFile.FilePK,
                            IsActive = true,
                            CreatedDate = detail.MaintenanceCheckAnswer.MaintenanceCheck.Time,
                            UpdatedDate = DateTime.Now,
                            CreatedByID = GetUserContext().UserDetails.ID,
                            UpdatedByID = GetUserContext().UserDetails.ID
                        };

                        Context.MaintenanceCheckDetailPhotos.Add(photo);

                        Context.SaveChanges();

                        transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public bool PushBriefingSignature(BriefingSignatureModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var existingSignature = Context.BriefingSignatures.Where(b => b.BriefingFK.Equals(model.BriefingID) && b.EmployeeFK.Equals(model.StaffID)).SingleOrDefault();

                    if (model.DataString != null && existingSignature == null)
                    {
                        File newFile = new File()
                        {
                            FilePK = Guid.NewGuid(),
                            FileDownloadName = "Briefing Signature ",
                            ContentType = ".jpg",
                            Data = System.Convert.FromBase64String(model.DataString),
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            CreatedByFK = GetUserContext().UserDetails.ID,
                            UpdatedByFK = GetUserContext().UserDetails.ID,
                            IsActive = true,
                            Longitude = model.Longitude,
                            Latitude = model.Latitude
                        };

                        var briefing = Context.Briefings.Where(b => b.BriefingPK.Equals(model.BriefingID)).SingleOrDefault();

                        if (briefing != null)
                        {
                            newFile.FileDownloadName = "Briefing Signature " + briefing.Name;
                        }

                        Context.Files.Add(newFile);

                        Context.SaveChanges();

                        BriefingSignature sig = new BriefingSignature()
                        {
                            BriefingSignaturePK = Guid.NewGuid(),
                            BriefingFK = model.BriefingID,
                            FileFK = newFile.FilePK,
                            EmployeeFK = model.StaffID,
                            BriefingStart = model.BriefingStart,
                            BriefingEnd = model.BriefingEnd,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            CreatedByID = GetUserContext().UserDetails.ID,
                            UpdatedByID = GetUserContext().UserDetails.ID
                        };

                        Context.BriefingSignatures.Add(sig);

                        Context.SaveChanges();

                        transaction.Commit();

                        return true;
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }

            return false;
        }

        public bool PushActivitySignature(ActivitySignatureModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var existingSignatures = Context.WorkInstructionSignatures.Where(w => w.WorkInstructionFK.Equals(model.ActivityID) && w.EmployeeFK.Equals(model.StaffID)).ToList();

                    if (model.DataString != null && existingSignatures.Count.Equals(0))
                    {
                        File newFile = new File()
                        {
                            FilePK = Guid.NewGuid(),
                            FileDownloadName = "Work Instruction Signature",
                            ContentType = ".jpg",
                            Data = System.Convert.FromBase64String(model.DataString),
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            CreatedByFK = GetUserContext().UserDetails.ID,
                            UpdatedByFK = GetUserContext().UserDetails.ID,
                            IsActive = true,
                            Longitude = model.Longitude,
                            Latitude = model.Latitude
                        };

                        var wi = Context.WorkInstructions.Where(w => w.WorkInstructionPK.Equals(model.ActivityID)).SingleOrDefault();

                        if (wi != null)
                        {
                            newFile.FileDownloadName = "Work Instruction Signature " + wi.WorkInstructionNumber + " " + wi.AddressLine1;
                        }

                        Context.Files.Add(newFile);

                        Context.SaveChanges();

                        WorkInstructionSignature sig = new WorkInstructionSignature()
                        {
                            WorkInstructionSignaturePK = Guid.NewGuid(),
                            WorkInstructionFK = model.ActivityID,
                            FileFK = newFile.FilePK,
                            EmployeeFK = model.StaffID,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            CreatedByID = GetUserContext().UserDetails.ID,
                            UpdatedByID = GetUserContext().UserDetails.ID
                        };

                        Context.WorkInstructionSignatures.Add(sig);

                        Context.SaveChanges();

                        transaction.Commit();

                        return true;
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }

            return false;
        }

        // abandoned, if user uploads photo, it is final
        public void DeleteAllVehicleDefectPhotos(Guid shiftID, bool startOfShift)
        {
            var vehicleChecklistDefectPhotos = Context.VehicleChecklistDefectPhotos.Where(a =>
                a.VehicleChecklistDefect.VehicleChecklistAnswer.VehicleChecklist.ShiftFK == shiftID &&
                a.VehicleChecklistDefect.VehicleChecklistAnswer.VehicleChecklist.StartOfShift == startOfShift)
                .ToList();
        }

        // returning true will delete the file from mobile, in this case we want to keep and let the shift delete function on mobile handle image deletion
        public bool PushVehicleDefectPhoto(VehicleChecklistAnswerDefectPhotoModel model)
        {
            var detail = Context.VehicleChecklistDefects.Where(d => d.VehicleChecklistAnswer.VehicleChecklistFK.Equals(model.VehicleChecklistID) && d.VehicleChecklistAnswer.ChecklistQuestionFK.Equals(model.ChecklistQuestionID)).SingleOrDefault();

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (detail != null)
                    {
                        // if shift complete or cancelled continue
                        if (detail.VehicleChecklistAnswer.VehicleChecklist.Shift.ShiftComplete || detail.VehicleChecklistAnswer.VehicleChecklist.Shift.IsCancelled)
                        {
                            return false;
                        }

                        // if VehicleChecklistDefectPhotoPK exists skip
                        var exists = detail.VehicleChecklistDefectPhotos.Any(a => a.VehicleChecklistDefectPhotoPK == model.VehicleChecklistDefectPhotoID);
                        if (exists)
                        {
                            return false;
                        }

                        File newFile = new File()
                        {
                            FilePK = Guid.NewGuid(),
                            FileDownloadName = "Vehicle Defect " + detail.VehicleChecklistAnswer.VehicleChecklist.Vehicle.Registration + " " + model.Time.ToString(),
                            ContentType = ".jpg",
                            Data = System.Convert.FromBase64String(model.DataString),
                            CreatedDate = model.Time,
                            UpdatedDate = DateTime.Now,
                            CreatedByFK = GetUserContext().UserDetails.ID,
                            UpdatedByFK = GetUserContext().UserDetails.ID,
                            IsActive = true,
                            Longitude = model.Longitude,
                            Latitude = model.Latitude
                        };

                        Context.Files.Add(newFile);

                        Context.SaveChanges();

                        VehicleChecklistDefectPhoto photo = new VehicleChecklistDefectPhoto()
                        {
                            VehicleChecklistDefectPhotoPK = model.VehicleChecklistDefectPhotoID,
                            VehicleChecklistDefectFK = detail.VehicleChecklistDefectPK,
                            FileFK = newFile.FilePK,
                            IsActive = true,
                            CreatedDate = model.Time,
                            UpdatedDate = DateTime.Now,
                            CreatedByID = GetUserContext().UserDetails.ID,
                            UpdatedByID = GetUserContext().UserDetails.ID
                        };

                        Context.VehicleChecklistDefectPhotos.Add(photo);

                        Context.SaveChanges();

                        transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }

            return false;
        }

        public bool PushTaskPhoto(TaskPhotoModel model)
        {
            var userDetails = GetUserContext().UserDetails;
            var task = Context.Tasks.Where(d => d.TaskPK == model.TaskID).SingleOrDefault();

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (task != null)
                    {
                        File newFile = new File()
                        {
                            FilePK = Guid.NewGuid(),
                            FileDownloadName = "Task Photo Task " + task.WorkInstruction.WorkInstructionNumber + " " + task.WorkInstruction.AddressLine1 + " " + task.Name + " " + model.Time.ToString(),
                            ContentType = ".jpg",
                            Data = System.Convert.FromBase64String(model.DataString),
                            CreatedDate = model.Time,
                            UpdatedDate = DateTime.Now,
                            CreatedByFK = userDetails.ID,
                            UpdatedByFK = userDetails.ID,
                            IsActive = true,
                            Longitude = model.Longitude,
                            Latitude = model.Latitude,
                            Comments = model.Comments,
                        };

                        Context.Files.Add(newFile);

                        Context.SaveChanges();

                        TaskPhoto photo = new TaskPhoto()
                        {
                            TaskPhotoPK = Guid.NewGuid(),
                            TaskFK = task.TaskPK,
                            FileFK = newFile.FilePK,
                            IsActive = true,
                            CreatedDate = model.Time,
                            UpdatedDate = DateTime.Now,
                            CreatedByID = userDetails.ID,
                            UpdatedByID = userDetails.ID
                        };

                        Context.TaskPhotos.Add(photo);

                        Context.SaveChanges();

                        transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }

            return true;
        }

        public bool PushTaskChecklistAnswerPhoto(TaskChecklistAnswerPhotoModel model)
        {
            var taskChecklistAnswer = Context.TaskChecklistAnswers.Where(a => a.TaskChecklistAnswerPK == model.TaskChecklistAnswerID).SingleOrDefault();

            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (taskChecklistAnswer != null)
                    {
                        var tasktype = taskChecklistAnswer.TaskChecklist.Task.Checklist.IsPostHireBatteryChecklist ? "Battery Check" : "Risk Assessment";
                        var file = new File
                        {
                            FilePK = Guid.NewGuid(),
                            FileDownloadName = tasktype + " Photo - Task " + taskChecklistAnswer.TaskChecklist.Task.WorkInstruction.WorkInstructionNumber + " - Question " + taskChecklistAnswer.ChecklistQuestion.QuestionOrder + " - " + model.Time.ToString(),
                            ContentType = ".jpg",
                            Data = System.Convert.FromBase64String(model.DataString),
                            CreatedDate = model.Time,
                            UpdatedDate = DateTime.Now,
                            CreatedByFK = GetUserContext().UserDetails.ID,
                            UpdatedByFK = GetUserContext().UserDetails.ID,
                            IsActive = true,
                            Longitude = model.Longitude,
                            Latitude = model.Latitude
                        };

                        Context.Files.Add(file);

                        Context.SaveChanges();

                        var photo = new TaskChecklistAnswerPhoto
                        {
                            TaskChecklistAnswerPhotoPK = model.TaskChecklistAnswerPhotoID,
                            TaskChecklistAnswerFK = taskChecklistAnswer.TaskChecklistAnswerPK,
                            FileFK = file.FilePK,
                            IsActive = true,
                            CreatedDate = model.Time,
                            UpdatedDate = DateTime.Now,
                            CreatedByID = GetUserContext().UserDetails.ID,
                            UpdatedByID = GetUserContext().UserDetails.ID
                        };

                        Context.TaskChecklistAnswerPhotos.Add(photo);

                        Context.SaveChanges();

                        transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }

            return true;
        }

        public bool PushTaskSignature(TaskSignatureModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    if (model.DataString != null)
                    {

                        var existingSignature = Context.TaskSignatures.Where(t => t.TaskSignaturePK.Equals(model.TaskSignatureID)).SingleOrDefault();

                        if (existingSignature == null)
                        {
                            File newFile = new File()
                            {
                                FilePK = Guid.NewGuid(),
                                FileDownloadName = "Task Signature",
                                ContentType = ".jpg",
                                Data = System.Convert.FromBase64String(model.DataString),
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                CreatedByFK = GetUserContext().UserDetails.ID,
                                UpdatedByFK = GetUserContext().UserDetails.ID,
                                IsActive = true,
                                Longitude = model.Longitude,
                                Latitude = model.Latitude
                            };

                            var task = Context.Tasks.Where(t => t.TaskPK.Equals(model.TaskID)).SingleOrDefault();

                            if (task != null)
                            {
                                newFile.FileDownloadName = "Task Signature Work Instruction " + task.WorkInstruction.WorkInstructionNumber + " " + task.WorkInstruction.AddressLine1;
                            }

                            Context.Files.Add(newFile);

                            Context.SaveChanges();

                            TaskSignature sig = new TaskSignature
                            {
                                TaskSignaturePK = model.TaskSignatureID,
                                TaskFK = model.TaskID,
                                FileFK = newFile.FilePK,
                                PrintedName = model.PrintedName,
                                IsClient = model.IsClient,
                                ClientVehicleReg = model.ClientVehicleReg,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                CreatedByID = GetUserContext().UserDetails.ID,
                                UpdatedByID = GetUserContext().UserDetails.ID
                            };

                            if (model.StaffID != Guid.Empty)
                            {
                                sig.EmployeeFK = model.StaffID;
                            }

                            Context.TaskSignatures.Add(sig);

                            Context.SaveChanges();
                        }

                        transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }

            return false;
        }

        public bool PushLoadingSheetSignature(LoadingSheetSignatureModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                var ls = Context.LoadingSheets.Where(t => t.LoadingSheetPK.Equals(model.LoadingSheetID)).SingleOrDefault();

                if (ls != null)
                {
                    try
                    {
                        if (model.DataString != null)
                        {

                            var existingSignature = Context.LoadingSheetSignatures.Where(t => t.LoadingSheetFK.Equals(model.LoadingSheetID)).SingleOrDefault();

                            if (existingSignature == null)
                            {
                                File newFile = new File()
                                {
                                    FilePK = Guid.NewGuid(),
                                    FileDownloadName = "Loading Sheet Signature",
                                    ContentType = ".jpg",
                                    Data = System.Convert.FromBase64String(model.DataString),
                                    CreatedDate = DateTime.Now,
                                    UpdatedDate = DateTime.Now,
                                    CreatedByFK = GetUserContext().UserDetails.ID,
                                    UpdatedByFK = GetUserContext().UserDetails.ID,
                                    IsActive = true,
                                    Longitude = model.Longitude,
                                    Latitude = model.Latitude
                                };


                                newFile.FileDownloadName = "Loading Sheet " + ls.LoadingSheetNumber + " Signature";

                                Context.Files.Add(newFile);

                                Context.SaveChanges();

                                LoadingSheetSignature sig = new LoadingSheetSignature()
                                {
                                    LoadingSheetSignaturePK = Guid.NewGuid(),
                                    LoadingSheetFK = model.LoadingSheetID,
                                    FileFK = newFile.FilePK,
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    UpdatedDate = DateTime.Now,
                                    CreatedByID = GetUserContext().UserDetails.ID,
                                    UpdatedByID = GetUserContext().UserDetails.ID
                                };

                                Context.LoadingSheetSignatures.Add(sig);

                                Context.SaveChanges();
                            }

                            transaction.Commit();

                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                        if (transaction != null)
                        {
                            transaction.Rollback();
                        }

                        e.Data.AddSafely(Global.ExceptionDataKey, model);
                        ExceptionDispatchInfo.Capture(e).Throw();
                    }
                }
            }

            return false;
        }

        public void PushEquipmentItems(PushEquipmentItemModel container)
        {
            var createdByFK = GetUserContext().UserDetails.ID;

            container.LoadingSheetEquipmentItems = container.LoadingSheetEquipmentItems ?? new List<LoadingSheetEquipmentItemModel>();
            container.EquipmentItemChecklists = container.EquipmentItemChecklists ?? new List<EquipmentItemChecklistModel>();
            container.EquipmentItemChecklistAnswers = container.EquipmentItemChecklistAnswers ?? new List<EquipmentItemChecklistAnswerModel>();

            // delete
            var loadingSheetEquipmentIDs = container.LoadingSheetEquipmentItems.Select(a => a.LoadingSheetEquipmentID).ToList();

            if (loadingSheetEquipmentIDs.Any())
            {
                var existingAnswers = Context.EquipmentItemChecklistAnswers.Where(a => loadingSheetEquipmentIDs.Contains(a.EquipmentItemChecklist.LoadingSheetEquipmentItem.LoadingSheetEquipmentFK)).ToList();
                var existingChecklists = Context.EquipmentItemChecklists.Where(a => loadingSheetEquipmentIDs.Contains(a.LoadingSheetEquipmentItem.LoadingSheetEquipmentFK)).ToList();
                var existingEquipmentItems = Context.LoadingSheetEquipmentItems.Where(a => loadingSheetEquipmentIDs.Contains(a.LoadingSheetEquipmentFK)).ToList();

                if (existingAnswers.Any())
                {
                    Context.EquipmentItemChecklistAnswers.RemoveRange(existingAnswers);
                    Context.SaveChanges();
                }

                if (existingChecklists.Any())
                {
                    Context.EquipmentItemChecklists.RemoveRange(existingChecklists);
                    Context.SaveChanges();
                }

                if (existingEquipmentItems.Any())
                {
                    Context.LoadingSheetEquipmentItems.RemoveRange(existingEquipmentItems);
                    Context.SaveChanges();
                }
            }

            //check for new loading sheet created on device
            foreach (var model in container.LoadingSheets)
            {
                if (!Context.LoadingSheets.Where(x => x.LoadingSheetPK == model.LoadingSheetID).Any())
                {
                    int loadingSheetNumber = Context.LoadingSheets.Select(x => x.LoadingSheetNumber).Max();
                    var workInstruction = Context.WorkInstructions.Where(w => w.WorkInstructionPK == model.ActivityID).Single();

                    //new loadingsheet
                    using (var tran = Context.Database.BeginTransaction())
                    {
                        try
                        {
                            LoadingSheet ls = new LoadingSheet()
                            {
                                LoadingSheetPK = model.LoadingSheetID,
                                LoadingSheetNumber = loadingSheetNumber + 1,
                                WorkInstructionFK = model.ActivityID,
                                LoadingSheetDate = DateTime.Now,
                                Name = "Created on Task " + workInstruction.WorkInstructionNumber.ToString(),
                                Comments = "",
                                EmployeeFK = workInstruction.EmployeeFK,
                                StatusFK = new Guid("67C2E52E-8F31-4269-85C3-154EDE5BC29A"),
                                IsActive = true,
                                CreatedByFK = createdByFK,
                                CreatedDate = DateTime.Now,
                                UpdatedByFK = createdByFK,
                                UpdatedDate = DateTime.Now
                            };
                            Context.LoadingSheets.Add(ls);
                            Context.SaveChanges();
                            tran.Commit();
                        }
                        catch (Exception e)
                        {
                            if (tran != null)
                            {
                                tran.Rollback();
                            }

                            e.Data.AddSafely(Global.ExceptionDataKey, container);
                            ExceptionDispatchInfo.Capture(e).Throw();
                        }
                    }
                }
            }

            //check any items added to loading sheet on device
            foreach (var model in container.LoadingSheetEquipments)
            {
                if (!Context.LoadingSheetEquipments.Where(x => x.LoadingSheetEquipmentPK == model.LoadingSheetEquipmentID).Any())
                {
                    //new item
                    using (var tran = Context.Database.BeginTransaction())
                    {
                        try
                        {
                            LoadingSheetEquipment lse = new LoadingSheetEquipment()
                            {
                                LoadingSheetEquipmentPK = model.LoadingSheetEquipmentID,
                                LoadingSheetFK = model.LoadingSheetID,
                                EquipmentFK = model.EquipmentID,
                                NonStandardEquipment = "",
                                IsNonStandard = false,
                                Quantity = model.Quantity,
                                SheetOrder = model.SheetOrder,
                                IsActive = true,
                                CreatedByFK = createdByFK,
                                CreatedDate = DateTime.Now,
                                UpdatedByFK = createdByFK,
                                UpdatedDate = DateTime.Now
                            };
                            Context.LoadingSheetEquipments.Add(lse);
                            Context.SaveChanges();
                            tran.Commit();
                        }
                        catch (Exception e)
                        {
                            if (tran != null)
                            {
                                tran.Rollback();
                            }

                            e.Data.AddSafely(Global.ExceptionDataKey, container);
                            ExceptionDispatchInfo.Capture(e).Throw();
                        }
                    }
                }
            }

            // insert
            foreach (var model in container.LoadingSheetEquipmentItems)
            {
                var loadingSheetEquipmentItem = new LoadingSheetEquipmentItem();
                loadingSheetEquipmentItem.LoadingSheetEquipmentItemPK = model.LoadingSheetEquipmentItemID;
                loadingSheetEquipmentItem.LoadingSheetEquipmentFK = model.LoadingSheetEquipmentID;
                loadingSheetEquipmentItem.ItemNumber = model.ItemNumber;
                loadingSheetEquipmentItem.ChecklistFK = model.ChecklistID;
                loadingSheetEquipmentItem.IsActive = true;
                loadingSheetEquipmentItem.CreatedByFK = createdByFK;
                loadingSheetEquipmentItem.CreatedDate = DateTime.Now;
                loadingSheetEquipmentItem.UpdatedByFK = createdByFK;
                loadingSheetEquipmentItem.UpdatedDate = DateTime.Now;
                Context.LoadingSheetEquipmentItems.Add(loadingSheetEquipmentItem);
            }

            foreach (var model in container.EquipmentItemChecklists)
            {
                var equipmentItemChecklist = new EquipmentItemChecklist();
                equipmentItemChecklist.EquipmentItemChecklistPK = model.EquipmentItemChecklistID;
                equipmentItemChecklist.LoadingSheetEquipmentItemFK = model.LoadingSheetEquipmentItemID;
                equipmentItemChecklist.ChecklistFK = model.ChecklistID;
                equipmentItemChecklist.IsActive = true;
                equipmentItemChecklist.CreatedByFK = createdByFK;
                equipmentItemChecklist.CreatedDate = DateTime.Now;
                equipmentItemChecklist.UpdatedByFK = createdByFK;
                equipmentItemChecklist.UpdatedDate = DateTime.Now;

                Context.EquipmentItemChecklists.Add(equipmentItemChecklist);
            }

            foreach (var model in container.EquipmentItemChecklistAnswers)
            {
                var equipmentItemChecklistAnswer = new EquipmentItemChecklistAnswer();
                equipmentItemChecklistAnswer.EquipmentItemChecklistAnswerPK = model.EquipmentItemChecklistAnswerID;
                equipmentItemChecklistAnswer.EquipmentItemChecklistFK = model.EquipmentItemChecklistID;
                equipmentItemChecklistAnswer.ChecklistQuestionFK = model.ChecklistQuestionID;
                equipmentItemChecklistAnswer.Answer = model.Answer;
                equipmentItemChecklistAnswer.Reason = model.Reason;
                equipmentItemChecklistAnswer.IsActive = true;
                equipmentItemChecklistAnswer.CreatedByFK = createdByFK;
                equipmentItemChecklistAnswer.CreatedDate = DateTime.Now;
                equipmentItemChecklistAnswer.UpdatedByFK = createdByFK;
                equipmentItemChecklistAnswer.UpdatedDate = DateTime.Now;

                Context.EquipmentItemChecklistAnswers.Add(equipmentItemChecklistAnswer);
            }

            Context.SaveChanges();
        }

        // an out of hours work instruction is a new work instruction and should be saved successfully before any other sync methods are ran
        public void PushOutOfHours(OutOfHoursModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    var workInstruction = Context.WorkInstructions.Where(a => a.WorkInstructionPK == model.ActivityID).SingleOrDefault();

                    // if work instruction has already been saved return
                    if (workInstruction != null)
                    {
                        return;
                    }

                    var userDetails = GetUserContext().UserDetails;
                    var employeeID = Context.UserProfiles.Where(a => a.UserFK == userDetails.ID).Select(a => a.EmployeeFK).Single();

                    // work instruction
                    workInstruction = new WorkInstruction();
                    workInstruction.WorkInstructionPK = model.ActivityID;
                    workInstruction.WorkInstructionNumber = Context.WorkInstructions.OrderByDescending(x => x.WorkInstructionNumber).Select(a => a.WorkInstructionNumber).FirstOrDefault() + 1;
                    workInstruction.JobTypeFK = model.JobTypeID;
                    workInstruction.JobFK = null;
                    workInstruction.JobPackFK = model.JobPackID;
                    workInstruction.DepotFK = model.DepotID;
                    workInstruction.WorkInstructionStartDate = model.StartDate;
                    workInstruction.WorkInstructionFinishDate = model.FinishDate;
                    workInstruction.SRWNumber = "";
                    workInstruction.Comments = "";
                    workInstruction.EmployeeFK = (Guid)employeeID;
                    workInstruction.TrafficManagementActivities = "";
                    workInstruction.ActivityDescription = "";
                    workInstruction.SiteInductionRequired = false;
                    workInstruction.SiteInductionNotes = "";
                    workInstruction.SiteSpecificConditions = "";
                    workInstruction.SharedRoadSpace = "";
                    workInstruction.StatusFK = WorkInstructionStatusesEnum.Scheduled;
                    workInstruction.IsScheduled = true;
                    workInstruction.IsOutOfHours = true;
                    workInstruction.IsActive = true;
                    workInstruction.CreatedByFK = userDetails.ID;
                    workInstruction.CreatedDate = DateTime.Now;
                    workInstruction.UpdatedByFK = userDetails.ID;
                    workInstruction.UpdatedDate = DateTime.Now;
                    workInstruction.IsChargeable = false;
                    workInstruction.AddressLine1 = model.AddressLine1;
                    workInstruction.AddressLine2 = model.AddressLine2;
                    workInstruction.AddressLine3 = model.Borough;
                    workInstruction.City = model.City;
                    workInstruction.County = model.County;
                    workInstruction.Postcode = model.Postcode;
                    workInstruction.IsNightShift = model.StartDate.Hour >= 17;
                    workInstruction.OrderNumber = 1;

                    Context.WorkInstructions.Add(workInstruction);

                    // out of hours
                    var outOfHour = new OutOfHour();
                    outOfHour.WorkInstructionFK = workInstruction.WorkInstructionPK;
                    outOfHour.CustomerFK = model.CustomerID;
                    outOfHour.IsActive = true;
                    outOfHour.CreatedByID = userDetails.ID;
                    outOfHour.CreatedDate = DateTime.Now;
                    outOfHour.UpdatedByID = userDetails.ID;
                    outOfHour.UpdatedDate = DateTime.Now;

                    Context.OutOfHours.Add(outOfHour);

                    // schedule
                    var schedule = new Schedule();
                    schedule.SchedulePK = Guid.NewGuid();
                    schedule.WorkInstructionFK = model.ActivityID;
                    schedule.StartDate = model.StartDate;
                    schedule.EndDate = model.FinishDate;
                    schedule.IsComplete = false;
                    schedule.IsActive = true;
                    schedule.ShiftFK = model.ShiftID;
                    schedule.CreatedByFK = userDetails.ID;
                    schedule.CreatedDate = DateTime.Now;
                    schedule.UpdatedByFK = userDetails.ID;
                    schedule.UpdatedDate = DateTime.Now;

                    Context.Schedules.Add(schedule);

                    // job pack
                    var jobPack = new JobPack();
                    jobPack.JobPackPK = model.JobPackID;
                    jobPack.ShiftFK = model.ShiftID;
                    jobPack.JobPackName = "";
                    jobPack.IsActive = true;
                    jobPack.CreatedByID = userDetails.ID;
                    jobPack.CreatedDate = DateTime.Now;
                    jobPack.UpdatedByID = userDetails.ID;
                    jobPack.UpdatedDate = DateTime.Now;

                    Context.JobPacks.Add(jobPack);

                    // shift
                    var shift = new Shift();
                    shift.ShiftPK = model.ShiftID;
                    shift.ShiftDate = model.StartDate;
                    shift.ShiftComplete = false;
                    shift.ShiftCompletionStatus = 0;
                    shift.IsActive = true;
                    shift.CreatedByID = userDetails.ID;
                    shift.CreatedDate = DateTime.Now;
                    shift.UpdatedByID = userDetails.ID;
                    shift.UpdatedDate = DateTime.Now;
                    shift.IsCancelled = false;
                    shift.IsConfirmed = true;

                    Context.Shifts.Add(shift);

                    // briefings
                    foreach (var briefingModel in model.Briefings)
                    {
                        var briefing = new Briefing();
                        briefing.BriefingPK = briefingModel.BriefingID;
                        briefing.BriefingTypeFK = briefingModel.BriefingTypeID;
                        briefing.JobPackFK = model.JobPackID;
                        briefing.Name = briefingModel.Name;
                        briefing.Details = "";
                        briefing.FileFK = briefingModel.FileID;
                        briefing.IsActive = true;
                        briefing.CreatedByID = userDetails.ID;
                        briefing.CreatedDate = DateTime.Now;
                        briefing.UpdatedByID = userDetails.ID;
                        briefing.UpdatedDate = DateTime.Now;

                        Context.Briefings.Add(briefing);
                    }

                    // work instruction method statements
                    foreach (var methodStatementModel in model.MethodStatements)
                    {
                        var workInstructionMethodStatement = new WorkInstructionMethodStatement();
                        workInstructionMethodStatement.WorkInstructionMethodStatementPK = methodStatementModel.ActivityMethodStatementID;
                        workInstructionMethodStatement.WorkInstructionFK = model.ActivityID;
                        workInstructionMethodStatement.MethodStatementFK = methodStatementModel.MethodStatementID;
                        workInstructionMethodStatement.IsActive = true;
                        workInstructionMethodStatement.CreatedByFK = userDetails.ID;
                        workInstructionMethodStatement.CreatedDate = DateTime.Now;
                        workInstructionMethodStatement.UpdatedByFK = userDetails.ID;
                        workInstructionMethodStatement.UpdatedDate = DateTime.Now;

                        Context.WorkInstructionMethodStatements.Add(workInstructionMethodStatement);
                    }

                    // schedule and shift staff
                    foreach (var shiftStaffModel in model.ShiftStaffs)
                    {
                        var scheduleEmployee = new ScheduleEmployee();
                        scheduleEmployee.ScheduleEmployeePK = Guid.NewGuid();
                        scheduleEmployee.ScheduleFK = schedule.SchedulePK;
                        scheduleEmployee.EmployeeFK = shiftStaffModel.EmployeeID;
                        scheduleEmployee.IsActive = true;
                        scheduleEmployee.CreatedByFK = userDetails.ID;
                        scheduleEmployee.CreatedDate = DateTime.Now;
                        scheduleEmployee.UpdatedByFK = userDetails.ID;
                        scheduleEmployee.UpdatedDate = DateTime.Now;

                        Context.ScheduleEmployees.Add(scheduleEmployee);

                        var shiftStaff = new ShiftStaff();
                        shiftStaff.ShiftStaffPK = Guid.NewGuid();
                        shiftStaff.ShiftFK = model.ShiftID;
                        shiftStaff.EmployeeFK = shiftStaffModel.EmployeeID;
                        shiftStaff.NotOnShift = false;
                        shiftStaff.ReasonNotOn = "";
                        shiftStaff.IsActive = true;
                        shiftStaff.CreatedByID = userDetails.ID;
                        shiftStaff.CreatedDate = DateTime.Now;
                        shiftStaff.UpdatedByID = userDetails.ID;
                        shiftStaff.UpdatedDate = DateTime.Now;

                        Context.ShiftStaffs.Add(shiftStaff);

                    }

                    // schedule and shift vehicles
                    foreach (var shiftVehicleModel in model.ShiftVehicles)
                    {
                        var scheduleVehicle = new ScheduleVehicle();
                        scheduleVehicle.ScheduleVehiclePK = Guid.NewGuid();
                        scheduleVehicle.ScheduleFK = schedule.SchedulePK;
                        scheduleVehicle.VehicleFK = shiftVehicleModel.VehicleID;
                        scheduleVehicle.IsActive = true;
                        scheduleVehicle.CreatedByID = userDetails.ID;
                        scheduleVehicle.CreatedDate = DateTime.Now;
                        scheduleVehicle.UpdatedByID = userDetails.ID;
                        scheduleVehicle.UpdatedDate = DateTime.Now;

                        Context.ScheduleVehicles.Add(scheduleVehicle);

                        var shiftVehicle = new ShiftVehicle();
                        shiftVehicle.ShiftVehiclePK = Guid.NewGuid();
                        shiftVehicle.ShiftFK = model.ShiftID;
                        shiftVehicle.VehicleFK = shiftVehicleModel.VehicleID;
                        shiftVehicle.EmployeeFK = null;
                        shiftVehicle.StartMileage = 0;
                        shiftVehicle.EndMileage = 0;
                        shiftVehicle.LoadingSheetCompleted = false;
                        shiftVehicle.NotOnShift = false;
                        shiftVehicle.ReasonNotOnShift = "";
                        shiftVehicle.IsActive = true;
                        shiftVehicle.CreatedByID = userDetails.ID;
                        shiftVehicle.CreatedDate = DateTime.Now;
                        shiftVehicle.UpdatedByID = userDetails.ID;
                        shiftVehicle.UpdatedDate = DateTime.Now;

                        Context.ShiftVehicles.Add(shiftVehicle);
                    }

                    // shift progress info
                    var shiftProgressInfo = new ShiftProgressInfo();
                    shiftProgressInfo.ShiftFK = model.ShiftID;
                    shiftProgressInfo.ShiftProgressID = 1; // NotStarted
                    shiftProgressInfo.Time = DateTime.Now;
                    shiftProgressInfo.CreatedDate = DateTime.Now;

                    Context.ShiftProgressInfoes.Add(shiftProgressInfo);

                    // tasks
                    foreach (var taskModel in model.Tasks)
                    {
                        var task = new Task();
                        task.TaskPK = taskModel.TaskID;
                        task.Name = taskModel.Name;
                        task.WorksInstructionFK = model.ActivityID;
                        task.TaskTypeFK = taskModel.TaskTypeID;
                        task.TaskOrder = taskModel.TaskOrder;
                        task.ChecklistFK = taskModel.ChecklistID;
                        task.PhotosRequired = taskModel.PhotosRequired;
                        task.ShowDetails = taskModel.ShowDetails;
                        task.ShowClientSiganture = taskModel.ShowClientSignature;
                        task.ClientSignatureRequired = taskModel.ClientSignatureRequired;
                        task.ShowStaffSignature = taskModel.ShowStaffSignature;
                        task.StaffSignatureRequired = taskModel.StaffSignatureRequired;
                        task.Tag = taskModel.Tag;
                        task.IsMandatory = taskModel.IsMandatory;
                        task.IsCancelled = false;
                        task.IsActive = true;
                        task.CreatedByID = userDetails.ID;
                        task.CreatedDate = DateTime.Now;
                        task.UpdatedByID = userDetails.ID;
                        task.UpdatedDate = DateTime.Now;

                        Context.Tasks.Add(task);
                    }

                    Context.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public bool PushOutOfHoursPhoto(OutOfHoursPhotoModel model)
        {
            using (var transaction = Context.Database.BeginTransaction())
            {
                try
                {
                    File newFile = new File()
                    {
                        FilePK = Guid.NewGuid(),
                        FileDownloadName = "OOH Photo " + model.Time.ToString(),
                        ContentType = ".jpg",
                        Data = System.Convert.FromBase64String(model.DataString),
                        CreatedDate = model.Time,
                        UpdatedDate = DateTime.Now,
                        CreatedByFK = GetUserContext().UserDetails.ID,
                        UpdatedByFK = GetUserContext().UserDetails.ID,
                        IsActive = true,
                        Longitude = model.Longitude,
                        Latitude = model.Latitude
                    };

                    Context.Files.Add(newFile);

                    Context.SaveChanges();

                    transaction.Commit();

                    return true;
                }
                catch (Exception e)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, model);
                    ExceptionDispatchInfo.Capture(e).Throw();
                    throw;
                }
            }
        }

        public void PushDatabase(string DataString)
        {
            string username = GetUserContext().UserDetails.UserName;
            string filename = username + "_" + DateTime.Now.Ticks + ".db";
            string path = Path.Combine(ConfigurationManager.AppSettings.Get("filesLocation") + @"databases\FM Conway WDM", filename);

            Directory.CreateDirectory(ConfigurationManager.AppSettings.Get("filesLocation") + @"databases\FM Conway WDM");
            var bytes = Convert.FromBase64String(DataString);

            using (var imageFile = new FileStream(path, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
        }

        public void PushCleansingLogs(List<CleansingLogModel> models)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                try
                {
                    var userDetails = GetUserContext().UserDetails;

                    foreach (var model in models)
                    {
                        var cleansingLog = Context.CleansingLogs.Where(a => a.CleansingLogPK == model.CleansingLogID).SingleOrDefault();

                        if (cleansingLog == null)
                        {
                            cleansingLog = new CleansingLog();
                            cleansingLog.CleansingLogPK = model.CleansingLogID;
                            cleansingLog.TaskFK = model.TaskID;
                            cleansingLog.WorkInstructionFK = model.ActivityID;
                            cleansingLog.IsActive = true;
                            cleansingLog.CreatedByFK = userDetails.ID;
                            cleansingLog.CreatedDate = model.CreatedDate;
                            cleansingLog.UpdatedByFK = userDetails.ID;
                            cleansingLog.UpdatedDate = DateTime.Now;

                            Context.CleansingLogs.Add(cleansingLog);
                        }

                        cleansingLog.Road = model.Road;
                        cleansingLog.Section = model.Section;
                        cleansingLog.LaneFK = model.LaneID;
                        cleansingLog.DirectionFK = model.DirectionID;
                        cleansingLog.StartMarker = model.StartMarker;
                        cleansingLog.EndMarker = model.EndMarker;
                        cleansingLog.GulliesCleaned = model.GulliesCleaned;
                        cleansingLog.GulliesMissed = model.GulliesMissed;
                        cleansingLog.CatchpitsCleaned = model.CatchpitsCleaned;
                        cleansingLog.CatchpitsMissed = model.CatchpitsMissed;
                        cleansingLog.Channels = model.Channels;
                        cleansingLog.SlotDrains = model.SlotDrains;
                        cleansingLog.Defects = model.Defects;
                        cleansingLog.Comments = model.Comments;

                        Context.SaveChanges();
                    }

                    tran.Commit();
                }
                catch (Exception e)
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, models);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public bool PushCleansingLogPhoto(CleansingLogPhotoModel model)
        {
            try
            {
                var userDetails = GetUserContext().UserDetails;
                var cleansingLog = Context.CleansingLogs.Where(a => a.CleansingLogPK == model.CleansingLogID).Single();

                var file = new File();
                file.FilePK = Guid.NewGuid();
                file.FileDownloadName = "Cleansing Log Photo " + cleansingLog.WorkInstruction.WorkInstructionNumber + " " + model.Time.ToString();
                file.ContentType = ".jpg";
                file.Data = Convert.FromBase64String(model.DataString);
                file.Longitude = model.Longitude;
                file.Latitude = model.Latitude;
                file.Comments = model.Comments;
                file.IsActive = true;
                file.CreatedByFK = userDetails.ID;
                file.CreatedDate = model.Time;
                file.UpdatedByFK = userDetails.ID;
                file.UpdatedDate = DateTime.Now;

                Context.Files.Add(file);

                var cleansingLogPhoto = new CleansingLogPhoto();
                cleansingLogPhoto.CleansingLogPhotoPK = Guid.NewGuid();
                cleansingLogPhoto.CleansingLogFK = cleansingLog.CleansingLogPK;
                cleansingLogPhoto.File = file;
                cleansingLogPhoto.IsActive = true;
                cleansingLogPhoto.CreatedByFK = userDetails.ID;
                cleansingLogPhoto.CreatedDate = model.Time;
                cleansingLogPhoto.UpdatedByFK = userDetails.ID;
                cleansingLogPhoto.UpdatedDate = DateTime.Now;

                Context.CleansingLogPhotos.Add(cleansingLogPhoto);

                Context.SaveChanges();
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, model);
                ExceptionDispatchInfo.Capture(e).Throw();
            }

            return true;
        }

        public void PushActivityDetails(ActivityDetailModel model)
        {
            try
            {
                var userDetails = GetUserContext().UserDetails;
                var workInstruction = Context.WorkInstructions.Where(a => a.WorkInstructionPK == model.ActivityID).Single();

                workInstruction.SiteName = model.SiteName;
                workInstruction.ActivityPriorityFK = model.ActivityPriorityID;
                workInstruction.Operator = model.Operator;

                Context.SaveChanges();
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, model);
                ExceptionDispatchInfo.Capture(e).Throw();
            }
        }

        public void PushActivityRiskAssessments(List<ActivityRiskAssessmentModel> models)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                try
                {
                    var userDetails = GetUserContext().UserDetails;

                    foreach (var model in models)
                    {
                        var activityRiskAssessment = Context.ActivityRiskAssessments.Where(a => a.ActivityRiskAssessmentPK == model.ActivityRiskAssessmentID).SingleOrDefault();

                        if (activityRiskAssessment == null)
                        {
                            activityRiskAssessment = new ActivityRiskAssessment();
                            activityRiskAssessment.ActivityRiskAssessmentPK = model.ActivityRiskAssessmentID;
                            activityRiskAssessment.TaskFK = model.TaskID;
                            activityRiskAssessment.CleansingLogFK = model.CleansingLogID;
                            //activityRiskAssessment.WorkInstructionFK = model.ActivityID;
                            activityRiskAssessment.IsActive = true;
                            activityRiskAssessment.CreatedByFK = userDetails.ID;
                            activityRiskAssessment.CreatedDate = model.CreatedDate;
                            activityRiskAssessment.UpdatedByFK = userDetails.ID;
                            activityRiskAssessment.UpdatedDate = DateTime.Now;

                            Context.ActivityRiskAssessments.Add(activityRiskAssessment);
                        }

                        activityRiskAssessment.RoadName = model.RoadName;
                        activityRiskAssessment.RoadSpeedFK = model.RoadSpeedID;
                        activityRiskAssessment.TMRequirementFK = model.TMRequirementID;
                        activityRiskAssessment.Schools = model.Schools;
                        activityRiskAssessment.TreeCanopies = model.TreeCanopies;
                        activityRiskAssessment.PedestrianCrossings = model.PedestrianCrossings;
                        activityRiskAssessment.OverheadCables = model.OverheadCables;
                        activityRiskAssessment.WaterCourses = model.WaterCourses;
                        activityRiskAssessment.AdverseWeather = model.AdverseWeather;
                        activityRiskAssessment.Notes = model.Notes;

                        Context.SaveChanges();
                    }

                    tran.Commit();
                }
                catch (Exception e)
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, models);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public void PushUnloadingProcesses(List<UnloadingProcessModel> models)
        {
            using (var tran = Context.Database.BeginTransaction())
            {
                try
                {
                    var userDetails = GetUserContext().UserDetails;

                    foreach (var model in models)
                    {
                        var unloadingProcess = Context.UnloadingProcesses.Where(a => a.UnloadingProcessPK == model.UnloadingProcessID).SingleOrDefault();

                        if (unloadingProcess == null)
                        {
                            unloadingProcess = new UnloadingProcess();
                            unloadingProcess.UnloadingProcessPK = model.UnloadingProcessID;
                            unloadingProcess.TaskFK = model.TaskID;
                            unloadingProcess.WorkInstructionFK = model.ActivityID;
                            unloadingProcess.IsActive = true;
                            unloadingProcess.CreatedByFK = userDetails.ID;
                            unloadingProcess.CreatedDate = model.CreatedDate;
                            unloadingProcess.UpdatedByFK = userDetails.ID;
                            unloadingProcess.UpdatedDate = DateTime.Now;

                            Context.UnloadingProcesses.Add(unloadingProcess);
                        }

                        unloadingProcess.DepartToDisposalSiteDate = model.DepartToDisposalSiteDate;
                        unloadingProcess.ArriveAtDisposalSiteDate = model.ArriveAtDisposalSiteDate;
                        unloadingProcess.DisposalSiteFK = model.DisposalSiteID;
                        unloadingProcess.WasteTicketNumber = model.WasteTicketNumber;
                        unloadingProcess.VolumeOfWasteDisposed = model.VolumeOfWasteDisposed;
                        unloadingProcess.Comments = model.Comments;
                        unloadingProcess.LeaveDisposalSiteDate = model.LeaveDisposalSiteDate;

                        Context.SaveChanges();
                    }

                    tran.Commit();
                }
                catch (Exception e)
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    e.Data.AddSafely(Global.ExceptionDataKey, models);
                    ExceptionDispatchInfo.Capture(e).Throw();
                }
            }
        }

        public bool PushUnloadingProcessPhoto(UnloadingProcessPhotoModel model)
        {
            try
            {
                var userDetails = GetUserContext().UserDetails;
                var unloadingProcess = Context.UnloadingProcesses.Where(a => a.UnloadingProcessPK == model.UnloadingProcessID).Single();

                var file = new File();
                file.FilePK = Guid.NewGuid();
                file.FileDownloadName = "Unloading Process Photo " + unloadingProcess.WorkInstruction.WorkInstructionNumber + " " + model.Time.ToString();
                file.ContentType = ".jpg";
                file.Data = Convert.FromBase64String(model.DataString);
                file.Longitude = model.Longitude;
                file.Latitude = model.Latitude;
                file.Comments = model.Comments;
                file.IsActive = true;
                file.CreatedByFK = userDetails.ID;
                file.CreatedDate = model.Time;
                file.UpdatedByFK = userDetails.ID;
                file.UpdatedDate = DateTime.Now;

                Context.Files.Add(file);

                var unloadingProcessPhoto = new UnloadingProcessPhoto();
                unloadingProcessPhoto.UnloadingProcessPhotoPK = Guid.NewGuid();
                unloadingProcessPhoto.UnloadingProcessFK = unloadingProcess.UnloadingProcessPK;
                unloadingProcessPhoto.File = file;
                unloadingProcessPhoto.IsActive = true;
                unloadingProcessPhoto.CreatedByFK = userDetails.ID;
                unloadingProcessPhoto.CreatedDate = model.Time;
                unloadingProcessPhoto.UpdatedByFK = userDetails.ID;
                unloadingProcessPhoto.UpdatedDate = DateTime.Now;

                Context.UnloadingProcessPhotos.Add(unloadingProcessPhoto);

                Context.SaveChanges();
            }
            catch (Exception e)
            {
                e.Data.AddSafely(Global.ExceptionDataKey, model);
                ExceptionDispatchInfo.Capture(e).Throw();
            }

            return true;
        }

        #endregion

        #region Data Helpers

        public void LogError(string description, Exception exception, bool app)
        {
            // full exception messages
            var exceptionString = exception?.ToString() ?? "";

            // add entity validation errors
            exceptionString += EntityValidationErrors(exception);

            // add any parameters passed with exception data (converted to json)
            exceptionString += ExceptionDataToJson(exception);

            // save to dbo.ErrorLogs
            LogError(description, exceptionString, app);
        }

        public void LogError(string description, string exception, bool app)
        {
            using (var ctx = new FMConwayEntities())
            {
                string id = "";
                try
                {
                    id = GetUserContext().UserDetails.ID.ToString();
                }
                catch (Exception e)
                {
                    exception = exception + Environment.NewLine + e;
                }

                var error = new ErrorLog();
                error.LogPK = Guid.NewGuid();
                error.Description = description;
                error.Exception = "UserID: " + id + Environment.NewLine + Environment.NewLine + exception;
                error.Location = app ? "Application" : "Service";
                error.CreatedDate = DateTime.Now;

                ctx.ErrorLogs.Add(error);

                ctx.SaveChanges();
            }
        }

        private string EntityValidationErrors(Exception exception)
        {
            var exceptionString = "";

            try
            {
                if (exception == null)
                {
                    return exceptionString;
                }

                var dbEntityValidationException = exception as DbEntityValidationException;
                if (dbEntityValidationException != null && dbEntityValidationException.EntityValidationErrors != null && dbEntityValidationException.EntityValidationErrors.Any(a => a.ValidationErrors.Any()))
                {
                    exceptionString += Environment.NewLine + Environment.NewLine + "-- Entity Validation Errors --" + Environment.NewLine;

                    foreach (var dbEntityValidationResult in dbEntityValidationException.EntityValidationErrors)
                    {
                        if (dbEntityValidationResult.ValidationErrors.Any())
                        {
                            try
                            {
                                // entity name
                                Type entityType = dbEntityValidationResult.Entry.Entity.GetType();

                                if (entityType.BaseType != null && entityType.Namespace == "System.Data.Entity.DynamicProxies")
                                {
                                    entityType = entityType.BaseType;
                                }

                                exceptionString += "Entity: " + entityType?.Name + Environment.NewLine;
                            }
                            catch
                            {
                                // ignore
                            }

                            // field errors
                            foreach (var dbValidationError in dbEntityValidationResult.ValidationErrors)
                            {
                                exceptionString += dbValidationError?.ErrorMessage + Environment.NewLine;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                exceptionString += "Error retrieving entity validation errors - " + e.Message + Environment.NewLine;
            }

            return exceptionString;
        }

        // exception.Data[key] can be an object (but not anonymous obj), list, array or just a simple primitive type
        // If you need to pass multiple parameters use a dictionary: e.Data.AddSafely(key, new Dictionary<string, object>)
        private string ExceptionDataToJson(Exception exception)
        {
            string json = "";

            try
            {
                if (exception?.Data == null)
                {
                    return json;
                }

                if (exception.Data.Contains(Global.ExceptionDataKey))
                {
                    json += Environment.NewLine + Environment.NewLine + "-- JSON --" + Environment.NewLine;

                    var parameters = exception.Data[Global.ExceptionDataKey];

                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    jss.MaxJsonLength = int.MaxValue;
                    jss.RegisterConverters(new List<JavaScriptConverter>
                    {
                        new ExcludeByteArrayConverter()
                    });

                    json += jss.Serialize(parameters) + Environment.NewLine;
                }
            }
            catch (Exception e)
            {
                json += "Error serializing exception data - " + e.Message + Environment.NewLine;
            }

            return json;
        }

        private ServiceUser GetUserContext()
        {
            string currentUsername = OperationContext.Current.ServiceSecurityContext.AuthorizationContext.Properties["Identity"] as String;

            var sms = new Triangle.Membership.Services.Sql.SqlMembershipService();
            return new ServiceUser(sms, currentUsername);
        }

        private DateTime GetWeekStart()
        {
            DateTime weekStart = DateTime.Now.Date;

            switch ((int)weekStart.DayOfWeek)
            {
                case 0:
                    break;
                case 1:
                    weekStart = weekStart.AddDays(-1);
                    break;
                case 2:
                    weekStart = weekStart.AddDays(-2);
                    break;
                case 3:
                    weekStart = weekStart.AddDays(-3);
                    break;
                case 4:
                    weekStart = weekStart.AddDays(-4);
                    break;
                case 5:
                    weekStart = weekStart.AddDays(-5);
                    break;
                case 6:
                    weekStart = weekStart.AddDays(-6);
                    break;
            }

            return weekStart;
        }

        #endregion

    }
}