﻿using Mobile.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Mobile
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService
    {

        /// <summary>
        /// Authenticates a user. If successful the time of the request will be used as the users start time if 
        /// one does not already exists for the day.
        /// </summary>
        [OperationContract]
        UserProfileModel SignOn();

        #region Data For App

        [OperationContract]
        IEnumerable<UserModel> GetUsers();

        [OperationContract]
        IEnumerable<StaffModel> GetStaff();

        [OperationContract]
        IEnumerable<VehicleModel> GetVehicles();

        [OperationContract]
        IEnumerable<EquipmentTypeModel> GetEquipmentTypes();

        [OperationContract]
        IEnumerable<EquipmentModel> GetEquipment();

        [OperationContract]
        IEnumerable<ChecklistModel> GetChecklists();

        [OperationContract]
        IEnumerable<VehicleTypeModel> GetVehicleTypes();

        [OperationContract]
        IEnumerable<BriefingTypeModel> GetBriefingTypes();

        [OperationContract]
        IEnumerable<ActivityTypeModel> GetActivityTypes();

        //[OperationContract]
        //IEnumerable<ObservationTypeModel> GetObservationTypes();

        [OperationContract]
        IEnumerable<TaskTypeModel> GetTaskTypes();

        [OperationContract]
        IEnumerable<WeatherConditionModel> GetWeatherConditions();

        [OperationContract]
        IEnumerable<ShiftModel> GetShifts(Guid userID, string syncDate);

        [OperationContract]
        ShiftUpdateModel GetShiftUpdate(Guid shiftID);

        [OperationContract]
        IEnumerable<JobPackModel> GetJobPacks(Guid userID, string syncDate);

        [OperationContract]
        IEnumerable<AreaModel> GetAreas(Guid userID, string syncDate);

        [OperationContract]
        string GetStaffRecord(Guid staffRecordID);

        [OperationContract]
        string GetDocument(Guid documentID, Guid jobPackID, bool briefing);

        [OperationContract]
        IEnumerable<ShiftVehicleModel> GetVehiclesOnShifts();

        [OperationContract]
        IEnumerable<VehicleChecklistModel> GetVehicleChecklists(Guid userID, string syncDate);

        [OperationContract]
        IEnumerable<ErrorLogModel> GetErrorLogs(int quantity);

        [OperationContract]
        bool CheckForNewShifts(Guid userID, string syncDate);

        [OperationContract]
        IEnumerable<ActivityPriorityModel> GetActivityPriorities();

        [OperationContract]
        IEnumerable<LaneModel> GetLanes();

        [OperationContract]
        IEnumerable<TMRequirementModel> GetTMRequirements();

        [OperationContract]
        IEnumerable<RoadSpeedModel> GetRoadSpeeds();

        [OperationContract]
        IEnumerable<DirectionModel> GetDirections();

        [OperationContract]
        IEnumerable<DisposalSiteModel> GetDisposalSites();

        [OperationContract]
        IEnumerable<CustomerModel> GetCustomers();

        [OperationContract]
        IEnumerable<ContactModel> GetContacts();

        [OperationContract]
        IEnumerable<MethodStatementModel> GetMethodStatements(DateTime syncDate);

        [OperationContract]
        string GetMethodStatementDocument(Guid methodStatementID);

        [OperationContract]
        IEnumerable<DepotModel> GetDepots();

        #endregion

        #region Data From App

        [OperationContract]
        void UpdateShiftStatus(Guid shiftID, int completionStatus);

        [OperationContract]
        void CompleteShift(Guid shiftID);

        [OperationContract]
        void PushCancelShift(Stream data);

        [OperationContract]
        List<ShiftProgressResult> UpdateShiftProgress(Stream data);

        [OperationContract]
        ServiceResult<string> UpdateShiftProgressStarted(Stream data);

        [OperationContract]
        void PushShiftStaff(Stream data);

        [OperationContract]
        void PushShiftVehicles(Stream data);

        [OperationContract]
        void PushVehicleChecklists(Stream data);

        [OperationContract]
        void PushTasks(Stream data);

        [OperationContract]
        void PushTaskChecklists(Stream data);

        [OperationContract]
        void PushTaskEquipment(Stream data);

        [OperationContract]
        void PushAreas(Stream data);

        [OperationContract]
        void PushAccidents(Stream data);

        [OperationContract]
        void PushObservations(Stream data);

        [OperationContract]
        void PushOutOfHours(Stream data);

        [OperationContract]
        void PushNotes(Stream data);

        [OperationContract]
        void PushMaintenanceChecks(Stream data);

        [OperationContract]
        bool PushMaintenanceCheckDetailPhoto(Stream data);
                
        [OperationContract]
        bool PushBriefingSignature(Stream data);

        [OperationContract]
        bool PushActivitySignature(Stream data);

        [OperationContract]
        bool PushDefectPhoto(Stream data);

        [OperationContract]
        bool PushTaskPhoto(Stream data);

        [OperationContract]
        bool PushTaskChecklistAnswerPhoto(Stream data);

        [OperationContract]
        bool PushTaskSignature(Stream data);

        [OperationContract]
        bool PushLoadingSheetSignature(Stream data);

        [OperationContract]
        void PushEquipmentItems(Stream data);

        [OperationContract]
        void PushError(Stream data);

        [OperationContract]
        void PushDatabase(Stream data);

        [OperationContract]
        void PushCleansingLogs(Stream data);

        [OperationContract]
        bool PushCleansingLogPhoto(Stream data);

        [OperationContract]
        void PushActivityDetails(Stream data);

        [OperationContract]
        void PushActivityRiskAssessments(Stream data);

        [OperationContract]
        void PushUnloadingProcesses(Stream data);

        [OperationContract]
        bool PushUnloadingProcessPhoto(Stream data);

        #endregion

    }

}
